# Bolt
## Tasks
* Identify port with CMS
* Identify username and password
* Identify CMS version
* CVE EDB-ID
* Exploit Path
* flag.txt
## Vulnerabilities
* Leaving credentials in public!
* Using CMS version that is vulnerable.


## NMAP SCAN
```nmap
Reason: 997 conn-refused
PORT     STATE SERVICE REASON  VERSION
22/tcp   open  ssh     syn-ack OpenSSH 7.6p1 Ubuntu 4ubuntu0.3 (Ubuntu Linux; protocol 2.0)
| ssh-hostkey: 
|   2048 f3:85:ec:54:f2:01:b1:94:40:de:42:e8:21:97:20:80 (RSA)
| ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQDaKxKph/4I3YG+2GjzPjOevcQldxrIll8wZ8SZyy2fMg3S5tl5G6PBFbF9GvlLt1X/gadOlBc99EG3hGxvAyoujfdSuXfxVznPcVuy0acAahC0ohdGp3fZaPGJMl7lW0wkPTHO19DtSsVPniBFdrWEq9vfSODxqdot8ij2PnEWfnCsj2Vf8hI8TRUBcPcQK12IsAbvBOcXOEZoxof/IQU/rSeiuYCvtQaJh+gmL7xTfDmX1Uh2+oK6yfCn87RpN2kDp3YpEHVRJ4NFNPe8lgQzekGCq0GUZxjUfFg1JNSWe1DdvnaWnz8J8dTbVZiyNG3NAVAwP1+iFARVOkiH1hi1
|   256 77:c7:c1:ae:31:41:21:e4:93:0e:9a:dd:0b:29:e1:ff (ECDSA)
| ecdsa-sha2-nistp256 AAAAE2VjZHNhLXNoYTItbmlzdHAyNTYAAAAIbmlzdHAyNTYAAABBBE52sV7veXSHXpLFmu5lrkk8HhYX2kgEtphT3g7qc1tfqX4O6gk5IlBUH25VUUHOhB5BaujcoBeId/pMh4JLpCs=
|   256 07:05:43:46:9d:b2:3e:f0:4d:69:67:e4:91:d3:d3:7f (ED25519)
|_ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAINZwq5mZftBwFP7wDFt5kinK8mM+Gk2MaPebZ4I0ukZ+
80/tcp   open  http    syn-ack Apache httpd 2.4.29 ((Ubuntu))
| http-methods: 
|_  Supported Methods: GET POST OPTIONS HEAD
|_http-server-header: Apache/2.4.29 (Ubuntu)
|_http-title: Apache2 Ubuntu Default Page: It works
8000/tcp open  http    syn-ack (PHP 7.2.32-1)

```

Let's look at port 8000, it looks more interesting.
Looks like there's a post from an admin named. There's one interesting post.
```txt
Hey guys,

i suppose this is our secret forum right? I posted my first message for our readers today but there seems to be a lot of freespace out there. Please check it out! my password is boltadmin123 just incase you need it!

Regards,

Jake (Admin)
```
Looks like we have the password of Jake.
```txt
bolt:boltadmin123
```
## Enumeration
After hours hours of enumeration, nothing else was found. 
Now let's check out some exploits about Bolt CMS.
```txt
[Bolt CMS 3.7.0 - Authenticated Remote Code Execution](https://www.exploit-db.com/exploits/48296)
```
They are quite a few, but this one stands out because right now we have the credentials for the user.
Let's give it a shot, I'll be using Metasploit for this.
```bash
0  exploit/unix/webapp/bolt_authenticated_rce  2020-05-07       excellent  Yes    Bolt CMS 3.7.0 - Authenticated Remote Code Execution
```
Look at the options and set accordingly.


## Foothold
Running the exploit and it works! Let's upgrade to a better shell first using the shell command.
```bash
root@bolt:~/public/files#

```
Instantly root! Let's grab that flag.
**For those who haven't found the version of the CMS.You can find the version of it by heading to /vendor/bolt/bolt and view the changelog.md**
