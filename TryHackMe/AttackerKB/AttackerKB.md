# Summary
**During the initial recon, we found a web server running webmin 1.890. The specific version of webmin is vulnerable to CVE-2019-15107 which can lead to RCE and total compromise of the machine. The exploit will put us in a root shell.**

# Enumeration
## NMAP
```txt
PORT      STATE SERVICE REASON  VERSION
22/tcp    open  ssh     syn-ack OpenSSH 7.6p1 Ubuntu 4ubuntu0.3 (Ubuntu Linux; protocol 2.0)
| ssh-hostkey: 
|   2048 b7:4c:d0:bd:e2:7b:1b:15:72:27:64:56:29:15:ea:23 (RSA)
| ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQDbZAxRhWUij6g6MP11OkGSk7vYHRNyQcTIdMmjj1kSvDhyuXS9QbM5t2qe3UMblyLaObwKJDN++KWfzl1+beOrq3sXkTA4Wot1RyYo0hPdQT0GWBTs63dll2+c4yv3nDiYAwtSsPLCeynPEmSUGDjkVnP12gxXe/qCsM2+rZ9tzXtSWiXgWvaxMZiHaQpT1KaY0z6ebzBTI8siU0t+6SMK7rNv1CsUNpGeicfbC5ZOE4/Nbc8cxNl7gDtZbyjdh9S7KTvzkSj2zBJ+8VbzsuZk1yy8uyLDgmuBQ6LzbYUNHkTQhJetVq7utFpRqLdpSJTcsz5PAxd1Upe9DqoYURuL
|   256 b7:85:23:11:4f:44:fa:22:00:8e:40:77:5e:cf:28:7c (ECDSA)
| ecdsa-sha2-nistp256 AAAAE2VjZHNhLXNoYTItbmlzdHAyNTYAAAAIbmlzdHAyNTYAAABBBEYCha8jk+VzcJRRwV41rl8EuJBiy7Cf8xg6tX41bZv0huZdCcCTCq9dLJlzO2V9s+sMp92TpzR5j8NAAuJt0DA=
|   256 a9:fe:4b:82:bf:89:34:59:36:5b:ec:da:c2:d3:95:ce (ED25519)
|_ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIOJnY5oycmgw6ND6Mw4y0YQWZiHoKhePo4bylKKCP0E5
10000/tcp open  http    syn-ack MiniServ 1.890 (Webmin httpd)
|_http-favicon: Unknown favicon MD5: C326121BF57F4EB231AB3017308C8280
| http-methods: 
|_  Supported Methods: GET HEAD POST OPTIONS
|_http-title: Site doesn't have a title (text/html; Charset=iso-8859-1).
Service Info: OS: Linux; CPE: cpe:/o:linux:linux_kernel

```

## Web
- We get an error
```txt
This web server is running in SSL mode. Try the URL [https://ip-10-10-3-178.eu-west-1.compute.internal:10000/](https://ip-10-10-3-178.eu-west-1.compute.internal:10000/) instead.
```
- If we change it to `https://10.10.3.178:10000/`. We get a login page for webmin.
- We can view the certificate to check for the hostname.
`root@source`
- We can add that to our `/etc/hosts`

# Finding exploit with AttackerKB
AttackerKB is something similar to exploitDB.
- Let's search for `webmin 1.890`
- We immediately see a [high severity exploit](https://attackerkb.com/topics/hxx3zmiCkR/webmin-password-change-cgi-command-injection?referrer=search)
- CVE number is [CVE-2019-15107](https://attackerkb.com/search?q=CVE-2019-15107)
- The type of attack was supply chain.
- We can directly read the attack on webmin's [site](https://www.webmin.com/exploit.html)

# Exploit
Since we know that there is a Metasploit module for this. We can use Metasploit to make our lives easier.
```bash
 5  exploit/linux/http/webmin_backdoor           2019-08-10       excellent  Yes    Webmin password_change.cgi Backdoor
 ```
 - Like from what we read, we'll have a root shell when the exploit completes.
 ```bash
 root@source:/usr/share/webmin/# whoami
whoami
root
```

 