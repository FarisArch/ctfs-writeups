# Enumeration
## NMAP SCAN
```nmap
PORT   STATE SERVICE VERSION
22/tcp open  ssh     OpenSSH 7.6p1 Ubuntu 4ubuntu0.3 (Ubuntu Linux; protocol 2.0)
| ssh-hostkey: 
|   2048 34:0e:fe:06:12:67:3e:a4:eb:ab:7a:c4:81:6d:fe:a9 (RSA)
|   256 49:61:1e:f4:52:6e:7b:29:98:db:30:2d:16:ed:f4:8b (ECDSA)
|_  256 b8:60:c4:5b:b7:b2:d0:23:a0:c7:56:59:5c:63:1e:c4 (ED25519)
80/tcp open  http    Apache httpd 2.4.29 ((Ubuntu))
|_http-server-header: Apache/2.4.29 (Ubuntu)
|_http-title: House of danak
Service Info: OS: Linux; CPE: cpe:/o:linux:linux_kernel
```
## Web
- robots.txt contains :
`/uploads`
- Found possible username from comment in source.
```html
<!-- john, please add some actual content to the site! lorem ipsum is horrible to look at. -->
```
### /Uploads/
- Contains 3 files :
`dict.lst,manifesto.txt,meme.jpg`
- `dict.lst` looks like a wordlist of possible passwords

### Feroxbuster
```txt
301        9l       28w      312c http://10.10.13.81/uploads
301        9l       28w      311c http://10.10.13.81/secret
```
- `/secret` contains a private key for SSH.

# Exploit
- Key is password protected, we can use ssh2john to brute the password.
- We get the password `letmein`
- We can SSH using the user `john` and the key.

# Privilege Escalation
- We are user john, check `sudo -l` but we have no password for john.
- Check groups we're in `lxd` group. Possible escalation vector.
- Check Hacktricks for lxd/lxc.
-  We can escalate by mounting the `/` in our lxd container.
-  The steps are quite lengthy to list down so [refer to this](https://book.hacktricks.xyz/linux-unix/privilege-escalation/interesting-groups-linux-pe/lxd-privilege-escalation)
-  We can now read root.txt since we mounted `/` in our container at `/mnt/root`


