# Apache Log Analysis II
## Tasks
### Name one of the non-standard HTTP requests
Let's head to /var/log/apache2 and check out access.log
Skimming through it I immediately saw something weird
```bash
192.168.56.206 - - [20/Apr/2021:13:30:15 +0000] "GXWR / HTTP/1.1" 501 498 "-" "Mozilla/5.0 (iPhone; CPU iPhone OS 12_2 like Mac OS X) AppleWebKit/605.1.15 (KHTML"

```
### At what time was the Nmap scan performed
Refer to the above for the time

# Persistence Mechanism II
## Tasks
### What username and hostname combination can be found in one of the authorized_keys file?
Since Fred doesn't have a .ssh directory, let's check out the root directory.
```bash
fred@acmeweb:/home$ sudo cat /root/.ssh/authorized_keys
ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABgQDYCKt0bYP2YIwMWdJWqF3lr3Drs3sS9hiybsxz9W6dG6d15mg0SVMSe5H+rPM6VmzOKJaVpDjT1Ll5eR6YcbefTF2bMXHveyvcrzDxyZeWdgBs5u8/4DZxEN6fq6IZRRftmrMgMzSnpmdCm8kvacgq3lIjLx/sKAlX9GqPIz09t0Rk5MB7zk3lg1wdTZxZwwCHPbZW7mGlVcxNBB9wdbAmcvezscoF0i7v0tY8iCoFlrBysOMBMrEJji2UONtI/wrt7AvoK+gshiG7VTjZ2oQBacnyHRToXHxOZiSIbCQrJ6rCxa32QOGQNmAVIucqYjRbJedz0NbGq7M9B+hBmG/mdtsoGOXQKyzoUlAbulRXjSVtManiUyq9im1HBHfuduiBrbfcOKz24NMT7RaIsPsZCUCpfHaT7S5XplQypAjkxABds8jod/TXcTYibdWE9scrUUidgCsPELQlKEfhhZ8+cyjbMCGNB5LOgieJSVk6D1JC97TaFNi4X9/9i2UA+L0= kali@kali


```
Our attack is from the hostname kali as user kali

# Program Execution History
## Tasks
### What is the first command in root's bash_history file?
Seems like an easy task.
```bash
fred@acmeweb:/home$ sudo cat /root/.bash_history
nano /etc/passwd
exit
```




