# Persistence Mechanisms III
## Figure out what's going on and find the flag
The moment I SSH in. It's like hell in here.
Let's try to do our best to find it. Let's check out systemctl
For this I compared between my machine's services and the VM's and one I found weird was IpManager.service. Let's check out the status of it.
```bash
fred@acmeweb:~$ systemctl status IpManager.service
● IpManager.service
   Loaded: loaded (/var/lib/network/IpManager.service; enabled; vendor preset: enabled)
   Active: active (running) since Sat 2021-06-12 14:27:22 UTC; 17min ago
 Main PID: 1357 (bash)
    Tasks: 2 (limit: 499)
   CGroup: /system.slice/IpManager.service
           ├─1357 /bin/bash /etc/network/ZGtsam5hZG1ua2Fu.sh
           └─2123 sleep 10

```
From the looks of it, it executes that script and sleeps for 10 seconds.
Let's head out there and go to that file where that flag maybe
```bash
##[gh0st_1n_the_machine]  
##
```
Deleting that file the process still goes on. Guess It is a ghost machine.


This is a really fun room and I recommend you all to try this.
Thank you for creathing this room
Made by ben and UP948723 and up934641 and CoolComputerMan