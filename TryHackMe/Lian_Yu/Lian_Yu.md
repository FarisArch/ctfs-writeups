Welcome to Lian_YU, this Arrowverse themed beginner CTF box! Capture the flags and have fun.

# Tasks
1. Web Directory found
2. File name found
3. FTP password
4. SSH password
5. User.txt
6. Root.txt

# NMAP SCAN
```nmap
PORT    STATE SERVICE VERSION  
21/tcp  open  ftp     vsftpd 3.0.2  
22/tcp  open  ssh     OpenSSH 6.7p1 Debian 5+deb8u8 (protocol 2.0)  
| ssh-hostkey:    
|   1024 56:50:bd:11:ef:d4:ac:56:32:c3:ee:73:3e:de:87:f4 (DSA)  
|   2048 39:6f:3a:9c:b6:2d:ad:0c:d8:6d:be:77:13:07:25:d6 (RSA)  
|   256 a6:69:96:d7:6d:61:27:96:7e:bb:9f:83:60:1b:52:12 (ECDSA)  
|_  256 3f:43:76:75:a8:5a:a6:cd:33:b0:66:42:04:91:fe:a0 (ED25519)  
80/tcp  open  http    Apache httpd  
|_http-title: Purgatory  
|_http-server-header: Apache  
111/tcp open  rpcbind 2-4 (RPC #100000)  
| rpcinfo:    
|   program version    port/proto  service  
|   100000  2,3,4        111/tcp   rpcbind  
|   100000  2,3,4        111/udp   rpcbind  
|   100000  3,4          111/tcp6  rpcbind  
|   100000  3,4          111/udp6  rpcbind  
|   100024  1          39758/udp6  status  
|   100024  1          39958/udp   status  
|   100024  1          46885/tcp6  status  
|_  100024  1          54163/tcp   status  
Service Info: OSs: Unix, Linux; CPE: cpe:/o:linux:linux_kernel
```
It's the newest version of FTP so no anonymous login or any exploits.

# Web (Port 80)
## Findings
- No robots.txt
- Apache

### Feroxbuster
```bash
301 7l       20w      234c http://10.10.39.73/island
```
using raft-medium-directories.txt
Code word is `vigilante`. possible password?
```bash
200 16l       35w      292c http://10.10.39.73/island/2100/
```
```html
<!-- you can avail your .ticket here but how?   -->
```
Possible extension?
using raft-large-words.txt
- Brings us to a site with a Youtube video but isn't available.
```bash
200 6l       11w       71c http://10.10.39.73/island/2100/green_arrow.ticket
```
using directory-list-2.3-medium.txt
```txt
This is just a token to get into Queen's Gambit(Ship)


RTy8yhBQdscX
```
Password is base58 encoded.
`!#th3h00d`
Possible Credentials :
`vigilante:!#th3h00d`
There is also other users
`slade`

# FTP (Port 21)
- We can login using the credentials.
- A bunch of images, possible stego challenge.
- File `Leave_me_alone.png` is broken, the file signature is not correct.
- We can fix it using ghex and set it PNGs file.
- We can view the password now.
`password`
- We can use the password to extract file from `aa.jpg`
- File contains `ss.zip`
- Contains shado and passwd.txt\
- shado contains the password

# Foothold
We can login into SSH using `slade` and password from shado.

# Privilege Escalation

- Check sudo -l
```bash
User slade may run the following commands on LianYu:  
   (root) PASSWD: /usr/bin/pkexec
```
- Entry of GTFObins
```bash
sudo pkexec /bin/sh
```
We are root.


