## IP Address Classes
| Classes | Range|
|---------|-------|
| Class A | 1 - 127|
| Class B | 128 - 191 |
| Class C | 192 - 223|
| Class D | 224 - 239 |
| Class E | 240 - 255 |

## Private Address Space

| Classes | Range|
|---------|-------|
| Class A | 10.0.0.0|
| Class B | 172.16.0.0 to 172.31.255.255 |
| Class C | 192.168.0.0 to 198.168.255.255 |

There are 5 categories of IPv4 addresses

Class E is used for research purposes. ( It's reserved)

There are 3 types of private address ranges.

Most business uses class A for their IP address, example TryHackMe.

The first default private range for home routers is 192.168.0.0.
The second common private home range is 192.168.1.0

The very last address in a range is reserved for broadcast

The third predominant address is typically reserved for the router, it's called gateway

An address used to test on individual computers is 127.0.0.1 or localhost/loopback address

A unique address reserved for unroutable packets is 0.0.0.0








