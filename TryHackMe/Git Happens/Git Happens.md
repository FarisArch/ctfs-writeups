# Tasks
* Super Secret Password


# Vulnerabilities
* Sensitive data exposure

# NMAP SCAN
```nmap
Starting Nmap 7.91 ( https://nmap.org ) at 2021-08-09 20:51 +08
Nmap scan report for 10.10.44.111
Host is up (0.24s latency).
Not shown: 999 closed ports
PORT   STATE SERVICE VERSION
80/tcp open  http    nginx 1.14.0 (Ubuntu)
| http-git: 
|   10.10.44.111:80/.git/
|     Git repository found!
|_    Repository description: Unnamed repository; edit this file 'description' to name the...
|_http-server-header: nginx/1.14.0 (Ubuntu)
|_http-title: Super Awesome Site!
Service Info: OS: Linux; CPE: cpe:/o:linux:linux_kernel

Service detection performed. Please report any incorrect results at https://nmap.org/submit/ .
Nmap done: 1 IP address (1 host up) scanned in 39.07 seconds
```
We found a .git repository.

We can dump it using scripts that are available on github
I used these in particular
https://github.com/internetwache/GitTools

Now I'll be using Git Cola to backtrack commits to see if I can find any passwords.
Going back a few commits I found a hash.
```txt
4004c23a71fd6ba9b03ec9cb7eed08471197d84319a865c5442a9d6a7c7cbea070f3cb6aa5106ef80f679a88dbbaf89ff64cb351a151a5f29819a3c094ecebbb
```
It seems to be SHA-512,we'll keep this in mind and keep down the commit history.

And what do you know, a hardcoded password was found in index.html
```txt
Th1s_1s_4_L0ng_4nd_S3cur3_P4ssw0rd!
```

