# Enumeration
## NMAP
```nmap
PORT      STATE SERVICE VERSION
22/tcp    open  ssh     OpenSSH 7.6p1 Ubuntu 4ubuntu0.3 (Ubuntu Linux; protocol 2.0)
| ssh-hostkey: 
|   2048 f3:a2:ed:93:4b:9c:bf:bb:33:4d:48:0d:fe:a4:de:96 (RSA)
|   256 22:72:00:36:eb:37:12:9f:5a:cc:c2:73:e0:4f:f1:4e (ECDSA)
|_  256 78:1d:79:dc:8d:41:f6:77:60:65:f5:74:b6:cc:8b:6d (ED25519)
30024/tcp open  ftp     vsftpd 3.0.3
| ftp-anon: Anonymous FTP login allowed (FTP code 230)
| -rw-r--r--    1 ftp      ftp          1743 Mar 23 20:03 id_rsa
|_-rw-r--r--    1 ftp      ftp            78 Mar 23 20:09 note.txt
| ftp-syst: 
|   STAT: 
| FTP server status:
|      Connected to ::ffff:10.17.1.163
|      Logged in as ftp
|      TYPE: ASCII
|      No session bandwidth limit
|      Session timeout in seconds is 300
|      Control connection is plain text
|      Data connections will be plain text
|      At session startup, client count was 2
|      vsFTPd 3.0.3 - secure, fast, stable
|_End of status
Service Info: OSs: Linux, Unix; CPE: cpe:/o:linux:linux_kernel
```

## FTP
- Found id_rsa in ftp and note.txt
- note.txt contains possible username
`errorcauser`

# Exploit
- We can crack the id_rsa password using ssh2john
- We get the password `cupcake`.
- We can SSH into the box using user `errorcauser`

## Internal Enumeration.
- Found note.txt
`I've set up a webserver locally so no one outside could access it`
- We can set Dynamic Port Forwarding.
1. `ssh -i id_rsa -D 1337 errorcauser@10.10.120.69`
2. `Add entry to /etc/proxychains`
3. `proxychains nmap -sT localhost` to find open ports
4. `Port forward the ports you want to localhost`
- We found port 80 and 3306 is open. 3306 is MySQL port.

## Web enumeration
- Wordpress 5.7
- MySQL
- Apache 2.4.29
- We can scan the web for plugins using nmap.
`sudo nmap -sS localhost --script http-wordpress-enum --script-args search-limit=1500 -vv`
```bash
PORT   STATE SERVICE REASON
80/tcp open  http    syn-ack ttl 64
| http-wordpress-enum: 
| Search limited to top 1500 themes/plugins
|   plugins
|     duplicator 1.3.26
|_    wp-file-manager 6.0

NSE: Script Post-scanning.
NSE: Starting runlevel 1 (of 1) scan.
Initiating NSE at 13:32
Completed NSE at 13:32, 0.00s elapsed
Read data files from: /usr/bin/../share/nmap
```
- Found known exploit for `duplicator 1.3.26`
`Wordpress Plugin Duplicator 1.3.26 - Unauthenticated Arbitrary File Read (Metasploit) `
- CVE number is CVE-2020-11738, there is a Metasploit module for this exploit.
- Found known exploit for `wp-file-manager 6.0`
- CVE number is CVE-2020-25213, there is a Metasploit module for this exploit.

# Exploit
- We can use the module to gain a shell into the web server.
`exploit(multi/http/wp_file_manager_rce)`
## Persistence
- I uploaded a [webshell.php](https://gist.github.com/joswr1ght/22f40787de19d80d110b37fb79ac3985#file-easy-simple-php-webshell-php) to run commands to gain persistence.
- Now we can get a reverse shell easily
```bash
[14:13:07] Welcome to pwncat 🐈!                                                                                                                                                                                               __main__.py:153
[14:15:16] received connection from 10.10.141.238:52750                                                                                                                                                                             bind.py:76
[14:15:19] 0.0.0.0:9001: upgrading from /bin/dash to /bin/bash                                                                                                                                                                  manager.py:504
[14:15:20] 10.10.141.238:52750: registered new host w/ db
```

# Privilege Escalation
- We are now user cth
- Found credentials in PHP config files.
`wordpress:@n0therp@ssw0rd`
- We get information that `Management now requires SSH sessions to be logged.`
- Listing out /var/logs, we can't read auth.log, but there is an unusual .log in the directory.
```bash
-rw-r--r--   1 cth       cth                1874 Mar 23 21:07 bash.log
```
- It contains our logged session! And we see the user changing the password.
```bash
cth@badbyte:~$ suod su

Command 'suod' not found, did you mean:

  command 'sudo' from deb sudo
  command 'sudo' from deb sudo-ldap

Try: sudo apt install <deb name>

cth@badbyte:~$ G00dP@$sw0rd2020
G00dP@: command not found
cth@badbyte:~$ passwd
Changing password for cth.
(current) UNIX password: 
Enter new UNIX password: 
Retype new UNIX password: 
passwd: password updated successfully
```
- Okay so maybe he changed from 2020 to 2021?
`G00dP@$sw0rd2021`
- And that worked and we're root!


