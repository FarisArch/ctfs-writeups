# RECON
## NMAP
```txt
PORT   STATE SERVICE VERSION
22/tcp open  ssh     OpenSSH 8.2p1 Ubuntu 4ubuntu0.4 (Ubuntu Linux; protocol 2.0)
80/tcp open  http    Apache httpd 2.4.48 ((Ubuntu))
Service Info: OS: Linux; CPE: cpe:/o:linux:linux_kernel
```

# Enumeration
## Web
- No robots.txt
- There's a login page, and trying basic SQL injection doesn't work and we get an alert `Incorrect password`, maybe the authentication is being done in the client side?
- If we view the source, we can see Javascript doing the authentication.
```html
  <script>
    function authenticate() {
      a = document.getElementById('uname')
      b = document.getElementById('pass')
      const RevereString = str => [...str].reverse().join('');
      if (a.value=="h3ck3rBoi" & b.value==RevereString("54321@terceSrepuS")) { 
        var xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function() {
          if (this.readyState == 4 && this.status == 200) {
            document.getElementById("flag").innerHTML = this.responseText ;
            document.getElementById("todel").innerHTML = "";
            document.getElementById("rm").remove() ;
          }
        };
        xhttp.open("GET", "RandomLo0o0o0o0o0o0o0o0o0o0gpath12345_Flag_"+a.value+"_"+b.value+".txt", true);
        xhttp.send();
      }
      else {
        alert("Incorrect Password, try again.. you got this hacker !")
      }
    }
  </script>
```
- It is reversing the value of 54321@terceSrepuS as the password.
- Reversing it gives us `SuperSecret@12345`
- We now have valid credentials
`h3ckrBoi:SuperSecret@12345`
- We can login as that user and retrieve the flag.
`flag{edb0be532c540b1a150c3a7e85d2466e}`
