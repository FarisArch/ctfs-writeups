It’s a cold day, and the snow is falling horizontally. It pierces your sight. You better use those extra pairs of socks that you were strangely given by the driver. Someone is waving on the other side of the street. You walk over to her. "Hi AGENT, I’m AGENT X, we’ve found the apartment of a person that we suspect got something to do with the mission. Come along!."  
  
Challenge: Logic Lock (misc)  
It turned out suspect's appartment has an electronic lock. After analyzing the PCB and looking up the chips you come to the conclusion that it's just a set of logic gates

# Challenge
The challenge gives us a logic-lock.png and a picture of logic gates.
We want output = 1
Challenge is better done with a paint tool, I'll be using GIMP
[[logic-lock.png]]


