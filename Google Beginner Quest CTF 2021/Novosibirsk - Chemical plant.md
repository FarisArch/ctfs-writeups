You have now investigated the chemical plant. Nothing seemed to be out of the ordinary, even though the workers acted somewhat passive, but that’s not a good enough to track. It seems like you have a new voice mail from the boss: "Hello there, AGENT! It seems like the corporation that owns the plant was informed by an anonymous source that you would arrive, and therefore they were prepared for your visit, but your colleague AGENT X has a lead in Moscow, we’ve already booked you a flight. FIRST CLASS of course. In fact if you look out of the window, you should be able to see a black car arriving now, and it will carry you to the airport. Good luck!"

# Challenge
https://cctv-web.2021.ctfcompetition.com/

So going to the page there is a login page, we can try some basic passwords but none of them work.
Checking out the source code there is something checking if our password is correct or not.
```js
const checkPassword = () => {
  const v = document.getElementById("password").value;
  const p = Array.from(v).map(a => 0xCafe + a.charCodeAt(0));
  if(p[0] === 52037 &&
     p[6] === 52081 &&
     p[5] === 52063 &&
     p[1] === 52077 &&
     p[9] === 52077 &&
     p[10] === 52080 &&
     p[4] === 52046 &&
     p[3] === 52066 &&
     p[8] === 52085 &&
     p[7] === 52081 &&
     p[2] === 52077 &&
     p[11] === 52066) {
    window.location.replace(v + ".html");
  } else {
    alert("Wrong password!");
  }
}
```
After playing around a bit, I found out that 0xCafe is a hexadecimal.
And we're adding that value to `a.charCodeAt(0)` which is unicode values for our input
The decimal value of 0xCafe is 51966. Since we're adding that to each our characters what if we subtract 51966 from the if statements?
```txt
71,111,111,100,80,97,115,115,119,111,114,100
```
We get this if we subtract from all the p values.
If we put it in cyber chef and change it from decimal, we'll get a string
`GoodPassword`
And we can login and grab our flag!
`CTF{IJustHopeThisIsNotOnShodan}`