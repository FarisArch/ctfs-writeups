You look questionable... if you don't have anything good to say, don't say anything at all!

# Challenge
We can connect to the server and we get a shell
But the catch here is that we can't say any commands!
```bash
user@host:/home/user$ bash
bash
You said a bad word, "bash"!!
```
I tried multiple bypasses by referring to [HackTricks](https://book.hacktricks.xyz/linux-unix/useful-linux-commands/bypass-bash-restrictions#bypass-paths-and-forbidden-words)

And luckily I found one that worked!
```bash
user@host:/home/user$ \u\n\a\m\e \-\a
\u\n\a\m\e \-\a
Linux host 5.4.120+ #1 SMP Tue Jun 22 14:53:20 PDT 2021 x86_64 x86_64 x86_64 GNU/Linux
```
Okay so now we just need to find the flag.
```bash
user@host:/home/user/just/out/of/reach$ \c\a\t flag.txt
\c\a\t flag.txt
flag{2d43e30a358d3f30fe65cc47a9cbbe98}
```
