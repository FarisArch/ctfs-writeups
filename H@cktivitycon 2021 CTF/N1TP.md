Nina found some new encryption scheme that she apparently thinks is really cool. She's annoying but she found a flag or something, can you deal with her?

# Challenge
```txt
NINA: Hello! I found a flag, look!
      560b032eb6481826df19237ce403d7c34c4db194fff59a4f559a4d09b6fa72157a642797e31a
NINA: But I encrypted it with a very special nonce, the same length as 
      the flag! I heard people say this encryption method is unbreakable!
      I'll even let you encrypt something to prove it!! What should we encrypt?
NINA: Ta-daaa!! I think this is called a 'one' 'time' 'pad' or something?
      51060328ac104b70881b267fb254d7914948e697aff2ce1c07c91c51b4ff2a172b6477c7e006
```
Okay so it's hinting towards OTP thats quite unbreak-able but if she's using the same key it's pretty easy to break it.
After looking around, I found this repository which is was made specially for this type of challenges
- https://github.com/SpiderLabs/cribdrag

- We have 2 tools we can use, `xorstrings.py` and `cribdrag.py`
- So our plan is, assuming Nina is using the same key to encrypt, we can XOR both of them and start cracking it 1 by 1.
- So what I did was create a long text that will probably exceed the length of the flag and XOR it with the encrypted flag

```bash
──(faris㉿kali)-[~/ctf]
└─$ python xorstrings.py 58020e25a21c537f88172277a053d782415ae69fa3f2c11f0fc6105fbdfa2d17237f77cae009540e0e20a61458648e183e 560b032eb6481826df19237ce403d7c34c4db194fff59a4f559a4d09b6fa72157a642797e31a                                130 ⨯
0e090d0b14544b59570e010b445000410d17570b5c075b505a5c5d560b005f02591b505d0313
```

- Next we can use `cribdrag.py` to crack it 
```bash
$python cribdrag.py 0e090d0b14544b59570e010b445000410d17570b5c075b505a5c5d560b005f02591b505d0313
```

After some tries, I managed to get this!
```py
Your message is currently:
0	hellomynameisfarisaimanbinmohdfaizalan
Your key is currently:
0	flag{9276cdb76a3dd6b1f523209cd9c0a11b}
```


