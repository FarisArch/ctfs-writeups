# NMAP SCAN
```nmap
PORT      STATE    SERVICE          VERSION
22/tcp    open     ssh              OpenSSH 7.6p1 Ubuntu 4ubuntu0.3 (Ubuntu Linux; protocol 2.0)
| ssh-hostkey: 
|   2048 e2:3f:6c:4e:6d:8b:dc:59:b7:cb:66:64:27:f9:22:86 (RSA)
|   256 ee:be:37:f3:75:4e:38:2a:a9:99:e0:18:1a:b8:d1:41 (ECDSA)
|_  256 7f:72:a7:29:be:30:9e:5e:aa:b9:fc:be:09:d2:8b:3a (ED25519)
80/tcp    open     http             Apache httpd 2.4.29 ((Ubuntu))
|_http-generator: WordPress 5.4.1
|_http-server-header: Apache/2.4.29 (Ubuntu)
|_http-title: Dunder Mifflin &#8211; Just another WordPress site
443/tcp   open     ssl/http         Apache httpd 2.4.29 ((Ubuntu))
|_http-server-header: Apache/2.4.29 (Ubuntu)
|_http-title: Apache2 Ubuntu Default Page: It works
| ssl-cert: Subject: commonName=office.csl/organizationName=Dunder Mifflin/stateOrProvinceName=PA/countryName=US
| Not valid before: 2020-05-08T20:01:51
|_Not valid after:  2021-05-08T20:01:51
|_ssl-date: TLS randomness does not represent time
| tls-alpn: 
|_  http/1.1
10000/tcp filtered snet-sensor-mgmt
Service Info: OS: Linux; CPE: cpe:/o:linux:linux_kernel
```

# NESSUS REPORT

# Web (Port 80)
## Findings
1. Wordpress 5.4.1
2. MYSQL
3. PHP
4. Apache 2.4.29
5. Post says there is another subdomain

### WPSCAN
- User identified
`dwight`

# Web (Port 443)
## Findings
1. /forum leads us to a chat log of the workers.
2. There is an endpoint of chatlogs.php that fetches files from the server.
`https://office.csl/forum/chatlogs/chatlogs.php?file=chatlog.txt`
3. Possible File Inclusion
### LFI
- Endpoint chatlog.php is vulnerable to LFI, able to grab /etc/passwd
- We do not have RFI
- Unable to read wp-config.php
- Fuzzing the LFI using LFI-Jhaddix.txt I was able to find .htpasswd
```bash
../.htpasswd            [Status: 200, Size: 1140, Words: 122, Lines: 29, Duration: 251ms]
```
`dwight:$apr1$7FARE4DE$lKgF/R9rSUEY6s.L79/dM/`
- Hash type is 1600 Apache $apr1$

### Possible Credentials
`dwight:cowboys1`
# Foothold

- Unable to SSH using credentials, but can login to port 80
- Heading to WP File Manager, we can download wp-config.php and upload files
- Upload a simple reverse shell for shell.

# Privilege Escalation
- Harvest credentials from wp-config.php
`wp-user:password`
- DB name is wordpress_db
- Nothing interesting found
- Checking out sudo -l
`User www-data may run the following commands on office:
    (dwight) NOPASSWD: /bin/bash`
- User www-data is not suppose to have sudo privileges.
- Checking out linpeas.sh
- There is port 10000 which is filtered on our side but open on localhost
- Google says webmin uses port 10000
- webmin.setup.out reveals the version
`Welcome to the Webmin setup script, version 1.890`
- We can forward the port to our box using
`ssh victimport:localhost:attackport`
- I'm using ctf-ssh from PinkDraconian to port forward
- Now we can access port 10000 locally.
- Login not successful using credentials
- Webmin 1.890 is exploitable with Metasploit using module `linux/http/webmin_backdoor`

- We're now root.



