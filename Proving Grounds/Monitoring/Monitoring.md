# Enumeration
## NMAP
```txt
PORT     STATE SERVICE    REASON  VERSION
22/tcp   open  ssh        syn-ack OpenSSH 7.2p2 Ubuntu 4ubuntu2.10 (Ubuntu Linux; protocol 2.0)
| ssh-hostkey: 
|   2048 b8:8c:40:f6:5f:2a:8b:f7:92:a8:81:4b:bb:59:6d:02 (RSA)
| ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQDMqjHxSe8UVPDyihFSjxzMKsqU1gOWFrI7Er+/4I+RstLTBrLn1gIldFGff88zYFOy5EWc37eZR/or/4qU6zMdRItYfbdAkyoBbun3MOM9jucnXobM4qQ2TgFjWK4hLk5Gcee2vFN2msegVoNf4aXvlSolQunD6h5kxhoaZ5vn5ok8RTOHH8PDkdYTKHX5a8SxR1/KQn+9d1l1aJZo05VA7qfs1P6GHMoRgKooKgVrws9ttLS8lb6yoZS8EO2mGhze84/G3KSRXID0YevcSmai0Snx3iAI4DdaFZoMhQDxwsui8L8uJpLYK4MLN2UwkuPWVsogX/PEowweR8QnCNHn
|   256 e7:bb:11:c1:2e:cd:39:91:68:4e:aa:01:f6:de:e6:19 (ECDSA)
| ecdsa-sha2-nistp256 AAAAE2VjZHNhLXNoYTItbmlzdHAyNTYAAAAIbmlzdHAyNTYAAABBBDxJyi14JgYiOtkyw9tQR9j86Loo9eSElOnBTrO7YeJleiYWENLJxM/T0vYil9yPzWRz/QT/FC2sqOviJiiaBNo=
|   256 0f:8e:28:a7:b7:1d:60:bf:a6:2b:dd:a3:6d:d1:4e:a4 (ED25519)
|_ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIKohQjgFvYRY5+ccAe3zwQ3CjcMFDzoyT3zdAP+lWxc3
25/tcp   open  smtp       syn-ack Postfix smtpd
|_smtp-commands: ubuntu, PIPELINING, SIZE 10240000, VRFY, ETRN, STARTTLS, ENHANCEDSTATUSCODES, 8BITMIME, DSN, 
| ssl-cert: Subject: commonName=ubuntu
| Issuer: commonName=ubuntu
| Public Key type: rsa
| Public Key bits: 2048
| Signature Algorithm: sha256WithRSAEncryption
| Not valid before: 2020-09-08T17:59:00
| Not valid after:  2030-09-06T17:59:00
| MD5:   e067 1ea3 92c2 ec73 cb21 de0e 73df cb66
| SHA-1: e39c c9b6 c35b b608 3dd0 cd25 e60f cb61 6551 da77
| -----BEGIN CERTIFICATE-----
| MIICsjCCAZqgAwIBAgIJAMvrYyFKXQezMA0GCSqGSIb3DQEBCwUAMBExDzANBgNV
| BAMMBnVidW50dTAeFw0yMDA5MDgxNzU5MDBaFw0zMDA5MDYxNzU5MDBaMBExDzAN
| BgNVBAMMBnVidW50dTCCASIwDQYJKoZIhvcNAQEBBQADggEPADCCAQoCggEBAMfU
| MtszkAvFxmsng/POeWCCF0bcBPmNp6ypRqh1ywyVB6qPlacE8tPM9cDK9t1XPqFz
| +kp7ZHaOlZbk9mvq9ihmvvmlutiM9MhojRMak9oqF5LX9gjhogPRrmKI6FtlrqDn
| 33DsOwNJCxXr2CqwBJeqmIsG5tJDeGoJjXbk9ga68Pwu450fWFH92FL0PTBoXJiV
| 9sjR8wjGyVDn1pTSMQYOIYRe7DrNVsITfLYHL99az2RcjpScOl4KcxV5KVrhsdJk
| wNY4F8g64YkUF/cKCQ4Lbk2KoKkzlq7Z84BFhjujzIwJzulxvaUI+JQELigDKaik
| eyb/iFo12IMCpIhCkV8CAwEAAaMNMAswCQYDVR0TBAIwADANBgkqhkiG9w0BAQsF
| AAOCAQEAVoDANDw/Aqp3SbfYfeRGNkXEZUPSYu3CzvjWG5StwsSOOxjoilae3wiT
| u5Wb3KH61G687ozMsA8kk5BUefGMl77Q74idC++zxwRXPyeCmJ9bEPlusgB2cAKT
| 216skYYuJ0T6xEfeRpY2bQCJMTagb6xzXQmOPC3VZGWX7oxDOTobws9A+eVC/6GK
| hReCKoTkBQU85fFrLxDV7MrQfxs2q+e5f+pXtKW+m4V/3fcrnP16uk6DB9yYO9Im
| mFsOPEhf+/rVjesBWL+5dzscZWcRC6z9OLNkhCYGkya5xrQ7ajCmXdG+G5ZQrOUg
| GO/4fjpxGPhhvZISI71SLM8q2cEcGQ==
|_-----END CERTIFICATE-----
|_ssl-date: TLS randomness does not represent time
80/tcp   open  http       syn-ack Apache httpd 2.4.18 ((Ubuntu))
|_http-favicon: Unknown favicon MD5: 8E1494DD4BFF0FC523A2E2A15ED59D84
| http-methods: 
|_  Supported Methods: GET HEAD POST OPTIONS
|_http-server-header: Apache/2.4.18 (Ubuntu)
|_http-title: Nagios XI
389/tcp  open  ldap       syn-ack OpenLDAP 2.2.X - 2.3.X
443/tcp  open  ssl/http   syn-ack Apache httpd 2.4.18 ((Ubuntu))
| http-methods: 
|_  Supported Methods: GET HEAD POST OPTIONS
|_http-server-header: Apache/2.4.18 (Ubuntu)
|_http-title: Nagios XI
| ssl-cert: Subject: commonName=192.168.1.6/organizationName=Nagios Enterprises/stateOrProvinceName=Minnesota/countryName=US/localityName=St. Paul/organizationalUnitName=Development
| Issuer: commonName=192.168.1.6/organizationName=Nagios Enterprises/stateOrProvinceName=Minnesota/countryName=US/localityName=St. Paul/organizationalUnitName=Development
| Public Key type: rsa
| Public Key bits: 2048
| Signature Algorithm: sha1WithRSAEncryption
| Not valid before: 2020-09-08T18:28:08
| Not valid after:  2030-09-06T18:28:08
| MD5:   20f0 951f 8eff 1b69 ef3f 1b1e fb4c 361f
| SHA-1: cc40 0ad7 60cf 4959 1c92 d9ab 0f06 106c 18f6 6661
| -----BEGIN CERTIFICATE-----
| MIIDxTCCAq2gAwIBAgIBADANBgkqhkiG9w0BAQUFADB9MQswCQYDVQQGEwJVUzES
| MBAGA1UECAwJTWlubmVzb3RhMREwDwYDVQQHDAhTdC4gUGF1bDEbMBkGA1UECgwS
| TmFnaW9zIEVudGVycHJpc2VzMRQwEgYDVQQLDAtEZXZlbG9wbWVudDEUMBIGA1UE
| AwwLMTkyLjE2OC4xLjYwHhcNMjAwOTA4MTgyODA4WhcNMzAwOTA2MTgyODA4WjB9
| MQswCQYDVQQGEwJVUzESMBAGA1UECAwJTWlubmVzb3RhMREwDwYDVQQHDAhTdC4g
| UGF1bDEbMBkGA1UECgwSTmFnaW9zIEVudGVycHJpc2VzMRQwEgYDVQQLDAtEZXZl
| bG9wbWVudDEUMBIGA1UEAwwLMTkyLjE2OC4xLjYwggEiMA0GCSqGSIb3DQEBAQUA
| A4IBDwAwggEKAoIBAQCe4uFtqzOvsxrF7Krjw2Pz0x+2cX/9Kfw2jMhIbR0rb5Bl
| BiYb8ifgtbB05ZL2EqfE8e/I5EwVp/dtHUds4bJSv2FfEE4xzXU0SRw0LK4FQ6u1
| ZBB2HqTGhxCN0/rmLhf0/IriWAS6l3NOR58pJW/syaqKL4OSOvG248MndIKzwNBH
| 8vVGSgEKRD0qFxbqS3pCQTsejbCimqBSqAsBJMwBcOpQnfBip8EjcTWqD8mpfmMS
| 4tHhn8k2/7UMGWbSl1erpiZKL/1SQ/V2Z2mJBF+85x4J+Rz2ealAbVt1W+G1Cy6D
| vvsK9L+RLokdPHgrzSuZGNKrJxg3nkHKwRFkZbExAgMBAAGjUDBOMB0GA1UdDgQW
| BBRVunDEJGH/2XNnyJVQYllWcHHjFjAfBgNVHSMEGDAWgBRVunDEJGH/2XNnyJVQ
| YllWcHHjFjAMBgNVHRMEBTADAQH/MA0GCSqGSIb3DQEBBQUAA4IBAQArlT8PTnzT
| dz6wmMzY9/vnBMkRnvH7vuB1MfRlnTyDy4QcTpzDBgdjkvy6MYMxsQz3TTJ+OrOn
| zPdp1NzEFGDDJQUhE22F1kzpJX8XedlHV5YRhdDKokwh2kKcyEsW6obOlC9przI5
| MpJvndKTj69peQAxrWImjD2o70WMKcoOIlbNnbPmmsKiR6jtL6G0+3ic7jPgZRRb
| WmPLzYh7GWMik7R0DWkng2x2Hq1YKNWmiGtMv3fC/w5PRpNT+/VV0NfOOJu36VB/
| rilrUGlO5q0HSx2lf1QoxYDnkQZ8/nzfAzjCYj5M4WuGKzGmkB9GPDC/REfHdu8m
| sMSOoeFVpu/b
|_-----END CERTIFICATE-----
|_ssl-date: TLS randomness does not represent time
| tls-alpn: 
|_  http/1.1
5667/tcp open  tcpwrapped syn-ack

```

## Web
-Apache 2.4.18
- Nagios XI?
- If we access it, it redirects us to a login page.
- Searching for `Nagios XI credentials we get` :
`nagiosadmin:PASSW0RD`
- But when we try to login on port 80
```txt
NSP: Sorry Dave, I can't let you do that
```
- If we try to login on port 443, it doesn't say the error message. Trying other credentials and we get this for a successful login:
`nagiosadmin:admin`
- Version is Nagios 5.6.0

## SMTP
- We can connect with `telnet`
- Authentication not enabled.
- Come back for later.

## LDAP
```txt
PORT    STATE SERVICE VERSION
389/tcp open  ldap    OpenLDAP 2.2.X - 2.3.X
| ldap-rootdse: 
| LDAP Results
|   <ROOT>
|       namingContexts: dc=nodomain
|       supportedControl: 2.16.840.1.113730.3.4.18
|       supportedControl: 2.16.840.1.113730.3.4.2
|       supportedControl: 1.3.6.1.4.1.4203.1.10.1
|       supportedControl: 1.3.6.1.1.22
|       supportedControl: 1.2.840.113556.1.4.319
|       supportedControl: 1.2.826.0.1.3344810.2.3
|       supportedControl: 1.3.6.1.1.13.2
|       supportedControl: 1.3.6.1.1.13.1
|       supportedControl: 1.3.6.1.1.12
|       supportedExtension: 1.3.6.1.4.1.4203.1.11.1
|       supportedExtension: 1.3.6.1.4.1.4203.1.11.3
|       supportedExtension: 1.3.6.1.1.8
|       supportedLDAPVersion: 3
|       supportedSASLMechanisms: DIGEST-MD5
|       supportedSASLMechanisms: NTLM
|       supportedSASLMechanisms: CRAM-MD5
|_      subschemaSubentry: cn=Subschema
| ldap-search: 
|   Context: dc=nodomain
|     dn: dc=nodomain
|         objectClass: top
|         objectClass: dcObject
|         objectClass: organization
|         o: nodomain
|         dc: nodomain
|     dn: cn=admin,dc=nodomain
|         objectClass: simpleSecurityObject
|         objectClass: organizationalRole
|         cn: admin
|_        description: LDAP administrator
```

# Exploit
- Attempting to exploit CVE-2018-15708 and CVE-2018-15710 failed
- Using this exploit from [exploitDB](https://www.exploit-db.com/exploits/46221)
- `magpie_debug.php` is not found both on port 80 and 443
- Version 5.6.0 is exploitable.
- We find a module for it on Metasploit.
```txt
1   exploit/linux/http/nagios_xi_mibs_authenticated_rce                  2020-10-20       excellent  Yes    Nagios XI 5.6.0-5.7.3 - Mibs.php Authenticated Remote Code Exection
```
- But no session created.
- Trying another exploit for it on Metasploit
```txt
exploit/linux/http/nagios_xi_plugins_check_plugin_authenticated_rce  2019-07-29       excellent  Yes    Nagios XI Prior to 5.6.6 getprofile.sh Authenticated Remote Command Execution
```
- And we get a shell!

# Privilege Escalation.
- The software was misconfigured and we automatically have a root shell.
```bash
meterpreter > getuid
Server username: root

meterpreter > cat proof.txt
1d563597f756b54b1d5b2ada43a9b807
```


