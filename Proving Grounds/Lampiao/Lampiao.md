# Enumeration
## NMAP
```txt
PORT     STATE SERVICE REASON  VERSION
22/tcp   open  ssh     syn-ack OpenSSH 6.6.1p1 Ubuntu 2ubuntu2.13 (Ubuntu Linux; protocol 2.0)
| ssh-hostkey: 
|   1024 46:b1:99:60:7d:81:69:3c:ae:1f:c7:ff:c3:66:e3:10 (DSA)
| ssh-dss AAAAB3NzaC1kc3MAAACBAKeg3YDejlMII2nywaeS2HFxd09ak99X7NdFEfHDe/Fng3UwA+gQjhQZ03h09BWb45SfR2EIHLWQ4cN8NN+8bajVwsLwItjKNis+mVMI4Jd8HFMV064cuzcB+xbikI8jzV1GIN4Gclifo+luxym7exJvHgKcLpL1rNVZjzYxPhofAAAAFQCKP3vJ9wD7JSGsDao7IA97RPWROwAAAIAOFHw5FJFFG3bpKsmzhluq0dj1VdltQ51Wd3lqWFtoSncq14ZWMunQhHkKt+KLuPIccv1XmqJrbP9HEWe2E8hl4oT3R7vzbEB/nvVILX3y68TR2/o0Iu5JMgy4uyXMVFFbdpZ3cOv4+fDbn7Yy9shhE+T144Utr0WvHHGvcged4QAAAIEAmqW1JA1Dj7CjHW64mRG+7uDNvb8InZplGWMVd0JINWgr1is4gRDnwldXukIDSA71cTkS3Al6mMCu0nftLqxZKodcIeuGuKBWIHSTKN3/pzVrFjOiOfUQK7lH3pHzR6DxpOLOVLMsP4qOGa6CBG9R4UREUSFZ+j6mVSPgo+tU9do=
|   2048 f3:e8:88:f2:2d:d0:b2:54:0b:9c:ad:61:33:59:55:93 (RSA)
| ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQCohkf0Lql5Q9/2RQx+I7+nJJ9hZfi+p0nYiwkia9NTSQlbQZ09JUGvfxRE3pYke/zu9TNCIcKCSdVuIg7VCIjvyyXlxIfhGm1KDIxa4yVSYY6nlp0PlNe/eMJu0eHmCul/RZR+QMml4Ov/DD7tBNARreXZtxgGG1cUp/51ad31VxOW0xZ8mteMAqyBYRmGPcE5EMFhB7iis8TGr5ZNvEq246RRG9yzDECYdOcGu0CaWdBn1CO9VKsr393RSEAY7dYDqDXssvA9Dw81Oqkek59OmLXBS0WFgnjxpfbmdfvbDsm9WQ2jTMgq6NTp6yYYlYoxxc4kkwJDgO0lD75gN6+Z
|   256 ce:63:2a:f7:53:6e:46:e2:ae:81:e3:ff:b7:16:f4:52 (ECDSA)
| ecdsa-sha2-nistp256 AAAAE2VjZHNhLXNoYTItbmlzdHAyNTYAAAAIbmlzdHAyNTYAAABBBJgCFIaCKti2RYMo5AGFAE91s78Z0eBZp4I+MlPV2Sw9oTZaTTbGBeLLKpsHHAs0mw1rUm36GxzU4F1oU57nBcE=
|   256 c6:55:ca:07:37:65:e3:06:c1:d6:5b:77:dc:23:df:cc (ED25519)
|_ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIAq63V1lqtuey7Q5i7rr9auAAqKBs27r5xq5k27l3XSb
80/tcp   open  http?   syn-ack
| fingerprint-strings: 
|   NULL: 
|     _____ _ _ 
|     |_|/ ___ ___ __ _ ___ _ _ 
|     \x20| __/ (_| __ \x20|_| |_ 
|     ___/ __| |___/ ___|__,_|___/__, ( ) 
|     |___/ 
|     ______ _ _ _ 
|     ___(_) | | | |
|     \x20/ _` | / _ / _` | | | |/ _` | |
|_    __,_|__,_|_| |_|
1898/tcp open  http    syn-ack Apache httpd 2.4.7 ((Ubuntu))
|_http-favicon: Unknown favicon MD5: CF2445DCB53A031C02F9B57E2199BC03
|_http-generator: Drupal 7 (http://drupal.org)
| http-methods: 
|_  Supported Methods: GET HEAD POST OPTIONS
| http-robots.txt: 36 disallowed entries 
| /includes/ /misc/ /modules/ /profiles/ /scripts/ 
| /themes/ /CHANGELOG.txt /cron.php /INSTALL.mysql.txt 
| /INSTALL.pgsql.txt /INSTALL.sqlite.txt /install.php /INSTALL.txt 
| /LICENSE.txt /MAINTAINERS.txt /update.php /UPGRADE.txt /xmlrpc.php 
| /admin/ /comment/reply/ /filter/tips/ /node/add/ /search/ 
| /user/register/ /user/password/ /user/login/ /user/logout/ /?q=admin/ 
| /?q=comment/reply/ /?q=filter/tips/ /?q=node/add/ /?q=search/ 
|_/?q=user/password/ /?q=user/register/ /?q=user/login/ /?q=user/logout/
|_http-server-header: Apache/2.4.7 (Ubuntu)
|_http-title: Lampi\xC3\xA3o
1 service unrecognized despite returning data. If you know the service/version, please submit the following fingerprint at https://nmap.org/cgi-bin/submit.cgi?new-service :
SF-Port80-TCP:V=7.91%I=7%D=9/26%Time=61500A55%P=x86_64-pc-linux-gnu%r(NULL
SF:,1179,"\x20_____\x20_\x20\x20\x20_\x20\x20\x20\x20\x20\x20\x20\x20\x20\
SF:x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20
SF:\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x2
SF:0\x20\x20\x20\x20\x20\x20\x20\x20\x20\n\|_\x20\x20\x20_\|\x20\|\x20\(\x
SF:20\)\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x2
SF:0\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x
SF:20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\
SF:x20\n\x20\x20\|\x20\|\x20\|\x20\|_\|/\x20___\x20\x20\x20\x20___\x20\x20
SF:__\x20_\x20___\x20_\x20\x20\x20_\x20\x20\x20\x20\x20\x20\x20\x20\x20\x2
SF:0\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\n
SF:\x20\x20\|\x20\|\x20\|\x20__\|\x20/\x20__\|\x20\x20/\x20_\x20\\/\x20_`\
SF:x20/\x20__\|\x20\|\x20\|\x20\|\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\
SF:x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\n\x20_\
SF:|\x20\|_\|\x20\|_\x20\x20\\__\x20\\\x20\|\x20\x20__/\x20\(_\|\x20\\__\x
SF:20\\\x20\|_\|\x20\|_\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x2
SF:0\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\n\x20\\___/\x20\\__\|
SF:\x20\|___/\x20\x20\\___\|\\__,_\|___/\\__,\x20\(\x20\)\x20\x20\x20\x20\
SF:x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20
SF:\x20\x20\n\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\
SF:x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20
SF:\x20\x20__/\x20\|/\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\
SF:x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\n\x20\x20\x20\x20\x20\x
SF:20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\
SF:x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\|___/\x20\x20\x20\x20\x20\x
SF:20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\
SF:x20\x20\x20\x20\n______\x20_\x20\x20\x20\x20\x20\x20\x20_\x20\x20\x20\x
SF:20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\
SF:x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20
SF:\x20\x20\x20\x20\x20\x20\x20\x20\x20_\x20\n\|\x20\x20___\(_\)\x20\x20\x
SF:20\x20\x20\|\x20\|\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\
SF:x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20
SF:\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\|\x20\|\n\
SF:|\x20\|_\x20\x20\x20_\x20\x20\x20\x20__\|\x20\|_\x20\x20\x20_\x20_\x20_
SF:_\x20___\x20\x20\x20__\x20_\x20\x20\x20\x20___\x20\x20__\x20_\x20_\x20\
SF:x20\x20_\x20\x20__\x20_\|\x20\|\n\|\x20\x20_\|\x20\|\x20\|\x20\x20/\x20
SF:_`\x20\|\x20\|\x20\|\x20\|\x20'_\x20`\x20_\x20\\\x20/\x20_`\x20\|\x20\x
SF:20/\x20_\x20\\/\x20_`\x20\|\x20\|\x20\|\x20\|/\x20_`\x20\|\x20\|\n\|\x2
SF:0\|\x20\x20\x20\|\x20\|\x20\|\x20\(_\|\x20\|\x20\|_\|\x20\|\x20\|\x20\|
SF:\x20\|\x20\|\x20\|\x20\(_\|\x20\|\x20\|\x20\x20__/\x20\(_\|\x20\|\x20\|
SF:_\|\x20\|\x20\(_\|\x20\|_\|\n\\_\|\x20\x20\x20\|_\|\x20\x20\\__,_\|\\__
SF:,_\|_\|\x20\|_\|");
Service Info: OS: Linux; CPE: cpe:/o:linux:linux_kernel
```

# Web (1898)
- 80 doesn't look too interesting right now.
- Wappalyzer says website is built with drupal 7?
- Robots.txt contains a lot. 
```txt
Disallow: /admin/
Disallow: /comment/reply/
Disallow: /filter/tips/
Disallow: /node/add/
Disallow: /search/
Disallow: /user/register/
Disallow: /user/password/
Disallow: /user/login/

# Files
Disallow: /CHANGELOG.txt
Disallow: /cron.php
Disallow: /INSTALL.mysql.txt
Disallow: /INSTALL.pgsql.txt
Disallow: /INSTALL.sqlite.txt
Disallow: /install.php
Disallow: /INSTALL.txt
Disallow: /LICENSE.txt
Disallow: /MAINTAINERS.txt
Disallow: /update.php
Disallow: /UPGRADE.txt
Disallow: /xmlrpc.php
```
- CHANGELOG.txt confirms that we're running Drupal 7.54

# Exploit
- There are a few modules for Drupal 7 in Metasploit
```bash
1  exploit/unix/webapp/drupal_drupalgeddon2       2018-03-28       excellent  Yes    Drupal Drupalgeddon 2 Forms API Property Injection
```
This one worked the best.
- The target is vulnerable and we have a shell.
- I uploaded a webshell to get a better shell and persistence.
```bash
meterpreter > upload /opt/scripts/webshell.php
[*] uploading  : /opt/scripts/webshell.php -> webshell.php
[*] Uploaded -1.00 B of 300.00 B (-0.33%): /opt/scripts/webshell.php -> webshell.php
[*] uploaded   : /opt/scripts/webshell.php -> webshell.php
```
```bash
[14:05:55] Welcome to pwncat 🐈!                                                                                                                                                                                               __main__.py:153
[14:06:08] received connection from 192.168.152.48:50094                                                                                                                                                                            bind.py:76
[14:06:11] 0.0.0.0:9001: upgrading from /bin/dash to /bin/bash
```
- Now we have a proper shell as www-data using pwncat.

# Privilege Escalation
- www-data needs password for sudo.
- Found database credentials in `/var/www/html/sites/default/settings.php`
```php
'database' => 'drupal',
      'username' => 'drupaluser',
      'password' => 'Virgulino',
```
- Found user and password in users table
```sql
+-------+---------------------------------------------------------+
| name  | pass                                                    |
+-------+---------------------------------------------------------+
|       |                                                         |
| tiago | $S$DNZ5o1k/NY7SUgtJvjPqNl40kHKwn4yXy2eroEnOAlpmT0TJ9Sx8 |
| Eder  | $S$Dv5orvhi7okjmViImnVPmVgfwJ2U..PNK4E9IT/k7Lqz9GZRb7tY |
+-------+---------------------------------------------------------+
```
- We only have user `tiago` in `/home
- Hash type is Drupal 7, mode 7900
- We can try to crack that.
- Try DB password with `tiago` to see if reused.
- It is reused and we are now user tiago.
- `tiago` cannot run sudo.
- Before enumerating further, get a shell with SSH.

## Interesting Findings
```bash
Linux version 4.4.0-31-generic (vulnerable to dirty cow?)	
```
- Had troubles finding the right exploit, some exploits break the box and had to reset.
- This [exploit works](https://www.exploit-db.com/exploits/40847)
- We're now root.
- proof.txt
`801fccdbdea2ef00d7f0d3160dced3f3`
