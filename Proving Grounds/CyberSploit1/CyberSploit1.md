# Enumeration
## NMAP
```nmap
PORT   STATE SERVICE VERSION
22/tcp open  ssh     OpenSSH 5.9p1 Debian 5ubuntu1.10 (Ubuntu Linux; protocol 2.0)
| ssh-hostkey: 
|   1024 01:1b:c8:fe:18:71:28:60:84:6a:9f:30:35:11:66:3d (DSA)
|   2048 d9:53:14:a3:7f:99:51:40:3f:49:ef:ef:7f:8b:35:de (RSA)
|_  256 ef:43:5b:d0:c0:eb:ee:3e:76:61:5c:6d:ce:15:fe:7e (ECDSA)
80/tcp open  http    Apache httpd 2.2.22 ((Ubuntu))
|_http-server-header: Apache/2.2.22 (Ubuntu)
|_http-title: Hello Pentester!
Service Info: OS: Linux; CPE: cpe:/o:linux:linux_kernel
```

## Web
- Apache 2.2.22
- Bootstrap 4.5.0
- robots.txt contains a joke.
- Found username in source code.
`<!-------------username:itsskv--------------------->`

### Feroxbuster 
- Found nothing interesting.

# Exploit
- I tried to bruteforce the SSH login since we have a valid user but no luck.
- I checked back the robots.txt for the content
- It contained the base64 encoded value of this
`cybersploit{youtube.com/c/cybersploit}`
- And who would have guessed that was the password lmao.
- We are now user itsskv.
- Found local.txt
`e22079402a7a74b87e2f9e8a4e96584a`

# Privilege Escalation.
## Internal Enumeration
- Check `sudo -l`, user can't run sudo on the box.
- Check `/etc/crontab`, no cronjobs.\
- Linux version `3.13.0-32-generic`
- Sudo version `1.8.3p1`
- Unusual SUID `-rwsr-xr-x 1 root root 14K Nov  8  2011 /usr/bin/arping (Unknown SUID binary)`
- Kernel version is vulnerable to OverlayFS
- Send the [binary to the machine and execute it](https://raw.githubusercontent.com/lucyoa/kernel-exploits/master/overlayfs/ofs_64.c)
- We are now root!
- To check for kernel exploits I use this [resource](https://github.com/lucyoa/kernel-exploits)
