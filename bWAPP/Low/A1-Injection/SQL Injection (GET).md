![[images/Pasted image 20210708201528.png]]

So it's asking us to search for a movie let's try doing normal user inputs.

And if it doesn't find it, it'll return **"No movies were found"**.
Let's try a single letter, let's say K:
![[Pasted image 20210708202237.png]]

So from what we're seeing is that if there's any instance of 'K' in that movie title, return it.

Let's try adding a single quote to see what happens.

![[Pasted image 20210708201634.png]]

Looks like we broke it and it looks vulnerable to SQL injection. Let's try the simple one first. Let's try : 
```sql
' or 1=1--
```
We get the response of :
```txt
You have an error in your SQL syntax; check the manual that corresponds to your MySQL server version for the right syntax to use near '%'' at line 1
```
Let's try fiddling with it a bit.
```sql
' -- or 1=1--
```
And it returns the whole list of movies. Let's see what's happening.
```sql
SELECT * FROM movies WHERE title LIKE '%" . ' -- or 1=1--. "%'"
```
So what we're essentially doing is that we're commenting out everything after the second single quote. So we're left with this.
```sql
SELECT * FROM movies WHERE title LIKE '%" . '
```
Which will return all the movies.
