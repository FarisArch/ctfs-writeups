So let's see what can we do with iFrame. Looking at the url, it can access robots.txt and access other directories.
But what if we make it access something else?I set up a simple page that which I will use to prove my concept.

Let's change the URL pointing to our site.
```txt
http://localhost:4000/iframei.php?ParamUrl=http://192.168.1.15:8001&ParamWidth=250&ParamHeight=250
```
And when we run it let's see what we got.
![[Pasted image 20210708150620.png]]

And that's my page, if we put something malicious there like a cookie stealer or something that downloads automatically, it'll be very dangerous.