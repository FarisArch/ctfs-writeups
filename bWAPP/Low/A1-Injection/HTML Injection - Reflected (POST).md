This is a bit same like the GET method one but this time we're using a POST request
Let's see what happens if we put our name in
![[Pasted image 20210708143341.png]]
Like usual it's rendering but if we look at the Network tabs. It's posting a request to :
```txt
http://localhost:4000/htmli_post.php
```
Which is this page.
Now let's try the same thing again with script tags.

And we'll get only a welcome response and then greeted by script tags.
This is again because the tags are being rendered in the page and will execute whenever the page is rendered.

