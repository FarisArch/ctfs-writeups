Looks like we can add text here, let's try adding random stuff to simulate user inputs.
![[Pasted image 20210708144926.png]]

Okay so is our text being rendered? Let's try some h1 tags.
```html
<h1>hello</h1>
```

![[Pasted image 20210708145041.png]]

So our h1 tags are being rendered. Uh oh, now let's try something with script tags.
```html
<script>alert(1)</script>
```

![[Pasted image 20210708145150.png]]

And our Entry box is empty for the third one but we get an alert. And if we refresh the page, we still get an alert. This is because the the script is now apart of the page, and whoever comes to this page will get an alert because the page is being rendered including the script tag.

This is very high vulnerability.