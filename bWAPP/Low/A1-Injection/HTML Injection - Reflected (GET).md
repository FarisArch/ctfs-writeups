It asks us to enter a first name and last name and if we just enter it, we'll see that it renders
```html
Welcome far is
```
What if we try some HTML tags?
![[Pasted image 20210708142856.png]]

If we use some h tags. We'll see it being rendered.
What if we put a script tag?
```html
<script>alert(1)</script>
```
```txt
Welcome
```
And then we are prompted with an alert 1.
The script tags are being rendered in the page so whenever we refresh the page it'll run that script tag.

