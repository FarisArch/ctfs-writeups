Navigating to the site it just says:
```txt
This is just a test page, reflecting back your message...
```
If we click the **message** word something happens.
![[Pasted image 20210708194117.png]]
Now there's "test" being rendered on the page our URL now is :
```txt
http://localhost:4000/phpi.php?message=test
```
What if we change the message parameter?
![[Pasted image 20210708194216.png]]

Okay not a big deal, how about html elements?
Nothing happens. How about php functions? Since this challenge is testing on php code injection. Let's try some built-in functions. I'll be using phpinfo().
```txt
http://localhost:4000/phpi.php?message=phpinfo()
```

![[Pasted image 20210708195057.png]]

Looks like the code worked.
