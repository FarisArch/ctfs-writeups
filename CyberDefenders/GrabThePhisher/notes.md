# Details
Scenario:

An attacker compromised a server and impersonated https://pancakeswap.finance/, a decentralized exchange native to BNB Chain, to host a phishing kit at https://apankewk.soup.xyz/mainpage.php. The attacker set it as an open directory with the file name "pankewk.zip". 

Provided the phishing kit, you are requested to analyze it and do your threat intel homework.

# Static Analysis
- As we unzip the file, we are given a folder which is in a structure of a web server.
```sh
-rw-rw-r-- 1 remnux remnux 122910 Jun 29  2022 background1.jpg
-rw-rw-r-- 1 remnux remnux  43879 Jun 29  2022 background2.jpg
-rw-rw-r-- 1 remnux remnux 151073 Jun 29  2022 background.jpg
drwx------ 2 remnux remnux   4096 Jul 19  2022 cgi-bin
-rw-rw-r-- 1 remnux remnux 116129 Jun 29  2022 favicon.ico
drwx------ 4 remnux remnux   4096 Jul 23  2022 images
-rw-rw-r-- 1 remnux remnux 300563 Jul  1  2022 index.html
drwx------ 2 remnux remnux   4096 Jul 23  2022 log
-rw-rw-r-- 1 remnux remnux  22919 Jun 29  2022 logo.png
drwx------ 3 remnux remnux   4096 Jul 23  2022 metamask
drwx------ 3 remnux remnux   4096 Jul 23  2022 _next
drwx------ 7 remnux remnux   4096 Jul 23  2022 src
```

## Opening the pages
- As we are dealing with a phishing kit, it's best to load the index.html to see what we're looking at rather than looking at the codes.

- Opening up `index.html` we're presented with a list of online cryptowallets to connect with
![index.html](web.png)

- Apparently none of the button work except for Metamask, but upon clicking it we get another window open with it trying to open `file:///metamask/`

- Checking back in our source folder the content of `metamask` is :
```sh
total 844
drwx------ 3 remnux remnux   4096 Jul 23  2022 .
drwx------ 9 remnux remnux   4096 Jul 23  2022 ..
-rw-rw-r-- 1 remnux remnux   6148 Jul  5  2022 .DS_Store
drwx------ 3 remnux remnux   4096 Jul 23  2022 fonts
-rw-rw-r-- 1 remnux remnux 839192 Jun 29  2022 index.html
-rw-rw-r-- 1 remnux remnux   1188 Jul  5  2022 metamask.php
```
Interesting

- Looking at the source code of `index.html`, we can check what the button is doing.
```html
<button onclick="vib(1);jj2 = true;jj = true;" class="sc-e72add9e-0 cPSiYt sc-4a6a2b7-0 eIETMV" id="wallet-connect-metamask" width="100%" scale="md">
```
- So we see it's calling and setting `jj2` and `jj` to true.
- Checking for `vib` `jj2` and `jj`, we find this script.
```html
    <script>
        jj = true;
        jj2 = false;
        function vib(d) {
            console.log(d);

            window.open("/metamask/", "...", "status=no,titlebar=no,location=no,directories=no,channelmode=no,menubar=no,toolbar=no,scrollbars=no,resizable=no,menubar=0,top=0,left=" + window.innerWidth + ",width=400,height=650");
        }
    </script>
```
- Okay nothing too interesting I guess, but we can assume that `metamask` will be serving `metamask.php` that we found in the folder.