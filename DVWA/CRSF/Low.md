### Objective

Your task is to make the current user change their own password, without them knowing about their actions, using a CSRF attack.

Since this is low security, let's say we have intercept the request. We can probably change the password right away in URL
