Let's try using OWASP ZAP for this.
I used this as a reference:
https://rajendrakv.wordpress.com/2020/06/14/brute-force-using-burp-suite-and-owasp-zap/

You can then use rockyou.txt as a payload and fuzz through.
Then order by size to see the responses.
```txt
admin:password
```
