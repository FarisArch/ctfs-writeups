### Objective

Your goal is to get the administrator’s password by brute forcing. Bonus points for getting the other four user passwords!

Let's try to our first step.
```txt
username:admin
password:password
```
```txt
Username and/or password incorrect.
```

But when you look at the URL this time its longer, now apparently we have user_token or CRSF token to avoid brute force. What if we try to change it again using the same token.
```txt
CSRF token is incorrect
```
Okay so we can't use the same token.

Let's capture the request using BurpSuite and send it to the intruder.
Next let's set up payloads for password and CRSF token. We have to somehow catch the token.For this select the pitchfork attack, and configure the token payload to be recursive grep. Next head to options and scroll down to Grep - Extract and highlight the token. 
Now you can start the attack and you'll notice we'll be using different tokens every time.
```txt
admin:password;
```
