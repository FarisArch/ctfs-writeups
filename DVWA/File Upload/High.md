### Objective

Execute any PHP function of your choosing on the target system (such as [phpinfo()](https://secure.php.net/manual/en/function.phpinfo.php) or [system()](https://secure.php.net/manual/en/function.system.php)) thanks to this file upload vulnerability.

Let's upload try uploading a php file.
```txt
Your image was not uploaded. We can only accept JPEG or PNG images.
```
Let's check out what is checking it.
```php
if( ( strtolower( $uploaded_ext ) == "jpg" || strtolower( $uploaded_ext ) == "jpeg" || strtolower( $uploaded_ext ) == "png" ) &&
        ( $uploaded_size < 100000 ) &&
        getimagesize( $uploaded_tmp ) ) { 

```
Looks like it checks the extension of the folder.
Let's try double extensions.
