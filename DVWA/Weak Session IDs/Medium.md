### Objective

This module uses four different ways to set the dvwaSession cookie value, the objective of each level is to work out how the ID is generated and then infer the IDs of other system users.

Let's click it first.
```txt
1624709828
```
Now let's click it again.
```txt
1624709874
```
Value went up by a lot?
Why.
Let's intercept the request and sent it to the sequencer.
Now let's run it and see what we got.
```txt
1624709462
1624709463
1624709464
1624709465
```
Weird right. Now what if we sleep it for 2 seconds before each request?
```txt
1624709520
1624709522
1624709524
1624709526
```
There's a time interval between each request? Let's do it manually from the web.
```txt
1624710150
```
Now wait 10 seconds.
```txt
1624710160
```
So I believe the cookie is set after someone clicks. So if user 1 clicks it and user 2 clicks it 5 seconds later, their cookie will be 5 digits apart. and If we view the source code.
```php
`<?php  
  
$html = "";  
  
if ($_SERVER['REQUEST_METHOD'] == "POST")
{ $cookie_value = time(); 
  setcookie("dvwaSession", $cookie_value);  
}  
?>`
```
It is using the time function as the cookies value

