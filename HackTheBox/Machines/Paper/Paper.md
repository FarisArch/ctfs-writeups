# Recon
## NMAP
```txt
PORT    STATE SERVICE  VERSION
22/tcp  open  ssh      OpenSSH 8.0 (protocol 2.0)
| ssh-hostkey: 
|   2048 10:05:ea:50:56:a6:00:cb:1c:9c:93:df:5f:83:e0:64 (RSA)
|   256 58:8c:82:1c:c6:63:2a:83:87:5c:2f:2b:4f:4d:c3:79 (ECDSA)
|_  256 31:78:af:d1:3b:c4:2e:9d:60:4e:eb:5d:03:ec:a0:22 (ED25519)
80/tcp  open  http     Apache httpd 2.4.37 ((centos) OpenSSL/1.1.1k mod_fcgid/2.3.9)
|_http-title: HTTP Server Test Page powered by CentOS
| http-methods: 
|_  Potentially risky methods: TRACE
|_http-generator: HTML Tidy for HTML5 for Linux version 5.7.28
|_http-server-header: Apache/2.4.37 (centos) OpenSSL/1.1.1k mod_fcgid/2.3.9
443/tcp open  ssl/http Apache httpd 2.4.37 ((centos) OpenSSL/1.1.1k mod_fcgid/2.3.9)
|_ssl-date: TLS randomness does not represent time
|_http-generator: HTML Tidy for HTML5 for Linux version 5.7.28
|_http-server-header: Apache/2.4.37 (centos) OpenSSL/1.1.1k mod_fcgid/2.3.9
|_http-title: HTTP Server Test Page powered by CentOS
| http-methods: 
|_  Potentially risky methods: TRACE
| ssl-cert: Subject: commonName=localhost.localdomain/organizationName=Unspecified/countryName=US
| Subject Alternative Name: DNS:localhost.localdomain
| Not valid before: 2021-07-03T08:52:34
|_Not valid after:  2022-07-08T10:32:34
| tls-alpn: 
|_  http/1.1

```

### Web
- Nothing interesting found from SSL certificate.
- An interesting finding that we can observe is that it returns different error code for `index.html` and `index.php`
`index.html`
```txt
# Not Found

The requested URL was not found on this server.
```
`index.php`
```txt
File not found.
```
- Checking the response on Burp, there's a header in the response that isn't usually there.
```http
HTTP/1.1 404 Not Found
Date: Thu, 10 Feb 2022 05:26:25 GMT
Server: Apache/2.4.37 (centos) OpenSSL/1.1.1k mod_fcgid/2.3.9
X-Powered-By: PHP/7.2.24
X-Backend-Server: office.paper
Connection: close
Content-Type: text/html; charset=UTF-8
Content-Length: 16

File not found.
```
- Looking at for the header I stumbled this on [OWASP ](https://www.zaproxy.org/docs/alerts/10039/)
```txt
The server is leaking information pertaining to backend systems (such as hostnames or IP addresses). Armed with this information an attacker may be able to attack other systems or more directly/efficiently attack those systems.
```
- So what we found is basically the hostname that is used in the backend. We can add this to our `/etc/hosts`
- Now when we navigate to `officer.paper`, it should resolved to something interesting.

### Web (Vhost office.paper)
- We found a virtual host, and it's seems to be hosting a paper company called Blunder Tiffin Inc.
- By the looks of the page it is using Wordpress as it's CMS.

#### Wordpress
- We can use WPSCAN to enumerate the site.
- Wordpress 5.2.3
- Users found :
```
prisonmike
nick
creedthoughts
```
- WPSCAN didn't find any vulnerable plugins.

### Gobuster VHOST
- Looking for more virtual hosts we stumble upon `chat.office.paper` using a wordlist from seclists.
- We are greeted with login page for rocket.chat
- Tried SQL Injection and default credentials but nothing worked.
- Checking for the version, we found we can find the version by querying to `http://chat.officer.paper/api/info`
`3.16.1`
- No exploits found.
- The login page says that we need to find a secretURL to register. Looking at the documentation of rocket.chat, the string is randomly generated so brute-forcing will not work.

- Looking back at the wordpress there is a suspicious comment.
```txt
Michael, you should remove the secret content from your drafts ASAP, as they are not that secure as you think!  
-Nick
```
- Searching around for reading unauthenticated post I found [this](https://0day.work/proof-of-concept-for-wordpress-5-2-3-viewing-unauthenticated-posts/)
- Using the method, we were able to read some secret content on `http://office.paper/?static=1`
```txt
test

Micheal please remove the secret from drafts for gods sake!

Hello employees of Blunder Tiffin,

Due to the orders from higher officials, every employee who were added to this blog is removed and they are migrated to our new chat system.

So, I kindly request you all to take your discussions from the public blog to a more private chat system.

-Nick

# Warning for Michael

Michael, you have to stop putting secrets in the drafts. It is a huge security issue and you have to stop doing it. -Nick

Threat Level Midnight

A MOTION PICTURE SCREENPLAY,  
WRITTEN AND DIRECTED BY  
MICHAEL SCOTT

[INT:DAY]

Inside the FBI, Agent Michael Scarn sits with his feet up on his desk. His robotic butler Dwigt….

# Secret Registration URL of new Employee chat system

http://chat.office.paper/register/8qozr226AhkCHZdyY

# I am keeping this draft unpublished, as unpublished drafts cannot be accessed by outsiders. I am not that ignorant, Nick.

# Also, stop looking at my drafts. Jeez!
```
- We can now register as a user at the chat.


# Foothold
![[rocketchat.png]]

- The only user we can interact with is user `recyclops` which is a bot that can do a lot of. It can fetch our files. 
`recyclops file user.txt`
- But we get an access denied. We can also try for command injection but it's checking for certain characters.
- We can use `recyclops list` which is equivalent to `ls` to enumerate directories.
- We found the bot's directory in `/home/dwight/hubot`, and scripts looks interesting.
```sh
-   Fetching the directory listing of ../hubot/scripts
    
-   total 48  
    drwx--x--x 2 dwight dwight 193 Jan 13 10:56 .  
    drwx------ 8 dwight dwight 4096 Sep 16 07:57 ..  
    -rwxr-xr-x 1 dwight dwight 490 Jul 3 2021 [cmd.coffee](http://cmd.coffee)  
    -rwxr-xr-x 1 dwight dwight 729 Jul 3 2021 dwight.js  
    -rwxr-xr-x 1 dwight dwight 303 Jul 3 2021 [error.coffee](http://error.coffee)  
    -rwxr-xr-x 1 dwight dwight 544 Jul 3 2021 example.js  
    -rwxr-xr-x 1 dwight dwight 1384 Jan 13 10:56 files.js  
    -rwxr-xr-x 1 dwight dwight 2410 Jul 3 2021 help.js  
    -rwxr-xr-x 1 dwight dwight 1428 Jul 3 2021 listof.js  
    -rwxr-xr-x 1 dwight dwight 555 Jul 3 2021 run.js  
    -rwxr-xr-x 1 dwight dwight 964 Jul 3 2021 smalltalk.js  
    -rwxr-xr-x 1 dwight dwight 900 Jul 3 2021 version.js  
    -rwxr-xr-x 1 dwight dwight 547 Jul 3 2021 why.js
```
- In on the files, we can see that it's checking for certain characters
```js
// Don't judge me. If it is working, then it ain't stupid.  
if((out.includes(";")) || (out.includes("`")) || (out.includes("&")) || (out.includes("$")) || (out.includes("0x0a")) || (out.includes("\\n"))|| (out.includes("*")) || (out.includes("?")) || (out.includes("|")) || (out.includes("[")) || (out.includes("{")) || (out.includes("(")) || (out.includes("\"")) || (out.includes("\\")) || (out.includes(">")) || (out.includes("<")) || (out.includes("'")) || (out.includes("%")) ){  
msg.send("Stop injecting OS commands!");
```
- No luck in bypassing these.
- Another interesting file is `.env` which is also in `hubot`. Perhaps there is hidden stuff there we can find.
```sh
-   <!=====Contents of file ../hubot/.env=====>
    
-   export ROCKETCHAT_URL='[http://127.0.0.1:48320](http://127.0.0.1:48320)'  
    export ROCKETCHAT_USER=recyclops  
    export ROCKETCHAT_PASSWORD=Queenofblad3s!23  
    export ROCKETCHAT_USESSL=false  
    export RESPOND_TO_DM=true  
    export RESPOND_TO_EDITED=true  
    export PORT=8000  
    export BIND_ADDRESS=127.0.0.1
    
-   <!=====End of file ../hubot/.env=====>
```
- We can try login as the bot into the chat system but we get logged off  and an alert saying that bot's don't use the web page to connect.
- We can try to SSH into the server as `rocketchat`
- But we get an error, we can try it with user `dwight` and  success! The password for the bot was reused.

# Post-Exploit
- We user dwight in group `dwight`
- `Linux paper 4.18.0-348.7.1.el8_5.x86_64` kernel
- Run linpeas for enumeration
### Interesting Findings
```sh
Sudo :

Sudo version 1.8.29 

PATH :
home/dwight/.local/bin:/home/dwight/bin:/usr/local/bin:/usr/bin:/usr/local/sbin:/usr/sbin
New path exported: /home/dwight/.local/bin:/home/dwight/bin:/usr/local/bin:/usr/bin:/usr/local/sbin:/usr/sbin:/sbin:/bin


Cron jobs :

/usr/bin/crontab
@reboot /home/dwight/bot_restart.sh >> /home/dwight/hubot/.hubot.log 2>&1

Network Interfaces :

tcp        0      0 127.0.0.1:33060         0.0.0.0:*               LISTEN      -                   
tcp        0      0 127.0.0.1:27017         0.0.0.0:*               LISTEN      -                   
tcp        0      0 127.0.0.1:3306          0.0.0.0:*               LISTEN      -                   
tcp        0      0 192.168.122.1:53        0.0.0.0:*               LISTEN      -                   
tcp        0      0 0.0.0.0:22              0.0.0.0:*               LISTEN      -                   
tcp        0      0 127.0.0.1:48320         0.0.0.0:*               LISTEN      -                   
tcp        0      0 127.0.0.1:8000          0.0.0.0:*               LISTEN      2393/node           
tcp6       0      0 :::80                   :::*                    LISTEN      -                   
tcp6       0      0 :::22                   :::*                    LISTEN      -                   
tcp6       0      0 :::443                  :::*                    LISTEN      - 
```
- We can forward port 8000 to our box and we are able to access it, but after dirbusting nothing came out.
-  I checked the forums a bit and I found out that the latest version of linpeas doesn't detect the vulnerability. So I went to download the release on 5/2/2022
- And luck be hold!
`VULNERABLE TO CVE-2021-3560`

### Privilege Escalation
#### Exploit
- The exploit uses polkit. If you wish to try and reproduce it and learn more about it check [this out](https://www.thesecmaster.com/step-by-step-procedure-to-fix-the-plokit-vulnerability-cve-2021-3560/)
- I'll be using a [script made by the box creator] themselves(https://github.com/secnigma/CVE-2021-3560-Polkit-Privilege-Esclation)
- The script basically runs the needed commands for the exploit to trigger. The exploit is based on timing so I ran it in a while loop.
```sh
$ while true; do ./poc.sh
done
```
- I open up another terminal and SSH into the server while checking if the user has been created by tab completing the user we wish to create in this case `secnigma`
```sh
$ su - secnigma
Password:
```
- The exploit works if you're prompted to enter the password for the created user.