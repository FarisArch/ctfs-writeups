# Enumeration
## NMAP
```txt
PORT   STATE SERVICE VERSION
21/tcp open  ftp     vsftpd 3.0.3
| ftp-anon: Anonymous FTP login allowed (FTP code 230)
| -rw-r--r--    1 ftp      ftp            33 Jun 08 10:58 allowed.userlist
|_-rw-r--r--    1 ftp      ftp            62 Apr 20 11:32 allowed.userlist.passwd
| ftp-syst: 
|   STAT: 
| FTP server status:
|      Connected to ::ffff:10.10.15.171
|      Logged in as ftp
|      TYPE: ASCII
|      No session bandwidth limit
|      Session timeout in seconds is 300
|      Control connection is plain text
|      Data connections will be plain text
|      At session startup, client count was 4
|      vsFTPd 3.0.3 - secure, fast, stable
|_End of status
80/tcp open  http    Apache httpd 2.4.41 ((Ubuntu))
|_http-server-header: Apache/2.4.41 (Ubuntu)
|_http-title: Smash - Bootstrap Business Template
Service Info: OS: Unix

```

## FTP
- Anonymous login is allowed
```bash
-rw-r--r--    1 ftp      ftp            33 Jun 08 10:58 allowed.userlist
-rw-r--r--    1 ftp      ftp            62 Apr 20 11:32 allowed.userlist.passwd
```
- We get list of users and passwords.
```txt
aron
pwnmeow
egotisticalsw
admin
```
```txt
root
Supersecretpassword1
@BaASD&9032123sADS
rKXM59ESxesUFHAd

```

## Web
- Apache 2.4.41
### Feroxbuster
```txt
403        9l       28w      278c http://10.129.197.97/.php
200       39l      115w     1577c http://10.129.197.97/login.php
403        9l       28w      278c http://10.129.197.97/.html
302        0l        0w        0c http://10.129.197.97/logout.php
```
- We have credentials, we can brute-force the usernames.
- Valid credetials are
`admin:rKXM59ESxesUFHAd`
- Flag is
`# c7110277ac44d78b6a9fff2232434d16`
