# Enumeration
## NMAP
```txt
PORT     STATE SERVICE REASON  VERSION
3306/tcp open  mysql?  syn-ack
| mysql-info: 
|   Protocol: 10
|   Version: 5.5.5-10.3.27-MariaDB-0+deb10u1
|   Thread ID: 67
|   Capabilities flags: 63486
|   Some Capabilities: Support41Auth, LongColumnFlag, Speaks41ProtocolOld, SupportsTransactions, InteractiveClient, IgnoreSigpipes, ConnectWithDatabase, ODBCClient, IgnoreSpaceBeforeParenthesis, FoundRows, SupportsCompression, Speaks41ProtocolNew, SupportsLoadDataLocal, DontAllowDatabaseTableColumn, SupportsAuthPlugins, SupportsMultipleResults, SupportsMultipleStatments
|   Status: Autocommit
|   Salt: |K@?[/sy(VY>hs*q!I-F
|_  Auth Plugin Name: mysql_native_password
|_ssl-cert: ERROR: Script execution failed (use -d to debug)
|_ssl-date: ERROR: Script execution failed (use -d to debug)
|_sslv2: ERROR: Script execution failed (use -d to debug)
|_tls-alpn: ERROR: Script execution failed (use -d to debug)
|_tls-nextprotoneg: ERROR: Script execution failed (use -d to debug)

```

## MySQL
- Running MariaDB
- We can connect remotely using root with no password
```bash
mysql -u root -h host
```
- Flag is located in database htb and table config.
```sql
+----+-----------------------+----------------------------------+
| id | name                  | value                            |
+----+-----------------------+----------------------------------+
|  1 | timeout               | 60s                              |
|  2 | security              | default                          |
|  3 | auto_logon            | false                            |
|  4 | max_size              | 2M                               |
|  5 | flag                  | 7b4bec00d1a39e3dd4e021ec3d915da8 |
|  6 | enable_uploads        | false                            |
|  7 | authentication_method | radius                           |
+----+-----------------------+----------------------------------+
```
