# Enumeration
## NMAP
```txt
PORT      STATE SERVICE       REASON  VERSION
135/tcp   open  msrpc         syn-ack Microsoft Windows RPC
139/tcp   open  netbios-ssn   syn-ack Microsoft Windows netbios-ssn
445/tcp   open  microsoft-ds? syn-ack
5985/tcp  open  http          syn-ack Microsoft HTTPAPI httpd 2.0 (SSDP/UPnP)
|_http-server-header: Microsoft-HTTPAPI/2.0
|_http-title: Not Found
47001/tcp open  http          syn-ack Microsoft HTTPAPI httpd 2.0 (SSDP/UPnP)
|_http-server-header: Microsoft-HTTPAPI/2.0
|_http-title: Not Found
49664/tcp open  msrpc         syn-ack Microsoft Windows RPC
49665/tcp open  msrpc         syn-ack Microsoft Windows RPC
49666/tcp open  msrpc         syn-ack Microsoft Windows RPC
49667/tcp open  msrpc         syn-ack Microsoft Windows RPC
49668/tcp open  msrpc         syn-ack Microsoft Windows RPC
49669/tcp open  msrpc         syn-ack Microsoft Windows RPC
Service Info: OS: Windows; CPE: cpe:/o:microsoft:windows

Host script results:
|_clock-skew: 4h09m34s
| p2p-conficker: 
|   Checking for Conficker.C or higher...
|   Check 1 (port 34058/tcp): CLEAN (Couldn't connect)
|   Check 2 (port 7734/tcp): CLEAN (Couldn't connect)
|   Check 3 (port 52424/udp): CLEAN (Timeout)
|   Check 4 (port 24540/udp): CLEAN (Failed to receive data)
|_  0/4 checks are positive: Host is CLEAN or ports are blocked
| smb2-security-mode: 
|   2.02: 
|_    Message signing enabled but not required
| smb2-time: 
|   date: 2021-10-03T06:26:31
|_  start_date: N/A

```

## SMB
**SMB is basically like FTP and is used to share files and folders in a file system. SMB are mostly found in Windows operating systems.**
- We can connect to the SMB share using smbclient and we can list out shares using `-L`
```txt
	Sharename       Type      Comment
	---------       ----      -------
	ADMIN$          Disk      Remote Admin
	C$              Disk      Default share
	IPC$            IPC       Remote IPC
	WorkShares      Disk      
```
- `ADMIN$ C$ IPC$` are common SMB shares found in all WorkShares isn't.
- We find the flag in `James.P/flag.txt`
`5f61c10dffbc77a704d76016a22f1664`
	