# Enumeration
## NMAP
```txt
PORT   STATE SERVICE REASON  VERSION
21/tcp open  ftp     syn-ack vsftpd 3.0.3
| ftp-anon: Anonymous FTP login allowed (FTP code 230)
|_-rw-r--r--    1 0        0              32 Jun 04 03:25 flag.txt
| ftp-syst: 
|   STAT: 
| FTP server status:
|      Connected to ::ffff:10.10.14.244
|      Logged in as ftp
|      TYPE: ASCII
|      No session bandwidth limit
|      Session timeout in seconds is 300
|      Control connection is plain text
|      Data connections will be plain text
|      At session startup, client count was 2
|      vsFTPd 3.0.3 - secure, fast, stable
|_End of status
Service Info: OS: Unix

```

## FTP
**FTP is a file transfer protocol using the client-server model which is usually active on port 21 TCP.**
- FTP version is 3.0.3
- Anonymous login is allowed. We can login as anonymous.
- We find the flag in the directory.
`035db21c881520061c53e0536e44f815 `