# Summary
**We scan the network and found that port 23 is open which is telnet. Due to misconfiguration, we can login as user root which has the highest privilege in the system with no password.**

# Enumeration
## NMAP
```txt

PORT   STATE SERVICE REASON  VERSION
23/tcp open  telnet  syn-ack Linux telnetd
Service Info: OS: Linux; CPE: cpe:/o:linux:linux_kernel
```

- We can login using telnet as user root.

- flag.txt
`b40abdfe23665f766f9c61ecba8a4c19`


