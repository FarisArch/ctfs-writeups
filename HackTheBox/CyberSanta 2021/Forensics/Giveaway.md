# Description
Santa's SOC team is working overtime during December due to Christmas phishing campaigns. A new team of malicious actors is targeting mainly those affected by the holiday spirit. Could you analyse the document and find the command & control server?

# Challenge
- We have `docm` file which means the Microsoft Document has macro enabled.
- We can use some tools from `oletools` specifically `olevba` to do this things.
```bash
$ olevba christmas_giveaway.docm > olevba.log
```
- Now we can analyze the contents of the file.

# Solution
- I went one by one line to see what each of them were doing and I finally found the part where the challenge starts.
```vba
Dim strFileURL, HPkXUcxLcAoMHOlj, cxPZSGdIQDAdRVpziKf, fqtSMHFlkYeyLfs, ehPsgfAcWaYrJm, FVpHoEqBKnhPO As String

    HPkXUcxLcAoMHOlj = "https://elvesfactory/" & Chr(Asc("H")) & Chr(84) & Chr(Asc("B")) & "" & Chr(123) & "" & Chr(84) & Chr(Asc("h")) & "1" & Chr(125 - 10) & Chr(Asc("_")) & "1s" & Chr(95) & "4"
     cxPZSGdIQDAdRVpziKf = "_" & Replace("present", "e", "3") & Chr(85 + 10)
     fqtSMHFlkYeyLfs = Replace("everybody", "e", "3")
     fqtSMHFlkYeyLfs = Replace(fqtSMHFlkYeyLfs, "o", "0") & "_"
     ehPsgfAcWaYrJm = Chr(Asc("w")) & "4" & Chr(110) & "t" & Chr(115) & "_" & Chr(Asc("f")) & "0" & Chr(121 - 7) & Chr(95)
     FVpHoEqBKnhPO = Replace("christmas", "i", "1")
     FVpHoEqBKnhPO = Replace(FVpHoEqBKnhPO, "a", "4") & Chr(119 + 6)

     Open XPFILEDIR For Output As #FileNumber
     Print #FileNumber, "strRT = HPkXUcxLcAoMHOlj & cxPZSGdIQDAdRVpziKf & fqtSMHFlkYeyLfs & ehPsgfAcWaYrJm & FVpHoEqBKnhPO"
```
- I decided to work on this string since I saw the start of the flag which is `HTB{`
- It's using ASCII numbers to represent the characters and numbers, if you follow it correctly you should get
```txt
HTB{Th1s_1s_4_pr3s3nt_3v3ryb0dy_w4nts_f0r_chr1stm4s}
```
-If you're unsure, you can always google the syntax for VBA