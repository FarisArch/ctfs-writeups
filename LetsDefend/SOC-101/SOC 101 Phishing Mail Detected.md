# Analysis
**EventID:**
87

**Event Time:**
April 4, 2021, 11 p.m.

**Rule:**
SOC101 - Phishing Mail Detected

**Level:**
Security Analyst

**SMTP Address**
146.56.195.192

**Source Address**
ethuyan852@gmail.com

**Destination Address**
mark@letsdefend.io

**E-mail Subject**
Its a Must have for your Phone

**Device Action**
Allowed

# Mail Analysis
-  Sender was `ethuyan852@gmail.com`
-  Receiver was `mark@letsdefend.io`
-  SMTP Address `146.56.195.192`
-  Sent on `April 4 2021 11 PM`
-  Title of email `Its a must have for your phone`
-  Body of the email contains a link to a product.
`http://nuangaybantiep.xyz`
- The domain doesn't resolve anymore.
### VirusTotal
