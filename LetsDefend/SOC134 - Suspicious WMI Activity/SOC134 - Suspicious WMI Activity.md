# Case Details
EventID : 71
Event Time : Mar, 07, 2021, 04:50 PM
Rule : SOC134 - Suspicious WMI Activity
Level : Security Analyst
Source Address : 172.16.17.54
Source Hostname : Desktop-Anderson
File Name : exec.bat
File Hash : 50459310eded4c520ab5c9e3626a9300
File Size : 52.00 B
Device Action : Allowed
Download (Password:infected) : 50459310eded4c520ab5c9e3626a9300.zip

# Static Analysis
 - Upon unzipping file dropped is `exec.bat`
 - File type is `exec.bat: ASCII text, with no line terminators`
## Hashes
50459310eded4c520ab5c9e3626a9300  exec.bat
be77e6113a985dc305be15ec419411bb840b69f8652d33fa919ae5c797b3ea85

## Scanners
[Virus Total Scan shows 0/58 flag](https://www.virustotal.com/gui/file/be77e6113a985dc305be15ec419411bb840b69f8652d33fa919ae5c797b3ea85)
![Scan Results](Capture.PNG)

## Analysis of contents in file
`python wmiexec.py LetsDefend/Administrator@127.0.0.1`

- Seems like it is trying to use `wmiexec.py` on the domain LetsDefend on localhost as Administrator. 

### What is wmiexec?
```txt
Wmi allows to open process in hosts where you know username/(password/Hash). Then, Wmiexec uses wmi to execute each command that is asked to execute (this is why Wmicexec gives you semi-interactive shell).
```
# Conclusion
- Normal users are only allowed to use their accounts. Attempting to leverage privileges to administrator is a bit suspicious plus `wmiexec.py` is a known tool used by threat actors to execute command. So this is a true positive.
