# Event Details
EventID: 83
Event Time: March 21, 2021, 1:02 p.m.
Rule: SOC119 - Proxy - Malicious Executable File Detected
Level: Security Analyst
Source Address 172.16.17.5
Source Hostname SusieHost
Destination Address 51.195.68.163
Destination Hostname win-rar.com
Username Susie
Request URL https://www.win-rar.com/postdownload.html?&L=0&Version=32bit
User Agent Chrome - Windows
Device Action Allowed

# Analysis
## Logs
- Let's check the logs for the source address and destination address.
- We can see a request was made to the address with a GET method.
```txt
Request URL: https://www.win-rar.com/postdownload.html?&L=0&Version=32bit
Request Method: GET

Device Action: Allowed

Process: chrome.exe

Parent Process: explorer.exe

Parent Process MD5: 8b88ebbb05a0e56b7dcc708498c02b3
```
- The URL points to win-rar.com which is a download page for famous archive tool `WinRar`
- VirusTotals scans also proved it to be clean.

# Final Verdict
- The alert was a false positive. WinRAR is a legitimate executable file. It is an archive tool used by major Windows distribution. The URL is also belongs to the company which is also legitimate.