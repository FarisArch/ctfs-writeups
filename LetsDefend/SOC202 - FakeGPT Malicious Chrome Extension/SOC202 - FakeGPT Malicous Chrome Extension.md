# Case Details
	
- EventID : 153
- Event Time : May, 29, 2023, 01:01 PM
- Rule : SOC202 - FakeGPT Malicious Chrome Extension
- Level : Security Analyst
- Hostname : Samuel
- IP Address : 172.16.17.173
- File Name : hacfaophiklaeolhnmckojjjjbnappen.crx
- File Path : C:\Users\LetsDefend\Download\hacfaophiklaeolhnmckojjjjbnappen.crx
- File Hash : 7421f9abe5e618a0d517861f4709df53292a5f137053a227bfb4eb8e152a4669
- Command Line : chrome.exe --single-argument C:\Users\LetsDefend\Download\hacfaophiklaeolhnmckojjjjbnappen.crx
- Trigger Reason : Suspicious extension added to the browser.
- Device Action : Allowed


# Analysis
- Filename is super suspicious at first glance.
- Let's check our the user at EndPoint Security.

## Endpoint Security

### Browser history
- Okay let's check the browser history.
```txt
2023-05-29 13:01:44 https://chrome.google.com/webstore/detail/chatgpt-for-google/hacfaophiklaeolhnmckojjjjbnappen
2023-05-29 13:01:47 https://support.google.com/chrome_webstore/?p=crx_warning
2023-05-29 13:01:48 https://support.google.com/chrome_webstore/answer/2664769?visit_id=638211162424485757-2090301244&p=crx_warning&rd=1
2023-05-29 13:01:55 chrome://extensions
2023-05-29 13:02:01 chrome://extensions/?id=hacfaophiklaeolhnmckojjjjbnappen
2023-05-29 13:10:18 https://chat.openai.com/
2023-05-29 13:10:22 https://chat.openai.com/auth/login
2023-05-29 13:10:59 https://auth0.openai.com/authorize?client_id=TdJIcbe16WoTHtN95nyywh5E4yOo6ItG&scope=openid%20email%20profile%20model.request%20model.read%20organization.read%20organization.write&response_type=code&redirect_uri=http
```
- We see indeed the user installed the extension for chrome, after installing it, it looks like they visited `openai.com` which is a legitimate website, nothing suspicious is happening.

### Process list
- We only found 1 related process.

```txt
Event Time : 2023-05-29 13:01:52.137

Process ID : 5756

Image Path : C:\Program Files\Google\Chrome\Application\chrome.exe

Process User : EC2AMAZ-ILGVOIN\LetsDefend

Parent Name : OpenWith.exe

Parent Path : C:\Windows\System32\OpenWith.exe

Command Line : "C:\Program Files\Google\Chrome\Application\chrome.exe" --single-argument C:\Users\LetsDefend\Desktop\hacfaophiklaeolhnmckojjjjbnappen.crx
```
- This is normal behavior for chrome for installing extensions.
- No other suspicous process found.

### Network process
```txt
2023-05-29 13:02:59 18.140.6.45
2023-05-29 13:03:13 172.217.17.142
```
- Apparently both of them are clean.



While we haven't found any suspicious behaviours, the extension filename is still suspicious.

## Log Management
- Searching for our host address, we find the two IP we found communicating with the host.

```txt
Type: Network Connection
DestinationIp: 18.140.6.45
DestinationHost: www.chatgptgoogle.org
DestinationPort: 80
Image: C:\Program Files\Google\Chrome\Application\chrome.exe
UtcTime: 2023-05-29 13:02:59.848
```
- Domain is flagged as malicious by 9 vendors.

```txt
Type: Network Connection
DestinationIp: 52.76.101.124
DestinationHost: www.chatgptforgoogle.pro
DestinationPort: 80
Image: C:\Program Files\Google\Chrome\Application\chrome.exe
UtcTime: 2023-05-29 13:02:47.847
```
- Domain is flagged as malicious by 11 vendors.

# Verdict
- Extension is malicious as it creates a network connection to two domains that are malicious, this may be an attempt to phish user for their credentials as you need to login to use chatGPT.

## IOCs

### Host
```txt
7421f9abe5e618a0d517861f4709df53292a5f137053a227bfb4eb8e152a4669 hacfaophiklaeolhnmckojjjjbnappen.crx
```

### Network
```txt
www.chatgptforgoogle.pro
www.chatgptgoogle.org
```



