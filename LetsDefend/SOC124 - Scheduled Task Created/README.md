# SOC124 - Scheduled Task Created

# Case Details
- EventID : 57
- Event Time :Feb, 14, 2021, 11:17 AM
- Rule :SOC124 - Scheduled Task Created
- Level : Security Analyst
- Source Address : 172.16.17.83
- Source Hostname : Maxim
- File Name : GoogleUpdate.exe
- File Hash : 82f657b0aee67a6a560321cf0927f9f7
- File Size : 151.29 KB
- Device Action : Blocked

## Static Analysis

- File name is `GoogleUpdate.exe`, type `GoogleUpdate.exe: PE32 executable (GUI) Intel 80386, for MS Windows`

### Hashes
```txt
82f657b0aee67a6a560321cf0927f9f7
794cf7644115198db451431bca7c89ff9a97550482b1e3f7f13eb7aca6120a11
```

- Checking the file reputation it is a google chrome installer, seems to be signed by Google itself.
![Alt text](image.png)
- Checking on other scanners, the file is not flagged as malicious at all.

# Verdict
- File is not malicious. File is legitimately signed by Google and is a Google Chrome instaler. This is a false positive.
