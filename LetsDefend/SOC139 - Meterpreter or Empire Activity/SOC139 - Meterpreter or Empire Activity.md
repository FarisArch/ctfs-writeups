# Case Details
	
EventID : 78
Event Time : Mar, 15, 2021, 02:15 PM
Rule : SOC139 - Meterpreter or Empire Activity
Level : Security Analyst
Source Address : 172.16.17.55
Source Hostname : Alex - HP
File Name : cobaltstrike_shellcode.exe
File Hash : 24d99ba5654cdf31141c66fd9417b7e0
File Size : 219.00 Kb
Device Action : Allowed
Download (Password:infected) : 24d99ba5654cdf31141c66fd9417b7e0.zip

# Static Analysis
- Upon unzipping the file, file dropped name `cobaltstrike_shellcode.bin`
- File type is `cobaltstrike_shellcode.bin: PE32 executable (GUI) Intel 80386 (stripped to external PDB), for MS Windows`

## Hashes
24d99ba5654cdf31141c66fd9417b7e0  cobaltstrike_shellcode.bin
b9321c27be4295c15d7f92fafc20d7ccac5f21204b79ebc2fed583dda0197cf9  cobaltstrike_shellcode.bin

## Scanners
[Virus Total scan shows 65/70 flags!](https://www.virustotal.com/gui/file/b9321c27be4295c15d7f92fafc20d7ccac5f21204b79ebc2fed583dda0197cf9)
![Scan results](Capture.PNG)

- The scan also shows these payloads are generated from cobalt strike.

### What is cobalt strike?
- Cobalt strike is an adversary simulation tool and is used by lots of companies for red team operations. Though this doesn't stop threat actors from using it for illegal purposes.

## Strings
- No interesting things found in strings output then library names and function calls.

## Libraries
- Only kernel32.dll and msvcrt.dll

## Function calls.
- We see `Sleep` is called which is suspicious.

- We were unable to find the IP or any network indicators.

# Dynamic Analysis
- So doing some dynamic analysis, there were no files dropped on the host or even connections made. So this leads me to another tool which is `ANY.RUN`.

- Doing some checks with the hash on ANY.RUN, all the tasks ran were confirmed to be cobalt strikes beacon, that when ran will connect to an IP address.

## Network indicators.
- We can check the log management of this computer and compare it with the results in ANY.RUN. I noticed that all of the connections made by the malware was with port 443. So checking the logs, we find this suspicious IP, which doesn't have any logs on it. `120.79.181.138`
- It is flagged malicious by 3 out of 70 vendors. Also checking the [relations of the IP](https://www.virustotal.com/gui/ip-address/120.79.181.138/relations) it is found that the IP is used in `cobaltstrike_shellcode.bin`.


# Conclusion
- It can be safely concluded that this is malicious due to it being a cobalt strike beacon. The executable connects to the IP `120.79.181.138` which is the command and control server. Further investigations are needed to determine the capabilities and severity of the executable.



