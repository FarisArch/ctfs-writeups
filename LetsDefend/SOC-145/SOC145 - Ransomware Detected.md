# Details
****EventID:**

92

**Event Time:**

May 23, 2021, 7:32 p.m.

**Rule:**

SOC145 - Ransomware Detected

**Level:**

Security Analyst

**Source Address**

172.16.17.88

**Source Hostname**

MarkPRD

**File Name**

ab.exe

**File Hash**

0b486fe0503524cfe4726a4022fa6a68

**File Size**

775.50 Kb

**Device Action**

Allowed

**Download (Password:infected):**

[0b486fe0503524cfe4726a4022fa6a68.zip](https://app.letsdefend.io/download/downloadfile/0b486fe0503524cfe4726a4022fa6a68.zip/)

# Analysis
- First of all we should contain the host, using the source IP of `172.16.17.88`. It is Windows 10 client.
- We were able to download the zip file containing the malware.
## Static Analysis
- Hash for the binary.
```sh
0b486fe0503524cfe4726a4022fa6a68  ab.bin
```
- Checking the hash of the file in VirusTotal, it was flagged a lot as ransomware by 59 vendors.
![[virustotal.png]]
- Checking the reports of the past reports, they were no C2 address involved in this.
- Using `strings` on the binary, we found the header which indicates that this binary was target for windows user.
`!This program cannot be run in DOS mode.`
- We can next try to decompile the binary in a program. I'll be using cutter.
### Libraries
1. kernel32.dll - Handles memory usage
2. advapi32.dll - Responsible for windows registry, shutting down system, start/stop windows services.
3. shell32.dll - Window shell API functions, used to open web pages and files.
4. ole32.dll
5. oleaut32.dll - System component.
6. mpr.dll
7. netapi32.dll
8. iphlpapi.dll
9. ws2_32.dll
10. rstrtmgr.dll
- Language used is `C`
- Compiled on Sunday April 4 00:35:19 2021

## Endpoint Management
- We can confirm in the process list of the source address that `ab.exe` is running.
- We also identified `wmic.exe`,`wbadmin.exe` and `vssadmin.exe` doing something weird.
`wmic.exe`
```powershell
wmic SHADOWCOPY DELETE /nointeractive
```
`wbadmin.exe`
```powershell
wbadmin DELETE SYSTEMSTATEBACKUP
```
`vssadmin.exe`
```powershell
vssadmin Delete Shadows /All /Quiet
```
- All these 3 share one thing in common, which is deleting shadow copies and backups of the client.

# Conclusion
- With the binary deleting and shadow copies and backups, it's safe to say that this binary is used by ransomware operators.
