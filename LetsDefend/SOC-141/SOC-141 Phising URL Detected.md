# Analysis
**EventID:**
86

**Event Time:**
March 22, 2021, 9:23 p.m.

**Rule:**
SOC141 - Phishing URL Detected

**Level:**
Security Analyst

**Source Address**
172.16.17.49

**Source Hostname**
EmilyComp

**Destination Address**
91.189.114.8

**Destination Hostname**
mogagrocol.ru

**Username**
ellie

**Request URL**
http://mogagrocol.ru/wp-content/plugins/akismet/fv/index.php?email=ellie@letsdefend.io

**User Agent**
Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.88 Safari/537.36

**Device Action**
Allowed

## Logs
- With the source and destination address, we can check in the logs if the user actually accessed it.
- Requested URL was : `http://mogagrocol.ru/wp-content/plugins/akismet/fv/index.php?email=ellie@letsdefend.io`
![[logs.png]]
- From this, it looked like that perhaps the firewall blocked it?

## Endpoint
- Looking at EmilyComp's history, there was no history of Emily visiting the URL. 

# Conclusion
- It is a malicious used for phishing but the URL was blocked by the firewall.