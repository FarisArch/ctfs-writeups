# Challenge
Login again like usual.
Like the second lab, the edit and delete post does not work and the 'Delete an item' button does not work.
Let's check the source code.
Source code looks fine, no hidden values or links.

But I do see it is running a javascript file. And I'm sure it wasn't there before.
`<script src="myscripts.js"></script>`

Let's check that out.
I didn't have to scroll far and I found this.
```js
( function( global, factory ) {

	"use strict";

	// I am putting all the endpoints here because mike keeps forgetting them. Fuck mike!
	// gfdhghjhgjhfghj.php - the edit endpoints
	// index.php - index
	// login.php - Login dumbass mike!!!!1111
	// ... I am too lazy, go figure it out yourself mike.
```
Looks like the developer left a note for the other developer.
And it looks like an endpoint we're not suppose to use yet.
Let's head there.
It doesn't move us?
Let's check Burp
# Edit IDOR
```http
GET /cheesems-3/gfdhghjhgjhfghj.php HTTP/1.1
Host: 23.239.9.22
Upgrade-Insecure-Requests: 1
User-Agent: Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/90.0.4430.212 Safari/537.36
Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9
Accept-Encoding: gzip, deflate
Accept-Language: en-US,en;q=0.9
Cookie: PHPSESSID=m7lk6n9c45304so6p2a66vcmdi
dnt: 1
sec-gpc: 1
Connection: close
```
```http
302 REDIRECT
```
Okay so it's redirecting us, what if we supply it a GET parameter ID.
```http
HTTP/1.1 200 OK
```
Now we're at the edit page of an ID we supplied!
And yet again, a hidden ID value that we can change and a GET parameter we can control.
Let's change post 4573 which is owned by user2.