# GET 10.php
It's a simple page with a form saying 
`What greeting do you want to say?` where we can enter our response and send it.

When we send the request, it seems to be reflected in here.
`## This is challenge: /RXSS/GET/10.php?say=Hi`

Let's see the source code of it.
```html
<h2>This is challenge: /RXSS/GET/10.php?say=Hi</h2>
```
Since we're controlling the say parameter, what if we try to end the h2 tags and put another maybe a h1 tag.
`</h2><hi>hi</h1>`
Okay we have a weird interaction now.
`This is challenge: /RXSS/GET/10.php?say=%3C%2Fh2%3E%3Ch1%3Ehi%3C%2Fh1%3E`

But our h1 tag is being reflected in the DOM  so it works!
So we do have HTML injection.

Let's try script tags.
`</h2><script>alert(document.domain);</script>`

And it works! So this particular page is indeed vulnerable to reflected XSS.

# GET 20.php
Again same stuff but this time it is :
`What URL do  you want to print?`
This time it creates a link of our response.
```html
<a href="test">link</a>
```
There is also a script that is doing that.
```html
<script>
document.write("a href=test>" + "link" "</a>")
</script>
```
Okay so we're controlling the value of a tags
Let's try inserting a h1 tag to see if it breaks.
`"><h1>hi`
And we don't get a link but the h1 tag isn't being reflected.
Maybe we can insert some events in the a tags?
`hi onmouseover=alert(1)`
And it worked!!
```html
<a href="hi" onmouseover="alert(1)">link</a>
```
So what's happening is lets take at the script.
```html
document.write("a href=hi onmouseover=alert(1)>" + "link" "</a>")
```
We have HTML injection is this page.

# POST 10.php
Okay now have the same challenge as the first one but this time it's a POST request, should be relatively the same.
Let's send our response to see what happens.

We see our response `hi` is being reflected in the body.
Let's try a h1 tag
`<h1>faris</h1>`
And it worked immediately! There is no protection at all on this page.
Let's do a script tag.
`<script>alert(document.domain)</script>`
And we get an alert.
This page is also vulnerable to HTML injection.