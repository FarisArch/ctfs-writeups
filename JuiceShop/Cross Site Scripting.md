# XSS on the search bar
First try searching for a non malicious html tag such as h1 tags.

It will return nothing and if you inspect element that area, you will see that now there is an empty h1 tag on the DOM.

Let's try with IMG tags.
```js
<img src=# onerror=alert(1)
```
XSS is triggered
Now let's do something with IFrame
```js
<iframe src="javascript:alert(document.domain)">
```
We get an alert of the domain of the server to indicate that XSS was triggered on the server itself.
