User was able to submit a zero star review on the Customer Feedback form due to insufficient input validation.
```http
POST https://faris-shop.herokuapp.com/api/Feedbacks/ HTTP/1.1
Connection: keep-alive
Content-Length: 87
sec-ch-ua: " Not A;Brand";v="99", "Chromium";v="90"
Accept: application/json, text/plain, */*
Authorization: Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJzdGF0dXMiOiJzdWNjZXNzIiwiZGF0YSI6eyJpZCI6MjIsInVzZXJuYW1lIjoiIiwiZW1haWwiOiJ0ZXN0MkBnbWFpbC5jb20iLCJwYXNzd29yZCI6IjVmNGRjYzNiNWFhNzY1ZDYxZDgzMjdkZWI4ODJjZjk5Iiwicm9sZSI6ImN1c3RvbWVyIiwiZGVsdXhlVG9rZW4iOiIiLCJsYXN0TG9naW5JcCI6InVuZGVmaW5lZCIsInByb2ZpbGVJbWFnZSI6Ii9hc3NldHMvcHVibGljL2ltYWdlcy91cGxvYWRzL2RlZmF1bHQuc3ZnIiwidG90cFNlY3JldCI6IiIsImlzQWN0aXZlIjp0cnVlLCJjcmVhdGVkQXQiOiIyMDIxLTA4LTAzIDA5OjIyOjEyLjMxMSArMDA6MDAiLCJ1cGRhdGVkQXQiOiIyMDIxLTA4LTAzIDEwOjAwOjMxLjcwOCArMDA6MDAiLCJkZWxldGVkQXQiOm51bGx9LCJpYXQiOjE2Mjc5ODUxMzcsImV4cCI6MTYyODAwMzEzN30.EJIh5vKHVTDJknGVgob-34fxjGdSFrgqZWky9Oomueuo_W6cZSn2PvNAlW8fYEPl6Hohti2DFOEcNpTTok8B0XoDA1mJ3SbQw50sAFNM-T1EzOH-zC_MFNEYRp3cLRmqQwa0KIqgBpBVLtchLTeC7UHFdPThbzHhKSSCnuqaCX0
sec-ch-ua-mobile: ?0
User-Agent: Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/90.0.4430.212 Safari/537.36
Content-Type: application/json
Origin: https://faris-shop.herokuapp.com
Sec-Fetch-Site: same-origin
Sec-Fetch-Mode: cors
Sec-Fetch-Dest: empty
Referer: https://faris-shop.herokuapp.com/
Accept-Language: en-US,en;q=0.9
Cookie: language=en; welcomebanner_status=dismiss; cookieconsent_status=dismiss; continueCode=ekxOpMRYWolm4wz3Z5JrLadgmTVacxNSeriBB06KQBb129yVqDvPNXgn78jE; token=eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJzdGF0dXMiOiJzdWNjZXNzIiwiZGF0YSI6eyJpZCI6MjIsInVzZXJuYW1lIjoiLz9zZWFyY2g9I3giLCJlbWFpbCI6InRlc3QyQGdtYWlsLmNvbSIsInBhc3N3b3JkIjoiNWY0ZGNjM2I1YWE3NjVkNjFkODMyN2RlYjg4MmNmOTkiLCJyb2xlIjoiY3VzdG9tZXIiLCJkZWx1eGVUb2tlbiI6IiIsImxhc3RMb2dpbklwIjoidW5kZWZpbmVkIiwicHJvZmlsZUltYWdlIjoiL2Fzc2V0cy9wdWJsaWMvaW1hZ2VzL3VwbG9hZHMvZGVmYXVsdC5zdmciLCJ0b3RwU2VjcmV0IjoiIiwiaXNBY3RpdmUiOnRydWUsImNyZWF0ZWRBdCI6IjIwMjEtMDgtMDNUMDk6MjI6MTIuMzExWiIsInVwZGF0ZWRBdCI6IjIwMjEtMDgtMDNUMTA6NTA6MjkuMTUxWiIsImRlbGV0ZWRBdCI6bnVsbH0sImlhdCI6MTYyNzk4NzgyOSwiZXhwIjoxNjI4MDA1ODI5fQ.u0IyqHgwOWutj1Fj22_WvJEaoRu1L2o6hsbS5fZCOnUwYOvH8CaT1x1ZVXWdAQxmej6WWBUPa-1yHqtDaCNsJk2Qrt8PczIYdn3SBW-fkr8T3Q-8eDQzkbTGYc2MkqSxOVyTvxsCSiU5ypCR4WAnffl4aoYaLdrJvXOK2moH-KE
dnt: 1
sec-gpc: 1
Host: faris-shop.herokuapp.com

{"UserId":22,"captchaId":1,"captcha":"7","comment":"test (***t2@gmail.com)","rating":0}
```
The rating parameter could be changed to 0, and since there were no validation the request went through.
