# Recon Phase
# Wappylzer
Identifies : 
### JS Frameworks
* Angular 11.2.14
* TypeScript
* ZoneJS

### JS Libraries
* JQuery 2.24

### Nuclei Scan
```bash
[2021-08-04 00:55:58] [exposed-metrics] [http] [low] https://faris-shop.herokuapp.com/metrics
```

### robots.txt
```txt
Disallow : /ftp
```
Information disclosure of sensitive files.

### JS File Analysis
Possible Path in main-es2018.js
```js
gs = [{
            path: "administration",
            component: za,
            canActivate: [O]
        }, {
            path: "accounting",
            component: qr,
            canActivate: [x]
        }, {
            path: "about",
            component: $t
        }, {
            path: "address/select",
            component: xo,
            canActivate: [P]
        }, {
            path: "address/saved",
            component: ko,
            canActivate: [P]
        }, {
            path: "address/create",
            component: Oo,
            canActivate: [P]
        }, {
            path: "address/edit/:addressId",
            component: Oo,
            canActivate: [P]
        }, {
            path: "delivery-method",
            component: ts
        }, {
            path: "deluxe-membership",
```
and more
```js
path: "saved-payment-methods",
            component: wr
        }, {
            path: "basket",
            component: st
        }, {
            path: "order-completion/:id",
            component: pc
        }, {
            path: "contact",
            component: Ft
        }, {
            path: "photo-wall",
            component: us
        }, {
            path: "complain",
            component: bi
        }, {
            path: "chatbot",
            component: fi
        }, {
            path: "order-summary",
            component: Cc
        }, {
            path: "order-history",
            component: Fc
        }, {
            path: "payment/:entity",
            component: yr
        }, {
            path: "wallet",
            component: Uc
        }, {
            path: "login",
            component: ha
        }, {
            path: "forgot-password",
            component: _e
        }, {
            path: "recycle",
            component: Yi
        }, {
            path: "register",
            component: he
        }, {
            path: "search",
            component: na
        }, {
            path: "hacking-instructor",
            component: na
        }, {
            path: "score-board",
            component: Jn
        }, {
            path: "track-result",
            component: xt
        }, {
            path: "track-result/new",
            component: xt,
            data: {
                type: "new"
            }
        }, {
            path: "2fa/enter",
            component: to
        }, {
            path: "privacy-security",
            component: no,
            children: [{
                path: "privacy-policy",
                component: Io
            }, {
                path: "change-password",
                component: ti
            }, {
                path: "two-factor-authentication",
                component: so
            }, {
                path: "data-export",
                component: fo
            }, {
                path: "last-login-ip",
                component: vo
            }]
```

Not much info from other files.


#### Notes
user1 : test3@gmail.com
pass : password

user2: test2@gmail.com
pass : password123

# User interaction
User is able to :
* Add item to basket
* Delete item from basket
* Set a username
* Upload an avatar
* Add Image URL
* Add address
* Change password

# Possible Endpoints for Exploit
- /track-result
- /payment
- /complain
- /basket
- /order-completion






