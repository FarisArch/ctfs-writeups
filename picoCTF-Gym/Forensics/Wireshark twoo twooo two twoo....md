# Solution
- We're given a `.pcap` file which can be viewed through Wireshark or TShark.


# Analyzing through Wireshark
- There is a few HTTP  `GET` requests made to `192.168.38.104` to `/flag` which fetches the flag but it isn't the real flag.
- The hint tells us to look for suspicious traffics and the only suspicious traffic I'm seeing is a DNS request being made to `subdomain.redshrimpherring.com` where `subdomain` is random text?
- They are a lot of DNS requesting being made to the domain. Most of the request is sent by `8.8.8.8` and  the response is being received by `192.168.38.104`. But if we go down a few lines, at the bottom we'll see a suspicious IP making a DNS query to `192.168.38.104` and the response is being sent to `18.217.1.57`.
- I have no idea how to export DNS requests using Wireshark so we have to use TShark

# Extracting DNS Requests using TShark
- Searching online on how to do it, I managed to create this
```sh
┌──(kali㉿kali)-[~/ctf]
└─$ tshark -r shark2.pcapng -T fields -e ip.dst -e dns.qry.name -Y "dns.flags.response eq 0 and dns.qry.name contains reddshrimpandherring.com.windomain.local" > 8.8.8.8domains.txt
```
- Which outputs the DNS requests with the IP Destination and contains the text `redshrimpherring.com.windomain.local`
- Now we can see each DNS requests and it's destination
```txt
8.8.8.8	BoORWyDu.reddshrimpandherring.com.windomain.local
8.8.8.8	MVsbj1/d.reddshrimpandherring.com.windomain.local
8.8.8.8	UWrgyXWr.reddshrimpandherring.com.windomain.local
8.8.8.8	9NzCwWxd.reddshrimpandherring.com.windomain.local
8.8.8.8	m/TqO+IW.reddshrimpandherring.com.windomain.local
8.8.8.8	o2ZJtOyF.reddshrimpandherring.com.windomain.local
18.217.1.57	cGljb0NU.reddshrimpandherring.com.windomain.local
```
- We can see a DNS response being sent to `18.217.1.57`. While others are only to `8.8.8.8`
- If we gather each of the DNS queries to `18.217.1.57`, we can observe that each subdomain name is base64 encoded. 
```txt
cGljb0NURntkbnNfM3hmMWxfZnR3X2RlYWRiZWVmfQ==
```
- If we base64 decode it we'll get :
```txt
picoCTF{dns_3xf1l_ftw_deadbeef}
```

