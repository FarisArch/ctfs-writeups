Author: Dylan

# Description

Can you find the flag? [shark1.pcapng](https://mercury.picoctf.net/static/81c7862241faf4a48bd64a858392c92b/shark1.pcapng).

So let's open this pcap file in wireshark.
Looking at the file first hand, there's a lot of traffic. Let's filter it so we show only http traffic.
```txt
http.response.code == 200
```
When we filtered it out, it showed us a lot of http, but most of them are kerberos encrypted. Let's find one that isn't
```txt
cvpbPGS{c33xno00_1_f33_h_qrnqorrs}
```
I found this in and it resembles a flag. Let's try to decode it using basic cipher like ROT
```txt
picoCTF{p33kab00_1_s33_u_deadbeef}
```