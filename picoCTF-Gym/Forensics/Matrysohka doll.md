# Description

Matryoshka dolls are a set of wooden dolls of decreasing size placed one inside another. What's the final one?

So my first to go is to open the file. But It can't be opened, looks like the magic numbers belong to png. Let's change ext to .png. 

Since it's a png file, we're unable to steghide on it. So let's try binwalk.
Since it's called a matryoshka doll, this must means it's being zipped a lot.
 
So what I did I keep extracting images until I reach the flag
```txt
picoCTF{4f11048e83ffc7d342a15bd2309b47de}
```