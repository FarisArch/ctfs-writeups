Author: jedavis/Danny

# Description

This [garden](https://jupiter.challenges.picoctf.org/static/4153422e18d40363e7ffc7e15a108683/garden.jpg) contains more than it seems.

Hints : Hex Editor

If it's not obvious for you, this challenge asks us to view the hex number of the file. For this I'll be using ghex

If you scroll to the bottom of the file, you'll find the flag.
```txt
picoCTF{more_than_m33ts_the_3y33dd2eEF5
```