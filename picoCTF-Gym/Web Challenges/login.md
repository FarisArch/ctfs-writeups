## Description
My dog-sitter's brother made this website but I can't get in; can you help?

[login.mars.picoctf.net](https://login.mars.picoctf.net)

It's a login page, let's try common credentials. 
```
Username: letmein
Password: lol123
```
Incorrect username.
```
Username: admin
Password: admin
```
Incorrect password.
There seems to be something handling this check. Let's view the source code.
```html
<!doctype html>
<html>
    <head>
        <link rel="stylesheet" href="styles.css">
        <script src="index.js"></script>
    </head>
    <body>
        <div>
          <h1>Login</h1>
          <form method="POST">
            <label for="username">Username</label>
            <input name="username" type="text"/>
            <label for="username">Password</label>
            <input name="password" type="password"/>
            <input type="submit" value="Submit"/>
          </form>
        </div>
    </body>
</html>

```
Looks like there is index.js that is handling it. Let's check it out.
```js
(async () => {
    await new Promise((e => window.addEventListener("load", e))), document.querySelector("form").addEventListener("submit", (e => {
        e.preventDefault();
        const r = {
                u: "input[name=username]",
                p: "input[name=password]"
            },
            t = {};
        for (const e in r) t[e] = btoa(document.querySelector(r[e]).value).replace(/=/g, "");
        return "YWRtaW4" !== t.u ? alert("Incorrect Username") : "cGljb0NURns1M3J2M3JfNTNydjNyXzUzcnYzcl81M3J2M3JfNTNydjNyfQ" !== t.p ? alert("Incorrect Password") : void alert(`Correct Password! Your flag is ${atob(t.p)}.`)
    }))
})();
```
So it checks if the string is equal to that string, the string seems to be base64 encoded. Let's grab that and decode it.
```text
picoCTF{53rv3r_53rv3r_53rv3r_53rv3r_53rv3r}
```