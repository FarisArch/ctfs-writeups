## Description
Find the flag being held on this server to get ahead of the competition
http://mercury.picoctf.net:34561/


Opening up the web server, it seems to be a page that ask you to select a color blue or red. Clicking those doesn't do anything.

Viewing the source code, nothing interesting pops out. The challenge seems to be hinting towards Burpsuite, so we probably need to change the headers or something.

Let's try changing the header for the POST request for Blue.
```html
HEAD /index.php HTTP/1.1
HOST: mercury.picoctf.net:34561
```

Looking at the response.
```html
HTTP/1.1 200 OK
flag: picoCTF{r3j3ct_th3_du4l1ty_8f878508}
Content-type: text/html; charset=UTF-8
```

