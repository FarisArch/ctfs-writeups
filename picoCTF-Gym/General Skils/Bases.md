Author: Sanjay C/Danny T

# Description

What does this `bDNhcm5fdGgzX3IwcDM1` mean? I think it has something to do with bases.

Right from the start we can identify it is base64, but if you're unsure you can use cyberchef to check it. If it's base64, it will detect it and can auto decode it.
```bash
┌─[✗]─[faris@parrot-hp]─[~/Downloads]
└──╼ $echo -n "bDNhcm5fdGgzX3IwcDM1" | base64 -d
l3arn_th3_r0p35
```