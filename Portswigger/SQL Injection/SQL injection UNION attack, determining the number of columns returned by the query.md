This lab contains an SQL injection vulnerability in the product category filter. The results from the query are returned in the application's response, so you can use a UNION attack to retrieve data from other tables. The first step of such an attack is to determine the number of columns that are being returned by the query. You will then use this technique in subsequent labs to construct the full attack.

To solve the lab, determine the number of columns returned by the query by performing an [SQL injection UNION](https://portswigger.net/web-security/sql-injection/union-attacks) attack that returns an additional row containing null values.

# Practical

Okay first order of business is to see how the website works and if it's vulnerable or not to SQL injection. Let's click around the site.
```html
https://ac7f1fa91f701465800202880029007d.web-security-academy.net/filter?category=Pets
```
If we click on a category we get this link.
And if we click **view details** we get this url.
```html
https://ac7f1fa91f701465800202880029007d.web-security-academy.net/product?productId=2
```
Let's try both of this with single quotes.
For the second one it doesn't seem to be vulnerable to SQL injection, it will keep returning Invalid product ID.
Let's try the category one.
And it breaks the site.
```txt
Internal Server Error
```
Okay since we're doing a UNION attack, we need to determine the number of columns. Let's do the ORDER BY payloads.
And if we increment it 1 by 1, we'll see that it breaks at :
```sql
ORDER BY 4--
```
That means that it probably has only 3 columns. Since the challenge asks us to return it with rows containing null values. Let's use the second method.
```sql
' UNION SELECT NULL--
```
For each time we increment it, we append NULL before the comment. If the numbers of NULLs matches and doesn't return an error. We successfully determined the number of columns. Since we checked with ORDER BY and it stopped at 3. Let's try 3.
```sql
' UNION SELECT NULL,NULL,NULL--
```
And we solved the lab!