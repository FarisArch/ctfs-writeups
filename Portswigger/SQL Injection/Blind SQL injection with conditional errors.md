This lab contains a [blind SQL injection](https://portswigger.net/web-security/sql-injection/blind) vulnerability. The application uses a tracking cookie for analytics, and performs an SQL query containing the value of the submitted cookie.

The results of the SQL query are not returned, and the application does not respond any differently based on whether the query returns any rows. If the SQL query causes an error, then the application returns a custom error message.

The database contains a different table called `users`, with columns called `username` and `password`. You need to exploit the blind [SQL injection](https://portswigger.net/web-security/sql-injection) vulnerability to find out the password of the `administrator` user.

To solve the lab, log in as the `administrator` user.

# Practical
Checking out page, it seems there's no 'Welcome back ' message, let's try for conditional errors.
This is our vulnerable parameter:
```txt
TrackingId=CyhXJ8XEDbU17bEs;
```
Let's try to trigger something. (Note this is an Oracle DB)