This lab contains a [blind SQL injection](https://portswigger.net/web-security/sql-injection/blind) vulnerability. The application uses a tracking cookie for analytics, and performs an SQL query containing the value of the submitted cookie.

The results of the SQL query are not returned, and the application does not respond any differently based on whether the query returns any rows or causes an error. However, since the query is executed synchronously, it is possible to trigger conditional time delays to infer information.

To solve the lab, exploit the [SQL injection](https://portswigger.net/web-security/sql-injection) vulnerability to cause a 10 second delay.

# Practical
Again we'll be practicing exploiting the tracking cookie for a blind SQL injection. But this time we don't have any welcome back messages to do conditional statement, so we have to perform a time delay.
So we have the trackingID
```html
TrackingId=2Dr9ubLXKmEiWjJe;
```
And we need to somehow add a time delay to it to see if it's vulnerable to blind SQL injection.
Let's try that.
```sql
TrackingId=2Dr9ubLXKmEiWjJe' || IF (1=1) WAITFOR DELAY '0:0:10';
```
And since we don't know the type of database we're dealing with. We need to test all the database syntax for time delays.
```sql
TrackingId=2Dr9ubLXKmEiWjJe' || IF(1=1) pg_sleep(10)--;
```
And if we see that Burp suite is not showing any results and 10 secs later it shows the result which means that it is vulnerable to blind SQL injection.