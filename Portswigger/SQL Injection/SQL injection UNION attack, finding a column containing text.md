This lab contains an SQL injection vulnerability in the product category filter. The results from the query are returned in the application's response, so you can use a UNION attack to retrieve data from other tables. To construct such an attack, you first need to determine the number of columns returned by the query. You can do this using a technique you learned in a [previous lab](https://portswigger.net/web-security/sql-injection/union-attacks/lab-determine-number-of-columns). The next step is to identify a column that is compatible with string data.

The lab will provide a random value that you need to make appear within the query results. To solve the lab, perform an [SQL injection UNION](https://portswigger.net/web-security/sql-injection/union-attacks) attack that returns an additional row containing the value provided. This technique helps you determine which columns are compatible with string data.

# Practical
## Task
* Make the database retrieve the string: 'topLX8'

Like usual let's click around the website to determine how it functions.
Clicking on categories like gives us this URL.
```html
https://ac3a1f411e8d3bf280a0db1b00d90082.web-security-academy.net/filter?category=Gifts
```
And clicking one of them products give us :
```html
https://ac3a1f411e8d3bf280a0db1b00d90082.web-security-academy.net/product?productId=2
```
Let's try to see if productID is vulnerable or not 
Like again it only returns Invalid product ID so it's probably not vulnerable.
Let's try with category again.
```html
https://ac3a1f411e8d3bf280a0db1b00d90082.web-security-academy.net/filter?category='
```
And we get an Internal Server Error.
So it's vulnerable to SQL injection. Since we're doing an UNION attack lets do that.
First order of business is to determine the number of columns. Let's do both methods. First method is using ORDER BY --.

Increment it 1 by 1 and observe the web response and behavior. At ORDER BY 4--, the web page returns an error. So it is possible it has 3 columns. Let's do the second method and try it with 4 columns.
```sql
' UNION SELECT NULL,NULL,NULL,NULL--
```
And the site breaks, reducing it to 3 and the site works so it is confirmed that we have 3 columns.
Our task is to retrieve the string **topLX8**
```sql
' UNION SELECT NULL,NULL,NULL--
```
Do this just replace the one of the position of NULL with the string and if it doesn't return the error it is probably a string since it's the same data type.
```sql
' UNION SELECT 'topLX8',NULL,NULL--
```
First position returns an error, try the second position.
```sql
' UNION SELECT NULL,'topLX8',NULL--
```
And it returns successfully! 
