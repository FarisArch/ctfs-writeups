This lab is vulnerable due to a logic flaw in its password brute-force protection. To solve the lab, brute-force the victim's password, then log in and access their account page.

-   Your credentials: `wiener:peter`
-   Victim's username: `carlos`
-   [Candidate passwords](https://portswigger.net/web-security/authentication/auth-lab-passwords)
## Hint
Advanced users may want to solve this lab by using a macro or the Turbo Intruder extension. However, it is possible to solve the lab without using these advanced features.

# Practical
So let's check out the login works. So we're given our credentials and our victim is carlos.
Let's try to brute force carlos's password. 

After a while we get this error.
```txt
Invalid password after many attempts. Please try again 1 minute later.
```
So our IP is on cool down for 1 minute, let's try adding a header to see if it can bypass it.
```txt
X-Forwarded-For: 100
```
And it didn't work, reading back from our notes in Portswigger.
```txt
For example, you might sometimes find that your IP is blocked if you fail to log in too many times. In some implementations, the counter for the number of failed attempts resets if the IP owner logs in successfully. This means an attacker would simply have to log in to their own account every few attempts to prevent this limit from ever being reached.
```
So what's it's trying to suggest it that, we can avoid the cooldown by logging in to our account so that the counter resets.
So let's make a custom word list, create a username.txt with carlos and wiener alternating between each other until 100.
Next, we need a custom password list too, so let's create pass.txt with the suggested passwords and wiener's password alternating, we should get around 200 password.

For this attack, we'll be using the Pitch Fork attack type.
Since we know that a successful login returns code 302, we'll filter for that in Burp Intruder.
After a while we get this :
```txt
Response 302 : username=carlos&password=master
```

Let's try this in our login page.
And we managed to login and solved the lab!