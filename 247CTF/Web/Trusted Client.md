Developers don't always have time to setup a backend service when prototyping code. Storing credentials on the client side should be fine as long as it's obfuscated right?

# Challenge
We're greeted with a login page, we could try some default credentials and some SQL injection but by the looks of it it doesn't seem vulnerable.

Let's check out the source code.
```html
<script>
window.onload=function() {
  document.getElementById('login').onsubmit=function() {
    [][(![]+[])[+[]]+([![]]+[][[]])[+!+[]+[+[]]]+(![]+[])[!+[]+!+[]]+(!![]+[])
	...
```
And we're greeted with a very long of gibberish. I don't even know what this is.
Let's do some research first.
I searched for "Writing Javascript without using letters or numbers"
And I stumbled upon this article
* https://hackaday.com/2012/08/13/writing-javascript-without-using-any-letters-or-numbers/
Which lead me to this GitHub repository.
* https://github.com/alcuadrado/hieroglyphy

Okay but there doesn't seem to be a decoder for the language, let's check out the Issues Tab.
And Thank God, someone asked about decoding.
* https://github.com/alcuadrado/hieroglyphy/issues/10

So looks like another developed an encoder but also with a decoder great!
Let's clone his repository.
And let's open the decoder using the html file.
```js
if (this.username.value == 'the_flag_is' && this.password.value == '247CTF{6c91b7f7f12c852f8[REDACTED]93d16dba0148}'){ alert('Valid username and password!'); } else { alert('Invalid username and password!'); }
```



