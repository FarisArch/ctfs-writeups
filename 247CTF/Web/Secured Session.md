If you can guess our random secret key, we will tell you the flag securely stored in your session.

# Challenge
So heading to the challenge it just gives us the source code.
```py
import os
from flask import Flask, request, session
from flag import flag

app = Flask(__name__)
app.config['SECRET_KEY'] = os.urandom(24)

def secret_key_to_int(s):
    try:
        secret_key = int(s)
    except ValueError:
        secret_key = 0
    return secret_key

@app.route("/flag")
def index():
    secret_key = secret_key_to_int(request.args['secret_key']) if 'secret_key' in request.args else None
    session['flag'] = flag
    if secret_key == app.config['SECRET_KEY']:
      return session['flag']
    else:
      return "Incorrect secret key!"

@app.route('/')
def source():
    return "

%s

" % open(__file__).read()

if __name__ == "__main__":
    app.run()
```
Not much we can do, if we check out /flag it says Incorrect Secret Key.
I then looked at our cookies and found a session cookie.
```txt
eyJmbGFnIjp7IiBiIjoiTWpRM1ExUkdlMlJoT0RBM09UVm1PR0UxWTJGaU1tVXdNemRrTnpNNE5UZ3dOMkk1WVRreGZRPT0ifX0.YRITwA.glbqiPKKWiOsf-DJVFnZAcNyXEo
```
This looks like JWT base64.
And if we take a look at the code, it set's session to the flag.
Let's decode this.
```txt
{"flag":{" b":"MjQ3Q1RGe2RhODA3OTVmOGE1Y2FiMmUwMzdkNzM4NTgwN2I5YTkxfQ=="}}.D.ð.	[ª#Ê)h.±ðÉTYÙ.Ãr\J
```
And let's decode more.
```txt
247CTF{da80795f8a5cab2e[REDACTED]7385807b9a91}
```


